package com.kikspot.frontend.recommendation.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationImagesDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;

/**
 * Created By Bhagya On November 23rd,2015
 *	Controller class for Recommendation Module For Handling Web Services
 */
@Controller("webRecommendationController")
@RequestMapping("/recommendations")
public class RecommendationController{
	private static Logger log=Logger.getLogger(RecommendationController.class);
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	@Autowired
	@Qualifier("kikspotRecommendationLocationDtoValidator")
	private Validator kikspotRecommendationLocationDtoValidator;


	@InitBinder("kikspotRecommendationLocationDto")
	protected void initKikspotRecommendationLocationBinder(WebDataBinder binder) {
	    binder.setValidator(kikspotRecommendationLocationDtoValidator);
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param map
	 * @param kikspotRecommendationLocation
	 * @return
	 * Method for initiating the adding Recommendation Location
	 */
	
	@RequestMapping(value="/addlocation.do",method=RequestMethod.GET)
	public String initAddRecommendationLocation(Map<String,Object> map,@ModelAttribute("kikspotRecommendationLocationDto") KikspotRecommendationLocationDto kikspotRecommendationLocationDto){
		log.info("inside RecommendationController");
		try{
			map.put("title", "Add Recommendation Location");
			return "recommendation/addLocation";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Initiating Add Recommendation Location");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocation
	 * @param map
	 * @return
	 * 
	 * Method for saving the recommendation Location which are added by admin
	 */
	@RequestMapping(value="/addlocation.do",method=RequestMethod.POST)
	public String addRecommendationLocation(@Valid @ModelAttribute("kikspotRecommendationLocationDto")KikspotRecommendationLocationDto kikspotRecommendationLocationDto,BindingResult validationResult,Map<String,Object> map,RedirectAttributes redAttribs){
		try{
			if(validationResult.hasErrors()){
				return "recommendation/addLocation";
			}
			Integer savedResult=this.recommendationService.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocationDto);
			if(savedResult>0){
				redAttribs.addFlashAttribute("status", "Recommendation Location Added Successfully");
				 return "redirect:/recommendations/viewlocations.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Saving Recommendation Location");
			return "error";
		}
	}
	/**
	 * Created By bhagya On December 18th,2015
	 * @param locationId
	 * @param kikspotRecommendationLocationDto
	 * @param map
	 * @return
	 * 
	 * Method for Initiating the Edit Recommendation Location
	 */
	@RequestMapping(value="/editlocation.do",method=RequestMethod.GET)
	public String initEditRecommendationLocation(@RequestParam("locationId") Integer locationId,@ModelAttribute("kikspotRecommendationLocationDto")KikspotRecommendationLocationDto kikspotRecommendationLocationDto,Map<String,Object> map){
		log.info("inside initEditRecommendationLocation()");
		try{
			KikspotRecommendationLocationDto resultedRecommendationDto=this.recommendationService.getKikspotRecommendationLocationByLocationId(locationId);
			map.put("location", resultedRecommendationDto);
			return "recommendation/editLocation";
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Initiating Edit Recommendation Location");
			return "error";
		}
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocationDto
	 * @param map
	 * @return
	 * 
	 * Method for Updating The Recommendation Location
	 */
	
	@RequestMapping(value="/editlocation.do",method=RequestMethod.POST)
	public String editRecommendationLocation(@Valid @ModelAttribute("kikspotRecommendationLocationDto")KikspotRecommendationLocationDto kikspotRecommendationLocationDto,BindingResult validationResult,Map<String,Object> map,RedirectAttributes redAttribs){
		log.info("inside editRecommendationLocation()");
		try{
			if(validationResult.hasErrors()){
				return "recommendation/editLocation";
			}
			Integer updatedResult=this.recommendationService.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocationDto);
			if(updatedResult>0){
				redAttribs.addFlashAttribute("status", "Recommendation Location Edited Successfully");
				return "redirect:/recommendations/viewlocations.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Editing Recommendation Location");
			return "error";
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param map
	 * @param listBeanDto
	 * @return
	 * 
	 * Method for getting the Recommendation Locations which are added by admin
	 */
	@RequestMapping(value="/viewlocations.do")
	public String viewRecommendationLocations(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info("inside viewRecommendationLocations()");
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("locationId");
			}
			ArrayList<KikspotRecommendationLocationDto> kikspotRecommendationLocationDtos=this.recommendationService.getKikspotRecommendationLocations(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection());
			Integer totalResults=kikspotRecommendationLocationDtos.get(0).getTotalLocations();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("locations", kikspotRecommendationLocationDtos);
			return "recommendation/viewLocations";
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Locations Not Found");
			return "recommendation/viewLocations";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While viewing Recommendation Locations");
			return "error";
		}
	}
	/**
	 * Created By bhagya On December 18th,2015
	 * @param kikspotLocationId
	 * @return
	 * 
	 * Method for checking the existence of Location
	 */
	@RequestMapping(value="/checklocation.do",method=RequestMethod.GET)
	@ResponseBody
	public String checkLocationExistenceByKikspotLocationId(@RequestParam("kikspotLocationId")String kikspotLocationId){
		log.info("inside checkLocationExistenceByKikspotLocationId()");
		try{
			KikspotRecommendationLocationDto kikspotRecommendationLocationDto=this.recommendationService.getKikspotRecommendationLocationByKikspotLocationId(kikspotLocationId);
			if(null!=kikspotRecommendationLocationDto){
				return "error";
			}
			else{
				throw new KikspotRecommendationLocationNotFoundException();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			log.info("No Location Exists,You Can Add Location");
			return "success";
		}
		catch(Exception e){
			log.error("Error While Validating Location Existence");
			return "error";
		}
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * Method for Deleting the Kikspot Recommendation Location
	 */
	@ResponseBody
	@RequestMapping(value="/deletelocation.do")
	public String deleteKikspotRecommendationLocation(@RequestParam("locationId")Integer locationId){
		log.info("inside deleteKikspotRecommendationLocation()");
		try{
			Integer deletedResult=this.recommendationService.deleteKikspotRecommendationLocationByLocationId(locationId);
			if(deletedResult>0){
				return "success";
			}
			else{
				throw new Exception();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			log.info("Recommendation Location Does not Exists");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Deleting Location");
			return "error";
		}
	
	}
	/**
	 * Created By Bhagya On december 22nd,2015
	 * @param locationId
	 * @return
	 * 
	 * Method for getting the recommendation Location Images By Location Id
	 */
	@RequestMapping(value="/locationimages.do")
	public String getRecommendationLocationImagesBylocationId(@RequestParam("locationId")Integer locationId,Map<String,Object> map){
		log.info("inside getRecommendationLocationImagesBylocationId()");
		try{
			ArrayList<RecommendationLocationImagesDto> recommendationLocationImagesDtos=this.recommendationService.getRecommendationLocationImagesByLocationId(locationId);
			map.put("locationImages", recommendationLocationImagesDtos);
			return "recommendation/editLocationImages";
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found");
			return "error";
		}
		catch(RecommendationLocationImagesNotFoundException e){
			map.put("locationId", locationId);
			map.put("message", "Recommendation Location Images Not Found");
			return "recommendation/editLocationImages";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Getting Recommendation Location Images");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param imageId
	 * @param map
	 * @return
	 * 
	 * Method for deleting the Location Image
	 */
	@RequestMapping(value="/deletelocationimage.do")
	public String deleteRecommendationLocationImageByImageId(@RequestParam("locationImageId") Integer locationImageId,Map<String,Object> map,@RequestParam(value="locationId",required=false)Integer locationId){
		log.info("inside deleteRecommendationLocationImageByImageId()");
		try{
			Integer deletedResult=this.recommendationService.deleteRecommendationLocationImageById(locationImageId);
			if(deletedResult>0){
				return "redirect:/recommendations/locationimages.do?locationId="+locationId;
			}
			else{
				throw new Exception();
			}
		}
		catch(RecommendationLocationImagesNotFoundException e){
			map.put("message", "Recommendation Location Image Not Found For Deleting");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Deleting Recommendation Location Image");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param locationId
	 * @param locationImages
	 * @return
	 * 
	 * Method for Adding the Location Images For Recommendation Location
	 */
	@RequestMapping(value="/addlocationimages.do",method=RequestMethod.POST)
	public String addLocationImagesForRecommendationLocation(@RequestParam("locationId") Integer locationId,@RequestParam("locationImages") List<MultipartFile> locationImagesFile,Map<String,Object> map){
		log.info("inside addingLocationImagesForRecommendationLocation");
		try{
			Integer savedResult=this.recommendationService.addRecommendationLocationImagesforRecommendationLocation(locationId, locationImagesFile);
			if(savedResult>0){
				return "redirect:/recommendations/locationimages.do?locationId="+locationId;
			}
			else{
				throw new Exception();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found For Adding Location Images");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Adding Recommendation Location Images");
			return "error";
		}
	
	}
}