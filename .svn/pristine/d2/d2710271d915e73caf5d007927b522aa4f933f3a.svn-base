package com.kikspot.frontend.notification.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.kikspot.backend.notification.dao.NotificationDao;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.UnsubscribedNotification;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.frontend.notification.dto.NotificationTypeDto;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;


/***
 * 
 * Created by Jeevan on DECEMBER 22, 2015
 * 
 * Service for Notification..
 * 
 * 
 * @author KNS-ACCONTS
 *
 */
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService{

	
	@Resource(name="userDao")
	private UserDao userDao;
	
	
	@Resource(name="notificationDao")
	private NotificationDao notificationDao;
	
	
	
	private static Logger log=Logger.getLogger(NotificationServiceImpl.class);
	
	private static ApnsService service;
	
	
	
/* ** Push Notification Parameters */	
	private String enviroment;
	
	private String passPhrase;
	
	private String certFile;

	public String getEnviroment() {
		return enviroment;
	}

	public void setEnviroment(String enviroment) {
		this.enviroment = enviroment;
	}

	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}

	public String getCertFile() {
		return certFile;
	}

	public void setCertFile(String certFile) {
		this.certFile = certFile;
	}
	
	
	
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 * Method to getPushNotificationService..
	 * 
	 * 
	 * @throws IOException
	 */
	public void getPushNotificationService()throws IOException{
		log.info("inside getPushNotificationService() ");
		InputStream certificate=this.getClass().getResourceAsStream(certFile);
		if(enviroment.equals("sandbox")){
			service=APNS
					.newService()
					.withCert(certificate, passPhrase)
					.withSandboxDestination()
					.build();
		}
		else if(enviroment.equals("production")){
			service=APNS
					.newService()
					.withCert(certificate, passPhrase)
					.withProductionDestination()
					.build();
		}
		Map<String, Date> devices=service.getInactiveDevices();
		System.out.println("Inactive Devices "+devices.size());
		ArrayList<String> deviceTokens=new ArrayList<String>();
		for(Entry<String, Date> entry:devices.entrySet()){
			deviceTokens.add(entry.getKey());			
		}
		try{
			ArrayList<UserDevice> userDevices=this.userDao.getUserDeviceByDeviceTokens(deviceTokens);
			for(UserDevice userDevice:userDevices){
				this.userDao.deleteUserDevice(userDevice);
			}
		}
		catch(Exception e){			
		}
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 23, 2015
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<NotificationTypeDto> getNotificationSettingsofUser(Integer userId)throws Exception{
		log.info("inside getNotificationSettingsofUser() ");
		ArrayList<NotificationType> notificationTypes=this.notificationDao.getAllNotificationTypes();
		ArrayList<UnsubscribedNotification> unsubscribedNotifications;
		try{
			unsubscribedNotifications=this.notificationDao.getAllUnsubscribedNotificationsofUser(userId);
		}
		catch(Exception e){
			unsubscribedNotifications=new ArrayList<UnsubscribedNotification>();
		}
		
		ArrayList<NotificationTypeDto> notificationTypeDtos=new ArrayList<NotificationTypeDto>();
		for(NotificationType notificationType:notificationTypes){
			NotificationTypeDto notificationTypeDto=new NotificationTypeDto();
			notificationTypeDto.setNotificationTypeId(notificationType.getNotificationTypeId());
			notificationTypeDto.setNotificationType(notificationType.getNotificationType());
			if(unsubscribedNotifications.size()>0){
				for(UnsubscribedNotification unsubscribedNotification : unsubscribedNotifications){
					
					if(unsubscribedNotification.getNotificationType().getNotificationTypeId().equals(notificationType.getNotificationTypeId())){
						notificationTypeDto.setIsEnabled(false);
					}
					else{
						notificationTypeDto.setIsEnabled(true);
					}
				}
			}
			else{
				notificationTypeDto.setIsEnabled(true);
			}
				notificationTypeDtos.add(notificationTypeDto);
			
		}		
		return notificationTypeDtos;
	}
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 23, 2015
	 * 
	 * Method to enableorDisableUserNotification..
	 * 
	 * @param userId
	 * @param notificationTypeId
	 * @param isEnable
	 * @return
	 * @throws Exception
	 */
	public Integer enableorDisableUserNotification(Integer userId,Integer notificationTypeId,Boolean isEnable)throws Exception{
		log.info("inside enableorDisableUserNotification() ");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		NotificationType notificationType=this.notificationDao.getNotificationTypeById(notificationTypeId);
		UnsubscribedNotification unsubscribedNotification;
		Integer result;
		if(isEnable){
			unsubscribedNotification=this.notificationDao.getUnsubscribedNotificationByUserandType(userId, notificationTypeId);
			result=this.notificationDao.deleteUnsubscribedNotification(unsubscribedNotification);
		}
		else{
			unsubscribedNotification=new UnsubscribedNotification();
			unsubscribedNotification.setKikspotUser(kikspotUser);
			unsubscribedNotification.setNotificationType(notificationType);
			result=this.notificationDao.saveorUpdateUnsubscribedNotifications(unsubscribedNotification);
		}
		return result;		
	}
	
	
	
	
	/***
	 * Created by Jeevan on DECEMBER 23, 2015
	 * Method to send PushNotification..
	 * 
	 * @param message
	 * @param userDevice
	 * @throws Exception
	 */
	public void sendPushNotification(String message,UserDevice userDevice)throws Exception{
		log.info("inside sendPushNotification() ");
		if(null==service){
			try{
				getPushNotificationService();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		log.info("Pushing notification to device message : "+message+", userId : "+userDevice.getKikspotUser().getUserId());
		//build payload(A JSON string consisting of all notification data in specified format and send it
		String payload = APNS.newPayload().alertBody(message).build();
		service.push(userDevice.getDeviceToken(), payload);
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 28, 2015
	 * 
	 *  Method to sendAdminNotificationtoUsers..
	 *  
	 * @param userIds
	 * @param message
	 * @param sendAll
	 * @throws Exception
	 */
	public void sendAdminNotificationtoUsers(ArrayList<Integer> userIds, String message,Boolean sendAll)throws Exception{
		log.info("inside sendAdminNotificationtoUsers() ");
		
		ArrayList<UserDevice> userDevices=new ArrayList<UserDevice>();
		try{
			if(sendAll){
				userDevices=this.userDao.getAllUserDevices();			
			}
			else{
				userDevices=this.userDao.getUserDevicesByUserIds(userIds);
			}
		}
		catch(Exception e){
		}
		for(UserDevice userDevice:userDevices){
			this.sendPushNotification(message, userDevice);
		}
	}
	
	
	
	
	
}
