package com.kikspot.frontend.recommendation.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationImagesDto;
import com.kikspot.frontend.recommendation.dto.UserLocationRatingsDto;

/**
 * Created By Bhagya On November 23rd,2015
 * Interface for the Recommendation Service
 */
public interface RecommendationService{
	public ArrayList<RecommendationLocationDto> getLocations(Integer userId,Double latitude,Double longitude,String pageToken,String sortBy) throws Exception;
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId) throws Exception;
	public Integer removeLocationFromUserList(Integer userId,String locationAPIId)throws Exception;
	public Integer saveUserLocationRatingtoDB(UserLocationRatingsDto userLocationRatingsDto) throws Exception;
	public Integer saveOrUpdateKikspotRecommendationLocation(KikspotRecommendationLocationDto kikspotRecommendationLocationDto) throws Exception;
	public KikspotRecommendationLocationDto getKikspotRecommendationLocationByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException;
	public ArrayList<KikspotRecommendationLocationDto> getKikspotRecommendationLocations(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRecommendationLocationNotFoundException;
	public KikspotRecommendationLocationDto getKikspotRecommendationLocationByKikspotLocationId(String kikspotLocationId) throws KikspotRecommendationLocationNotFoundException;
	public Integer deleteKikspotRecommendationLocationByLocationId(Integer locationId) throws Exception;
	public ArrayList<RecommendationLocationImagesDto> getRecommendationLocationImagesByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException, RecommendationLocationImagesNotFoundException;
	public Integer deleteRecommendationLocationImageById(Integer locationImageId) throws Exception;
	public Integer addRecommendationLocationImagesforRecommendationLocation(Integer locationId,List<MultipartFile> locationImagesFile) throws Exception;
}