package com.kikspot.frontend.user.service;

/**
 * Created By Bhagya On october 13th,2015
 * Implementation class for UserService Interface
 */
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.exceptions.AccountInActiveException;
import com.kikspot.backend.exceptions.EmailAlreadyExistsException;
import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserDeviceNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.exceptions.UserLocationNotFoundException;
import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;
import com.kikspot.backend.exceptions.UsernameAlreadyExistsException;
import com.kikspot.backend.game.dao.CredDao;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.user.model.UserLocations;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.backend.user.model.UserRole;
import com.kikspot.backend.user.model.UserType;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.EmailSender;
import com.kikspot.frontend.game.service.CredService;
import com.kikspot.frontend.user.dto.ChangePasswordDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.dto.UserLocationDto;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

	private static Logger log = Logger.getLogger(UserServiceImpl.class);

	@Resource(name = "userDao")
	private UserDao userDao;

	@Resource(name = "emailSender")
	private EmailSender emailSender;

	@Resource(name = "credService")
	private CredService credService;

	@Resource(name="credDao")
	private CredDao credDao;
	
	
	
	@Autowired
	private org.springframework.security.crypto.password.PasswordEncoder bCryptEncoder;

	private String userProfilePath;

	private String shareUrl;

	public String getUserProfilePath() {
		return userProfilePath;
	}

	public void setUserProfilePath(String userProfilePath) {
		this.userProfilePath = userProfilePath;
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}

	/**
	 * Created By Bhagya On October 19th,2015
	 * 
	 * @param email
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for to send the password reset email to user
	 * @throws MailNotSentException
	 */
	public Integer sendPasswordResetEmail(String email)
			throws UserNotFoundException, UserNotSavedOrUpdatedException,
			MailNotSentException {
		log.info("inside sendPasswordResetEmail()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserByEmail(email);
		String passwordToken = this.generatePasswordToken();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		Date validity = cal.getTime();
		kikspotUser.setPasswordTokenExpiryDate(validity);
		kikspotUser.setPasswordToken(passwordToken);
		this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		KikspotUserDto kikspotUserDto = KikspotUserDto
				.populateKikspotUserDto(kikspotUser);
		this.emailSender.sendForgotPasswordMail(kikspotUserDto);
		return 1;
	}

	/**
	 * Created By Bhagya On october 19th,2015 This Method return random UUID
	 * java.util.UUID class represents an immutable universally unique
	 * identifier (UUID)
	 * 
	 */
	private String generatePasswordToken() {
		String uuid = UUID.randomUUID().toString();
		String token = uuid.toString().replaceAll("-", "").toUpperCase();
		return token;
	}

	/**
	 * Created By Bhagya On october 19th,2015
	 * 
	 * @param token
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method For Getting the UserId By Password Reset TOken
	 */
	public Integer getUserIdByPasswordResetToken(String token)
			throws UserNotFoundException {
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserByPasswordResetToken(token);
		return kikspotUser.getUserId();
	}

	/**
	 * Created By Bhagya On october 20th, 2015
	 * 
	 * @param userId
	 * @param password
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for hadling the process of password reset
	 */
	public Integer handlePasswordReset(Integer userId, String password)
			throws UserNotFoundException, UserNotSavedOrUpdatedException {
		log.info("inside handlePasswordReset()");
		Integer result = 0;
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		Date passwordTokenExpiryDate = kikspotUser.getPasswordTokenExpiryDate();
		if (passwordTokenExpiryDate.after(new Date())) {
			String encryptedPassword = bCryptEncoder.encode(password);
			kikspotUser.setPassword(encryptedPassword);
			kikspotUser.setPasswordToken(null);
			kikspotUser.setPasswordTokenExpiryDate(null);
			result = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		}
		return result;
	}

	/**
	 * Created By Bhagya On October 26th,2015
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for handling the facebook login users check the
	 *             kikspot user already exists or not by emaillId If user exists
	 *             means returns the userId If user Not exists means , its
	 *             create a new user and return the created userId
	 * 
	 *             Modified by bhagya on December 10th,2015 Setting the defaullt
	 *             value for isActive
	 */
	public Integer handleFacebookLoginUsers(KikspotUserDto kikspotUserDto)
			throws UserNotSavedOrUpdatedException {
		log.info("inside handleFacebookLoginUsers()");
		try {
			KikspotUser kikspotUser = this.userDao
					.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			return kikspotUser.getUserId();
		} catch (UserNotFoundException e) {
			KikspotUser kikspotUser = new KikspotUser();
			kikspotUser.setEmailId(kikspotUserDto.getEmailId());
			kikspotUser.setFirstName(kikspotUserDto.getFirstName());
			kikspotUser.setLastName(kikspotUserDto.getLastName());
			kikspotUser.setUserRole(UserRole.ROLE_USER);
			kikspotUser.setIsActive(true); // by default true
			Integer userId = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			try {
				this.credService.addCredPostUserSignup(kikspotUser);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return userId;
		}
	}

	/**
	 * Created By Bhagya On October 27th,2015
	 * 
	 * @param emailId
	 * @return
	 * 
	 *         Method For getting the kikspot user by email Id
	 * 
	 *         Modified By Bhagya On December 10th,2015 Handling the Reactive
	 *         Users
	 */
	public KikspotUserDto getUserByEmailId(String emailId) {
		log.info("inside getUserByEmailId();");
		try {
			
			KikspotUser user = this.userDao.getKikspotUserforAuthentication(emailId);
			if (user != null) {
				if (null != user.getReactivationDate()
						&& user.getReactivationDate().before(new Date())) {
					this.updateUserStatus(user);
				}
				KikspotUserDto userDto = KikspotUserDto
						.populateKikspotUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service " + e.toString());
			return null;
		}
	}

	/**
	 * 
	 * Created by Jeevan on November 23, 2015
	 * 
	 * Method to initUserSignup.
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 *             Modified by bhagya on December 10th,2015 Setting the defaullt
	 *             value for isActive
	 */
	@SuppressWarnings("unused")
	public KikspotUserDto initUserSignUp(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside initUserSignUp");
		KikspotUser kikspotUser;
		if (null != kikspotUserDto.getUserId()
				&& kikspotUserDto.getUserId() > 0) {
			kikspotUser = this.userDao.getKikspotUserById(kikspotUserDto
					.getUserId());
		} else {
			kikspotUser = new KikspotUser();
			kikspotUser.setIsActive(true); // by default true
		}
		try {
			KikspotUser user = this.userDao
					.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			throw new EmailAlreadyExistsException();
		} catch (UserNotFoundException e) {

		}
		try {
			KikspotUser user = this.userDao
					.getKikspotUserByUsername(kikspotUser.getUsername());
			throw new UsernameAlreadyExistsException();
		} catch (UserNotFoundException e) {

		}

		kikspotUser.setUsername(kikspotUserDto.getUsername());
		kikspotUser.setEmailId(kikspotUserDto.getEmailId());
		kikspotUser.setPassword(bCryptEncoder.encode(kikspotUserDto
				.getPassword()));
		kikspotUser.setUserRole(UserRole.ROLE_USER);
		;
		kikspotUser.setUserType(UserType.LOGGEDIN_USER);
		kikspotUser.setAccountCreationDate(new Date());
		kikspotUser = this.getReferralofLoggedinUser(kikspotUser,
				kikspotUserDto);

		// Need to have code related to creds.
		// Get

		Integer userSavedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		// Need to have logic to send mail..

		kikspotUserDto.setUserId(userSavedResult);
		this.credService.addCredPostUserSignup(kikspotUser);
		
		return kikspotUserDto;
	}

	private KikspotUser getReferralofLoggedinUser(KikspotUser kikspotUser,
			KikspotUserDto kikspotUserDto) {
		try {
			UserProbableReferral probableReferral = this.userDao
					.getUserProbableReferralByEmail(kikspotUserDto.getEmailId());
			kikspotUser.setReferredUser(probableReferral.getKikspotUser());

		} catch (UserProbableReferralNotFoundException e) {
			log.warn("NO USER PROBABLE REFERRAL FOUND AND NEED TO BE ADDED NEW REFERRAL CODE");
		}
		return kikspotUser;
	}

	/**
	 * 
	 * Created by Jeevan on November 24, 2015
	 * 
	 * Method to CompleteSignup Process.
	 * 
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 *             Will be used by Both social login and Email Sign up..
	 * 
	 * 
	 */
	public Integer completeSignUp(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside completeSignUp()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(kikspotUserDto.getUserId());
		
		String savedName=kikspotUser.getFirstName();
		
		kikspotUser.setFirstName(kikspotUserDto.getFirstName());
		kikspotUser.setLastName(kikspotUserDto.getLastName());
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		kikspotUser.setDob(kikspotUserDto.getDob());
		kikspotUser.setAddress1(kikspotUserDto.getAddress1());
		kikspotUser.setAddress2(kikspotUserDto.getAddress2());
		kikspotUser.setCity(kikspotUserDto.getCity());
		kikspotUser.setState(kikspotUserDto.getState());
		kikspotUser.setZip(kikspotUserDto.getZip());

		// For Social Login, Need to obtain username
		if (null != kikspotUserDto.getUsername()
				&& kikspotUserDto.getUsername().trim().length() > 0
				&& (null == kikspotUser.getUsername() || kikspotUser
						.getUsername().trim().length() == 0)) {
			try {
				KikspotUser user = this.userDao
						.getKikspotUserByUsername(kikspotUser.getUsername());
				throw new UsernameAlreadyExistsException();
			} catch (UserNotFoundException e) {
				kikspotUser.setUsername(kikspotUserDto.getUsername());
			}
		}
		Integer userSavedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		if(null==savedName || savedName.trim().length()<1){
				this.emailSender.sendRegistrationMail(kikspotUserDto);
		}
		return userSavedResult;
	}

	/**
	 * 
	 * Created by Jeevan on November 24, 2015
	 * 
	 * Method to perform Social Login..
	 * 
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 *             Modified by bhagya on December 10th,2015 For Existing
	 *             User,checking the reactivation date is before the current
	 *             date and updating the user account status For new
	 *             User,Setting the defaullt value for isActive
	 */
	public KikspotUserDto performSocialLogin(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside performSocialLogin() ");

		KikspotUser kikspotUser;
		try {
			kikspotUser = this.userDao.getKikspotUserByEmail(kikspotUserDto
					.getEmailId());
			if (null != kikspotUser.getReactivationDate()
					&& kikspotUser.getReactivationDate().before(new Date())) {
				this.updateUserStatus(kikspotUser);
			}
			KikspotUserDto userDto = KikspotUserDto
					.populateKikspotUserDto(kikspotUser);
			if (userDto.getIsActive() == false) {
				throw new AccountInActiveException();
			}
			return userDto;
		} catch (UserNotFoundException e) {
			log.info("User Logged in for first time ");
			if (null != kikspotUserDto.getUserId()
					&& kikspotUserDto.getUserId() > 0) {
				kikspotUser = this.userDao.getKikspotUserById(kikspotUserDto
						.getUserId());
			} else {
				kikspotUser = new KikspotUser();
				kikspotUser.setIsActive(true); // by default true
			}
			kikspotUser.setUsername(kikspotUserDto.getUsername());
			kikspotUser.setEmailId(kikspotUserDto.getEmailId());
			kikspotUserDto.setFirstName(kikspotUserDto.getFirstName());
			kikspotUserDto.setLastName(kikspotUserDto.getLastName());
			kikspotUserDto.setAccountCreationDate(new Date());
			kikspotUser.setUserRole(UserRole.ROLE_USER);
			kikspotUser.setUserType(UserType.SOCIAL_USER);
			kikspotUser = this.getReferralofLoggedinUser(kikspotUser,
					kikspotUserDto);

			Integer userSavedResult = this.userDao
					.saveOrUpdateKikspotUser(kikspotUser);
			// Need to have logic to send mail..
			kikspotUserDto.setUserId(userSavedResult);
			this.credService.addCredPostUserSignup(kikspotUser);
			this.emailSender.sendRegistrationMail(kikspotUserDto);
			// Added on DEC 11, 2015... logic to saveUserDeviceToken..
			this.saveUserDeviceTokentoDB(kikspotUser,
					kikspotUserDto.getDeviceToken());
			return kikspotUserDto;
		}
	}

	/**
	 * Created by Jeevan on Novemeber 24, 2015 Method to registerByPassUser..
	 * 
	 * @return
	 * @throws Exception
	 * 
	 *             Modified by bhagya on December 10th,2015 Setting the defaullt
	 *             value for isActive
	 */
	public Integer registerByPassUser(String deviceToken) throws Exception {
		log.info("inside registerByPassUser() ");
		KikspotUser kikspotUser = new KikspotUser();
		kikspotUser.setAccountCreationDate(new Date());
		kikspotUser.setUserRole(UserRole.ROLE_USER);
		kikspotUser.setUserType(UserType.BYPASS_USER);
		kikspotUser.setIsActive(true); // by default true
		Integer userSavedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		this.saveUserDeviceTokentoDB(kikspotUser, deviceToken);
		return userSavedResult;
	}

	/**
	 * Created by Jeevan on
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 *             Modified By Bhagya On December 10th,2015 Checking the Account
	 *             Active Status and if Account is InActive means throw
	 *             AccountInActiveException
	 */

	public Integer authenticateMobileUser(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside authenticateMobileUser() ");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserforAuthentication(kikspotUserDto.getUsername());
		if (null != kikspotUser.getReactivationDate()
				&& kikspotUser.getReactivationDate().before(new Date())) {
			this.updateUserStatus(kikspotUser);
		}
		if (kikspotUser.getIsActive() == false) {
			throw new AccountInActiveException();
		} else if (bCryptEncoder.matches(kikspotUserDto.getPassword(),
				kikspotUser.getPassword())) {
			// Added on DECEMBER 11, 2015... code to update UserDeviceToken on
			// each login..
			this.saveUserDeviceTokentoDB(kikspotUser,
					kikspotUserDto.getDeviceToken());
			return kikspotUser.getUserId();
		} else {
			throw new PasswordNotMatchException();
		}
	}

	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 * 
	 * Method to handle saving or updating User Device TOken..
	 * 
	 * 
	 * @param kikspotUser
	 * @param deviceToken
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	private Integer saveUserDeviceTokentoDB(KikspotUser kikspotUser,
			String deviceToken) throws Exception {
		log.info("inside saveUserDeviceTokentoDB()");
		UserDevice userDevice;
		try {
			userDevice = this.userDao.getUserDeviceofUser(kikspotUser);
		} catch (UserDeviceNotFoundException e) {
			userDevice = new UserDevice();
			userDevice.setKikspotUser(kikspotUser);
		}
		userDevice.setDeviceType("iOS");
		userDevice.setDeviceToken(deviceToken);
		userDevice.setUpdatedDate(new Date());
		Integer deviceSavedResult = this.userDao
				.saveorUpdateUserDevice(userDevice);
		return deviceSavedResult;
	}

	/**
	 * 
	 * Created by Jeevan on DECEMBER 11, 2015 Method to updateUserLocation...
	 * 
	 * 
	 * @param userLocationDto
	 * @throws Exception
	 */
	public void updateUserLocation(UserLocationDto userLocationDto)
			throws Exception {
		log.info("inside updateUserLocation() ");
		UserLocations userLocation;
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(userLocationDto.getUserId());
		try {
			userLocation = this.userDao.getUserLocationsOfUser(kikspotUser);
		} catch (UserLocationNotFoundException e) {
			userLocation = new UserLocations();
			userLocation.setKikspotUser(kikspotUser);
		}
		userLocation.setLatitude(userLocationDto.getLatitude());
		userLocation.setLongitude(userLocationDto.getLongitude());
		userLocation.setUpdatedDate(new Date());
		this.userDao.saveorUpdateUserLocation(userLocation);
	}

	/**
	 * Created by Jeevan on DECEMBER 14, 2015
	 * 
	 * Method to saveUserPReferenceandProfileImage ofUSer..
	 * 
	 * 
	 * @param userId
	 * @param preferredVenueType
	 * @param profile
	 * @throws Exception
	 * 
	 * 
	 *             Steps: 1. Get KikspotYUserBy Id 2. Set PreferredVenue Type.
	 *             2. From Multipart, obtain file type. Save file to Server on
	 *             TIMESTAMP.type 4. Set profile pic path to database.. 5. If
	 *             needed create context path..
	 * 
	 * 
	 * 
	 */
	public Integer saveUserPreferenceandProfileImage(Integer userId,
			String preferredVenueType, MultipartFile profile) throws Exception {
		log.info("inside saveUserPreferenceandProfileImage() ");
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		kikspotUser.setPreferredVenueType(preferredVenueType);
		System.out.println(profile.getContentType() + " " + profile.getName()
				+ " " + profile.getSize());
		String fileType = profile.getOriginalFilename().substring(
				profile.getOriginalFilename().lastIndexOf(".") + 1);
		File imageFile = new File(userProfilePath + userId + "/"
				+ new Date().getTime() + "." + fileType);
		System.out.println("FILE NAME AND PATH " + imageFile.getAbsolutePath());
		if (!imageFile.exists()) {
			imageFile.mkdirs();
		}
		profile.transferTo(imageFile);
		kikspotUser.setProfilePicture(imageFile.getName());
		Integer userUpdatedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		return userUpdatedResult;
	}

	/**
	 * Created by Jeevan on DECEMBER 15, 2015 Method to init Forgot Pasword
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * 
	 *             Steps : 1. GET Kikspot User by ID 2. Generate unique code 3.
	 *             Save it to user 4. Send a mail containing code.. 5. Send
	 *             response to mobile app.
	 * 
	 */
	public Integer initForgotPasswordforMobile(String email) throws Exception {
		log.info("inside initForgotPasswordforMobile() ");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserforAuthentication(email);
		kikspotUser.setPasswordToken(RandomStringUtils.randomNumeric(4));
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		kikspotUser.setPasswordTokenExpiryDate(cal.getTime());
		Integer updatedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		// sending forgot password mail.
		this.emailSender.sendForgotPasswordMailforMobileUser(kikspotUser);
		return updatedResult;
	}

	/**
	 * Created by Jeevan on DECEMBER 15, 2015
	 * 
	 * Method to rest
	 * 
	 * @param email
	 * @param token
	 * @throws Exception
	 */
	public void resetMobileUserPassword(String email, String token,
			String newPassword) throws Exception {
		log.info("inside resetMobileUserPassword()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserByEmail(email);
		if (token.equals(kikspotUser.getPasswordToken())) {
			kikspotUser.setPasswordToken("");
			kikspotUser.setPasswordTokenExpiryDate(null);
			kikspotUser.setPassword(this.bCryptEncoder.encode(newPassword));
			this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		} else {
			throw new PasswordNotMatchException();
		}
	}

	/**
	 * Created By Bhagya On December 10th,2015
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method For Updating the User Status ,when the reactivation
	 *             date is before the current date
	 */

	public Integer updateUserStatus(KikspotUser kikspotUser)
			throws UserNotSavedOrUpdatedException {
		log.info("inside UserServiceImpl -> updateUserStatus()");
		kikspotUser.setIsActive(true);
		kikspotUser.setReactivationDate(null);
		this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return 0;
	}

	/**
	 * Created By Bhagya On december 03rd, 2015
	 * 
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method For Getting the List Of ALl Users Modified By Bhagya
	 *             On December 10th,2015 If Reactivation Date is before the
	 *             todays date means, we are activating that user
	 *             
	 *             
	 */
	public ArrayList<KikspotUserDto> getAllUsers(Integer pageNo,Integer pageSize, String sortBy, String searchBy, Boolean ascending)
			throws UserNotFoundException {
		log.info("inside AdminServiceImpl -> getAllUsers()");
		ArrayList<KikspotUser> kikspotUsers = this.userDao.getAllUsers(pageNo,pageSize, sortBy, searchBy, ascending);
		Map<Integer, Integer> userCredMap=this.getCredsMapofUser(kikspotUsers);
		ArrayList<KikspotUserDto> kikspotUserDtos = new ArrayList<KikspotUserDto>();
		for (KikspotUser kikspotUser : kikspotUsers) {
			if (null != kikspotUser.getReactivationDate()
					&& kikspotUser.getReactivationDate().before(new Date())) {
				try {
					this.updateUserStatus(kikspotUser);
				} catch (UserNotSavedOrUpdatedException e) {
					e.printStackTrace();
				}
			}
			KikspotUserDto kikspotUserDto = KikspotUserDto.populateKikspotUserDto(kikspotUser);
			if(null!=userCredMap.get(kikspotUser.getUserId())){
				kikspotUserDto.setCreds(userCredMap.get(kikspotUser.getUserId()));
			}
			else{
				kikspotUserDto.setCreds(0);
			}
			
			kikspotUserDtos.add(kikspotUserDto);
		}

		return kikspotUserDtos;
	}

	
	
	
	private Map<Integer, Integer> getCredsMapofUser(ArrayList<KikspotUser> kikspotUsers){
		log.info("inside getCredsMapofUser() ");
		Map<Integer, Integer> userCredMap=new HashMap<Integer, Integer>();
		try{
			ArrayList<UserGameCreds> userGameCreds=this.credDao.getGameCredsofUsers(kikspotUsers);
			for(UserGameCreds userGameCred:userGameCreds){
				userCredMap.put(userGameCred.getKikspotUser().getUserId(), userGameCred.getCreds());
			}
		}
		catch(Exception e){
	
		}
		return userCredMap;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Created By Bhagya On December 10th,2015
	 * 
	 * @param isActive
	 * @param reactivationDate
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 */
	public Integer savingChangedUserStatus(Integer userId, Boolean isActive,
			Date reactivationDate) throws UserNotFoundException,
			UserNotSavedOrUpdatedException {
		log.info("inside AdminServiceImpl -> savingChangedUserStatus()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		kikspotUser.setIsActive(isActive);
		kikspotUser.setReactivationDate(reactivationDate);
		Integer savedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return savedResult;
	}

	
	
	/**
	 * Created By Bhagya On December 16th,2015
	 * 
	 * @param changePasswordDto
	 * @return
	 * @throws UserNotFoundException
	 * @throws PasswordNotMatchException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for perform the change Password
	 */
	public Integer performChangePassword(ChangePasswordDto changePasswordDto)
			throws UserNotFoundException, PasswordNotMatchException,
			UserNotSavedOrUpdatedException {
		log.info("inside processChangePassword()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(changePasswordDto.getUserId());
		String newPassword = bCryptEncoder.encode(changePasswordDto
				.getNewPassword());
		if (!bCryptEncoder.matches(changePasswordDto.getOldPassword(),
				kikspotUser.getPassword())) {
			throw new PasswordNotMatchException();
		}
		kikspotUser.setPassword(newPassword);
		Integer savedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return savedResult;
	}

	/**
	 * Created By Bhagya On December 17th,2015
	 * 
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method for getting the Kikspot User By Username
	 */
	public KikspotUserDto getKikspotUserByUsername(String username)
			throws UserNotFoundException {
		log.info("inside getKikspotUserByUsername()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserforAuthentication(username);
		KikspotUserDto kikspotUserDto = KikspotUserDto
				.populateKikspotUserDto(kikspotUser);
		return kikspotUserDto;
	}

	/**
	 * Created By Firdous on december 18th,2015 Method for Updating the Admin
	 * Edit Profile
	 */

	@Override
	public Integer saveInfo(KikspotUserDto kikspotUserDto) throws Exception {
		log.info("inside getEdit()");
		KikspotUser user = this.userDao.getKikspotUserById(kikspotUserDto
				.getUserId());
		user.setFirstName(kikspotUserDto.getFirstName());
		user.setLastName(kikspotUserDto.getLastName());
		user.setAddress1(kikspotUserDto.getAddress1());
		user.setAddress2(kikspotUserDto.getAddress2());
		user.setCity(kikspotUserDto.getCity());
		user.setState(kikspotUserDto.getState());
		user.setZip(kikspotUserDto.getZip());
		user.setDob(kikspotUserDto.getDob());
		Integer result = this.userDao.saveOrUpdateKikspotUser(user);
		return result;
	}

	/**
	 * Created By Bhagya On December 24th,2015
	 * 
	 * @param kikspotUserId
	 * @return
	 * @throws UserNotFoundException
	 * @throws RefferdUsersNotFoundException
	 * 
	 *             Method for getting the List of Reffered Users Of a User
	 */

	public ArrayList<KikspotUserDto> getReferredUsersOfUser(
			Integer kikspotUserId) throws UserNotFoundException,
			ReferredUsersNotFoundException {
		log.info("inside getReferredUsersOfUser()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(kikspotUserId);
		ArrayList<KikspotUser> referredUsers = this.userDao
				.getReferredUsersByKikspotUser(kikspotUser);
		ArrayList<KikspotUserDto> referredUserDtos = new ArrayList<KikspotUserDto>();
		for (KikspotUser referredUser : referredUsers) {
			KikspotUserDto refferedUserDto = KikspotUserDto
					.populateKikspotUserDto(referredUser);
			referredUserDtos.add(refferedUserDto);
		}
		return referredUserDtos;
	}

	/**
	 * Created By Bhagya On december 28th,2015
	 * 
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method for getting the Kikspot User Details By kikspotUserId
	 */

	public KikspotUserDto getKikspotUserDetailsByUserId(Integer userId)
			throws UserNotFoundException {
		log.info("inside getKikspotUserDetailsByUserId()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		KikspotUserDto kikspotUserDto = KikspotUserDto
				.populateKikspotUserDto(kikspotUser);
		return kikspotUserDto;
	}

	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * Method to sendShareUrlofUser..
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String sendShareUrlofUser(Integer userId) throws Exception {
		log.info("inside sendShareUrlofUser() ");
		String url = shareUrl + "?referredUser=" + userId;
		return url;
	}

	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * MEthod to saveProbableReferredUser..
	 * 
	 * @param userId
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public Integer saveProbableReferredUser(Integer userId, String email)
			throws Exception {
		log.info("inside saveProbableReferredUser()");
		UserProbableReferral userProbableReferral = new UserProbableReferral();
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		userProbableReferral.setEmail(email);
		userProbableReferral.setKikspotUser(kikspotUser);
		Integer savedResult = this.userDao
				.saveorUpdateUserProbableReferral(userProbableReferral);
		return savedResult;
	}
	
	
	public Integer updateUserGameCreds(Integer userId,Integer creds)throws Exception{
		log.info("inside updateUserGameCreds()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		Integer savedResult=this.credService.updateUserCreds(kikspotUser, creds);
		return savedResult;
	}

}