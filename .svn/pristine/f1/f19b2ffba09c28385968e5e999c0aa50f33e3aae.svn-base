package com.kikspot.backend.game.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.Game;
import com.kikspot.backend.game.model.GameLevels;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.game.model.UserGameCreds;



/**
 * Created by Jeevan on November 27, 2015
 * 
 *  Dao for GameDao
 *  
 * @author KNS-ACCONTS
 *
 */
@Repository("gameDao")
@Transactional
public class GameDaoImpl implements GameDao{

	
	private static Logger log=Logger.getLogger(GameDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
/***************************************** GAME RULES ******************************************/	
	/**
	 * 
	 * @param gameRule
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateGameRule(GameRules gameRule)throws Exception{
		log.info("inside saveorUpdateGameRule() ");
		sessionFactory.getCurrentSession().saveOrUpdate(gameRule);
		sessionFactory.getCurrentSession().flush();
		return gameRule.getGameRuleId();
	}
	
	
	
	
	/**
	 * 
	 * @param gameId
	 * @return
	 * @throws GameRuleNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameRules> getGameRulesofGame(Integer gameId,Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending,String locationFilter)throws GameRuleNotFoundException{
		log.info("inside getGameRulesofGame() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		criteria.add(Restrictions.eq("game.gameId", gameId));
		if(null!=searchBy){
			criteria.add(Restrictions.ilike("rule", "%"+searchBy+"%"));
		}
		if(null!=sortBy){
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));
			}
			else{
				criteria.addOrder(Order.desc(sortBy));
			}				
		}
		
		//Location Filter
		if(null!=locationFilter && locationFilter.trim().length()>0){
			if(locationFilter.equalsIgnoreCase("GLOBAL")){
				criteria.add(Restrictions.or(Restrictions.isNull("locationId"),Restrictions.eq("locationId", "")));
			}
			else{
				criteria.add(Restrictions.or(Restrictions.eq("locationId", locationFilter), Restrictions.eq("locationName", locationFilter)));
			}
		}

		
		Integer totalRules=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<GameRules> gameRules=(ArrayList<GameRules>) criteria.list();
		if(!gameRules.isEmpty()){
			gameRules.get(0).setTotalRules(totalRules);
			return gameRules;
		}
		else{
			throw new GameRuleNotFoundException();
		}		
	}
	
	

	
	
	
	
/********************************* GAME LEVELS **********************************************************/	
	
	/**
	 * 
	 * @param gameLevel
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateGameLevel(GameLevels gameLevel)throws Exception{
		log.info("inside saveorUpdateGameLevel() ");
		sessionFactory.getCurrentSession().saveOrUpdate(gameLevel);
		sessionFactory.getCurrentSession().flush();
		return gameLevel.getGameLevelId();
	}
	
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on DEC 03, 2015
	 * 
	 * Method to get GameLevelsofGame..
	 * 
	 * 
	 * @param gameId
	 * @return
	 * @throws GameLevelNotFoundException
	 * 
	 * Modified By BHagya on December 14th,2015
	 * Implemented searching,sorting,pagination
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameLevels> getGameLevelsofGame(Integer gameId,Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending)throws GameLevelNotFoundException{
		log.info("inside getGameLevelsofGame()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameLevels.class);
		criteria.add(Restrictions.eq("game.gameId", gameId));
		if(null!=searchBy){
			criteria.add(Restrictions.ilike("level", "%"+searchBy+"%"));
		}
		if(null!=sortBy){
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));
			}
			else{
				criteria.addOrder(Order.desc(sortBy));
			}				
		}
		
		Integer totalLevels=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<GameLevels> gameLevels=(ArrayList<GameLevels>) criteria.list();
		if(!gameLevels.isEmpty()){
			gameLevels.get(0).setTotalLevels(totalLevels);
			return gameLevels;
		}
		else{
			throw new GameLevelNotFoundException();
		}
	}
	
	
	
	
	
	   /**
     * 
     * Added by Firdous on 02-12-2015
     * method to get the Game rule by accepting game rule id
     * 
     */

	@SuppressWarnings("unchecked")
	public GameRules getGameRuleByRuleId(Integer id) throws GameRuleNotFoundException {
		log.info("inside getGameRuleByRuleId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		criteria.add(Restrictions.eq("gameRuleId",id));
		ArrayList<GameRules> gameRule=(ArrayList<GameRules>)criteria.list();
		
		if(!gameRule.isEmpty()){
		return gameRule.get(0);
		}
		else{
			throw new GameRuleNotFoundException();
		}
	}

	


	/**
	 * Added by Firdous
	 * Method to delete the game rule
	 *
	 */
	
	public Integer deleteGameRule(GameRules gamerule) throws Exception {
		log.info("inside deleteGameRule()");
		 sessionFactory.getCurrentSession().delete(gamerule);
		 sessionFactory.getCurrentSession().flush();
		 return 1;
	}

	/**
	 * added by Firdous on 08-12-2015
	 * method to get game by game id
	 * 
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Game getGameById(int i) throws Exception {
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Game.class);
		criteria.add(Restrictions.eq("gameId",i));
		ArrayList<Game> game=(ArrayList<Game>) criteria.list();
		if(!game.isEmpty()){
			return game.get(0);
		}
		else{
			throw new Exception();
		}		
	}
	/**
	 * Created By Bhagya On december 14th,2015
	 * @param gameLevelId
	 * @return
	 * @throws GameLevelNotFoundException
	 * 
	 * Method For Gettting the Game Level By GameLevelId
	 */
	@SuppressWarnings("unchecked")
	public GameLevels getGameLevelByGameLevelId(Integer gameLevelId) throws GameLevelNotFoundException{
		log.info("inside getGameLevelVyGameLevelId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameLevels.class);
		criteria.add(Restrictions.eq("gameLevelId",gameLevelId ));
		ArrayList<GameLevels> gameLevels=(ArrayList<GameLevels>) criteria.list();
		if(!gameLevels.isEmpty()){
			return gameLevels.get(0);
		}
		else{
			throw new GameLevelNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevel
	 * @return
	 * @throws Exception
	 * 
	 * Method For Deleting The Game Level
	 */
	public Integer deleteGameLevel(GameLevels gameLevel) throws Exception {
		log.info("inside deleteGameLevel()");
         sessionFactory.getCurrentSession().delete(gameLevel);
		 sessionFactory.getCurrentSession().flush();
		 return 1;
	}
	/**
	 * Created By Bhagya On December 31st,2015
	 * @param locationId
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method For Getting The Game Rules By Location Id For ADMIN
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameRules> getGameRulesByLocationId(String locationId) throws GameRuleNotFoundException{
		log.info("inside getGameRulesByLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		criteria.add(Restrictions.eq("locationId", locationId));
		ArrayList<GameRules> gameRules=(ArrayList<GameRules>) criteria.list();
		if(!gameRules.isEmpty()){
			return gameRules;
		}
		else{
		}
			throw new GameRuleNotFoundException();
		
	}
	/**
	 * Created By bhagya On Jan 05th,2015
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method for getting the distinct locations of a Game Rules
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getDistinctLocationsOfGameRules() throws GameRuleNotFoundException{
		log.info("inside getDistinctLocationsOfGameRules()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		ProjectionList projectionList=Projections.projectionList();
		projectionList.add(Projections.property("locationId"));
		projectionList.add(Projections.property("locationName"));
		criteria.setProjection(Projections.distinct(projectionList));
		List<Object[]> locations= criteria.list();
		ArrayList<String> locationStrings=new ArrayList<String>();
		for(Object[] location:locations){
			String locationId = String.valueOf(location[0]); 
			String locationName = String.valueOf(location[1]); 
			
			if(!locationName.equalsIgnoreCase("null") && locationName.length()>0){
				locationStrings.add(locationName);
			}
			else if(locationId.length()>0 && !locationId.equalsIgnoreCase("null")){
				locationStrings.add(locationId);
			}
			else if(!locationStrings.contains("GLOBAL")){
				if(locationId.equalsIgnoreCase("null") || locationId.length()<=0 ){
					locationStrings.add("GLOBAL");
				}
			}
		}
		
		return locationStrings;
	}
	
	/**
	 * Created By BHagya On Jan 06th,2016
	 * @param locationId
	 * @param isActive
	 * @param checkExpiryDate
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method For Getting the Game Rules Of a Location By Location Id FOr USER
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameRules> getGameRulesOfaLocationByLocationIdForUser(String locationId,Boolean isActive,Boolean checkExpiryDate,String sortBy) throws GameRuleNotFoundException{
		log.info("inside getGameRulesOfaLocationByLocationIdForUser()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		//criteria.add(Restrictions.eq("locationId", locationId));
		criteria.add(Restrictions.or(Restrictions.eq("locationId", locationId),Restrictions.isNull("locationId")));
		if(isActive==false){
			criteria.add(Restrictions.eq("isActive", false));
		}
		else{
			criteria.add(Restrictions.eq("isActive", true));
		}
		if(checkExpiryDate==true){
			criteria.add(Restrictions.ge("expiryDate", new Date()));
		}
		if(null!=sortBy && sortBy.equalsIgnoreCase("time")){
			criteria.addOrder(Order.asc("expiryDate"));
		}
		ArrayList<GameRules> locationGameRules=(ArrayList<GameRules>) criteria.list();
		if(!locationGameRules.isEmpty()){
			return locationGameRules;
		}
		else{
			throw new GameRuleNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param isActive
	 * @param checkExpiryDate
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method For Getting ALL Game Rules
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameRules> getAllGameRules(Boolean isActive,Boolean checkExpiryDate,String sortBy) throws GameRuleNotFoundException{
		log.info("inside getAllGameRules()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		if(isActive==false){
			criteria.add(Restrictions.eq("isActive", false));
		}
		else{
			criteria.add(Restrictions.eq("isActive", true));
		}
		if(checkExpiryDate==true){
			criteria.add(Restrictions.ge("expiryDate", new Date()));
		}
		if(null!=sortBy && sortBy.equalsIgnoreCase("time")){
			criteria.addOrder(Order.asc("expiryDate"));
		}
		ArrayList<GameRules> gameRules=(ArrayList<GameRules>) criteria.list();
		if(!gameRules.isEmpty()){
			return gameRules;
		}
		else{
			throw new GameRuleNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param locationAPIIds
	 * @param isActive
	 * @param checkExpiryDate
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method For Getting The Game Rules For List Of Locations
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameRules> getGameRulesForListOfLocations(List<String> locationAPIIds,Boolean isActive,Boolean checkExpiryDate,String sortBy) throws GameRuleNotFoundException{
		log.info("inside getGameRulesForListOfLocations()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		//criteria.add(Restrictions.in("locationId", locationAPIIds));
		criteria.add(Restrictions.or(Restrictions.in("locationId", locationAPIIds),Restrictions.isNull("locationId")));
		if(isActive==false){
			criteria.add(Restrictions.eq("isActive", false));
		}
		else{
			criteria.add(Restrictions.eq("isActive", true));
		}
		if(checkExpiryDate==true){
			criteria.add(Restrictions.ge("expiryDate", new Date()));
		}
		if(null!=sortBy && sortBy.equalsIgnoreCase("time")){
			criteria.addOrder(Order.asc("expiryDate"));
		}
		ArrayList<GameRules> gameRulesOfLocations=(ArrayList<GameRules>) criteria.list();
		if(!gameRulesOfLocations.isEmpty()){
			return gameRulesOfLocations;
		}
		else{
			throw new GameRuleNotFoundException();
		}
	}
	
	
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param gameRuleId
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method For Getting The Game Rule By Game Rule Id
	 */
	
	@SuppressWarnings("unchecked")
	public GameRules getGameRuleByGameRuleId(Integer gameRuleId) throws GameRuleNotFoundException{
		log.info("inside getGameRuleByGameRuleId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		criteria.add(Restrictions.eq("gameRuleId", gameRuleId));
		ArrayList<GameRules> gameRules=(ArrayList<GameRules>) criteria.list();
		if(!gameRules.isEmpty()){
			return gameRules.get(0);
		}
		else{
			throw new GameRuleNotFoundException();
		}
		
	}
	
	
	
	
	/**
	 * Created by Jeevan on FEB 22, 2016
	 * 
	 * Method to getActiveGameRulesofGivenLocations..
	 * 
	 * @param locationIds
	 * @return
	 * @throws Exception
	 */
	public Set<String> getActiveGameRulesOfGivenLocations(Set<String> locationIds)throws Exception{
		log.info("inside getActiveGameRulesofGivenLocations");
		
		Set<String> activeLocations=new LinkedHashSet<String>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		//criteria.add(Restrictions.in("locationId", locationAPIIds));
		criteria.add(Restrictions.in("locationId", locationIds));		
		criteria.add(Restrictions.eq("isActive", true));	
		criteria.add(Restrictions.ge("expiryDate", new Date()));
		criteria.setProjection(Projections.property("locationId"));
		
		
		activeLocations.addAll(criteria.list());
		return activeLocations;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param minCreds
	 * @return
	 * @throws GameLevelNotFoundException
	 * 
	 * Method for getting the Game Level By Minimum creds
	 */
	
	@SuppressWarnings("unchecked")
	public GameLevels getGameLevelByMinimumCreds(Integer minCreds) throws GameLevelNotFoundException{
		log.info("inside getGameLevelByMinimumCreds()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameLevels.class);
		criteria.add(Restrictions.ge("minCreds", minCreds));
		criteria.addOrder(Order.asc("minCreds"));
		ArrayList<GameLevels> gameLevels=(ArrayList<GameLevels>) criteria.list();
		if(!gameLevels.isEmpty()){
			return gameLevels.get(0);
		}
		else{
			throw new GameLevelNotFoundException();
		}
	}
	
	
	/**
	 * Created By Bhagya On Jan 11th,2016
	 * @param gameLevel
	 * @return
	 * @throws UserGameCredNotFoundException
	 * 
	 * Method for getting the users from user game creds table by criteria of game level
	 * 
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getUsersGameCredsByGameLevel(String gameLevel) throws UserGameCredNotFoundException{
		log.info("inside getUsersByGameLevel()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.add(Restrictions.eq("level", gameLevel));
		ArrayList<UserGameCreds> users=(ArrayList<UserGameCreds>) criteria.list();
		if(!users.isEmpty()){
			return users;
		}
		else{
			throw new UserGameCredNotFoundException();
		}
	}
	/**
	 * Created By bhagya On January 22nd,2016
	 * @param level
	 * @return
	 * @throws GameLevelNotFoundException
	 * 
	 * Method for getting the Game Level By level
	 */
	@SuppressWarnings("unchecked")
	public GameLevels getGameLevelByLevel(String level) throws GameLevelNotFoundException{
		log.info("inside getGameLevelByLevel()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(GameLevels.class);
		criteria.add(Restrictions.eq("level", level));
		ArrayList<GameLevels> gameLevels=(ArrayList<GameLevels>) criteria.list();
		if(!gameLevels.isEmpty()){
			return gameLevels.get(0);
		}
		else{
			throw new GameLevelNotFoundException();
		}
	}
}
