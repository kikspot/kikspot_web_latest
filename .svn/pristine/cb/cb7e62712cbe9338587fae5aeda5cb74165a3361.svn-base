package com.kikspot.frontend.mobile.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleInActiveException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.user.service.UserService;


/***
 * 
 * 
 *  Created by Jeevan on November 27, 2015
 *  Controller for Game...
 * 
 * @author KNS-ACCONTS
 *
 */


@RestController
@RequestMapping("/mobile")
public class GameController {

	
	private static Logger log=Logger.getLogger(GameController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="gameService")
	private GameService gameService;
	
	
	/**
	 * Created by Jeevan on November 27, 2015
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping("/dashboard.do")
	public String getUserDashboard(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getUserDashboard()");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			UserDashboardDto userDashboardDto=this.gameService.getUserDashboardInformation(userId);
		
			json.accumulate("USER",userDashboardDto.getKikspotUserDto().getFirstName());
			json.accumulate("CREDS", userDashboardDto.getUserGameCredDto().getCreds());
			json.accumulate("TRENDING", userDashboardDto.getTrending());
			ArrayList<JSONObject> topCredUsers=new ArrayList<JSONObject>();
			int i=1;
			for(UserGameCredsDto userGameCredDto:userDashboardDto.getUserGameCredsDtos()){
				JSONObject topCredUser=new JSONObject();
				topCredUser.accumulate("POSITION", i);
				topCredUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getFirstName());
				topCredUser.accumulate("CREDS",userGameCredDto.getCreds());
				topCredUsers.add(topCredUser);
				i++;
			}
			json.accumulate("TOPCREDUSERS",topCredUsers);
			ArrayList<JSONObject> gameRuleJsons=new ArrayList<JSONObject>();
			for(GameRulesDto gameRuleDto:userDashboardDto.getGameRulesDtos()){
				JSONObject gameRuleJson=new JSONObject();
				gameRuleJson.accumulate("RULE", gameRuleDto.getRule());
				gameRuleJsons.add(gameRuleJson);
			}
			json.accumulate("GAMERULES", gameRuleJsons);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching User Dashboard");
			json.accumulate("ERROR", "Error While Fetching Dashboard");
		}
		System.out.println(json.toString());
		return json.toString();
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For getting the Game Rules Of A Location
	 */
	@RequestMapping("/getgamerulesoflocation.do")
	public String getGameRulesOfaLocation(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameRulesOfaLocation");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			String locationAPIId=inputJson.getString("locationAPIId");
			Boolean isActive=inputJson.getBoolean("isActive");
			Boolean checkExpiryDate=inputJson.getBoolean("checkExpiryDate");
			String sortBy=null;
			if(null!=inputJson.get("sortBy")){
				sortBy=inputJson.getString("sortBy");
			}
			ArrayList<GameRulesDto> locationGameRulesDtos=this.gameService.getGameRulesOfaLocationByLocationIdForUser(locationAPIId, isActive, checkExpiryDate,sortBy);
			ArrayList<JSONObject> locationGameRulesJSONS=new ArrayList<JSONObject>();
			for(GameRulesDto locationGameRulesDto: locationGameRulesDtos){
				JSONObject locationGameRuleJSON=new JSONObject();
				locationGameRuleJSON.accumulate("gameRuleId", locationGameRulesDto.getGameRuleId());
				if(null!=locationGameRulesDto.getRule()){
					locationGameRuleJSON.accumulate("rule", locationGameRulesDto.getRule());
				}
				else{
					locationGameRuleJSON.accumulate("rule", "");
				}
				locationGameRuleJSON.accumulate("ruleLimit", locationGameRulesDto.getRuleLimit());
				locationGameRuleJSON.accumulate("creds", locationGameRulesDto.getCreds());
				Date expiryDate=locationGameRulesDto.getExpiryDate();
				SimpleDateFormat formatExpiryDate=new SimpleDateFormat("MM/dd/yyyy");
				locationGameRuleJSON.accumulate("expiryDate", formatExpiryDate.format(expiryDate));
				locationGameRulesJSONS.add(locationGameRuleJSON);
			}
			outputJson.accumulate("locationGameRules", locationGameRulesJSONS);
			
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rules Not Found For Location");
			outputJson.accumulate("ERROR", "Game Rules Not Found For Location");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Game Rules For Location");
			outputJson.accumulate("ERROR", "Error While Fetching Game Rules For Location");
		}
		System.out.println("JSON RESPONSE "+outputJson.toString());
		return outputJson.toString();
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For Getting ALL Game Rules
	 */
	@RequestMapping("/getgamerules.do")
	public String getAllGameRules(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getAllGameRules()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Boolean isActive=inputJson.getBoolean("isActive");
			Boolean checkExpiryDate=inputJson.getBoolean("checkExpiryDate");
			Integer userId=inputJson.getInt("userId");
			String sortBy=null;
			if(null!=inputJson.get("sortBy")){
				sortBy=inputJson.getString("sortBy");
			}
			ArrayList<GameRulesDto> gameRulesDtos=this.gameService.getAllGameRules(isActive, checkExpiryDate,sortBy,userId);
			ArrayList<JSONObject> gameRulesJSONS=new ArrayList<JSONObject>();
			for(GameRulesDto gameRulesDto:gameRulesDtos){
				JSONObject gameRulesJSON=new JSONObject();
				if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
					gameRulesJSON.accumulate("locationAPIId", gameRulesDto.getLocationId());
				}
				else{
					gameRulesJSON.accumulate("locationAPIId", "");
				}
				gameRulesJSON.accumulate("gameRuleId", gameRulesDto.getGameRuleId());
				if(null!=gameRulesDto.getRule()){
					gameRulesJSON.accumulate("rule", gameRulesDto.getRule());
				}
				else{
					gameRulesJSON.accumulate("rule", "");
				}
				gameRulesJSON.accumulate("ruleLimit", gameRulesDto.getRuleLimit());
				gameRulesJSON.accumulate("creds", gameRulesDto.getCreds());
				Date expiryDate=gameRulesDto.getExpiryDate();
				SimpleDateFormat formatExpiryDate=new SimpleDateFormat("MM/dd/yyyy");
				gameRulesJSON.accumulate("expiryDate", formatExpiryDate.format(expiryDate));
				gameRulesJSONS.add(gameRulesJSON);
			}
			outputJson.accumulate("locationGameRules", gameRulesJSONS);
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rules Not Found");
			outputJson.accumulate("ERROR", "Game Rules Not Found");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Game Rules");
			outputJson.accumulate("ERROR", "Error While Fetching Game Rules");
		}
		System.out.println("JSON RESPONSE "+outputJson.toString());
		return outputJson.toString();
	}
	/***
	 * Created By Bhagya On Jan 06th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for getting The Game Rules Of Locations List
	 */
	@RequestMapping("/getgamerulesoflocationslist.do")
	public String getGameRulesForListOfLocations(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameRulesForListOfLocations()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			JSONArray locationAPIIds=inputJson.getJSONArray("locationAPIIds");
			List<String> locationAPIIdsList=new ArrayList<String>();
			for(int i=0;i<locationAPIIds.length();i++){
				String locationAPIId=locationAPIIds.getString(i);
				locationAPIIdsList.add(locationAPIId);
			}
			Boolean isActive=inputJson.getBoolean("isActive");
			Boolean checkExpiryDate=inputJson.getBoolean("checkExpiryDate");
			Integer userId=inputJson.getInt("userId");
			String sortBy=null;
			if(null!=inputJson.get("sortBy")){
				sortBy=inputJson.getString("sortBy");
			}
			ArrayList<GameRulesDto> gameRulesLocationsDtos=this.gameService.getGameRulesForListOfLocations(locationAPIIdsList, isActive, checkExpiryDate,sortBy,userId);
			ArrayList<JSONObject> gameRulesLocationsJSONS=new ArrayList<JSONObject>();
			for(GameRulesDto gameRuleLocationsDto: gameRulesLocationsDtos){
				JSONObject gameRulesLocationsJSON= new JSONObject();
				if(null!=gameRuleLocationsDto.getLocationId() && gameRuleLocationsDto.getLocationId().trim().length()>0){
					gameRulesLocationsJSON.accumulate("locationAPIId", gameRuleLocationsDto.getLocationId());
				}
				else{
					gameRulesLocationsJSON.accumulate("locationAPIId", "");
				}
				gameRulesLocationsJSON.accumulate("gameRuleId", gameRuleLocationsDto.getGameRuleId());
				if(null!=gameRuleLocationsDto.getRule()){
					gameRulesLocationsJSON.accumulate("rule", gameRuleLocationsDto.getRule());
				}
				else{
					gameRulesLocationsJSON.accumulate("rule", "");
				}
				gameRulesLocationsJSON.accumulate("ruleLimit", gameRuleLocationsDto.getRuleLimit());
				gameRulesLocationsJSON.accumulate("creds", gameRuleLocationsDto.getCreds());
				Date expiryDate=gameRuleLocationsDto.getExpiryDate();
				SimpleDateFormat formatExpiryDate=new SimpleDateFormat("MM/dd/yyyy");
				gameRulesLocationsJSON.accumulate("expiryDate", formatExpiryDate.format(expiryDate));
				gameRulesLocationsJSONS.add(gameRulesLocationsJSON);
			}
			outputJson.accumulate("gameRulesOfLocations", gameRulesLocationsJSONS);
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rules Not Found For Locations");
			outputJson.accumulate("ERROR", "Game Rules Not Found For Locations");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Game Rules For Locations");
			outputJson.accumulate("ERROR", "Error While Fetching Game Rules For Locations");
		}
		System.out.println("JSON RESPONSE "+outputJson.toString());
		return outputJson.toString();
	}
	/**
	 * Created By Bhagya on Jan 07th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for Updating the User Game Creds For Game Rule
	 */
	@RequestMapping("/updateusergamecreds.do")
	public String updateUserGameCredsByGameRule(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside updateUserGameCredsByGameRule()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer gameRuleId=inputJson.getInt("gameRuleId");
			Integer userId=inputJson.getInt("userId");
			String locationId=inputJson.getString("locationAPIId");
			UserGameCredsDto userGameCredsDto=this.gameService.updateUserGameCredsByGameRuleId(gameRuleId, userId, locationId);
			JSONObject userGameCredsJSON=new JSONObject();
			userGameCredsJSON.accumulate("earnedCreds", userGameCredsDto.getCreds());
			userGameCredsJSON.accumulate("level", userGameCredsDto.getLevel());
			outputJson.accumulate("userGameCreds", userGameCredsJSON);
		}
		catch(UserNotFoundException e){
			log.error("User Not Found For Updating User Game Creds");
			outputJson.accumulate("ERROR", "User Not Found For Updating User Game Creds");
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rule Not Found For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Not Found For Updating User Game Creds");
		}
		catch(GameRuleInActiveException e){
			log.error("Game Rule Is InActive For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Is InActive For Updating User Game Creds");
		}
		catch(GameRuleExpiredException e){
			log.error("Game Rule Is Expired For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Is Expired For Updating User Game Creds");
		}
		catch(GameRuleExceedsRuleLimitException e){
			log.error("Game Rule Exceeds Rule Limit For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Exceeds Rule Limit For Updating User Game Creds");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Updating User Game Creds");
			outputJson.accumulate("ERROR", "Error While Updating User Game Creds");
		}
		System.out.println(outputJson.toString());
		return outputJson.toString();
	}
}
