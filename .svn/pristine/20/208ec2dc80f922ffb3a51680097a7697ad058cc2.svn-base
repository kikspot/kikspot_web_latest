package com.kikspot.backend.recommendation.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.exceptions.UserRemovedLoctionsNotFoundException;
import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;
import com.kikspot.backend.recommendation.model.LocalAPIRecommendationLocations;
import com.kikspot.backend.recommendation.model.LocationRatingToggler;
import com.kikspot.backend.recommendation.model.RecommendationLocation;
import com.kikspot.backend.recommendation.model.RecommendationLocationImages;
import com.kikspot.backend.recommendation.model.UserLocationRatings;
import com.kikspot.backend.recommendation.model.UserRecommendationLocationImages;
import com.kikspot.backend.recommendation.model.UserRemovedLocation;
import com.kikspot.backend.user.model.KikspotUser;



/**
 * Created by Jeevan on December 07, 2015
 * 
 *  DAO for RecommendationDao..
 *  
 *  
 * @author KNS-ACCONTS
 *
 */
@Transactional
@Repository("recommendationDao")
public class RecommendationDaoImpl implements RecommendationDao {

	
	private static Logger log=Logger.getLogger(RecommendationDaoImpl.class);
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on December 07, 2015
	 * 
	 * Method to saveOrUpdate UserRemovedLocation..
	 * 
	 * 
	 * 
	 * @param userRemovedLocation
	 * @return
	 * @throws Exception
	 */
	public Integer saveOrUpdateUserRemovedLocation(UserRemovedLocation userRemovedLocation)throws Exception{
		log.info("inside saveorUpdateUserRemovedLocation() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userRemovedLocation);
		sessionFactory.getCurrentSession().flush();
		return userRemovedLocation.getRemovedLocationId();
	}
	
	
	
	
	/**
	 * Created by Jeevan on December 07, 2015
	 * Method to get USerRemovedLocations..
	 * 
	 * 
	 * @param userId
	 * @return
	 * @throws UserRemovedLoctionsNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserRemovedLocation> getUserRemovedLocations(Integer userId)throws UserRemovedLoctionsNotFoundException{
		log.info("inside getUserRemovedLocations");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserRemovedLocation.class);
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		ArrayList<UserRemovedLocation> userRemovedLocations=(ArrayList<UserRemovedLocation>) criteria.list();
		if(!userRemovedLocations.isEmpty()){
			return userRemovedLocations;
		}
		else{
			throw new UserRemovedLoctionsNotFoundException();
		}		
	}
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on Decemeber 08, 2015
	 * 
	 * Method to saveUserLocationRatings..
	 * 
	 * 
	 * @param userLocationRatings
	 * @return
	 * @throws Exception
	 * 
	 * Modified By Bhagya On Jan 08th,2016
	 * 	Change the method to only save the rating(removed the update operation)
	 */
	public Integer saveUserLocationRatings(UserLocationRatings userLocationRatings)throws Exception{
		log.info("inside saveorUpdateUserLocationRatings() ");
		sessionFactory.getCurrentSession().save(userLocationRatings);
		sessionFactory.getCurrentSession().flush();
		return userLocationRatings.getUserLocationRatingId();
	}
	
	
	
	
	
	
	
	/**
	 * Created by Jeevan on December 08, 2015
	 * Method to getAverageUserLocationRatings..
	 * 
	 * 
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> getAllAverageUserLocationRatings()throws UserLocationRatingsNotFoundException{
		log.info("inside getAllAverateUserLocationRatings");
		Map<String, Double> ratingMap=new LinkedHashMap<String, Double>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		ProjectionList projectionList=Projections.projectionList();
		projectionList.add(Projections.property("locationAPIId"));
		projectionList.add(Projections.avg("rating"));
		projectionList.add(Projections.groupProperty("locationAPIId"));
		criteria.setProjection(projectionList);	
		if(criteria.list().size()>0){
			List<Object[]> rows=criteria.list();		
			if(rows.size()>0){
				for(Object[] row:rows){
					ratingMap.put((String)row[0], (Double)row[1]);
				}	
				return ratingMap;
			}
			else{
				return ratingMap;
			}
		}
		else{
			return ratingMap;
		}
	}
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on December 08, 2015
	 *  
	 *  Method to getAverageUserLocationRating..
	 *  
	 * 
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 */
	public Integer getAverageUserLocationRating(String locationAPIId)throws UserLocationRatingsNotFoundException{
		log.info("inside getAverageUserLocationRating() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class)
				.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.setProjection(Projections.avg("rating"));
		if(!criteria.list().isEmpty()){
			return (Integer)criteria.list().get(0);
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on
	 *  December 09, 2015
	 *  
	 *  Method to getLocationRatingByUserandLocation..
	 *  
	 * 
	 * @param userId
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * 
	 */
	
	@SuppressWarnings("unchecked")
	public UserLocationRatings getLocationRatingByUserandLocation(Integer userId,String locationAPIId)throws UserLocationRatingsNotFoundException{
		log.info("inside getLocationRatingByUserandLocation() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser.userId", userId),Restrictions.eq("locationAPIId", locationAPIId)));
		ArrayList<UserLocationRatings> userLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!userLocationRatings.isEmpty()){
			return userLocationRatings.get(0);
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocation
	 * @return
	 * 
	 * Method For save Or Update the kikspot recommendation Locations Which are added By Admin
	 */
	
	public Integer saveOrUpdateKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation)throws Exception{
		log.info("inside saveOrUpdateKikspotRecommendationLocation()");
		sessionFactory.getCurrentSession().saveOrUpdate(kikspotRecommendationLocation);
		sessionFactory.getCurrentSession().flush();
		return kikspotRecommendationLocation.getLocationId();
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the KikspotRecommendation Location By location Id
	 */
	@SuppressWarnings("unchecked")	
	public KikspotRecommendationLocation getKikspotRecommendationLocationByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocationByLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		criteria.add(Restrictions.eq("locationId", locationId));
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			return kikspotRecommendationLocations.get(0);
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @param ascending
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the Recommendation Locations Which are added by Admin
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotRecommendationLocation> getKikspotRecommendationLocations(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocations()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		if(null!=searchBy){
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("kikspotLocationId", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("locationName", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("address", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("phoneNo", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("url", searchBy,
					MatchMode.ANYWHERE));
						
			criteria.add(disjunction);
		}
		if(null!=sortBy){
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));
			}
			else{
				criteria.addOrder(Order.desc(sortBy));
			}				
		}
		
		Integer totalLocations=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			kikspotRecommendationLocations.get(0).setTotalLocations(totalLocations);
			return kikspotRecommendationLocations;
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the KikspotRecommendation Location By Kikspot location Id
	 */
	@SuppressWarnings("unchecked")	
	public KikspotRecommendationLocation getKikspotRecommendationLocationByKikspotLocationId(String kikspotLocationId) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocationByLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		criteria.add(Restrictions.eq("kikspotLocationId", kikspotLocationId));
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			return kikspotRecommendationLocations.get(0);
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	
	
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocation
	 * @return
	 * 
	 * Method for Deleting the kikspot Recommendation Location
	 */
	public Integer deleteKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation)throws Exception{
		log.info("inside deleteKikspotRecommendationLocation()");
		sessionFactory.getCurrentSession().delete(kikspotRecommendationLocation);
		sessionFactory.getCurrentSession().flush();
		return 1;
	}
	
	
	
	
	
	
	
	
	/**
	 * created By Bhagya On December 21st,2015
	 * @param recommendationLocationImages
	 * @return
	 * 
	 * Method for save or update the recommendation Location Images
	 */
	
	public Integer saveOrUpdateKikspotRecommendationLocationImages(RecommendationLocationImages recommendationLocationImages)throws Exception{
		log.info("inside saveOrUpdateKikspotRecommendationLocationImages()");
		sessionFactory.getCurrentSession().saveOrUpdate(recommendationLocationImages);
		sessionFactory.getCurrentSession().flush();
		return recommendationLocationImages.getLocationImageId();
	}
	/**
	 * Created By Bhagya On december 22nd,2015
	 * @param kikspotRecommendationLocation
	 * @return
	 * @throws RecommendationLocationImagesNotFoundException
	 * 
	 * Method for getting the list of recommendation location Images by Kikspot recommendation Location
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<RecommendationLocationImages> getRecommendationLocationImagesByKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation) throws RecommendationLocationImagesNotFoundException{
		log.info("inside getRecommendationLocationImagesByKikspotRecommendationLocation()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(RecommendationLocationImages.class);
		criteria.add(Restrictions.eq("kikspotRecommendationLocation", kikspotRecommendationLocation));
		ArrayList<RecommendationLocationImages> recommendationLocationImages=(ArrayList<RecommendationLocationImages>) criteria.list();
		if(!recommendationLocationImages.isEmpty()){
			return recommendationLocationImages;
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param locationImageId
	 * @return
	 * @throws RecommendationLocationImagesNotFoundException
	 * 
	 * Method For getting Recommendation Location Image By LocationImageId
	 */
	@SuppressWarnings("unchecked")
	public RecommendationLocationImages getRecommendationLocationImageById(Integer locationImageId) throws RecommendationLocationImagesNotFoundException{
		log.info("inside getRecommendationLocationImageById()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(RecommendationLocationImages.class);
		criteria.add(Restrictions.eq("locationImageId", locationImageId));
		ArrayList<RecommendationLocationImages> recommendationLocationImages=(ArrayList<RecommendationLocationImages>) criteria.list();
		if(!recommendationLocationImages.isEmpty()){
			return recommendationLocationImages.get(0);
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
	}
	
	
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param recommendationLocationImages
	 * @return
	 * @throws Exception
	 * 
	 * Method for delete the recommendation Location Image
	 */
	public Integer deleteRecommendationLocationImage(RecommendationLocationImages recommendationLocationImages) throws Exception{
		log.info("inside deleteRecommendationLocationImage()");
		sessionFactory.getCurrentSession().delete(recommendationLocationImages);
		sessionFactory.getCurrentSession().flush();
		return recommendationLocationImages.getLocationImageId();
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * Method to getAllKikspotRecommendationLocationsNEarBylocation...
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws Exception
	 */
	public ArrayList<KikspotRecommendationLocation> getAllKikspotRecommendationLocationsNearbyLocation(Double latitude,Double longitude,Integer pageNo,Integer pageSize,String sortBy)throws Exception{
		log.info("inside getAllKikspotRecommendationLocationsNearbyLocation()");
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=new ArrayList<KikspotRecommendationLocation>();
		StringBuilder query=new StringBuilder();
		Integer distanceCount=3791;
		query.append("SELECT location_id,kikspot_location_id,address,icon_url,location_name,phone_no,ratings,url,location_date, ( "+distanceCount+" * acos( cos( radians("+latitude+") ) * cos( radians( latitude ) ) * ");
		query.append(" cos( radians( longitude ) - radians("+longitude+") ) + sin( radians("+latitude+") )");
		query.append("* sin( radians( latitude ) ) ) ) AS distance FROM kikspot_recommendation_location ");
		query.append("HAVING distance < 5 ORDER BY distance LIMIT 0 , 50 ");	
		SQLQuery sqlQuery=sessionFactory.getCurrentSession().createSQLQuery(query.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> rows=sqlQuery.list();
		if(rows.size()>0){
			for(Object[] row:rows){
				KikspotRecommendationLocation kikspotRecommendationLocation=new KikspotRecommendationLocation();
				kikspotRecommendationLocation.setLocationId((Integer)row[0]);
				kikspotRecommendationLocation.setKikspotLocationId((String)row[1]);
				kikspotRecommendationLocation.setAddress((String)row[2]);
				kikspotRecommendationLocation.setIconUrl((String)row[3]);
				kikspotRecommendationLocation.setLocationName((String)row[4]);
				kikspotRecommendationLocation.setPhoneNo((String)row[5]);
				kikspotRecommendationLocation.setRatings((Double)row[6]);
				kikspotRecommendationLocation.setUrl((String)row[7]);
				kikspotRecommendationLocation.setLocationDate((Date)row[8]);
				kikspotRecommendationLocation.setDistance((Double)row[9]);
				kikspotRecommendationLocations.add(kikspotRecommendationLocation);
			}	
		}
		return kikspotRecommendationLocations;
	}
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method For getting Location Ratings and sorted most recent order
	 * Steps: Getting the List of all User ratings by applying the criteria of locationAPIId
	 * 		  Sorting the results by userLocationRatingId for getting the most recent results at top
	 * 		  Return those sorted results (sequence of the most recent ratings)
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserLocationRatingsByLocationAPIId(String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Jan 08th,2016
	 * @param userRecommendationLocationImages
	 * @return
	 * 
	 * Method for save or update the User Recommendation Location Images
	 */
	
	public Integer saveOrUpdateUserRecommendationLocationImages(UserRecommendationLocationImages userRecommendationLocationImages)throws Exception{
		log.info("inside saveOrUpdateUserRecommendationLocationImages()");
		sessionFactory.getCurrentSession().saveOrUpdate(userRecommendationLocationImages);
		sessionFactory.getCurrentSession().flush();
		return userRecommendationLocationImages.getUserLocationImageId();
	}
	/**
	 * Created By BHagya On Jan 08th,2016
	 * @param kikspotUser
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method For getting the Most Recent rating of the user
	 * Steps: Getting the location rating which are satisfy the restricted criteria of kikspotuser and locatioAPIID
	 * 		  Sorting those results as descending order
	 * 		  And returning the first result(most recent)
	 */
	@SuppressWarnings("unchecked")
	public UserLocationRatings getMostRecentLocationRatingOfUser(KikspotUser kikspotUser,String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getMostRecentLocationRatingOfuser()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser", kikspotUser),Restrictions.eq("locationAPIId", locationAPIId)));
		criteria.addOrder(Order.desc("ratingDate"));
		ArrayList<UserLocationRatings> userLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!userLocationRatings.isEmpty()){
			return userLocationRatings.get(0);
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	
	
	/**
	 * 
	 *  Created by Jeevan on February 03, 2016
	 *  
	 *   MEthod to saveorUpdateLocalAPIRecommendationLocation..
	 * 
	 * @param localAPIRecommendation
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateLocalAPIRecommendationLocation(LocalAPIRecommendationLocations localAPIRecommendation)throws Exception{
		log.info("inside saveorUpdateLocalAPIRecommendationLocation() ");
		sessionFactory.getCurrentSession().saveOrUpdate(localAPIRecommendation);
		sessionFactory.getCurrentSession().flush();
		return localAPIRecommendation.getLocalAPIRecommendationLocationId();
	}
	
	
	/**
	 * 
	 * @param locationId
	 * @return
	 * @throws RecommendationLocationImagesNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public LocalAPIRecommendationLocations GetLocalAPIRecommendationLocationsByLocationId(String locationId)throws RecommendationLocationImagesNotFoundException{
		log.info("inside GetLocalAPIRecommendationLocationsByLocationId() ");
		ArrayList<LocalAPIRecommendationLocations> localAPIRecommendationLocations=new ArrayList<LocalAPIRecommendationLocations>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(LocalAPIRecommendationLocations.class)
				.add(Restrictions.eqOrIsNull("locationAPIId", locationId));
		localAPIRecommendationLocations=(ArrayList<LocalAPIRecommendationLocations>) criteria.list();
		if(!localAPIRecommendationLocations.isEmpty()){
			return localAPIRecommendationLocations.get(0);
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
	}
	
	
	
	/**
	 * Created by Jeevan on February 04, 2016
	 * Method to getLocationRatingToggler..
	 * 
	 * @return
	 * @throws Exception
	 */
	public LocationRatingToggler getLocationRatingToggler()throws Exception{
		log.info("inside getLocationRatingToggler()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(LocationRatingToggler.class);
		ArrayList<LocationRatingToggler> locationTogglers=(ArrayList<LocationRatingToggler>) criteria.list();
		return locationTogglers.get(0);
	}
	
	
	/**
	 * Created by Jeevan on February 04, 2016
	 * Method to saverUpdateLoca
	 * @param locationRatingToggler
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateLocationRatingToggler(LocationRatingToggler locationRatingToggler)throws Exception{
		log.info("inside saveorUpdateLocationRatingToggler() ");
		sessionFactory.getCurrentSession().saveOrUpdate(locationRatingToggler);
		sessionFactory.getCurrentSession().flush();
		return locationRatingToggler.getLocationRatingTogglerId();
	}
	
	
	
	
	
	
	
	
	
	
}



