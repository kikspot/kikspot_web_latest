/**
 *  Created by    : Bhagya

 * Created Date	  : November 11th, 2015
 * file Name	  : CustomAuthenticationFailureHandler.java
 * Purpose		  : Handling Autyhentication Faiulure mechanism and redirecting to User according to the Login Failure Pages..
 * Type			  : Support Utility/Service
 */


package com.kikspot.common.security.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;


/*
 * Custom Authentication Failure Adapter,
 * Handles Authentication Failure Conditions With Detailed Messages for Each Failure....
 */
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	/* pre defined */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		
		super.onAuthenticationFailure(request, response, exception);
		String message;
		System.out.println("EXCEPTION "+exception.getMessage()+" "+exception.getClass());
		if(exception.getClass().isAssignableFrom(CredentialsExpiredException.class)){
			message="Your Account is Expired, Please Contact the Admin for further Details";
		}		
		else if(exception.getClass().isAssignableFrom(LockedException.class)){
			message="Your Account is InActive, Please Contact the Admin for further Details";
		}
		else if(exception.getClass().isAssignableFrom(DisabledException.class)){
			message="Your Account is not yet Activated, Please click on the Activation link on your Mail";			
		}
		else{
			message="Invalid Login Credentials..Try Again";
		}
		
		
		
		
        request.getSession().setAttribute("error", message );      
		
	}
	
	
}
