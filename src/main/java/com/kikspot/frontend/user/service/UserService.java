package com.kikspot.frontend.user.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserPreferencesNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.user.dto.ChangePasswordDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.dto.UserLocationDto;
import com.kikspot.frontend.user.dto.UserPreferencesDto;
/**
 * Created By Bhagya On October 13th,2015
 *	Interface for the User service
 */

public interface UserService{
	public Integer sendPasswordResetEmail(String email) throws UserNotFoundException, UserNotSavedOrUpdatedException, MailNotSentException;
	public Integer getUserIdByPasswordResetToken(String token) throws UserNotFoundException;
	public Integer handlePasswordReset(Integer userId,String password) throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public Integer handleFacebookLoginUsers(KikspotUserDto kikspotUserDto) throws UserNotSavedOrUpdatedException;
	public KikspotUserDto getUserByEmailId(String emailId);
	public KikspotUserDto initUserSignUp(KikspotUserDto kikspotUserDto)throws Exception;
	public Integer completeSignUp(KikspotUserDto kikspotUserDto)throws Exception;
	public KikspotUserDto performSocialLogin(KikspotUserDto kikspotUserDto)throws Exception;
	public Integer registerByPassUser(String deviceToken)throws Exception;
	public Map<String, Object> authenticateMobileUser(KikspotUserDto kikspotUserDto) throws Exception;
	public Integer updateUserStatus(KikspotUser kikspotUser) throws UserNotSavedOrUpdatedException;
	public ArrayList<KikspotUserDto> getAllUsers(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws UserNotFoundException;
	public Integer savingChangedUserStatus(Integer userId,Boolean isActive, Date reactivationDate) throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public void updateUserLocation(UserLocationDto userLocationDto)throws Exception;
	public Integer saveUserPreferenceandProfileImage(Integer userId,String preferredVenueType, MultipartFile profile)throws Exception;
	public Integer initForgotPasswordforMobile(String email)throws Exception;
	public void resetMobileUserPassword(String email,String token,String newPassword)throws Exception;
	public Integer performChangePassword(ChangePasswordDto changePasswordDto) throws UserNotFoundException, PasswordNotMatchException, UserNotSavedOrUpdatedException;
	public KikspotUserDto getKikspotUserByUsername(String username) throws UserNotFoundException;
	public Integer updateUserProfile(KikspotUserDto kikspotUserDto) throws Exception;
	public ArrayList<KikspotUserDto> getReferredUsersOfUser(Integer kikspotUserId) throws UserNotFoundException, ReferredUsersNotFoundException;
	public KikspotUserDto getKikspotUserDetailsByUserId(Integer userId) throws UserNotFoundException;
	public String sendShareUrlofUser(Integer userId)throws Exception;
	public Integer saveProbableReferredUser(Integer userId,String email)throws Exception;
	public Integer updateUserGameCreds(Integer userId,Integer creds)throws Exception;
	public ArrayList<UserPreferencesDto> getAllUserPreferences(Integer userId) throws UserPreferencesNotFoundException, UserNotFoundException;
	public Integer saveUserPreferences(Integer userId, ArrayList<Integer> optionIds) throws Exception;
	public Integer saveUserInfo(KikspotUserDto kikspotUserDto) throws Exception;
	public Boolean isLoggedInUser(Integer userId)throws Exception;
	public Integer saveInfo(KikspotUserDto kikspotUserDto) throws Exception;
	public Integer resetUserPassword(Integer userId, String password) throws UserNotFoundException, UserNotSavedOrUpdatedException;
//	public Integer deleteUser(Integer id) throws Exception;
	public String getCityOfUserByUserId(Integer userId) throws Exception;
	public String getUserCountryByUserId(Integer userId) throws Exception;
	public void updateUserLocation(Integer userId, double latitude, double longitude) throws Exception;
	public ArrayList<KikspotUserDto> getAllUsersEcxeptByPassUsers() throws Exception;
	public void saveReferralCode(Integer userId, String referralCode) throws Exception;
	
	public String getAllUsersOfMasterGeoLocated() throws Exception ;
	public Integer saveMasterGeoLocationConfiguration(Integer masterId,Double latitude,Double longitude,List<String> users) throws Exception;
	
	
}