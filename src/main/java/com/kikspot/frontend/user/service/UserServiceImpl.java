package com.kikspot.frontend.user.service;

/**
 * Created By Bhagya On october 13th,2015
 * Implementation class for UserService Interface
 */
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.admin.model.MasterGeoLocationConfiguration;
import com.kikspot.backend.exceptions.AccountInActiveException;
import com.kikspot.backend.exceptions.EmailAlreadyExistsException;
import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.LimitedLookUserAccessDeniedException;
import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserDeviceNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.exceptions.UserLocationNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.exceptions.UserPreferenceOptionsNotFoundException;
import com.kikspot.backend.exceptions.UserPreferenceSettingNotFoundException;
import com.kikspot.backend.exceptions.UserPreferencesNotFoundException;
import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;
import com.kikspot.backend.exceptions.UsernameAlreadyExistsException;
import com.kikspot.backend.game.dao.CredDao;
import com.kikspot.backend.game.dao.GameDao;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.notification.dao.NotificationDao;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.NotificationTypes;
import com.kikspot.backend.recommendation.dao.RecommendationDao;
import com.kikspot.backend.recommendation.model.UserGeoLocationRatings;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.user.model.UserLocations;
import com.kikspot.backend.user.model.UserPreferenceOptions;
import com.kikspot.backend.user.model.UserPreferenceSetting;
import com.kikspot.backend.user.model.UserPreferences;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.backend.user.model.UserRole;
import com.kikspot.backend.user.model.UserType;
import com.kikspot.backend.useraction.dao.FeedbackDao;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.EmailSender;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.CredService;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.notification.service.NotificationService;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;
import com.kikspot.frontend.user.dto.ChangePasswordDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.dto.UserLocationDto;
import com.kikspot.frontend.user.dto.UserPreferenceOptionsDto;
import com.kikspot.frontend.user.dto.UserPreferencesDto;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsDelegate;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;
import com.notnoop.apns.DeliveryError;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

	private static Logger log = Logger.getLogger(UserServiceImpl.class);

	@Resource(name = "userDao")
	private UserDao userDao;

	
	@Resource(name = "feedbackDao")
	private FeedbackDao feedbackDao;

	@Resource(name = "emailSender")
	private EmailSender emailSender;

	@Resource(name = "credService")
	private CredService credService;

	@Resource(name="credDao")
	private CredDao credDao;
	
	@Resource(name ="gameService")
	private GameService gameService;
	
	@Resource(name ="gameDao")
	private GameDao gameDao;
	
	@Resource(name="notificationDao")
	private NotificationDao notificationDao;
	
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	@Resource(name="notificationService")
	private NotificationService notificationService;
	
	@Resource(name="recommendationDao")
	private RecommendationDao recommendationDao;
	
	@Resource(name="gameService")
	private GameService gameservice;
	
	@Autowired
	private org.springframework.security.crypto.password.PasswordEncoder bCryptEncoder;

	private String userProfilePath;

	private String shareUrl;

	public String getUserProfilePath() {
		return userProfilePath;
	}

	public void setUserProfilePath(String userProfilePath) {
		this.userProfilePath = userProfilePath;
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}
	
	private String serverURL;
	

	public String getServerURL() {
		return serverURL;
	}



	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}
	
	private static ApnsService service;
	/**
	 * Created By Bhagya On October 19th,2015
	 * 
	 * @param email
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for to send the password reset email to user
	 * @throws MailNotSentException
	 */
	public Integer sendPasswordResetEmail(String email)
			throws UserNotFoundException, UserNotSavedOrUpdatedException,
			MailNotSentException {
		log.info("inside sendPasswordResetEmail()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserByEmail(email);
		String passwordToken = this.generatePasswordToken();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		Date validity = cal.getTime();
		kikspotUser.setPasswordTokenExpiryDate(validity);
		kikspotUser.setPasswordToken(passwordToken);
		this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		KikspotUserDto kikspotUserDto = KikspotUserDto
				.populateKikspotUserDto(kikspotUser);
		this.emailSender.sendForgotPasswordMail(kikspotUserDto);
		return 1;
	}

	/**
	 * Created By Bhagya On october 19th,2015 This Method return random UUID
	 * java.util.UUID class represents an immutable universally unique
	 * identifier (UUID)
	 * 
	 */
	private String generatePasswordToken() {
		String uuid = UUID.randomUUID().toString();
		String token = uuid.toString().replaceAll("-", "").toUpperCase();
		return token;
	}

	/**
	 * Created By Bhagya On october 19th,2015
	 * 
	 * @param token
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method For Getting the UserId By Password Reset TOken
	 */
	public Integer getUserIdByPasswordResetToken(String token)
			throws UserNotFoundException {
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserByPasswordResetToken(token);
		return kikspotUser.getUserId();
	}

	/**
	 * Created By Bhagya On october 20th, 2015
	 * 
	 * @param userId
	 * @param password
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for hadling the process of password reset
	 */
	public Integer handlePasswordReset(Integer userId, String password)
			throws UserNotFoundException, UserNotSavedOrUpdatedException {
		log.info("inside handlePasswordReset()");
		Integer result = 0;
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		Date passwordTokenExpiryDate = kikspotUser.getPasswordTokenExpiryDate();
		if (passwordTokenExpiryDate.after(new Date())) {
			String encryptedPassword = bCryptEncoder.encode(password);
			kikspotUser.setPassword(encryptedPassword);
			kikspotUser.setPasswordToken(null);
			kikspotUser.setPasswordTokenExpiryDate(null);
			result = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		}
		return result;
	}

	/**
	 * Created By Bhagya On October 26th,2015
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for handling the facebook login users check the
	 *             kikspot user already exists or not by emaillId If user exists
	 *             means returns the userId If user Not exists means , its
	 *             create a new user and return the created userId
	 * 
	 *             Modified by bhagya on December 10th,2015 Setting the defaullt
	 *             value for isActive
	 */
	public Integer handleFacebookLoginUsers(KikspotUserDto kikspotUserDto)
			throws UserNotSavedOrUpdatedException {
		log.info("inside handleFacebookLoginUsers()");
		try {
			KikspotUser kikspotUser = this.userDao
					.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			return kikspotUser.getUserId();
		} catch (UserNotFoundException e) {
			KikspotUser kikspotUser = new KikspotUser();
			kikspotUser.setEmailId(kikspotUserDto.getEmailId());
			kikspotUser.setFirstName(kikspotUserDto.getFirstName());
			kikspotUser.setLastName(kikspotUserDto.getLastName());
			kikspotUser.setUserRole(UserRole.ROLE_USER);
			kikspotUser.setIsActive(true); // by default true
			kikspotUser.setLastLoginDate(new Date());
			Integer userId = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			try {
				this.credService.addCredPostUserSignup(kikspotUser);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return userId;
		}
	}

	/**
	 * Created By Bhagya On October 27th,2015
	 * 
	 * @param emailId
	 * @return
	 * 
	 *         Method For getting the kikspot user by email Id
	 * 
	 *         Modified By Bhagya On December 10th,2015 Handling the Reactive
	 *         Users
	 */
	public KikspotUserDto getUserByEmailId(String emailId) {
		log.info("inside getUserByEmailId();");
		try {
			
			KikspotUser user = this.userDao.getKikspotUserforAuthentication(emailId);
			if (user != null) {
				if (null != user.getReactivationDate()
						&& user.getReactivationDate().before(new Date())) {
					this.updateUserStatus(user);
				}
				KikspotUserDto userDto = KikspotUserDto
						.populateKikspotUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service " + e.toString());
			return null;
		}
	}

	/**
	 * 
	 * Created by Jeevan on November 23, 2015
	 * 
	 * Method to initUserSignup.
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 *             Modified by bhagya on December 10th,2015 Setting the defaullt
	 *             value for isActive
	 */
	@SuppressWarnings("unused")
	public KikspotUserDto initUserSignUp(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside initUserSignUp");
		KikspotUser kikspotUser;
		if (null != kikspotUserDto.getUserId()&& kikspotUserDto.getUserId() > 0) {
			kikspotUser = this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
			kikspotUser.setLastLoginDate(new Date());
		} else {
			kikspotUser = new KikspotUser();
			kikspotUser.setIsActive(true); // by default true
			kikspotUser.setIsMasterGeoLocation(false);
		}
		//Comment By Bhagya On may 12th, Client don't need the emailId Resrtiction
		/*try {
			KikspotUser user = this.userDao
					.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			throw new EmailAlreadyExistsException();
		} catch (UserNotFoundException e) {

		}*/
		try {
			KikspotUser user = this.userDao.getKikspotUserByUsername(kikspotUserDto.getUsername());
			throw new UsernameAlreadyExistsException();
		} catch (UserNotFoundException e) {

		}
        try
        {
        	KikspotUser user = this.userDao.getKikspotUserByEmail(kikspotUserDto.getUsername());
			throw new UsernameAlreadyExistsException();
        }
        catch (UserNotFoundException e) {

		}
        try {
			KikspotUser user = this.userDao.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			throw new EmailAlreadyExistsException();
		} catch (UserNotFoundException e) {

		}
		try {
			KikspotUser user = this.userDao.getKikspotUserByUsername(kikspotUserDto.getEmailId());
			throw new EmailAlreadyExistsException();
		} catch (UserNotFoundException e) {

		}
		
		kikspotUser.setUsername(kikspotUserDto.getUsername());
		kikspotUser.setEmailId(kikspotUserDto.getEmailId());
		kikspotUser.setPassword(bCryptEncoder.encode(kikspotUserDto
				.getPassword()));
		kikspotUser.setUserRole(UserRole.ROLE_USER);
		kikspotUser.setUserType(UserType.LOGGEDIN_USER);
		kikspotUser.setAccountCreationDate(new Date());
		kikspotUser.setLastLoginDate(new Date());
		
		//commeneted referral user by bhagya,because we have implemented referral option for particular Game rule "Sharee Account creation"
		//kikspotUser = this.getReferralofLoggedinUser(kikspotUser,kikspotUserDto);
		
		// Need to have code related to creds.
		// Get
		if(kikspotUserDto.getReferralCode()!=null && kikspotUserDto.getReferralCode().length()>0){
			System.out.println(" referral code "+kikspotUserDto.getReferralCode());
			try{
				System.out.println("sending notification");
				KikspotUser referredUser=this.userDao.getUserByReferralCode(kikspotUserDto.getReferralCode());
				kikspotUser.setReferredUser(referredUser);
		        Integer userSavedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		        System.out.println("updated user");
		        if(null!=kikspotUserDto.getDeviceToken()){
		        	this.saveUserDeviceTokentoDB(kikspotUser,kikspotUserDto.getDeviceToken());
		        }
		        kikspotUserDto.setUserId(userSavedResult);
		        this.credService.addCredPostUserSignup(kikspotUser);
		        try{
		        	ArrayList<GameRules> gameRules=this.gameService.getGameRuleByType("Sharee Account Creation");
		        	ArrayList<GameRules> filteredGameRules=this.gameService.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(gameRules);
		        	for(GameRules gameRule:filteredGameRules){
		        		System.out.println(" inside referral "+referredUser.getUserId() );
		        		UserGameCredsDto sharerCreds= this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),referredUser.getUserId(),null); //Sharer Gain Creds
		        		UserGameCredsDto shareeCreds= this.gameservice.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(), kikspotUserDto.getUserId(), null); //Sgaree Gain Creds
		        		try{
		        			NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.Shared.toString());
		        			String sharerMessage="Congrats! U accomplish the game rule of "+sharerCreds.getGameRule().getRule()+", and you Redeemed "+sharerCreds.getGameRule().getCreds()+" Creds .Your Cred  is Succesfully updated ";
		        			String shareeMessage="Congrats! U accomplish the game rule of "+shareeCreds.getGameRule().getRule()+", and you Redeemed "+shareeCreds.getGameRule().getCreds()+" Creds .Your Cred  is Succesfully updated ";
		        			this.notificationService.sendNotificationToUser( referredUser.getUserId(), notificationType, sharerMessage, null);
		        			this.notificationService.sendNotificationToUser( kikspotUserDto.getUserId(), notificationType, shareeMessage, null);
		        			
		        			/*UserDevice userDevice=this.userDao.getUserDeviceofUser(kikspotUser);
		        			if(null!=userDevice.getDeviceToken() && null!=userDevice.getDeviceType()&& userDevice.getDeviceType().equalsIgnoreCase("iOS")){
								System.out.println(" IOS DEVICE At service "+userDevice.getDeviceToken());
								this.getApplePushNotificationService();
								this.sendIOSUserRatedNotifications(userDevice.getDeviceToken(), "Congrats! You just earned "+shareeCreds.getGameRule().getCreds()+" creds! Go to the Notifications Screen for details!", service);
		        			}
		        			UserDevice userDevice1=this.userDao.getUserDeviceofUser(referredUser);
		        			if(null!=userDevice1.getDeviceToken() && null!=userDevice1.getDeviceType()&& userDevice1.getDeviceType().equalsIgnoreCase("iOS")){
								System.out.println(" IOS DEVICE At service "+userDevice1.getDeviceToken());
								this.getApplePushNotificationService();
								this.sendIOSUserRatedNotifications(userDevice1.getDeviceToken(), "Congrats! You just earned "+sharerCreds.getGameRule().getCreds()+" creds! Go to the Notifications Screen for details!", service);
		        			}*/
		        		}
		        		catch(UserDeviceNotFoundException e){
							
		        		}catch(Exception e){
						
		        		}
		        	}
		        }
			
		        catch(GameRuleNotFoundException e){
					 
		        }
		   /** By default turn off the NEWLOCATION Notification **/
		        NotificationType notificationType=this.notificationDao.getNotificationTypeByType("New Location");
		        Integer configResult=this.notificationService.enableorDisableUserNotification(kikspotUserDto.getUserId(),notificationType.getNotificationTypeId(),false);
			}
			catch(ReferredUsersNotFoundException e){
				throw new ReferredUsersNotFoundException();
			}
			catch(Exception e){
				
    		}
		}
		else{
			 Integer userSavedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			 System.out.println("updated user");
		        if(null!=kikspotUserDto.getDeviceToken()){
		        	this.saveUserDeviceTokentoDB(kikspotUser,kikspotUserDto.getDeviceToken());
		        }
		        kikspotUserDto.setUserId(userSavedResult);
		        this.credService.addCredPostUserSignup(kikspotUser);
		        /** By default turn off the NEWLOCATION Notification **/
		        NotificationType notificationType=this.notificationDao.getNotificationTypeByType("New Location");
		        Integer configResult=this.notificationService.enableorDisableUserNotification(kikspotUserDto.getUserId(),notificationType.getNotificationTypeId(),false);
		}
	    return kikspotUserDto;
	}

	private KikspotUser getReferralofLoggedinUser(KikspotUser kikspotUser,
			KikspotUserDto kikspotUserDto) {
		try {
			UserProbableReferral probableReferral = this.userDao
					.getUserProbableReferralByEmail(kikspotUserDto.getEmailId());
			kikspotUser.setReferredUser(probableReferral.getKikspotUser());

		} catch (UserProbableReferralNotFoundException e) {
			log.warn("NO USER PROBABLE REFERRAL FOUND AND NEED TO BE ADDED NEW REFERRAL CODE");
		}
		return kikspotUser;
	}

	
	
	/**
	 * 
	 * Created by Jeevan on November 24, 2015
	 * 
	 * Method to CompleteSignup Process.
	 * 
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 *             Will be used by Both social login and Email Sign up..
	 * 
	 * 
	 */
	public Integer completeSignUp(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside completeSignUp()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(kikspotUserDto.getUserId());
		
		String savedName=kikspotUser.getFirstName();
		
		kikspotUser.setFirstName(kikspotUserDto.getFirstName());
		kikspotUser.setLastName(kikspotUserDto.getLastName());
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		kikspotUser.setDob(kikspotUserDto.getDob());
		kikspotUser.setAddress1(kikspotUserDto.getAddress1());
		kikspotUser.setAddress2(kikspotUserDto.getAddress2());
		kikspotUser.setCity(kikspotUserDto.getCity());
		kikspotUser.setState(kikspotUserDto.getState());
		kikspotUser.setZip(kikspotUserDto.getZip());
		kikspotUser.setLastLoginDate(new Date());
		
		// For Social Login, Need to obtain username
		if (null != kikspotUserDto.getUsername() && kikspotUserDto.getUsername().trim().length() > 0) {
			try {
				KikspotUser user = this.userDao.getKikspotUserByUsername(kikspotUserDto.getUsername());
				throw new UsernameAlreadyExistsException();
			} catch (UserNotFoundException e) {
				kikspotUser.setUsername(kikspotUserDto.getUsername());
			}
			try {
				KikspotUser user = this.userDao.getKikspotUserByEmail(kikspotUserDto.getUsername());
				throw new UsernameAlreadyExistsException();
			} catch (UserNotFoundException e) {
				kikspotUser.setUsername(kikspotUserDto.getUsername());
			}
			
		}
		Integer userSavedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
	     log.info(" SAVED RESULT "+userSavedResult);
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date());
		cal.setTime(kikspotUser.getAccountCreationDate()); 
		cal.add(Calendar.MINUTE, 10);
		kikspotUserDto.setUsername(kikspotUser.getUsername());
		//Under Assumption that New Account details will be filled within 10 minutes of account creation
		if(new Date().before(cal.getTime())){
				kikspotUserDto.setEmailId(kikspotUser.getEmailId());
				this.emailSender.sendRegistrationMail(kikspotUserDto);
				try{
					//NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.SIGNUP.name());
					//this.notificationService.sendNotificationToUser(kikspotUser.getUserId(), notificationType, "Congratulations, You are Successfully Signedup for Kikspot",null);
				}
				catch(Exception e){			
				}
		}
		
	
		return userSavedResult;
	}
	
	
	

	/**
	 * 
	 * Created by Jeevan on November 24, 2015
	 * 
	 * Method to perform Social Login..
	 * 
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 *             Modified by bhagya on December 10th,2015 For Existing
	 *             User,checking the reactivation date is before the current
	 *             date and updating the user account status For new
	 *             User,Setting the defaullt value for isActive
	 */
	public KikspotUserDto performSocialLogin(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside performSocialLogin() ");

		KikspotUser kikspotUser;
		try {
			kikspotUser = this.userDao.getKikspotUserByEmail(kikspotUserDto
					.getEmailId());
			if (null != kikspotUser.getReactivationDate()
					&& kikspotUser.getReactivationDate().before(new Date())) {
				this.updateUserStatus(kikspotUser);
			}
			KikspotUserDto userDto = KikspotUserDto
					.populateKikspotUserDto(kikspotUser);
			if (userDto.getIsActive() == false) {
				throw new AccountInActiveException();
			}
			userDto.setIsFirstTimeLogin(false);
			kikspotUser.setLastLoginDate(new Date());
			this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			return userDto;
		} catch (UserNotFoundException e) {
			log.info("User Logged in for first time ");
			if (null != kikspotUserDto.getUserId()
					&& kikspotUserDto.getUserId() > 0) {
				kikspotUser = this.userDao.getKikspotUserById(kikspotUserDto
						.getUserId());
			} else {
				kikspotUser = new KikspotUser();
				kikspotUser.setIsActive(true); // by default true
				kikspotUser.setIsMasterGeoLocation(false);
			}
			kikspotUser.setUsername(kikspotUserDto.getFirstName()+" "+kikspotUserDto.getLastName());//As username is mandatory to be save in database for now save first name as username
			kikspotUser.setEmailId(kikspotUserDto.getEmailId());
			kikspotUser.setFirstName(kikspotUserDto.getFirstName());
			kikspotUser.setLastName(kikspotUserDto.getLastName());
			kikspotUser.setAccountCreationDate(new Date());
			kikspotUser.setUserRole(UserRole.ROLE_USER);
			kikspotUser.setUserType(UserType.SOCIAL_USER);
			kikspotUser = this.getReferralofLoggedinUser(kikspotUser,
					kikspotUserDto);

			Integer userSavedResult = this.userDao
					.saveOrUpdateKikspotUser(kikspotUser);
			// Need to have logic to send mail..
			kikspotUserDto.setUserId(userSavedResult);
			this.credService.addCredPostUserSignup(kikspotUser);
			// Set the new location notification as OFF by default
			NotificationType notificationType=this.notificationDao.getNotificationTypeByType("New Location");
			Integer configResult=this.notificationService.enableorDisableUserNotification(kikspotUserDto.getUserId(),notificationType.getNotificationTypeId(),false);
			this.emailSender.sendRegistrationMail(kikspotUserDto);
			// Added on DEC 11, 2015... logic to saveUserDeviceToken..
			this.saveUserDeviceTokentoDB(kikspotUser,
					kikspotUserDto.getDeviceToken());
			kikspotUser.setLastLoginDate(new Date());
			this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			kikspotUserDto.setIsFirstTimeLogin(true);
			
			return kikspotUserDto;
		}
	}

	
	
	
	/**
	 * Created by Jeevan on Novemeber 24, 2015 Method to registerByPassUser..
	 * 
	 * @return
	 * @throws Exception
	 * 
	 *             Modified by bhagya on December 10th,2015 Setting the defaullt
	 *             value for isActive
	 */
	public Integer registerByPassUser(String deviceToken) throws Exception {
		log.info("inside registerByPassUser() ");
		KikspotUser kikspotUser = new KikspotUser();
		kikspotUser.setAccountCreationDate(new Date());
		kikspotUser.setLastLoginDate(new Date());
		kikspotUser.setUserRole(UserRole.ROLE_USER);
		kikspotUser.setUserType(UserType.BYPASS_USER);
		kikspotUser.setUsername("GUEST - "+deviceToken);
		kikspotUser.setIsActive(true); // by default true
		kikspotUser.setIsMasterGeoLocation(false);
		Integer userSavedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		this.saveUserDeviceTokentoDB(kikspotUser, deviceToken);
		  NotificationType notificationType=this.notificationDao.getNotificationTypeByType("New Location");
		  Integer configResult=this.notificationService.enableorDisableUserNotification(userSavedResult,notificationType.getNotificationTypeId(),false);
		return userSavedResult;
	}

	/**
	 * Created by Jeevan on
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 *             Modified By Bhagya On December 10th,2015 Checking the Account
	 *             Active Status and if Account is InActive means throw
	 *             AccountInActiveException
	 */

	public Map<String,Object> authenticateMobileUser(KikspotUserDto kikspotUserDto)
			throws Exception {
		log.info("inside authenticateMobileUser() ");
		KikspotUser kikspotUser = this.userDao.getKikspotUserforAuthenticationMobile(kikspotUserDto.getUsername());
		System.out.println("user name: "+kikspotUserDto.getUsername());
		
		if (null != kikspotUser.getReactivationDate()
				&& kikspotUser.getReactivationDate().before(new Date())) {
			this.updateUserStatus(kikspotUser);
		}
		if (kikspotUser.getIsActive() == false) {
			throw new AccountInActiveException();
		} 
		else if (bCryptEncoder.matches(kikspotUserDto.getPassword(),
				kikspotUser.getPassword())) {
			// Added on DECEMBER 11, 2015... code to update UserDeviceToken on
			// each login..
			this.saveUserDeviceTokentoDB(kikspotUser,kikspotUserDto.getDeviceToken());
			kikspotUser.setLastLoginDate(new Date());
			kikspotUser.setAppVersion(kikspotUserDto.getAppVersion());
			this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			Map<String,Object> usermap=new HashMap<String, Object>();
			usermap.put("userId", kikspotUser.getUserId());
			usermap.put("email", kikspotUser.getEmailId());
			usermap.put("appVersion", kikspotUser.getAppVersion());
			return usermap;
		} 
		else {
			throw new PasswordNotMatchException();
		}
		
	}

	
	
	
	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 * 
	 * Method to handle saving or updating User Device TOken..
	 * 
	 * 
	 * @param kikspotUser
	 * @param deviceToken
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	private Integer saveUserDeviceTokentoDB(KikspotUser kikspotUser,
			String deviceToken) throws Exception {
		log.info("inside saveUserDeviceTokentoDB()");
		UserDevice userDevice;
		try {
			userDevice = this.userDao.getUserDeviceofUser(kikspotUser);
		} catch (UserDeviceNotFoundException e) {
			userDevice = new UserDevice();
			userDevice.setKikspotUser(kikspotUser);
		}
		userDevice.setDeviceType("iOS");
		userDevice.setDeviceToken(deviceToken);
		userDevice.setUpdatedDate(new Date());
		System.out.println("user device is updating");
		Integer deviceSavedResult = this.userDao
				.saveorUpdateUserDevice(userDevice);
		System.out.println("user device updated");
		return deviceSavedResult;
	}

	/**
	 * 
	 * Created by Jeevan on DECEMBER 11, 2015 Method to updateUserLocation...
	 * 
	 * 
	 * @param userLocationDto
	 * @throws Exception
	 */
	public void updateUserLocation(UserLocationDto userLocationDto)
			throws Exception {
		log.info("inside updateUserLocation() ");
		UserLocations userLocation;
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(userLocationDto.getUserId());
		try {
			userLocation = this.userDao.getUserLocationsOfUser(kikspotUser);
		} 
		catch (UserLocationNotFoundException e) {
			userLocation = new UserLocations();
			userLocation.setKikspotUser(kikspotUser);
		}
		userLocation.setLatitude(userLocationDto.getLatitude());
		userLocation.setLongitude(userLocationDto.getLongitude());
		userLocation.setUpdatedDate(new Date());
		userLocation.setCity(userLocationDto.getCity());
		userLocation.setCountry(userLocationDto.getCountry());
		this.userDao.saveorUpdateUserLocation(userLocation);
		ArrayList<UserGameCredsDto> accomplishedRulesDtos=new ArrayList<UserGameCredsDto>();
		try{
			
			
			ArrayList<UserGeoLocationRatings> userGeoLocationRatings=new ArrayList<UserGeoLocationRatings>();
			 ArrayList<GameRules> gameRules=new ArrayList<GameRules>();
			 ArrayList<GameRules> secondGeoLocatedRules=new ArrayList<GameRules>();
			 ArrayList<GameRules> thirdGeoLocatedRules=new ArrayList<GameRules>();
			
			try{
				gameRules=this.gameService.getGameRuleByType("Geolocated");
		
				 System.out.println(" GEOLOCATED GAME RULES SIZE "+gameRules.size());
			}
			catch(GameRuleNotFoundException e){
				
			}
			try{
				secondGeoLocatedRules=this.gameService.getGameRuleByType("Second Geolocated");
				System.out.println(" SECOND GEOLOCATED GAME RULES SIZE "+secondGeoLocatedRules.size());
			}
			catch(GameRuleNotFoundException e){
				
			}
			try{
				thirdGeoLocatedRules=this.gameService.getGameRuleByType("Third Geolocated");
				System.out.println(" THIRD GEOLOCATED GAME RULES SIZE "+secondGeoLocatedRules.size());
			}
			catch(GameRuleNotFoundException e){
				
			}
			
			if(gameRules.size()>0){
				try{
					
					 userGeoLocationRatings=this.recommendationDao.getUserGeoLocationRatingsByRatedDate(kikspotUser);
					
					}
					catch(UserLocationRatingsNotFoundException e){
						for(GameRules gameRule:gameRules){
							if(null!=gameRule.getLocationId()){
								RecommendationLocationDto locationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRule.getLocationId(),null,null,null);
								if(gameRule.getUserId()!=null ){
									if(gameRule.getUserId()==userLocationDto.getUserId()){
										try{
											Double distance=this.recommendationService.distance(locationDto.getLatitude(),locationDto.getLongitude(),userLocationDto.getLatitude(),userLocationDto.getLongitude());
												if(distance<0.3){
												System.out.println(" inside fi all condition satisfies with user geolocated");
													UserGameCredsDto accomplishRuleDto=this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),userLocationDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishRuleDto);
												}
										
											}
									
											catch(GameRuleExpiredException e1){
											}
											catch(GameRuleExceedsRuleLimitException e1){
											}
										}
									}
									else{
										try{
											
											Double distance=this.recommendationService.distance(locationDto.getLatitude(),locationDto.getLongitude(),userLocationDto.getLatitude(),userLocationDto.getLongitude());
											if(distance<0.3){
												System.out.println(" inside satisfies Geolocated condition without user");
												UserGameCredsDto accomplishRuleDto=this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),userLocationDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishRuleDto);
											}
											
										}
										
										catch(GameRuleExpiredException e1){
										}
										catch(GameRuleExceedsRuleLimitException e1){
										}
									
								} // ELSE CONDITION
							}
						
							
						   }// FOR LOOP
						} // CATCH CONDITION
					
				
		    }
			if(secondGeoLocatedRules.size()>0){
				SimpleDateFormat dateFormat=new SimpleDateFormat("HH:mm");
				SimpleDateFormat userDateFormat=new SimpleDateFormat("HH:mm");
				String userDateString=userDateFormat.format(new Date());
				Date userDate=dateFormat.parse(userDateString);
				
				for(GameRules gameRule:secondGeoLocatedRules){
					if(null!=gameRule.getLocationId()){
						RecommendationLocationDto locationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRule.getLocationId(),null,null,null);
						if(gameRule.getStartTime()==null){
							SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
							GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(gameRule);
							ruleDto.setStartTime(timeformat.parse("05:00 PM"));
							this.gameservice.saveOrUpdateGameRule(ruleDto);
						}
						if(gameRule.getEndTime()==null){
							SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
							GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(gameRule);
							ruleDto.setEndTime(timeformat.parse("09:00 PM"));
							this.gameservice.saveOrUpdateGameRule(ruleDto);
						}
						
						String startTime=userDateFormat.format(gameRule.getStartTime());
						String endTime=userDateFormat.format(gameRule.getEndTime());
						Date starttime=dateFormat.parse(startTime);
						Date endtime=dateFormat.parse(endTime);
						ArrayList<UserGeoLocationRatings> geoLocatedNightRatings=new ArrayList<UserGeoLocationRatings>();
					
						 ArrayList<Double> uniqueFilteredGeoNightRatings=new ArrayList<Double>();
						try{
							 geoLocatedNightRatings=this.recommendationDao.getUserGeoLocationRatingsByAndStartTimeAndEndTime(kikspotUser, gameRule.getStartTime(), gameRule.getEndTime());
							 Set<Double> uniqueGeoLocations=new HashSet<Double>();
								 for(UserGeoLocationRatings  geolocation:geoLocatedNightRatings){
									 uniqueGeoLocations.add(geolocation.getLatitude());
								 }
								 for(Double georate:uniqueGeoLocations){
									 uniqueFilteredGeoNightRatings.add(georate);
								 }
								
							 
							}
							catch(UserLocationRatingsNotFoundException e){
								
							}
						System.out.println(" SIZE OF UNIQUE FILTered NIGHT RATINGS SIZE "+uniqueFilteredGeoNightRatings.size());
					
						if( uniqueFilteredGeoNightRatings.size()==1){
											
							if(! uniqueFilteredGeoNightRatings.get(0).equals(userLocationDto.getLatitude())){
								
								if(gameRule.getUserId()!=null){
									if(gameRule.getUserId()==userLocationDto.getUserId()){
										try{
											if(userDate.before(endtime) && userDate.after(starttime)){
												Double distance=this.recommendationService.distance(locationDto.getLatitude(),locationDto.getLongitude(),userLocationDto.getLatitude(),userLocationDto.getLongitude());
												if(distance<0.3){
													System.out.println(" INSIDE IF SATISFIES SECOND GEO LOCATED WITH USER CONDITION");
													UserGameCredsDto accomplishRuleDto=this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),userLocationDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishRuleDto);
												}
											}
										}
										catch(GameRuleExpiredException e){
										}
										catch(GameRuleExceedsRuleLimitException e){
										}
									}
								}
								else{
									try{
										
										
										if(userDate.before(endtime) && userDate.after(starttime)){
											Double distance=this.recommendationService.distance(locationDto.getLatitude(),locationDto.getLongitude(),userLocationDto.getLatitude(),userLocationDto.getLongitude());
											if(distance<0.3){
												System.out.println(" inside if SECOND GEO lOACTED WITHOUT USER");
												UserGameCredsDto accomplishRuleDto=this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),userLocationDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishRuleDto);
											}
										}
									}
									catch(GameRuleExpiredException e){
									}
									catch(GameRuleExceedsRuleLimitException e){
									}
									
								}
							}
						}
				} //IF LOCATION ID
			}//FOR LOOP
			
			}
			if(thirdGeoLocatedRules.size()>0){
				
				SimpleDateFormat dateFormat=new SimpleDateFormat("HH:mm");
				SimpleDateFormat userDateFormat=new SimpleDateFormat("HH:mm");
				String userDateString=userDateFormat.format(new Date());
				Date userDate=dateFormat.parse(userDateString);
				
				for(GameRules gameRule:thirdGeoLocatedRules){
					if(null!=gameRule.getLocationId()){
						RecommendationLocationDto locationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRule.getLocationId(),null,null,null);
						if(gameRule.getStartTime()==null){
							SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
							GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(gameRule);
							ruleDto.setStartTime(timeformat.parse("05:00 PM"));
							this.gameservice.saveOrUpdateGameRule(ruleDto);
						}
						if(gameRule.getEndTime()==null){
							SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
							GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(gameRule);
							ruleDto.setEndTime(timeformat.parse("09:00 PM"));
							this.gameservice.saveOrUpdateGameRule(ruleDto);
						}
						
						String startTime=userDateFormat.format(gameRule.getStartTime());
						String endTime=userDateFormat.format(gameRule.getEndTime());
						Date starttime=dateFormat.parse(startTime);
						Date endtime=dateFormat.parse(endTime);
						ArrayList<UserGeoLocationRatings> geoLocatedNightRatings=new ArrayList<UserGeoLocationRatings>();
						
						 ArrayList<Double> uniqueFilteredGeoNightRatings=new ArrayList<Double>();
						try{
						 geoLocatedNightRatings=this.recommendationDao.getUserGeoLocationRatingsByAndStartTimeAndEndTime(kikspotUser, gameRule.getStartTime(), gameRule.getEndTime());
						 
						 Set<Double> uniqueGeoLocations=new HashSet<Double>();
						 for(UserGeoLocationRatings  geolocation:geoLocatedNightRatings){
							 uniqueGeoLocations.add(geolocation.getLatitude());
						 }
						
						 for(Double georate:uniqueGeoLocations){
							 uniqueFilteredGeoNightRatings.add(georate);
						 }
						
						
						}
						catch(UserLocationRatingsNotFoundException e){
							
						}
					
						if( uniqueFilteredGeoNightRatings.size()==2){
						
							if(!uniqueFilteredGeoNightRatings.get(0).equals(userLocationDto.getLatitude())&& !uniqueFilteredGeoNightRatings.get(1).equals(userLocationDto.getLatitude())){
							
								if(gameRule.getUserId()!=null){
									if(gameRule.getUserId()==userLocationDto.getUserId()){
										try{
											if(userDate.before(endtime) && userDate.after(starttime)){
												Double distance=this.recommendationService.distance(locationDto.getLatitude(),locationDto.getLongitude(),userLocationDto.getLatitude(),userLocationDto.getLongitude());
												if(distance<0.3){
													System.out.println(" INSIDE IF SATISFIES THIRD GEO LOCATED CONDITION with user condition");
													UserGameCredsDto accomplishRuleDto=this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),userLocationDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishRuleDto);
												}
											}
										}
										catch(GameRuleExpiredException e){
										}
										catch(GameRuleExceedsRuleLimitException e){
										}
									}
								}
								else{
									try{
										if(userDate.before(endtime) && userDate.after(starttime)){
											Double distance=this.recommendationService.distance(locationDto.getLatitude(),locationDto.getLongitude(),userLocationDto.getLatitude(),userLocationDto.getLongitude());
											if(distance<0.3){
												System.out.println(" INSIDE IF THIRD GEO LOCATED CONDITIONS WITHOUT USER");
												UserGameCredsDto accomplishRuleDto=this.gameService.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(),userLocationDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishRuleDto);
											}
										}
									}
									catch(GameRuleExpiredException e){
									}
									catch(GameRuleExceedsRuleLimitException e){
									}
									
								}
							}
						}
					} //IF CONDITION OF LOCATION ID
			}// FOR LOOP
		}
		}
		catch(GameRuleNotFoundException e){
		}
		
		UserGeoLocationRatings userGeoLocationRating=new UserGeoLocationRatings();
		userGeoLocationRating.setLatitude(userLocationDto.getLatitude());
		userGeoLocationRating.setLongitude(userLocationDto.getLongitude());
		userGeoLocationRating.setGeoRatingDate(new Date());
		userGeoLocationRating.setKikspotUser(kikspotUser);
		this.recommendationDao.saveUserGeoLocationRatings(userGeoLocationRating);
		
		try{
		 if(accomplishedRulesDtos.size()>0){
			 NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.RateSubmitted.toString());
			 StringBuilder notificationMessage=new StringBuilder();
			 //notificationMessage.append("Congrats! U accomplish the game rules of ");
			 notificationMessage.append("Let it rain! You have just redeeming the following rewards: ");
			 for(UserGameCredsDto accomplishedRule:accomplishedRulesDtos){
				 notificationMessage.append(accomplishedRule.getGameRule().getRule() +" : "+accomplishedRule.getGameRule().getCreds() +" Creds,");
			 }
			 if(notificationMessage.toString().endsWith(",")){
				 notificationMessage.deleteCharAt(notificationMessage.lastIndexOf(","));
			 }
			 this.notificationService.sendNotificationToUser(kikspotUser.getUserId(), notificationType, notificationMessage.toString(), null);
		 }
		}
		catch(Exception e){
			
		}
	}

	/**
	 * Created by Jeevan on DECEMBER 14, 2015
	 * 
	 * Method to saveUserPReferenceandProfileImage ofUSer..
	 * 
	 * 
	 * @param userId
	 * @param preferredVenueType
	 * @param profile
	 * @throws Exception
	 * 
	 * 
	 *             Steps: 1. Get KikspotYUserBy Id 2. Set PreferredVenue Type.
	 *             2. From Multipart, obtain file type. Save file to Server on
	 *             TIMESTAMP.type 4. Set profile pic path to database.. 5. If
	 *             needed create context path..
	 * 
	 * 
	 * 
	 */
	public Integer saveUserPreferenceandProfileImage(Integer userId,
			String preferredVenueType, MultipartFile profile) throws Exception {
		log.info("inside saveUserPreferenceandProfileImage() ");
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		kikspotUser.setPreferredVenueType(preferredVenueType);
		System.out.println(profile.getContentType() + " " + profile.getName()
				+ " " + profile.getSize());
		String fileType = profile.getOriginalFilename().substring(
				profile.getOriginalFilename().lastIndexOf(".") + 1);
		File imageFile = new File(userProfilePath + userId + "/"
				+ new Date().getTime() + "." + fileType);
		System.out.println("FILE NAME AND PATH " + imageFile.getAbsolutePath());
		if (!imageFile.exists()) {
			imageFile.mkdirs();
		}
		profile.transferTo(imageFile);
		kikspotUser.setProfilePicture(imageFile.getName());
		Integer userUpdatedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		return userUpdatedResult;
	}

	/**
	 * Created by Jeevan on DECEMBER 15, 2015 Method to init Forgot Pasword
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * 
	 *             Steps : 1. GET Kikspot User by ID 2. Generate unique code 3.
	 *             Save it to user 4. Send a mail containing code.. 5. Send
	 *             response to mobile app.
	 * 
	 */
	public Integer initForgotPasswordforMobile(String email) throws Exception {
		log.info("inside initForgotPasswordforMobile() ");
		KikspotUser kikspotUser = this.userDao.getKikspotUserforAuthenticationMobile(email);
		kikspotUser.setPasswordToken(RandomStringUtils.randomNumeric(4));
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		kikspotUser.setPasswordTokenExpiryDate(cal.getTime());
		Integer updatedResult = this.userDao
				.saveOrUpdateKikspotUser(kikspotUser);
		// sending forgot password mail.
		this.emailSender.sendForgotPasswordMailforMobileUser(kikspotUser);
		return updatedResult;
	}

	/**
	 * Created by Jeevan on DECEMBER 15, 2015
	 * 
	 * Method to rest
	 * 
	 * @param email
	 * @param token
	 * @throws Exception
	 */
	public void resetMobileUserPassword(String email, String token,
			String newPassword) throws Exception {
		log.info("inside resetMobileUserPassword()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserByEmail(email);
		if (token.equals(kikspotUser.getPasswordToken())) {
			kikspotUser.setPasswordToken("");
			kikspotUser.setPasswordTokenExpiryDate(null);
			kikspotUser.setPassword(this.bCryptEncoder.encode(newPassword));
			this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		} else {
			throw new PasswordNotMatchException();
		}
	}

	/**
	 * Created By Bhagya On December 10th,2015
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method For Updating the User Status ,when the reactivation
	 *             date is before the current date
	 */

	public Integer updateUserStatus(KikspotUser kikspotUser)
			throws UserNotSavedOrUpdatedException {
		log.info("inside UserServiceImpl -> updateUserStatus()");
		kikspotUser.setIsActive(true);
		kikspotUser.setReactivationDate(null);
		this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return 0;
	}

	/**
	 * Created By Bhagya On december 03rd, 2015
	 * 
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method For Getting the List Of ALl Users Modified By Bhagya
	 *             On December 10th,2015 If Reactivation Date is before the
	 *             todays date means, we are activating that user
	 *             
	 *             
	 */
	public ArrayList<KikspotUserDto> getAllUsers(Integer pageNo,Integer pageSize, String sortBy, String searchBy, Boolean ascending)
			throws UserNotFoundException {
		log.info("inside AdminServiceImpl -> getAllUsers()");
		ArrayList<KikspotUser> kikspotUsers = this.userDao.getAllUsers(pageNo,pageSize, sortBy, searchBy, ascending);
		Map<Integer, Integer> userCredMap=this.getCredsMapofUser(kikspotUsers);
		ArrayList<KikspotUserDto> kikspotUserDtos = new ArrayList<KikspotUserDto>();
		for (KikspotUser kikspotUser : kikspotUsers) {
			if (null != kikspotUser.getReactivationDate()
					&& kikspotUser.getReactivationDate().before(new Date())) {
				try {
					this.updateUserStatus(kikspotUser);
				} catch (UserNotSavedOrUpdatedException e) {
					e.printStackTrace();
				}
			}
			KikspotUserDto kikspotUserDto = KikspotUserDto.populateKikspotUserDto(kikspotUser);
			if(null!=userCredMap.get(kikspotUser.getUserId())){
				kikspotUserDto.setCreds(userCredMap.get(kikspotUser.getUserId()));
			}
			else{
				kikspotUserDto.setCreds(0);
			}
			
			kikspotUserDtos.add(kikspotUserDto);
		}

		return kikspotUserDtos;
	}

	
	
	
	private Map<Integer, Integer> getCredsMapofUser(ArrayList<KikspotUser> kikspotUsers){
		log.info("inside getCredsMapofUser() ");
		Map<Integer, Integer> userCredMap=new HashMap<Integer, Integer>();
		try{
			ArrayList<UserGameCreds> userGameCreds=this.credDao.getGameCredsofUsers(kikspotUsers);
			for(UserGameCreds userGameCred:userGameCreds){
				userCredMap.put(userGameCred.getKikspotUser().getUserId(), userGameCred.getCreds());
			}
		}
		catch(Exception e){
	
		}
		return userCredMap;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Created By Bhagya On December 10th,2015
	 * 
	 * @param isActive
	 * @param reactivationDate
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 */
	public Integer savingChangedUserStatus(Integer userId, Boolean isActive,
			Date reactivationDate) throws UserNotFoundException,
			UserNotSavedOrUpdatedException {
		log.info("inside AdminServiceImpl -> savingChangedUserStatus()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		kikspotUser.setIsActive(isActive);
		kikspotUser.setReactivationDate(reactivationDate);
		Integer savedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return savedResult;
	}

	
	
	/**
	 * Created By Bhagya On December 16th,2015
	 * 
	 * @param changePasswordDto
	 * @return
	 * @throws UserNotFoundException
	 * @throws PasswordNotMatchException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for perform the change Password
	 */
	public Integer performChangePassword(ChangePasswordDto changePasswordDto)
			throws UserNotFoundException, PasswordNotMatchException,
			UserNotSavedOrUpdatedException {
		log.info("inside processChangePassword()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(changePasswordDto.getUserId());
		String newPassword = bCryptEncoder.encode(changePasswordDto
				.getNewPassword());
		if (!bCryptEncoder.matches(changePasswordDto.getOldPassword(),
				kikspotUser.getPassword())) {
			throw new PasswordNotMatchException();
		}
		kikspotUser.setPassword(newPassword);
		Integer savedResult = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return savedResult;
	}

	/**
	 * Created By Bhagya On December 17th,2015
	 * 
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method for getting the Kikspot User By Username
	 */
	public KikspotUserDto getKikspotUserByUsername(String username)
			throws UserNotFoundException {
		log.info("inside getKikspotUserByUsername()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserforAuthentication(username);
		KikspotUserDto kikspotUserDto = KikspotUserDto
				.populateKikspotUserDto(kikspotUser);
		return kikspotUserDto;
	}

	/**
	 * Created By Firdous on 5th February,2016 Method for Updating user Profile
	 * 
	 */

	
	public Integer updateUserProfile(KikspotUserDto kikspotUserDto) throws Exception {
		log.info("inside getEdit()");
		
		KikspotUser user = this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
		/**Check if the another user with same username exists? If yes then show the message as username already exist*/
		 try
	        {
			 
	        	KikspotUser kikspotuser = this.userDao.getKikspotUserByUsernameNotEqualToPassedUserId(kikspotUserDto.getUsername(),user.getUserId());
				throw new UsernameAlreadyExistsException();
	        }
	        catch (UserNotFoundException e) {
			}
		 try
	        {
	        	KikspotUser kikspotuser = this.userDao.getKikspotUserByEmailNotEqualToPassedUserId(kikspotUserDto.getUsername(),user.getUserId());
				throw new UsernameAlreadyExistsException();
				
	        }
	        catch (UserNotFoundException e) {
			}
	        try {
				KikspotUser kikspotuser = this.userDao.getKikspotUserByEmailNotEqualToPassedUserId(kikspotUserDto.getEmailId(),user.getUserId());
				throw new EmailAlreadyExistsException();
			} catch (UserNotFoundException e) {
				
			}
			try {
				KikspotUser kikspotuser = this.userDao.getKikspotUserByUsernameNotEqualToPassedUserId(kikspotUserDto.getEmailId(),user.getUserId());
				throw new EmailAlreadyExistsException();
			} catch (UserNotFoundException e) {
			}
			user.setUsername(kikspotUserDto.getUsername());
			user.setFirstName(kikspotUserDto.getFirstName());
			user.setLastName(kikspotUserDto.getLastName());
			user.setAddress1(kikspotUserDto.getAddress1());
			user.setAddress2(kikspotUserDto.getAddress2());
			user.setCity(kikspotUserDto.getCity());
			user.setState(kikspotUserDto.getState());
			user.setZip(kikspotUserDto.getZip());
			user.setEmailId(kikspotUserDto.getEmailId());
			user.setDob(kikspotUserDto.getDob());
			Integer result = this.userDao.saveOrUpdateKikspotUser(user);
			return result;
	    
		
	}

	/**
	 * Created By Bhagya On December 24th,2015
	 * 
	 * @param kikspotUserId
	 * @return
	 * @throws UserNotFoundException
	 * @throws RefferdUsersNotFoundException
	 * 
	 *             Method for getting the List of Reffered Users Of a User
	 */

	public ArrayList<KikspotUserDto> getReferredUsersOfUser(
			Integer kikspotUserId) throws UserNotFoundException,
			ReferredUsersNotFoundException {
		log.info("inside getReferredUsersOfUser()");
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(kikspotUserId);
		ArrayList<KikspotUser> referredUsers = this.userDao
				.getReferredUsersByKikspotUser(kikspotUser);
		ArrayList<KikspotUserDto> referredUserDtos = new ArrayList<KikspotUserDto>();
		for (KikspotUser referredUser : referredUsers) {
			KikspotUserDto refferedUserDto = KikspotUserDto
					.populateKikspotUserDto(referredUser);
			referredUserDtos.add(refferedUserDto);
		}
		return referredUserDtos;
	}

	/**
	 * Created By Bhagya On december 28th,2015
	 * 
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method for getting the Kikspot User Details By kikspotUserId
	 */

	public KikspotUserDto getKikspotUserDetailsByUserId(Integer userId)
			throws UserNotFoundException {
		log.info("inside getKikspotUserDetailsByUserId()");
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		KikspotUserDto kikspotUserDto = KikspotUserDto
				.populateKikspotUserDto(kikspotUser);
		if(null!=kikspotUserDto.getProfilePicture() && kikspotUserDto.getProfilePicture().length()>0){
			String profilepicURL=serverURL+"/users/"+userId+"/"+kikspotUserDto.getProfilePicture();
			kikspotUserDto.setProfilePicture(profilepicURL);
		}
		else{
			kikspotUserDto.setProfilePicture("");
		}
		return kikspotUserDto;
	}

	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * Method to sendShareUrlofUser..
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String sendShareUrlofUser(Integer userId) throws Exception {
		log.info("inside sendShareUrlofUser() ");
		String url = shareUrl + "?referredUser=" + userId;
		return url;
	}

	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * MEthod to saveProbableReferredUser..
	 * 
	 * @param userId
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public Integer saveProbableReferredUser(Integer userId, String email)
			throws Exception {
		log.info("inside saveProbableReferredUser()");
		UserProbableReferral userProbableReferral = new UserProbableReferral();
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		userProbableReferral.setEmail(email);
		userProbableReferral.setKikspotUser(kikspotUser);
		Integer savedResult = this.userDao
				.saveorUpdateUserProbableReferral(userProbableReferral);
		return savedResult;
	}
	
	/**
	 * 
	 */
	public Integer updateUserGameCreds(Integer userId,Integer creds)throws Exception{
		log.info("inside updateUserGameCreds()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		Integer savedResult=this.credService.updateUserCreds(kikspotUser, creds);
		return savedResult;
	}
	
	
	
	/**
	 * Created By Bhagya On Feb 02nd, 2016
	 * @param userId
	 * @return
	 * @throws UserPreferencesNotFoundException
	 * @throws UserNotFoundException
	 * 
	 * Method for getting All User Preferences
	 * 
	 * Steps: Getting User By Id
	 * 		Getting List Of all User Preferences
	 * 		For Each Preference getting list of all User Preference Options
	 * 			Getting the List Of all User preference Settings By User and Userprefernce
	 * 				Adding the OptionIds to arrayList
	 * 			For Each User Preference Option we are checking that optionId is exist at ArrayList of optionIds
	 * 					If optionId is exist means, setting the isselected as true
	 * 					else setting the isSelected as false
	 * 		Setting the List of UserPrefernceOptionDtos to UserPrferenceDto
	 * 	Returning the List Of User Preferences Dto
	 */
	
	public ArrayList<UserPreferencesDto> getAllUserPreferences(Integer userId) throws UserPreferencesNotFoundException, UserNotFoundException{
		log.info("inside getAllUserPreferences()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		ArrayList<UserPreferences> userPreferences=userDao.getListOfAllUserPreferences();
		ArrayList<UserPreferencesDto> userPreferencesDtos=new ArrayList<UserPreferencesDto>();
		for(UserPreferences userPreference:userPreferences){
			UserPreferencesDto userPreferencesDto=UserPreferencesDto.populateUserPreferencesDto(userPreference);
			try{
				ArrayList<UserPreferenceOptions> userPreferenceOptions=this.userDao.getUserPreferenceOptionsByUserPreference(userPreference);
				ArrayList<UserPreferenceOptionsDto> userPreferenceOptionsDtos=new ArrayList<UserPreferenceOptionsDto>();
				ArrayList<UserPreferenceOptions> optionIds=new ArrayList<UserPreferenceOptions>();
				try{
					optionIds=this.userDao.getUserPreferenceOptionsByUserAndUserPreference(kikspotUser, userPreference);							
				}
				catch(UserPreferenceSettingNotFoundException e){
					
				}
				for(UserPreferenceOptions userPreferenceOption:userPreferenceOptions){
					UserPreferenceOptionsDto userPreferenceOptionsDto=UserPreferenceOptionsDto.populateUserPreferenceOptionsDto(userPreferenceOption);
					if(optionIds.contains(userPreferenceOption)){
						userPreferenceOptionsDto.setIsSelected(true);
					}
					else{
						userPreferenceOptionsDto.setIsSelected(false);
					}
					userPreferenceOptionsDtos.add(userPreferenceOptionsDto);
				}
				userPreferencesDto.setUserPreferenceOptionsDtos(userPreferenceOptionsDtos);
			}
			catch(UserPreferenceOptionsNotFoundException e){
				e.printStackTrace();
			}
			
			userPreferencesDtos.add(userPreferencesDto);
		}
		return userPreferencesDtos;
	}
	
	/**
	 * Created By Bhagya On Feb 03rd, 2016
	 * @param userId
	 * @param optionIds
	 * @return
	 * @throws Exception
	 * 
	 * Method for saving the User Preferences
	 * Steps : Getting kikspotUser By userId
	 * 			Getting list of UserPreferenceOptions By OptionIds
	 * 			Iterating the UserOperations for saving
	 * 					Initially we are getting userPreferencesettings for that user and preference (question), and then delete those user preference settings (because for update the options of preferences for user) 
	 * 					After we create the new user preference setting
	 * 					save those userPreference setting	 
	 */
	public Integer saveUserPreferences(Integer userId, ArrayList<Integer> optionIds) throws Exception{
		log.info("inside saveUserPreferences()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		ArrayList<UserPreferenceOptions> userPreferenceOptions=new ArrayList<UserPreferenceOptions>();
		try{
			userPreferenceOptions=this.userDao.getUserPreferenceOptionsByOptionIds(optionIds);
			//Deleting the Existing User Preference Settings based on User and preference  
			for(UserPreferenceOptions userPreferenceOption: userPreferenceOptions){
				try{
					ArrayList<UserPreferenceSetting> userPreferenceSettings=this.userDao.getUserPreferenceSettingsByUserAndUserPreference(kikspotUser, userPreferenceOption.getUserPreferences());
					for(UserPreferenceSetting userPreferenceSetting:userPreferenceSettings){
						this.userDao.deleteUserPreferenceSetting(userPreferenceSetting);
					}
				}
				catch(UserPreferenceSettingNotFoundException e){
					
				}
			}
			//Saving the User Preference Setting (user selected preference options we are saving)
			for(UserPreferenceOptions userPreferenceOption: userPreferenceOptions){
				UserPreferenceSetting userPreferenceSetting=new UserPreferenceSetting();
				userPreferenceSetting.setKikspotUser(kikspotUser);
				userPreferenceSetting.setUserPreferences(userPreferenceOption.getUserPreferences());
				userPreferenceSetting.setUserPreferenceOptions(userPreferenceOption);
				this.userDao.saveUserPreferenceSetting(userPreferenceSetting);
			}
		}
		catch(UserPreferenceOptionsNotFoundException e){
			System.out.println(" User Preference Options Not Found For List Of OptionIds");
		}
		
		return 1;
	}


	
	
	/**
	 * Created by Jeevan on FEBRUARY 01, 2016
	 * 
	 * Method to check IFUserIsLoggedinType...
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Boolean isLoggedInUser(Integer userId)throws Exception{
		log.info("inside isLoggedInUser");
		Boolean isLoggedInUser=true;
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		if(kikspotUser.getUserType().equals(UserType.BYPASS_USER)){
			isLoggedInUser=false;
			throw new LimitedLookUserAccessDeniedException();
		}
		return isLoggedInUser;
	}
	
	
	/**
	 * Created by Firdous on 5th February
	 * Method to update the profile of admin
	 */

	
	public Integer saveInfo(KikspotUserDto kikspotUserDto) throws Exception {
		log.info("inside getEdit()");
		KikspotUser user = this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
		user.setFirstName(kikspotUserDto.getFirstName());
		user.setLastName(kikspotUserDto.getLastName());
		user.setAddress1(kikspotUserDto.getAddress1());
		user.setAddress2(kikspotUserDto.getAddress2());
		user.setCity(kikspotUserDto.getCity());
		user.setState(kikspotUserDto.getState());
		user.setZip(kikspotUserDto.getZip());
		user.setDob(kikspotUserDto.getDob());
		Integer result = this.userDao.saveOrUpdateKikspotUser(user);
		return result;
	}

	/**
	 * Created by Firdous on 22nd March
	 * Method to update the profile of admin
	 */

	
	public Integer saveUserInfo(KikspotUserDto kikspotUserDto) throws Exception {
		log.info("inside getEdit()");
		KikspotUser user = this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
		user.setFirstName(kikspotUserDto.getFirstName());
		user.setLastName(kikspotUserDto.getLastName());
		user.setAddress1(kikspotUserDto.getAddress1());
		user.setAddress2(kikspotUserDto.getAddress2());
		user.setCity(kikspotUserDto.getCity());
		user.setState(kikspotUserDto.getState());
		user.setZip(kikspotUserDto.getZip());
		user.setDob(kikspotUserDto.getDob());
		//Added EmailId At edit profile by bhagya , because of the clients need
		/*user.setEmailId(kikspotUserDto.getEmailId());*/
		
		
        try {
			KikspotUser kikspotuser = this.userDao.getKikspotUserByEmailNotEqualToPassedUserId(kikspotUserDto.getEmailId(),user.getUserId());
			throw new EmailAlreadyExistsException();
		} catch(UserNotFoundException e) {
			
		}
		try {
			KikspotUser kikspotuser = this.userDao.getKikspotUserByUsernameNotEqualToPassedUserId(kikspotUserDto.getEmailId(),user.getUserId());
			throw new EmailAlreadyExistsException();
		} catch (UserNotFoundException e) {
			
		}
		user.setEmailId(kikspotUserDto.getEmailId());
		Integer result = this.userDao.saveOrUpdateKikspotUser(user);
		return result;
	}

	/**
	 * Created by Firdous on 17th February
	 * Method to reset the password of a user by Admin
	 */

	public Integer resetUserPassword(Integer userId, String password) throws UserNotFoundException,
			UserNotSavedOrUpdatedException {
		log.info("inside resetUserPassword()");
		Integer result;
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		String encryptedPassword = bCryptEncoder.encode(password);
		kikspotUser.setPassword(encryptedPassword);
		kikspotUser.setPasswordToken(null);
		kikspotUser.setPasswordTokenExpiryDate(null);
		result = this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return result;
	}

	/*@Override
	public Integer deleteUser(Integer id) throws Exception {
		log.info("inside deleteUser()");
		KikspotUser user=this.userDao.getKikspotUserById(id);
		ArrayList<Feedback> feedbacks=this.feedbackDao.getAllFeedbacks(null,null,null,null,null, id);
		this.feedbackDao.deleteFeedback(id);
		this.notificationDao.deleteUserNotifications(id);
		Integer result=this.userDao.deleteUser(user);
		return result;
		
	}
    */
	/**
	 * 
	 * Added by Firdous on 10th march 2016
	 * Method to get the user location by userId
	 * 
	 */
	@Override
	public String getCityOfUserByUserId(Integer userId) throws Exception {
		log.info("inside getCityOfUserByUserId()");
		KikspotUser user=this.userDao.getKikspotUserById(userId);
		UserLocations location=this.userDao.getUserLocationsOfUser(user);
        UserLocationDto locationDto=UserLocationDto.populateUserLocationDto(location);
		return locationDto.getCity();
	}
    
	/**
	 * Added by Firdous on 10th march 2016
	 * Method to get the country of a user
	 */
	@Override
	public String getUserCountryByUserId(Integer userId) throws Exception {
		log.info("inside getUserCountryByUserId()");
		KikspotUser user=this.userDao.getKikspotUserById(userId);
		UserLocations location=this.userDao.getUserLocationsOfUser(user);
        UserLocationDto locationDto=UserLocationDto.populateUserLocationDto(location);
		return locationDto.getCountry();
	}

	@Override
	public void updateUserLocation(Integer userId,double latitude, double longitude) throws Exception {
		log.info("inside updateUserLocation() ");
		UserLocations userLocation;
		KikspotUser kikspotUser = this.userDao
				.getKikspotUserById(userId);
		try {
			userLocation = this.userDao.getUserLocationsOfUser(kikspotUser);
		} catch (UserLocationNotFoundException e) {
			userLocation = new UserLocations();
			userLocation.setKikspotUser(kikspotUser);
		}
		userLocation.setLatitude(latitude);
		userLocation.setLongitude(longitude);
		userLocation.setUpdatedDate(new Date());
		
		this.userDao.saveorUpdateUserLocation(userLocation);
	}
    
	/**
	 * Added by Firdous 
	 * Method to get all the users except by pass users
	 * 
	 */
	@Override
	public ArrayList<KikspotUserDto> getAllUsersEcxeptByPassUsers() throws Exception {
		log.info("inside getAllUsersEcxeptByPassUsers()");
		ArrayList<KikspotUser> kikspotUsers = this.userDao.getAllUsersExceptByPass();
		
		ArrayList<KikspotUserDto> kikspotUserDtos = new ArrayList<KikspotUserDto>();
		for (KikspotUser kikspotUser : kikspotUsers) {
			if (null != kikspotUser.getReactivationDate() && kikspotUser.getReactivationDate().before(new Date())) {
				try {
					this.updateUserStatus(kikspotUser);
					
				} 
				catch (UserNotSavedOrUpdatedException e) {
					e.printStackTrace();
				}
			}
			KikspotUserDto kikspotUserDto = KikspotUserDto.populateKikspotUserDto(kikspotUser);
			kikspotUserDtos.add(kikspotUserDto);
		}

		return kikspotUserDtos;
	}

	@Override
	public void saveReferralCode(Integer userId,String referralCode) throws Exception {
		KikspotUser kikspotUser = this.userDao.getKikspotUserById(userId);
		kikspotUser.setReferralCode(referralCode);
		this.userDao.saveOrUpdateKikspotUser(kikspotUser);
	}
	/**
	 * Created By bhagya On May 12th, 2016
	 * @param deviceToken
	 * @param notificationMessage
	 * @param service
	 * @throws InterruptedException
	 * 
	 * Method for send The IOS User Rated Notifications
	 */

	public void sendIOSUserRatedNotifications(String deviceToken,String notificationMessage,ApnsService service) throws InterruptedException{
		log.info("INSIDE sendIOSUserRatedNotifications()");
		System.out.println(" inside the send IOS Notification");
		String payload = APNS.newPayload()
	            .alertBody(notificationMessage)
	            .sound("Glass.aiff")
	            .actionKey("Play").build();
		Thread.sleep(3000);
		service.push(deviceToken, payload);		
	
		System.out.println(" NOTIFICATION SENT TO IOS DEVICE : NOTIFICATION MESSAGE "+notificationMessage +" DEVICE TOKEN "+deviceToken);
		log.info(" NOTIFICATION SENT TO IOS DEVICE : NOTIFICATION MESSAGE "+notificationMessage +" DEVICE TOKEN "+deviceToken);
	}

	/**
	 * Created By Bhagya On May 12th, 2016
	 * Method for to get the Apple Push Notification Servcie
	 * @throws IOException
	 */

	public void getApplePushNotificationService() throws IOException{
		log.info("INSIDE  getApplePushNotificationService()");
		if(null==service){
			Properties properties=new Properties();
			InputStream pushProperties=this.getClass().getClassLoader().getResourceAsStream("push.properties");
			properties.load(pushProperties);
			String passPhrase=properties.getProperty("passphrase");
			String enviroment=properties.getProperty("environment");
			InputStream certFile=this.getClass().getClassLoader().getResourceAsStream(properties.getProperty("certfile"));
			log.info("passPhrase"+passPhrase +" environment "+enviroment);
				ApnsServiceBuilder serviceBuilder = APNS.newService()
			    		.withCert(certFile, passPhrase);
				serviceBuilder.withDelegate(new ApnsDelegate() {
				      
					
					@Override
					public void connectionClosed(DeliveryError deliveryError, int messageIdentifier) {
						// TODO Auto-generated method stub
						System.out.println(" Notification Delivery Error Code For IOS Device: Error Code "+deliveryError.code() +" Error Message "+deliveryError.toString());
						log.info(" Notification Delivery Error Code For IOS Device: Error Code "+deliveryError.code() +" Error Message  "+deliveryError.toString());
					}
	
					
	
					@Override
					public void messageSendFailed(ApnsNotification notification, Throwable e) {
						// TODO Auto-generated method stub
						System.out.println("Daily Notification Message send failed.  Error Message: " + e.toString());
						log.info(" Notification Message send failed For IOS Device "+ e.toString());
					}

					@Override
					public void cacheLengthExceeded(int arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void messageSent(ApnsNotification notification, boolean arg1) {
						// TODO Auto-generated method stub
						System.out.println("Daily Notification Message sent.  Payload: " + notification.getPayload().toString() + " token " + notification.getDeviceToken().toString());
						log.info(" Notification Message sent For IOS Device");
						System.out.println("expiryy "+notification.getExpiry() +" identifer "+notification.getIdentifier());
						
					}

					@Override
					public void notificationsResent(int arg0) {
						// TODO Auto-generated method stub
						
					}
		        });
				log.info("BEFORE FOR LOOP passPhrase"+passPhrase +" environment "+enviroment );
			    	if(enviroment.equals("sandbox")){
			    		log.info("Sandbox host selected");
			    		System.out.println("Sandbox host selected");
			    	}
			    	else {
			    		log.info("Production host selected");
			    		System.out.println("Production host selected");
			    	}
			    	
			    	 service =enviroment.equals("sandbox") ?
			    			 	 serviceBuilder.withSandboxDestination().build() : 
			    				 serviceBuilder.withProductionDestination().build();
			    		log.info("After creating the service "+service.toString());
			    			Map<String, Date> devices=service.getInactiveDevices();
			    			System.out.println("SIZE OF INACTIVE DEVICES  "+devices.size());
			    			log.info("SIZE OF INACTIVE DEVICES  "+devices.size());
			    			for(Entry<String, Date> device: devices.entrySet()){
			    				
			    			}	
			    		
	
			}
		
	}
	
	/**
	 * Created By Bhagya On May 21st, 2019
	 */
	@Override
	public String getAllUsersOfMasterGeoLocated() throws Exception {
		log.info("inside getAllUsersOfMasterGeoLocated()");
		ArrayList<KikspotUser> kikspotUsers = this.userDao.getAllUsersOfMasterGeoLocated();
		
		StringBuilder masterGeoLocatedUsers=new StringBuilder();
		for (KikspotUser kikspotUser : kikspotUsers) {
			masterGeoLocatedUsers.append(kikspotUser.getUsername());
			masterGeoLocatedUsers.append(",");		
		}
		if(masterGeoLocatedUsers.length()>0){
		masterGeoLocatedUsers.deleteCharAt(masterGeoLocatedUsers.lastIndexOf(","));
		}
		return masterGeoLocatedUsers.toString();
	}
	/**
	 * Created By Bhagya On May 21st, 2019
	 * @param latitude
	 * @param longitude
	 * @param users
	 * @return
	 * @throws Exception
	 * 
	 * Method for to save the master geo location configuration
	 */
	public Integer saveMasterGeoLocationConfiguration(Integer masterId,Double latitude,Double longitude,List<String> users) throws Exception{
		log.info("inside UserServiceImpl -> saveMasterGeoLocationConfiguration()");
		ArrayList<KikspotUser> kikspotUsers = this.userDao.getAllUsersExceptByPass();
		for(KikspotUser kikspotUser:kikspotUsers){
			kikspotUser.setIsMasterGeoLocation(false);
			this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		}
		for(String user:users){
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(Integer.parseInt(user));
			kikspotUser.setIsMasterGeoLocation(true);
			Integer savedResult=this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		}
		MasterGeoLocationConfiguration masterGeoLocationConfiguration=new MasterGeoLocationConfiguration();
		masterGeoLocationConfiguration.setLatitude(latitude);
		masterGeoLocationConfiguration.setLongitude(longitude);
		masterGeoLocationConfiguration.setMasterId(masterId);
		Integer savedResult=this.recommendationDao.saveorUpdateMasterGeoLocationConfiguration(masterGeoLocationConfiguration);
		
		
		return savedResult;
	}
}