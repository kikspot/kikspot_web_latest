package com.kikspot.frontend.user.dto;

import com.kikspot.backend.user.model.UserPreferenceOptions;
/**
 * Created By Bhagya On Feb 02nd, 2016
 * Dto Class For User Preference Options 
 */
public class UserPreferenceOptionsDto{
	
	private Integer userPreferenceOptionsId;
	private UserPreferencesDto userPreferences;
	private String option;
	private Boolean isSelected;
	
	public Integer getUserPreferenceOptionsId() {
		return userPreferenceOptionsId;
	}
	public void setUserPreferenceOptionsId(Integer userPreferenceOptionsId) {
		this.userPreferenceOptionsId = userPreferenceOptionsId;
	}
	public UserPreferencesDto getUserPreferences() {
		return userPreferences;
	}
	public void setUserPreferences(UserPreferencesDto userPreferences) {
		this.userPreferences = userPreferences;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	public Boolean getIsSelected() {
		return isSelected;
	}
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	public static UserPreferenceOptionsDto populateUserPreferenceOptionsDto(UserPreferenceOptions userPreferenceOptions){
		UserPreferenceOptionsDto userPreferenceOptionsDto=new UserPreferenceOptionsDto();
		userPreferenceOptionsDto.setUserPreferenceOptionsId(userPreferenceOptions.getUserPreferenceOptionsId());
		userPreferenceOptionsDto.setUserPreferences(UserPreferencesDto.populateUserPreferencesDto(userPreferenceOptions.getUserPreferences()));
		userPreferenceOptionsDto.setOption(userPreferenceOptions.getOption());
		return userPreferenceOptionsDto;
	}
	
}