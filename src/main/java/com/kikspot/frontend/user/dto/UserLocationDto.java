package com.kikspot.frontend.user.dto;
import java.util.Date;

import com.kikspot.backend.user.model.UserLocations;



/**
 * Created by Jeevan on DECEMBER 11, 2015
 * 
 * DTO for UserLocation..
 * 
 * @author KNS-ACCONTS
 *
 */
public class UserLocationDto {

	private Integer userLocationId;
	private KikspotUserDto kikspotUserDto;
	private Double latitude;
	private Double longitude;
	private Date updatedDate;
	private Integer userId;
	private String city;
	private String country;
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getUserLocationId() {
		return userLocationId;
	}
	public void setUserLocationId(Integer userLocationId) {
		this.userLocationId = userLocationId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
	/***
	 * 
	 * @param userLocation
	 * @return
	 */
	public static UserLocationDto populateUserLocationDto(UserLocations userLocation){
		UserLocationDto userLocationDto=new UserLocationDto();
		userLocationDto.setUserLocationId(userLocation.getUserLocationId());
		userLocationDto.setLatitude(userLocationDto.getLatitude());
		userLocationDto.setLongitude(userLocation.getLongitude());
		userLocationDto.setCity(userLocation.getCity());
		userLocationDto.setCountry(userLocation.getCountry());
		if(null!=userLocation.getKikspotUser()){
			userLocationDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userLocation.getKikspotUser()));
		}
		if(null!=userLocation.getUpdatedDate()){
			userLocationDto.setUpdatedDate(userLocation.getUpdatedDate());
		}
		return userLocationDto;
	}
	
	
}
