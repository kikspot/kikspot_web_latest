package com.kikspot.frontend.user.dto;
import java.util.Date;

/**
 * Created by bhagya on october 13th,2015
 * 
 * Data Transfer object for kikspot user entity
 */
import com.kikspot.backend.user.model.KikspotUser;

public class KikspotUserDto{
	
	private Integer userId;
	private String username;
	private String password;
	private String emailId;
	private String firstName;
	private String lastName;
	private Date dob;
	private Date accountCreationDate;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String zip;
	private String passwordToken;
	private Date passwordTokenExpiryDate;
	private String userRole;
	private String userType;
	private String referralCode;
	
	private KikspotUserDto referredUser;
	private String profilePicture;
	private String preferredVenueType;
	private String deviceToken;
	private Integer creds;
	private Boolean isFirstTimeLogin;
	
	private Integer totalUsers;
	/**
	 * Added isActive And reactivationDate By Bhagya on dec 09th,2015
	 */
	private Boolean isActive;
	private Date reactivationDate;
	
	//for support
	private String dateofBirth;
	private Date lastLoginDate;
	private Boolean isMasterGeoLocation;
	
	/**
	 * Added the app_version by bhagya on Jan 27th, 2020
	 * As per the requirement of version update.
	 * @return
	 */
	private String appVersion;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getAccountCreationDate() {
		return accountCreationDate;
	}
	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPasswordToken() {
		return passwordToken;
	}
	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}
	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}
	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}
	
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public KikspotUserDto getReferredUser() {
		return referredUser;
	}
	public void setReferredUser(KikspotUserDto referredUser) {
		this.referredUser = referredUser;
	}
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}
	public String getDateofBirth() {
		return dateofBirth;
	}
	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Date getReactivationDate() {
		return reactivationDate;
	}
	public void setReactivationDate(Date reactivationDate) {
		this.reactivationDate = reactivationDate;
	}	
	public String getPreferredVenueType() {
		return preferredVenueType;
	}
	public void setPreferredVenueType(String preferredVenueType) {
		this.preferredVenueType = preferredVenueType;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	
	public Integer getCreds() {
		return creds;
	}
	public void setCreds(Integer creds) {
		this.creds = creds;
	}
	
	
	public Boolean getIsFirstTimeLogin() {
		return isFirstTimeLogin;
	}
	public void setIsFirstTimeLogin(Boolean isFirstTimeLogin) {
		this.isFirstTimeLogin = isFirstTimeLogin;
	}
	
	
	public Date getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	
	public Boolean getIsMasterGeoLocation() {
		return isMasterGeoLocation;
	}
	public void setIsMasterGeoLocation(Boolean isMasterGeoLocation) {
		this.isMasterGeoLocation = isMasterGeoLocation;
	}
	
	
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public static KikspotUserDto populateKikspotUserDto(KikspotUser kikspotUser){
		KikspotUserDto kikspotUserDto=new KikspotUserDto();
		kikspotUserDto.setUserId(kikspotUser.getUserId());
		kikspotUserDto.setUsername(kikspotUser.getUsername());
		kikspotUserDto.setEmailId(kikspotUser.getEmailId());
		kikspotUserDto.setPassword(kikspotUser.getPassword());
		kikspotUserDto.setAccountCreationDate(kikspotUser.getAccountCreationDate());
		kikspotUserDto.setAddress1(kikspotUser.getAddress1());
		kikspotUserDto.setAddress2(kikspotUser.getAddress2());
		kikspotUserDto.setCity(kikspotUser.getCity());
		if(null!=kikspotUser.getDob())
			kikspotUserDto.setDob(kikspotUser.getDob());
		kikspotUserDto.setFirstName(kikspotUser.getFirstName());
		kikspotUserDto.setLastName(kikspotUser.getLastName());
		kikspotUserDto.setPasswordToken(kikspotUser.getPasswordToken());
		if(null!=kikspotUser.getPasswordTokenExpiryDate())
			kikspotUserDto.setPasswordTokenExpiryDate(kikspotUser.getPasswordTokenExpiryDate());
		kikspotUserDto.setProfilePicture(kikspotUser.getProfilePicture());
		if(null!=kikspotUser.getReferredUser()){
			kikspotUserDto.setReferredUser(KikspotUserDto.populateKikspotUserDto(kikspotUser.getReferredUser()));
		}
		kikspotUserDto.setState(kikspotUser.getState());
		kikspotUserDto.setTotalUsers(kikspotUser.getTotalUsers());
		kikspotUserDto.setUserRole(kikspotUser.getUserRole().name());
		kikspotUserDto.setUserType(kikspotUser.getUserType().name());
		kikspotUserDto.setZip(kikspotUser.getZip());
		kikspotUserDto.setIsActive(kikspotUser.getIsActive());
		if(null!=kikspotUser.getReactivationDate())
			kikspotUserDto.setReactivationDate(kikspotUser.getReactivationDate());
		kikspotUserDto.setPreferredVenueType(kikspotUser.getPreferredVenueType());
		if(null!=kikspotUser.getLastLoginDate())
			kikspotUserDto.setLastLoginDate(kikspotUser.getLastLoginDate());
		kikspotUserDto.setIsMasterGeoLocation(kikspotUser.getIsMasterGeoLocation());
		kikspotUserDto.setAppVersion(kikspotUser.getAppVersion());
		return kikspotUserDto;
	}
	
}