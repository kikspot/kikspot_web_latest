package com.kikspot.frontend.user.dto;

import java.util.Date;

import com.kikspot.backend.user.model.UserDevice;


/***
 *  Created by Jeevan on December 11, 2015
 *  
 *   Dto for UserDEvices..
 *   
 * @author KNS-ACCONTS
 *
 */
public class UserDeviceDto {

	
	private Integer userDeviceId;
	private KikspotUserDto kikspotUserDto;
	private String deviceToken;
	private String deviceType;
	private Date updatedDate;
	
	
	
	public Integer getUserDeviceId() {
		return userDeviceId;
	}
	public void setUserDeviceId(Integer userDeviceId) {
		this.userDeviceId = userDeviceId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	public static UserDeviceDto populateUserDeviceDto(UserDevice userDevice){
		UserDeviceDto userDeviceDto=new UserDeviceDto();
		userDeviceDto.setUserDeviceId(userDevice.getUserDeviceId());
		userDeviceDto.setDeviceType(userDeviceDto.getDeviceType());
		userDeviceDto.setDeviceToken(userDevice.getDeviceToken());
		if(null!=userDevice.getUpdatedDate()){
			userDeviceDto.setUpdatedDate(userDevice.getUpdatedDate());
		}
		if(null!=userDevice.getKikspotUser()){
			userDeviceDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userDevice.getKikspotUser()));
		}
		return userDeviceDto;
	}
}
