package com.kikspot.frontend.user.dto;

import java.util.ArrayList;
import com.kikspot.backend.user.model.UserPreferences;
/**
 * Created By Bhagya On Feb 02nd, 2016
 *	Dto class for User Preferences
 */
public class UserPreferencesDto{
	private Integer preferenceId;
	private String preferenceItem;
	private ArrayList<UserPreferenceOptionsDto> userPreferenceOptionsDtos;
	
	public Integer getPreferenceId() {
		return preferenceId;
	}
	public void setPreferenceId(Integer preferenceId) {
		this.preferenceId = preferenceId;
	}
	public String getPreferenceItem() {
		return preferenceItem;
	}
	public void setPreferenceItem(String preferenceItem) {
		this.preferenceItem = preferenceItem;
	}
	
	public ArrayList<UserPreferenceOptionsDto> getUserPreferenceOptionsDtos() {
		return userPreferenceOptionsDtos;
	}
	public void setUserPreferenceOptionsDtos(ArrayList<UserPreferenceOptionsDto> userPreferenceOptionsDtos) {
		this.userPreferenceOptionsDtos = userPreferenceOptionsDtos;
	}
	
	public static UserPreferencesDto populateUserPreferencesDto(UserPreferences userPreferences){
		UserPreferencesDto userPreferencesDto=new UserPreferencesDto();
		userPreferencesDto.setPreferenceId(userPreferences.getPreferenceId());
		userPreferencesDto.setPreferenceItem(userPreferences.getPreferenceItem());
		return userPreferencesDto;
	}
	
}