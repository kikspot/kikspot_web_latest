package com.kikspot.frontend.user.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class ChangePasswordDto{
	
	@NotBlank
	@NotNull
	@Size(min=8,max=20)
	private String oldPassword;
	
	@NotBlank
	@NotNull
	@Size(min=8,max=20)
	private String newPassword;
	
	@NotBlank
	@NotNull
	@Size(min=8,max=20)
	private String confirmPassword;
	private Integer userId;
	
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
}