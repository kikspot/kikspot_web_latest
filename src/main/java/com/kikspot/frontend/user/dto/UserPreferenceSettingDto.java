package com.kikspot.frontend.user.dto;

import com.kikspot.backend.user.model.UserPreferenceSetting;
/**
 * Created By Bhagya On Feb 02nd,2016
 * Dto class For User Preference Setting Dto
 *
 */
public class UserPreferenceSettingDto{
	private Integer userPreferenceSettingId;
	private KikspotUserDto kikspotUser;
	private UserPreferencesDto userPreferences;
	private UserPreferenceOptionsDto userPreferenceOptions;
	
	public Integer getUserPreferenceSettingId() {
		return userPreferenceSettingId;
	}
	public void setUserPreferenceSettingId(Integer userPreferenceSettingId) {
		this.userPreferenceSettingId = userPreferenceSettingId;
	}
	public KikspotUserDto getKikspotUser() {
		return kikspotUser;
	}
	public void setKikspotUser(KikspotUserDto kikspotUser) {
		this.kikspotUser = kikspotUser;
	}
	public UserPreferencesDto getUserPreferences() {
		return userPreferences;
	}
	public void setUserPreferences(UserPreferencesDto userPreferences) {
		this.userPreferences = userPreferences;
	}
	public UserPreferenceOptionsDto getUserPreferenceOptions() {
		return userPreferenceOptions;
	}
	public void setUserPreferenceOptions(UserPreferenceOptionsDto userPreferenceOptions) {
		this.userPreferenceOptions = userPreferenceOptions;
	}
	
	public static UserPreferenceSettingDto populateUserPreferenceSettingDto(UserPreferenceSetting userPreferenceSetting){
		UserPreferenceSettingDto userPreferenceSettingDto=new UserPreferenceSettingDto();
		userPreferenceSettingDto.setUserPreferenceSettingId(userPreferenceSetting.getUserPreferenceSettingId());
		userPreferenceSettingDto.setKikspotUser(KikspotUserDto.populateKikspotUserDto(userPreferenceSetting.getKikspotUser()));
		userPreferenceSettingDto.setUserPreferences(UserPreferencesDto.populateUserPreferencesDto(userPreferenceSetting.getUserPreferences()));
		userPreferenceSettingDto.setUserPreferenceOptions(UserPreferenceOptionsDto.populateUserPreferenceOptionsDto(userPreferenceSetting.getUserPreferenceOptions()));
		return userPreferenceSettingDto;
	}
}