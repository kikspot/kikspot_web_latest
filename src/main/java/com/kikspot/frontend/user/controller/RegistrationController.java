package com.kikspot.frontend.user.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;





@Controller("RegistrationController")
@RequestMapping("/register")
public class RegistrationController {

	private static Logger log=Logger.getLogger(RegistrationController.class);
	
	@Resource(name="userService")
	private UserService userService;

		


	@InitBinder
	public void initBinder(WebDataBinder binder) {
	     SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    /* SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/
	     dateFormat.setLenient(true);
	    /* dateTimeFormat.setLenient(false);*/
	     // true passed to CustomDateEditor constructor means convert empty String to null
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));
	}
	
	/**
	 * @author Firdous
	 * @date 03-11-2015
	 * this method is for getting the user profile 
	 * 
	 */
	
	@RequestMapping(value="/initeditprofile.do",method=RequestMethod.GET)
	public String initProfileEdit(Map<String, Object> map)  {
		log.info("inside initEditProfile");
		try  {
			
			String mailId=SecurityContextHolder.getContext().getAuthentication().getName();
			KikspotUserDto kikspotUserDto=this.userService.getUserByEmailId(mailId);
			map.put("kikspotUserDto",kikspotUserDto);				
			return "admin/editProfile";
		}
		
		catch(Exception e)  {
			log.info("Error while initiating profile editing Process");
			String message="Error while initiating profile editing Process";
			map.put("message", message);
			return "error";
		}
	}
	
	/** 
	 * @author Firdous
	 * @param kikspotUserDto
	 * @date 03-11-2015
	 * @param map
	 * @return
	 * method to update the profile of admin
	 */
	
	
	
	@RequestMapping(value="/editprofile.do",method=RequestMethod.POST)
	public String profileEdit(@ModelAttribute KikspotUserDto kikspotUserDto,Map<String,Object>map,RedirectAttributes redAttribs){
		log.info("inside profileEdit()");
		try  {	
			
			Integer result=this.userService.saveInfo(kikspotUserDto);
			if(result>0) {
				redAttribs.addFlashAttribute("status",kikspotUserDto.getUsername()+" updated Successfully");
				return "dashboard";
			}
			else {
				throw new Exception();
			}
		}			
		catch(Exception e)  {				
			log.error("Error while profile editing");
			String message="Error while profile editing, Please Try Again";
			map.put("message", message);
			return "dashboard";
		}
	}
	
	
	
}
