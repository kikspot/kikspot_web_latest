package com.kikspot.frontend.user.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kikspot.backend.exceptions.EmailAlreadyExistsException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UsernameAlreadyExistsException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

import twitter4j.org.json.JSONObject;

/**
 * Created By Bhagya On December 03rd,2015
 *	Controller Class for handling the user services
 */
@Controller("userManagementController")
@RequestMapping("/user")
public class UserManagementController{
	private static Logger log=Logger.getLogger(UserManagementController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	     SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	     dateFormat.setLenient(true);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));
	}
	
	/**
	 * Created By Bhagya On December 03rd,2015
	 * 	Method for Showing the list of users to admin
	 */
	@RequestMapping("/viewusers.do")
	public String viewUsersForAdmin(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info("inside UserController -> viewUsersForAdmin()");
		Integer totalResults;
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("userId");
			}
			ArrayList<KikspotUserDto>  kikspotUserDtos=this.userService.getAllUsers(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection());
			totalResults=kikspotUserDtos.get(0).getTotalUsers();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("kikspotUsers", kikspotUserDtos);
			return "user/viewUsers";
		}
		catch(UserNotFoundException e){
			String message=null;
			if(null!=listBeanDto.getSearchBy()&& listBeanDto.getSearchBy().trim().length()>0){
			  message="Users Not Found For Searched Criteria";
			}
			else{
			 message="Users Not Found";
			}
			map.put("message",message);
			return "user/viewUsers";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Error Occured While Viewing Users";
			map.put("message",error);
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 10th,2015
	 * @param isActive
	 * @param reactivationDate
	 * @return
	 * 
	 * Method For saving the user status which is changed by admin
	 */
	@ResponseBody
	@RequestMapping("/changeuserstatus.do")
	public String changeUserStatus(@RequestParam("userId")Integer userId,@RequestParam("isActive") Boolean isActive,@RequestParam(value="reactivationDate",required=false)Date reactivationDate){
		log.info("inside changeUserStatus()");
		try{
			Integer savedResult=this.userService.savingChangedUserStatus(userId, isActive, reactivationDate);
			return "SUCCESS";
		}
		catch(UserNotFoundException e){
			log.error(" User Not Found For Changing the user Status");
			return "ERROR";
		}
		catch(UserNotSavedOrUpdatedException e){
			log.error("User Status Is Not Saved Or Updated");
			return "ERROR";
		}
		catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
		
	}
	
	/**
	 * Created By Bhagya On December 24th,2015
	 * @param kikspotUserId
	 * @return
	 * 
	 * Method for view the List Of Reffered Users Of a User
	 */
	@ResponseBody
	@RequestMapping("/viewreferredusers.do")
	public String getReferredUsersOfUser(@RequestParam("kikspotUserId") Integer kikspotUserId,Map<String,Object> map){
		log.info("inside getReferredUsersOfUser()");
		try{
			ArrayList<KikspotUserDto> referredUserDtos=this.userService.getReferredUsersOfUser(kikspotUserId);
			JSONObject referredUserJsons=new JSONObject();
			for(KikspotUserDto kikspotUser: referredUserDtos){
				JSONObject referredUserJson=new JSONObject();
				referredUserJson.accumulate("userId", kikspotUser.getUserId());
				if(null!=kikspotUser.getUsername()){
					referredUserJson.accumulate("username", kikspotUser.getUsername());
				}else{
					referredUserJson.accumulate("username", "");
				}
				if(null!=kikspotUser.getFirstName()){
					referredUserJson.accumulate("firstName", kikspotUser.getFirstName());
				}else{
					referredUserJson.accumulate("firstName","");
				}
				if(null!=kikspotUser.getLastName()){
					referredUserJson.accumulate("lastName", kikspotUser.getLastName());
				}else{
					referredUserJson.accumulate("lastName","");
				}
				if(null!=kikspotUser.getEmailId()){
					referredUserJson.accumulate("email",kikspotUser.getEmailId());
				}else{
					referredUserJson.accumulate("email","");
				}
			
				referredUserJson.accumulate("userType", kikspotUser.getUserType());
				Date accountCreationDate=kikspotUser.getAccountCreationDate();
				SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyy");
				referredUserJson.accumulate("accountCreationDate",formatter.format(accountCreationDate));
				referredUserJsons.append("referredUser", referredUserJson);
				System.out.println(kikspotUser.getFirstName()+ "Ds");
			}
			
			return referredUserJsons.toString();
		}
		catch(UserNotFoundException e){
			log.error("No Referred Users Found");
			return "error";
		}
		catch(ReferredUsersNotFoundException e){
			System.out.println(" Referred Users Does Not exists");
			log.error(" Reffered Users Does not exists");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
		
	}
	
	/**
	 * 
	 * Added by Firdous on 5th February 2016
	 * Method to edit the user profile of a user
	 * @param kikspotUserDto
	 * @param map
	 * @param redAttribs
	 * @return
	 * 
	 */
	
	@RequestMapping(value="/editprofile.do",method=RequestMethod.POST)
	public String profileEdit(@ModelAttribute KikspotUserDto kikspotUserDto,Map<String,Object>map,RedirectAttributes redAttribs){
		log.info("inside profileEdit()");
		try  {	
			Integer result=this.userService.updateUserProfile(kikspotUserDto);
			if(result>0) {
				redAttribs.addFlashAttribute("status",kikspotUserDto.getUsername()+" updated Successfully");
				return  "redirect:/user/viewusers.do";
			}
			else {
				throw new Exception();
			}
		}
		catch(EmailAlreadyExistsException e){
			log.error("Error while profile editing");
			System.out.println("Email already exists exception");
			String message="Email already exists,Please try with other email";
			redAttribs.addFlashAttribute("status",message);
			return "redirect:/user/viewusers.do";
		}
		catch(UsernameAlreadyExistsException e){
			log.error("Error while profile editing");
			System.out.println("user name already exists exception");
			String message="User name already exists,Please try with other username";
			redAttribs.addFlashAttribute("status",message);
			return "redirect:/user/viewusers.do";
		}
		catch(Exception e)  {				
			log.error("Error while profile editing");
			String message="Error while profile editing, Please Try Again";
			map.put("message", message);
			return "dashboard";
		}
	}
	
	
	/**
	 * 
	 * @param userId
	 * @param creds
	 * @return
	 */
	@RequestMapping("/updatecreds.do")
	@ResponseBody
	public String updateUserCreds(@RequestParam("userId") Integer userId, @RequestParam("creds") Integer creds){
		log.info("inside updateUserCreds() ");
		try{
			Integer savedResult=this.userService.updateUserGameCreds(userId, creds);
			return "success";
		}
		catch(Exception e){
			return "error";
		}
	}
	
	/**
	 * 
	 * Created by Firdous on 17 February 2016
	 * Method to reset the password of a user by admin
	 * @param userId
	 * @param password
	 * @param map
	 * @return
	 * 
	 */
	@RequestMapping(value="/resetuserpassword.do",method=RequestMethod.POST)
	public String performResetPassword(@RequestParam("userId") Integer userId,@RequestParam("password") String password,Map<String,Object> map){
		log.info("inside performResetPassword()");
		try{
			
			this.userService.resetUserPassword(userId, password);
			map.put("message", "Password Reset Successfully");
			return "redirect:/user/viewusers.do";
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(UserNotSavedOrUpdatedException e){
			e.printStackTrace();
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Reset Password, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	
	/**
	 * @author Firdous
	 * method to delete the feedback of a user
	 * @param feedbackId
	 * 
	 * 
	 *//*
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="/deleteuser.do")
	public String deleteuser(@RequestParam("userId")Integer id){
		
		log.info("inside deleteFeedback()");
		try {
			Integer result=this.userService.deleteUser(id);
			if(result>0){
			  return "success";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "error";
		}	
	}
		*/
	
}