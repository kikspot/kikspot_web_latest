package com.kikspot.frontend.user.controller;
/**
 * Created By Bhagya on October 13th, 2015
 * Maintaining the Login methods
 */
import java.util.Map;








import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.user.dto.ChangePasswordDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

import org.apache.log4j.Logger;

@Controller("webLoginController")
public class LoginController{
	
private static Logger log=Logger.getLogger(LoginController.class);
	
	@Resource(name="userService")
	private UserService userService;
	/**
	 * Created by bhagya on october 13th,2015
	 * Init the login 
	 */
	@RequestMapping(value="/login.do",method=RequestMethod.GET)
	public String login(){
		log.info("inside login");
		
		try{			
			 return "login";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	
	/**
	 * Created by bhagya on october 13th,2015
	 * @param request
	 * @param map
	 * @return
	 * 
	 * Method for handling the process after login success
	 */
	
	@RequestMapping(value="/loginsuccess.do",method=RequestMethod.GET)
	public String redirectToLoginSuccess(HttpServletRequest request,Map<String, Object> map){
		log.info("inside redirectToLoginSuccess");
		HttpSession session=request.getSession(true);
		try{
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();
			String user=auth.getName();
			KikspotUserDto kikspotUserDto=this.userService.getKikspotUserByUsername(user);
			session.setAttribute("username", kikspotUserDto.getUsername());
			session.setAttribute("userId", kikspotUserDto.getUserId());
			return "redirect:/home.do";
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Login, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
	}
	
	
	
	
	/**
	 *   Created by Jeevan on DECEMBER 29, 2015
	 *   
	 *   Method to redirect user to home..
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/home.do",method=RequestMethod.GET)
	public String redirectUsertoHome(HttpServletRequest request, HttpServletResponse response,Map<String, Object> map){
		log.info("inside redirectUsertoHome() ");
		try{
			HttpSession session=request.getSession(true);
			if(null!=session.getAttribute("username")){
				return "dashboard";
			}
			else{
				return "redirect:/";
			}			
		}
		catch(Exception e){
			e.printStackTrace();			
			return "redirect:/";
		}
	}
	
	
	
	
	/**
	 * Created By bhagya On october 19th, 2015
	 * Initialize the Forgot Password
	 */
	@RequestMapping(value="/forgotpassword.do",method=RequestMethod.GET)
	public String initForgotPassword(){
		log.info("inside initForgotPassword()");
		try{
			
			return "forgotPassword";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param email
	 * @param map
	 * @return
	 * 
	 * Method for process the Forgot Password,
	 * Reset password link to be sent the registered mail
	 */
	@RequestMapping(value="/forgotpassword.do",method=RequestMethod.POST)
	public String processForgotPassword(@RequestParam("emailId") String emailId,Map<String, Object> map){
		log.info("inside processForgotPassword()");
		try{
			if(!emailId.equals("")){
				Integer successMail=this.userService.sendPasswordResetEmail(emailId);
				String msg= "Password reset link has been sent to your registered email, please check your mail.";
				map.put("sucessMessage", msg);
				map.put("title", "Reset Password");
				return "login";
			}
			
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(UserNotSavedOrUpdatedException e){
			e.printStackTrace();
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(MailNotSentException e){
			e.printStackTrace();
			String error="Mail Not Sent";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While ForgotPassword, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		return "forgotPassword";
	}
	/**
	 * Created By bhagya On october 19th,2015
	 * @param token
	 * @param map
	 * @return
	 * 
	 * Method for initiate the Reset Password
	 */
	@RequestMapping(value="/resetpassword.do",method=RequestMethod.GET)
	public String initResetPassword(@RequestParam("token") String token,Map<String,Object> map){
		log.info("inside initResetPassword()");
		try{
			Integer userId = this.userService.getUserIdByPasswordResetToken(token);
			map.put("userId", userId);
			map.put("title", "Reset Password");
			return "resetPassword";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("errorMessage","This link is not valid any more");
			return "login";
		}
	}
	/**
	 * Created By Bhagya on october 20th, 2015
	 * @param userId
	 * @param password
	 * @param confirmPassword
	 * @param map
	 * @return
	 * Method For Saving the reset Password
	 */
	@RequestMapping(value="/resetpassword.do",method=RequestMethod.POST)
	public String performResetPassword(@RequestParam("userId") Integer userId,@RequestParam("password") String password,
			@RequestParam("confirmPassword") String confirmPassword,Map<String,Object> map){
		log.info("inside performResetPassword()");
		try{
			if(password.equals(confirmPassword)){
			this.userService.handlePasswordReset(userId, password);
			map.put("sucessMessage", "Password Reset Successfully");
			}
			else{
				map.put("errorMessage", "Confirmation Password doesn't match Password");
			}
			return "login";
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(UserNotSavedOrUpdatedException e){
			e.printStackTrace();
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Reset Password, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 16th,2015
	 * @param userId
	 * @param map
	 * @param changePasswordDto
	 * @return
	 * 
	 * Method FOr Initiating the Change Password
	 */
	@RequestMapping(value="/changepassword.do",method=RequestMethod.GET)
	public String initChangePassword(@RequestParam("userId")Integer userId,Map<String,Object> map,@ModelAttribute("changePasswordDto") ChangePasswordDto changePasswordDto){
		log.info("inside initChangePassword()");
		try{
			map.put("userId", userId);
			return "changePassword";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Initiating Change Password,Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 16th,2015
	 * @param changePasswordDto
	 * @param validateResult
	 * @param map
	 * @return
	 * 
	 * Method for Performing The change Password
	 */
	@RequestMapping(value="/changepassword.do",method=RequestMethod.POST)
	public String processChangePassword(@Valid @ModelAttribute("changePasswordDto") ChangePasswordDto changePasswordDto,BindingResult validateResult,Map<String,Object> map){
		log.info("inside processChangePassword()");
		try{
			if(validateResult.hasErrors()){
				return "changePassword";
			}
			Integer updatedResult=this.userService.performChangePassword(changePasswordDto);
			if(updatedResult>0){
				map.put("message", "Password Changed Successfully");
				return "changePassword";
			}
			else{
				throw new Exception();
			}
		}
		catch(UserNotFoundException e){
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(PasswordNotMatchException e){
			String error="Password entered does not match with existing password";
			map.put("message",error);
			return "changePassword";
		}
		catch(UserNotSavedOrUpdatedException e){
			String error="Problem While Updating Password";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Performing Change Password,Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	
	
	
	/**
	 * Created By Bhagya On december 23rd,2015
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 * 
	 * Method for handling the logout 
	 */
	
	@RequestMapping(value="/logoutsuccess.do", method=RequestMethod.GET)
	public String logoutHandler(Map<String, Object>map,HttpServletRequest request,HttpServletResponse response){
		log.info("inside logoutHandler()");
		try{
			
			HttpSession session=request.getSession(false);
			if(null!=session)
				session.invalidate();
				response.reset();
			   response.setHeader("Cache-Control", "no-cache");
			   response.setHeader("Pragma", "no-cache");
			    response.setHeader("Cache-Control", "no-store");
			   response.setHeader("Cache-Control", "must-revalidate");
			   response.setDateHeader("Expires", 0); 
			   String message="Successfully Logged Out";
			   map.put("message", message);			  
			  return "logout";
		}
		catch(Exception e){
			log.info("User Logged Out"+ e.toString());
			e.printStackTrace();
			String message="Error While Logging out the User";
			   map.put("message", message);
			return "error";
		}
	}
	
	
	
	/**
	 * 
	 * @param userId
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/referuser.do",method=RequestMethod.GET)
	public String getProbableReferredUserofShare(@RequestParam("referredUser") Integer userId,Map<String, Object> map){
		log.info("inside getProbableReferredUserofShare()");
		try{
			map.put("referredUser", userId);
			return "referUser";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Downloading App");
			log.error("Error While Downloading App"+e.toString());
			return "error";
		}
	}
	
	
	
	
	/**
	 * 
	 * @param userId
	 * @param email
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/referuser.do",method=RequestMethod.POST)
	public String saveProbableReferredUserofShare(@RequestParam("referredUser") Integer userId, @RequestParam("email") String email,
			Map<String, Object> map){
		log.info("inside saveProbableReferredUserofShare() ");
		try{
			Integer savedResult=this.userService.saveProbableReferredUser(userId, email);
			return "redirect:/https://itunes.apple.com?app=kikspot";
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Downloading Kikspot App";
			map.put("message", message);
			return "error";
		}
		
	}
	
	
	
}

