package com.kikspot.frontend.mobile.controller;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;
import com.kikspot.frontend.game.service.CredService;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

/**
 * Created By Bhagya On January 21st,2016
 *	Controller Class Rewards,implemented services for mobile
 */
@RestController
@RequestMapping("/mobile")
public class RewardsController{
	
	private static Logger log=Logger.getLogger(RewardsController.class);
	
	@Resource(name="credService")
	private CredService credService;
	
	/**
	 * Created By Bhagya On January 22nd,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For Getting the Kikspot Rewards Of a User
	 */
	@RequestMapping(value="/getuserrewards.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getMatchedRewardsForUser(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getMatchedRewardsForUser()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			ArrayList<KikspotRewardsDto> kikspotRewardsDtos=this.credService.getKikspotRewardsOfUserByUserLevelAndCreds(userId);
			ArrayList<JSONObject> kikspotRewardsJSONS=new ArrayList<JSONObject>();
			if(!kikspotRewardsDtos.isEmpty()){
				for(KikspotRewardsDto kikspotRewardDto:kikspotRewardsDtos){
					JSONObject kikspotRewardJSON=new JSONObject();
					kikspotRewardJSON.accumulate("rewardId", kikspotRewardDto.getRewardId());
					if(null!=kikspotRewardDto.getReward()){
						kikspotRewardJSON.accumulate("reward", kikspotRewardDto.getReward());
					}
					else{
						kikspotRewardJSON.accumulate("reward", "");
					}
					kikspotRewardsJSONS.add(kikspotRewardJSON);
				}
				outputJson.accumulate("rewards", kikspotRewardsJSONS);
			}
			else{
				throw new KikspotRewardNotFoundException();
			}
			
		}
		catch(UserNotFoundException e){
			log.error("User Not Found");
			outputJson.accumulate("ERROR", "User Not Found");
		}
		catch(UserGameCredNotFoundException e){
			log.error("User Game Creds Not Found");
			outputJson.accumulate("ERROR", "User Game Creds Not Found");
		}
		catch(KikspotRewardNotFoundException e){
			log.error("Rewards Not Found For User");
			outputJson.accumulate("ERROR", "Rewards Not Found For User");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Rewards of User");
			outputJson.accumulate("ERROR", "Error While Fetching Rewards of User");
		}
		System.out.println(" JSON Response : "+outputJson.toString());
		return outputJson.toString();
	}
	
	
	/**
	 * Created By Bhagya On January 22nd,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For Getting the kikspot Reward Deatils By kikspot Reward
	 */
	@RequestMapping(value="/getrewarddetails.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getKikspotRewardDetails(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getKikspotRewardDetails()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			Integer rewardId=inputJson.getInt("rewardId");
			KikspotRewardsDto kikspotRewardDto=this.credService.getKikspotRewardByRewardIdForUser(userId, rewardId);
			JSONObject kikspotRewardJSON=new JSONObject();
			kikspotRewardJSON.accumulate("rewardValue", kikspotRewardDto.getRewardValue());
			outputJson.accumulate("rewardDetails", kikspotRewardJSON);
		}
		catch(UserNotFoundException e){
			log.error("User Not Found");
			outputJson.accumulate("ERROR", "User Not Found");
		}
		catch(KikspotRewardNotFoundException e){
			log.error("Reward Not Found For User");
			outputJson.accumulate("ERROR", "Reward Not Found For User");
		}
		catch(UserGameCredNotFoundException e){
			log.error("User Game Creds Not Found");
			outputJson.accumulate("ERROR", "User Game Creds Not Found");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Reward Details of User");
			outputJson.accumulate("ERROR", "Error While Fetching Reward Details of User");
		}
		System.out.println("JSON Response : "+outputJson.toString());
		return outputJson.toString();
	}
}