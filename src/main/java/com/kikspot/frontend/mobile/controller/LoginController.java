package com.kikspot.frontend.mobile.controller;

import java.util.Map;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

import twitter4j.org.json.JSONObject;

@RestController
public class LoginController{
private static Logger log=Logger.getLogger(LoginController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	/**
	 * Created By Bhagya On October 26th, 2015
	 * @param userDto
	 * @param map
	 * @return
	 * 
	 * This method returns the userId based on facebook login user details
	 */
	@RequestMapping(value="/facebooklogin.do",method=RequestMethod.POST)
	public String getUserIdOfFacebookLoginUser(@RequestBody KikspotUserDto userDto,Map<String,Object> map,HttpServletRequest request,HttpServletResponse response){
		log.info("inside getUserIdOfFacebookLoginUser()");
		try{
			/*JSONObject json=JsonParser.getJson(request, response);
			String emailId=json.getString("emailId");
			System.out.println(" emailId at controller"+emailId);*/
			Integer userId=this.userService.handleFacebookLoginUsers(userDto);
			JSONObject obj=new JSONObject();
			obj.accumulate("userId", userId);
			return obj.toString();
		}
		catch(UserNotSavedOrUpdatedException e){
			e.printStackTrace();
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Facebook Login, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
}