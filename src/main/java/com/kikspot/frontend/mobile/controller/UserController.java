package com.kikspot.frontend.mobile.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.kikspot.backend.exceptions.AccountInActiveException;
import com.kikspot.backend.exceptions.EmailAlreadyExistsException;
import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserPreferencesNotFoundException;
import com.kikspot.backend.exceptions.UsernameAlreadyExistsException;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UpdatedAppVersionNotMatchException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.user.dto.ChangePasswordDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.dto.UserLocationDto;
import com.kikspot.frontend.user.dto.UserPreferenceOptionsDto;
import com.kikspot.frontend.user.dto.UserPreferencesDto;
import com.kikspot.frontend.user.service.UserService;


/**
 * 
 *  Created by Jeevan on November 23, 2015
 *  
 *  Controller for all User Related aCtivities..
 *  
 * 
 * @author KNS-ACCONTS
 *
 */
@RestController
@RequestMapping("/mobile")
public class UserController {
	
	
	private static Logger log=Logger.getLogger(UserController.class);
	

	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="gameService")
	private GameService gameService;
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	  
	  dateFormat.setLenient(false);	
	   // true passed to CustomDateEditor constructor means convert empty String to null
	  binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));
	  
	 
	}
	
	
	/**
	 * 
	 * Created by Jeevan on November 23, 2015
	 * 
	 *  Method to initiate Signup..
	 *  
	 * @param request
	 * @param response
	 * @param userDto
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping("/signup.do")
	public String signupUser(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto) throws JSONException{
		log.info("inside singUpUser() ");
		System.out.println("inside sign up");
		JSONObject json=new JSONObject();
		try{
			KikspotUserDto userDto=this.userService.initUserSignUp(kikspotUserDto);
			System.out.println("save user info inside database");
			json.accumulate("STATUS", "Success");
			json.accumulate("UserId", userDto.getUserId());
			json.accumulate("Email", userDto.getEmailId());
			if(null!=userDto.getReferredUser()){
				json.accumulate("Referral", true);
			}
			else{
				json.accumulate("Referral", false);
			}
		}
		catch(EmailAlreadyExistsException e){
			log.error("Email Already Exists "+e.toString());
			json.accumulate("ERROR", "Email Already Exists");
			
		}
		catch(UsernameAlreadyExistsException e){
			log.error("Username Already Exists "+e.toString());
			json.accumulate("ERROR", "Username Already Exists");
			
		}
		/*catch(EmailAlreadyExistsException e){
			log.error("Email Already Exists "+e.toString());
			json.accumulate("ERROR", "Email Already Exists");
		}*/
		catch(ReferredUsersNotFoundException e){
			log.error("No User Probable Referral Found For Entered Referral Code "+e.toString());
			json.accumulate("ERROR", "No User Probable Referral Found For Entered Referral Code");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While User Sign Up "+e.toString());
			json.accumulate("ERROR","Error Signingup User");
		}
		return json.toString();
		
	}
	
	
	
	
	
	/**
	 * Created by Jeevan on November 24, 2015
	 * Method to complete Signup Process.
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param kikspotUserDto
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping("/completesignup.do")
	public String completeSignup(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws JSONException{
		log.info("inside completeSignup()");
		JSONObject json=new JSONObject();
		try{
			SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
			if(null!=kikspotUserDto.getDateofBirth() && kikspotUserDto.getDateofBirth().trim().length()>0){
				kikspotUserDto.setDob(dateFormat.parse(kikspotUserDto.getDateofBirth()));
			}
			Integer savedResult=this.userService.completeSignUp(kikspotUserDto);
			if(savedResult>0)
				json.accumulate("STATUS", "Registration Completed Successfully");			
			else
				throw new Exception();
		}
		catch(UsernameAlreadyExistsException e){
			log.error("Username Already Exists "+e.toString());
			json.accumulate("ERROR", "Username Already Exists");
			
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Completing Registration "+e.getMessage());
			json.accumulate("ERROR", "Unexpected Error While Completing Signup, Please Try Again");			
		}
		return json.toString();
	}
	
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  
	 *  Method to PerformSocialLogin..()
	 * 
	 * @param request
	 * @param response
	 * @param kikspotDto
	 * @return
	 * @throws JsonGenerationException
	 * @throws JSONException 
	 * 
	 *  Modified By Bhagya On December 10th,2015
	 * Handling the condition when the account is inactive
	 */
	@RequestMapping("/sociallogin.do")
	public String performSocialLogin(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws JsonGenerationException, JSONException{
		log.info("inside performSocialLogin() ");
		JSONObject json=new JSONObject();
		try{
			
			KikspotUserDto userDto=this.userService.performSocialLogin(kikspotUserDto);
			System.out.println("SOCIAL "+kikspotUserDto.getDeviceToken());
				json.accumulate("STATUS", "Success");
				json.accumulate("UserId", userDto.getUserId());
			
			if(null!=userDto.getFirstName() && userDto.getFirstName().trim().length()>0){
				if(null!=userDto.getReferredUser()){
					json.accumulate("Referral", true);
				}
				else{
					json.accumulate("Referral", false);
				}
			}
			json.accumulate("FirstLogin", userDto.getIsFirstTimeLogin());
		}
		catch(AccountInActiveException e){
			log.error(" Account is InActive For Social Login User "+e.toString());
			json.accumulate("ERROR", "Account is InActive For Social Login User");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Social Login User "+e.toString());
			json.accumulate("ERROR","Error While Social Login User");
		}
		return json.toString();
	}
	
	
	
	/**
	 * 
	 * Created by Jeevan on November 14, 2015
	 * 
	 *  Method to byPassUser...
	 *  
	 * 
	 * 
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping("/bypassuser.do")
	public String bypassUser(HttpServletRequest request,HttpServletResponse response,@RequestBody String deviceToken)throws JSONException{
		JSONObject json=new JSONObject();
		try{
			
			JSONObject deviceJson=new JSONObject(deviceToken);
			deviceToken=deviceJson.getString("deviceToken");
			
		//	String deviceToken=inputJson.getString("deviceToken");
		///	deviceToken=deviceToken.substring(deviceToken.charAt(deviceToken.indexOf(":\"")));
			Integer userSavedResult=this.userService.registerByPassUser(deviceToken);
			json.accumulate("STATUS", "Success");
			json.accumulate("UserId", userSavedResult);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While User Sign Up Bypass User "+e.toString());
			json.accumulate("ERROR","Error While Displaying Your Dashboard, Please Try Again");
		}	
		return json.toString();
	}
	
	
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * 
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  Method to perform User Login..
	 * 
	 * Modified By Bhagya On December 10th,2015
	 * Handling the condition when the account is inactive
	 * 
	 */
	@RequestMapping("/login.do")
	public String loginUser(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws Exception{
		log.info("inside loginUser() ");
		JSONObject json=new JSONObject();
		
		System.out.println("LOGIN "+kikspotUserDto.getDeviceToken());
		try{
			Map<String,Object> usermap=this.userService.authenticateMobileUser(kikspotUserDto);
			
			String appVersion=(String) usermap.get("appVersion");
			
			if(null!=appVersion && appVersion.length()>0){
				json.accumulate("STATUS", "Success");
				json.accumulate("UserId", usermap.get("userId"));
				json.accumulate("Email", usermap.get("email"));
			}
			else{
				throw new UpdatedAppVersionNotMatchException();
			}
		}
		catch(UserNotFoundException e){
			log.error(" NO User Found with given username "+e.toString());
			json.accumulate("ERROR", "No User Exists with Entered Username");
		}
		catch(AccountInActiveException e){
			log.error(" Account is InActive with Entered username "+e.toString());
			json.accumulate("ERROR", "Account is InActive with Entered username");
		}
		catch(PasswordNotMatchException e){
			log.error("Password Does Not match "+e.toString());
			json.accumulate("ERROR", "Invalid Password, Please Enter Again");
		}
		catch(UpdatedAppVersionNotMatchException e){
			log.error("Updated App Version Does Not match "+e.toString());
			json.accumulate("ERROR", "There is a newer version of this app available! Please update the version from App Store"); 
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return json.toString();
	}
	
	
	
	/**
	 * Created By Bhagya On November 26th, 2015
	 * @param request
	 * @param response
	 * @param kikspotUserDto
	 * @return
	 * @throws JSONException
	 * 
	 * Method For Updating the Profile Details
	 */
	
	@RequestMapping("/editprofile.do")
	public String editProfile(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws JSONException{
		log.info("inside editProfile()");
		JSONObject json=new JSONObject();
		try{
			SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
			if(null!=kikspotUserDto.getDateofBirth() && kikspotUserDto.getDateofBirth().trim().length()>0){
				kikspotUserDto.setDob(dateFormat.parse(kikspotUserDto.getDateofBirth()));
			}
			
			Integer savedResult=this.userService.saveUserInfo(kikspotUserDto);
			if(savedResult>0)
				json.accumulate("STATUS", "Profile Updated Successfully");			
			else
				throw new Exception();
		}
		catch(EmailAlreadyExistsException e){
			log.error("Email Already Exist");
			json.accumulate("ERROR", "Email Already Exist.. Please try with other user name");
		}
		catch(UsernameAlreadyExistsException e){
			log.error("USER NAME Already Exist");
			json.accumulate("ERROR", "User Name Already Exist.. Please try with other user name");
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Edit Profile");
			json.accumulate("ERROR", "Unexpected Error While Completing Edit Profile, Please Try Again");			
		}
		return json.toString();
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 *  
	 * 
	 * @param request
	 * @param response
	 * @param userLocationDto
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping("/saveuserlocation.do")
	public String saveUserLocation(HttpServletRequest request, HttpServletResponse response, @RequestBody UserLocationDto userLocationDto)throws JSONException{
		log.info("inside saveUserLocation() ");
		JSONObject json=new JSONObject();
		try{
			System.out.println(" inside save user location");
			this.userService.updateUserLocation(userLocationDto);
			json.accumulate("STATUS", "success");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("error while saving user location "+e.toString());
			json.accumulate("STATUS", "success");
			//No need to send any error as user location ll be available everytime,
		}
		return json.toString();
	}
	
	
	
	
	
	
	
	/***
	 * Created by Jeevan on DECEMBER 15, 2015
	 * Method to saveuserPreferenceandProfileImage..
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping("/saveprofileimage.do")
	public String saveUserPreferenceandProfileImage(HttpServletRequest request,HttpServletResponse response)throws JSONException{
		log.info("inside saveUserPreferenceandProfileImage() ");
		System.out.println("INSIDE CONTROLLER ");
		JSONObject json=new JSONObject();
		try{
			
		
		
			MultipartRequest multiRequest=(MultipartRequest) request;			
			//JSONObject inputJson=JsonParser.getJson(request, response);
			MultipartFile imageFile=multiRequest.getFile("profileImage");
			System.out.println(imageFile.getOriginalFilename());
			System.out.println("REQ "+request.getParameter("json")+" "+request.getParameter("userId"));
			 JSONObject json1=new JSONObject(request.getParameter("json"));
			 System.out.println("request : "+json1.toString());
			Integer userId=json1.getInt("userId");
			String preferredVenueType=json1.getString("preferredVenueType");
			Integer updatedResult=this.userService.saveUserPreferenceandProfileImage(userId, preferredVenueType, imageFile);
			if(updatedResult>0){
				json.accumulate("STATUS", "success");
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Update User Profile Picture "+e.toString());
			json.accumulate("ERROR", "Error While Uploading User Profile Picture");
		}		
		return json.toString();
	}
	
	
	
	
	
	
	/**
	 * Created by Jeevan on Decemebr 15, 2015
	 * 
	 *  Method to initiate Forgot Password process.
	 *  
	 *  
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping(value="/forgotpassword.do")
	public String initForgotPassword(HttpServletRequest request,HttpServletResponse response)throws JSONException{
		log.info("inside initForgotPassword() ");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			String email=inputJson.getString("email");
			
			Integer result=this.userService.initForgotPasswordforMobile(email);
			if(result>0){
				json.accumulate("email", email);
				json.accumulate("STATUS", "success");
			}
			else{
				throw new Exception();
			}
		}
		catch(UserNotFoundException e){
			log.error(" NO User Found with given Email "+e.toString());
			json.accumulate("ERROR", "No User Exists with Entered Email");
		}
		catch(MailNotSentException e){
			e.printStackTrace();
			log.error("Error While Sending Password Token to mail "+e.toString());
			json.accumulate("ERROR", "Error While Sending Password Token trough Mail");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Sending Password Token Mail "+e.toString());
			json.accumulate("ERROR", "Error While Initiating Forgot Password");
						
		}		
		return json.toString();
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 15, 2015
	 * 
	 * Method to resetPassword...
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/resetpassword.do")
	public String resetPassword(HttpServletRequest request,HttpServletResponse response)throws Exception{
		log.info("inside resetPassword()");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			String email=inputJson.getString("email");
			String token=inputJson.getString("token");
			String password=inputJson.getString("password");
			this.userService.resetMobileUserPassword(email, token, password);
			json.accumulate("STATUS", "success");
		}
		catch(PasswordNotMatchException e){
			e.printStackTrace();
			log.error("Token does not match");
			json.accumulate("ERROR","Token Does Not Match, Please Enter Again");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Resetting Mobile Password");
			json.accumulate("ERROR", "Error While Resetting Password");
		}
		
		
		return json.toString();
	}
	
	/**
	 * Created By Bhagya On December 16th,2015
	 * @param changePasswordDto
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for Performing the change Password
	 */
	@RequestMapping("/changepassword.do")
	public String performChangePassword(@RequestBody ChangePasswordDto changePasswordDto,HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside UserController -> performChangePassword()");
		JSONObject json=new JSONObject();
		try{
			Integer updatedResult=this.userService.performChangePassword(changePasswordDto);
			if(updatedResult>0){
				json.accumulate("STATUS", "success");
			}
			else{
				throw new Exception();
			}
			
		}
		catch(UserNotFoundException e){
			log.error("User does not exist");
			json.accumulate("ERROR","User does not exist");
		}
		catch(PasswordNotMatchException e){
			log.error("Password entered does not match with existing password, Please Enter Again");
			json.accumulate("ERROR","Password entered does not match with existing password, Please Enter Again");
			
		}
		catch(UserNotSavedOrUpdatedException e){
			log.error("Problem while saving the new password");
			json.accumulate("ERROR","Problem while saving the new password");
		}
		catch(Exception e){
			log.error("Problem while performing the change password");
			json.accumulate("ERROR","Problem while performing the change password");
		}
		return json.toString();
	}
	/**
	 * Created By BHagya On December 28th,2015
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException 
	 * 
	 * Method for showing the User Profile Details
	 */
	@RequestMapping("/profiledetails.do")
	public String getProfileDetails(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getProfileDetails()");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
			JSONObject kikspotUserJSON=new JSONObject();
			kikspotUserJSON.accumulate("userId", kikspotUserDto.getUserId());
			kikspotUserJSON.accumulate("username", kikspotUserDto.getUsername());
			if(null!= kikspotUserDto.getFirstName()){
			kikspotUserJSON.accumulate("firstName", kikspotUserDto.getFirstName());
			kikspotUserJSON.accumulate("lastName", kikspotUserDto.getLastName());
			if(null!=kikspotUserDto.getDob()){
				Date dateOfBirth=kikspotUserDto.getDob();
				SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyy");
				kikspotUserJSON.accumulate("dateofBirth", formatter.format(dateOfBirth));
			}
			else{
				kikspotUserJSON.accumulate("dateofBirth", "");
			}
			kikspotUserJSON.accumulate("address1", kikspotUserDto.getAddress1());
			kikspotUserJSON.accumulate("address2", kikspotUserDto.getAddress2());
			kikspotUserJSON.accumulate("city", kikspotUserDto.getCity());
			kikspotUserJSON.accumulate("state", kikspotUserDto.getState());
			kikspotUserJSON.accumulate("zip", kikspotUserDto.getZip());
			
			}
			else{
				kikspotUserJSON.accumulate("firstName", "");
				kikspotUserJSON.accumulate("lastName","");
				kikspotUserJSON.accumulate("dateofBirth","");
				kikspotUserJSON.accumulate("address1","");
				kikspotUserJSON.accumulate("address2","");
				kikspotUserJSON.accumulate("city","");
				kikspotUserJSON.accumulate("state","");
				kikspotUserJSON.accumulate("zip","");
				
			}
			if(null!=kikspotUserDto.getEmailId()){
				kikspotUserJSON.accumulate("emailId", kikspotUserDto.getEmailId());
			}
			else{
				kikspotUserJSON.accumulate("emailId", "");
			}
			kikspotUserJSON.accumulate("profilePicture", kikspotUserDto.getProfilePicture());
			json.accumulate("profileDetails ",kikspotUserJSON);
			System.out.println("JSON Response "+json.toString());
			
			
		}
		catch(UserNotFoundException e){
			log.error("User does not exist");
			json.accumulate("ERROR","User does not exist");
			
		}
		catch(Exception e){
			log.error("Problem while getting user profile ");
			json.accumulate("ERROR","Problem while getting the user profile");
			
		}
		return json.toString();
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * Method to getShareUrlofREferral..
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping("/shareapp.do")
	public String getShareUrlforUser(HttpServletRequest request, HttpServletResponse response)throws JSONException{
		log.info("inside getSharedUrlforUser() ");
		JSONObject json=new JSONObject();
		try{
			
			String referralCode;
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			if(this.userService.getKikspotUserDetailsByUserId(userId).getReferralCode()==null){
				referralCode="referral"+userId;
				this.userService.saveReferralCode(userId,referralCode);
			}
			else{
				referralCode=this.userService.getKikspotUserDetailsByUserId(userId).getReferralCode();
			}
			//String url=this.userService.sendShareUrlofUser(userId);
			json.accumulate("referralCode",referralCode);
			String url="https://itunes.apple.com/us/app/kikspot/id1118403068?ls=1&mt=8";
			json.accumulate("URL", url);
			json.accumulate("message", "Hey! Checkout this new app, Kikspot! ");
			System.out.println(json.toString());
		}
		catch(Exception e){
			e.printStackTrace();
			json.accumulate("ERROR", "Error While Getting Link to Share");
			log.error("ERROR WHile Getting Share link "+e.toString());
		}
		return json.toString();
	}
	/**
	 * Created By Bhagya On feb 02nd, 2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for getting the list of all user preferences of a user
	 */
	
	@RequestMapping(value="/getuserpreferences.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getAllUserPreferencesofUser(HttpServletRequest request, HttpServletResponse response) throws JSONException{
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			ArrayList<UserPreferencesDto> userPreferencesDtos=this.userService.getAllUserPreferences(userId);
			ArrayList<JSONObject> userPreferencesJSONS=new ArrayList<JSONObject>();
			for(UserPreferencesDto userPreferencesDto:userPreferencesDtos){
				JSONObject userPreferenceJSON=new JSONObject();
				userPreferenceJSON.accumulate("preferenceId", userPreferencesDto.getPreferenceId());
				if(null!=userPreferencesDto.getPreferenceItem()){
				userPreferenceJSON.accumulate("preferenceItem", userPreferencesDto.getPreferenceItem());
				}
				else{
					userPreferenceJSON.accumulate("preferenceItem", "");
				}
				ArrayList<UserPreferenceOptionsDto> userPreferenceOptionsDtos=userPreferencesDto.getUserPreferenceOptionsDtos();
				ArrayList<JSONObject> userPreferenceOptionsJSONS=new ArrayList<JSONObject>();
					for(UserPreferenceOptionsDto userPreferenceOptionsDto:userPreferenceOptionsDtos){
						JSONObject userPreferenceOptionJSON=new JSONObject();
						userPreferenceOptionJSON.accumulate("userPreferenceOptionsId", userPreferenceOptionsDto.getUserPreferenceOptionsId());
						if(null!=userPreferenceOptionsDto.getOption()){
							userPreferenceOptionJSON.accumulate("option", userPreferenceOptionsDto.getOption());
						}
						else{
							userPreferenceOptionJSON.accumulate("option", "");
						}
						userPreferenceOptionJSON.accumulate("isSelected", userPreferenceOptionsDto.getIsSelected());
						
						userPreferenceOptionsJSONS.add(userPreferenceOptionJSON);
					}
				
				userPreferenceJSON.accumulate("preferenceOptions", userPreferenceOptionsJSONS);
				
				userPreferencesJSONS.add(userPreferenceJSON);
			}
			outputJson.accumulate("userPreferences", userPreferencesJSONS);
		}
		catch(UserNotFoundException e){
			outputJson.accumulate("ERROR", "User Not Found");
		}
		catch(UserPreferencesNotFoundException e){
			outputJson.accumulate("ERROR", "User Preferences Not Found");
		}
		catch(Exception e){
			e.printStackTrace();
			outputJson.accumulate("ERROR", "Error While Getting User Preferences");
			log.error("ERROR While Getting User Preferences "+e.toString());
		}
		System.out.println(" output Json "+outputJson.toString());
		return outputJson.toString();
		
	}
	/**
	 * Created By Bhagya On Feb 03rd, 2016
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException 
	 * 
	 * Method for saving the user preferences of a user
	 */
	@RequestMapping("/saveuserpreferences.do")
	public String saveUserPreferences(HttpServletRequest request, HttpServletResponse response) throws JSONException{
		log.info("inside saveUserPreferences()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			JSONArray options=inputJson.getJSONArray("options");
			ArrayList<Integer> optionIds=new ArrayList<Integer>();
			for(int i=0;i<options.length();i++){
				optionIds.add(options.getInt(i));
			}
			Integer SavedResult=this.userService.saveUserPreferences(userId, optionIds);
			if(SavedResult>0){
				outputJson.accumulate("STATUS", "User Preferences Saved Successfully");
			}
			else{
				throw new Exception();
			}
		}
		catch(UserNotFoundException e){
			outputJson.accumulate("ERROR", "User Not Found");
		}
		catch(Exception e){
			e.printStackTrace();
			outputJson.accumulate("ERROR", "Error While Saving User Preferences");
			log.error("ERROR While Saving User Preferences "+e.toString());
		}
		System.out.println(" output Json "+outputJson.toString());
		return outputJson.toString();
	}
	
	
}
