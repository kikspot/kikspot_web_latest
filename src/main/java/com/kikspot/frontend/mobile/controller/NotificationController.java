package com.kikspot.frontend.mobile.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.NotificationNotFoundException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.notification.dto.NotificationTypeDto;
import com.kikspot.frontend.notification.dto.UserNotificationDto;
import com.kikspot.frontend.notification.service.NotificationService;





/**
 * 
 * 
 * Created by Jeevan on DECEMBER 23, 2015
 * 
 * Controller for Notification;..
 * 
 * 
 * @author KNS-ACCONTS
 *
 */
@RequestMapping("/mobile/")
@RestController("notificationController")
public class NotificationController {

	
	private static Logger log=Logger.getLogger(NotificationController.class);
	
	
	@Resource(name="notificationService")
	private NotificationService notificationService;
	
	
	
	/**
	 * 
	 * 
	 *  Created by Jeevan on DECEMBER 23, 2015
	 *  
	 *  Method to getUserNotificationConfigurations..
	 *  
	 *  
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */	
	@RequestMapping(value="/usernotifications.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getUserNotificationConfigurations(HttpServletRequest request,HttpServletResponse response )throws JSONException{
		log.info("inside getUserNotificationConfigurations() ");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			ArrayList<NotificationTypeDto> notificationTypeDtos=this.notificationService.getNotificationSettingsofUser(userId);
			ArrayList<JSONObject> notificationJsons=new ArrayList<JSONObject>();
			for(NotificationTypeDto notificationTypeDto:notificationTypeDtos){
				
				JSONObject notificationJson=new JSONObject();
				notificationJson.accumulate("notificationId", notificationTypeDto.getNotificationTypeId());
				notificationJson.accumulate("notification", notificationTypeDto.getNotificationType());
				if(null!=notificationTypeDto.getIsEnabled()){
				notificationJson.accumulate("enabled", notificationTypeDto.getIsEnabled());
				}
				else{
					notificationJson.accumulate("enabled",true);
				}
				
				notificationJsons.add(notificationJson);
			}
			json.accumulate("notifications", notificationJsons);
		}
		catch(NotificationNotFoundException e){
			e.printStackTrace();
			log.error("Notification Not Found "+e.toString());
			json.accumulate("ERROR", "No Notifications Found");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Getting Notification Configurations "+e.toString());
			json.accumulate("ERROR", "Error While Getting Notification Configurations ");
		}
		return json.toString();
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 23, 2015
	 * 
	 * Method to changeNotificationConfiguration..
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping("/notificationconfig.do")
	public String changeNotificationConfiguration(HttpServletRequest request, HttpServletResponse response)throws JSONException{
		log.info("inside changeNotificationConfiguration() ");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			Integer notificationId=inputJson.getInt("notificationId");
			Boolean isEnable=inputJson.getBoolean("enable");
			Integer configResult=this.notificationService.enableorDisableUserNotification(userId, notificationId, isEnable);
			
			json.accumulate("STATUS", "success");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error While Changing Notification Configuration "+e.toString());
			json.accumulate("ERROR", "Error While Changing Notification Configuration");
		}		
		return json.toString();
	}
	/**
	 * Created by Firdous on 3rd February 2016
	 * 
	 * Method to get all the notifications of a particular user
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	
	@RequestMapping(value="/notifications.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getAllNotificationsOfUser(HttpServletRequest request,HttpServletResponse response)
	      				throws JSONException{
		 log.info("inside getAllNotificationsOfUser() ");
		 JSONObject outputJson=new JSONObject();
		 try {
		 JSONObject inputJson=JsonParser.getJson(request, response);
		 Integer userId=inputJson.getInt("userId");
		 ArrayList<UserNotificationDto> notificationDtos=this.notificationService.getNotificationsOfUser(userId);
		 ArrayList<JSONObject> kikspotNotificationJSONS=new ArrayList<JSONObject>();
		 Integer unReadNotifications=0;
		 if(!notificationDtos.isEmpty()){
			for(UserNotificationDto userNotification:notificationDtos){
				JSONObject kikspotNotificationJSON=new JSONObject();
				kikspotNotificationJSON.accumulate("NotificationId", userNotification.getUserNotificationId());
			
				if(null!=userNotification.getNotificationMessage()){
					
					kikspotNotificationJSON.accumulate("Notification:",userNotification.getNotificationMessage());
					String formatedNotificationDate=this.notificationDateComparisions(userNotification.getNotificationDate());
					/*SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy hh:mm a");
					sdf.setTimeZone(TimeZone.getTimeZone("EST"));
					kikspotNotificationJSON.accumulate("Date:",sdf.format(userNotification.getNotificationDate()));*/
					// modified by bhagya on may 10th, 2018-> notification date format as per the trello task  
					kikspotNotificationJSON.accumulate("Date:",formatedNotificationDate);
					kikspotNotificationJSON.accumulate("NotificationType",userNotification.getNotificationTypeDto().getNotificationType());
				}
				else{
					kikspotNotificationJSON.accumulate("notifications", "");
				}
				
				if(null!=userNotification.getTriggeredId()){
					kikspotNotificationJSON.accumulate("TriggeredId", userNotification.getTriggeredId());
				}
				else{
					kikspotNotificationJSON.accumulate("TriggeredId","");
				}
				if(null!=userNotification.getIsRead() && userNotification.getIsRead()){
					kikspotNotificationJSON.accumulate("unRead", false);
					
				}
				else{
					unReadNotifications+=1;
					kikspotNotificationJSON.accumulate("unRead", true);
				}
				
				kikspotNotificationJSON.accumulate("TriggeredId","");
				
				    kikspotNotificationJSONS.add(kikspotNotificationJSON);
			 }
             outputJson.accumulate("Notifications", kikspotNotificationJSONS);
             outputJson.accumulate("unReadNotifications", unReadNotifications);
             
             
		  }
		  else{
			throw new NotificationNotFoundException();
		  }
		}
		catch(UserNotFoundException e){
			log.error("User Not Found");
			outputJson.accumulate("ERROR", "User Not Found");
		}
		catch(NotificationNotFoundException e){
			log.error("Notifications  Not Found For User");
			outputJson.accumulate("ERROR", "Notifications Not Found For User");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Notifications of User");
			outputJson.accumulate("ERROR", "Error While Fetching Notifications of User");
		}
		System.out.println(" User Notifications JSON Response : "+outputJson.toString());
		return outputJson.toString();
	}
	
	
	
	/**
	 * 
	 * Created by Jeevan on FEB 16, 2016
	 *  Method to sendNotificationasRead..
	 *  
	 *  
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * 
	 */
	@RequestMapping("/readnotification.do")
	public String setNotificationasRead(HttpServletRequest request, HttpServletResponse response)throws Exception{
		log.info("inside setNotificationasRead() ");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			Integer notificationId=inputJson.getInt("notificationId");
			this.notificationService.setNotificationasRead(userId, notificationId);
			json.accumulate("STATUS", "SUCCESS");
		}
		catch(Exception e){
			e.printStackTrace();
			json.accumulate("STATUS", "SUCCESS");
			log.error("Error While Setting Notification as Read "+e.toString());			
		}		
		return json.toString();
	}
	
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/deletenotification.do")
	public String deleteNotification(HttpServletRequest request, HttpServletResponse response) throws Exception{
		log.info("inside deleteNotification() ");
		JSONObject json=new JSONObject();		
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			Integer notificationId=inputJson.getInt("notificationId");
			this.notificationService.deleteNotification(notificationId);
			json.accumulate("STATUS", "SUCCESS");
		}
		catch(Exception e){
			e.printStackTrace();
			json.accumulate("ERROR", "Error While Deleting Notification");
			log.error("Error While Setting Notification as Read "+e.toString());			
			
		}
		return json.toString();	
	}
	
	
	/**
	 * Added by firdous on 26th May 2016
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * 
	 * Method to get the number of unread notifications of a user
	 */
	@RequestMapping("/getnumberofunreadnotifications.do")
	public String unReadNotificationCount(HttpServletRequest request, HttpServletResponse response) throws Exception{
		log.info("inside deleteNotification() ");
		JSONObject json=new JSONObject();		
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			Integer count=this.notificationService.getNumberOfUnreadNotifications(userId);
			json.accumulate("COUNT",count);
		}
		catch(NotificationNotFoundException e){
			log.error("Notifications  Not Found For User");
			json.accumulate("COUNT",0);
		}
		catch(Exception e){
			e.printStackTrace();
			json.accumulate("ERROR", "Error While Gettig the count");
			log.error("Error While Getting Unread Notification count "+e.toString());			
			
		}
		return json.toString();	
	}
	/**
	 * Created By Bhagya on May 10th, 2018
	 * @param notificationDate
	 * @return
	 * @throws Exception
	 * 
	 * Method for handling the notification date formats
	 */
	
	public String notificationDateComparisions(Date notificationDate) throws Exception{
		String finalNotificationDate="";
			Date today=new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(notificationDate);
			//calendar.setTimeZone(TimeZone.getTimeZone("EST"));
			String ampm="";
			String hours = String.valueOf(calendar.get(Calendar.HOUR));
			if(hours.length()==1){
				hours="0"+hours;
			}
			String minutes = String.valueOf(calendar.get(Calendar.MINUTE));
			if(minutes.length()==1){
				minutes="0"+minutes;
			}
			if(calendar.get(Calendar.AM_PM)==0){
		        ampm="AM";
			}
		    else{
		    	ampm="PM";
		    }
			int seconds = calendar.get(Calendar.SECOND);
			Calendar yesterdayCalendar = Calendar.getInstance();
			yesterdayCalendar.add(Calendar.DATE,-1);
			Calendar afterTwoDaysCalendar = Calendar.getInstance();
			afterTwoDaysCalendar.add(Calendar.DATE,-3);
			Calendar afterOneWeekCalendar = Calendar.getInstance();
			afterOneWeekCalendar.add(Calendar.DATE,-7);
			SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
			//dateFormat.setTimeZone(TimeZone.getTimeZone("EST"));
			String formatNotificationDate=dateFormat.format(notificationDate);
			String formatTodayDate=dateFormat.format(today);
			String formatyesterdayDate=dateFormat.format(yesterdayCalendar.getTime());
			String formatAfterTwoDaysDate=dateFormat.format(afterTwoDaysCalendar.getTime());
			String formatAfterOneWeekDate=dateFormat.format(afterOneWeekCalendar.getTime());
			
			if(dateFormat.parse(formatNotificationDate).compareTo(dateFormat.parse(formatTodayDate))==0){
				
				if(Integer.parseInt(hours)<=12){
					
					//finalNotificationDate=" Hours/Minutes Ago at "+hours+":"+minutes+" "+ampm;
					SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
					//sdf.setTimeZone(TimeZone.getTimeZone("EST"));
					finalNotificationDate=sdf.format(notificationDate);
					//finalNotificationDate=""+hours+":"+minutes+" "+ampm;
				}
			}
			else if(dateFormat.parse(formatNotificationDate).compareTo(dateFormat.parse(formatyesterdayDate))==0){
				SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
				//sdf.setTimeZone(TimeZone.getTimeZone("EST"));
				finalNotificationDate=" Yesterday at "+sdf.format(notificationDate);
				//finalNotificationDate=" Yesterday at "+hours+":"+minutes+" "+ampm;
				
			}
			/*else if(dateFormat.parse(formatNotificationDate).after(dateFormat.parse(formatAfterTwoDaysDate))){
			
				
				finalNotificationDate=" Day of the week at "+hours+":"+minutes+" "+ampm;
			}*/
			else if(dateFormat.parse(formatNotificationDate).after(dateFormat.parse(formatAfterOneWeekDate))){
				
				SimpleDateFormat sdf=new SimpleDateFormat("EEEE hh:mm a");
				//sdf.setTimeZone(TimeZone.getTimeZone("EST"));
				finalNotificationDate=sdf.format(notificationDate);
				
			}
			else{
				//SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy hh:mm a");
				SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
				//sdf.setTimeZone(TimeZone.getTimeZone("EST"));
				finalNotificationDate=sdf.format(notificationDate);
			}
		
		return finalNotificationDate;
	}
}
