package com.kikspot.frontend.mobile.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList

;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.kikspot.backend.exceptions.APIUnavailableException;
import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.LimitedLookUserAccessDeniedException;
import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.backend.exceptions.RecommendationGeoLocationNotMatchedException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.frontend.admin.dto.MasterGeoLocationConfigurationDto;
import com.kikspot.frontend.common.exception.UpdatedAppVersionNotMatchException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.recommendation.dto.RatingAlgorithmDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.UserLocationRatingsDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;



/**
 * 
 *Created By Bhagya On November 23rd,2015
 * Controller class for the recommendation module and consists of mobile services
 * 
 */
@RestController
@RequestMapping("/mobile")
public class RecommendationController{
	private static Logger log=Logger.getLogger(RecommendationController.class);
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="gameService")
	private GameService gameservice;
	
	/**
	 * Created By Bhagya On November 23rd,2015
	 * @param request
	 * @param response
	 * @return
	 * Method For Returning the Basic Details Of Near By Places Based On Latitude and Longitude
	 * @throws JSONException 
	 */
	/**
	 * Modified the app version logic by bhagya on jan 28th, 2020 for to force the users to update the kikspot app
	 */
	@RequestMapping(value="/getrecommendationlocations.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
	public String getlocationsByLatAndLng(HttpServletRequest request,HttpServletResponse response,Map<String,Object> map) throws JSONException{
		log.info("inside  RecommendationController->getLocationsByLatAndLng()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			Double latitude=json.getDouble("latitude");
			Double longitude=json.getDouble("longitude");
			String pageToken=json.getString("pageToken");
			Integer requestFilter=json.getInt("requestFilter");
			
			String sortBy=null;
			if(null!=json.get("sortBy")){
				sortBy=json.getString("sortBy");
			}
			
			String searchString=null;
			try{
				if(null!=json.get("searchString")){
					searchString=json.getString("searchString");
				}
			}
						catch(Exception e){}
			System.out.println("SORT BY "+sortBy +" userId "+userId +" latitude "+latitude +" longitude "+longitude +" pageToken "+pageToken +" requestFilter "+requestFilter +" searchString "+searchString);
			/**
			 * Added By Bhagya on may 21st, 2019
			 * Updating the master geo location for user, if the user have mastergeolocated
			 */
			KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
			ArrayList<JSONObject> recommendationLocationJSONS=new ArrayList<JSONObject>();
			if(null!=kikspotUserDto.getAppVersion()&& kikspotUserDto.getAppVersion().length()>0){
				if(kikspotUserDto.getIsMasterGeoLocation()){
					MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
					latitude=masterGeoLocationConfigurationDto.getLatitude();
					longitude=masterGeoLocationConfigurationDto.getLongitude();
				}
				log.info(" User Latitude "+latitude +" longitude "+longitude);
				ArrayList<RecommendationLocationDto> recommendationLocationDtos=this.recommendationService.getLocations(userId, latitude, longitude,pageToken,sortBy,searchString,requestFilter);
				
				if(recommendationLocationDtos.size()>0 ){
						System.out.println(" Size at controller "+recommendationLocationDtos.size());
						
						for(RecommendationLocationDto recommendationLocationDto:recommendationLocationDtos){
							System.out.println(" Location NAme "+recommendationLocationDto.getLocationName() +" LocationId"+recommendationLocationDto.getLocationAPIId() +" distance "+recommendationLocationDto.getDistance()+" Miles");
							JSONObject recommendationLocationJSON=new JSONObject();
							recommendationLocationJSON.accumulate("locationAPIId", recommendationLocationDto.getLocationAPIId());
							recommendationLocationJSON.accumulate("locationName", recommendationLocationDto.getLocationName());
							recommendationLocationJSON.accumulate("latitude", recommendationLocationDto.getLatitude());
							recommendationLocationJSON.accumulate("longitude", recommendationLocationDto.getLongitude());
							if(null!=recommendationLocationDto.getImage() && recommendationLocationDto.getImage().trim().length()>0){
								recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getImage());
							}
							else{
								recommendationLocationJSON.accumulate("iconUrl","");
							}
							//recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getIconUrl());
							recommendationLocationJSON.accumulate("distance", recommendationLocationDto.getDistance()+" Miles");
							
							if(Double.isNaN(recommendationLocationDto.getRatings())){
								recommendationLocationJSON.accumulate("rating", 7);
							}
							else{
								recommendationLocationJSON.accumulate("rating", recommendationLocationDto.getRatings());
							}
							recommendationLocationJSON.accumulate("isNew", recommendationLocationDto.getIsNew());
							if(recommendationLocationDto.getRatings()>=6){
								recommendationLocationJSON.accumulate("trending", "up");
							}
							else{
								recommendationLocationJSON.accumulate("trending", "down");
							}
							//recommendationLocationJSON.accumulate("trending", recommendationLocationDto.getTrending());
							/*if(null!=recommendationLocationDto.getImage() && recommendationLocationDto.getImage().trim().length()>0 && null!=recommendationLocationDto.getIconUrl()){
								recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getImage());
							}
							else{
								recommendationLocationJSON.accumulate("iconUrl","");
							}*/
							recommendationLocationJSON.accumulate("hasActiveRules",recommendationLocationDto.getHasActiveRules());
							
							//recommendationLocationJSON.accumulate("hasActiveRewards",recommendationLocationDto.getHasActiveRewards());
							recommendationLocationJSON.accumulate("address", recommendationLocationDto.getAddress());
							Integer ratingsCount=this.recommendationService.getUserRatingByLocationId(recommendationLocationDto.getLocationAPIId());
							recommendationLocationJSON.accumulate("rates", ratingsCount);
							
							if(ratingsCount>0){
								recommendationLocationJSON.accumulate("newlyAdded", false);
							}
							else{
								recommendationLocationJSON.accumulate("newlyAdded", true);
							}
							recommendationLocationJSONS.add(recommendationLocationJSON);
						}
				
					output.accumulate("recommendationLocations", recommendationLocationJSONS);
					output.accumulate("pageToken", recommendationLocationDtos.get(0).getPageToken());
					System.out.println(" pageToken "+recommendationLocationDtos.get(0).getPageToken());
				}
				else{
					throw new KikspotRecommendationLocationNotFoundException();
				}
			}
			else{
				output.accumulate("recommendationLocations", recommendationLocationJSONS);
				throw new UpdatedAppVersionNotMatchException();	
				
			}
		
					
		}
		
		catch(APIUnavailableException e){
			String message="Problem while getting locations,please try again";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());		
		}
		catch(NoResultsFoundException e){
			//e.printStackTrace();
			String message="No Results Found For Search Criteria";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());			
		}
		catch(UserLocationRatingsNotFoundException e){
			String message="User Location Comments or Images Not Found For Requested Location";
			output.accumulate("ERROR", message);
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			String message="User Locations Not Found Based on Admin Recommendation Configuration Settings";
			output.accumulate("ERROR", message);
		}
		catch(UpdatedAppVersionNotMatchException e){
			log.error("Updated App Version Does Not match "+e.toString());
			output.accumulate("ERROR", "There is a newer version of this app available! Please update the version from App Store"); 
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting Recommendation Locations";
			output.accumulate("ERROR", message);			
		}
		log.info(" Recommendation Locations Json Reposne  "+output.toString());
		System.out.println(" Json Response"+output.toString());	
		return output.toString();
	
	}
	
		
	@RequestMapping(value="/refreshlocations.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
	public String refereshLocations(HttpServletRequest request,HttpServletResponse response,Map<String,Object> map) throws JSONException{
		log.info("inside  RecommendationController->getLocationByLatAndLng()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			Double latitude=json.getDouble("latitude");
			Double longitude=json.getDouble("longitude");
			String pageToken=json.getString("pageToken");
			Integer requestFilter=json.getInt("requestFilter");
			String sortBy=null;
			if(null!=json.get("sortBy")){
				sortBy=json.getString("sortBy");
			}
			String searchString=null;
			try{
				if(null!=json.get("searchString")){
					searchString=json.getString("searchString");
				}
			}
			catch(Exception e){}
			try{
				this.recommendationService.refreshLocationsofUser(userId, sortBy);
			}
			catch(Exception e){}
			/**
			 * Added By Bhagya on may 21st, 2019
			 * Updating the master geo location for user, if the user have mastergeolocated
			 */
			KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
			if(kikspotUserDto.getIsMasterGeoLocation()){
				MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
				latitude=masterGeoLocationConfigurationDto.getLatitude();
				longitude=masterGeoLocationConfigurationDto.getLongitude();
			}
			ArrayList<RecommendationLocationDto> recommendationLocationDtos=this.recommendationService.getLocations(userId, latitude, longitude,pageToken,sortBy,searchString,requestFilter);
			if(recommendationLocationDtos.size()>0 ){
					ArrayList<JSONObject> recommendationLocationJSONS=new ArrayList<JSONObject>();
					for(RecommendationLocationDto recommendationLocationDto:recommendationLocationDtos){
						JSONObject recommendationLocationJSON=new JSONObject();
						recommendationLocationJSON.accumulate("locationAPIId", recommendationLocationDto.getLocationAPIId());
						recommendationLocationJSON.accumulate("locationName", recommendationLocationDto.getLocationName());
						recommendationLocationJSON.accumulate("latitude", recommendationLocationDto.getLatitude());
						recommendationLocationJSON.accumulate("longitude", recommendationLocationDto.getLongitude());
						//recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getIconUrl());
						recommendationLocationJSON.accumulate("distance", recommendationLocationDto.getDistance()+" Miles");
						if(Double.isNaN(recommendationLocationDto.getRatings())){
							recommendationLocationJSON.accumulate("rating", 7);
						}
						else{
							recommendationLocationJSON.accumulate("rating", recommendationLocationDto.getRatings());
						}
						recommendationLocationJSON.accumulate("isNew", recommendationLocationDto.getIsNew());
						if(recommendationLocationDto.getRatings()>6){
							recommendationLocationJSON.accumulate("trending", "up");
						}
						else{
							recommendationLocationJSON.accumulate("trending", "down");
						}
						if(null!=recommendationLocationDto.getImage() && recommendationLocationDto.getImage().trim().length()>0){
							recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getImage());
						}
						else{
							recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getIconUrl());
						}
						recommendationLocationJSON.accumulate("hasActiveRules",recommendationLocationDto.getHasActiveRules());
						//recommendationLocationJSON.accumulate("hasActiveRewards",recommendationLocationDto.getHasActiveRewards());
						recommendationLocationJSON.accumulate("address", recommendationLocationDto.getAddress());
						Integer ratingsCount=this.recommendationService.getUserRatingByLocationId(recommendationLocationDto.getLocationAPIId());
						recommendationLocationJSON.accumulate("rates", ratingsCount);
						if(ratingsCount>0){
							recommendationLocationJSON.accumulate("newlyAdded", false);
						}
						else{
							recommendationLocationJSON.accumulate("newlyAdded", true);
						}
						recommendationLocationJSONS.add(recommendationLocationJSON);
					}
					
					output.accumulate("recommendationLocations", recommendationLocationJSONS);
					output.accumulate("pageToken", recommendationLocationDtos.get(0).getPageToken());
			}
			else{
				throw new KikspotRecommendationLocationNotFoundException();
			}
						
		}
		catch(APIUnavailableException e){
			String message="Problem while getting locations,please try again";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());		
		}
		catch(NoResultsFoundException e){
			//e.printStackTrace();
			String message="No Results Found For Search Criteria";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());			
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			String message="User Locations Not Found Based on Admin Recommendation Configuration Settings";
			output.accumulate("ERROR", message);
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting Recommendation Locations";
			output.accumulate("ERROR", message);			
		}
		System.out.println(" Json Response"+output.toString());
		return output.toString();
	}
	
	
	
	
	/**
	 * Created By Bhagya On november 25th,2015
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 * 
	 * Method For getting the Location Details By Location APIId
	 * @throws JSONException 
	 */
	@RequestMapping(value="/getrecommendationlocationdetails.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
	public String getDetailsOfLocationByLocationAPIId(HttpServletRequest request,HttpServletResponse response,Map<String,Object> map) throws JSONException{
		log.info("inside RecommendationController->getDetailsOfLocationByLocationAPIId()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			String locationAPIId=json.getString("locationAPIId");
			Double latitude=json.getDouble("latitude");
			Double longitude=json.getDouble("longitude");
			Integer userId=json.getInt("userId");
			/**
			 * Added By Bhagya on may 21st, 2019
			 * Updating the master geo location for user, if the user have mastergeolocated
			 */
			KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
			if(kikspotUserDto.getIsMasterGeoLocation()){
				MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
				latitude=masterGeoLocationConfigurationDto.getLatitude();
				longitude=masterGeoLocationConfigurationDto.getLongitude();
			}
			RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(locationAPIId,latitude,longitude,userId);
			JSONObject recommendationLocationJSON=new JSONObject();
			recommendationLocationJSON.accumulate("locationAPIId", recommendationLocationDto.getLocationAPIId());
			recommendationLocationJSON.accumulate("locationName", recommendationLocationDto.getLocationName());
			recommendationLocationJSON.accumulate("latitude", recommendationLocationDto.getLatitude());
			recommendationLocationJSON.accumulate("longitude", recommendationLocationDto.getLongitude());
			recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getIconUrl());
			recommendationLocationJSON.accumulate("image", recommendationLocationDto.getImage());
			
			recommendationLocationJSON.accumulate("ratings", recommendationLocationDto.getRatings());
			recommendationLocationJSON.accumulate("address", recommendationLocationDto.getAddress());
			recommendationLocationJSON.accumulate("phoneNo", recommendationLocationDto.getPhoneNo());
			recommendationLocationJSON.accumulate("isNew", recommendationLocationDto.getIsNew());
			recommendationLocationJSON.accumulate("url", recommendationLocationDto.getUrl());
			recommendationLocationJSON.accumulate("distance", recommendationLocationDto.getDistance()+" Miles");
			Integer ratingsCount=this.recommendationService.getUserRatingByLocationId(recommendationLocationDto.getLocationAPIId());
			recommendationLocationJSON.accumulate("rates", ratingsCount);
			if(ratingsCount>0){
				recommendationLocationJSON.accumulate("newlyAdded", false);
			}
			else{
				recommendationLocationJSON.accumulate("newlyAdded", true);
			}
			output.accumulate("recommendationLocationDetails", recommendationLocationJSON);
			/*ArrayList<JSONObject> userLocationRewardsJSON=new ArrayList<JSONObject>();
			if(null!=recommendationLocationDto.getKikspotRewardsDto()){
				for(KikspotRewardsDto kikspotRewardsDto:recommendationLocationDto.getKikspotRewardsDto()){
					JSONObject userLocationRewardJSON=new JSONObject();
					userLocationRewardJSON.accumulate("reward value", kikspotRewardsDto.getRewardValue());
					userLocationRewardJSON.accumulate("redeem creds", kikspotRewardsDto.getCreds());
					userLocationRewardsJSON.add(userLocationRewardJSON);
				}
			}
			output.accumulate("rewadsDetails", userLocationRewardsJSON);*/
			// Modified by bhagya on Jan 08th, 2019 -- based on client requirement .. instead of sending rewards, they want send us the game rules .. treating the game rules as rewards
			ArrayList<JSONObject> userLocationRulesJSON=new ArrayList<JSONObject>();
			
			if(null!=recommendationLocationDto.getGameRulesDto()){
				for(GameRulesDto gameRulesDto:recommendationLocationDto.getGameRulesDto()){
					JSONObject userLocationRuleJSON=new JSONObject();
					userLocationRuleJSON.accumulate("Game Rule", gameRulesDto.getRule());
					userLocationRuleJSON.accumulate("Creds", gameRulesDto.getCreds());
					userLocationRulesJSON.add(userLocationRuleJSON);
				}
			}
			output.accumulate("rulesDetails", userLocationRulesJSON);
			ArrayList<UserLocationRatingsDto> userLocationRatingsDtos=this.recommendationService.getLocationCommentsAndLocationImagesByLocationAPIId(locationAPIId);
			ArrayList<JSONObject> userLocationRatingJSONS=new ArrayList<JSONObject>();
			for(UserLocationRatingsDto userLocationRatingDto:userLocationRatingsDtos){
				JSONObject userLocationRatingJSON=new JSONObject();
				userLocationRatingJSON.accumulate("rating", userLocationRatingDto.getRating());
				Date recentRatedDate=userLocationRatingDto.getRatingDate();
				SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyy hh:mm aa");
				//format.setTimeZone(TimeZone.getTimeZone("EST"));
				userLocationRatingJSON.accumulate("ratingDate", format.format(recentRatedDate));
				if(null!=userLocationRatingDto.getComment()){
					userLocationRatingJSON.accumulate("comment", userLocationRatingDto.getComment());
				}
				else{
					userLocationRatingJSON.accumulate("comment", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getUsername()){
					userLocationRatingJSON.accumulate("username", userLocationRatingDto.getKikspotUserDto().getUsername());
				}
				else{
					userLocationRatingJSON.accumulate("username", "");
				}
				//Modified to USername as per shruti's request/.. 
				if(null!=userLocationRatingDto.getKikspotUserDto().getFirstName()){
					userLocationRatingJSON.accumulate("firstName", userLocationRatingDto.getKikspotUserDto().getUsername());
				}
				else{
					userLocationRatingJSON.accumulate("firstName", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getLastName()){
					userLocationRatingJSON.accumulate("lastName", "");
				}
				else{
					userLocationRatingJSON.accumulate("lastName", "");
				}
				if(null!=userLocationRatingDto.getImage()){
					userLocationRatingJSON.accumulate("image", userLocationRatingDto.getImage());
				}
				else{
					userLocationRatingJSON.accumulate("image", "");
				}
				
				userLocationRatingJSONS.add(userLocationRatingJSON);
			}
			
			output.accumulate("locationComments", userLocationRatingJSONS);
			
			System.out.println("Location Details Json Response"+output.toString());
			log.info("Recommendation Location Details Json Response"+output.toString());
		}
		catch(APIUnavailableException e){
			String message="Problem while getting locations,please try again";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());		
		}
		catch(NoResultsFoundException e){
			//e.printStackTrace();
			String message="No Results Found For Search Criteria";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());
			
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting  Location Details";
			output.accumulate("ERROR", message);			
		}
		return output.toString();
	}
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on December 07, 2015
	 *  
	 *  Method to removeLocationFromUserList..
	 *  
	 * 
	 * 
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * 
	 * 
	 */
	@RequestMapping(value="/removerecommendation.do")
	public String removeLocationFromUserList(HttpServletRequest request,HttpServletResponse response)throws JSONException{
		log.info("inside removeLocationFromUserList() ");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			String locationAPIId=json.getString("locationAPIId");
			Integer userId=json.getInt("userId");
			String sortBy=json.getString("sortBy");
			this.recommendationService.removeLocationFromUserList(userId, locationAPIId,sortBy);
			outputJson.accumulate("STATUS", "SUCCESS");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("ERROR WHILE SAVING USER REMOVED LOCATION "+e.getMessage());
			outputJson.accumulate("ERROR", "Error While Removing Location from User Recommendation List");
		}		
		System.out.println(outputJson.toString());
		return outputJson.toString();
	}
	
	
	
	
	
	
	/**
	 * 
	 *  
	 *  Created by Jeevan on December 09, 2015
	 *  
	 *  Method to rateUserLocations..
	 *  
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 *  May needed to be changed, if Pics are included in Rating..
	 *  
	 *  Modified By Bhagya On Jan 08th,2016
	 *  	Changed to only save the User Location Rating (without updation)
	 *  
	 */
	@RequestMapping(value="/ratelocation.do")
	public String rateUserLocation(HttpServletRequest request, HttpServletResponse response,@RequestBody UserLocationRatingsDto userLocationRatingsDto)throws JSONException{
		log.info("inside rateUserLocation()");
		JSONObject outputJson=new JSONObject();
		
		try{
			if(this.userService.isLoggedInUser(userLocationRatingsDto.getUserId())){
				//Integer userLocationRatingsSavedResult=this.recommendationService.saveUserLocationRatingtoDB(userLocationRatingsDto);
				Integer userLocationRatingsSavedResult=this.recommendationService.checkLocationTogglerConditionAndSaveRateALocation(userLocationRatingsDto);
				if(userLocationRatingsSavedResult>0){
					outputJson.accumulate("STATUS", "success");
					outputJson.accumulate("userLocationRatingId",userLocationRatingsSavedResult);
			}
				else
					throw new Exception();
			}
		
		}
		catch(LimitedLookUserAccessDeniedException e){
			//e.printStackTrace();
			log.error("LIMITED LOOK USER CANNOT RATE A LOCATION "+e.toString());
			outputJson.accumulate("ERROR", "You Dont Have Access to Rate a Location, Please Signup to Continue.");
		}
		catch(RecommendationGeoLocationNotMatchedException e){
			//e.printStackTrace();
			log.error("Recommendation Location Not Matches with Current Location, Cant Rate "+e.toString());
			outputJson.accumulate("ERROR", "No cheating! Can't rate unless you're there!");
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Saving Location Ratings";
			log.error(message+" "+e.toString());
			outputJson.accumulate("ERROR", message);
		}		
		System.out.println(outputJson.toString());
		return outputJson.toString();
	}
	
	
	
	/***
	 * Created By Bhagya On Jan 05th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For getting the User Most recent Location RAting By UserId and Location APIId
	 */
	
	@RequestMapping(value="/getuserlocationrating.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getUserLocationRating(HttpServletRequest request, HttpServletResponse response) throws JSONException{
		log.info("inside getUserLocationRating()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			String locationAPIId=json.getString("locationAPIId");
			UserLocationRatingsDto userLocationRatingsDto=this.recommendationService.getMostRecentUserLocationRatingByUserIdAndLocationId(userId, locationAPIId);
			JSONObject userLocationRatingJson=new JSONObject();
			userLocationRatingJson.accumulate("rating", userLocationRatingsDto.getRating());
			if(null!=userLocationRatingsDto.getComment()){
				userLocationRatingJson.accumulate("comment", userLocationRatingsDto.getComment());
			}
			else{
				userLocationRatingJson.accumulate("comment", "");
			}
			Date recentRatedDate=userLocationRatingsDto.getRatingDate();
			SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyy hh:mm aa");
			//format.setTimeZone(TimeZone.getTimeZone("EST"));
			userLocationRatingJson.accumulate("recentRatingDate", format.format(recentRatedDate));
			
			output.accumulate("userRecentLocationRating", userLocationRatingJson);
			System.out.println(" Json Response"+output.toString());	
		}
		catch(UserNotFoundException e){
			String message="User Does Not Exist";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());
		}
		catch(UserLocationRatingsNotFoundException e){
			String message="User Location Rating Not Found";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting User Location Ratings";
			log.error(message+" "+e.toString());
			output.accumulate("ERROR", message);
		}
		return output.toString();
	}
	
	
	
	
	
	
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For getting the Location Comments and logged in user most recent rating date
	 * Steps: Getting the list of comments of a location 
	 * 		  Get the Most Recent Rating of logged in user
	 * 		  Return the comments and most recent rating date and time of logged in user
	 */
	
	@RequestMapping(value="/getlocationcomments.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
	public String getLocationCommentsofUsers(HttpServletRequest request, HttpServletResponse response) throws JSONException{
		log.info("inside getLocationCommentsofUsers()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			String locationAPIId=json.getString("locationAPIId");
			Integer userId=json.getInt("userId");
			ArrayList<UserLocationRatingsDto> userLocationRatingsDtos=this.recommendationService.getLocationCommentsByLocationAPIId(locationAPIId,userId);
			ArrayList<JSONObject> userLocationRatingJSONS=new ArrayList<JSONObject>();
			for(UserLocationRatingsDto userLocationRatingDto:userLocationRatingsDtos){
				JSONObject userLocationRatingJSON=new JSONObject();
				userLocationRatingJSON.accumulate("rating", userLocationRatingDto.getRating());
				Date recentRatedDate=userLocationRatingDto.getRatingDate();
				SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyy hh:mm aa");
				//format.setTimeZone(TimeZone.getTimeZone("EST"));
				userLocationRatingJSON.accumulate("ratingDate", format.format(recentRatedDate));
				if(null!=userLocationRatingDto.getComment()){
					userLocationRatingJSON.accumulate("comment", userLocationRatingDto.getComment());
				}
				else{
					userLocationRatingJSON.accumulate("comment", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getUsername()){
					userLocationRatingJSON.accumulate("username", userLocationRatingDto.getKikspotUserDto().getUsername());
				}
				else{
					userLocationRatingJSON.accumulate("username", "");
				}
				//Modified to USername as per shruti's request/.. 
				if(null!=userLocationRatingDto.getKikspotUserDto().getFirstName()){
					userLocationRatingJSON.accumulate("firstName", userLocationRatingDto.getKikspotUserDto().getUsername());
				}
				else{
					userLocationRatingJSON.accumulate("firstName", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getLastName()){
					userLocationRatingJSON.accumulate("lastName", "");
				}
				else{
					userLocationRatingJSON.accumulate("lastName", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getCity()){
					userLocationRatingJSON.accumulate("city", userLocationRatingDto.getKikspotUserDto().getCity());
				}
				else{
					userLocationRatingJSON.accumulate("city", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getState()){
					userLocationRatingJSON.accumulate("state", userLocationRatingDto.getKikspotUserDto().getState());
				}
				else{
					userLocationRatingJSON.accumulate("state", "");
				}
				
				userLocationRatingJSONS.add(userLocationRatingJSON);
			}
			//getting mostRecent location rating of the user
			try{
			UserLocationRatingsDto mostRecentLocationRatingsDto=this.recommendationService.getMostRecentUserLocationRatingByUserIdAndLocationId(userId, locationAPIId);
			Date recentRatingDate=mostRecentLocationRatingsDto.getRatingDate();
			SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			//format.setTimeZone(TimeZone.getTimeZone("EST"));
			output.accumulate("recentRatingDate", format.format(recentRatingDate));
			}
			catch(UserLocationRatingsNotFoundException e){
				output.accumulate("recentRatingDate", "");
			}
			output.accumulate("locationComments", userLocationRatingJSONS);
		}
		catch(UserLocationRatingsNotFoundException e){
			String message="User Location Ratings Not Found For Requested Location";
			output.accumulate("ERROR", message);
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting User Location Ratings For Requested Location";
			log.error(message+" "+e.toString());
			output.accumulate("ERROR", message);
		}
		System.out.println("JSON Response"+output.toString());
		return output.toString();
	}
	
	
	
	/**
	 * Created By Bhagya On Jan 08th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for adding the User recommendation Location Images
	 */
	@RequestMapping("/adduserlocationimages.do")
	public String addUserRecommendationLocationImages(HttpServletRequest request, HttpServletResponse response) throws JSONException{
		log.info("inside addUserRecommendationLocationImages()");
		JSONObject outputJSON=new JSONObject();
		try{
			 JSONObject inputJson=new JSONObject(request.getParameter("json"));
			 Integer userId=inputJson.getInt("userId");
			 Integer userLocationRatingId=inputJson.getInt("userLocationRatingId");
			 String locationId=inputJson.getString("locationAPIId");			 
			 Double latitude=38.9308349462589; Double longitude=-122.0312186;
			 
			 try{
				 latitude=inputJson.getDouble("latitude");
				 longitude=inputJson.getDouble("longitude");
			 }
			 catch(Exception e){
				 
			 }
			 /**
			  * Added By Bhagya on May21st, 2019
			  * Method for to get the master geo location coordinates if the user is masterGeoLocated
			  */
			 KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
				if(kikspotUserDto.getIsMasterGeoLocation()){
					MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
					latitude=masterGeoLocationConfigurationDto.getLatitude();
					longitude=masterGeoLocationConfigurationDto.getLongitude();
				}
			 MultipartHttpServletRequest multiRequest= (MultipartHttpServletRequest) request;
			 List<MultipartFile> imageFiles=multiRequest.getFiles("images");
			 Integer savedResult=this.recommendationService.saveUserRecommendationLocationImages(userId, locationId, imageFiles,latitude,longitude,userLocationRatingId);
			 if(savedResult>0){
				 outputJSON.accumulate("STATUS", "User Recommendation Location Images Added Successfully");
			 }
			 else{
				 throw new Exception();
			 }
		}
		catch(UserNotFoundException e){
			String message="User Does Not Exist For Adding Location Images";
			outputJSON.accumulate("ERROR", message);
		}
		catch(RecommendationGeoLocationNotMatchedException e){
			//e.printStackTrace();
			log.error("Recommendation Location Not Matches with Current Location, Cant Rate "+e.toString());
			outputJSON.accumulate("ERROR", "No cheating! Can't rate unless you're there!");
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Saving User Recommendation Location Images";
			log.error(message+" "+e.toString());
			outputJSON.accumulate("ERROR", message);
		}
		System.out.println(" JSON Response "+outputJSON.toString());
		return outputJSON.toString();
	}
	
	/*@RequestMapping("/updatecredsforsharing.do")
	public String addCredsForSharing(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside addCredsForSharing");
		JSONObject outputJSON=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			String type=json.getString("type");
			try{
			GameRules gameRule=this.gameservice.getGameRuleByActivityType(type);
			if(gameRule!=null){
			 UserGameCredsDto userGameCreds=this.gameservice.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(), userId, null);
			 JSONObject userGameCredsJSON=new JSONObject();
				userGameCredsJSON.accumulate("earnedCreds",gameRule.getCreds());
				userGameCredsJSON.accumulate("level", userGameCreds.getLevel());
				outputJSON.accumulate("userGameCreds", userGameCredsJSON);
			}
			}
			catch(GameRuleNotFoundException e){
				log.error("game rule not found");
				String message=e.toString();
				outputJSON.accumulate("ERROR",message);
				e.printStackTrace();
			}
			catch(GameRuleExpiredException e){
				log.error("game rule expired");
				String message=e.toString();
				outputJSON.accumulate("ERROR",message);
				e.printStackTrace();
			}
			catch(GameRuleExceedsRuleLimitException e){
				log.error("game rule limitt exceeds");
				String message=e.toString();
				outputJSON.accumulate("ERROR",message);
				e.printStackTrace();
			}
		}
		
	
		catch(Exception e){
			log.error("some error occured");
			String message="Error occured while updating the creds";
			outputJSON.accumulate("ERROR",message);
			e.printStackTrace();
			
		}
		System.out.println(" JSON Response "+outputJSON.toString());
		return  outputJSON.toString();
	}*/
	
	@RequestMapping("/updatecredsforsharing.do")
	public String addCredsForSharing(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside addCredsForSharing");
		JSONObject outputJSON=new JSONObject();
		
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			String type=json.getString("type");
			try{
			Map<String, String> resultMap=this.gameservice.updateGameCredsForShareFunctions(userId, type);
			 	JSONObject userGameCredsJSON=new JSONObject();
				userGameCredsJSON.accumulate("earnedCreds",Integer.parseInt(resultMap.get("earnedCreds")));
				userGameCredsJSON.accumulate("level", resultMap.get("gameLevel"));
				outputJSON.accumulate("userGameCreds", userGameCredsJSON);
			
			}
			catch(GameRuleNotFoundException e){
				log.error("game rule not found");
				String message=e.toString();
				outputJSON.accumulate("ERROR",message);
				//e.printStackTrace();
			}
			catch(GameRuleExpiredException e){
				log.error("game rule expired");
				String message=e.toString();
				outputJSON.accumulate("ERROR",message);
				//e.printStackTrace();
			}
			catch(GameRuleExceedsRuleLimitException e){
				log.error("game rule limit exceeds");
				String message=e.toString();
				outputJSON.accumulate("ERROR",message);
				//e.printStackTrace();
			}
		}
		
	
		catch(Exception e){
			log.error("some error occured");
			String message="Error occured while updating the creds";
			outputJSON.accumulate("ERROR",message);
			e.printStackTrace();
			
		}
		System.out.println(" JSON Response "+outputJSON.toString());
		return  outputJSON.toString();
	}
	
	/*@RequestMapping("/updatecredsforsharingcomments.do")
	public String addCredsForSharingLocationComments(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside  addCredsForSharingLocationComments()");
		JSONObject outputJSON=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			GameRules gameRule=this.gameservice.getGameRuleByActivityType("share comments");
			if(gameRule!=null)	{
				UserGameCredsDto userGameCreds=this.gameservice.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(), userId, null);
				JSONObject userGameCredsJSON=new JSONObject();
				userGameCredsJSON.accumulate("earnedCreds", userGameCreds.getCreds());
				userGameCredsJSON.accumulate("level", userGameCreds.getLevel());
				outputJSON.accumulate("userGameCreds", userGameCredsJSON);
			}
		}
		catch(GameRuleNotFoundException e){
			log.error("game rule not found");
			e.printStackTrace();
			
		}
		catch(GameRuleExpiredException e){
			log.error("game rule expired");
			e.printStackTrace();
			
		}
		catch(GameRuleExceedsRuleLimitException e){
			log.error("game rule expired");
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		return  outputJSON.toString();
	}*/
	
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For getting the Location Comments and logged in user most recent rating date
	 * Steps: Getting the list of comments of a location 
	 * 		  Get the Most Recent Rating of logged in user
	 * 		  Return the comments and most recent rating date and time of logged in user
	 *//*
	
	@RequestMapping("/getlocationcommentsandimages.do")
	public String getLocationCommentsAndLocationImagesofUsers(HttpServletRequest request, HttpServletResponse response) throws JSONException{
		log.info("inside getLocationCommentsofUsers()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			String locationAPIId=json.getString("locationAPIId");
			ArrayList<UserLocationRatingsDto> userLocationRatingsDtos=this.recommendationService.getLocationCommentsAndLocationImagesByLocationAPIId(locationAPIId);
			ArrayList<JSONObject> userLocationRatingJSONS=new ArrayList<JSONObject>();
			for(UserLocationRatingsDto userLocationRatingDto:userLocationRatingsDtos){
				JSONObject userLocationRatingJSON=new JSONObject();
				userLocationRatingJSON.accumulate("rating", userLocationRatingDto.getRating());
				Date recentRatedDate=userLocationRatingDto.getRatingDate();
				SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyy hh:mm aa");
				//format.setTimeZone(TimeZone.getTimeZone("EST"));
				userLocationRatingJSON.accumulate("ratingDate", format.format(recentRatedDate));
				if(null!=userLocationRatingDto.getComment()){
					userLocationRatingJSON.accumulate("comment", userLocationRatingDto.getComment());
				}
				else{
					userLocationRatingJSON.accumulate("comment", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getUsername()){
					userLocationRatingJSON.accumulate("username", userLocationRatingDto.getKikspotUserDto().getUsername());
				}
				else{
					userLocationRatingJSON.accumulate("username", "");
				}
				//Modified to USername as per shruti's request/.. 
				if(null!=userLocationRatingDto.getKikspotUserDto().getFirstName()){
					userLocationRatingJSON.accumulate("firstName", userLocationRatingDto.getKikspotUserDto().getUsername());
				}
				else{
					userLocationRatingJSON.accumulate("firstName", "");
				}
				if(null!=userLocationRatingDto.getKikspotUserDto().getLastName()){
					userLocationRatingJSON.accumulate("lastName", "");
				}
				else{
					userLocationRatingJSON.accumulate("lastName", "");
				}
				if(null!=userLocationRatingDto.getImage()){
					userLocationRatingJSON.accumulate("image", userLocationRatingDto.getImage());
				}
				else{
					userLocationRatingJSON.accumulate("image", "");
				}
				
				userLocationRatingJSONS.add(userLocationRatingJSON);
			}
		
			output.accumulate("locationComments", userLocationRatingJSONS);
		}
		catch(UserLocationRatingsNotFoundException e){
			String message="User Location Comments or Images Not Found For Requested Location";
			output.accumulate("ERROR", message);
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting User Location Comments or Images For Requested Location";
			log.error(message+" "+e.toString());
			output.accumulate("ERROR", message);
		}
		System.out.println("JSON Response"+output.toString());
		return output.toString();
	}*/
	
}