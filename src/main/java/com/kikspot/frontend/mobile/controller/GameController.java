package com.kikspot.frontend.mobile.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//import org.apache.catalina.filters.ExpiresFilter;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleInActiveException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.GamesNotFoundException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.exceptions.UserLocationNotFoundException;
import com.kikspot.frontend.admin.dto.MasterGeoLocationConfigurationDto;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.game.dto.GameDto;
import com.kikspot.frontend.game.dto.GameLevelsDto;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.CredService;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.recommendation.service.RecommendationService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;


/***
 * 
 * 
 *  Created by Jeevan on November 27, 2015
 *  Controller for Game...
 * 
 * @author KNS-ACCONTS
 *
 */


@RestController
@RequestMapping("/mobile")
public class GameController {

	
	private static Logger log=Logger.getLogger(GameController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="gameService")
	private GameService gameService;
	
	@Resource(name="credService")
	private CredService credService;
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	/**
	 * Created by Jeevan on November 27, 2015
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping(value="/dashboard.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getUserDashboard(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getUserDashboard()");
		System.out.println("getting user dash board info");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			double latitude=inputJson.getDouble("latitude");
			double longitude=inputJson.getDouble("longitude");
			 /**
			  * Added By Bhagya on May21st, 2019
			  * Method for to get the master geo location coordinates if the user is masterGeoLocated
			  */
			 KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
				if(kikspotUserDto.getIsMasterGeoLocation()){
					MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
					latitude=masterGeoLocationConfigurationDto.getLatitude();
					longitude=masterGeoLocationConfigurationDto.getLongitude();
				}
			UserDashboardDto userDashboardDto=this.gameService.getUserDashboardInformation(userId,latitude,longitude);
			if(userDashboardDto.getKikspotUserDto().getUserType().equalsIgnoreCase("BYPASS_USER")){
				json.accumulate("USER","GUEST");
			}
			else{
				json.accumulate("USER",userDashboardDto.getKikspotUserDto().getUsername().toUpperCase());
			}
			json.accumulate("CREDS", userDashboardDto.getUserGameCredDto().getCreds());
			json.accumulate("TRENDING", userDashboardDto.getTrending());
			json.accumulate("LEVEL", userDashboardDto.getUserGameCredDto().getLevel());
			ArrayList<JSONObject> topCredUsers=new ArrayList<JSONObject>();
			int i=1;
			for(UserGameCredsDto userGameCredDto:userDashboardDto.getUserGameCredsDtos()){
				JSONObject topCredUser=new JSONObject();
				topCredUser.accumulate("POSITION", i);
				topCredUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getUsername());
				topCredUser.accumulate("CREDS",userGameCredDto.getCreds());
				topCredUsers.add(topCredUser);
				i++;
			}
			
			json.accumulate("TOPCREDUSERS",topCredUsers);
			ArrayList<JSONObject> gameRuleJsons=new ArrayList<JSONObject>();
			for(GameRulesDto gameRuleDto:userDashboardDto.getGameRulesDtos()){
				JSONObject gameRuleJson=new JSONObject();
				gameRuleJson.accumulate("RULE", gameRuleDto.getRule());
				gameRuleJsons.add(gameRuleJson);
			}
			json.accumulate("GAMERULES", gameRuleJsons);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching User Dashboard");
			json.accumulate("ERROR", "Error While Fetching Dashboard");
		}
		System.out.println(json.toString());
		return json.toString();
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For getting the Game Rules Of A Location
	 */
	@RequestMapping(value="/getgamerulesoflocation.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getGameRulesOfaLocation(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameRulesOfaLocation");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			String locationAPIId=inputJson.getString("locationAPIId");
			Boolean isActive=inputJson.getBoolean("isActive");
			Boolean checkExpiryDate=inputJson.getBoolean("checkExpiryDate");
			Integer userId=inputJson.getInt("userId");
			String sortBy=null;
			if(null!=inputJson.get("sortBy")){
				sortBy=inputJson.getString("sortBy");
			}
			ArrayList<GameRulesDto> locationGameRulesDtos=this.gameService.getGameRulesOfaLocationByLocationIdForUser(locationAPIId, isActive, checkExpiryDate,sortBy,userId);
			ArrayList<JSONObject> locationGameRulesJSONS=new ArrayList<JSONObject>();
			for(GameRulesDto locationGameRulesDto: locationGameRulesDtos){
				JSONObject locationGameRuleJSON=new JSONObject();
				locationGameRuleJSON.accumulate("gameRuleId", locationGameRulesDto.getGameRuleId());
				if(null!=locationGameRulesDto.getRule()){
					locationGameRuleJSON.accumulate("rule", locationGameRulesDto.getRule());
				}
				else{
					locationGameRuleJSON.accumulate("rule", "");
				}
				if(null!=  locationGameRulesDto.getRuleLimit()){
					locationGameRuleJSON.accumulate("ruleLimit", locationGameRulesDto.getRuleLimit());
				}
				else{
					locationGameRuleJSON.accumulate("ruleLimit", -1);
				}
				locationGameRuleJSON.accumulate("creds", locationGameRulesDto.getCreds());
				if(null!=locationGameRulesDto.getExpiryDate()){
					//Date expiryDate=locationGameRulesDto.getExpiryDate();
					//SimpleDateFormat formatExpiryDate=new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
					//formatExpiryDate.setTimeZone(TimeZone.getTimeZone("America/New_York"));
					/*formatExpiryDate.setTimeZone(TimeZone.getTimeZone("EST"));*/
					String expiryDateFormat=this.getExpiryDateFormat(locationGameRulesDto.getExpiryDate());
					locationGameRuleJSON.accumulate("expiryDate", expiryDateFormat);
					//locationGameRuleJSON.accumulate("expiryDate", formatExpiryDate.format(expiryDate));
				}
				else{
					locationGameRuleJSON.accumulate("expiryDate", "");
				}
			//	locationGameRuleJSON.accumulate("distance", locationGameRulesDto.getDistance());
				if(null!=locationGameRulesDto.getLocationName()){
					locationGameRuleJSON.accumulate("locationName", locationGameRulesDto.getLocationName());
				}
				else{
					locationGameRuleJSON.accumulate("locationName", "");
				}
				locationGameRulesJSONS.add(locationGameRuleJSON);
			}
			outputJson.accumulate("locationGameRules", locationGameRulesJSONS);
			
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rules Not Found For Location");
			outputJson.accumulate("ERROR", "There are no active rewards for this location");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Game Rules For Location");
			outputJson.accumulate("ERROR", "Error While Fetching Game Rules For Location");
		}
		System.out.println("JSON RESPONSE "+outputJson.toString());
		return outputJson.toString();
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method For Getting ALL Game Rules
	 */
	@RequestMapping(value="/getgamerules.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getAllGameRules(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getAllGameRules()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Boolean isActive=inputJson.getBoolean("isActive");
			Boolean checkExpiryDate=inputJson.getBoolean("checkExpiryDate");
			Integer userId=inputJson.getInt("userId");
			double latitude=inputJson.getDouble("latitude");
			double longitude=inputJson.getDouble("longitude");
			String sortBy=null;
			if(null!=inputJson.get("sortBy")){
				sortBy=inputJson.getString("sortBy");
			}
			log.info("JSON REQUEST FOR ALL GAME RULES SORY BY "+ sortBy);
			 /**
			  * Added By Bhagya on May21st, 2019
			  * Method for to get the master geo location coordinates if the user is masterGeoLocated
			  */
			 KikspotUserDto kikspotUserDto=this.userService.getKikspotUserDetailsByUserId(userId);
				if(kikspotUserDto.getIsMasterGeoLocation()){
					MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
					latitude=masterGeoLocationConfigurationDto.getLatitude();
					longitude=masterGeoLocationConfigurationDto.getLongitude();
				}
			ArrayList<GameRulesDto> gameRulesDtos=this.gameService.getAllGameRules(isActive, checkExpiryDate,sortBy,userId,latitude,longitude);
			ArrayList<JSONObject> gameRulesJSONS=new ArrayList<JSONObject>();
			for(GameRulesDto gameRulesDto:gameRulesDtos){
				JSONObject gameRulesJSON=new JSONObject();
				if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
					gameRulesJSON.accumulate("locationAPIId", gameRulesDto.getLocationId());
				}
				else{
					gameRulesJSON.accumulate("locationAPIId", "");
				}
				gameRulesJSON.accumulate("gameRuleId", gameRulesDto.getGameRuleId());
				if(null!=gameRulesDto.getRule()){
					gameRulesJSON.accumulate("rule", gameRulesDto.getRule());
				}
				else{
					gameRulesJSON.accumulate("rule", "");
				}
				if(null!=  gameRulesDto.getRuleLimit()){
					gameRulesJSON.accumulate("ruleLimit", gameRulesDto.getRuleLimit());
				}
				else{
					gameRulesJSON.accumulate("ruleLimit", -1);
				}
				gameRulesJSON.accumulate("creds", gameRulesDto.getCreds());
				if(null!=gameRulesDto.getExpiryDate()){
					String expiryDateFormat=this.getExpiryDateFormat(gameRulesDto.getExpiryDate());
					gameRulesJSON.accumulate("expiryDate", expiryDateFormat);
				}
				else{
					gameRulesJSON.accumulate("expiryDate", "");
				}
			//	gameRulesJSON.accumulate("distance", gameRulesDto.getDistance());
				if(null!=gameRulesDto.getLocationName()){
					gameRulesJSON.accumulate("locationName", gameRulesDto.getLocationName());
				}
				else{
					gameRulesJSON.accumulate("locationName", "");
				}
				if(null!=gameRulesDto.getDistance()){
					gameRulesJSON.accumulate("distance", gameRulesDto.getDistance());
				}
				else{
					gameRulesJSON.accumulate("distance", "");
				}
				gameRulesJSONS.add(gameRulesJSON);
			}
			outputJson.accumulate("locationGameRules", gameRulesJSONS);
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rules Not Found");
			outputJson.accumulate("ERROR", "There are no active rewards for this location");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Game Rules");
			outputJson.accumulate("ERROR", "Error While Fetching Game Rules");
		}
		System.out.println("JSON RESPONSE "+outputJson.toString());
		log.info("JSON RESPONSE OF ALL GAME RULES"+outputJson.toString());
		return outputJson.toString();
	}
	/***
	 * Created By Bhagya On Jan 06th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for getting The Game Rules Of Locations List
	 */
	@RequestMapping(value="/getgamerulesoflocationslist.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getGameRulesForListOfLocations(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameRulesForListOfLocations()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			JSONArray locationAPIIds=inputJson.getJSONArray("locationAPIId");
			List<String> locationAPIIdsList=new ArrayList<String>();
			for(int i=0;i<locationAPIIds.length();i++){
				String locationAPIId=locationAPIIds.getString(i);
				locationAPIIdsList.add(locationAPIId);
			}
			Boolean isActive=inputJson.getBoolean("isActive");
			Boolean checkExpiryDate=inputJson.getBoolean("checkExpiryDate");
			Integer userId=inputJson.getInt("userId");
			String sortBy=null;
			if(null!=inputJson.get("sortBy")){
				sortBy=inputJson.getString("sortBy");
			}
			ArrayList<GameRulesDto> gameRulesLocationsDtos=this.gameService.getGameRulesForListOfLocations(locationAPIIdsList, isActive, checkExpiryDate,sortBy,userId);
			ArrayList<JSONObject> gameRulesLocationsJSONS=new ArrayList<JSONObject>();
			for(GameRulesDto gameRuleLocationsDto: gameRulesLocationsDtos){
				JSONObject gameRulesLocationsJSON= new JSONObject();
				if(null!=gameRuleLocationsDto.getLocationId() && gameRuleLocationsDto.getLocationId().trim().length()>0){
					gameRulesLocationsJSON.accumulate("locationAPIId", gameRuleLocationsDto.getLocationId());
				}
				else{
					gameRulesLocationsJSON.accumulate("locationAPIId", "");
				}
				gameRulesLocationsJSON.accumulate("gameRuleId", gameRuleLocationsDto.getGameRuleId());
				if(null!=gameRuleLocationsDto.getRule()){
					gameRulesLocationsJSON.accumulate("rule", gameRuleLocationsDto.getRule());
				}
				else{
					gameRulesLocationsJSON.accumulate("rule", "");
				}
				if(null!=  gameRuleLocationsDto.getRuleLimit()){
					gameRulesLocationsJSON.accumulate("ruleLimit", gameRuleLocationsDto.getRuleLimit());
				}
				else{
					gameRulesLocationsJSON.accumulate("ruleLimit", -1);
				}
				gameRulesLocationsJSON.accumulate("creds", gameRuleLocationsDto.getCreds());
				if(null!= gameRuleLocationsDto.getExpiryDate()){
					String expiryDateFormat=this.getExpiryDateFormat(gameRuleLocationsDto.getExpiryDate());
					gameRulesLocationsJSON.accumulate("expiryDate", expiryDateFormat);
				}
				else{
					gameRulesLocationsJSON.accumulate("expiryDate", "");
				}
				//gameRulesLocationsJSON.accumulate("distance", gameRuleLocationsDto.getDistance());
				if(null!=gameRuleLocationsDto.getLocationName()){
					gameRulesLocationsJSON.accumulate("locationName", gameRuleLocationsDto.getLocationName());
				}
				else{
					gameRulesLocationsJSON.accumulate("locationName", "");
				}
		
				gameRulesLocationsJSONS.add(gameRulesLocationsJSON);
			}
			outputJson.accumulate("gameRulesOfLocations", gameRulesLocationsJSONS);
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rules Not Found For Locations");
			outputJson.accumulate("ERROR", "There are no active rewards for this locations");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching Game Rules For Locations");
			outputJson.accumulate("ERROR", "Error While Fetching Game Rules For Locations");
		}
		System.out.println("JSON RESPONSE "+outputJson.toString());
		return outputJson.toString();
	}
	/**
	 * Created By Bhagya on Jan 07th,2016
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for Updating the User Game Creds For Game Rule
	 */
	@RequestMapping("/updateusergamecreds.do")
	public String updateUserGameCredsByGameRule(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside updateUserGameCredsByGameRule()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer gameRuleId=inputJson.getInt("gameRuleId");
			Integer userId=inputJson.getInt("userId");
			String locationId=inputJson.getString("locationAPIId");
			UserGameCredsDto userGameCredsDto=this.gameService.updateUserGameCredsByGameRuleId(gameRuleId, userId, locationId);
			JSONObject userGameCredsJSON=new JSONObject();
			userGameCredsJSON.accumulate("earnedCreds", userGameCredsDto.getCreds());
			userGameCredsJSON.accumulate("level", userGameCredsDto.getLevel());
			outputJson.accumulate("userGameCreds", userGameCredsJSON);
		}
		catch(UserNotFoundException e){
			log.error("User Not Found For Updating User Game Creds");
			outputJson.accumulate("ERROR", "User Not Found For Updating User Game Creds");
		}
		catch(GameRuleNotFoundException e){
			log.error("Game Rule Not Found For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Not Found For Updating User Game Creds");
		}
		catch(GameRuleInActiveException e){
			log.error("Game Rule Is InActive For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Is InActive For Updating User Game Creds");
		}
		catch(GameRuleExpiredException e){
			log.error("Game Rule Is Expired For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Is Expired For Updating User Game Creds");
		}
		catch(GameRuleExceedsRuleLimitException e){
			log.error("Game Rule Exceeds Rule Limit For Updating User Game Creds");
			outputJson.accumulate("ERROR", "Game Rule Exceeds Rule Limit For Updating User Game Creds");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Updating User Game Creds");
			outputJson.accumulate("ERROR", "Error While Updating User Game Creds");
		}
		System.out.println(outputJson.toString());
		return outputJson.toString();
	}
	/**
	 * Created By Firdous on 8th march
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 * 
	 * Method for getting the user game details 
	 */
	@RequestMapping(value="/getgamedetailsofuser.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getGameDetailsOfUser(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameDetailsOfUser()");
		JSONObject outputJson=new JSONObject();
		System.out.println("inside getgamedetailsofuser.do");
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			ArrayList<GameLevelsDto> gameLevels=this.gameService.getGameLevels(null,null,null,null,null);
			for(GameLevelsDto gameLevel:gameLevels){
				outputJson.accumulate("MinCreds",gameLevel.getMinCreds());
			}
			try{
				UserGameCredsDto gameCredDto=this.gameService.getUsersGameCredsByUserId(userId);
				/*GameLevelsDto gameLevelDto=this.gameService.getGameLevelByLevelName(gameCredDto.getLevel());*/
				GameLevelsDto gameLevelDto=this.gameService.getNextGameLevelByLevelName(gameCredDto.getLevel());
				/*Integer pointsNeededOfNextLevel=this.gameService.getPointsNeededForNextLevel(gameCredDto);*/
				Integer pointsNeededOfNextLevel=this.gameService.getPointsNeededForNextLevelToReach(gameLevelDto, gameCredDto);
				//ArrayList<UserGameCredsDto>	userGameCredDtos=new ArrayList<UserGameCredsDto>();
				//userGameCredDtos=this.gameService.getGameCredDetailsOfAllUsers();
				ArrayList<Integer> userGameCreds=new ArrayList<Integer>();
				userGameCreds=this.gameService.getDistinctGameCredDetailsOfAllUsers();
			
				int rank=0;
				int i=1;
				/*for(UserGameCredsDto userGameCredDto:userGameCredDtos){
					System.out.println(" rank "+ i +" creds "+userGameCredDto.getCreds());
					if(userGameCredDto.getKikspotUserDto().getUserId().equals(userId)){
						rank=i;
					}
					i++;
			    }*/
				for(Integer cred:userGameCreds){
					if(cred.equals(gameCredDto.getCreds())){
						rank=i;
					}
					i++;
				}
				outputJson.accumulate("Rank",rank);
			    outputJson.accumulate("CREDS",gameCredDto.getCreds());
			    outputJson.accumulate("LEVEL",gameCredDto.getLevel());
			    outputJson.accumulate("NEXTLEVEL", gameLevelDto.getLevel());
			    if(pointsNeededOfNextLevel==0){
				   outputJson.accumulate("POINTSNEEDEDFORNEXTLEVEL","0");
			    }
			    else{
			       outputJson.accumulate("POINTSNEEDEDFORNEXTLEVEL", pointsNeededOfNextLevel);
			    }
			}
			catch(UserGameCredNotFoundException e){
				log.error("User game cred not Found to get user game details");
				outputJson.accumulate("Rank",0);
			    outputJson.accumulate("CREDS",0);
			    outputJson.accumulate("LEVEL",0);
			    outputJson.accumulate("NEXTLEVEL", 0);
				outputJson.accumulate("POINTSNEEDEDFORNEXTLEVEL",0);
			}
			catch(GameLevelNotFoundException e){
				log.error("Game LEVEL not Found to get user game details");
				outputJson.accumulate("Rank",0);
			    outputJson.accumulate("CREDS",0);
			    outputJson.accumulate("LEVEL",0);
			    outputJson.accumulate("NEXTLEVEL", 0);
				outputJson.accumulate("POINTSNEEDEDFORNEXTLEVEL",0);
			}
			
		    
	    }
	
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while getting user game details");
			outputJson.accumulate("ERROR", "Error While getting user game details");
		}
		
		System.out.println(" JSON Response "+outputJson.toString());
		
		return outputJson.toString();
	}
	
	
	/**
	 * Created by firdous on 8th march 2016
	 * Method to get the game details of all the users in descending order of their cred points
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
		
	
	@RequestMapping(value="/getgamedetailsofallusers.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getGameDetailsOfAllUsers(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameDetailsOfAllUsers()");
		JSONObject outputJson=new JSONObject();
		System.out.println("inside getgamedetailsofallusers.do");
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			ArrayList<JSONObject> usersGameRecord=new ArrayList<JSONObject>();
			Integer userId=inputJson.getInt("userId");
			String time=inputJson.getString("time");
			String sortBy=inputJson.getString("location");
			ArrayList<Integer> userGameCreds=new ArrayList<Integer>();
			userGameCreds=this.gameService.getDistinctGameCredDetailsOfAllUsers();
			if(sortBy.equals("city")){
				String city1=this.userService.getCityOfUserByUserId(userId);
				ArrayList<UserGameCredsDto>	userGameCredDtos=new ArrayList<UserGameCredsDto>();
				if(time.equals("weekly")){
					userGameCredDtos=this.gameService.getAllUsersCredsInCurrentweek();
				}
				else{ 
					userGameCredDtos=this.gameService.getGameCredDetailsOfAllUsers();
				}
				
				if(!userGameCredDtos.isEmpty()){
					//int i=1;
					for(UserGameCredsDto userGameCredDto:userGameCredDtos){
						try{
							// Commented the city and country condition by bhagya on may 09th, 2019 -- based on client requirement , need to show all users ..don't restrict city or country
							/*if(this.userService.getCityOfUserByUserId(userGameCredDto.getKikspotUserDto().getUserId()).equals(city1)){*/
								JSONObject credUser=new JSONObject();
								int rank=0;
								int i=1;
								for(Integer cred:userGameCreds){
									if(cred.equals(userGameCredDto.getCreds())){
										rank=i;
									}
									i++;
								}
								
								//credUser.accumulate("POSITION", i);
								credUser.accumulate("POSITION", rank);
								credUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getUsername());
								credUser.accumulate("CREDS",userGameCredDto.getCreds());
								//i++;
								try{
									credUser.accumulate("CHANGE",userGameCredDto.getCreds()-this.credService.getCredsOfUserInSixMonths(userGameCredDto.getKikspotUserDto()));
								}
								catch(UserGameCredNotFoundException e){
									credUser.accumulate("CHANGE",userGameCredDto.getCreds()-0);
								}
								usersGameRecord.add(credUser);
							/*}*/
						}
						catch(UserLocationNotFoundException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						}
						catch(NullPointerException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						}
						
				   }
				}
			    else{
					throw new UserGameCredNotFoundException();
				}
			   
			}
			else {
				String country1=this.userService.getUserCountryByUserId(userId);
				KikspotUserDto userDto=this.userService.getKikspotUserDetailsByUserId(userId);
				ArrayList<UserGameCredsDto>	userGameCredDtos=new ArrayList<UserGameCredsDto>();
				if(time.equals("weekly")){
				    userGameCredDtos=this.gameService.getAllUsersCredsInCurrentweek();
				}
				else{
					
					userGameCredDtos=this.gameService.getGameCredDetailsOfAllUsers();
					
				}
				if(!userGameCredDtos.isEmpty()){
					//int i=1;
					for(UserGameCredsDto userGameCredDto:userGameCredDtos){
						
						try{
							// Commented the city and country condition by bhagya on may 09th, 2019 -- based on client requirement , need to show all users ..don't restrict city or country
							/*if(this.userService.getUserCountryByUserId(userGameCredDto.getKikspotUserDto().getUserId()).equals(country1)){*/
								JSONObject credUser=new JSONObject();
								int rank=0;
								int i=1;
								
								for(Integer cred:userGameCreds){
									if(cred.equals(userGameCredDto.getCreds())){
										rank=i;
									
									}
									i++;
									
								}
								
								//credUser.accumulate("POSITION", i);
								credUser.accumulate("POSITION", rank);
								//credUser.accumulate("POSITION", i);
								credUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getUsername());
								credUser.accumulate("CREDS",userGameCredDto.getCreds());
								//i++;
								try{
									credUser.accumulate("CHANGE",userGameCredDto.getCreds()-this.credService.getCredsOfUserInSixMonths(userGameCredDto.getKikspotUserDto()));
								}
								catch(UserGameCredNotFoundException e){
									credUser.accumulate("CHANGE",userGameCredDto.getCreds()-0);
								}
								usersGameRecord.add(credUser);
							/*}*/
						}
						catch(UserLocationNotFoundException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						}
						catch(NullPointerException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						}
						
			     	}
				}
				else{
					 throw new UserGameCredNotFoundException();
				}
			   
			}
			if(!usersGameRecord.isEmpty()){
				   outputJson.accumulate("USERSCREDS",usersGameRecord);
			}
			else{
					outputJson.accumulate("ERROR","No records found");
			}
		}
		catch(UserCredHistoryNotFoundException e){
			log.error("user  not found");
			outputJson.accumulate("ERROR","No records Found For This selection");
		}
		catch(UserLocationNotFoundException e){
			log.error("user location not found");
			outputJson.accumulate("ERROR","No Location exists");
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while getting all users game details");
			outputJson.accumulate("ERROR", "Error While getting all users game details");
		}
		System.out.println(" Json Response "+outputJson.toString());
		return outputJson.toString();
    }
		
		
	/**
	 * Added by Firdous 0n 8th march 2016
	 * Method to get the game details of all the friends of a user		
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	
	@RequestMapping(value="/getfriendsgamedetails.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
		public String getGameDetailsOfUserFriends(HttpServletRequest request,HttpServletResponse response) throws JSONException{
			log.info("inside getGameDetailsOfUser()");
			JSONObject outputJson=new JSONObject();
			
			try{
				JSONObject inputJson=JsonParser.getJson(request, response);
				ArrayList<JSONObject> topCredUsers=new ArrayList<JSONObject>();
				Integer userId=inputJson.getInt("userId");
				String time=inputJson.getString("time");
				String sortBy=inputJson.getString("location");
				if(sortBy.equals("city")){
					String city1=this.userService.getCityOfUserByUserId(userId);
					ArrayList<UserGameCredsDto>	userGameCredDtos=new ArrayList<UserGameCredsDto>();
					if(time.equals("weekly")){
						userGameCredDtos=this.gameService.getUserCredsOfFriendsInPresentWeek(userId);
					}
					else{
						userGameCredDtos=this.gameService.getUsersCredsOfUsersFriends(userId);
					}
					if(!userGameCredDtos.isEmpty()){
					int i=1;
					for(UserGameCredsDto userGameCredDto:userGameCredDtos){
						try{
							// Commented the city and country condition by bhagya on may 09th, 2019 -- based on client requirement , need to show all users ..don't restrict city or country
							/*if(this.userService.getCityOfUserByUserId(userGameCredDto.getKikspotUserDto().getUserId()).equals(city1)){*/
								JSONObject topCredUser=new JSONObject();
								topCredUser.accumulate("POSITION", i);
								topCredUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getUsername());
								topCredUser.accumulate("CREDS",userGameCredDto.getCreds());
								i++;
								try{
									topCredUser.accumulate("CHANGE",userGameCredDto.getCreds()-this.credService.getCredsOfUserInSixMonths(userGameCredDto.getKikspotUserDto()));
								}
								catch(UserGameCredNotFoundException e){
									topCredUser.accumulate("CHANGE",userGameCredDto.getCreds()-0);
								}
								topCredUsers.add(topCredUser);
							/*}*/
						}
						catch(UserLocationNotFoundException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						}
						catch(NullPointerException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						
						}
						
					}
					}
					else{
						 throw new UserGameCredNotFoundException();
					  }
				}
				else{
					
					String country=this.userService.getUserCountryByUserId(userId);
					ArrayList<UserGameCredsDto>	userGameCredDtos=new ArrayList<UserGameCredsDto>();
					if(time.equals("weekly")){
						userGameCredDtos=this.gameService.getUserCredsOfFriendsInPresentWeek(userId);
					}
					else{
						
					userGameCredDtos=this.gameService.getUsersCredsOfUsersFriends(userId);
					}
					if(!userGameCredDtos.isEmpty()){
						
					int i=1;
					for(UserGameCredsDto userGameCredDto:userGameCredDtos){
						try{
							// Commented the city and country condition by bhagya on may 09th, 2019 -- based on client requirement , need to show all users ..don't restrict city or country
							/*if(this.userService.getUserCountryByUserId(userGameCredDto.getKikspotUserDto().getUserId()).equals(country)){*/
								JSONObject topCredUser=new JSONObject();
								topCredUser.accumulate("POSITION", i);
								topCredUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getUsername());
								topCredUser.accumulate("CREDS",userGameCredDto.getCreds());
								i++;
								try{
									topCredUser.accumulate("CHANGE",userGameCredDto.getCreds()-this.credService.getCredsOfUserInSixMonths(userGameCredDto.getKikspotUserDto()));
								}
								catch(UserGameCredNotFoundException e){
									topCredUser.accumulate("CHANGE",userGameCredDto.getCreds()-0);
								}
								
								topCredUsers.add(topCredUser);
							/*}*/
							
						}
						catch(UserLocationNotFoundException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						}
						catch(NullPointerException e){
							log.error("No location exists for the user "+userGameCredDto.getKikspotUserDto().getUserId());
						
						}
						
						
					}
				    }
					else{
						 throw new UserGameCredNotFoundException();
					  }
					}
				if(!topCredUsers.isEmpty()){
				   outputJson.accumulate("TOPCREDUSERS",topCredUsers);
				}
				else{
					outputJson.accumulate("ERROR","No records found");
				}
			}
			catch(UserNotFoundException e){
				log.error("user  not found");
				outputJson.accumulate("ERROR","User Not Found");
			}
			catch(UserCredHistoryNotFoundException e){
				log.error("user  not found");
				outputJson.accumulate("ERROR","No records Found For This selection");
			}
			catch(UserLocationNotFoundException e){
				log.error("user location not found");
				outputJson.accumulate("ERROR","No Location exists");
			}
			catch(UserGameCredNotFoundException e){
				outputJson.accumulate("ERROR","No User Creds found for last 6 months");
			}
			catch(ReferredUsersNotFoundException e){
				log.error("no user found as referred users");
				outputJson.accumulate("ERROR","You haven't added any friends yet!");
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("error while getting user game details");
				outputJson.accumulate("ERROR", "Error While getting all users game details");
			}
			return outputJson.toString();
		}
	
	
	/**
	 * Added by Firdous on 17 march 2016
	 * Method which returns all the game names along with rules associated with that games
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping(value="/getgamedetails.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getGameDetailsOfAllTheGames(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getGameDetailsOfAllTheGames()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			ArrayList<GameDto> gameDtos=this.gameService.getAllGames();
			JSONObject gameDetail=new JSONObject();
			ArrayList<JSONObject> gameDetails1=new ArrayList<JSONObject>();
			for(GameDto gameDto:gameDtos){
					try{
						ArrayList<GameRulesDto> gameRuleDtos=this.gameService.getGlobalGameRulesByGameId(gameDto.getGameId(),userId);
						
					    for(GameRulesDto gameRulesDto:gameRuleDtos){
					    	JSONObject gameDetail1=new JSONObject();
					    	gameDetail1.accumulate("GAME NAME",gameDto.getGameName());
					    	gameDetail1.accumulate("GameRuleId",gameRulesDto.getGameRuleId());
						 	gameDetail1.accumulate("GameRule", gameRulesDto.getRule());
						 	
							if(null!=gameRulesDto.getExpiryDate()){
								String expiryDateFormat=this.getExpiryDateFormat(gameRulesDto.getExpiryDate());
								gameDetail1.accumulate("expiryDate", expiryDateFormat);    
						       
							}
							else{
								gameDetail1.accumulate("expiryDate", "");
							}
						 	gameDetail1.accumulate("IsActive",gameRulesDto.getIsActive());
						 	gameDetail1.accumulate("GameRuleCreds","Earn "+gameRulesDto.getCreds() +" Cred");
							gameDetails1.add(gameDetail1);
						 	
						 	
					    }
					
					   
					}
					catch(GameRuleNotFoundException e){
						
						log.error("No Game Rules Found for "+gameDto.getGameId());
					}
				
			}
			outputJson.accumulate("ALL GAME DETAILS",gameDetails1);
			System.out.println(" Json Response"+outputJson.toString());	
			return outputJson.toString();
		}
		catch(GamesNotFoundException e){
			log.error("Games Not Found");
			outputJson.accumulate("ERROR","No Games Found");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("error while getting user game details");
			outputJson.accumulate("ERROR", "Error While getting all users game deatils");
		}
		return outputJson.toString();
	}
	
	/**
	 * 
	 * ADDED BY FIRDOUS ON 17th March 2016
	 * Method to get all the game level details with points needed to complete the level
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	
	@RequestMapping(value="/getgameleveldetails.do",method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String getAllGameLevelsDetails(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getAllGameLevelsDetails()");
		JSONObject outputJson=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			ArrayList<JSONObject> gameLevelDetails=new ArrayList<JSONObject>();
			
			UserGameCredsDto gameCredDto=this.gameService.getUsersGameCredsByUserId(userId);
			ArrayList<GameLevelsDto> gameLevels=this.gameService.getGameLevels(null,null,null,null,null);
			for(GameLevelsDto gameLevelDto:gameLevels){
				JSONObject gameLevel=new JSONObject();
				Integer pointNeededForLevelToReach=this.gameService.getPointsNeededForNextLevelToReach(gameLevelDto,gameCredDto);
				gameLevel.accumulate("LEVEL",gameLevelDto.getLevel());
				gameLevel.accumulate("MinCreds",gameLevelDto.getMinCreds());
				gameLevel.accumulate("POINTNEEDEDTOCOMPLETETHISLEVEL",pointNeededForLevelToReach);
				gameLevelDetails.add(gameLevel);
			}
			
			outputJson.accumulate("GAME LEVEL DETAILS",gameLevelDetails);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("error in getting game levels details");
			outputJson.accumulate("ERROR","Can not get game level details");
		}
		return outputJson.toString();
	}
	
	@RequestMapping("addgamerule.do")
	public String addGameRuleByUser(){
		return null;
	}
	/***
	 * Created By Bhagya on April 02nd , 2019
	 * @param expiryDate
	 * @return
	 * @throws ParseException
	 * 
	 * Game rule Expiry date format
	 */
	public String getExpiryDateFormat(Date expiryDate) throws ParseException{
		String expiryDateFormat="";
		SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		//dateFormat.setTimeZone(TimeZone.getTimeZone("EST"));
		Date currentDate=new Date();
		String ExpiryDateFormat=dateFormat.format(expiryDate);
		String currentDateFormat=dateFormat.format(currentDate);
		
		//long difference = expiryDate.getTime() - currentDate.getTime(); IST
		
		// EST //
		long difference =dateFormat.parse(ExpiryDateFormat).getTime() - dateFormat.parse(currentDateFormat).getTime(); 
	       long diffMinutes = difference / (60 * 1000) % 60;
			long diffHours = difference / (60 * 60 * 1000) % 24;
			long diffDays = difference / (24 * 60 * 60 * 1000);
			long diffSeconds = difference / 1000 % 60;
			Calendar afterOneWeekCalendar = Calendar.getInstance();
			//afterOneWeekCalendar.setTimeZone(TimeZone.getTimeZone("EST"));
			afterOneWeekCalendar.add(Calendar.DATE,7);
			Date afterOneweekDate=afterOneWeekCalendar.getTime();
			String afterOneweekDateFormat=dateFormat.format(afterOneweekDate);
			
			String finalDays;
			String finalHours;
			String finalMinutes;
			 if(diffDays==1 || diffDays==0){
	    		   finalDays=diffDays +" Day" ;
	    	   }
	    	   else{
	    		  finalDays=diffDays +" Days"; 
	    	   }
			 if(diffHours==1){
	    		   finalHours=diffHours +" hour" ;
	    	   }
	    	   else{
	    		  finalHours=diffHours +" hours"; 
	    	   }
			 if(diffMinutes==1){
	    		   finalMinutes=diffMinutes +" minute" ;
	    	   }
	    	   else{
	    		  finalMinutes=diffMinutes +" minutes"; 
	    	   }
			 if(diffDays==0){
				 expiryDateFormat=finalHours+" "+finalMinutes;
			 }  
			 else{
				 expiryDateFormat= finalDays+" "+finalHours+" "+finalMinutes;
			 }
			 
			 if(diffHours==0){
				 expiryDateFormat=finalMinutes;
			 } 
			 
			 if(diffDays==0 && diffHours==0 && diffMinutes==0 && diffSeconds!=0){
				 expiryDateFormat= diffSeconds +"Seconds";
			 } 
			 if(diffDays==0 && diffHours==0 && diffMinutes==0 && diffSeconds==1){
				 expiryDateFormat= diffSeconds +"Second";
			 } 
			
	       /*if( ExpiryDateFormat.compareTo(currentDateFormat)==0){
				
				if(diffHours<=1){
					expiryDateFormat=diffMinutes +" minutes";
				}
				else if(diffHours<=12){
					expiryDateFormat=diffHours +" hours";
					
				}
			}
	       else if(dateFormat.parse(ExpiryDateFormat).before(dateFormat.parse(afterOneweekDateFormat))){
	    	   if(diffDays==1){
	    		   expiryDateFormat=diffDays +" Day";
	    	   }
	    	   else{
	    		   expiryDateFormat=diffDays +" Days";
	    	   }
	    	  
	       }
	       else{
	    	   SimpleDateFormat finalExpiryDateFormat=new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	    	   expiryDateFormat=finalExpiryDateFormat.format(expiryDate);
	    	 
	       }*/
			return expiryDateFormat;
			
		}
}
