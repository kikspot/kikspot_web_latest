package com.kikspot.frontend.mobile.controller;

/**
 * 
 * 
 * created by Firdous on 23-11-2015
 * 
 * controller to handle the feedback messages of a user
 * 
 * 
 */


import java.util.List;



import javax.annotation.Resource;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.useraction.dto.FeedbackDto;
import com.kikspot.frontend.useraction.service.FeedbackService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;





@RestController
@RequestMapping("/mobile")
public class FeedBackController {
	
	private static Logger log = Logger.getLogger(FeedBackController.class);
	 
    @Resource(name="feedbackService")	
    private FeedbackService feedbackService;
    
    
    
    @Resource(name="userService")
	private UserService userService;
    
   
    
    
    /**
     * @author Firdous
     * @param request
     * @param response
     * @return json object with feedback message
     * @throws Exception
     */
    
    @RequestMapping(value="/feedback.do")
    public String getFeedBackFromUser(HttpServletRequest request,HttpServletResponse response) throws JSONException  {
    	  
    	 log.info("inside getFeedbackFromUser()");
    	 String error;
		 JSONObject obj=new JSONObject();
		 try{
			 JSONObject json=new JSONObject(request.getParameter("json"));
			 //JSONObject json=JsonParser.getJson(request, response); // using this json for test the service from webapp
			 MultipartHttpServletRequest multiRequest= (MultipartHttpServletRequest) request;
			 List<MultipartFile> feedbackFiles=multiRequest.getFiles("files");
		     String message=json.getString("feedbackMessage");
		     Integer userId=json.getInt("userId");
		     Integer result=this.feedbackService.addFeedback(message, userId, feedbackFiles);
		     if(result>0){
			 obj.accumulate("STATUS", "Feedback Added Successfully");	
		     }
		     else{
		    	 throw new Exception();
		     }
		 }
		
		 catch(UserNotFoundException e)
		 {   
			 log.error("user not found");
			 error="User Not Found For Adding Feedback";
			 obj.accumulate("ERROR",error);
		 }
		 catch(MailNotSentException e)
		 {
			 log.error("mail not sent");
			 error="Mail Not Sent To The User";
			 obj.accumulate("ERROR",error);
		 }
		 catch(Exception e)  {
			 e.printStackTrace();
			 log.error("Error occured While getting the Feedback From User "+e.getMessage());
			 error = "Error occured While getting the Feedback From User";
			 obj.accumulate("ERROR", error);
		 }
		  
		  String result = obj.toString();
		  log.info("response : " + result);
		  return result;
      }
    

	/**
      * @author Firdous
      * @param request
      * @param response
      * @return display the feedback message of a user
      * @throws Exception
      */
    
      @RequestMapping(value="/showfeedback.do")
      public String showFeedBack(HttpServletRequest request,HttpServletResponse response) throws Exception  {
    	   	log.info("inside showfeedback( )");
    	   	String error="no feedback present";
    	   	JSONObject obj=new JSONObject();
    	
		try  {
			JSONObject json=JsonParser.getJson(request, response);
			Integer feedbackId=json.getInt("feedbackId");
			FeedbackDto feedbackDto=this.feedbackService.getFeedbackById(feedbackId);
			String result=feedbackDto.getFeedbackMessage();
			obj.accumulate("user Feed Back", result);
		 }
		  
		 catch(FeedBackNotFoundException e) {
			 error="No Feedbacks Found For the User";
			 obj.accumulate("ERROR",error);
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 error = "Error occured While Showing Feedbacks";
			 obj.accumulate("ERROR", error);
		}
		
		 String result = obj.toString();
		 log.info("response : " + result);
		 System.out.println("response : " + result);
		 return result;
    	
    }
      
      
      /**
       * @author Firdous
       * @param request
       * @param response
       * @return total number of feedbacks
       * @throws Exception
       */
      
      
      @RequestMapping(value="/totalfeedbacks.do")
      public String showTotalfeedback(HttpServletRequest request,HttpServletResponse response) throws Exception  {
    	log.info("inside showTotalfeedback()");
      	String error="no feedback present";
        JSONObject obj=new JSONObject();
       	try {
      		 Integer result=this.feedbackService.getTotalResults();
      		 JSONObject editJson=new JSONObject();
             editJson.accumulate("toatlfeedbacks",result);
	    	 obj.accumulate("total number of feedbacks", editJson);
		}
		
		 catch(Exception e){
			 e.printStackTrace();
			 error = "Error occured while Showing Total Feedbacks";
			 obj.accumulate("ERROR", error);
			
		}
		String result = obj.toString();
		log.info("response : " + result);
		System.out.println("response : " + result);
		return result;
    	
      	
      }
      
      /**
       * Created By Firdous
       * @param request
       * @param response
       * @return
       * @throws Exception
       * 
       * Method for delete feedback
       */
      @RequestMapping(value = "/deletefeedback.do")
  	  public String deleteUserFeedBack(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	  log.info("inside deleteUserMessage()");
  		JSONObject obj = new JSONObject();
  		try  
  		{
  			JSONObject json = JsonParser.getJson(request, response);
  			Integer feedbackId = json.getInt("feedbackId");
  			Integer result=this.feedbackService.deleteFeedBack(feedbackId);
  			obj.accumulate("STATUS", "User Feedback Deleted");
   		 }

  		 catch (FeedBackNotFoundException e) {
  			log.error("Error in Deleting Feedback" + e.toString());
  			obj.accumulate("ERROR", "No Feedback found for this user");
  		  } 
  		  catch (Exception e) {
  			e.printStackTrace();
  			log.error("Some Error Occurred While Deleting Feedback" + e.toString());
  			obj.accumulate("ERROR", "Error While Deleting Feedback");
  		  }
  		  return obj.toString();

  	 }

    	 
}
