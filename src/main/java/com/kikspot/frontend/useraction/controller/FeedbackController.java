package com.kikspot.frontend.useraction.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.game.controller.GameController;
import com.kikspot.frontend.useraction.dto.FeedbackDto;
import com.kikspot.frontend.useraction.service.FeedbackService;





@Controller("FeedbackController")
@RequestMapping("/feedback")
public class FeedbackController {

	private static Logger log=Logger.getLogger(FeedbackController.class);
	
	
	@Resource(name="feedbackService")
	private FeedbackService feedbackService;


	/**
	 * 
	 * @author Firdous Tabassum
	 * @param map
	 * @param listBeanDto
	 * @param id
	 * @return
	 * @throws Exception
	 * Method to get all the feedbacks of a user
	 * And  get the feedbacks based on the user id of the feedback users
	 */
	
	
	
	@RequestMapping(value="/getfeedbacks.do")
	public String getFeedback(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto,@RequestParam(value="feedbackUserId",required=false)Integer feedbackUserId) throws Exception{
		
		log.info("inside getFeedback()");
		
		String defaultMail=null;
		Integer totalResults;
		ArrayList<FeedbackDto> feedbackDtos=null;
		try   {
			if(null==listBeanDto.getSortBy()) {
				listBeanDto.setSortBy("feedbackId");
			}
			
			feedbackDtos=this.feedbackService.getFeedbacks(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection(),feedbackUserId);
			totalResults=feedbackDtos.get(0).getTotalResults();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			ArrayList<KikspotUser> users=this.feedbackService.getusers();
			defaultMail=this.feedbackService.getDefaultFeedbackMail();
			map.put("defaultMail", defaultMail);
			map.put("users", users);
			map.put("feedbacks", feedbackDtos);
			map.put("feedbackUserId", feedbackUserId);
			return "feedback/viewFeedbacks";
		}
		
		catch(FeedBackNotFoundException e)	{
			log.error("feedback not found");
			defaultMail=this.feedbackService.getDefaultFeedbackMail();
			map.put("defaultMail", defaultMail);
			String message=e.toString();
			map.put("message", message);
			return "feedback/viewFeedbacks";
		}
		
		catch(UserNotFoundException e) {
			log.error("no user found");
			String message=e.toString();
			map.put("message",message);
			return "feedback/viewFeedbacks";
			
		}
		
		catch(Exception e) {
			e.printStackTrace();
			map.put("message","error");
			return "error";
		}
		
	}
	
	
	
	/**
	 * @author Firdous
	 * method to delete the feedback of a user
	 * @param feedbackId
	 * 
	 * 
	 */
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="/deletefeedback.do")
	public String deleteFeedback(@RequestParam("feedbackId")Integer id){
		
		log.info("inside deleteFeedback()");
		try {
			Integer result=this.feedbackService.deleteFeedBack(id);
			if(result>0){
			  return "success";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "error";
		}	
	}


	
	/**
	 * 
	 * @param defaultMail
	 * @return
	 */
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST,value="/changefeedbackmail.do")
	public String changeDefaultFeedbackMail(@RequestParam("defaultMail") String defaultMail){
		log.info("inside changeDefaultFeedbackMail()");
		try{
			this.feedbackService.updateDefaultMail(defaultMail);
			return "success";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}

}
