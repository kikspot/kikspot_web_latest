package com.kikspot.frontend.useraction.service;

/** 
 * 
 * created by Firdous 24-11-2015
 * Service class to handle User feedback
 * 
 * 
 */

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.useraction.dao.FeedbackDao;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.backend.useraction.model.FeedbackDefaultMail;
import com.kikspot.frontend.common.utility.EmailSender;
import com.kikspot.frontend.notification.service.NotificationService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.useraction.dto.FeedbackDto;


@Transactional
@Service("feedbackService")
public class FeedBackServiceImpl implements FeedbackService {
    
	private static Logger log=Logger.getLogger(FeedBackServiceImpl.class);
	@Resource(name="feedbackDao")
	private FeedbackDao feedbackDao;
	
	@Resource(name="userDao")
	private UserDao userDao;
	
	@Resource(name = "emailSender")
	private EmailSender emailSender;
	
    
	 private String feedbackImagePath;
	
	 @Resource(name="notificationService")
	private NotificationService notificationService;
	 
		public String getFeedbackImagePath() {
			return feedbackImagePath;
		}

		public void setFeedbackImagePath(String feedbackImagePath) {
			this.feedbackImagePath = feedbackImagePath;
		}
	
		
	

	/**
	 * Added by Firdous
	 * method to get the feedback from a user and save it inside the database
	 * 
	 */
	@Override
	public Integer saveFeedback( String feedbackMessage,Integer userId,String attachments) throws Exception {
		log.info("inside saveFeedback()");
		Feedback feedback=new Feedback();
		System.out.println("inside save");
		KikspotUser user=this.userDao.getKikspotUserById(userId);
		feedback.setFeedbackMessage(feedbackMessage);
		feedback.setAttachements(attachments);
		feedback.setKikspotUser(user);
		feedback.setFeedbackDate(new Date());
		Integer result=this.feedbackDao.saveFeedBack(feedback);
		System.out.println("result at save"+result);
		return result;
	}
    
	/**
	 * 
	 * 
	 * method to get the total number of feedbacks
	 * 
	 * 
	 */
	
	public Integer getTotalResults() throws Exception {
		log.info("inside getTotalResults()");
		Integer totalResults=this.feedbackDao.getTotalFeedbacks();
		return totalResults;
		
	}
   
	/**
     * 
     * created by firdous on 26-11-2015
     * method to delete the feedback of a user
     * 
     * 
     */
	@Override
	public Integer deleteFeedBack(Integer feedbackId) throws Exception {
		log.info("inside deleteFeedBack()");
		Feedback feedback=this.feedbackDao.getFeedbackById(feedbackId);
		Integer result=this.feedbackDao.deleteFeedback(feedback);
		return result;
	}

   
	
	 /**
     * 
     * Added by firdous
     * Method get the feedback by accepting fedback id
     * 
     */
	@Override
	public FeedbackDto getFeedbackById(Integer id) throws Exception {
		Feedback feedback=this.feedbackDao.getFeedbackById(id);
		FeedbackDto feedbackDto=FeedbackDto.populateFeedBackDto(feedback);
		return feedbackDto;
	}

	/**
	 * 
	 * added by Firdous
	 * Method to get the feedbacks of all users
	 * @throws FeedbackNotFoundException 
	 * 
	 */
	@Override
	public ArrayList<FeedbackDto> getFeedbacks(Integer pageNo, Integer pageSize, String sortBy, String searchBy,
	    Boolean ascending,Integer id) throws Exception {
		log.info("inside getFeedbacks()");
		ArrayList<FeedbackDto> feedbackDtos=new ArrayList<FeedbackDto>();
		ArrayList<Feedback> feedbacks=this.feedbackDao.getAllFeedbacks(pageNo,pageSize, sortBy, searchBy,ascending,id);
		for(Feedback feedback:feedbacks){
			FeedbackDto feedbackDto=FeedbackDto.populateFeedBackDto(feedback);
			feedbackDtos.add(feedbackDto);
	    }
	  return feedbackDtos;
	}
	
    /**
     * Added by firdous
     * method to get all the users who has sent the feebacks
     * 
     */

	@Override
	public ArrayList<KikspotUser> getusers() throws Exception {
		log.info("inside getUsers( )");
		ArrayList<KikspotUser> users=this.feedbackDao.getFeedbackUsers();
		return users;
	}

	 /**
     * Added by Firdous 
     * method to add the feedback of a user inside the database.. accept multipart file. save the file in a system folder 
     * 
     */

	@SuppressWarnings("resource")
	@Override
	public Integer addFeedback(String message, Integer userId, List<MultipartFile> feedbackFiles) throws Exception {
		log.info("inside addFeedback()");
		System.out.println("inside add feedback images");
		List<File> files=new ArrayList<File>();
		String attachments="";
		if(null!=feedbackFiles){
		for (MultipartFile multipartFile : feedbackFiles) {
			 File feedbackFile = null;
			 System.out.println("File type="+multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf(".")+1));
			 File directory = new File(feedbackImagePath + "/" + userId);
			 if (!directory.exists()) {
					directory.mkdirs();
			 }
			 if (multipartFile.getSize() > 0) {
					String fileType=multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf(".")+1);
					System.out.println("File type="+multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf(".")+1));
				    feedbackFile=new File(feedbackImagePath+userId+"/"+new Date().getTime()+"."+fileType);
				   /* if(!feedbackFile.exists()){
				    	feedbackFile.mkdirs();
					}*/
				    feedbackFile.setReadable(true);
				    feedbackFile.setWritable(true);
				    files.add(feedbackFile);
				    feedbackFile.createNewFile();
				    String filename= feedbackFile.getName();
					attachments+=filename+";";
					FileOutputStream fos = new FileOutputStream(feedbackFile);
					fos.write(multipartFile.getBytes());
					fos.close();
					
			 }
			 Thread.sleep(200);
		 }
		}
		 Integer result=this.saveFeedback(message,userId,attachments);
	     KikspotUser user=this.userDao.getKikspotUserById(userId);
		 KikspotUserDto kikspotUserDto=KikspotUserDto.populateKikspotUserDto(user);
		 this.emailSender.sendEmailToUser(kikspotUserDto);
		 try{
			 FeedbackDefaultMail feedbackDefaultMail=this.feedbackDao.getFeedbackDefaultMail();
			 this.emailSender.feedbackInfoToAdmin(kikspotUserDto,message,files,result,feedbackDefaultMail.getDefaultEmail());
			 /// Send the Notification to the user
			 NotificationType notificationType=this.notificationService.getNotificationTypeByType("Feedback");
			 this.notificationService.sendNotificationToUser(kikspotUserDto.getUserId(),notificationType, " Feedback Sent Successfully! "+message,"");
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 log.info(" Error while sending feedback email "+e.getMessage());
		 }
		 return result;
		
	}
	
	/*@SuppressWarnings("resource")
	@Override
	public Integer addFeedback(String message, Integer userId, List<MultipartFile> feedbackFiles) throws Exception {
		log.info("inside addFeedback()");
		List<File> files=new ArrayList<File>();
		String attachments="";
		
			for(MultipartFile imageFile:feedbackFiles){
				if(imageFile.getSize()>0){
					String fileType=imageFile.getOriginalFilename().substring(imageFile.getOriginalFilename().lastIndexOf(".")+1);
					File destinationFile=new File(feedbackImagePath+userId+"/"+new Date().getTime()+"."+fileType);
					if(!destinationFile.exists()){
						destinationFile.mkdirs();
					}
				    files.add(destinationFile);
				    destinationFile.createNewFile();
				    String filename= destinationFile.getName();
					attachments+=filename+";";
					
					
			 }
			 Thread.sleep(200);
		 }
		
		 Integer result=this.saveFeedback(message,userId,attachments);
	     KikspotUser user=this.userDao.getKikspotUserById(userId);
		 KikspotUserDto kikspotUserDto=KikspotUserDto.populateKikspotUserDto(user);
		 this.emailSender.sendEmailToUser(kikspotUserDto);
		 try{
			 FeedbackDefaultMail feedbackDefaultMail=this.feedbackDao.getFeedbackDefaultMail();
			 this.emailSender.feedbackInfoToAdmin(kikspotUserDto,message,files,result,feedbackDefaultMail.getDefaultEmail());
		 }
		 catch(Exception e){}
		 return result;
		
	}
	*/
	/**
	 * Created by Jeevan on FEBRUARY 23, 2016
	 * 
	 * Method to getDefaultFeedbackMail..
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getDefaultFeedbackMail()throws Exception{
		log.info("inside getDefaultFeedbackMail() ");
		FeedbackDefaultMail defaultMail=this.feedbackDao.getFeedbackDefaultMail();
		return defaultMail.getDefaultEmail();
	}
	
	/**
	 * 
	 * @param mail
	 * @throws Exception
	 */
	public void updateDefaultMail(String mail)throws Exception{
		log.info("inside updateDefaultmail()");
		FeedbackDefaultMail feedbackDefaultMail=this.feedbackDao.getFeedbackDefaultMail();
		feedbackDefaultMail.setDefaultEmail(mail);
		this.feedbackDao.saveorUpdateFeedbackDefaultMail(feedbackDefaultMail);
	}
}

