package com.kikspot.frontend.useraction.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.useraction.dto.FeedbackDto;



public interface FeedbackService {
	public Integer saveFeedback(String feedbackMessage,Integer user_id,String attachments) throws Exception;
	public Integer getTotalResults() throws Exception;
	public Integer deleteFeedBack(Integer feedbackId) throws Exception;
	public ArrayList<FeedbackDto> getFeedbacks(Integer pageNo, Integer pageSize, String sortBy, String searchBy,
		    Boolean ascending,Integer id) throws Exception;
	public FeedbackDto getFeedbackById(Integer id) throws Exception;
	public ArrayList<KikspotUser> getusers() throws Exception;
	public Integer addFeedback(String message, Integer userId, List<MultipartFile> feedbackFiles) throws Exception;

	public String getDefaultFeedbackMail()throws Exception;
	public void updateDefaultMail(String mail)throws Exception;
	

}

