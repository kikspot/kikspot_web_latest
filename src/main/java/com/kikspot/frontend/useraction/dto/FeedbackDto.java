package com.kikspot.frontend.useraction.dto;

import java.util.ArrayList;
import java.util.Date;

import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.frontend.user.dto.KikspotUserDto;


public class FeedbackDto {
	
	
	private Integer feedbackId;
	
	private Integer userId;

	private KikspotUserDto kikspotUser;
	
	private Date feedbackDate;
 
	private String feedbackMessage;

	private Integer totalResults;
	
	private String attachments;
	
	private ArrayList<FeedbackAttachmentDto> feedbackAttachmentDto;

	public Integer getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(Integer feedbackId) {
		this.feedbackId = feedbackId;
	}

	/*public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}*/
	

	public String getFeedbackMessage() {
		return feedbackMessage;
	}

	public KikspotUserDto getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUserDto kikspotUser) {
		this.kikspotUser = kikspotUser;
	}

	public void setFeedbackMessage(String feedbackMessage) {
		this.feedbackMessage = feedbackMessage;
	}

	public Date getFeedbackDate() {
		return feedbackDate;
	}

	public void setFeedbackDate(Date feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
	
	public String getAttachments() {
		return attachments;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	
	public ArrayList<FeedbackAttachmentDto> getFeedbackAttachmentDto() {
		return feedbackAttachmentDto;
	}

	public void setFeedbackAttachmentDto(ArrayList<FeedbackAttachmentDto> feedbackAttachmentDto) {
		this.feedbackAttachmentDto = feedbackAttachmentDto;
	}

	public Integer getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}

	
	public static FeedbackDto populateFeedBackDto(Feedback feedback){
		
		FeedbackDto feedbackDto=new FeedbackDto();
		ArrayList<FeedbackAttachmentDto> feedbackattachmentDtos=new ArrayList<FeedbackAttachmentDto>();
		String attachments=feedback.getAttachements();
		String[] splittedFiles=null;
		if(null!=attachments)	{
			splittedFiles = attachments.split(";");
			for(String file:splittedFiles) {
				FeedbackAttachmentDto feedbackAttachmentDto=FeedbackAttachmentDto.populateFeedBackAttachmentDto(file);
				feedbackattachmentDtos.add(feedbackAttachmentDto);
			}
			feedbackDto.setFeedbackAttachmentDto(feedbackattachmentDtos);
		}
		feedbackDto.setUserId(feedback.getKikspotUser().getUserId());
		feedbackDto.setFeedbackDate(feedback.getFeedbackDate());
		feedbackDto.setFeedbackId(feedback.getFeedbackId());
		feedbackDto.setFeedbackMessage(feedback.getFeedbackMessage());
		feedbackDto.setAttachments(feedback.getAttachements());
		feedbackDto.setTotalResults(feedback.getTotalFeedbacks());
		feedbackDto.setKikspotUser(KikspotUserDto.populateKikspotUserDto(feedback.getKikspotUser()));
		return feedbackDto;
	}

		
}
