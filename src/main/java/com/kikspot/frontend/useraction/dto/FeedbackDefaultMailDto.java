package com.kikspot.frontend.useraction.dto;

import com.kikspot.backend.useraction.model.FeedbackDefaultMail;

public class FeedbackDefaultMailDto {
	
	private Integer feedbackDefaultMailId;
	
	private String defaultEmail;

	public Integer getFeedbackDefaultMailId() {
		return feedbackDefaultMailId;
	}

	public void setFeedbackDefaultMailId(Integer feedbackDefaultMailId) {
		this.feedbackDefaultMailId = feedbackDefaultMailId;
	}

	public String getDefaultEmail() {
		return defaultEmail;
	}

	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}
	
	
	public static FeedbackDefaultMailDto populateFeedbackDefaultMailDto(FeedbackDefaultMail defaultMail){
		FeedbackDefaultMailDto	 defaultMailDto=new FeedbackDefaultMailDto();
		defaultMailDto.setDefaultEmail(defaultMail.getDefaultEmail());
		defaultMailDto.setFeedbackDefaultMailId(defaultMail.getFeedbackDefaultMailId());
		return defaultMailDto;
  }

}
