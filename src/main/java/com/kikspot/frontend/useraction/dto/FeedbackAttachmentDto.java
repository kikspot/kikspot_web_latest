package com.kikspot.frontend.useraction.dto;

/**
 * 
 * Created by Firdous on 28-12-2015
 * Dto to set the file type based on extensions
 * 
 */
public class FeedbackAttachmentDto {

	
	private String fileType;
	
	private String extension;
	
	private String fileName;
	
	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public static FeedbackAttachmentDto populateFeedBackAttachmentDto(String file){
		 FeedbackAttachmentDto feedbackAttachmentDto= new FeedbackAttachmentDto();
		 if(file.contains(".jpg") || file.contains(".png") || file.contains(".gif")|| file.contains(".jpeg") ) {
				feedbackAttachmentDto.setFileType("image");
		 }
		 else if(file.contains(".mp4") || file.contains(".mpeg") || file.contains(".3gp")) {
				feedbackAttachmentDto.setFileType("video");
		 }
		 else {
				feedbackAttachmentDto.setFileType("otherFiles");
		 }
			
		 feedbackAttachmentDto.setFileName(file);
		 return feedbackAttachmentDto;
	}
	
}
