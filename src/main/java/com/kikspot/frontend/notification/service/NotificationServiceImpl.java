package com.kikspot.frontend.notification.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.kikspot.backend.exceptions.NotificationNotFoundException;
import com.kikspot.backend.notification.dao.NotificationDao;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.NotificationTypes;
import com.kikspot.backend.notification.model.UnsubscribedNotification;
import com.kikspot.backend.notification.model.UserNotification;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.frontend.notification.dto.NotificationTypeDto;
import com.kikspot.frontend.notification.dto.UserNotificationDto;
import com.kikspot.frontend.useraction.dto.FeedbackDto;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;


/***
 * 
 * Created by Jeevan on DECEMBER 22, 2015
 * 
 * Service for Notification..
 * 
 * 
 * @author KNS-ACCONTS
 *
 */
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService{

	
	@Resource(name="userDao")
	private UserDao userDao;
	
	
	@Resource(name="notificationDao")
	private NotificationDao notificationDao;
	
	
	
	private static Logger log=Logger.getLogger(NotificationServiceImpl.class);
	
	private static ApnsService service;
	
	
	
/* ** Push Notification Parameters */	
	private String enviroment;
	
	private String passPhrase;
	
	private String certFile;

	


	public String getEnviroment() {
		return enviroment;
	}

	public void setEnviroment(String enviroment) {
		this.enviroment = enviroment;
	}

	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}

	public String getCertFile() {
		return certFile;
	}

	public void setCertFile(String certFile) {
		this.certFile = certFile;
	}
	
	
	
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 * Method to getPushNotificationService..
	 * 
	 * 
	 * @throws IOException
	 */
	public void getPushNotificationService()throws IOException{
		log.info("inside getPushNotificationService() ");
		System.out.println(certFile+" "+enviroment+" "+passPhrase);
		InputStream certificate=this.getClass().getClassLoader().getResourceAsStream(certFile);
		if(enviroment.equals("sandbox")){
			service=APNS
					.newService()
					.withCert(certificate, passPhrase)
					.withSandboxDestination()
					.build();
		}
		else if(enviroment.equals("production")){
			service=APNS
					.newService()
					.withCert(certificate, passPhrase)
					.withProductionDestination()
					.build();
		}
		
		Map<String, Date> devices=service.getInactiveDevices();
		System.out.println("Inactive Devices "+devices.size());
		ArrayList<String> deviceTokens=new ArrayList<String>();
		for(Entry<String, Date> entry:devices.entrySet()){
			deviceTokens.add(entry.getKey());			
		}
		try{
			if(!deviceTokens.isEmpty()){
				ArrayList<UserDevice> userDevices=this.userDao.getUserDeviceByDeviceTokens(deviceTokens);
				for(UserDevice userDevice:userDevices){
					this.userDao.deleteUserDevice(userDevice);
				}
			}
		}
		catch(Exception e){			
		}
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 23, 2015
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<NotificationTypeDto> getNotificationSettingsofUser(Integer userId)throws Exception{
		log.info("inside getNotificationSettingsofUser() ");
		ArrayList<NotificationType> notificationTypes=this.notificationDao.getAllNotificationTypes();
		ArrayList<UnsubscribedNotification> unsubscribedNotifications;
		try{
			unsubscribedNotifications=this.notificationDao.getAllUnsubscribedNotificationsofUser(userId);
		}
		catch(Exception e){
			unsubscribedNotifications=new ArrayList<UnsubscribedNotification>();
		}
		
		ArrayList<NotificationTypeDto> notificationTypeDtos=new ArrayList<NotificationTypeDto>();
		for(NotificationType notificationType:notificationTypes){
			NotificationTypeDto notificationTypeDto=new NotificationTypeDto();
			notificationTypeDto.setNotificationTypeId(notificationType.getNotificationTypeId());
			notificationTypeDto.setNotificationType(notificationType.getNotificationType());
			if(unsubscribedNotifications.size()>0){
				for(UnsubscribedNotification unsubscribedNotification : unsubscribedNotifications){
					
					if(unsubscribedNotification.getNotificationType().getNotificationTypeId().equals(notificationType.getNotificationTypeId())){
						notificationTypeDto.setIsEnabled(false);
					}
					/*else{
					System.out.println("inside inner else");
						notificationTypeDto.setIsEnabled(true);
					}*/
				}
				notificationTypeDtos.add(notificationTypeDto);
			}
			else{
				notificationTypeDto.setIsEnabled(true);
				notificationTypeDtos.add(notificationTypeDto);
			}
				
			
		}		
		return notificationTypeDtos;
	}
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 23, 2015
	 * Method to enableorDisableUserNotification..
	 * @param userId
	 * @param notificationTypeId
	 * @param isEnable
	 * @return
	 * @throws Exception
	 */
	public Integer enableorDisableUserNotification(Integer userId,Integer notificationTypeId,Boolean isEnable)throws Exception{
		log.info("inside enableorDisableUserNotification() ");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		NotificationType notificationType=this.notificationDao.getNotificationTypeById(notificationTypeId);
		UnsubscribedNotification unsubscribedNotification;
		Integer result;
		if(isEnable){
			unsubscribedNotification=this.notificationDao.getUnsubscribedNotificationByUserandType(userId, notificationTypeId);
			result=this.notificationDao.deleteUnsubscribedNotification(unsubscribedNotification);
		}
		else{
			unsubscribedNotification=new UnsubscribedNotification();
			unsubscribedNotification.setKikspotUser(kikspotUser);
			unsubscribedNotification.setNotificationType(notificationType);
			result=this.notificationDao.saveorUpdateUnsubscribedNotifications(unsubscribedNotification);
		}
		return result;		
	}
	
	
	
	
	/***
	 * Created by Jeevan on DECEMBER 23, 2015
	 * Method to send PushNotification..
	 * 
	 * @param message
	 * @param userDevice
	 * @throws Exception
	 */
	public void sendPushNotification(String message,UserDevice userDevice)throws Exception{
		log.info("inside sendPushNotification() ");
		if(null==service){
			try{
				getPushNotificationService();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		log.info("Pushing notification to device message : "+message+", userId : "+userDevice.getKikspotUser().getUserId());
		//build payload(A JSON string consisting of all notification data in specified format and send it
		System.out.println("get notifictions and add to payload");
		try{
			System.out.println(service.toString());
			String payload=APNS.newPayload().alertBody(message).sound("Glass.aiff").actionKey("Play").build();
			System.out.println("Sending Notification " +userDevice.getDeviceToken()+"  "+message);
			service.push(userDevice.getDeviceToken(), payload);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 28, 2015
	 * 
	 * Method to sendAdminNotificationtoUsers..
	 *  
	 * @param userIds
	 * @param message
	 * @param sendAll
	 * 
	 * 
	 * @throws Exception
	 */
	public void sendAdminNotificationtoUsers(ArrayList<Integer> userIds, String message,Boolean sendAll)throws Exception{
		log.info("inside sendAdminNotificationtoUsers() ");
		
		ArrayList<KikspotUser> kikspotUsers;
		if(sendAll){
			kikspotUsers=this.userDao.getAllUsers(null, null, "userId", null, true);
		}
		else{
			kikspotUsers=this.userDao.getKikspotUserByUserIds(userIds);
		}
		ArrayList<Integer> kikspotUserIds=new ArrayList<Integer>();
		for(KikspotUser kikspotUser: kikspotUsers){
			kikspotUserIds.add(kikspotUser.getUserId());
		}		
		for(KikspotUser kikspotUser:kikspotUsers){
			UserNotification userNotification=new UserNotification();			
			userNotification.setKikspotUser(kikspotUser);
			userNotification.setNotificationDate(new Date());
			userNotification.setNotificationMessage(message);
			 // commented the Admin notifications and not sended to the mobile team. As per the trello, they don't required the admin notifications
			// 
			//userNotification.setNotificationType(this.notificationDao.getNotificationTypeByType(NotificationTypes.ADMIN.toString()));
			userNotification.setTriggeredId(null);
			userNotification.setIsRead(false);
			//this.notificationDao.saveorUpdateUserNotification(userNotification);			
		}
		
		try{
			ArrayList<UserDevice> userDevices=this.userDao.getUserDevicesByUserIds(kikspotUserIds);
			for(UserDevice userDevice: userDevices){
				//this.sendPushNotification(message, userDevice);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	
    
	/**
	 * Created by Firdous on 3rd February
	 * Method to get the user notifications
	 * 
	 */
	@Override
	public ArrayList<UserNotificationDto> getNotificationsOfUser(Integer userId) throws Exception {
		log.info("inside getNotificationsOfUser()");
		ArrayList<UserNotificationDto> UserNotificationDtos=new ArrayList<UserNotificationDto>();
		ArrayList<UserNotification> userNotifications=this.notificationDao.getNotifications(userId);
		for(UserNotification userNotification:userNotifications){
			//Mark all the notifications as read
			userNotification.setIsRead(true);
			this.notificationDao.saveorUpdateUserNotification(userNotification);
			UserNotificationDto userNotificationDto=UserNotificationDto.populateUserNotificationDto(userNotification);
			UserNotificationDtos.add(userNotificationDto);
	    }
	  return UserNotificationDtos;
	
	}
	
	
	/**
	 * 
	 * @param type
	 * @return
	 * @throws NotificationNotFoundException
	 * 
	 * 
	 */
	public NotificationType getNotificationTypeByType(String type)throws NotificationNotFoundException{
		log.info("inside getNotificationTypeByType() ");
		NotificationType notificationType=this.notificationDao.getNotificationTypeByType(type);
		return notificationType;
	}
	
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on February 02, 2015
	 * 
	 *  Method to sendNotificationtoUser..
	 *  
	 *  
	 * @param userId
	 * @param notificationType
	 * @param notificationMessage
	 * @throws Exception
	 * 
	 * 
	 *  Steps:
	 *  
	 *  1. Get UnSubscribedNotification of User.
	 *  2. Check if UserIs unsubscribed for notification
	 *  3. If yes,
	 *               break
	 *  4. If No,
	 *            Save UseRNotification to DB
	 * 5. Get User Device by UserId
	 * 
	 * 6.  Send Push Notification.
	 * 
	 * 
	 * 
	 */
	public void sendNotificationToUser(Integer userId,NotificationType notificationType,String notificationMessage,String triggeredId)throws Exception{
		log.info("inside sendNotificationToUser() ");
		UnsubscribedNotification unsubscribedNotification=null;
		try{
		 unsubscribedNotification=this.notificationDao.getUnsubscribedNotificationByUserandType(userId, notificationType.getNotificationTypeId());
		}
		catch(NotificationNotFoundException e){
			log.info("No Unsubscribed Notification Found ");
		}
		if(null==unsubscribedNotification){
			UserNotification userNotification=new UserNotification();
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
			userNotification.setKikspotUser(kikspotUser);
			userNotification.setNotificationDate(new Date());
			userNotification.setNotificationMessage(notificationMessage);
			userNotification.setNotificationType(notificationType);
			userNotification.setTriggeredId(triggeredId);
			userNotification.setIsRead(false);
			this.notificationDao.saveorUpdateUserNotification(userNotification);
			/*try{
				UserDevice userDevice=this.userDao.getUserDeviceofUser(kikspotUser);
				if(null!=userDevice){
					this.sendPushNotification(notificationMessage, userDevice);
				}
			}
			catch(Exception e){		
				e.printStackTrace();
			}*/
		}		
	}
	
	
	
	
	/**
	 * Created by Jeevan on FEBRUARY 16, 2016
	 * 
	 * Method to setNotificationofUser as Read..
	 * 
	 * @param userId
	 * @param notificationId
	 * @throws Exception
	 */
	public void setNotificationasRead(Integer userId, Integer notificationId)throws Exception{
		UserNotification userNotification=this.notificationDao.getUserNotificationByNotificationId(notificationId);
		userNotification.setIsRead(true);
		this.notificationDao.saveorUpdateUserNotification(userNotification);
	}
	
	/**
	 * Created by Jeevan on FEBRUARY 22, 2016
	 * 
	 * Method to deleteNotification..
	 * 
	 * @param notificationId
	 * @throws Exception
	 */
	public void deleteNotification(Integer notificationId)throws Exception{
		log.info("inside deleteNotification() ");
		UserNotification userNotification=this.notificationDao.getUserNotificationByNotificationId(notificationId);
		this.notificationDao.deleteUserNotification(userNotification);
	}
	
	
	
	/**
	 * 
	 * Created by Firdous on 26th May 2106
	 * Method to get the total number of unread notifications
	 * 
	 */
	public Integer getNumberOfUnreadNotifications(Integer userId) throws Exception {
		Integer unReadNotifications=0;
		try{
		  this.notificationDao.getNotifications(userId);
		  unReadNotifications=this.notificationDao.getUnreadNotifications(userId);
		}
		catch(NotificationNotFoundException e){
		  throw new NotificationNotFoundException();
		}
		return unReadNotifications;
	}
	
}
