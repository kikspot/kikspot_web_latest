package com.kikspot.frontend.notification.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kikspot.frontend.notification.service.NotificationService;
import com.kikspot.frontend.user.service.UserService;




/**
 * Created by Jeevan on DEcember 28, 2015
 * 
 *  Controller for Notifications.
 * @author KNS-ACCONTS
 *
 */
@RequestMapping("/notification")
@Controller("webNotificationController")
public class NotificationController {

	
	
	@Resource(name="notificationService")
	private NotificationService notificationService;
	
	
	@Resource(name="userService")
	private UserService userService;
	
	
	
	private static Logger log=Logger.getLogger(NotificationController.class);
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 28, 2015
	 * 
	 *  Method to sendNotificationsofAdmin..
	 *  
	 * @param userIds
	 * @param message
	 * @param sendAll
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("/sendnotification.do")
	public String sendNotificationsofAdmin(@RequestParam(value="userIds[]", required=false)List<Integer> userIds, @RequestParam("message") String message,
			@RequestParam(defaultValue="false",required=false,value="sendAll") Boolean sendAll)throws Exception{
		log.info("inside sendNotificationsofAdmin()");
		try{
			ArrayList<Integer> users=new ArrayList<Integer>();
			if(null!=userIds){
				users.addAll(userIds);
			}
			this.notificationService.sendAdminNotificationtoUsers(users, message, sendAll);
			return "success";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Sending Admin Notifications to Users ..");
			return "error";
		}
	}
	
	
	
	
	
	
}
