package com.kikspot.frontend.notification.dto;

import com.kikspot.backend.notification.model.UnsubscribedNotification;
import com.kikspot.frontend.user.dto.KikspotUserDto;



public class UnsubscribedNotificationDto {
	
	private Integer unsubscribedNotificationId;
	private KikspotUserDto kikspotUserDto;
	private NotificationTypeDto notificationTypeDto;
	
	
	public Integer getUnsubscribedNotificationId() {
		return unsubscribedNotificationId;
	}
	public void setUnsubscribedNotificationId(Integer unsubscribedNotificationId) {
		this.unsubscribedNotificationId = unsubscribedNotificationId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public NotificationTypeDto getNotificationTypeDto() {
		return notificationTypeDto;
	}
	public void setNotificationTypeDto(NotificationTypeDto notificationTypeDto) {
		this.notificationTypeDto = notificationTypeDto;
	}
	
	
	
	
	public static UnsubscribedNotificationDto populateUnsubscribedNotificationDto(UnsubscribedNotification unsubscribedNotification){
		UnsubscribedNotificationDto unsubscribedNotificationDto=new UnsubscribedNotificationDto();
		unsubscribedNotificationDto.setUnsubscribedNotificationId(unsubscribedNotification.getUnsubscribedNotificationId());
		unsubscribedNotificationDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(unsubscribedNotification.getKikspotUser()));
		unsubscribedNotificationDto.setNotificationTypeDto(NotificationTypeDto.populateNotificationTypeDto(unsubscribedNotification.getNotificationType()));
		return unsubscribedNotificationDto;
	}
	
	

}
