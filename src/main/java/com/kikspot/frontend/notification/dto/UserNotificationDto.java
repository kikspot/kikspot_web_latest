package com.kikspot.frontend.notification.dto;

import java.util.Date;

import com.kikspot.backend.notification.model.UserNotification;
import com.kikspot.frontend.user.dto.KikspotUserDto;



/**
 * Created by Jeevan on DECEMBER 21, 2015
 * 
 * DTO for UserNotifications..
 * 
 * 
 * @author KNS-ACCONTS
 *
 */
public class UserNotificationDto {
	
	
	private Integer userNotificationId;
	private KikspotUserDto kikspotUserDto;
	private NotificationTypeDto notificationTypeDto;
	private String notificationMessage;
	private Date notificationDate;
	private String triggeredId;
	private Boolean isRead;
	private Integer totalNotifications;
	public Integer getUserNotificationId() {
		return userNotificationId;
	}
	public void setUserNotificationId(Integer userNotificationId) {
		this.userNotificationId = userNotificationId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public NotificationTypeDto getNotificationTypeDto() {
		return notificationTypeDto;
	}
	public void setNotificationTypeDto(NotificationTypeDto notificationTypeDto) {
		this.notificationTypeDto = notificationTypeDto;
	}
	public String getNotificationMessage() {
		return notificationMessage;
	}
	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}
	public Date getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}
	public String getTriggeredId() {
		return triggeredId;
	}
	public void setTriggeredId(String triggeredId) {
		this.triggeredId = triggeredId;
	}
	public Boolean getIsRead() {
		return isRead;
	}
	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}
	public Integer getTotalNotifications() {
		return totalNotifications;
	}
	public void setTotalNotifications(Integer totalNotifications) {
		this.totalNotifications = totalNotifications;
	}
	
	
	
	/**
	 * 
	 * @param userNotification
	 * @return
	 */
	public static UserNotificationDto populateUserNotificationDto(UserNotification userNotification){
		UserNotificationDto userNotificationDto=new UserNotificationDto();
		userNotificationDto.setUserNotificationId(userNotification.getUserNotificationId());
		userNotificationDto.setIsRead(userNotification.getIsRead());
		userNotificationDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userNotification.getKikspotUser()));
		userNotificationDto.setNotificationMessage(userNotification.getNotificationMessage());
		userNotificationDto.setNotificationTypeDto(NotificationTypeDto.populateNotificationTypeDto(userNotification.getNotificationType()));
		userNotificationDto.setTriggeredId(userNotification.getTriggeredId());
		userNotificationDto.setTotalNotifications(userNotification.getTotalNotifications());
		userNotificationDto.setNotificationDate(userNotification.getNotificationDate());
		return userNotificationDto;
	}
	
	
	
	

}
