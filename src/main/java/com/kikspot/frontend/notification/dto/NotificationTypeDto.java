package com.kikspot.frontend.notification.dto;

import com.kikspot.backend.notification.model.NotificationType;

public class NotificationTypeDto {

	
	private Integer notificationTypeId;
	private String notificationType;
	private Boolean isEnabled;
	
	
	public Integer getNotificationTypeId() {
		return notificationTypeId;
	}
	public void setNotificationTypeId(Integer notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public Boolean getIsEnabled() {
		return isEnabled;
	}
	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	
	
	public static NotificationTypeDto populateNotificationTypeDto(NotificationType notificationType){
		NotificationTypeDto notificationTypeDto=new NotificationTypeDto();
		notificationTypeDto.setNotificationTypeId(notificationType.getNotificationTypeId());
		notificationTypeDto.setNotificationType(notificationType.getNotificationType());
		return notificationTypeDto;
	}
	
	
	
	
}
