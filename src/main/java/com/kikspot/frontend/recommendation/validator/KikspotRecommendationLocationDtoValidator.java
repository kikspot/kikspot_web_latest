package com.kikspot.frontend.recommendation.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
/**
 * Created By Bhagya On December 15th,2015
 *	Validator For Kikspot Recommendation Locations
 */
@Component("kikspotRecommendationLocationDtoValidator")
public class KikspotRecommendationLocationDtoValidator implements Validator{

	@Override
	public boolean supports(Class<?> paramClass) {
		// TODO Auto-generated method stub
		return KikspotRecommendationLocationDto.class.equals(paramClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// TODO Auto-generated method stub
	//	ValidationUtils.rejectIfEmptyOrWhitespace(errors,"kikspotLocationId","Location Id Cannot be Empty","Location Id Cannot be Empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"locationName","Location Name Cannot be Empty","Location Name Cannot be Empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"latitude","Latitude Cannot be Empty","Latitude Cannot be Empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"longitude","Longitude Cannot be Empty","Longitude Cannot be Empty");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors,"address","Address Cannot be Empty","Address Cannot be Empty");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors,"phoneNo","PhoneNo Cannot be Empty","PhoneNo Cannot be Empty");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors,"ratings","Rating Cannot be Empty","Rating Cannot be Empty");
	//	ValidationUtils.rejectIfEmptyOrWhitespace(errors,"distance","Distance Cannot be Empty","Distance Cannot be Empty");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors,"url","Website URL Cannot be Empty","Website URL Cannot be Empty");
	
		
	}
	
}