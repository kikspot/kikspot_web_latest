package com.kikspot.frontend.recommendation.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.recommendation.model.LocationRatingToggler;
import com.kikspot.frontend.admin.dto.MasterGeoLocationConfigurationDto;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.game.dto.GameRuleTypeDto;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.LocationRatingTogglerDto;
import com.kikspot.frontend.recommendation.dto.RecommendationConfigurationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationImagesDto;
import com.kikspot.frontend.recommendation.dto.UserLocationRatingsDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

/**
 * Created By Bhagya On November 23rd,2015
 *	Controller class for Recommendation Module For Handling Web Services
 */
@Controller("webRecommendationController")
@RequestMapping("/recommendations")
public class RecommendationController{
	private static Logger log=Logger.getLogger(RecommendationController.class);
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="gameService")
	private GameService gameService;
	
	@Autowired
	@Qualifier("kikspotRecommendationLocationDtoValidator")
	private Validator kikspotRecommendationLocationDtoValidator;

	private String serverURL;
	
	public String getServerURL() {
		return serverURL;
	}
	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}
	
	
	@InitBinder("kikspotRecommendationLocationDto")
	protected void initKikspotRecommendationLocationBinder(WebDataBinder binder) {
	    binder.setValidator(kikspotRecommendationLocationDtoValidator);
	}
	 @InitBinder
		public void initBinder(WebDataBinder binder) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
			dateFormat.setLenient(true);
			// true passed to CustomDateEditor constructor means convert empty String to null
			binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));
		}


	/**
	 * Created By Bhagya On December 18th,2015
	 * @param map
	 * @param kikspotRecommendationLocation
	 * @return
	 * Method for initiating the adding Recommendation Location
	 */
	
	@RequestMapping(value="/addlocation.do",method=RequestMethod.GET)
	public String initAddRecommendationLocation(Map<String,Object> map,@ModelAttribute("kikspotRecommendationLocationDto") KikspotRecommendationLocationDto kikspotRecommendationLocationDto){
		log.info("inside RecommendationController");
		try{
			map.put("title", "Add Recommendation Location");
			return "recommendation/addLocation";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Initiating Add Recommendation Location");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocation
	 * @param map
	 * @return
	 * 
	 * Method for saving the recommendation Location which are added by admin
	 */
	@RequestMapping(value="/addlocation.do",method=RequestMethod.POST)
	public String addRecommendationLocation(@Valid @ModelAttribute("kikspotRecommendationLocationDto")KikspotRecommendationLocationDto kikspotRecommendationLocationDto,BindingResult validationResult,Map<String,Object> map,RedirectAttributes redAttribs){
		
		try{
			if(validationResult.hasErrors()){
				return "recommendation/addLocation";
			}
			Integer savedResult=this.recommendationService.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocationDto);
			if(savedResult>0){
				redAttribs.addFlashAttribute("status", "Recommendation Location Added Successfully");
				 return "redirect:/recommendations/viewlocations.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Saving Recommendation Location");
			return "error";
		}
	}
	/**
	 * Created By bhagya On December 18th,2015
	 * @param locationId
	 * @param kikspotRecommendationLocationDto
	 * @param map
	 * @return
	 * 
	 * Method for Initiating the Edit Recommendation Location
	 */
	@RequestMapping(value="/editlocation.do",method=RequestMethod.GET)
	public String initEditRecommendationLocation(@RequestParam("locationId") Integer locationId,@ModelAttribute("kikspotRecommendationLocationDto")KikspotRecommendationLocationDto kikspotRecommendationLocationDto,Map<String,Object> map){
		log.info("inside initEditRecommendationLocation()");
		try{
			KikspotRecommendationLocationDto resultedRecommendationDto=this.recommendationService.getKikspotRecommendationLocationByLocationId(locationId);
			if(resultedRecommendationDto.getCloseDate()!=null){
			SimpleDateFormat format=new SimpleDateFormat("MM/dd/yy");
			//format.setTimeZone(TimeZone.getTimeZone("EST"));
			format.format(resultedRecommendationDto.getCloseDate());
			}
			map.put("location", resultedRecommendationDto);
			return "recommendation/editLocation";
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Initiating Edit Recommendation Location");
			return "error";
		}
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocationDto
	 * @param map
	 * @return
	 * 
	 * Method for Updating The Recommendation Location
	 */
	
	@RequestMapping(value="/editlocation.do",method=RequestMethod.POST)
	public String editRecommendationLocation(@Valid @ModelAttribute("kikspotRecommendationLocationDto")KikspotRecommendationLocationDto kikspotRecommendationLocationDto,BindingResult validationResult,Map<String,Object> map,RedirectAttributes redAttribs){
		log.info("inside editRecommendationLocation()");
		try{
			if(validationResult.hasErrors()){
				return "recommendation/editLocation";
			}
			Integer updatedResult=this.recommendationService.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocationDto);
			if(updatedResult>0){
				redAttribs.addFlashAttribute("status", "Recommendation Location Edited Successfully");
				return "redirect:/recommendations/viewlocations.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Editing Recommendation Location");
			return "error";
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param map
	 * @param listBeanDto
	 * @return
	 * 
	 * Method for getting the Recommendation Locations which are added by admin
	 */
	@RequestMapping(value="/viewlocations.do")
	public String viewRecommendationLocations(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info("inside viewRecommendationLocations()");
		RecommendationConfigurationDto configDto=null;
		LocationRatingTogglerDto locationRatingTogglerDto=null;
		MasterGeoLocationConfigurationDto masterGeoLocationConfigurationDto=null;
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("locationId");
			}
			ArrayList<KikspotRecommendationLocationDto> kikspotRecommendationLocationDtos=this.recommendationService.getKikspotRecommendationLocations(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection());
			Integer totalResults=kikspotRecommendationLocationDtos.get(0).getTotalLocations();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			locationRatingTogglerDto=this.recommendationService.getLocationRatingTogglerfromDB();
			configDto=this.recommendationService.getRecommendationsConfiguration();
			masterGeoLocationConfigurationDto=this.recommendationService.getMasterGeoLocationConfiguration();
			ArrayList<KikspotUserDto> kikspotUserDtos=this.userService.getAllUsersEcxeptByPassUsers();
			String masterGeoLocatedUsers=this.userService.getAllUsersOfMasterGeoLocated();
			
			ArrayList<GameRuleTypeDto> gameRuleTypes=this.gameService.getGameRuleTypes();
			ArrayList<GameRuleTypeDto> filteredgameRuleTypes=new ArrayList<GameRuleTypeDto>();
			for(GameRuleTypeDto gameRuleType : gameRuleTypes){
				if(gameRuleType.getGameRuleType().equalsIgnoreCase("Share App") || gameRuleType.getGameRuleType().equalsIgnoreCase("Share Recommendation List") 
						|| gameRuleType.getGameRuleType().equalsIgnoreCase("Share Game Status") || gameRuleType.getGameRuleType().equalsIgnoreCase("Share Comments")
						|| gameRuleType.getGameRuleType().equalsIgnoreCase("Sharee Account Creation") ||  gameRuleType.getGameRuleType().equalsIgnoreCase("User First Rating")
						||  gameRuleType.getGameRuleType().equalsIgnoreCase("User Second Rating")||  gameRuleType.getGameRuleType().equalsIgnoreCase("User Third Rating")
						||  gameRuleType.getGameRuleType().equalsIgnoreCase("User Fourth Rating")||  gameRuleType.getGameRuleType().equalsIgnoreCase("User First Comment")
						||  gameRuleType.getGameRuleType().equalsIgnoreCase("User Second Comment")||  gameRuleType.getGameRuleType().equalsIgnoreCase("User Third Comment")
						||  gameRuleType.getGameRuleType().equalsIgnoreCase("User Fourth Comment")){
					
				}
				else{
					filteredgameRuleTypes.add(gameRuleType);
				}
			}
			
			map.put("toggleRating", locationRatingTogglerDto.getAllowGlobalRating());
			map.put("locations", kikspotRecommendationLocationDtos);
			map.put("recommendationConfiguration", configDto);
			map.put("masterGeoLocation", masterGeoLocationConfigurationDto);
			map.put("users", kikspotUserDtos);
			
			map.put("masterGeoLocatedUsers", masterGeoLocatedUsers);
			map.put("gameRuleTypes", filteredgameRuleTypes);
			
			return "recommendation/viewLocations";
		}
		/*catch(KikspotRecommendationLocationNotFoundException e){
			map.put("recommendationConfiguration", configDto);
			map.put("message", "Recommendation Locations Not Found");
			return "recommendation/viewLocations";
		}*/
		catch(KikspotRecommendationLocationNotFoundException e){
			try{
			locationRatingTogglerDto=this.recommendationService.getLocationRatingTogglerfromDB();
			configDto=this.recommendationService.getRecommendationsConfiguration();
			}
			catch(Exception e1){
				
			}
			map.put("recommendationConfiguration", configDto);
			map.put("toggleRating",locationRatingTogglerDto.getAllowGlobalRating());
			map.put("message", "Recommendation Locations Not Found");
			return "recommendation/viewLocations";
		} 
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While viewing Recommendation Locations");
			return "error";
		}
	}
	
	
	/**
	 * 
	 * @param recommendationConfigurationId
	 * @param isBar
	 * @param isBowlingAlley
	 * @param isCasino
	 * @param isLounge
	 * @param isNightClub
	 * @param isPub
	 * @return
	 */
	@RequestMapping(method=RequestMethod.POST,value="/saverecommendationconfiguration.do")
	public String saveRecommendationConfigurations(@RequestParam("recommendationConfigurationId") Integer recommendationConfigurationId, @RequestParam(value="isBar",required=false,defaultValue="false") Boolean isBar,
			 @RequestParam(value="isBowlingAlley",required=false,defaultValue="false") Boolean isBowlingAlley, @RequestParam(value="isCasino",required=false,defaultValue="false") Boolean isCasino,
			 @RequestParam(value="isLounge",required=false,defaultValue="false") Boolean isLounge, @RequestParam(value="isNightClub",required=false,defaultValue="false") Boolean isNightClub,
			 @RequestParam(value="isPub",required=false,defaultValue="false") Boolean isPub
			){
		try{
			RecommendationConfigurationDto configDto=new RecommendationConfigurationDto();
			configDto.setIsBar(isBar);
			configDto.setIsBowlingAlley(isBowlingAlley);
			configDto.setIsCasino(isCasino);
			configDto.setIsLounge(isLounge);
			configDto.setIsNightClub(isNightClub);
			configDto.setIsPub(isPub);
			configDto.setRecommendationConfigurationId(recommendationConfigurationId);
			this.recommendationService.saveorUpdateRecommendationConfiguration(configDto);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "redirect:/recommendations/viewlocations.do";
				
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Created By bhagya On December 18th,2015
	 * @param kikspotLocationId
	 * @return
	 * 
	 * Method for checking the existence of Location
	 */
	@RequestMapping(value="/checklocation.do",method=RequestMethod.GET)
	@ResponseBody
	public String checkLocationExistenceByKikspotLocationId(@RequestParam("kikspotLocationId")String kikspotLocationId){
		log.info("inside checkLocationExistenceByKikspotLocationId()");
		try{
			KikspotRecommendationLocationDto kikspotRecommendationLocationDto=this.recommendationService.getKikspotRecommendationLocationByKikspotLocationId(kikspotLocationId);
			if(null!=kikspotRecommendationLocationDto){
				return "error";
			}
			else{
				throw new KikspotRecommendationLocationNotFoundException();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			log.info("No Location Exists,You Can Add Location");
			return "success";
		}
		catch(Exception e){
			log.error("Error While Validating Location Existence");
			return "error";
		}
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * Method for Deleting the Kikspot Recommendation Location
	 */
	@ResponseBody
	@RequestMapping(value="/deletelocation.do")
	public String deleteKikspotRecommendationLocation(@RequestParam("locationId")Integer locationId){
		log.info("inside deleteKikspotRecommendationLocation()");
		try{
			Integer deletedResult=this.recommendationService.deleteKikspotRecommendationLocationByLocationId(locationId);
			if(deletedResult>0){
				return "success";
			}
			else if(deletedResult==0){
				
				return "userCredsAvailable";
			}
			else{
				throw new Exception();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			log.info("Recommendation Location Does not Exists");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Deleting Location");
			return "error";
		}
	
	}
	/**
	 * Created By Bhagya On december 22nd,2015
	 * @param locationId
	 * @return
	 * 
	 * Method for getting the recommendation Location Images By Location Id
	 */
	@RequestMapping(value="/locationimages.do")
	public String getRecommendationLocationImagesBylocationId(@RequestParam("locationId")Integer locationId,Map<String,Object> map){
		log.info("inside getRecommendationLocationImagesBylocationId()");
		try{
			ArrayList<RecommendationLocationImagesDto> recommendationLocationImagesDtos=this.recommendationService.getRecommendationLocationImagesByLocationId(locationId);
			map.put("locationImages", recommendationLocationImagesDtos);
			return "recommendation/editLocationImages";
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found");
			return "error";
		}
		catch(RecommendationLocationImagesNotFoundException e){
			map.put("locationId", locationId);
			map.put("message", "Recommendation Location Images Not Found");
			return "recommendation/editLocationImages";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Getting Recommendation Location Images");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param imageId
	 * @param map
	 * @return
	 * 
	 * Method for deleting the Location Image
	 */
	@RequestMapping(value="/deletelocationimage.do")
	public String deleteRecommendationLocationImageByImageId(@RequestParam("locationImageId") Integer locationImageId,Map<String,Object> map,@RequestParam(value="locationId",required=false)Integer locationId){
		log.info("inside deleteRecommendationLocationImageByImageId()");
		try{
			Integer deletedResult=this.recommendationService.deleteRecommendationLocationImageById(locationImageId);
			if(deletedResult>0){
				return "redirect:/recommendations/locationimages.do?locationId="+locationId;
			}
			else{
				throw new Exception();
			}
		}
		catch(RecommendationLocationImagesNotFoundException e){
			map.put("message", "Recommendation Location Image Not Found For Deleting");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Deleting Recommendation Location Image");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param locationId
	 * @param locationImages
	 * @return
	 * 
	 * Method for Adding the Location Images For Recommendation Location
	 */
	@RequestMapping(value="/addlocationimages.do",method=RequestMethod.POST)
	public String addLocationImagesForRecommendationLocation(@RequestParam("locationId") Integer locationId,@RequestParam("locationImages") List<MultipartFile> locationImagesFile,Map<String,Object> map){
		log.info("inside addingLocationImagesForRecommendationLocation");
		try{
			Integer savedResult=this.recommendationService.addRecommendationLocationImagesforRecommendationLocation(locationId, locationImagesFile);
			if(savedResult>0){
				return "redirect:/recommendations/locationimages.do?locationId="+locationId;
			}
			else{
				throw new Exception();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			map.put("message", "Recommendation Location Not Found For Adding Location Images");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Adding Recommendation Location Images");
			return "error";
		}	
	}
	
	
	
	/**
	 * Created By bhagya On Feb 04th, 2016
	 * @param locationAPIId
	 * @param map
	 * @return
	 * 
	 * Method For Showing the location details of a location
	 */
	
	@RequestMapping(value="/locationdetails.do",method=RequestMethod.GET)
	public String getLocationDetailsByLocationAPIId(@RequestParam("locationAPIId") String locationAPIId,Map<String,Object> map){
		log.info("inside getLocationDetailsByLocationAPIId()");
		try{
			
			//RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(locationAPIId,null,null,null);
			RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(locationAPIId,null,null,null);
			
			map.put("location",recommendationLocationDto);
			return "recommendation/locationDetails";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Getting Recommendation Location Details");
			return "error";
		}		
	}
	
	
	/**
	 * Created by Jeevan on FEB 04, 2016
	 * Method to Toggle Location Rating From all loications
	 * 
	 * @param allowGlobalRating
	 * @return
	 */
	@RequestMapping("/togglelocationrating.do")
	@ResponseBody
	public String toggleLocationRating(@RequestParam("allowGlobalRating") Boolean allowGlobalRating){
		log.info("inside toggleLocationRating()");
		try{
			this.recommendationService.saveorUpdateLocationTogglerToDB(allowGlobalRating);
			return "success";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Toggling User Location Rating Request");
			return "Error While Toggling User Location Rating Request";
		}
		
	}
	
	
	/**
	 * added by Firdous on 1st march 2016
	 * method to change the status of a location
	 * @param locationId
	 * @param isOpen
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/changelocationstatus.do")
	public String changeLocationStatus(@RequestParam("locationId")Integer locationId,@RequestParam("isOpen")Boolean isOpen,@RequestParam("reason")String reason,@RequestParam("modifiedBy")String modifiedBy){
		log.info("inside changeloactionStatus()");
		try{
			Integer updatedResult=this.recommendationService.updateLocationStatus(locationId,isOpen,reason,modifiedBy);
			KikspotRecommendationLocationDto kikspotRecommendationLocationDto=this.recommendationService.getKikspotRecommendationLocationByLocationId(locationId);
			//System.out.println(" IS OPEN "+kikspotRecommendationLocationDto.getIsOpen());
			if(updatedResult>0 && kikspotRecommendationLocationDto.getIsOpen()==isOpen){
				return "success";
			}
			else if(updatedResult>0){
				return "missmatch";
			}
			else{
				return "error";
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			e.printStackTrace();
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	
	}
	/**
	 * 
	 * 
	 * @param locationAPIIds
	 * @param map
	 * @return
	 * Added by firdous on 5th April
	 * Method to share recommendation locations
	 * 
	 */
	@RequestMapping(value="/sharerecommendationlocations.do",method=RequestMethod.GET)
	public String shareRecommendationsList(@RequestParam("locationAPIId") String locationAPIIds,Map<String, Object> map) {
		log.info("inside shareRecommendationsList()");
		System.out.println("inside share recommendation locations");
		try{
			System.out.println(" server url "+serverURL);
			    String[] locationIds=locationAPIIds.split(",");
				List<String> locationAPIIdsList=new ArrayList<String>();
				for(int i=0;i<locationIds.length;i++){
					String locationAPIId=locationIds[i];
					locationAPIIdsList.add(locationAPIId);
				}
				ArrayList<RecommendationLocationDto> recommendationLocationDtos=this.recommendationService.getLocations(locationAPIIdsList);
				for(RecommendationLocationDto recommendationLocationDto:recommendationLocationDtos){
				
				if(null!=recommendationLocationDto.getIconUrl()){
					if(!recommendationLocationDto.getIconUrl().contains("https:")){
						try{
							//String iconURL="http://54.225.230.190:7090/var/kikspotImages/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
							String iconURL=serverURL+"/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
							recommendationLocationDto.setIconUrl(iconURL);
						}
						catch(Exception e){}
					}
			    }
				if(null!=recommendationLocationDto.getImage()){
					if(!recommendationLocationDto.getImage().contains("https:")){
						try{
							//String iconURL="http://54.225.230.190:7090/var/kikspotImages/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
							String imageURL=serverURL+"/recommendations/images/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getImage();
							recommendationLocationDto.setImage(imageURL);
						}
						catch(Exception e){}
					}
			    }
				}
				
				map.put("locations",recommendationLocationDtos);
				return "recommendation/viewSharedRecommendations";
				
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Getting Recommendation Location Details");
			return "error";
		}
		
	}
	
	/**
	 * 
	 * @param locationAPIId
	 * @param map
	 * @return
	 * Method to share the location comments
	 * 
	 */
	@RequestMapping(value="/sharelocationcomments.do",method=RequestMethod.GET)
	public String shareLocationComments(@RequestParam("locationAPIId") String locationAPIId,Map<String, Object> map) {
		log.info("inside  shareLocationComments()");
		try{
			RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(locationAPIId,null,null,null);
			try{
			   ArrayList<UserLocationRatingsDto> userLocationRatingsDto=this.recommendationService.getCommentsOfLocationsByLocationAPIId(locationAPIId);
			   if(null!=recommendationLocationDto.getIconUrl()){
					if(!recommendationLocationDto.getIconUrl().contains("https:")){
						try{
						//String iconURL="http://54.225.230.190:7090/var/kikspotImages/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
						String iconURL=serverURL+"/recommendations/icons/"+locationAPIId+"/"+recommendationLocationDto.getIconUrl();
						recommendationLocationDto.setIconUrl(iconURL);
						}
						catch(Exception e){}
						}
					}
					if(null!=recommendationLocationDto.getImage()){
						if(!recommendationLocationDto.getImage().contains("https:")){
						try{
						//String iconURL="http://54.225.230.190:7090/var/kikspotImages/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
						String imageURL=serverURL+"/recommendations/images/"+locationAPIId+"/"+recommendationLocationDto.getImage();
						recommendationLocationDto.setImage(imageURL);
						}
						catch(Exception e){}
					}
				}
			   map.put("userLocationRatings",userLocationRatingsDto);
			   map.put("rating","");
			}
					
			catch(UserLocationRatingsNotFoundException e){
				map.put("rating",recommendationLocationDto.getRatings());
			}
			String locationName=recommendationLocationDto.getLocationName().toUpperCase();
			recommendationLocationDto.setLocationName(locationName);
			map.put("location",recommendationLocationDto);
			return "recommendation/locationComments";
		}
		
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Getting Recommendation Location Details");
			return "error";
		}
		
	}
	
	/**
	 * Created By Bhagya on may21st, 2019
	 * @param masterId
	 * @param latitude
	 * @param longitude
	 * @param users
	 * @return
	 * 
	 * Method for to save the master geolocation configuration
	 */
	@RequestMapping(method=RequestMethod.POST,value="/savemastergeolocationconfiguration.do")
	public String saveRecommendationConfigurations(@RequestParam("masterId") Integer masterId, @RequestParam(value="latitude",required=false,defaultValue="false") Double latitude,
			 @RequestParam(value="longitude",required=false) Double longitude, @RequestParam(value="users",required=false)String users
			){
		try{
			List<String> usersList=new ArrayList<String>();
			if(null!=users && users.length()>0){
				usersList=Arrays.asList(users.split(","));
			}
			Integer savedResult=this.userService.saveMasterGeoLocationConfiguration(masterId,latitude, longitude, usersList);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "redirect:/recommendations/viewlocations.do";
				
	}
}