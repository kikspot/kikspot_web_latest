package com.kikspot.frontend.recommendation.dto;

import java.util.Date;

import com.kikspot.backend.user.model.KikspotUser;

public class UserGeoLocationRatingsDto{
	private Integer userGeoLocationRatingId;
	private KikspotUser kikspotUser;
	private Double latitude;
	private Double longitude;
	private Date geoRatingDate;
	public Integer getUserGeoLocationRatingId() {
		return userGeoLocationRatingId;
	}
	public void setUserGeoLocationRatingId(Integer userGeoLocationRatingId) {
		this.userGeoLocationRatingId = userGeoLocationRatingId;
	}
	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}
	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Date getGeoRatingDate() {
		return geoRatingDate;
	}
	public void setGeoRatingDate(Date geoRatingDate) {
		this.geoRatingDate = geoRatingDate;
	}
	
	
	
}