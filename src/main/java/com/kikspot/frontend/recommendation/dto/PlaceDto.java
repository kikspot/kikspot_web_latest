package com.kikspot.frontend.recommendation.dto;

import java.util.ArrayList;
/**
 * Created By Bhagya On November 26th, 2015
 *	Dto For Google Places API  And Yelp API,places
 */


public class PlaceDto{
	private String placeId;
	private String placeName;
	private Double latitude;
	private Double longitude;
	private String address;
	private String phoneNumber;
	private String internationalPhoneNumber;
	private String iconURL;
	private String imageURL;
	private String websiteURL;
	private Double rating;
	private String vicinity;
	private PriceDto price;
	private ArrayList<ReviewsDto> reviews;
	private Integer reviewCount;
	private ArrayList<PhotoDto> photos;
	private String weekdayHours;
	private String types;
	private StatusDto status;
	private String pageToken;
	private Boolean isNew;
	
	// added isOpen,googlePlaceId by bhagya on jan 09th, 2019
	private Boolean isOpen;
	private String googlePlaceId;
	
	public String getPlaceId() {
		return placeId;
	}
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getInternationalPhoneNumber() {
		return internationalPhoneNumber;
	}
	public void setInternationalPhoneNumber(String internationalPhoneNumber) {
		this.internationalPhoneNumber = internationalPhoneNumber;
	}
	public String getIconURL() {
		return iconURL;
	}
	public void setIconURL(String iconURL) {
		this.iconURL = iconURL;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getWebsiteURL() {
		return websiteURL;
	}
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating(Double rating) {
		this.rating = rating;
	}
	public String getVicinity() {
		return vicinity;
	}
	public void setVicinity(String vicinity) {
		this.vicinity = vicinity;
	}
	
	public PriceDto getPrice() {
		return price;
	}
	public void setPrice(PriceDto price) {
		this.price = price;
	}
	public ArrayList<ReviewsDto> getReviews() {
		return reviews;
	}
	public void setReviews(ArrayList<ReviewsDto> reviews) {
		this.reviews = reviews;
	}
	
	
	public Integer getReviewCount() {
		return reviewCount;
	}
	public void setReviewCount(Integer reviewCount) {
		this.reviewCount = reviewCount;
	}
	public ArrayList<PhotoDto> getPhotos() {
		return photos;
	}
	public void setPhotos(ArrayList<PhotoDto> photos) {
		this.photos = photos;
	}
	public String getWeekdayHours() {
		return weekdayHours;
	}
	public void setWeekdayHours(String weekdayHours) {
		this.weekdayHours = weekdayHours;
	}
	public String getTypes() {
		return types;
	}
	public void setTypes(String types) {
		this.types = types;
	}
	public StatusDto getStatus() {
		return status;
	}
	public void setStatus(StatusDto status) {
		this.status = status;
	}
	public String getPageToken() {
		return pageToken;
	}
	public void setPageToken(String pageToken) {
		this.pageToken = pageToken;
	}
	public Boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public Boolean getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}
	public String getGooglePlaceId() {
		return googlePlaceId;
	}
	public void setGooglePlaceId(String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}
	
	
}