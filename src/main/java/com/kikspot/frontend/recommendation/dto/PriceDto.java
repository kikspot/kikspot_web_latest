package com.kikspot.frontend.recommendation.dto;
/**
 * Created By Bhagya On November 26th,2015
 *	Enum for the Price at Google Places API
 */
public enum PriceDto{
	 FREE,
	 INEXPENSIVE,
	 MODERATE,
	 EXPENSIVE,
	 VERY_EXPENSIVE,
	 NONE
}