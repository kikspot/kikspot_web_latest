package com.kikspot.frontend.recommendation.dto;

import com.kikspot.backend.recommendation.model.UserRecommendationLocationImages;
import com.kikspot.frontend.user.dto.KikspotUserDto;

/**
 * Created By Bhagya On Jan 08th,2016
 *	Dto for User Recommendation Location Images
 */
public class UserRecommendationLocationImagesDto{
	
	private Integer userLocationImageId;
	private String image;
	private String locationId;
	private KikspotUserDto kikspotUserDto;
	
	public Integer getUserLocationImageId() {
		return userLocationImageId;
	}
	public void setUserLocationImageId(Integer userLocationImageId) {
		this.userLocationImageId = userLocationImageId;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public static UserRecommendationLocationImagesDto populateUserRecommendationLocationImagesDto(UserRecommendationLocationImages userRecommendationLocationImages){
		UserRecommendationLocationImagesDto userRecommendationLocationImagesDto=new UserRecommendationLocationImagesDto();
		userRecommendationLocationImagesDto.setUserLocationImageId(userRecommendationLocationImages.getUserLocationImageId());
		if(null!=userRecommendationLocationImages.getImage()){
			userRecommendationLocationImagesDto.setImage(userRecommendationLocationImages.getImage());
		}
		if(null!=userRecommendationLocationImages.getLocationId()){
			userRecommendationLocationImagesDto.setLocationId(userRecommendationLocationImages.getLocationId());
		}
		userRecommendationLocationImagesDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userRecommendationLocationImages.getKikspotUser()));
		return userRecommendationLocationImagesDto;
	}
	
}