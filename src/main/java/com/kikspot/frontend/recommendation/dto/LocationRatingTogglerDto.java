package com.kikspot.frontend.recommendation.dto;

import com.kikspot.backend.recommendation.model.LocationRatingToggler;


/**
 * Created by Jeevan on February 06, 2016
 * 
 * Dto for LocationRatingTogglerDto..
 * 
 * @author KNS-ACCONTS
 *
 */
public class LocationRatingTogglerDto {
	
	
	private Integer locationRatingTogglerId;
	private Boolean allowGlobalRating;
	
	
	public Integer getLocationRatingTogglerId() {
		return locationRatingTogglerId;
	}
	public void setLocationRatingTogglerId(Integer locationRatingTogglerId) {
		this.locationRatingTogglerId = locationRatingTogglerId;
	}
	public Boolean getAllowGlobalRating() {
		return allowGlobalRating;
	}
	public void setAllowGlobalRating(Boolean allowGlobalRating) {
		this.allowGlobalRating = allowGlobalRating;
	}
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on FEB 04, 2016
	 *  
	 *  Method to PopulateLocationRatingTogglerDto...
	 *  
	 *  
	 * @param locationRatingToggler
	 * @return
	 */
	public static LocationRatingTogglerDto populationLocationRatingTogglerDto(LocationRatingToggler locationRatingToggler){
		LocationRatingTogglerDto locationRatingTogglerDto=new LocationRatingTogglerDto();
		locationRatingTogglerDto.setLocationRatingTogglerId(locationRatingToggler.getLocationRatingTogglerId());
		locationRatingTogglerDto.setAllowGlobalRating(locationRatingToggler.getAllowGlobalRating());
		return locationRatingTogglerDto;
	}
	

}
