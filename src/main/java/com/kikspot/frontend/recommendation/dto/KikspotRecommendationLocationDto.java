package com.kikspot.frontend.recommendation.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;

/**
 *Created By Bhagya On december 18th,2015
 *	Dto for Kikspot Recommendation Location
 *	
 *	This Dto For recommendation Locations Which Are added by admin 
 *
 */
public class KikspotRecommendationLocationDto{
	private Integer locationId;

	private String googleLocationId;
	
	
	@NotBlank
	@NotNull
	private String locationName;
	
	private Double latitude;
	
	private Double longitude;
	
	private Double ratings;
	private String iconUrl;
	
	@NotBlank
	@NotNull
	private String address;
	@NotBlank
	@NotNull
	private String phoneNo;
	
	private Boolean isNew;
	@NotBlank
	@NotNull
	private String url;
	private Integer totalLocations;
	private Date locationDate;
	private Double distance;
	@DateTimeFormat
	private Date closeDate;
	private Boolean isOpen;
	private MultipartFile iconImage;
	private List<MultipartFile> locationImages=new ArrayList<MultipartFile>();
	
	private Date openDate;
	
	private String reason;
	private Date statusModifiedDate;
	private String createdBy;
	private String modifiedBy;
	
	public Boolean getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}
		
	public Date getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}
	
	public Integer getLocationId() {
		return locationId;
	}
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
	
	
	public String getGoogleLocationId() {
		return googleLocationId;
	}
	public void setGoogleLocationId(String googleLocationId) {
		this.googleLocationId = googleLocationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getRatings() {
		return ratings;
	}
	public void setRatings(Double ratings) {
		this.ratings = ratings;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Date getLocationDate() {
		return locationDate;
	}
	public void setLocationDate(Date locationDate) {
		this.locationDate = locationDate;
	}
	public Integer getTotalLocations() {
		return totalLocations;
	}
	public void setTotalLocations(Integer totalLocations) {
		this.totalLocations = totalLocations;
	}
	public MultipartFile getIconImage() {
		return iconImage;
	}
	public void setIconImage(MultipartFile iconImage) {
		this.iconImage = iconImage;
	}
	
	public List<MultipartFile> getLocationImages() {
		return locationImages;
	}
	public void setLocationImages(List<MultipartFile> locationImages) {
		this.locationImages = locationImages;
	}
	
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public Date getStatusModifiedDate() {
		return statusModifiedDate;
	}
	public void setStatusModifiedDate(Date statusModifiedDate) {
		this.statusModifiedDate = statusModifiedDate;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public static KikspotRecommendationLocationDto populateRecommendationLocationDto(KikspotRecommendationLocation recommendationLocation){
		KikspotRecommendationLocationDto recommendationLocationDto=new KikspotRecommendationLocationDto();
		recommendationLocationDto.setLocationId(recommendationLocation.getLocationId());
		recommendationLocationDto.setGoogleLocationId(recommendationLocation.getGoogleLocationId());
		recommendationLocationDto.setLocationName(recommendationLocation.getLocationName());
		recommendationLocationDto.setLatitude(recommendationLocation.getLatitude());
		recommendationLocationDto.setLongitude(recommendationLocation.getLongitude());
		recommendationLocationDto.setRatings(recommendationLocation.getRatings());
		recommendationLocationDto.setAddress(recommendationLocation.getAddress());
		recommendationLocationDto.setPhoneNo(recommendationLocation.getPhoneNo());
		recommendationLocationDto.setLocationDate(recommendationLocation.getLocationDate());
		recommendationLocationDto.setIconUrl(recommendationLocation.getIconUrl());
		if(null!=recommendationLocation.getCloseDate()){
			recommendationLocationDto.setCloseDate(recommendationLocation.getCloseDate());
		}
		recommendationLocationDto.setIsNew(recommendationLocation.getIsNew());
		recommendationLocationDto.setIsOpen(recommendationLocation.getIsOpen());
		recommendationLocationDto.setTotalLocations(recommendationLocation.getTotalLocations());
		recommendationLocationDto.setDistance(recommendationLocation.getDistance());
		recommendationLocationDto.setUrl(recommendationLocation.getUrl());
		if(null!=recommendationLocation.getOpenDate()){
			recommendationLocationDto.setOpenDate(recommendationLocation.getOpenDate());
		}
		recommendationLocationDto.setReason(recommendationLocation.getReason());
		recommendationLocationDto.setStatusModifiedDate(recommendationLocation.getStatusModifiedDate());
		recommendationLocationDto.setCreatedBy(recommendationLocation.getCreatedBy());
		recommendationLocationDto.setModifiedBy(recommendationLocation.getModifiedBy());
		return recommendationLocationDto;
		
	}
	
	
}