package com.kikspot.frontend.recommendation.dto;

import java.util.Date;
/***
 * Created By Bhagya on may 28th, 2018
 * @author KNS-ACCONTS
 * Dto class for Venue score data
 */
public class VenueScoreDataDto{
	
	private Integer dataId;
	private String venueId;
	private Double venueScore;
	private Date scoreDate;
	
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public String getVenueId() {
		return venueId;
	}
	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}
	
	public Double getVenueScore() {
		return venueScore;
	}
	public void setVenueScore(Double venueScore) {
		this.venueScore = venueScore;
	}
	public Date getScoreDate() {
		return scoreDate;
	}
	public void setScoreDate(Date scoreDate) {
		this.scoreDate = scoreDate;
	}
	
	
}