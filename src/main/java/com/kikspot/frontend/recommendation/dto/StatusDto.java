package com.kikspot.frontend.recommendation.dto;
/**
 * Created By Bhagya On November 27th,2015
 *	enum for status,at Google Places API
 */
public enum StatusDto{
	OPENED,
	CLOSED,
	NONE
}