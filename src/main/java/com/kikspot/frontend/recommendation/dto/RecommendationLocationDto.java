package com.kikspot.frontend.recommendation.dto;

import java.util.ArrayList;

import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.recommendation.model.RecommendationLocation;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;

/**
 * Created By Bhagya On November 23rd,2015
 *	Dto Class For Recommendation Location Model
 */
public class RecommendationLocationDto{
	private Integer locationId;
	private String locationAPIId;
	private String locationName;
	private Double latitude;
	private Double longitude;
	private Double ratings;
	private String iconUrl;
	private String image;
	private String address;
	private String phoneNo;
	private Boolean isNew;
	private String url;
	private Double distance;
	private String sortBy;
	private Boolean hasActiveRules;
	
	// Added trending by bhagya on may28th, 2018 - asper the client requirement task
	
	private String trending;
	
	
	public Boolean getHasActiveRules() {
		return hasActiveRules;
	}
	public void setHasActiveRules(Boolean hasActiveRules) {
		this.hasActiveRules = hasActiveRules;
	}



	//Added pageToken by bhagya on december 15th,2015
	private String pageToken;
	
	// Added hasActiveRewards by bhagya on august 13th, 2018
	
	private Boolean hasActiveRewards;
	private ArrayList<KikspotRewardsDto> kikspotRewardsDto;
	
	// Added gameRulesDto by bhagya on jan 08th, 2019
	private ArrayList<GameRulesDto> gameRulesDto;
	
	public Integer getLocationId() {
		return locationId;
	}
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
	public String getLocationAPIId() {
		return locationAPIId;
	}
	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getRatings() {
		return ratings;
	}
	public void setRatings(Double ratings) {
		this.ratings = ratings;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
		
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public String getPageToken() {
		return pageToken;
	}
	public void setPageToken(String pageToken) {
		this.pageToken = pageToken;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getTrending() {
		return trending;
	}
	public void setTrending(String trending) {
		this.trending = trending;
	}
		
	public Boolean getHasActiveRewards() {
		return hasActiveRewards;
	}
	public void setHasActiveRewards(Boolean hasActiveRewards) {
		this.hasActiveRewards = hasActiveRewards;
	}
	
	
	public ArrayList<KikspotRewardsDto> getKikspotRewardsDto() {
		return kikspotRewardsDto;
	}
	public void setKikspotRewardsDto(ArrayList<KikspotRewardsDto> kikspotRewardsDto) {
		this.kikspotRewardsDto = kikspotRewardsDto;
	}
	
	public ArrayList<GameRulesDto> getGameRulesDto() {
		return gameRulesDto;
	}
	public void setGameRulesDto(ArrayList<GameRulesDto> gameRulesDto) {
		this.gameRulesDto = gameRulesDto;
	}
	public static RecommendationLocationDto populateRecommendationLocationDto(RecommendationLocation recommendationLocation){
		RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
		recommendationLocationDto.setLocationId(recommendationLocation.getLocationId());
		recommendationLocationDto.setLocationAPIId(recommendationLocation.getLocationAPIId());
		recommendationLocationDto.setLocationName(recommendationLocation.getLocationName());
		recommendationLocationDto.setLatitude(recommendationLocation.getLatitude());
		recommendationLocationDto.setLongitude(recommendationLocation.getLongitude());
		recommendationLocationDto.setRatings(recommendationLocation.getRatings());
		recommendationLocationDto.setAddress(recommendationLocation.getAddress());
		recommendationLocationDto.setPhoneNo(recommendationLocation.getPhoneNo());
		recommendationLocationDto.setIconUrl(recommendationLocation.getIconUrl());
		recommendationLocationDto.setImage(recommendationLocation.getImage());
		recommendationLocationDto.setIsNew(recommendationLocation.getIsNew());
		recommendationLocationDto.setPageToken(recommendationLocation.getPageToken());
		return recommendationLocationDto;
		
	}
}