package com.kikspot.frontend.recommendation.dto;

import java.util.Date;

import com.kikspot.backend.recommendation.model.UserLocationRatings;
import com.kikspot.frontend.user.dto.KikspotUserDto;


/**
 * 
 * @author KNS-ACCONTS
 * 
 * Created by Jeevan on December 09, 2015
 * 
 *  Dto for UserLocationRatings..
 *  
 *
 */
public class UserLocationRatingsDto {
	
	
	private Integer userLocationRatingId;
	private KikspotUserDto kikspotUserDto;
	private RecommendationLocationDto recommendationLocationDto;
	private Integer userId;
	private Integer rating;
	private String comment;
	private Date ratingDate;
	private String locationAPIId;
	private Integer totalRatings;
	private Double latitude;
	private Double longitude;
	// Added image By bhagya on april 17th, 2018
	private String image;
	
	
	public Integer getUserLocationRatingId() {
		return userLocationRatingId;
	}
	public void setUserLocationRatingId(Integer userLocationRatingId) {
		this.userLocationRatingId = userLocationRatingId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public RecommendationLocationDto getRecommendationLocationDto() {
		return recommendationLocationDto;
	}
	public void setRecommendationLocationDto(
			RecommendationLocationDto recommendationLocationDto) {
		this.recommendationLocationDto = recommendationLocationDto;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getRatingDate() {
		return ratingDate;
	}
	public void setRatingDate(Date ratingDate) {
		this.ratingDate = ratingDate;
	}
	public String getLocationAPIId() {
		return locationAPIId;
	}
	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}	
	
	public Integer getTotalRatings() {
		return totalRatings;
	}
	public void setTotalRatings(Integer totalRatings) {
		this.totalRatings = totalRatings;
	}
		
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * Created by Jeevan on December 09, 2015
	 * 
	 * Method to populated To from Model..
	 * 
	 * @param userLocationRatings
	 * @return
	 */
	public static UserLocationRatingsDto populateUserLocationRatingsDto(UserLocationRatings userLocationRatings){
		UserLocationRatingsDto userLocationRatingsDto=new UserLocationRatingsDto();
		userLocationRatingsDto.setUserLocationRatingId(userLocationRatings.getUserLocationRatingId());
		userLocationRatingsDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userLocationRatings.getKikspotUser()));
		userLocationRatingsDto.setLocationAPIId(userLocationRatings.getLocationAPIId());
		userLocationRatingsDto.setRating(userLocationRatings.getRating());
		userLocationRatingsDto.setTotalRatings(userLocationRatings.getTotalRatings());
		if(null!=userLocationRatings.getRatingDate()){
			userLocationRatingsDto.setRatingDate(userLocationRatings.getRatingDate());
		}
		if(null!=userLocationRatings.getRecommendationLocation()){
			userLocationRatingsDto.setRecommendationLocationDto(RecommendationLocationDto.populateRecommendationLocationDto(userLocationRatings.getRecommendationLocation()));
		}
		if(null!=userLocationRatings.getComment()){
			userLocationRatingsDto.setComment(userLocationRatings.getComment());
		}
		return userLocationRatingsDto;
	}
	
	
	

}
