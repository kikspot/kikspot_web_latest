package com.kikspot.frontend.recommendation.dto;

import com.kikspot.backend.recommendation.model.RecommendationConfiguration;


/**
 * 
 * @author KNS-ACCONTS
 *
 */
public class RecommendationConfigurationDto {
	
	
	private Integer recommendationConfigurationId;
	
	private Boolean isBar;
		
	private Boolean isPub;
	
	private Boolean isNightClub;

	private Boolean isLounge;	

	private Boolean isCasino;
	
	private Boolean isBowlingAlley;

	public Integer getRecommendationConfigurationId() {
		return recommendationConfigurationId;
	}

	public void setRecommendationConfigurationId(
			Integer recommendationConfigurationId) {
		this.recommendationConfigurationId = recommendationConfigurationId;
	}

	public Boolean getIsBar() {
		return isBar;
	}

	public void setIsBar(Boolean isBar) {
		this.isBar = isBar;
	}

	public Boolean getIsPub() {
		return isPub;
	}

	public void setIsPub(Boolean isPub) {
		this.isPub = isPub;
	}

	public Boolean getIsNightClub() {
		return isNightClub;
	}

	public void setIsNightClub(Boolean isNightClub) {
		this.isNightClub = isNightClub;
	}

	public Boolean getIsLounge() {
		return isLounge;
	}

	public void setIsLounge(Boolean isLounge) {
		this.isLounge = isLounge;
	}

	public Boolean getIsCasino() {
		return isCasino;
	}

	public void setIsCasino(Boolean isCasino) {
		this.isCasino = isCasino;
	}

	public Boolean getIsBowlingAlley() {
		return isBowlingAlley;
	}

	public void setIsBowlingAlley(Boolean isBowlingAlley) {
		this.isBowlingAlley = isBowlingAlley;
	}

	/**
	 * 
	 * @param configuration
	 * @return
	 */
	
	public static RecommendationConfigurationDto populateRecommendationConfigurationDto(RecommendationConfiguration configuration){
		RecommendationConfigurationDto configurationDto=new RecommendationConfigurationDto();
		configurationDto.setRecommendationConfigurationId(configuration.getRecommendationConfigurationId());
		configurationDto.setIsBar(configuration.getIsBar());
		configurationDto.setIsBowlingAlley(configuration.getIsBowlingAlley());
		configurationDto.setIsCasino(configuration.getIsCasino());
		configurationDto.setIsLounge(configuration.getIsLounge());
		configurationDto.setIsNightClub(configuration.getIsNightClub());
		configurationDto.setIsPub(configuration.getIsPub());
		return configurationDto;
	}
	

}
