package com.kikspot.frontend.recommendation.dto;

import com.kikspot.backend.recommendation.model.UserRemovedLocation;
import com.kikspot.frontend.user.dto.KikspotUserDto;


/**
 * Created by Jeevan on 
 * 
 *  December 07, 2015
 *  
 *   Dto for UserRemovedLocationDto..
 *   
 *   
 * @author KNS-ACCONTS
 *
 */
public class UserRemovedLocationDto {
	
	private Integer removedLocationId;
	private KikspotUserDto kikspotUserDto;
	private RecommendationLocationDto recommendationLocationDto;
	private String locationAPIId;
	private String sortBy;
	
	
	
	public Integer getRemovedLocationId() {
		return removedLocationId;
	}
	public void setRemovedLocationId(Integer removedLocationId) {
		this.removedLocationId = removedLocationId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public RecommendationLocationDto getRecommendationLocationDto() {
		return recommendationLocationDto;
	}
	public void setRecommendationLocationDto(
			RecommendationLocationDto recommendationLocationDto) {
		this.recommendationLocationDto = recommendationLocationDto;
	}
	public String getLocationAPIId() {
		return locationAPIId;
	}
	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}
		
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	/**
	 * 
	 * @param userRemovedLocation
	 * @return
	 */
	public static UserRemovedLocationDto populateUserRemovedLocationDto(UserRemovedLocation userRemovedLocation){
		UserRemovedLocationDto userRemovedLocationDto=new UserRemovedLocationDto();
		userRemovedLocationDto.setRemovedLocationId(userRemovedLocation.getRemovedLocationId());
		userRemovedLocationDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userRemovedLocation.getKikspotUser()));
		userRemovedLocationDto.setLocationAPIId(userRemovedLocation.getLocationAPIId());
		userRemovedLocationDto.setSortBy(userRemovedLocation.getSortBy());
		if(null!=userRemovedLocation.getRecommendationLocation())
			userRemovedLocationDto.setRecommendationLocationDto(RecommendationLocationDto.populateRecommendationLocationDto(userRemovedLocation.getRecommendationLocation()));
		return userRemovedLocationDto;
	}

}
