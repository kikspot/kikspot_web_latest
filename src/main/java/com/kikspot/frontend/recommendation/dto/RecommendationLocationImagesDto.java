package com.kikspot.frontend.recommendation.dto;

import com.kikspot.backend.recommendation.model.RecommendationLocationImages;

/**
 * Created By Bhagya On December 21st,2015
 *	Dto class for Recommendation Location Images
 */
public class RecommendationLocationImagesDto{
	private Integer locationImageId;
	private KikspotRecommendationLocationDto kikspotRecommendationLocation;
	private String image;
	
	public Integer getLocationImageId() {
		return locationImageId;
	}
	public void setLocationImageId(Integer locationImageId) {
		this.locationImageId = locationImageId;
	}
	
	public KikspotRecommendationLocationDto getKikspotRecommendationLocation() {
		return kikspotRecommendationLocation;
	}
	public void setKikspotRecommendationLocation(KikspotRecommendationLocationDto kikspotRecommendationLocation) {
		this.kikspotRecommendationLocation = kikspotRecommendationLocation;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public static RecommendationLocationImagesDto populateRecommendationLocationImagesDto(RecommendationLocationImages recommendationLocationImages){
		RecommendationLocationImagesDto recommendationLocationImagesDto=new RecommendationLocationImagesDto();
		recommendationLocationImagesDto.setLocationImageId(recommendationLocationImages.getLocationImageId());
		recommendationLocationImagesDto.setKikspotRecommendationLocation(KikspotRecommendationLocationDto.populateRecommendationLocationDto(recommendationLocationImages.getKikspotRecommendationLocation()));
		recommendationLocationImagesDto.setImage(recommendationLocationImages.getImage());
		return recommendationLocationImagesDto;
	}
}