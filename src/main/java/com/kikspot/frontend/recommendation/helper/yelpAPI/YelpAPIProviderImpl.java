package com.kikspot.frontend.recommendation.helper.yelpAPI;

import java.util.ArrayList;






import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.exceptions.BusinessInformationUnavailableException;
import com.kikspot.frontend.recommendation.dto.PlaceDto;
import com.kikspot.frontend.recommendation.dto.ReviewsDto;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;




/**
 * 
 *Created By Bhagya On November 23rd,2015
 * Interacting with Yelp API Service and get the information about locations or places
 */
@Transactional
@Service("yelpAPIProvider")
public class YelpAPIProviderImpl implements YelpAPIProvider{
	
	private static Logger log=Logger.getLogger(YelpAPIProviderImpl.class);
	
	private String consumerKey;
	private String consumerSecret;
	private String token;
	private String tokenSecret;
	
	
	public String getConsumerKey() {
		return consumerKey;
	}


	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}


	public String getConsumerSecret() {
		return consumerSecret;
	}


	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getTokenSecret() {
		return tokenSecret;
	}


	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}
	//Get tokens here from Yelp developers site, Manage API access.
	 
	  
	/**
	 * Created By Bhagya On November 30th,2015
	 * 
	 * @return
	 * @throws Exception 
	 * 
	 * Method For Getting the Near By Places through accessing the yelp API
	 * 	Parsing the JSON Response
	 * 	populate the response to business dtos 
	 */
	 
	  public ArrayList<PlaceDto> getNearByPlacesFromYelpAPI(Double latitude,Double longitude) throws Exception{
		  log.info("inside yelpAPIProviderImpl -> getNearByPlacesFromYelpAPI()");
		  Yelp yelp = new Yelp(consumerKey, consumerSecret, token, tokenSecret); 
		   try{
		    	ArrayList<PlaceDto> placeDtos=new ArrayList<PlaceDto>();
		    	String response = yelp.search( latitude,longitude);
		    	JSONObject obj = new JSONObject(response);	
		    	JSONObject errorObject=obj.optJSONObject("error");
	   			if(null==errorObject){
			   			JSONArray businessesArray = (JSONArray) obj.get("businesses");
			   			for (int i = 0; i < businessesArray.length(); i++) {
			   				JSONObject businessObject=businessesArray.getJSONObject(i);
			   				//String businessId=businessObject.getString("id");
			   				PlaceDto placeDto=this.parsingYelpApiJSONResponseObject(businessObject);
			   				placeDtos.add(placeDto);
			   			}
			   				
				    } //if block
	   			else{
	   				String errorId=errorObject.optString("id");
	   				if(errorId.equalsIgnoreCase("BUSINESS_UNAVAILABLE")){
	   					System.out.println("ERROR : "+errorId +" " +errorObject.optString("text") );
	   					throw new BusinessInformationUnavailableException();
	   				}
	   				else{
	   					System.out.println("ERROR : "+errorId +" " +errorObject.optString("text") );
	   					throw new Exception();
	   				}
	   			}
	   		 return placeDtos;
		    }//try block
		    catch(JSONException e){
		    	e.printStackTrace();
		    	 throw e;
		    }
		   	
	  }
	  
	  
	  
	  /**
	   * Created By Bhagya On november 30th,2015
	   * @param businessId
	   * @return
	 * @throws Exception 
	 * 	Method for getting the business Information by business Id
	 */
	  
	  public PlaceDto getPlaceInformationByPlaceIdFromYelpAPI(String businessId) throws Exception{
		  log.info("inside yelpAPIProviderImpl -> getBusinessInformationByBusinessIdFromYelpAPI()");
		  Yelp yelp = new Yelp(consumerKey, consumerSecret, token, tokenSecret); 
		   try{
			   	String businessResponse= yelp.searchByBusinessId(businessId);
			   	JSONObject businessResponseObject = new JSONObject(businessResponse);
			   	JSONObject errorObject=businessResponseObject.optJSONObject("error");
	   			if(null==errorObject){
					PlaceDto placeDto=this.parsingYelpApiJSONResponseObject(businessResponseObject);
					return placeDto;
	   			}
	   			else{
	   				String errorId=errorObject.optString("id");
	   				if(errorId.equalsIgnoreCase("BUSINESS_UNAVAILABLE")){
	   					System.out.println("ERROR : "+errorId +" " +errorObject.optString("text") );
	   					throw new BusinessInformationUnavailableException();
	   				}
	   				else{
	   					System.out.println("ERROR : "+errorId +" " +errorObject.optString("text") );
	   					throw new Exception();
	   				}
	   			}
			
		  }
		  catch(JSONException e){
			  e.printStackTrace();
			  throw e;
		  }
		
	  }
	  /**
	   * created By Bhagya On November 30th,2015
	   * @param businessResponseObject
	   * @return
	   * @throws JSONException
	   * 
	   * Method for parsing the JSON REsponse of a yelp API
	   */
	  private PlaceDto parsingYelpApiJSONResponseObject(JSONObject businessResponseObject) throws JSONException{
		  try{
			    JSONObject locationObject=businessResponseObject.optJSONObject("location");
				JSONObject coordinateObject=locationObject.optJSONObject("coordinate");
				
			  	PlaceDto placeDto=new PlaceDto();
	   			placeDto.setPlaceId(businessResponseObject.getString("id"));
	   			placeDto.setPlaceName(businessResponseObject.getString("name"));
	   			placeDto.setRating(businessResponseObject.optDouble("rating"));
	   			placeDto.setPhoneNumber(businessResponseObject.optString("phone"));
	   			placeDto.setInternationalPhoneNumber(businessResponseObject.optString("display_phone"));
	   			placeDto.setLatitude(coordinateObject.optDouble("latitude"));
	   			placeDto.setLongitude(coordinateObject.optDouble("longitude"));
	   			//address
	   			JSONArray displayAddressArray=locationObject.optJSONArray("display_address");
				if(null!=displayAddressArray && displayAddressArray.length()>0){
					StringBuilder displayAddressStringBuilder=new StringBuilder();
					for(int j=0;j<displayAddressArray.length();j++){
						String displayAddress= (String) displayAddressArray.get(j);
						displayAddressStringBuilder.append(displayAddress+",");
					}
				displayAddressStringBuilder.deleteCharAt(displayAddressStringBuilder.lastIndexOf(","));
				placeDto.setAddress(displayAddressStringBuilder.toString());
				}
				
	   			
				//review count
				placeDto.setReviewCount(businessResponseObject.optInt("review_count"));
				
				//website URL
				placeDto.setWebsiteURL(businessResponseObject.optString("url"));
				
				//image URL
				placeDto.setImageURL(businessResponseObject.optString("image_url"));
				
				//categories 
				
				JSONArray categoriesJSONArray=businessResponseObject.optJSONArray("categories");
				StringBuilder categoriesBuilder=new StringBuilder();
					if(null!=categoriesJSONArray && categoriesJSONArray.length()>0){
						for(Integer m=0;m<categoriesJSONArray.length();m++){
							JSONArray catJsonArray=categoriesJSONArray.optJSONArray(m);
							categoriesBuilder.append(catJsonArray.optString(0)+",");
						}
						categoriesBuilder.deleteCharAt(categoriesBuilder.lastIndexOf(","));
						
					}
				placeDto.setTypes(categoriesBuilder.toString());
				
				//reviews
				//Reviews Details
				JSONArray reviewsJSONArray=businessResponseObject.optJSONArray("reviews");
				ArrayList<ReviewsDto> reviewsDtos=new ArrayList<ReviewsDto>();
		   				if(null!=reviewsJSONArray && reviewsJSONArray.length()>0){
		   					for(Integer g=0;g<reviewsJSONArray.length();g++){
		   						JSONObject reviewObject=reviewsJSONArray.getJSONObject(g);
		   						JSONObject userObject=reviewObject.optJSONObject("user");
		   						ReviewsDto reviewsDto=new ReviewsDto();
		   						reviewsDto.setText(reviewObject.optString("excerpt"));
		   						reviewsDto.setAuthor(userObject.optString("name"));
		   						reviewsDto.setProfilePhotoURL(userObject.optString("image_url"));
		   						reviewsDto.setRating(reviewObject.optInt("rating"));
		   						reviewsDto.setTime(reviewObject.getLong("time_created"));
		   						reviewsDtos.add(reviewsDto);
		   					}
		   				}
		   		placeDto.setReviews(reviewsDtos);
	   				
	   		return placeDto;
		  }
		  catch(JSONException e){
			  e.printStackTrace();
			  throw e;
		  }
		
	  }
}