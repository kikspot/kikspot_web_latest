package com.kikspot.frontend.recommendation.helper.googlePlacesAPI;

import java.io.BufferedReader;




import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.exceptions.InvalidRequestException;
import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.backend.exceptions.OverQueryLimitException;
import com.kikspot.backend.exceptions.PlaceNotFoundException;
import com.kikspot.backend.exceptions.RequestDeniedException;
import com.kikspot.backend.exceptions.UnknownErrorException;
import com.kikspot.backend.recommendation.model.RecommendationConfiguration;
import com.kikspot.frontend.recommendation.dto.PhotoDto;
import com.kikspot.frontend.recommendation.dto.PlaceDto;
import com.kikspot.frontend.recommendation.dto.PriceDto;
import com.kikspot.frontend.recommendation.dto.RecommendationConfigurationDto;
import com.kikspot.frontend.recommendation.dto.ReviewsDto;
import com.kikspot.frontend.recommendation.dto.StatusDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;





/**
 * 
 * Created By Bhagya On November 26th,2015
 *	Implementation Class For Google Places API Provider
 *
 */

@Transactional
@Service("googlePlacesAPIProvider")
public class GooglePlacesAPIProviderImpl implements GooglePlacesAPIProvider{
	
	private static Logger log=Logger.getLogger( GooglePlacesAPIProviderImpl.class);
	
	private String apiKey;
	private String photoURL;
	private String nearBySearchURL;
	private String placeDetailsURL;
	private String radius;
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public String getPhotoURL() {
		return photoURL;
	}

	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}
	
	public String getNearBySearchURL() {
		return nearBySearchURL;
	}

	public void setNearBySearchURL(String nearBySearchURL) {
		this.nearBySearchURL = nearBySearchURL;
	}

	public String getPlaceDetailsURL() {
		return placeDetailsURL;
	}

	public void setPlaceDetailsURL(String placeDetailsURL) {
		this.placeDetailsURL = placeDetailsURL;
	}
	
	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	ArrayList<PlaceDto> placeDtos=new ArrayList<PlaceDto>();
	
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	
	/**
	 * Created By Bhagya On November 26th,2015
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws Exception 
	 */
	
	public ArrayList<PlaceDto> getNearByPlacesByLatAndLng(Double latitude,Double longitude,String pageToken,String searchString) throws Exception{
		log.info("inside GooglePlacesAPIProvider ->getNearByPlacesByLatAndLng");
		String latLng=latitude.toString()+","+longitude.toString();
		String url=nearBySearchURL+latLng+"&key="+apiKey;
		
		//url+="&type=casino|night_club|bar";
		url+=this.populateQueryWithConfigurationTypes();
		/*if(null!=searchString&& searchString.trim().length()>1){
			url+="&radius=25000&keyword="+searchString;
		}
		else{
			url+="&radius="+radius;
		}*/
		if(null!=searchString&& searchString.trim().length()>1){
			/*In url both radius and rankby won't allow. Either rankby or radius need to use*/
			url+="&rankby=distance&keyword="+searchString;
			//url+="&radius="+radius+"&keyword="+searchString;
		}
		else{
			url+="&rankby=distance";
			//url+="&radius="+radius;
		}
		System.out.println(url);
		log.info("GOOGle PLACES URL "+url);
		placeDtos.clear();
		try{
			JSONObject responseObject=null;
			if(null!=pageToken && pageToken.trim().length()>0){
				String pageTokenUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken="+pageToken+"&key="+apiKey;
				try {
					Thread.sleep(2000); // Page tokens have a delay before they are available
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				responseObject=this.getPlacesObjectByRequestingApi(pageTokenUrl);
				
			 }
			 else{
				 responseObject=this.getPlacesObjectByRequestingApi(url);
			 }
			 placeDtos=this.parsingJSONObjectResult(responseObject);
			 System.out.println(" Place Dtos Size at LatLang service "+placeDtos.size());
			 return placeDtos;
		}
		catch(RequestDeniedException e){
			System.out.println(" Request was Denied because of lack of an invalid key parameter.");
			e.printStackTrace();
			throw new Exception();
		}
		catch(InvalidRequestException e){
			System.out.println("Invalid Request,generally indicates that the query (reference) is missing");
			e.printStackTrace();
			throw new Exception();
		}
		catch(NoResultsFoundException e){
			System.out.println("No Results Found At Requested Criteria");
			e.printStackTrace();
			throw new NoResultsFoundException();
		}
		catch(OverQueryLimitException e){
			System.out.println("Exceeds the Limit For Accessing Google Places API");
			e.printStackTrace();
			throw new Exception();
		}
		catch(PlaceNotFoundException e){
			System.out.println(" Place Not Found At Google Places API");
			e.printStackTrace();
			throw new Exception();
		}
		catch(UnknownErrorException e){
			System.out.println("Indicates a server-side error,trying again may be successful ");
			e.printStackTrace();
			throw new Exception();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		
	}
	
	
	
	private String populateQueryWithConfigurationTypes()throws Exception{
		log.info("inside populateQueryWithConfigurationTypes() ");
		StringBuilder query=new StringBuilder();
		RecommendationConfigurationDto configDto=this.recommendationService.getRecommendationsConfiguration();
		if(configDto.getIsBar()){
			
			if(query.indexOf("&type=")==0){
				query.append("|bar");
			}
			else{
				query.append("&type=bar");
			}
			
		}
		if(configDto.getIsBowlingAlley()){
			if(query.indexOf("&type=")==0){
				query.append("|bowling_alley");
			}
			else{
				query.append("&type=bowling_alley");
			}
			
		}
		
		if(configDto.getIsCasino()){
			if(query.indexOf("&type=")==0){
				query.append("|casino");
			}
			else{
				query.append("&type=casino");
			}
			
		}
		
		if(configDto.getIsNightClub()){
			if(query.indexOf("&type=")==0){
				query.append("|night_club");
			}
			else{
				query.append("&type=night_club");
			}
			
		}
		
		if(configDto.getIsPub()){
			query.append("&keyword=pub");
			if(configDto.getIsLounge()){
				query.append("|lounge");
			}
		}
		else if(configDto.getIsLounge()){
			
			query.append("&keyword=lounge");
		}
		
		System.out.println("config query"+query.toString());
		log.info(" Configuration List "+query.toString());
		
		return query.toString();
	}
	
	
	
	/**
	 * Created by Jeevan on February 04, 2016 based on model created by Bhagya
	 * 
	 * 
	 * Method to get List of all Prominent Locations Within a City.
	 * 
	 *  Range set to 25000 deliberately
	 * 
	 * 
	 * @param latitude
	 * @param longitude
	 * @param pageToken
	 * @return
	 * @throws Exception
	 */
	
	public ArrayList<PlaceDto> getNearByPlacesByLatAndLngforEntireCity(Double latitude,Double longitude,String pageToken,String searchString) throws Exception{
		log.info("inside GooglePlacesAPIProvider ->getNearByPlacesByLatAndLng");
		String latLng=latitude.toString()+","+longitude.toString();
		/*In url both radius and rankby won't allow. Either rankby or radius need to use*/
		//String url=nearBySearchURL+latLng+"&radius=25000&key="+apiKey;
		String url=nearBySearchURL+latLng+"&rankby=distance&key="+apiKey;
		
	//	url+="&type=casino|night_club|bar";
		System.out.println("inside get near by places");
		url+=this.populateQueryWithConfigurationTypes();
		if(null!=searchString&& searchString.trim().length()>1){
			url+="keyword="+searchString;
		}
		System.out.println(url);
		log.info("GOOGle PLACES URL "+url);
		placeDtos.clear();
		try{
			JSONObject responseObject=null;
			 responseObject=this.getPlacesObjectByRequestingApi(url);
			 placeDtos=this.parsingJSONObjectResult(responseObject);
			  pageToken=placeDtos.get(0).getPageToken();
			  System.out.println(pageToken);
			  String pageToken2=placeDtos.get(1).getPageToken();
			  System.out.println(pageToken2);
			  System.out.println(placeDtos.size());
			  System.out.println("PAGE TOKEN "+pageToken);
			  /** Modified  by bhagya on jan 07th, 2019.. Google place api request will return 20places at one request. Intially we are requesting thrice and 
			   * getting 60 places.. Now for handle the loading twice , we are requesting only twice. now.. 
			   * Modified the i value iteration from 2 to 1.
			   *  Before Modification for(int i=0;i<2;i++)
			   *  After Modification for(int i=0;i<1;i++)
			   */
			/*for(int i=0;i<1;i++){
				String pageTokenUrl=null;
				 Thread.sleep(2000);
				 
				pageTokenUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken="+pageToken+"&key="+apiKey;
				 
				 
				System.out.println(pageTokenUrl);
				responseObject=this.getPlacesObjectByRequestingApi(pageTokenUrl);
				ArrayList<PlaceDto> pageTokenPlaces=this.parsingJSONObjectResult(responseObject);
				placeDtos.addAll(pageTokenPlaces);
				if(i<1)
					pageToken=pageTokenPlaces.get(0).getPageToken();
				if(pageToken==""){
					i++;
				}
				System.out.println("PAGE "+pageToken);
				System.out.println(placeDtos.size());
			}*/
			System.out.println(" place dtos size "+ placeDtos.size());
			 return placeDtos;
		}
		catch(RequestDeniedException e){
			System.out.println(" Request was Denied because of lack of an invalid key parameter.");
			e.printStackTrace();
			throw new Exception();
		}
		catch(InvalidRequestException e){
			System.out.println("Invalid Request,generally indicates that the query (reference) is missing");
			e.printStackTrace();
			throw new Exception();
		}
		catch(NoResultsFoundException e){
			System.out.println("No Results Found At Requested Criteria");
			e.printStackTrace();
			throw new NoResultsFoundException();
		}
		catch(OverQueryLimitException e){
			System.out.println("Exceeds the Limit For Accessing Google Places API");
			e.printStackTrace();
			throw new Exception();
		}
		catch(PlaceNotFoundException e){
			System.out.println(" Place Not Found At Google Places API");
			e.printStackTrace();
			throw new Exception();
		}
		catch(UnknownErrorException e){
			System.out.println("Indicates a server-side error,trying again may be successful ");
			e.printStackTrace();
			throw new Exception();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		
	}
	
	
	
	
	/**
	 * Created By Bhagya On november 26th,2015
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * 
	 * Method For Getting the JSON Response Object By requesting the Google Place API
	 */
	public JSONObject getPlacesObjectByRequestingApi(String url) throws IOException, JSONException{
		log.info("inside GooglePlacesAPIProvider ->getPlacesObjectByRequestingApi() ");
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		JSONObject responseObject = new JSONObject(response.toString());
		return responseObject;
	}
	
	
	
	
	
	/***
	 * Created By Bhagya on November 26th, 2015
	 * @param responseObject
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 * 
	 * Method For Parsing the JSON Object of a List of Places
	 * 	If the next_page_token is exists,Getting the List Of all Maximum Places 
	 * @throws RequestDeniedException 
	 * @throws InvalidRequestException 
	 * @throws NoResultsFoundException 
	 * @throws PlaceNotFoundException 
	 * @throws OverQueryLimitException 
	 * @throws UnknownErrorException 
	 */
	public ArrayList<PlaceDto> parsingJSONObjectResult(JSONObject responseObject) throws JSONException, IOException, RequestDeniedException, InvalidRequestException, NoResultsFoundException, PlaceNotFoundException, OverQueryLimitException, UnknownErrorException{
		log.info("inside GooglePlacesAPIProvider -> parsingJSONObjectResult");
		String status=responseObject.getString("status");
		if(status.equalsIgnoreCase("OK")){
			JSONArray placesArray = (JSONArray)responseObject.get("results");
			for (int i = 0; i < placesArray.length(); i++) {
				JSONObject placeObject=placesArray.getJSONObject(i);
				JSONObject geometryObject=placeObject.getJSONObject("geometry");
				JSONObject latAndLngObject=geometryObject.getJSONObject("location");
				JSONObject openingHoursObject=placeObject.optJSONObject("opening_hours");
				PlaceDto placeDto=new PlaceDto();
				placeDto.setPlaceId(placeObject.getString("place_id"));
				placeDto.setPlaceName(placeObject.getString("name").replaceAll("\uFFFD", ""));
				placeDto.setLatitude(latAndLngObject.getDouble("lat"));
				placeDto.setLongitude(latAndLngObject.getDouble("lng"));
				placeDto.setIconURL(placeObject.optString("icon"));
				try{
					placeDto.setRating(placeObject.optDouble("rating"));
				}
				catch(Exception e){}
				if(placeObject.isNull("rating")){
					placeDto.setRating(4.0);
				}
				if(!placeObject.isNull("rating")){
					placeDto.setRating(placeObject.getDouble("rating"));
				}
				else{
					placeDto.setRating(4.0);
				}
				//Place Status
				 StatusDto placeStatus = StatusDto.NONE;
					if(null!=openingHoursObject){
						if(openingHoursObject.has("open_now")){
							 placeStatus = openingHoursObject.getBoolean("open_now") ? StatusDto.OPENED : StatusDto.CLOSED;
						 }
					}
			   placeDto.setStatus(placeStatus);
			   //Photo
			   ArrayList<PhotoDto> photoDtos=new ArrayList<PhotoDto>();
			   JSONArray jsonPhotos=placeObject.optJSONArray("photos");
			   if(null!=jsonPhotos && jsonPhotos.length()>0){
					for (Integer j = 0; j< jsonPhotos.length(); j++) {
						PhotoDto photoDto=new PhotoDto();
						JSONObject jsonPhoto = jsonPhotos.getJSONObject(j);
						photoDto.setReference(jsonPhoto.optString("photo_reference"));
						photoDto.setHeight(jsonPhoto.optInt("height"));
						photoDto.setWidth(jsonPhoto.optInt("width"));
						photoDtos.add(photoDto);
					}
				}
				placeDto.setPhotos(photoDtos);
				//Image URL
				if(photoDtos.size()>0){
					String photoReference=photoDtos.get(0).getReference();
					String imageUrl=photoURL+photoReference+"&key="+apiKey;
					placeDto.setImageURL(imageUrl);
				}
				placeDto.setVicinity(placeObject.optString("vicinity").replaceAll("\uFFFD", ""));
				//Place Types
				JSONArray placeTypes=placeObject.optJSONArray("types");
				StringBuilder placeTypesBuilder=new StringBuilder();
				if(null!=placeTypes && placeTypes.length()>0){
					for (Integer k = 0; k < placeTypes.length(); k++) {
				       	placeTypesBuilder.append(placeTypes.get(k)+",");
				    }
				placeTypesBuilder.deleteCharAt(placeTypesBuilder.lastIndexOf(","));
				}
			    
				placeDto.setTypes(placeTypesBuilder.toString());
				
				placeDto.setIsNew(false);
				placeDtos.add(placeDto);
			}
			if(!responseObject.isNull("next_page_token")){
				String pageToken=responseObject.getString("next_page_token");
				placeDtos.get(0).setPageToken(pageToken);
			}
			else{
				placeDtos.get(0).setPageToken("");
			}
		}
		else if(status.equalsIgnoreCase("REQUEST_DENIED")){
			throw new RequestDeniedException();		
		}
		else if(status.equalsIgnoreCase("ZERO_RESULTS")){
			throw new NoResultsFoundException();
		}
		else if(status.equalsIgnoreCase("INVALID_REQUEST")){
			throw new InvalidRequestException();
		}
		else if(status.equalsIgnoreCase("NOT_FOUND")){
			throw new PlaceNotFoundException();
		}
		else if(status.equalsIgnoreCase("OVER_QUERY_LIMIT")){
			throw new OverQueryLimitException();
		}
		else if(status.equalsIgnoreCase("UNKNOWN_ERROR")){
			throw new UnknownErrorException();
		}
		return placeDtos;
	}
	
	
	
	
	/**
	 * Created By Bhagya On November 26th,2015
	 * @param placeId
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * 
	 * Method For getting the Complete Information or Details Of a Place
	 * 	Requesting the Google API
	 * 	Parsing The JSON Response
	 * 	Populate the PLACE DTO
	 * @throws RequestDeniedException 
	 * @throws InvalidRequestException 
	 * @throws NoResultsFoundException 
	 * @throws PlaceNotFoundException 
	 * @throws OverQueryLimitException 
	 * @throws UnknownErrorException 
	 */
	
	public PlaceDto getPlaceDetailsByplaceId(String placeId) throws IOException, JSONException, RequestDeniedException, InvalidRequestException, NoResultsFoundException, PlaceNotFoundException, OverQueryLimitException, UnknownErrorException{
		log.info("inside GooglePlacesAPIProvider -> getPlaceDetailsByplaceId()");
		String placeDetailsCompleteURL=placeDetailsURL+placeId+"&key="+apiKey;
		System.out.println(" place details complete url "+placeDetailsCompleteURL);
		JSONObject placeDetailsResponse=this.getPlacesObjectByRequestingApi(placeDetailsCompleteURL);
		String status=placeDetailsResponse.getString("status");
		PlaceDto placeDto=new PlaceDto();
		if(status.equalsIgnoreCase("OK")){
			 try{
				JSONObject placeObject=placeDetailsResponse.getJSONObject("result");
				JSONObject geometryObject=placeObject.getJSONObject("geometry");
				JSONObject latAndLngObject=geometryObject.getJSONObject("location");
				JSONObject openingHoursObject=placeObject.optJSONObject("opening_hours");
				placeDto.setPlaceId(placeObject.getString("place_id"));
				placeDto.setPlaceName(placeObject.getString("name"));
				placeDto.setLatitude(latAndLngObject.optDouble("lat"));
				placeDto.setLongitude(latAndLngObject.optDouble("lng"));
				placeDto.setAddress(placeObject.optString("formatted_address"));
				placeDto.setPhoneNumber(placeObject.optString("formatted_phone_number"));
				placeDto.setInternationalPhoneNumber(placeObject.optString("international_phone_number"));
				
				try{
					placeDto.setRating(placeObject.optDouble("rating"));
				}
				catch(Exception e){}
				if(placeObject.isNull("rating")){
					placeDto.setRating(4.0);
				}
				if(!placeObject.isNull("rating")){
					placeDto.setRating(placeObject.getDouble("rating"));
				}
				else{
					placeDto.setRating(4.0);
				}
			
				placeDto.setVicinity(placeObject.optString("vicinity"));
				placeDto.setIconURL(placeObject.optString("icon"));
				placeDto.setWebsiteURL(placeObject.optString("website"));
				JSONArray jsonPhotos=placeObject.optJSONArray("photos");
				
				// List of Photos
				ArrayList<PhotoDto> photoDtos=new ArrayList<PhotoDto>();
				if(null!=jsonPhotos && jsonPhotos.length()>0){
					for (Integer i = 0; i < jsonPhotos.length(); i++) {
						PhotoDto photoDto=new PhotoDto();
						JSONObject jsonPhoto = jsonPhotos.getJSONObject(i);
						photoDto.setReference(jsonPhoto.optString("photo_reference"));
						photoDto.setHeight(jsonPhoto.optInt("height"));
						photoDto.setWidth(jsonPhoto.optInt("width"));
						photoDtos.add(photoDto);
					}
				}
				placeDto.setPhotos(photoDtos);
				//Image URL
				if(photoDtos.size()>0){
					String photoReference=photoDtos.get(0).getReference();
					String imageUrl=photoURL+photoReference+"&key="+apiKey;
					placeDto.setImageURL(imageUrl);
				}
				//week Day Schedule
				 StatusDto placeStatus = StatusDto.NONE;
				if(null!=openingHoursObject){
					if(openingHoursObject.has("open_now")){
						 placeStatus = openingHoursObject.getBoolean("open_now") ? StatusDto.OPENED : StatusDto.CLOSED;
					 }
					JSONArray weekDayArray=openingHoursObject.optJSONArray("weekday_text");
					 StringBuilder  weekDayBuilder=new StringBuilder();
					 if(null!=weekDayArray && weekDayArray.length()>0){
						for (Integer i = 0; i < weekDayArray.length(); i++) {
							weekDayBuilder.append(weekDayArray.get(i)+",");
						}
						weekDayBuilder.deleteCharAt(weekDayBuilder.lastIndexOf(","));
					 }
					
					placeDto.setWeekdayHours(weekDayBuilder.toString());
				}
				placeDto.setStatus(placeStatus);
				//Place Types
				JSONArray placeTypes=placeObject.optJSONArray("types");
				StringBuilder placeTypesBuilder=new StringBuilder();
				if(null!=placeTypes && placeTypes.length()>0){
					for (Integer i = 0; i < placeTypes.length(); i++) {
				       	placeTypesBuilder.append(placeTypes.get(i)+",");
				    }
				placeTypesBuilder.deleteCharAt(placeTypesBuilder.lastIndexOf(","));
				}
			    
				placeDto.setTypes(placeTypesBuilder.toString());
				//Reviews
				JSONArray jsonReviews = placeObject.optJSONArray("reviews");
				ArrayList<ReviewsDto> reviewsDtos=new ArrayList<ReviewsDto>();
				if(null!=jsonReviews && jsonReviews.length()>0){
					for(Integer i=0;i<jsonReviews.length();i++){
						JSONObject jsonReview = jsonReviews.getJSONObject(i);
						ReviewsDto reviewsDto=new ReviewsDto();		
						reviewsDto.setAuthor(jsonReview.optString("author_name"));
						reviewsDto.setAuthorUrl(jsonReview.optString("author_url"));
						reviewsDto.setProfilePhotoURL(jsonReview.optString("profile_photo_url"));
						reviewsDto.setRating(jsonReview.optInt("rating"));
						reviewsDto.setText(jsonReview.optString("text"));
						reviewsDto.setTime(jsonReview.optLong("time"));
						reviewsDtos.add(reviewsDto);
					}
				}
				placeDto.setReviews(reviewsDtos);
				placeDto.setReviewCount(reviewsDtos.size());
				//PRICE
				PriceDto price = PriceDto.NONE;
		        if (placeObject.has("price_level")){
		            price = PriceDto.values()[placeObject.getInt("price_level")];
		        }
		        placeDto.setPrice(price);
			 }
			 catch(JSONException e){
				e.printStackTrace();
				throw new JSONException(e.getMessage());
			 }
		}
		
		else if(status.equalsIgnoreCase("REQUEST_DENIED")){
			throw new RequestDeniedException();		
		}
		else if(status.equalsIgnoreCase("ZERO_RESULTS")){
			throw new NoResultsFoundException();
		}
		else if(status.equalsIgnoreCase("INVALID_REQUEST")){
			throw new InvalidRequestException();
		}
		else if(status.equalsIgnoreCase("NOT_FOUND")){
			throw new PlaceNotFoundException();
		}
		else if(status.equalsIgnoreCase("OVER_QUERY_LIMIT")){
			throw new OverQueryLimitException();
		}
		else if(status.equalsIgnoreCase("UNKNOWN_ERROR")){
			throw new UnknownErrorException();
		}
		return placeDto;
	}

	@Override
	public ArrayList<PlaceDto> getPlaceDetailsByApiId(List<String> locationAPIIdsList) throws Exception {
		
		return null;
	}
	
	
}