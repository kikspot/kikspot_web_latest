package com.kikspot.frontend.recommendation.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.frontend.admin.dto.MasterGeoLocationConfigurationDto;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.LocationRatingTogglerDto;
import com.kikspot.frontend.recommendation.dto.RecommendationConfigurationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationImagesDto;
import com.kikspot.frontend.recommendation.dto.UserLocationRatingsDto;
import com.notnoop.apns.ApnsService;

/**
 * Created By Bhagya On November 23rd,2015
 * Interface for the Recommendation Service
 */
public interface RecommendationService{
	public ArrayList<RecommendationLocationDto> getLocations(Integer userId,Double latitude,Double longitude,String pageToken,String sortBy,String searchString,Integer requestFilter) throws Exception;
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId,Double latitude, Double longitude,Integer userId) throws Exception;
	public Integer removeLocationFromUserList(Integer userId,String locationAPIId,String sortBy)throws Exception;
	public Integer saveUserLocationRatingtoDB(UserLocationRatingsDto userLocationRatingsDto) throws Exception;
	public Integer saveOrUpdateKikspotRecommendationLocation(KikspotRecommendationLocationDto kikspotRecommendationLocationDto) throws Exception;
	public KikspotRecommendationLocationDto getKikspotRecommendationLocationByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException;
	public ArrayList<KikspotRecommendationLocationDto> getKikspotRecommendationLocations(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRecommendationLocationNotFoundException;
	public KikspotRecommendationLocationDto getKikspotRecommendationLocationByKikspotLocationId(String kikspotLocationId) throws KikspotRecommendationLocationNotFoundException;
	public Integer deleteKikspotRecommendationLocationByLocationId(Integer locationId) throws Exception;
	public ArrayList<RecommendationLocationImagesDto> getRecommendationLocationImagesByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException, RecommendationLocationImagesNotFoundException;
	public Integer deleteRecommendationLocationImageById(Integer locationImageId) throws Exception;
	public Integer addRecommendationLocationImagesforRecommendationLocation(Integer locationId,List<MultipartFile> locationImagesFile) throws Exception;
	public UserLocationRatingsDto getMostRecentUserLocationRatingByUserIdAndLocationId(Integer userId, String locationAPIId) throws UserNotFoundException, UserLocationRatingsNotFoundException;
	public ArrayList<UserLocationRatingsDto> getLocationCommentsByLocationAPIId(String locationAPIId,Integer userId) throws UserLocationRatingsNotFoundException;
	public Integer saveUserRecommendationLocationImages(Integer userId,String locationId,List<MultipartFile> imageFiles,Double latitude,Double longitude,Integer userLocationRatingId) throws Exception;
	public double distance(double lat1, double lon1, double lat2, double lon2);
	public LocationRatingTogglerDto getLocationRatingTogglerfromDB()throws Exception;
	public Integer saveorUpdateLocationTogglerToDB(Boolean allowGlobalRating)throws Exception;
	public void refreshLocationsofUser(Integer userId,String sortBy)throws Exception;
	public Integer updateLocationStatus(Integer locationId, Boolean isOpen,String reason,String modifiedBy) throws Exception;
	public void getApplePushNotificationService() throws IOException;
	public void sendIOSUserRatedNotifications(String deviceToken,String notificationMessage,ApnsService service) throws InterruptedException;
	public RecommendationConfigurationDto getRecommendationsConfiguration() throws Exception;
	public Integer saveorUpdateRecommendationConfiguration(RecommendationConfigurationDto configDto)throws Exception;
	public ArrayList<RecommendationLocationDto> getLocations(List<String> locationAPIIdsList) throws Exception;
	public ArrayList<UserLocationRatingsDto> getCommentsOfLocationsByLocationAPIId(String locationAPIId) throws UserLocationRatingsNotFoundException, Exception;
	public Integer checkLocationTogglerConditionAndSaveRateALocation(UserLocationRatingsDto userLocationRatingsDto) throws Exception;
	public ArrayList<UserLocationRatingsDto> getLocationCommentsAndLocationImagesByLocationAPIId(String locationAPIId);
	public Integer getUserRatingByLocationId(String locationAPIId);
	public MasterGeoLocationConfigurationDto getMasterGeoLocationConfiguration() throws Exception;
}