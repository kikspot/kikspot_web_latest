package com.kikspot.frontend.recommendation.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.log4j.Logger;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kikspot.backend.admin.model.MasterGeoLocationConfiguration;
import com.kikspot.backend.exceptions.APIUnavailableException;
import com.kikspot.backend.exceptions.ApiScoreDataNotFoundException;
import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleInActiveException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.InvalidRequestException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.backend.exceptions.OverQueryLimitException;
import com.kikspot.backend.exceptions.PlaceNotFoundException;
import com.kikspot.backend.exceptions.RecommendationGeoLocationNotMatchedException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.backend.exceptions.RequestDeniedException;
import com.kikspot.backend.exceptions.UnknownErrorException;
import com.kikspot.backend.exceptions.UserDeviceNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.exceptions.UserRemovedLoctionsNotFoundException;
import com.kikspot.backend.exceptions.UsersScoreDataNotFoundException;
import com.kikspot.backend.exceptions.VenueScoreDataNotFoundException;
import com.kikspot.backend.game.dao.CredDao;
import com.kikspot.backend.game.dao.GameDao;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.NotificationTypes;
import com.kikspot.backend.notification.model.UserNotification;
import com.kikspot.backend.recommendation.dao.RecommendationDao;
import com.kikspot.backend.recommendation.model.ApiData;
import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;
import com.kikspot.backend.recommendation.model.LocationRatingToggler;
import com.kikspot.backend.recommendation.model.RecommendationConfiguration;
import com.kikspot.backend.recommendation.model.RecommendationLocationImages;
import com.kikspot.backend.recommendation.model.ScoreData;
import com.kikspot.backend.recommendation.model.UserLocationRatings;
import com.kikspot.backend.recommendation.model.UserRecommendationLocationImages;
import com.kikspot.backend.recommendation.model.UserRemovedLocation;
import com.kikspot.backend.recommendation.model.VenueScoreData;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.user.model.UserLocations;
import com.kikspot.frontend.admin.dto.MasterGeoLocationConfigurationDto;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.notification.service.NotificationService;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.LocationRatingTogglerDto;
import com.kikspot.frontend.recommendation.dto.PlaceDto;
import com.kikspot.frontend.recommendation.dto.RecommendationConfigurationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationImagesDto;
import com.kikspot.frontend.recommendation.dto.UserLocationRatingsDto;
import com.kikspot.frontend.recommendation.helper.googlePlacesAPI.GooglePlacesAPIProvider;
import com.kikspot.frontend.recommendation.helper.yelpAPI.YelpAPIProvider;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsDelegate;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;
import com.notnoop.apns.DeliveryError;



/**
 * Created By Bhagya On November 23rd, 2015
 * Implementation class for Recommendation Services
 */
@Transactional
@Service("recommendationService")
public class RecommendationServiceImpl implements RecommendationService{
	
	private static Logger log=Logger.getLogger(RecommendationServiceImpl.class);
	
	@Resource(name="googlePlacesAPIProvider")
	private GooglePlacesAPIProvider googlePlacesAPIProvider;
	
	@Resource(name="yelpAPIProvider")
	private YelpAPIProvider yelpAPIProvider;
	
	
	@Resource(name="userDao")
	private UserDao userDao;
	
	
	@Resource(name="recommendationDao")
	private RecommendationDao recommendationDao;
	
	@Resource(name="gameService")
	private GameService gameservice;
	
	@Resource(name="notificationService")
	private NotificationService notificationService;
	/*Added Game DAO By BHagya On Jan 04th,2015**/
	@Resource(name="gameDao")
	private GameDao gameDao;
	
	@Resource(name="credDao")
	private CredDao creadDao;
	
	private String locationImagePath;
	private String iconImagePath;
	private String userLocationImagePath;
	private Integer radius;
	private String unit;
	
	private String serverURL;
	
	private ArrayList<RecommendationLocationDto> globalRecommendationLocationDtos=new ArrayList<RecommendationLocationDto>();

	public String getServerURL() {
		return serverURL;
	}



	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}



	public String getLocationImagePath() {
		return locationImagePath;
	}



	public void setLocationImagePath(String locationImagePath) {
		this.locationImagePath = locationImagePath;
	}



	public String getIconImagePath() {
		return iconImagePath;
	}



	public void setIconImagePath(String iconImagePath) {
		this.iconImagePath = iconImagePath;
	}

	


	public String getUserLocationImagePath() {
		return userLocationImagePath;
	}



	public void setUserLocationImagePath(String userLocationImagePath) {
		this.userLocationImagePath = userLocationImagePath;
	}



	public Integer getRadius() {
		return radius;
	}



	public void setRadius(Integer radius) {
		this.radius = radius;
	}



	public String getUnit() {
		return unit;
	}



	public void setUnit(String unit) {
		this.unit = unit;
	}

	
	private static ApnsService service;


	/**
	 * Created By Bhagya On November 24th, 2015
	 * @param userId
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws Exception
	 * 
	 * Method for getting the near by location based on lat and lng from google places api
	 * Saving the recommendation locations to Db
	 * 
	 *  1. Remove user deleted Locations..
	 *  2. Get User Ratings
	 *  3. Get Trending from User.
	 *  4. Get Game Rules..
	 *  5. Handle Sorting..
	 *  
	 *  
	 * 
	 */
	public ArrayList<RecommendationLocationDto> getLocations(Integer userId,Double latitude,Double longitude,String pageToken,String sortBy,String searchString,Integer requestFilter) throws Exception{
		log.info("inside RecommendationServiceImpl->getLocationsAndSaveRecommendationLocations()");
		ArrayList<RecommendationLocationDto> recommendationLocationDtos=new ArrayList<RecommendationLocationDto>();
		//"Getting locations from Google..
		ArrayList<PlaceDto> placeDtos=new ArrayList<PlaceDto>();
		ArrayList<PlaceDto> placesDto=null;
		String pageToken1="";
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=new ArrayList<KikspotRecommendationLocation>();
		if(sortBy.equalsIgnoreCase("citywide")){
		
			for(int i=0;i<=2;i++){
			 placesDto=this.googlePlacesAPIProvider.getNearByPlacesByLatAndLngforEntireCity(latitude, longitude, pageToken,searchString);
			 pageToken=placesDto.get(0).getPageToken();
			 placeDtos.addAll(placesDto);
			 
			}
			if(placeDtos.size()>0){
				pageToken1=placeDtos.get(0).getPageToken();
			}
		}
		else{
		
			/*for(int i=0;i<=2;i++){*/ // for to return 20 places only
			try{
			placesDto=this.googlePlacesAPIProvider.getNearByPlacesByLatAndLng(latitude, longitude,pageToken,searchString);
			pageToken=placesDto.get(0).getPageToken();
			placeDtos.addAll(placesDto);
			 pageToken1=placeDtos.get(0).getPageToken();
			}
			catch(NoResultsFoundException e){
				
			}
			/*}*/
		}
		
		
		
		
			try{
				
				 kikspotRecommendationLocations=this.recommendationDao.getAllKikspotRecommendationLocationsNearbyLocation(latitude, longitude, null, null, null);
				
			    placeDtos.addAll(this.populatePlaceDtofromKikspotRecommendations(kikspotRecommendationLocations));
				}
		
			catch(Exception e){
				e.printStackTrace();
			}		
		
		Map<String, PlaceDto> placesMap=new LinkedHashMap<String, PlaceDto>();	
		
		
		
		Set<String> locationIds=new LinkedHashSet<String>();
		if(null!=placeDtos && placeDtos.size()>0){
			
		
		for(PlaceDto placeDto :placeDtos){
			if(null!=placeDto.getPlaceId() ){
				
				placesMap.put(placeDto.getPlaceId(), placeDto);
			
				
				
				locationIds.add(placeDto.getPlaceId());
				if(null!=placeDto.getIsOpen() && placeDto.getIsOpen()==false){
					placesMap.remove(placeDto.getGooglePlaceId());
					placesMap.remove(placeDto.getPlaceId());
				}
				else if(null!=placeDto.getIsOpen() && placeDto.getIsOpen()==true){
					placesMap.remove(placeDto.getGooglePlaceId());
					
				}
			 
			}
			else{
				throw new APIUnavailableException();
			}
			
		}
		// User Removed Locations
				try{
					ArrayList<UserRemovedLocation> userRemovedLocations=this.recommendationDao.getUserRemovedLocations(userId);
					for(UserRemovedLocation userRemovedLocation:userRemovedLocations){
						placesMap.remove(userRemovedLocation.getLocationAPIId());
					}
				}
				catch(UserRemovedLoctionsNotFoundException e){
				}
		
				
				
		
		Map<String, Double> ratingsMap=this.recommendationDao.getAllAverageUserLocationRatings();
		Set<String> activeGameLocations=this.gameDao.getActiveGameRulesOfGivenLocations(locationIds,userId);
		System.out.println("places map size"+placesMap.size());
		for(Entry<String, PlaceDto> entry: placesMap.entrySet()){
			PlaceDto placeDto=entry.getValue();
			RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
			/*System.out.println("PLACE "+placeDto.getPlaceName());
			System.out.println("ID "+placeDto.getPlaceId());
			System.out.println(placeDto.getLatitude() + " "+placeDto.getLongitude());
			System.out.println(" IS OPEN "+placeDto.getIsOpen() +" address "+placeDto.getAddress());*/
			
			if(null!=placeDto.getPlaceId()){
				try{
					recommendationLocationDto.setLocationAPIId(placeDto.getPlaceId());
					recommendationLocationDto.setLocationName(placeDto.getPlaceName());
					recommendationLocationDto.setLatitude(placeDto.getLatitude());
					recommendationLocationDto.setLongitude(placeDto.getLongitude());
					recommendationLocationDto.setIconUrl(placeDto.getIconURL());
					recommendationLocationDto.setImage(placeDto.getImageURL());
					recommendationLocationDto.setDistance(this.distance(latitude, longitude, recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude()));
					
					recommendationLocationDto.setUrl(placeDto.getWebsiteURL());
					recommendationLocationDto.setIsNew(placeDto.getIsNew());
					/*for(String ratingPlaceId:ratingsMap.keySet()){
						System.out.println("place id"+placeDto.getPlaceId());
						if(ratingPlaceId.equalsIgnoreCase(placeDto.getPlaceId())){
								recommendationLocationDto.setRatings(ratingsMap.get(entry.getKey()));
								System.out.println("ratings"+recommendationLocationDto.getRatings());
						}
			           	else{
					      //As Google API Give Ratings in scale of 5
						   recommendationLocationDto.setRatings(placeDto.getRating()*2);
					   } 
					}*/
					 // Save the API Score as based on client requirement which was mentioned in the trello task ..comparison with existed avg rating  and 
					//commented  the old code below
					 
					/*if(null!=ratingsMap.get(entry.getKey())){
						recommendationLocationDto.setRatings(ratingsMap.get(entry.getKey()));
					}
					else{
						//As Google API Give Ratings in scale of 5
							Double googleRating=placeDto.getRating()*2;
							recommendationLocationDto.setRatings(googleRating);
					}*/
					if(null!=ratingsMap.get(entry.getKey())){
					
						Double locationAvgRating=ratingsMap.get(entry.getKey());
						recommendationLocationDto.setRatings(locationAvgRating);
						// Added the venue score data implementation by bhagya on may 28th, 2018
						//If already exists in db.. Checking the previous score with the current score and then set the trending and updating the score  to db, otherwise creating the venue score in db.
						// If no user ratings are available in db means, reading the google place rating and set the trending 
						
						
						
						try{
							
							// checking  the venue score is available or not in db based on venueId or placeId
							VenueScoreData	venueScoreData=this.recommendationDao.getVenueScoreByVenueId(placeDto.getPlaceId());
							try{
							
							VenueScoreData existedVenueScoreData=this.recommendationDao.getVenueScoreOf30MinutesAgoByVenueId(placeDto.getPlaceId()); 
								if(locationAvgRating>existedVenueScoreData.getVenueScore()){
										
										recommendationLocationDto.setTrending("up");
									}
									else{
										
										recommendationLocationDto.setTrending("down");
									}
									
									existedVenueScoreData.setScoreDate(new Date());
									existedVenueScoreData.setVenueScore(locationAvgRating);
									this.recommendationDao.saveOrUpdateVenueScoreData(existedVenueScoreData);
							}
							catch(VenueScoreDataNotFoundException e){
								
								venueScoreData.setScoreDate(new Date());
								venueScoreData.setVenueScore(locationAvgRating);
								this.recommendationDao.saveOrUpdateVenueScoreData(venueScoreData);
								if(locationAvgRating>=6){
									recommendationLocationDto.setTrending("up");
								}
								else{
									recommendationLocationDto.setTrending("down");
								}
								
							}		
								
								
						}
						catch(VenueScoreDataNotFoundException e){
						
							VenueScoreData venueScoreData=new VenueScoreData();
							venueScoreData.setVenueId(placeDto.getPlaceId());
							venueScoreData.setScoreDate(new Date());
							venueScoreData.setVenueScore(locationAvgRating);
							this.recommendationDao.saveOrUpdateVenueScoreData(venueScoreData);
							if(locationAvgRating>=6){
								recommendationLocationDto.setTrending("up");
							}
							else{
								recommendationLocationDto.setTrending("down");
							}
						}
						
						
						
					}
					else{
					
					//As Google API Give Ratings in scale of 5
						Double googleRating=placeDto.getRating()*2;
						recommendationLocationDto.setRatings(googleRating);
						
						if(googleRating>=6){
							recommendationLocationDto.setTrending("up");
						}
						else{
							recommendationLocationDto.setTrending("down");
						}
						
						
					} 
					
					// Added the final score value as rating based on rating algorithm
					//Double finalScore=this.getAverageRatingofLocationBasedonRatingAlgorithm(recommendationLocationDto.getLocationAPIId(),recommendationLocationDto.getRatings());
					//System.out.println(" locationId "+recommendationLocationDto.getLocationAPIId() +" Location Name "+recommendationLocationDto.getLocationName() +" final Score "+finalScore);
					//recommendationLocationDto.setRatings(finalScore);
					if(activeGameLocations.contains(recommendationLocationDto.getLocationAPIId())){
						recommendationLocationDto.setHasActiveRules(true);
					}
					else{
						recommendationLocationDto.setHasActiveRules(false);
					}
					// Setting the address parameter by fetching the varaibale as vicinity
					recommendationLocationDto.setAddress(placeDto.getVicinity());
					
					// Setting the Active Rewards
					
					try{
						UserGameCredsDto gameCredDto=this.gameservice.getUsersGameCredsByUserId(userId);
						ArrayList<KikspotRewards> kikspotRewards=this.recommendationDao.getTheActiveRewardsByLevelAndLocationId(gameCredDto.getLevel(),recommendationLocationDto.getLocationAPIId());
						recommendationLocationDto.setHasActiveRewards(true);
					}
					catch(UserNotFoundException e){
						
					}
					catch(KikspotRewardNotFoundException e){
						recommendationLocationDto.setHasActiveRewards(false);
					}
					
					
					
				}
				catch(Exception e){}
				//System.out.println(" ds "+recommendationLocationDto.getLocationName()+" "+recommendationLocationDto.getRatings());
					recommendationLocationDtos.add(recommendationLocationDto);
					
			}
			else{
				throw new APIUnavailableException();
			}
			
		}
		
		}
		
			if(requestFilter==1){
				globalRecommendationLocationDtos.clear();
			}
			globalRecommendationLocationDtos.addAll(recommendationLocationDtos);
		
			// For to remove the duplicate values
		ArrayList<RecommendationLocationDto> newListDtos=new ArrayList<RecommendationLocationDto>();
		Map<String,RecommendationLocationDto> locationsMap= new HashMap<String, RecommendationLocationDto>();
		for(RecommendationLocationDto recommendationLocationDto:globalRecommendationLocationDtos){
			
			if(!locationsMap.containsKey(recommendationLocationDto.getLocationAPIId().trim())){
				newListDtos.add(recommendationLocationDto);
				locationsMap.put(recommendationLocationDto.getLocationAPIId().trim(), recommendationLocationDto);
			}
		}
		
		
		//Sorting Recommendations.
		//recommendationLocationDtos=this.sortRecommendationLocationDtos(recommendationLocationDtos, sortBy);
		//System.out.println(" global recommendation location dtos "+ newListDtos.size());
		recommendationLocationDtos=this.sortRecommendationLocationDtos( newListDtos, sortBy);
	//	System.out.println("size of location="+recommendationLocationDtos.size());
		if(recommendationLocationDtos.size()>0){
			recommendationLocationDtos.get(0).setPageToken(pageToken1);
		}
		return  recommendationLocationDtos; 
	}
	
	
	
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * MEthod to populated PlaceDto from RecommendationLocatios..
	 * 
	 * @param kikspotRecommendationLocations
	 * @return
	 */
	private ArrayList<PlaceDto> populatePlaceDtofromKikspotRecommendations(ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations){
		log.info("inside populatePlaceDtofromKikspotRecommendations()");
		ArrayList<PlaceDto> placeDtos=new ArrayList<PlaceDto>();
		for(KikspotRecommendationLocation recommendationLocation:kikspotRecommendationLocations){
			PlaceDto placeDto=new PlaceDto();
			placeDto.setAddress(recommendationLocation.getAddress().replaceAll("\uFFFD", ""));
			placeDto.setVicinity(recommendationLocation.getAddress().replaceAll("\uFFFD", "")); // considering the Google place ApI parameter as Address
			if(null!=recommendationLocation.getIconUrl() && recommendationLocation.getIconUrl().length()>0){
				String iconURL=serverURL+"/recommendations/icons/"+recommendationLocation.getLocationId()+"/"+recommendationLocation.getIconUrl();
				placeDto.setIconURL(iconURL);
				placeDto.setImageURL(iconURL); // using the icon url only as the image url.
			}
			else{
				placeDto.setIconURL("");
				placeDto.setImageURL("");
			}
			placeDto.setInternationalPhoneNumber(recommendationLocation.getPhoneNo());
			placeDto.setLatitude(recommendationLocation.getLatitude());
			placeDto.setLongitude(recommendationLocation.getLongitude());
			placeDto.setPhoneNumber(recommendationLocation.getPhoneNo());
		//	placeDto.setPlaceId(recommendationLocation.getKikspotLocationId());
			placeDto.setPlaceId(recommendationLocation.getLocationId().toString());
			placeDto.setGooglePlaceId(recommendationLocation.getGoogleLocationId());
			placeDto.setPlaceName(recommendationLocation.getLocationName().replaceAll("\uFFFD", ""));
			
			placeDto.setRating(recommendationLocation.getRatings());
			placeDto.setWebsiteURL(recommendationLocation.getUrl());
			
			placeDto.setIsOpen(recommendationLocation.getIsOpen());
			Calendar cal=Calendar.getInstance();
			cal.setTime(recommendationLocation.getLocationDate());
			cal.add(Calendar.DATE, 120);
			if(cal.getTime().after(new Date())){
				placeDto.setIsNew(true);
			}
			else{
				placeDto.setIsNew(false);
			}
			/*try{
			RecommendationLocationImages recommendationLocationImage=this.recommendationDao.getMostRecentRecommendationLocationImageByKikspotRecommendationLocation(recommendationLocation);
			placeDto.setImageURL(recommendationLocationImage.getImage());
			}
			catch(RecommendationLocationImagesNotFoundException e){
				
			}*/
			/*catch(RecommendationLocationImagesNotFoundException e){
				String image=serverURL+"/image/default.jpg";
				placeDto.setImageURL(image);
			}*/
			placeDtos.add(placeDto);
		}
		
		return placeDtos;
	}
	
	
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * MEthod to sort Recommendations as per user criteria..
	 * @param recommendationLocationDtos
	 * @param sortBy
	 * @return
	 */
	private ArrayList<RecommendationLocationDto> sortRecommendationLocationDtos(ArrayList<RecommendationLocationDto> recommendationLocationDtos,String sortBy) throws Exception{
		log.info("inside sortRecommendationLocationDtos()");
		//Default Sort by Distance... Asc..
		if(null==sortBy || sortBy.trim().length()<1 || sortBy.equalsIgnoreCase("close")){
			Collections.sort(recommendationLocationDtos, new Comparator<RecommendationLocationDto>() {
				@Override
				public int compare(RecommendationLocationDto o1,
						RecommendationLocationDto o2) {
					return o1.getDistance().compareTo(o2.getDistance());
				}
				
			});
		}
		else if(sortBy.equalsIgnoreCase("score")){
			Collections.sort(recommendationLocationDtos, new Comparator<RecommendationLocationDto>() {
				@Override
				public int compare(RecommendationLocationDto o1,
						RecommendationLocationDto o2) {
					return o2.getRatings().compareTo(o1.getRatings());
				}				
			});
			if(recommendationLocationDtos.size()>10){
				recommendationLocationDtos.subList(10, recommendationLocationDtos.size()).clear();
			}
		}
		else if(sortBy.equalsIgnoreCase("citywide")){
			Collections.sort(recommendationLocationDtos, new Comparator<RecommendationLocationDto>() {
				@Override
				public int compare(RecommendationLocationDto o1,
						RecommendationLocationDto o2) {
					return o2.getRatings().compareTo(o1.getRatings());
				}
				
			});
			if(recommendationLocationDtos.size()>10){
				recommendationLocationDtos.subList(10, recommendationLocationDtos.size()).clear();
			}
		}
		else if(sortBy.equalsIgnoreCase("trending")){
			Collections.sort(recommendationLocationDtos, new Comparator<RecommendationLocationDto>() {
				@Override
				public int compare(RecommendationLocationDto o1,
						RecommendationLocationDto o2) {
					 return new CompareToBuilder() // adding multiple fields comparison based on client requirement (rating and distance comparison)
					    .append(Math.round(o2.getRatings()), Math.round(o1.getRatings()))
		                .append(o1.getDistance(), o2.getDistance()).toComparison();
		               
		    }
				/*	return o2.getRatings().compareTo(o1.getRatings());
				}*/
				
			});
		}
		
		
		
		return recommendationLocationDtos;
	}
	
	
	
	
	
	
	/**
	 * Created By Bhagya On November 25th,2015
	 * @param locationAPIId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the details of lOcation By Location APIId from Google places API
	 */
	
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId,Double latitude,Double longitude,Integer userId) throws Exception{
		log.info("inside RecommendationServiceImpl->getLocationDetailsByLocationAPIId()");
		
		try{
			PlaceDto placeDto;
			try{
				Integer locationId=Integer.parseInt(locationAPIId);
				KikspotRecommendationLocation recommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
				placeDto=new PlaceDto();
				placeDto.setAddress(recommendationLocation.getAddress());
				placeDto.setIconURL(recommendationLocation.getIconUrl());
								/*if(null!=recommendationLocation.getIconUrl() && recommendationLocation.getIconUrl().length()>0){
					System.out.println(" inside if");
					String iconURL=serverURL+"/recommendations/icons/"+recommendationLocation.getLocationId()+"/"+recommendationLocation.getIconUrl();
					System.out.println(" iconUrL "+iconURL);
					placeDto.setIconURL(iconURL);
					placeDto.setImageURL(iconURL);
				}
				else{
					placeDto.setIconURL("");
					placeDto.setImageURL("");
				}*/
				
				placeDto.setInternationalPhoneNumber(recommendationLocation.getPhoneNo());
				placeDto.setLatitude(recommendationLocation.getLatitude());
				placeDto.setLongitude(recommendationLocation.getLongitude());
				placeDto.setPhoneNumber(recommendationLocation.getPhoneNo());
			//	placeDto.setPlaceId(recommendationLocation.getKikspotLocationId());
				placeDto.setPlaceId(recommendationLocation.getLocationId().toString());
				placeDto.setPlaceName(recommendationLocation.getLocationName().replaceAll("\uFFFD", ""));
				placeDto.setRating(recommendationLocation.getRatings());
				/** Modified by  bhagya on 07th Jan 2019** Added the Location avg rating as rating value **/
				Map<String, Double> ratingsMap=this.recommendationDao.getAllAverageUserLocationRatings();
				
			
				if(null!=ratingsMap.get(locationAPIId)){
					
					Double locationAvgRating=ratingsMap.get(locationAPIId);
					placeDto.setRating(locationAvgRating);
				}
				/***/
				placeDto.setWebsiteURL(recommendationLocation.getUrl());
				Calendar cal=Calendar.getInstance();
				cal.setTime(recommendationLocation.getLocationDate());
				cal.add(Calendar.DATE, 120);
				if(cal.getTime().after(new Date())){
					placeDto.setIsNew(true);
				}
				else{
					placeDto.setIsNew(false);
				}
				/*try{
					ArrayList<RecommendationLocationImagesDto> recommendationLocationImagesDtos=this.getRecommendationLocationImagesByLocationId(locationId);
					if(recommendationLocationImagesDtos.size()>0){
					String image=serverURL+"/recommendations/images/"+locationId+"/"+recommendationLocationImagesDtos.get(0).getImage();
					placeDto.setImageURL(image);
					}
					else{
						String image=serverURL+"/image/default.jpg";
						placeDto.setImageURL(image);
					}
				}
				catch(Exception e){
					String image=serverURL+"/image/default.jpg";
					placeDto.setImageURL(image);
				}*/
				try{
					RecommendationLocationImages recommendationLocationImage=this.recommendationDao.getMostRecentRecommendationLocationImageByKikspotRecommendationLocation(recommendationLocation);
					placeDto.setImageURL(recommendationLocationImage.getImage());
					}
					catch(RecommendationLocationImagesNotFoundException e){
						
					}
					/*catch(RecommendationLocationImagesNotFoundException e){
					String image=serverURL+"/image/default.jpg";
					placeDto.setImageURL(image);
					}*/
				
			}
			catch(NumberFormatException e){
				placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(locationAPIId);
			}
			RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
			if(null!=placeDto.getPlaceId()){
				recommendationLocationDto.setLocationAPIId(placeDto.getPlaceId());
				recommendationLocationDto.setLocationName(placeDto.getPlaceName().replaceAll("\uFFFD", ""));
				recommendationLocationDto.setLatitude(placeDto.getLatitude());
				recommendationLocationDto.setLongitude(placeDto.getLongitude());
				recommendationLocationDto.setRatings(placeDto.getRating());
				//recommendationLocationDto.setIconUrl(placeDto.getIconURL()); 
				// from google icon images are not coming ,so we are using the image of the location as icon image
				
				
				if(null!=placeDto.getImageURL()){
					
					if(!placeDto.getImageURL().contains("https:")){
						try{
						//String iconURL="http://54.225.230.190:7090/var/kikspotImages/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
						String imageURL=serverURL+"/recommendations/images/"+locationAPIId+"/"+placeDto.getImageURL();
						placeDto.setImageURL(imageURL);
						}
						catch(Exception e){}
					}
					
				}
				
					
				recommendationLocationDto.setImage(placeDto.getImageURL());
				
				recommendationLocationDto.setIconUrl(placeDto.getImageURL());
				
				if(null!=placeDto.getIconURL()){
										
					if(!placeDto.getIconURL().contains("https:")){
						try{
						//String iconURL="http://54.225.230.190:7090/var/kikspotImages/recommendations/icons/"+recommendationLocationDto.getLocationId()+"/"+recommendationLocationDto.getIconUrl();
						String iconURL=serverURL+"/recommendations/icons/"+locationAPIId+"/"+placeDto.getIconURL();
						recommendationLocationDto.setIconUrl(iconURL);
						}
						catch(Exception e){}
						}
										
					}
				
				recommendationLocationDto.setUrl(placeDto.getWebsiteURL());
				recommendationLocationDto.setPhoneNo(placeDto.getPhoneNumber());
				recommendationLocationDto.setAddress(placeDto.getAddress().replaceAll("\uFFFD", ""));
				recommendationLocationDto.setIsNew(false);
				if(null!=latitude && null!=longitude){
					recommendationLocationDto.setDistance(this.distance(latitude, longitude, recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude()));
				}
				/*else{
					throw new APIUnavailableException();
				}*/
				//setting the active rewards
				/*try{
					UserGameCredsDto gameCredDto=this.gameservice.getUsersGameCredsByUserId(userId);
					ArrayList<KikspotRewards> kikspotRewards=this.recommendationDao.getTheActiveRewardsByLevelAndLocationId(gameCredDto.getLevel(),recommendationLocationDto.getLocationAPIId());
					ArrayList<KikspotRewardsDto> kikspotRewardsDtos=new ArrayList<KikspotRewardsDto>();
					for(KikspotRewards kikspotReward: kikspotRewards){
						KikspotRewardsDto kikspotRewardsDto  =KikspotRewardsDto.populateKikspotRewards(kikspotReward);
					    kikspotRewardsDtos.add(kikspotRewardsDto);
					    recommendationLocationDto.setKikspotRewardsDto(kikspotRewardsDtos);
					}
				}
				catch(UserNotFoundException e){
					
				}
				
				catch(KikspotRewardNotFoundException e){
					System.out.println(" Kikspot Rewards not found exception");
				}*/
				try{
					if(null!=userId){
						ArrayList<GameRulesDto> gameRulesDtos=this.gameservice.getGameRulesByLocationIdAndUserId(recommendationLocationDto.getLocationAPIId(), userId);
						recommendationLocationDto.setGameRulesDto(gameRulesDtos);
					}
					else{
						ArrayList<GameRulesDto> gameRulesDtos=this.gameservice.getGameRulesByOnlyLocationId(recommendationLocationDto.getLocationAPIId());
						recommendationLocationDto.setGameRulesDto(gameRulesDtos);
					}
				}
				catch(GameRuleNotFoundException e){
					
				}
				
			}
			return recommendationLocationDto;	
		}
		/*catch(APIUnavailableException e){
			throw new APIUnavailableException();
		}*/
		catch(RequestDeniedException e){
			System.out.println(" Request was Denied because of lack of an invalid key parameter.");
			throw new Exception();
		}
		catch(InvalidRequestException e){
			System.out.println("Invalid Request,generally indicates that the query (reference) is missing or place Details doesn't exist for requested Place Id");
			throw new Exception();
		}
		catch(NoResultsFoundException e){
			System.out.println("No Results Found At Requested Criteria");
			throw new NoResultsFoundException();
		}
		catch(OverQueryLimitException e){
			System.out.println("Exceeds the Limit For Accessing Google Places API");
			throw new Exception();
		}
		catch(PlaceNotFoundException e){
			System.out.println(" Place Not Found At Google Places API");
			throw new Exception();
		}
		catch(UnknownErrorException e){
			System.out.println("Indicates a server-side error,trying again may be successful ");
			throw new Exception();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}	
	}
	
	
	
	
	
	/**
	 *  Created by Jeevan on December 07, 2015
	 *  
	 *  Method to getDistance between 2 locations based on Geocoodinates.
	 *  
	 *  Return result in meters
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @param unit
	 * @return
	 */
	 public double distance(double lat1, double lon1, double lat2, double lon2) {
		  double theta = lon1 - lon2;
		  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		  dist = Math.acos(dist);
		  dist = rad2deg(dist);
		  dist = dist * 60 * 1.1515;
		  //conversion of miles to KM and then to Meters
		  // dist = (dist * 1.609344)*1000;		 
		
		   BigDecimal bd=new BigDecimal(dist);
		   return  bd.setScale(2,RoundingMode.HALF_UP).doubleValue();
		}
	
	
	
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts decimal degrees to radians             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private double deg2rad(double deg) {
		  return (deg * Math.PI / 180.0);
		}
		
		
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts radians to decimal degrees             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private double rad2deg(double rad) {
		  return (rad * 180.0 / Math.PI);
		}
		
		
		
		public Integer removeLocationFromUserList(Integer userId,String locationAPIId,String sortBy)throws Exception{
			log.info("inside removeLocationFromUserList() ");
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
			UserRemovedLocation userRemovedLocation=new UserRemovedLocation();
			
			userRemovedLocation.setKikspotUser(kikspotUser);
			userRemovedLocation.setLocationAPIId(locationAPIId);
			userRemovedLocation.setSortBy(sortBy);
			return this.recommendationDao.saveOrUpdateUserRemovedLocation(userRemovedLocation);
			
		}
		
		
		/**
		 * Created by Jeevan on DECEMEBER 09, 2015
		 *  
		 *   Method to saveUserLocationRatingstoDB()..
		 * @param userLocationRatingsDto
		 * @return
		 * @throws Exception
		 * 
		
		 Modified By Bhagya On Jan 08th,2016
		 	Change the method to only save the UserLocationRating to DB (removed the update operation)
		 * 
		 */
		/*public Integer saveUserLocationRatingtoDB(UserLocationRatingsDto userLocationRatingsDto) throws Exception {
			log.info("inside saveUserLocationRatingtoDB() ");
			
			Integer locationRatingSavedResult;
			UserLocationRatings userLocationRatings=new UserLocationRatings();
			userLocationRatings.setKikspotUser(this.userDao.getKikspotUserById(userLocationRatingsDto.getUserId()));
			userLocationRatings.setComment(userLocationRatingsDto.getComment());
			userLocationRatings.setRatingDate(new Date());
			userLocationRatings.setRating(userLocationRatingsDto.getRating());
			userLocationRatings.setLocationAPIId(userLocationRatingsDto.getLocationAPIId());
			
			UserLocations userLocation=this.userDao.getUserLocationsOfUser(this.userDao.getKikspotUserById(userLocationRatingsDto.getUserId()));
			LocationRatingToggler locationRatingToggler=this.recommendationDao.getLocationRatingToggler();
			if(!locationRatingToggler.getAllowGlobalRating()){			
				RecommendationLocationDto recommendationLocationDto=this.getLocationDetailsByLocationAPIId(userLocationRatingsDto.getLocationAPIId());
				Double distance=this.distance(userLocationRatingsDto.getLatitude(), userLocationRatingsDto.getLongitude(), recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude());
				if(distance>0.05){
					throw new RecommendationGeoLocationNotMatchedException();
				}
			
				else{
					try{
						GameRules globalRule=this.gameDao.getGameRuleByActivityType("Rating");
						if(globalRule!=null){
							this.gameservice.updateUserGameCredsByGameRuleId(globalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
						}
					}
					catch(Exception e){
						e.printStackTrace();
					}
					try{
						ArrayList<GameRules> rules=this.gameDao.getGameRulesByLocationId(userLocationRatingsDto.getLocationAPIId());
						for(GameRules rule:rules){
							try{
								ArrayList<UserLocationRatings> userRatings=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> locationRatings=new ArrayList<UserLocationRatings>();
							//	ArrayList<UserLocationRatings> locationRatings1=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> locationComments=new ArrayList<UserLocationRatings>();
								SimpleDateFormat dateFormat=new SimpleDateFormat("HH:mm");
								SimpleDateFormat userDateFormat=new SimpleDateFormat("HH:mm");
								String userDateString=userDateFormat.format(new Date());
								Date userDate=dateFormat.parse(userDateString);
								String startTime=userDateFormat.format(rule.getStartTime());
								String endTime=userDateFormat.format(rule.getEndTime());
								Date starttime=dateFormat.parse(startTime);
								Date endtime=dateFormat.parse(endTime);
								SimpleDateFormat testDateFormat=new SimpleDateFormat("MM/dd/yyyy");
								SimpleDateFormat testDateFormat1=new SimpleDateFormat("MM/dd/yyyy");
								String currentDateString=testDateFormat.format(new Date());
								Date currentDate=testDateFormat1.parse(currentDateString);
							  try{
									userRatings=this.recommendationDao.getUserLocationRatingsByUserId(userLocationRatingsDto.getUserId());
									//locationRatings1=this.recommendationDao.getUserLocationRatingsByLocationAPIId(userLocationRatingsDto.getLocationAPIId());
									locationRatings=this.recommendationDao.getUserLocationRatingsByLocationAPIIdAndDate(userLocationRatingsDto.getLocationAPIId());
									locationComments=this.recommendationDao.getUserLocationCommentsByLocationId(userLocationRatingsDto.getLocationAPIId());
						  
								}
								catch(UserLocationRatingsNotFoundException e){
										System.out.println("user location rating not found");
								}
							  *//***
							   * If the rule has no open date and close or expiry date means ...we are not checking any conditions
							   * If the rule has no rule limit .. we are not checking any rule limit conditions
							   * So, By default conditionsRestrcitions=true for open date,close date and rule limit null means.
							   *//*
							  	Boolean conditionsRestrictions=true;
							  	if(null!=rule.getOpenDate()){
							  		if(rule.getOpenDate().after(new Date())){
							  			conditionsRestrictions=true;
							  		}
							  		else{
							  			conditionsRestrictions=false;
							  		}
							  	}
							  	if(null!=rule.getExpiryDate()){
							  		if(rule.getOpenDate().before(new Date())){
							  			conditionsRestrictions=true;
							  		}
							  		else{
							  			conditionsRestrictions=false;
							  		}
							  	}
							  	if(null!=rule.getRuleLimit()){
							  		if(rule.getRuleLimit()>0){
							  			conditionsRestrictions=true;
							  		}
							  		else{
							  			conditionsRestrictions=false;
							  		}
							  	}
							  	
								if(conditionsRestrictions==true && null!=rule.getGameRuleType()){
									
									if(rule.getGameRuleType().equalsIgnoreCase("First Time Rate")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationRatings.size()<1){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											}
										}
										else{
											this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
										}
									}
				
									if(rule.getGameRuleType().equalsIgnoreCase("First Nightly Rate")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0  && locationRatings.size()<=1){
										if(locationRatings.size()<1){
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
										
											else{
												if(userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
										
										else {
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													if(parseDate.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														}
													}
												}
											}
											else{
												String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
											
										}
									}
									if(rule.getGameRuleType().equalsIgnoreCase("First 10 Rating")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationRatings.size()<10){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											}
										}
										else{
											this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
										}
									}
										
									if(rule.getGameRuleType().equalsIgnoreCase("Second Rating")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationRatings.size()==1){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate) ){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
										}
										else{
											String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											if(parseDate.equals(currentDate) ){
												if(userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
					
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Third Rating") && rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationRatings.size()==2){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(locationRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
										}
										else{
											String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											String parse1=testDateFormat.format(locationRatings.get(1).getRatingDate());
											Date parseDate1=testDateFormat1.parse(parse1);
											if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)){
												if(userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Fourth Rating") && rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationRatings.size()==3){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(locationRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												String parse2=testDateFormat.format(locationRatings.get(2).getRatingDate());
												Date parseDate2=testDateFormat1.parse(parse2);
												if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)&&  parseDate2.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
										}
										else{
											String parse=testDateFormat.format(locationRatings.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											String parse1=testDateFormat.format(locationRatings.get(1).getRatingDate());
											Date parseDate1=testDateFormat1.parse(parse1);
											String parse2=testDateFormat.format(locationRatings.get(2).getRatingDate());
											Date parseDate2=testDateFormat1.parse(parse2);
											if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)&&  parseDate2.equals(currentDate)){
												if(userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
									}
				
									if(rule.getGameRuleType().equalsIgnoreCase("Rate A Second Spot Same Night")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && userRatings.size()==1 && !userRatings.get(0).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(userRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
										else{
											String parse=testDateFormat.format(userRatings.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
												this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											}
										}
										
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Rate A Third Spot Same Night")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && userRatings.size()==2){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(userRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												if(!userRatings.get(0).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !userRatings.get(1).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
													if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														}
													}
												}
											}
										}
										else{
											String parse=testDateFormat.format(userRatings.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											String parse1=testDateFormat.format(userRatings.get(1).getRatingDate());
											Date parseDate1=testDateFormat1.parse(parse1);
											if(!userRatings.get(0).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !userRatings.get(1).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
												if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
										}
					
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Rate A Fourth Spot Same Night")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && userRatings.size()==3){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(userRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												String parse2=testDateFormat.format(userRatings.get(2).getRatingDate());
												Date parseDate2=testDateFormat1.parse(parse2);
												if(!userRatings.get(0).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !userRatings.get(1).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !userRatings.get(2).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
													if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														}
													}
												}
											}
										}
										else{
											String parse=testDateFormat.format(userRatings.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											String parse1=testDateFormat.format(userRatings.get(1).getRatingDate());
											Date parseDate1=testDateFormat1.parse(parse1);
											String parse2=testDateFormat.format(userRatings.get(2).getRatingDate());
											Date parseDate2=testDateFormat1.parse(parse2);
											if(!userRatings.get(0).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !userRatings.get(1).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !userRatings.get(2).getLocationAPIId().equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
												if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
											
										}
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Comment")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											}
										}
										else{
											this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
										}
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Second Comment")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationComments.size()==1 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(locationComments.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
										else{
											String parse=testDateFormat.format(locationComments.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
												this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											}
										}
										
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Third Comment")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationComments.size()==2 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
										if(null!=rule.getUserId() ){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												String parse=testDateFormat.format(locationComments.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(locationComments.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													}
												}
											}
										}
										else{
											String parse=testDateFormat.format(locationComments.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											String parse1=testDateFormat.format(locationComments.get(1).getRatingDate());
											Date parseDate1=testDateFormat1.parse(parse1);
											if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
												if(userDate.before(endtime) && userDate.after(starttime)){
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
									}
									if(rule.getGameRuleType().equalsIgnoreCase("Fourth Comment")&& rule.getExpiryDate().compareTo(new Date())>0 && rule.getRuleLimit()>0 && locationComments.size()==3 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
										if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(locationComments.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													String parse1=testDateFormat.format(locationComments.get(1).getRatingDate());
													Date parseDate1=testDateFormat1.parse(parse1);
													String parse2=testDateFormat.format(locationComments.get(2).getRatingDate());
													Date parseDate2=testDateFormat1.parse(parse2);
													if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														}
													}
												}
										}
										else{
											String parse=testDateFormat.format(locationComments.get(0).getRatingDate());
											Date parseDate=testDateFormat1.parse(parse);
											String parse1=testDateFormat.format(locationComments.get(1).getRatingDate());
											Date parseDate1=testDateFormat1.parse(parse1);
											String parse2=testDateFormat.format(locationComments.get(2).getRatingDate());
											Date parseDate2=testDateFormat1.parse(parse2);
											if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
												if(userDate.before(endtime) && userDate.after(starttime)){								
													this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												}
											}
										}
									}
								} // MAIN IF FOR CHECKING RULE CONDITIONS
							}
							catch(NullPointerException e){
								e.printStackTrace();
							}
							catch(GameRuleExpiredException e){
								e.printStackTrace();
							}
							catch(GameRuleExceedsRuleLimitException e){
								e.printStackTrace();
							}
				
						}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			  }	// else condition
			
				 locationRatingSavedResult=this.recommendationDao.saveUserLocationRatings(userLocationRatings);
			//	getLocationDetailsByLocationAPIId(userLocationRatingsDto.getLocationAPIId());
				String message="Your Rating is Succesfully Submitted ";
				try{
					NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.RATING.toString());
					this.notificationService.sendNotificationToUser(userLocationRatingsDto.getUserId(), notificationType, message, userLocationRatings.getLocationAPIId());
				}
				catch(Exception e){
					e.printStackTrace();
				}
			} // IF CONDITION
			else{
				 locationRatingSavedResult=0;
			}
			return locationRatingSavedResult;
		}
		*/
		
		
		
		
		
		
		public Integer saveUserLocationRatingtoDB(UserLocationRatingsDto userLocationRatingsDto) throws Exception {
			log.info("inside saveUserLocationRatingtoDB() ");
			Integer locationRatingSavedResult=0;
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(userLocationRatingsDto.getUserId());
			UserLocationRatings userLocationRatings=new UserLocationRatings();
			userLocationRatings.setKikspotUser(kikspotUser);
			userLocationRatings.setComment(userLocationRatingsDto.getComment());
			userLocationRatings.setRatingDate(new Date());
			userLocationRatings.setRating(userLocationRatingsDto.getRating());
			
			userLocationRatings.setLocationAPIId(userLocationRatingsDto.getLocationAPIId());
			ArrayList<UserGameCredsDto> accomplishedRulesDtos=new ArrayList<UserGameCredsDto>();
			Integer oldCreds=0;
			Integer earnedCreds=0;
			Integer updatedCreds=0;
			
			try{
				UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
				oldCreds=userGameCreds.getCreds();
			}
			catch(UserGameCredNotFoundException e){
				
			}
			
			
					
					SimpleDateFormat testDateFormat=new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat testDateFormat1=new SimpleDateFormat("MM/dd/yyyy");
					String currentDateString=testDateFormat.format(new Date());
					Date currentDate=testDateFormat1.parse(currentDateString);
					
					SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
					SimpleDateFormat userDateFormat=new SimpleDateFormat("hh:mm a");
					String userDateString=userDateFormat.format(new Date());
					Date userDate=dateFormat.parse(userDateString);
					if(null==userLocationRatingsDto.getComment()||userLocationRatingsDto.getComment().length()==0) {
						System.out.println(" inside user ocmments condition");
					// GLOBAL RATING
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("Rating",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								System.out.println(" inside Global Rating");
								
									if(null!=filteredGlobalRule.getUserId()){
											if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
												System.out.println(" inside Global Rating rate with user condition satisfies");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									
									else{
										System.out.println(" inside Global  Rating if not comes under user");
										UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
										accomplishedRulesDtos.add(accomplishedRuleDto);
									}
								}
								
							
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					/// GLOBAL FIRST TIME RATE
					
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("First Time Rate",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								
								try{
									ArrayList<UserLocationRatings> locationRatings=this.recommendationDao.getUserLocationRatingsByLocationAPIIdAndDate(userLocationRatingsDto.getLocationAPIId());
																		
									}
							
								catch(UserLocationRatingsNotFoundException e){
									if(null!=filteredGlobalRule.getUserId()){
										if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
											System.out.println(" inside Global First Time Rating rate with user condition satisfies");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
								
									else{
										System.out.println(" inside Global First Time  Rating if not comes under user");
										UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
										accomplishedRulesDtos.add(accomplishedRuleDto);
									}
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
			
					/// GLOBAL FIRST NIGHTLY RATE
					
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("First Nightly Rate",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								if(filteredGlobalRule .getStartTime()==null){
									SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
									GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRule );
									ruleDto.setStartTime(timeformat.parse("05:00 PM"));
									this.gameservice.saveOrUpdateGameRule(ruleDto);
								}
								if(filteredGlobalRule .getEndTime()==null){
									SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
									GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRule );
									ruleDto.setEndTime(timeformat.parse("09:00 PM"));
									this.gameservice.saveOrUpdateGameRule(ruleDto);
								}
								try{

									ArrayList<UserLocationRatings> locationRatings=this.recommendationDao. getLocationRatingsByLocationAPIIdAndStartTimeAndEndTime(userLocationRatingsDto.getLocationAPIId(),filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime());									
									}
							
								catch(UserLocationRatingsNotFoundException e){
									String startTime=userDateFormat.format(filteredGlobalRule.getStartTime());
									String endTime=userDateFormat.format(filteredGlobalRule.getEndTime());
									Date starttime=dateFormat.parse(startTime);
									Date endtime=dateFormat.parse(endTime);
									if(null!=filteredGlobalRule.getUserId()){
										if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
											if(userDate.before(endtime) && userDate.after(starttime)){
												System.out.println(" inside Global First Nightly Rating rate with user condition satisfies");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									}
								
									else{
										if(userDate.before(endtime) && userDate.after(starttime)){
											System.out.println(" inside Global First Nightly  Rating if not comes under user");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					
					/// GLOBAL FIRST 10 RATINGS
					
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("First 10 Rating",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);		
						if(filteredGlobalRules.size()>0){	
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								if(filteredGlobalRule .getStartTime()==null){
									SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
									GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRule );
									ruleDto.setStartTime(timeformat.parse("05:00 PM"));
									this.gameservice.saveOrUpdateGameRule(ruleDto);
								}
								if(filteredGlobalRule .getEndTime()==null){
									SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
									GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRule );
									ruleDto.setEndTime(timeformat.parse("09:00 PM"));
									this.gameservice.saveOrUpdateGameRule(ruleDto);
								}
								String startTime=userDateFormat.format(filteredGlobalRule.getStartTime());
								String endTime=userDateFormat.format(filteredGlobalRule.getEndTime());
								Date starttime=dateFormat.parse(startTime);
								Date endtime=dateFormat.parse(endTime);
								try{

									ArrayList<UserLocationRatings> locationRatings=this.recommendationDao. getLocationRatingsByLocationAPIIdAndStartTimeAndEndTime(userLocationRatingsDto.getLocationAPIId(),filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime());
									
									if(locationRatings.size()<10){
										if(null!=filteredGlobalRule.getUserId()){
											if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
												if(userDate.before(endtime) && userDate.after(starttime)){
													System.out.println(" inside Global First 10 Rating rate with user condition satisfies");
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
										}
									
										else{
											if(userDate.before(endtime) && userDate.after(starttime)){
												System.out.println(" inside Global First 10  Rating if not comes under user");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									} //if 
								} // try 
								catch(UserLocationRatingsNotFoundException e){
									if(null!=filteredGlobalRule.getUserId()){
										if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
											if(userDate.before(endtime) && userDate.after(starttime)){
												System.out.println(" inside Global First 10 Rating rate with user condition satisfies");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									}
								
									else{
										if(userDate.before(endtime) && userDate.after(starttime)){
											System.out.println(" inside Global First 10  Rating if not comes under user");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					
					
					// GLOBAL USER FIRST RATING
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User First Rating",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							
							for(GameRules filteredGlobalRule:filteredGlobalRules){
							try{
									ArrayList<UserLocationRatings> userGlobalRatings=this.recommendationDao.getUserRatingsPerDayByStartTimeAndEndTimeAndUser(kikspotUser,filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									
								}
								catch(UserLocationRatingsNotFoundException e){
									if(null!=filteredGlobalRule.getUserId()){
												if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
										}
										
										else{
									
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
										
								}
							}//for loop
								
						}

							
						
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					
					
					// GLOBAL USER SECOND RATING
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User Second Rating",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							
							for(GameRules filteredGlobalRule:filteredGlobalRules){
							try{
									ArrayList<UserLocationRatings> userGlobalRatings=this.recommendationDao.getUserRatingsPerDayByStartTimeAndEndTimeAndUser(kikspotUser,filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									if(userGlobalRatings.size()==1){
										if(null!=filteredGlobalRule.getUserId()){
											if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									
										else{
									
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
								
									}
								}
								catch(UserLocationRatingsNotFoundException e){
									
								}
							}//for loop
								
						}

							
						
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					// GLOBAL USER THIRD RATING
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User Third Rating",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							
							for(GameRules filteredGlobalRule:filteredGlobalRules){
							try{
									ArrayList<UserLocationRatings> userGlobalRatings=this.recommendationDao.getUserRatingsPerDayByStartTimeAndEndTimeAndUser(kikspotUser,filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									if(userGlobalRatings.size()==2){
										if(null!=filteredGlobalRule.getUserId()){
											if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									
										else{
									
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
								
									}
								}
								catch(UserLocationRatingsNotFoundException e){
									
								}
							}//for loop
								
						}

							
						
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					// GLOBAL USER FOURTH RATING
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User Fourth Rating",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							
							for(GameRules filteredGlobalRule:filteredGlobalRules){
							try{
									ArrayList<UserLocationRatings> userGlobalRatings=this.recommendationDao.getUserRatingsPerDayByStartTimeAndEndTimeAndUser(kikspotUser,filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									if(userGlobalRatings.size()==3){
										if(null!=filteredGlobalRule.getUserId()){
											if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									
										else{
									
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
								
									}
								}
								catch(UserLocationRatingsNotFoundException e){
									
								}
							}//for loop
								
						}

							
						
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
				//// GLOBAL RATE A SECOND SPOT SAME NIGHT
									try{
											ArrayList<GameRules> GlobalRulesSecondSpot=this.gameDao.getGlobalGameRuleforRatingCondition("Rate A Second Spot Same Night",userLocationRatingsDto.getUserId());
											ArrayList<GameRules> filteredGlobalRulesSecondSpot=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(GlobalRulesSecondSpot);
											if(filteredGlobalRulesSecondSpot.size()>0){
												for(GameRules filteredGlobalRuleSecondSpot:filteredGlobalRulesSecondSpot){
													
														System.out.println(" if it is rate a second spot at GLOBAL");
														ArrayList<UserLocationRatings> userNightRatings=new ArrayList<UserLocationRatings>();
														if(filteredGlobalRuleSecondSpot .getStartTime()==null ){
															SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
															GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRuleSecondSpot );
															ruleDto.setStartTime(timeformat.parse("05:00 PM"));
															this.gameservice.saveOrUpdateGameRule(ruleDto);
														}
														if(filteredGlobalRuleSecondSpot .getEndTime()==null){
															SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
															GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRuleSecondSpot );
															ruleDto.setEndTime(timeformat.parse("09:00 PM"));
															this.gameservice.saveOrUpdateGameRule(ruleDto);
														}
														try{
															
															userNightRatings=this.recommendationDao.getUserLocationRatingsByUserIdAndStartTimeAndEndTime(userLocationRatingsDto.getUserId(), filteredGlobalRuleSecondSpot.getStartTime(), filteredGlobalRuleSecondSpot.getEndTime());
															System.out.println(" SIZE OF USER NIGHT RATINGS AT GLOBAL "+userNightRatings.size());
															if(userNightRatings.size()>=1){
																System.out.println(" SIZE satisfies at GLOBAL SECOND RATING");
																Set<String> locationIds=new HashSet<String>();
																for(UserLocationRatings rating:userNightRatings){
																	locationIds.add(rating.getLocationAPIId());
																}
																ArrayList<String> locationIDSUnique=new ArrayList<String>();
																for(String Id:locationIds){
																	locationIDSUnique.add(Id);
																}
																System.out.println(" location IDS Unique Size "+locationIDSUnique.size());
														
															if( locationIDSUnique.size()==1 &&!locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
																System.out.println(" if satisfies uniques IDS Condition "+locationIDSUnique.get(0) +" user APIID "+userLocationRatingsDto.getLocationAPIId());
																String startTime=userDateFormat.format(filteredGlobalRuleSecondSpot.getStartTime());
																String endTime=userDateFormat.format(filteredGlobalRuleSecondSpot.getEndTime());
																Date starttime=dateFormat.parse(startTime);
																Date endtime=dateFormat.parse(endTime);
																if(null!=filteredGlobalRuleSecondSpot.getUserId()){
																	if(filteredGlobalRuleSecondSpot.getUserId()==userLocationRatingsDto.getUserId()){
																		String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
																		Date parseDate=testDateFormat1.parse(parse);
																		if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
																			System.out.println(" inside rate a second spot same night with satisfies user condition at Global");
																			UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRuleSecondSpot.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																			accomplishedRulesDtos.add(accomplishedRuleDto);
																		}
																	}
																}
																else{
																	String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
																	Date parseDate=testDateFormat1.parse(parse);
																	if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
																		System.out.println(" inside rate a second spot same night with satisfies  condition at Global ");
																		UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRuleSecondSpot.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																		accomplishedRulesDtos.add(accomplishedRuleDto);
																	}
																}
																
															}
															}
													  }
													  catch(UserLocationRatingsNotFoundException e){
														 // e.printStackTrace();
													  }
													  catch(Exception e){
														  e.printStackTrace();
													  }
											
												}// For Loop
											} //Main If
									} // MAin Try BLOCK
											catch(GameRuleNotFoundException e){
												//e.printStackTrace();
											}
											catch(Exception e){
												e.printStackTrace();
											}
									
								
								
									////GLOBAL  RATE A THIRD SPOT SAME NIGHT
									try{
										ArrayList<GameRules> GlobalRulesThirdSpot=this.gameDao.getGlobalGameRuleforRatingCondition("Rate A Third Spot Same Night",userLocationRatingsDto.getUserId());
										ArrayList<GameRules> filteredGlobalRulesThirdSpot=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(GlobalRulesThirdSpot);
										if(filteredGlobalRulesThirdSpot.size()>0){
											for(GameRules filteredGlobalRuleThirdSpot:filteredGlobalRulesThirdSpot){
												System.out.println(" if it is rate a third spot at Global ");
												ArrayList<UserLocationRatings> userNightRatings=new ArrayList<UserLocationRatings>();
												if(filteredGlobalRuleThirdSpot .getStartTime()==null ){
													SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
													GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRuleThirdSpot );
													ruleDto.setStartTime(timeformat.parse("05:00 PM"));
													this.gameservice.saveOrUpdateGameRule(ruleDto);
												}
												if(filteredGlobalRuleThirdSpot .getEndTime()==null ){
													SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
													GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRuleThirdSpot);
													ruleDto.setEndTime(timeformat.parse("09:00 PM"));
													this.gameservice.saveOrUpdateGameRule(ruleDto);
												}
												try{
													 
													userNightRatings=this.recommendationDao.getUserLocationRatingsByUserIdAndStartTimeAndEndTime(userLocationRatingsDto.getUserId(), filteredGlobalRuleThirdSpot.getStartTime(), filteredGlobalRuleThirdSpot.getEndTime());
													if(userNightRatings.size()>=1){
														
														Set<String> locationIds=new HashSet<String>();
														for(UserLocationRatings rating:userNightRatings){
															locationIds.add(rating.getLocationAPIId());
														}
														ArrayList<String> locationIDSUnique=new ArrayList<String>();
														for(String Id:locationIds){
															locationIDSUnique.add(Id);
														}
												
													if( locationIDSUnique.size()==2 && !locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(1).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
														String startTime=userDateFormat.format(filteredGlobalRuleThirdSpot.getStartTime());
														String endTime=userDateFormat.format(filteredGlobalRuleThirdSpot.getEndTime());
														Date starttime=dateFormat.parse(startTime);
														Date endtime=dateFormat.parse(endTime);
														if(null!=filteredGlobalRuleThirdSpot.getUserId()){
															if(filteredGlobalRuleThirdSpot.getUserId()==userLocationRatingsDto.getUserId()){
																
																	String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
																	Date parseDate=testDateFormat1.parse(parse);
																	if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
																		System.out.println(" inside rate a third spot same night with satisfies user condition at Global ");
																		UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRuleThirdSpot.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																		accomplishedRulesDtos.add(accomplishedRuleDto);
																	}
																
															}
														}
														else{
															String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
															Date parseDate=testDateFormat1.parse(parse);
															
																if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
																	System.out.println(" inside rate a third spot same night with satisfies  condition at global");
																	UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRuleThirdSpot.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																	accomplishedRulesDtos.add(accomplishedRuleDto);
																}
															
														}
														
													}
													}
											  }
											  catch(UserLocationRatingsNotFoundException e){
											  }
											}// FOR LOOP
										}// MAIN IF
										
								}// MAIn TRY BLOCK
								catch(GameRuleNotFoundException e){
									
								}
								catch(Exception e){
									
								}
									
								////GLOBAL  RATE A FOURTH SPOT SAME NIGHT
									try{
										ArrayList<GameRules> GlobalRulesFourthSpot=this.gameDao.getGlobalGameRuleforRatingCondition("Rate A Fourth Spot Same Night",userLocationRatingsDto.getUserId());
										ArrayList<GameRules> filteredGlobalRulesFourthSpot=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(GlobalRulesFourthSpot);
										if(filteredGlobalRulesFourthSpot.size()>0){
											for(GameRules filteredGlobalRuleFourthSpot:filteredGlobalRulesFourthSpot){
											
												System.out.println(" if it is rate afourth spot at Global");
												ArrayList<UserLocationRatings> userNightRatings=new ArrayList<UserLocationRatings>();
												if(filteredGlobalRuleFourthSpot .getStartTime()==null){
													SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
													GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRuleFourthSpot );
													ruleDto.setStartTime(timeformat.parse("05:00 PM"));
													this.gameservice.saveOrUpdateGameRule(ruleDto);
												}
												if(filteredGlobalRuleFourthSpot .getEndTime()==null){
													SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
													GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(filteredGlobalRuleFourthSpot );
													ruleDto.setEndTime(timeformat.parse("09:00 PM"));
													this.gameservice.saveOrUpdateGameRule(ruleDto);
												}
												try{
													 
													userNightRatings=this.recommendationDao.getUserLocationRatingsByUserIdAndStartTimeAndEndTime(userLocationRatingsDto.getUserId(), filteredGlobalRuleFourthSpot.getStartTime(), filteredGlobalRuleFourthSpot.getEndTime());
													if(userNightRatings.size()>=1){
														
														Set<String> locationIds=new HashSet<String>();
														for(UserLocationRatings rating:userNightRatings){
															locationIds.add(rating.getLocationAPIId());
														}
														ArrayList<String> locationIDSUnique=new ArrayList<String>();
														for(String Id:locationIds){
															locationIDSUnique.add(Id);
														}
												
													if( locationIDSUnique.size()==3 && !locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(1).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())&& !locationIDSUnique.get(2).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
														String startTime=userDateFormat.format(filteredGlobalRuleFourthSpot.getStartTime());
														String endTime=userDateFormat.format(filteredGlobalRuleFourthSpot.getEndTime());
														Date starttime=dateFormat.parse(startTime);
														Date endtime=dateFormat.parse(endTime);
														if(null!=filteredGlobalRuleFourthSpot.getUserId()){
															if(filteredGlobalRuleFourthSpot.getUserId()==userLocationRatingsDto.getUserId()){
																String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
																Date parseDate=testDateFormat1.parse(parse);
																if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
																	System.out.println(" inside rate a fourth spot same night with satisfies user condition at Global");
																	UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRuleFourthSpot.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																	accomplishedRulesDtos.add(accomplishedRuleDto);
																}
															}
														}
														else{
															String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
															Date parseDate=testDateFormat1.parse(parse);
															if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
																System.out.println(" inside rate a fourth spot same night with satisfies  condition at Global");
																UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRuleFourthSpot.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																accomplishedRulesDtos.add(accomplishedRuleDto);
															}
														}
														
													}
													}
											  }
											  catch(UserLocationRatingsNotFoundException e){
											  }
									 } ///For condition
									}	///IF Condition
								}
								catch(GameRuleNotFoundException e){
									
								}
								catch(Exception e){
									
								}
					
					
					} // END IF COndition OF USER COMMENTS
					
					/// GLOBAL COMMENT
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("Comment",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								System.out.println(" inside Global Comment");
								if(null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
									if(null!=filteredGlobalRule.getUserId()){
										if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
											System.out.println(" inside Global Comment with user condition satisfies");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
									else{
										System.out.println(" inside Global  Comment if not comes under user");
										UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
										accomplishedRulesDtos.add(accomplishedRuleDto);
									}
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					/// GLOBAL USER FIRST COMMENT
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User First Comment",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								try{
								ArrayList<UserLocationRatings> userGlobalComments=this.recommendationDao.getUserCommentsPerDayByStartTimeAndEndTimeAndUser(kikspotUser, filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									
								}
								catch(UserLocationRatingsNotFoundException e){
									if(null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
										if(null!=filteredGlobalRule.getUserId()){
											if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
												System.out.println(" inside Global Comment with user condition satisfies");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
										else{
											System.out.println(" inside Global  Comment if not comes under user");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					/// GLOBAL USER SECOND COMMENT
					
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User Second Comment",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								try{
									ArrayList<UserLocationRatings> userGlobalComments=this.recommendationDao.getUserCommentsPerDayByStartTimeAndEndTimeAndUser(kikspotUser, filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									if(userGlobalComments.size()==1){
										if(null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
											if(null!=filteredGlobalRule.getUserId()){
												if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
													
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
											else{
												
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									}
								}
								catch(UserLocationRatingsNotFoundException e){
									
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					//// GLOBAL USER THIRD COMMENT
					
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User Third Comment",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								try{
									ArrayList<UserLocationRatings> userGlobalComments=this.recommendationDao.getUserCommentsPerDayByStartTimeAndEndTimeAndUser(kikspotUser, filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									if(userGlobalComments.size()==2){
										if(null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
											if(null!=filteredGlobalRule.getUserId()){
												if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
													
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
											else{
												
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									}
								}
								catch(UserLocationRatingsNotFoundException e){
									
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					

					//// GLOBAL USER FOURTH COMMENT
					try{
						ArrayList<GameRules> globalRules=this.gameDao.getGlobalGameRuleforRatingCondition("User Fourth Comment",userLocationRatingsDto.getUserId());
						ArrayList<GameRules> filteredGlobalRules=this.gameservice.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(globalRules);
						if(filteredGlobalRules.size()>0){
							for(GameRules filteredGlobalRule:filteredGlobalRules){
								try{
									ArrayList<UserLocationRatings> userGlobalComments=this.recommendationDao.getUserCommentsPerDayByStartTimeAndEndTimeAndUser(kikspotUser, filteredGlobalRule.getStartTime(),filteredGlobalRule.getEndTime(),userLocationRatingsDto.getLocationAPIId());
									if(userGlobalComments.size()==3){
										if(null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
											if(null!=filteredGlobalRule.getUserId()){
												if(filteredGlobalRule.getUserId()==userLocationRatingsDto.getUserId()){
													
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
											else{
												
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(filteredGlobalRule.getGameRuleId(),userLocationRatingsDto.getUserId(),null);
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									}
								}
								catch(UserLocationRatingsNotFoundException e){
									
								}
							}
						}
					}
					catch(GameRuleNotFoundException e){
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					
					
					
					
							
					try{
						ArrayList<GameRules> rules=this.gameDao.getGameRulesByLocationId(userLocationRatingsDto.getLocationAPIId());
						System.out.println(" NUMBER OF RULES OF LOCATION "+userLocationRatingsDto.getLocationAPIId()+" size" +rules.size());
						for(GameRules rule:rules){
							System.out.println(" Rule "+rule.getRule());
							try{
								ArrayList<UserLocationRatings> userRatings=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> locationRatings=new ArrayList<UserLocationRatings>();
							//	ArrayList<UserLocationRatings> locationRatings1=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> locationComments=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> userNightLocationRatings=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> userNightRatings=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> userNightLocationComments=new ArrayList<UserLocationRatings>();
								ArrayList<UserLocationRatings> nightLocationRatings=new ArrayList<UserLocationRatings>();
								
								
								 if(rule.getStartTime()==null){
										SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
										GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(rule);
										ruleDto.setStartTime(timeformat.parse("05:00 PM"));
										rule.setStartTime(timeformat.parse("05:00 PM"));
										if(rule.getStartTime()==null && rule.getGameRuleType().contains("Night")){
											System.out.println(" inside the night conditions");
										      this.gameservice.saveOrUpdateGameRule(ruleDto);
										}
									}
									if(rule.getEndTime()==null){
										SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
										GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(rule);
										ruleDto.setEndTime(timeformat.parse("09:00 PM"));
										rule.setEndTime(timeformat.parse("09:00 PM"));
										if(rule.getStartTime()==null && rule.getGameRuleType().contains("Night")){
											this.gameservice.saveOrUpdateGameRule(ruleDto);
										}
									}
							  try{
									userRatings=this.recommendationDao.getUserLocationRatingsByUserId(userLocationRatingsDto.getUserId());
							  }
							  catch(UserLocationRatingsNotFoundException e){
							  }
							  try{
									locationRatings=this.recommendationDao.getUserLocationRatingsByLocationAPIIdAndDate(userLocationRatingsDto.getLocationAPIId());
							  }
							  catch(UserLocationRatingsNotFoundException e){
							  }
							  try{
								  locationComments=this.recommendationDao.getUserLocationCommentsByLocationId(userLocationRatingsDto.getLocationAPIId());
							  }
							  catch(UserLocationRatingsNotFoundException e){
							  }
							  		  try{
										 
											nightLocationRatings=this.recommendationDao.getLocationRatingsByLocationAPIIdAndStartTimeAndEndTime(userLocationRatingsDto.getLocationAPIId(), rule.getStartTime(), rule.getEndTime());
									  }
									  catch(UserLocationRatingsNotFoundException e){
									  }
									  
									  try{
											 
										  userNightLocationComments=this.recommendationDao.getUserLocationCommentsByLocationIdAndUserIdAndStartTimeAndEndTime(userLocationRatingsDto.getLocationAPIId(), kikspotUser.getUserId(),rule.getStartTime(), rule.getEndTime());
									  }
									  catch(UserLocationRatingsNotFoundException e){
									  }
									  try{
											 
											userNightRatings=this.recommendationDao.getUserLocationRatingsByUserIdAndStartTimeAndEndTime(userLocationRatingsDto.getUserId(), rule.getStartTime(), rule.getEndTime());
									  }
									  catch(UserLocationRatingsNotFoundException e){
									  }
									  try{
											 
											userNightLocationRatings=this.recommendationDao.getUserNightLocationRatingsByUserIdAndLocationAPIIDStartTimeAndEndTime(userLocationRatingsDto.getLocationAPIId(), kikspotUser.getUserId(), rule.getStartTime(), rule.getEndTime());
									  }
									  catch(UserLocationRatingsNotFoundException e){
									  }
									//locationRatings1=this.recommendationDao.getUserLocationRatingsByLocationAPIId(userLocationRatingsDto.getLocationAPIId());
								
							
							  /***
							   * If the rule has no open date and close or expiry date means ...we are not checking any conditions
							   * If the rule has no rule limit .. we are not checking any rule limit conditions
							   * So, By default conditionsRestrcitions=true for open date,close date and rule limit null means.
							   */
							//  System.out.println("userNightLocationRatings "+userNightLocationRatings.size());
							  	Boolean openDateRestriction=true;
							  	Boolean closeDateRestriction=true;
							  	Boolean ruleLimitRestriction=true;
							  	if(null!=rule.getOpenDate()){
							  		if(rule.getOpenDate().after(new Date())){
							  			openDateRestriction=false;
							  		}
							  		
							  	}
							  	if(null!=rule.getExpiryDate()){
							  		if(rule.getExpiryDate().before(new Date())){
							  			closeDateRestriction=false;
							  		}
							  	}
							  	if(null!=rule.getRuleLimit()){
							  		if(rule.getRuleLimit()>0){
							  			ruleLimitRestriction=true;
							  		}
							  		else{
							  			ruleLimitRestriction=false;
							  		}
							  	}
							  	
								if(openDateRestriction==true && closeDateRestriction==true && ruleLimitRestriction==true && null!=rule.getGameRuleType()){
									
									//Not entering Start Time and End Time for Night conditions means, we will consider timings as 5PM to 9PM
									if(rule.getStartTime()==null){
										SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
										GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(rule);
										ruleDto.setStartTime(timeformat.parse("05:00 PM"));
										if(rule.getStartTime()==null && rule.getGameRuleType().contains("Night")){
											System.out.println(" Inside the night condition");
												this.gameservice.saveOrUpdateGameRule(ruleDto);
										}
									}
									if(rule.getEndTime()==null ){
										SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
										GameRulesDto ruleDto=GameRulesDto.populateGameRulesDto(rule);
										ruleDto.setEndTime(timeformat.parse("09:00 PM"));
										if(rule.getStartTime()==null && rule.getGameRuleType().contains("Night")){
											this.gameservice.saveOrUpdateGameRule(ruleDto);
										}
									}
									String startTime=userDateFormat.format(rule.getStartTime());
									String endTime=userDateFormat.format(rule.getEndTime());
									Date starttime=dateFormat.parse(startTime);
									Date endtime=dateFormat.parse(endTime);
									
									if(null==userLocationRatingsDto.getComment()||userLocationRatingsDto.getComment().length()==0) {
										
									if(rule.getGameRuleType().equalsIgnoreCase("Rating")){
										System.out.println(" inside Location rating");
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												System.out.println(" inside Location Rating rate with user condition satisfies");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
										else{
											System.out.println(" inside Location  Rating if not comes under user");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
									
									if(rule.getGameRuleType().equalsIgnoreCase("First Time Rate") && locationRatings.size()<1){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												System.out.println(" inside first time rate with user condition satisfies");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
										else{
											System.out.println(" inside first time rate if not comes under user");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
				
									if(rule.getGameRuleType().equalsIgnoreCase("First Nightly Rate")&& nightLocationRatings.size()<1){
										if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													if(userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" inside first nightly rate with satisfies user condition with night time");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
										}
										
										else{
											if(userDate.before(endtime) && userDate.after(starttime)){
												System.out.println(" inside first time rate without user and with night condition");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
										
										
										
									}
									if(rule.getGameRuleType().equalsIgnoreCase("First 10 Rating")  && nightLocationRatings.size()<10){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												if(userDate.before(endtime) && userDate.after(starttime)){
													System.out.println(" inside first 10 rating with user condition");
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
										}
										else{
											if(userDate.before(endtime) && userDate.after(starttime)){
												System.out.println(" inside first 10 rating with out conditions");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
									}
								if(userNightLocationRatings.size()>=1){
										if(rule.getGameRuleType().equalsIgnoreCase("Second Rating") && userNightLocationRatings.size()==1){
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightLocationRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													if(parseDate.equals(currentDate) ){
														if(userDate.before(endtime) && userDate.after(starttime)){
															System.out.println(" inside second rating with user condition");
															UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
															accomplishedRulesDtos.add(accomplishedRuleDto);
														}
													}
												}
											}
											else{
												String parse=testDateFormat.format(userNightLocationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate) ){
													if(userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" inside second rating with satisfy condition");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
						
										}
										if(rule.getGameRuleType().equalsIgnoreCase("Third Rating")  && userNightLocationRatings.size()==2){
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightLocationRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													String parse1=testDateFormat.format(userNightLocationRatings.get(1).getRatingDate());
													Date parseDate1=testDateFormat1.parse(parse1);
													if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															System.out.println(" Inside third rating with satisfies night condition and with user condition");
															UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
															accomplishedRulesDtos.add(accomplishedRuleDto);
														}
													}
												}
											}
											else{
												String parse=testDateFormat.format(userNightLocationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userNightLocationRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" Inside third rating with satisfies night condition");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
										}
										if(rule.getGameRuleType().equalsIgnoreCase("Fourth Rating")  && userNightLocationRatings.size()==3){
											if(null!=rule.getUserId()){
												System.out
														.println(" inside user not null for fourth rating");
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightLocationRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													String parse1=testDateFormat.format(userNightLocationRatings.get(1).getRatingDate());
													Date parseDate1=testDateFormat1.parse(parse1);
													String parse2=testDateFormat.format(userNightLocationRatings.get(2).getRatingDate());
													Date parseDate2=testDateFormat1.parse(parse2);
													if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)&&  parseDate2.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															System.out.println(" inside fourth rating with satisfies user condition");
															UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
															accomplishedRulesDtos.add(accomplishedRuleDto);
														}
													}
												}
											}
											else{
												System.out
														.println(" inside without user condition");
												String parse=testDateFormat.format(userNightLocationRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userNightLocationRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												String parse2=testDateFormat.format(userNightLocationRatings.get(2).getRatingDate());
												Date parseDate2=testDateFormat1.parse(parse2);
												if(parseDate.equals(currentDate) &&  parseDate1.equals(currentDate)&&  parseDate2.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" inside the fourth rating with satisfies condition");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
										}
									} //IF CONDITION CLOSE FOR userNightLocationRatings
								
									if(userNightRatings.size()>=1){
										Set<String> locationIds=new HashSet<String>();
										for(UserLocationRatings rating:userNightRatings){
											locationIds.add(rating.getLocationAPIId());
										}
										ArrayList<String> locationIDSUnique=new ArrayList<String>();
										for(String Id:locationIds){
											locationIDSUnique.add(Id);
										}
										
										
										if(rule.getGameRuleType().equalsIgnoreCase("Rate A Second Spot Same Night") && locationIDSUnique.size()==1 &&!locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
											
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" inside rate a second spot same night with satisfies user condition");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
											else{
												String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
													System.out.println(" inside rate a second spot same night with satisfies  condition");
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
											
										}
										if(rule.getGameRuleType().equalsIgnoreCase("Rate A Third Spot Same Night")&& locationIDSUnique.size()==2){
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													String parse1=testDateFormat.format(userNightRatings.get(1).getRatingDate());
													Date parseDate1=testDateFormat1.parse(parse1);
													if(!locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(1).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
														if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
															if(userDate.before(endtime) && userDate.after(starttime)){
																System.out.println(" inside ifrate a third spot same night satisfies with user condition");
																UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																accomplishedRulesDtos.add(accomplishedRuleDto);
															}
														}
													}
												}
											}
											else{
												
												String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userNightRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												if(!locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(1).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
													if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															System.out.println(" inside if rate a third spot same night satisfies with user condition");
															UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
															accomplishedRulesDtos.add(accomplishedRuleDto);
														}
													}
												}
											}
						
										}
										if(rule.getGameRuleType().equalsIgnoreCase("Rate A Fourth Spot Same Night") && locationIDSUnique.size()==3){
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													String parse1=testDateFormat.format(userNightRatings.get(1).getRatingDate());
													Date parseDate1=testDateFormat1.parse(parse1);
													String parse2=testDateFormat.format(userNightRatings.get(2).getRatingDate());
													Date parseDate2=testDateFormat1.parse(parse2);
													if(!locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(1).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(2).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
														if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
															if(userDate.before(endtime) && userDate.after(starttime)){
																System.out.println(" inside  rate a fouth spot same night with satisfies user condition");
																UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																accomplishedRulesDtos.add(accomplishedRuleDto);
															}
														}
													}
												}
											}
											else{
												String parse=testDateFormat.format(userNightRatings.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userNightRatings.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												String parse2=testDateFormat.format(userNightRatings.get(2).getRatingDate());
												Date parseDate2=testDateFormat1.parse(parse2);
												if(!locationIDSUnique.get(0).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(1).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId()) && !locationIDSUnique.get(2).equalsIgnoreCase(userLocationRatingsDto.getLocationAPIId())){
													if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															System.out.println(" inside  rate a fourth spot same night with satisfies condition");
															UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
															accomplishedRulesDtos.add(accomplishedRuleDto);
														}
													}
												}
												
											}
										}
									
									}
									} // End of  If Condition User Comments
									if(rule.getGameRuleType().equalsIgnoreCase("Comment") && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
										if(null!=rule.getUserId()){
											if(rule.getUserId()==userLocationRatingsDto.getUserId()){
												System.out.println(" inside if comment satisfies with user condition");
												UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
												accomplishedRulesDtos.add(accomplishedRuleDto);
											}
										}
										else{
											System.out.println(" inside if comment satisfies with condition");
											UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
											accomplishedRulesDtos.add(accomplishedRuleDto);
										}
									}
									if(userNightLocationComments.size()>=1){
										if(rule.getGameRuleType().equalsIgnoreCase("Second Comment") && userNightLocationComments.size()==1 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
											if(null!=rule.getUserId()){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightLocationComments.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" inside if satisfies second comment with user condtion");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
											else{
												String parse=testDateFormat.format(userNightLocationComments.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												if(parseDate.equals(currentDate) && userDate.before(endtime) && userDate.after(starttime)){
													System.out.println(" inside if satisfies second comment with condtions");
													UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
													accomplishedRulesDtos.add(accomplishedRuleDto);
												}
											}
											
										}
										
										if(rule.getGameRuleType().equalsIgnoreCase("Third Comment") && userNightLocationComments.size()==2 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
											System.out.println(" inside third comment");
											if(null!=rule.getUserId() ){
												if(rule.getUserId()==userLocationRatingsDto.getUserId()){
													String parse=testDateFormat.format(userNightLocationComments.get(0).getRatingDate());
													Date parseDate=testDateFormat1.parse(parse);
													String parse1=testDateFormat.format(userNightLocationComments.get(1).getRatingDate());
													Date parseDate1=testDateFormat1.parse(parse1);
													if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
														if(userDate.before(endtime) && userDate.after(starttime)){
															System.out.println(" inside if satisfies third comment with user condition");
															UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
															accomplishedRulesDtos.add(accomplishedRuleDto);
														}
													}
												}
											}
											else{
												String parse=testDateFormat.format(userNightLocationComments.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userNightLocationComments.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												if(parseDate.equals(currentDate) && parseDate1.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){
														System.out.println(" inside if satisfies all at third comment without user condition");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
										}
										if(rule.getGameRuleType().equalsIgnoreCase("Fourth Comment") && userNightLocationComments.size()==3 && null!=userLocationRatingsDto.getComment() && userLocationRatingsDto.getComment().length()>0){
											System.out.println(" inside fourth comment");
											if(null!=rule.getUserId()){
													if(rule.getUserId()==userLocationRatingsDto.getUserId()){
														String parse=testDateFormat.format(userNightLocationComments.get(0).getRatingDate());
														Date parseDate=testDateFormat1.parse(parse);
														String parse1=testDateFormat.format(userNightLocationComments.get(1).getRatingDate());
														Date parseDate1=testDateFormat1.parse(parse1);
														String parse2=testDateFormat.format(userNightLocationComments.get(2).getRatingDate());
														Date parseDate2=testDateFormat1.parse(parse2);
														if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
															if(userDate.before(endtime) && userDate.after(starttime)){
																System.out.println(" inside if fourth comment satisfies with user condition");
																UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
																accomplishedRulesDtos.add(accomplishedRuleDto);
															}
														}
													}
											}
											else{
												String parse=testDateFormat.format(userNightLocationComments.get(0).getRatingDate());
												Date parseDate=testDateFormat1.parse(parse);
												String parse1=testDateFormat.format(userNightLocationComments.get(1).getRatingDate());
												Date parseDate1=testDateFormat1.parse(parse1);
												String parse2=testDateFormat.format(userNightLocationComments.get(2).getRatingDate());
												Date parseDate2=testDateFormat1.parse(parse2);
												if(parseDate.equals(currentDate) && parseDate1.equals(currentDate) && parseDate2.equals(currentDate)){
													if(userDate.before(endtime) && userDate.after(starttime)){	
														System.out.println(" inside if fourth comment satisfies without user condition");
														UserGameCredsDto accomplishedRuleDto=this.gameservice.updateUserGameCredsByGameRuleId(rule.getGameRuleId(),userLocationRatingsDto.getUserId(),userLocationRatingsDto.getLocationAPIId());
														accomplishedRulesDtos.add(accomplishedRuleDto);
													}
												}
											}
										}
									}//Condition checking for Location comments
								} // MAIN IF FOR CHECKING RULE CONDITIONS
							}
							catch(NullPointerException e){
								e.printStackTrace();
							}
							catch(GameRuleNotFoundException e){
								
							}
							catch(GameRuleExpiredException e){
								
							}
							catch(GameRuleExceedsRuleLimitException e){
								
							}
							catch(GameRuleInActiveException e){
								
							}
				
						}
				}
				catch(Exception e){
					//e.printStackTrace();
				}
			
					log.info("Before Calling the save method user location ratings");
					System.out.println("Before Calling the save method user location ratings");
				 locationRatingSavedResult=this.recommendationDao.saveUserLocationRatings(userLocationRatings);
				 log.info("After Completion of save method of user location ratings");
				 System.out.println("After Completion of save method of user location ratings");
				 
				 // Saving the Score Data by bhagya on may 14th, 2018 - based on the rating algorithm which was mentioned in the trello
				 ScoreData scoreData=new ScoreData();
				 scoreData.setVenueId(userLocationRatingsDto.getLocationAPIId());
				 scoreData.setDateStamp(currentDate);
				 scoreData.setTimeStamp(userDate);
				 scoreData.setUserScore(userLocationRatingsDto.getRating());
				 scoreData.setKikspotUser(kikspotUser);
				 this.recommendationDao.saveorUpdateUserScore(scoreData);
				 
				
				 
			//	getLocationDetailsByLocationAPIId(userLocationRatingsDto.getLocationAPIId());
				
				 try{
						UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(this.userDao.getKikspotUserById(userLocationRatingsDto.getUserId()));
						updatedCreds=userGameCreds.getCreds();
					}
					catch(UserGameCredNotFoundException e){
						
					}
				 earnedCreds=updatedCreds-oldCreds;
				 RecommendationLocationDto recommendationLocationDto=this.getLocationDetailsByLocationAPIId(userLocationRatingsDto.getLocationAPIId(),null,null,null);
				//String message="Your Rating is Succesfully Submitted And Redeemed Creds Was "+earnedCreds+" And Total Creds Was "+updatedCreds;
				
				try{
					NotificationType notificationType=this.notificationService.getNotificationTypeByType("Cred Earned");
					System.out.println(" accomplished game rules "+accomplishedRulesDtos.size());
					 if(accomplishedRulesDtos.size()>0){
						
						 StringBuilder notificationMessage=new StringBuilder();
						 StringBuilder iOSNotificationMessage=new StringBuilder(); // Modified the notification message as per the trello task. 
						 iOSNotificationMessage.append("Let it rain! You have just earned "+earnedCreds+" creds by redeeming the following rewards: \n");
						 //notificationMessage.append("Congrats! U accomplish the game rules of ");
						 for(UserGameCredsDto accomplishedRule:accomplishedRulesDtos){
							 //notificationMessage.append(accomplishedRule.getGameRule().getRule() +" : "+accomplishedRule.getGameRule().getCreds() +" Creds,");
							 iOSNotificationMessage.append(accomplishedRule.getGameRule().getRule().trim() +": "+accomplishedRule.getGameRule().getCreds() +" Creds \n");
						 }
						 /*if(notificationMessage.toString().endsWith(",")){
							notificationMessage.deleteCharAt(notificationMessage.lastIndexOf(","));
						 }*/
						 if(iOSNotificationMessage.toString().endsWith("\n")){
							 iOSNotificationMessage.deleteCharAt(iOSNotificationMessage.lastIndexOf("\n"));
						 }
						// System.out.println("NOTIFICATION BUILDER "+notificationMessage.toString());
						 //log.info("NOTIFICATION BUILDER "+notificationMessage.toString());
						 //this.notificationService.sendNotificationToUser(userLocationRatingsDto.getUserId(), notificationType, notificationMessage.toString(), userLocationRatings.getLocationAPIId());
						 System.out.println("NOTIFICATION BUILDER "+iOSNotificationMessage.toString());
						 log.info("NOTIFICATION BUILDER "+iOSNotificationMessage.toString());
						 this.notificationService.sendNotificationToUser(userLocationRatingsDto.getUserId(), notificationType, iOSNotificationMessage.toString(), userLocationRatings.getLocationAPIId());
					 }
					if(null==userLocationRatingsDto.getComment() || userLocationRatingsDto.getComment().length()==0) { // adding this if condition, for to send the single notification for either rating or comment. 
					 String message="You just weighed in by submitting a "+userLocationRatingsDto.getRating()+" rating for "+recommendationLocationDto.getLocationName()+"! You rock!";
					
					 this.notificationService.sendNotificationToUser(userLocationRatingsDto.getUserId(), notificationType, message, userLocationRatings.getLocationAPIId());
					 System.out.println("Rating Notification "+message);
					}
					UserDevice userDevice=this.userDao.getUserDeviceofUser(kikspotUser);
					log.info(" USER DEVICE INFORMATION FOR SEND PUSH NOTIFICATION "+userDevice.getDeviceToken() +userDevice.getDeviceType() +" user Id " +kikspotUser.getUserId());
					if(null!=userDevice.getDeviceType()&& userDevice.getDeviceType().equalsIgnoreCase("iOS")){
						
						System.out.println(" IOS DEVICE At service "+userDevice.getDeviceToken());
						this.getApplePushNotificationService();
						this.sendIOSUserRatedNotifications(userDevice.getDeviceToken(), "Congrats! You just earned "+earnedCreds+" creds! Go to the Notifications Screen for details!", service);
						
					}
				}
				
				catch(UserDeviceNotFoundException e){
					
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				//}
			return locationRatingSavedResult;
		}
		
		
		
		/**
		 * Created by Bhagya On december 18th,2015
		 * @param kikspotRecommendationLocationDto
		 * @return
		 * @throws Exception
		 * 
		 * Method for saving or Updating the Kikspot recommendation Locations Which Are Added By Admin
		 */
		
		public Integer saveOrUpdateKikspotRecommendationLocation(KikspotRecommendationLocationDto kikspotRecommendationLocationDto) throws Exception{
			log.info("inside saveOrUpdateKikspotRecommendationLocation()");
			
			KikspotRecommendationLocation kikspotRecommendationLocation=null;
			
			if(null!=kikspotRecommendationLocationDto.getLocationId() && kikspotRecommendationLocationDto.getLocationId()>0){
				kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(kikspotRecommendationLocationDto.getLocationId());
				// UPDATING THE LOCATION FOR GAME RULES
				try
				{
					ArrayList<GameRules> gameRules=this.gameDao.getGameRuleByLocationId(kikspotRecommendationLocation.getLocationId().toString());
					for(GameRules gameRule:gameRules){
						gameRule.setLocationName(kikspotRecommendationLocationDto.getLocationName());
						gameRule.setLatitude(kikspotRecommendationLocationDto.getLatitude());
						gameRule.setLongitude(kikspotRecommendationLocationDto.getLongitude());
						this.gameDao.saveorUpdateGameRule(gameRule);
					}
				}
				catch(GameRuleNotFoundException e){
					
				}
				
			}
			else{
				System.out.println(" inside else block");
				kikspotRecommendationLocation=new KikspotRecommendationLocation();
				kikspotRecommendationLocation.setIsNew(true);
				kikspotRecommendationLocation.setCreatedBy(kikspotRecommendationLocationDto.getCreatedBy());
			}
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			if(null!=kikspotRecommendationLocationDto.getOpenDate() && kikspotRecommendationLocationDto.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=kikspotRecommendationLocationDto.getCloseDate() && kikspotRecommendationLocationDto.getCloseDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false)
			{
				kikspotRecommendationLocation.setIsOpen(false);
			}
			else{
				kikspotRecommendationLocation.setIsOpen(true);
			}
			
			// Setted the visible as true by bhagya on Feb 17th, 2017 as per prakash request. For control the admin added locations to users
			kikspotRecommendationLocation.setVisible(true);
						
			//kikspotRecommendationLocation=new KikspotRecommendationLocation();
			if(null!=kikspotRecommendationLocationDto.getGoogleLocationId() && kikspotRecommendationLocationDto.getGoogleLocationId().length()>0){
			kikspotRecommendationLocation.setGoogleLocationId(kikspotRecommendationLocationDto.getGoogleLocationId());
			}
			kikspotRecommendationLocation.setLocationName(kikspotRecommendationLocationDto.getLocationName());
			
			
			kikspotRecommendationLocation.setLatitude(kikspotRecommendationLocationDto.getLatitude());
			kikspotRecommendationLocation.setLongitude(kikspotRecommendationLocationDto.getLongitude());
			//By Default, avg rating is 10..
			if(null!=kikspotRecommendationLocationDto.getRatings()){
				kikspotRecommendationLocation.setRatings(kikspotRecommendationLocationDto.getRatings());
			}
			else{
				kikspotRecommendationLocation.setRatings(5.0);
			}
			
			kikspotRecommendationLocation.setAddress(kikspotRecommendationLocationDto.getAddress());
			kikspotRecommendationLocation.setPhoneNo(kikspotRecommendationLocationDto.getPhoneNo());
			kikspotRecommendationLocation.setUrl(kikspotRecommendationLocationDto.getUrl());
			kikspotRecommendationLocation.setDistance(kikspotRecommendationLocationDto.getDistance());
			kikspotRecommendationLocation.setOpenDate(kikspotRecommendationLocationDto.getOpenDate());
			kikspotRecommendationLocation.setCloseDate(kikspotRecommendationLocationDto.getCloseDate());
			kikspotRecommendationLocation.setLocationDate(new Date());
			if(null!=kikspotRecommendationLocationDto.getModifiedBy() && kikspotRecommendationLocationDto.getModifiedBy().length()>0){
				kikspotRecommendationLocation.setModifiedBy(kikspotRecommendationLocationDto.getModifiedBy());
			}
			Integer savedResult=this.recommendationDao.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocation);
			
			System.out.println(" saved result "+savedResult);
			if(null!=kikspotRecommendationLocationDto.getIconImage() && kikspotRecommendationLocationDto.getIconImage().getSize()>0L){
				MultipartFile file=kikspotRecommendationLocationDto.getIconImage();
				String fileType=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
				File destinationFile=new File(iconImagePath+savedResult+"/"+new Date().getTime()+"."+fileType);
				if(!destinationFile.exists()){
					destinationFile.mkdirs();
				}
				file.transferTo(destinationFile);
				kikspotRecommendationLocation.setIconUrl(destinationFile.getName());
			}
			if(null!=kikspotRecommendationLocationDto.getLocationImages()){
				this.saveKikspotLocationImages(kikspotRecommendationLocation,kikspotRecommendationLocationDto.getLocationImages());
			}
			this.recommendationDao.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocation);
			//String message=kikspotRecommendationLocationDto.getLocationName()+" has been added near to your location";
			String message="A new spot just came online, "+kikspotRecommendationLocationDto.getLocationName()+" at "+kikspotRecommendationLocationDto.getAddress()+". Check it out!";
			/*
			 * 1. Get List of all user Ids whose lat and long close to added location.. and get UserDevices..
			 * 
			 * 2. Send Notification to all users..
			 * 
			 */
			ArrayList<Integer> userIds=this.userDao.getAllUsersNearbytoGivenLocation(kikspotRecommendationLocationDto.getLatitude(), kikspotRecommendationLocationDto.getLongitude(), radius, unit);
			if(userIds.size()>0){
				try{
					//	ArrayList<UserDevice> userDevices=this.userDao.getUserDevicesByUserIds(userIds);
					for(Integer userId:userIds){
						NotificationType notificationType=this.notificationService.getNotificationTypeByType("New Location");
						String locationId="";
						if(null!=kikspotRecommendationLocation.getGoogleLocationId() && kikspotRecommendationLocation.getGoogleLocationId().length()>0){
							locationId=kikspotRecommendationLocation.getGoogleLocationId();
						}
						else{
							locationId=String.valueOf(kikspotRecommendationLocation.getLocationId());
						}
						this.notificationService.sendNotificationToUser(userId, notificationType, message, locationId);
					}
					
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			return savedResult;
		}
		
		
		
		
		/**
		 * Created By Bhagya On December 18th,2015
		 * @param locationId
		 * @return
		 * @throws KikspotRecommendationLocationNotFoundException
		 * 
		 * Service for getting the kikspot Recommendation Location By location ID
		 */
		
		public KikspotRecommendationLocationDto getKikspotRecommendationLocationByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException{
			log.info("inside getKikspotRecommendationLocationByLocationId()");
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
			try{
				
				Boolean openDateRestriction=true;
				Boolean closeDateRestriction=true;
				
				if(null!= kikspotRecommendationLocation.getOpenDate() &&  kikspotRecommendationLocation.getOpenDate().after(new Date())){
					openDateRestriction=false;
				}
				if(null!= kikspotRecommendationLocation.getCloseDate() &&  kikspotRecommendationLocation.getCloseDate().before(new Date())){
				
					closeDateRestriction=false;
				}
				if(openDateRestriction==false || closeDateRestriction==false){
					 kikspotRecommendationLocation.setIsOpen(false);
					 
				}
			
		    }
		    catch(Exception e){
			
		    }
			KikspotRecommendationLocationDto kikspotRecommendationLocationDto=KikspotRecommendationLocationDto.populateRecommendationLocationDto(kikspotRecommendationLocation);
			return kikspotRecommendationLocationDto;
		}
		
		
		
		
		/**
		 * Created By Bhagya On December 18th,2015
		 * @param gameId
		 * @param pageNo
		 * @param pageSize
		 * @param sortBy
		 * @param searchBy
		 * @param ascending
		 * @return
		 * @throws KikspotRecommendationLocationNotFoundException
		 * 
		 * Method for getting the Kikspot Recommendation Locations which are added By Admin
		 */
		
		public ArrayList<KikspotRecommendationLocationDto> getKikspotRecommendationLocations(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRecommendationLocationNotFoundException{
			log.info("inside getKikspotRecommendationLocations()");
			
			ArrayList<KikspotRecommendationLocationDto> kikspotRecommendationLocationDtos=new ArrayList<KikspotRecommendationLocationDto>();
			ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=this.recommendationDao.getKikspotRecommendationLocations(pageNo, pageSize, sortBy, searchBy, ascending);
			for(KikspotRecommendationLocation recommendationLocation:kikspotRecommendationLocations){
			
				try{
				
					Boolean openDateRestriction=true;
					Boolean closeDateRestriction=true;
					
					if(null!=recommendationLocation.getOpenDate() && recommendationLocation.getOpenDate().after(new Date())){
						openDateRestriction=false;
					}
					if(null!=recommendationLocation.getCloseDate() && recommendationLocation.getCloseDate().before(new Date())){
					
						closeDateRestriction=false;
					}
					if(openDateRestriction==false || closeDateRestriction==false){
						 recommendationLocation.setIsOpen(false);
						 
					}
				
			    }
			    catch(Exception e){
				
			    }
				
			KikspotRecommendationLocationDto kikspotRecommendationLocationDto=KikspotRecommendationLocationDto.populateRecommendationLocationDto(recommendationLocation);
			
			kikspotRecommendationLocationDtos.add(kikspotRecommendationLocationDto);
			}
			
			return kikspotRecommendationLocationDtos;
		}
		
		
		
		/**
		 * Created By Bhagya On December 18th,2015
		 * @param kikspotLocationId
		 * @return
		 * @throws KikspotRecommendationLocationNotFoundException
		 * 
		 * Method for getting the Kikspot Recommendation Location By Kikspot Location Id
		 */
		public KikspotRecommendationLocationDto getKikspotRecommendationLocationByKikspotLocationId(String kikspotLocationId) throws KikspotRecommendationLocationNotFoundException{
			log.info("inside getKikspotRecommendationLocationByLocationId()");
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByKikspotLocationId(kikspotLocationId);
			KikspotRecommendationLocationDto kikspotRecommendationLocationDto=KikspotRecommendationLocationDto.populateRecommendationLocationDto(kikspotRecommendationLocation);
			return kikspotRecommendationLocationDto;
		}
		
		
		
		/**
		 * Created By Bhagya On December 18th,2015
		 * @param locationId
		 * @return
		 * @throws Exception
		 * 
		 * Method for Deleting the recommendation Location By location ID
		 * 
		 * Modified By Bhagya Jan 04th,2015
		 * 	Steps : Delete the Game Rules Of a Location
		 * 			Delete the Location Images of a Location
		 * 			Delete the kikspot recommendation Location .
		 */
		
		public Integer deleteKikspotRecommendationLocationByLocationId(Integer locationId) throws Exception{
			log.info("inside deleteKikspotRecommendationLocationByLocationId()");
			Integer deletedResult=0;
			ArrayList<Integer> results=new ArrayList<Integer>();			
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
			// Deleting the game rules of a location
			
			try{
				ArrayList<GameRules> gameRules=this.gameDao.getGameRulesByLocationId(locationId.toString());
				if(!gameRules.isEmpty()){
							
					for(GameRules gameRule:gameRules){
						deletedResult=0;
						try{
							ArrayList<UserGameCreds> userGameCreds=this.gameDao.getUsersGameCredsByGameRule(gameRule);
							results.add(0);
							}
							catch(UserGameCredNotFoundException e){
								deletedResult=this.gameDao.deleteGameRule(gameRule);
								results.add(deletedResult);
							}
						//this.gameDao.deleteGameRule(gameRule);
					}
				}
			}
			catch(GameRuleNotFoundException e){
				
			}
			//deleting the recommendation location images of location
			
			 if(!results.contains(0)){
			
				try{
					ArrayList<RecommendationLocationImages> recommendationLocationImages=this.recommendationDao.getRecommendationLocationImagesByKikspotRecommendationLocation(kikspotRecommendationLocation);
					if(!recommendationLocationImages.isEmpty()){
						for(RecommendationLocationImages recommendationLocationImage: recommendationLocationImages){
							this.recommendationDao.deleteRecommendationLocationImage(recommendationLocationImage);
						}
					}
				}
				catch(RecommendationLocationImagesNotFoundException e){
					
				}
				 deletedResult=this.recommendationDao.deleteKikspotRecommendationLocation(kikspotRecommendationLocation);
				
			
				
			 }
			 else{
				 deletedResult=0;
			 }
			
			return deletedResult;
		}
		
		/**
		 * Created By Bhagya On December 21st,2015
		 * @param kikspotLocationId
		 * @param imageFiles
		 * @throws Exception
		 * 
		 * Method for saving Kikspot Location Images
		 */
		
		private void saveKikspotLocationImages(KikspotRecommendationLocation kikspotRecommendationLocation,List<MultipartFile> imageFiles)throws Exception{
			log.info("inside saveProductImagesCorrespondstoProduct()");
			for(MultipartFile imageFile:imageFiles){
				if(imageFile.getSize()>0){
					String fileType=imageFile.getOriginalFilename().substring(imageFile.getOriginalFilename().lastIndexOf(".")+1);
					File destinationFile=new File(locationImagePath+kikspotRecommendationLocation.getLocationId()+"/"+new Date().getTime()+"."+fileType);
					if(!destinationFile.exists()){
						destinationFile.mkdirs();
					}
					imageFile.transferTo(destinationFile);
					RecommendationLocationImages recommendationLocationImages=new RecommendationLocationImages();
					recommendationLocationImages.setImage(destinationFile.getName());
					recommendationLocationImages.setKikspotRecommendationLocation(kikspotRecommendationLocation);
					this.recommendationDao.saveOrUpdateKikspotRecommendationLocationImages(recommendationLocationImages);
				}
						
			}		
		}
	/**
	 * Created By Bhagya On December 22nd,2015	
	 * @param locationId
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * @throws RecommendationLocationImagesNotFoundException
	 * 
	 * Method for getting the Recommendation Location Images By Location ID
	 */
	public ArrayList<RecommendationLocationImagesDto> getRecommendationLocationImagesByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException, RecommendationLocationImagesNotFoundException{
		log.info("inside getRecommendationLocationImagesByLocationId()");
		KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
		ArrayList<RecommendationLocationImages> recommendationLocationImages=this.recommendationDao.getRecommendationLocationImagesByKikspotRecommendationLocation(kikspotRecommendationLocation);
		ArrayList<RecommendationLocationImagesDto> recommendationLocationImagesDtos=new ArrayList<RecommendationLocationImagesDto>();
		for(RecommendationLocationImages recommendationLocationImage: recommendationLocationImages){
			RecommendationLocationImagesDto recommendationLocationImagesDto=RecommendationLocationImagesDto.populateRecommendationLocationImagesDto(recommendationLocationImage);
			recommendationLocationImagesDtos.add(recommendationLocationImagesDto);
		}
		return recommendationLocationImagesDtos;
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param locationImageId
	 * @return
	 * @throws Exception
	 * 
	 * Method for deleting the recommendation Location Image By Id
	 */
	
	public Integer deleteRecommendationLocationImageById(Integer locationImageId) throws Exception{
		log.info("inside deleteRecommendationLocationImageById()");
		RecommendationLocationImages recommendationLocationImage=this.recommendationDao.getRecommendationLocationImageById(locationImageId);
		Integer deletedResult=this.recommendationDao.deleteRecommendationLocationImage(recommendationLocationImage);
		return deletedResult;
	}
	/***
	 * Created By Bhagya On december 22nd,2015
	 * @param locationId
	 * @param locationImagesFile
	 * @return
	 * @throws Exception
	 * 
	 * Method for adding Recommendation Location Images for the Kikspot Recommendation Location
	 */
	public Integer addRecommendationLocationImagesforRecommendationLocation(Integer locationId,List<MultipartFile> locationImagesFile) throws Exception{
		log.info("inside addRecommendationLocationImagesforRecommendationLocation()");
		KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
		this.saveKikspotLocationImages(kikspotRecommendationLocation, locationImagesFile);
		return 1;
	}
	
	
	/**
	 * Created By Bhagya On Jan 05th,2016
	 * @param userId
	 * @param locationAPIId
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method For getting The MOST Recent  User Location Rating By userId and LocationAPIId
	 */
	public UserLocationRatingsDto getMostRecentUserLocationRatingByUserIdAndLocationId(Integer userId, String locationAPIId) throws UserNotFoundException, UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingByUserIdAndLocationId()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		UserLocationRatings userLocationRatings=this.recommendationDao.getMostRecentLocationRatingOfUser(kikspotUser, locationAPIId);
		UserLocationRatingsDto userLocationRatingsDto=UserLocationRatingsDto.populateUserLocationRatingsDto(userLocationRatings);
		return userLocationRatingsDto;
	}
	
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for Getting the List of comments of a Location
	 * Steps: Get list of user ratings of a location
	 */
	public ArrayList<UserLocationRatingsDto> getLocationCommentsByLocationAPIId(String locationAPIId,Integer userId) throws UserLocationRatingsNotFoundException{
		log.info("inside getLocationCommentsByLocationAPIId()");
		ArrayList<UserLocationRatings> usersLocationRatings=this.recommendationDao.getUserLocationRatingsByLocationAPIId(locationAPIId);
		ArrayList<UserLocationRatingsDto> usersLocationsRatingsDtos=new ArrayList<UserLocationRatingsDto>();
		for(UserLocationRatings userLocationRating:usersLocationRatings){
			UserLocationRatingsDto userLocationRatingsDto=UserLocationRatingsDto.populateUserLocationRatingsDto(userLocationRating);
			usersLocationsRatingsDtos.add(userLocationRatingsDto);
		}
		return usersLocationsRatingsDtos;
	}
	
	
	
	/**
	 * Created By Bhagya On Jan 08th,2016
	 * @param userId
	 * @param locationId
	 * @param imageFiles
	 * @return
	 * @throws Exception
	 * 
	 * Method for saving the User Recommendation Location Images
	 */
	public Integer saveUserRecommendationLocationImages(Integer userId,String locationId,List<MultipartFile> imageFiles,Double latitude,Double longitude,Integer userLocationRatingId) throws Exception{
		log.info("inside saveUserRecommendationLocationImages()");
		LocationRatingToggler locationRatingToggler=this.recommendationDao.getLocationRatingToggler();
		RecommendationLocationDto recommendationLocationDto=this.getLocationDetailsByLocationAPIId(locationId,latitude,longitude,null);
		if(!locationRatingToggler.getAllowGlobalRating()){
					
			
			Double distance=this.distance(latitude,longitude, recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude());
			if(distance>0.02){
				throw new RecommendationGeoLocationNotMatchedException();
			}
		}		
		
		
		for(MultipartFile imageFile:imageFiles){
			if(imageFile.getSize()>0){
				String fileType=imageFile.getOriginalFilename().substring(imageFile.getOriginalFilename().lastIndexOf(".")+1);
				File destinationFile=new File(userLocationImagePath+userId+"/"+locationId+"/"+new Date().getTime()+"."+fileType);
				if(!destinationFile.exists()){
					destinationFile.mkdirs();
				}
				imageFile.transferTo(destinationFile);
				UserRecommendationLocationImages userRecommendationLocationImages=new UserRecommendationLocationImages();
				KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
				userRecommendationLocationImages.setKikspotUser(kikspotUser);
				userRecommendationLocationImages.setLocationId(locationId);
				userRecommendationLocationImages.setImage(destinationFile.getName());
				userRecommendationLocationImages.setUserLocationRatingId(userLocationRatingId);
				this.recommendationDao.saveOrUpdateUserRecommendationLocationImages(userRecommendationLocationImages);
			}
			
			
		}			
		return 1;
	}	
	
	
	
	/**
	 * Created by Jeevan on FEBRUARY 04, 2016
	 * 
	 * Method to getLocationRatingTogglerFromDB...
	 * 
	 * @return
	 * @throws Exception
	 */
	public LocationRatingTogglerDto getLocationRatingTogglerfromDB()throws Exception{
		log.info("inside getLocationRatingTogglerfromDB()");
		LocationRatingToggler locationRatingToggler=this.recommendationDao.getLocationRatingToggler();
		LocationRatingTogglerDto locationRatingTogglerDto=LocationRatingTogglerDto.populationLocationRatingTogglerDto(locationRatingToggler);
		return locationRatingTogglerDto;
	}
	
	
	public Integer saveorUpdateLocationTogglerToDB(Boolean allowGlobalRating)throws Exception{
		log.info("inside saveorUpdateLocationTogglerToDB()");
		LocationRatingToggler locationRatingToggler=this.recommendationDao.getLocationRatingToggler();
		locationRatingToggler.setAllowGlobalRating(allowGlobalRating);
		return this.recommendationDao.saveorUpdateLocationRatingToggler(locationRatingToggler);
	}
	
	
	
	/**
	 * Created by Jeevan on FEBRUARY 22, 2016
	 * 
	 * Method to refreshLocationsOfUser()..
	 * 
	 * 
	 * @param userId
	 * @param sortBy
	 * @throws Exception
	 */
	public void refreshLocationsofUser(Integer userId,String sortBy)throws Exception{
		log.info("inside refreshLocationsOfUser() ");		
		ArrayList<UserRemovedLocation> userRemovedLocations=this.recommendationDao.getUserRemovedLocationsOfUserandSort(userId, sortBy);
		for(UserRemovedLocation userRemovedLocation: userRemovedLocations){
			this.recommendationDao.deleteUserRemovedLocations(userRemovedLocation);
		}		
	}

 /**
  * added by Firdous on 1st march 2016
  * method to change the status of a location
  * 
  */

	@Override
	public Integer updateLocationStatus(Integer locationId, Boolean isOpen,String reason,String modifiedBy) throws Exception {
		
		log.info("inside updateLocationStatus()");
		KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
		kikspotRecommendationLocation.setIsOpen(isOpen);
		kikspotRecommendationLocation.setReason(reason);
		kikspotRecommendationLocation.setStatusModifiedDate(new Date());
		kikspotRecommendationLocation.setModifiedBy(modifiedBy);
		Integer updatedResult=	this.recommendationDao.saveOrUpdateKikspotRecommendationLocation(kikspotRecommendationLocation);
		return updatedResult;
	}
	
	
	
	
	/**
	 * @author Jeevan
	 * @return
	 * @throws Exception
	 */
	public RecommendationConfigurationDto getRecommendationsConfiguration() throws Exception{
		RecommendationConfiguration configuration=this.recommendationDao.getRecommendationConfiguration();
		RecommendationConfigurationDto configurationDto=RecommendationConfigurationDto.populateRecommendationConfigurationDto(configuration);
		return configurationDto;
	}
	
	
	/**
	 * @author Jeevan
	 * @return
	 * @throws Exception
	 * Method to save or update the recommendation configurations
	 * 
	 */
	
	public Integer saveorUpdateRecommendationConfiguration(RecommendationConfigurationDto configDto)throws Exception{
		log.info("inside saveorUpdateRecommendationConfiguration()");
		RecommendationConfiguration config=new RecommendationConfiguration();
		config.setIsBar(configDto.getIsBar());
		config.setIsBowlingAlley(configDto.getIsBowlingAlley());
		config.setIsCasino(configDto.getIsCasino());
		config.setIsLounge(configDto.getIsLounge());
		config.setIsNightClub(configDto.getIsNightClub());
		config.setIsPub(configDto.getIsPub());
		config.setRecommendationConfigurationId(configDto.getRecommendationConfigurationId());
		
		return this.recommendationDao.saveorUpdateRecommendationConfiguration(config);
	}

   /**
    * 
    * Added by Firdous on 28 March 2016
    * Method to get all the location details which are selected by the user to share with other user
    */

	@Override
	public ArrayList<RecommendationLocationDto> getLocations(List<String> locationAPIIdsList) throws Exception {
		log.info("inside getLocations()");
		ArrayList<RecommendationLocationDto> recommendationLocationDtos=new ArrayList<RecommendationLocationDto>();
		ArrayList<PlaceDto> placeDtos=new ArrayList<PlaceDto>();
		ArrayList<KikspotRecommendationLocation> recommendationLocations=new ArrayList<KikspotRecommendationLocation>();
		for(String locationAPIId:locationAPIIdsList){
			try{
			   KikspotRecommendationLocation recommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(Integer.parseInt(locationAPIId));
			   recommendationLocations.add(recommendationLocation);
			}
		    catch(KikspotRecommendationLocationNotFoundException e){
			  PlaceDto placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(locationAPIId);
			  placeDtos.add(placeDto);
		    }
			 catch(NumberFormatException e){
			  PlaceDto placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(locationAPIId);
			  placeDtos.add(placeDto);
			}
		}
		ArrayList<PlaceDto> placeDtos1=this.populatePlaceDtofromKikspotRecommendations(recommendationLocations);
		placeDtos.addAll(placeDtos1);
		for(PlaceDto placeDto:placeDtos){
			RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
			recommendationLocationDto.setLocationName(placeDto.getPlaceName());
			recommendationLocationDto.setLatitude(placeDto.getLatitude());
			recommendationLocationDto.setLongitude(placeDto.getLongitude());
			recommendationLocationDto.setIconUrl(placeDto.getIconURL());
			recommendationLocationDto.setImage(placeDto.getImageURL());
			recommendationLocationDto.setAddress(placeDto.getAddress());
			recommendationLocationDto.setRatings(placeDto.getRating());
			recommendationLocationDto.setPhoneNo(placeDto.getPhoneNumber());
			recommendationLocationDto.setUrl(placeDto.getWebsiteURL());
			
			try{
				recommendationLocationDto.setLocationId(Integer.parseInt(placeDto.getPlaceId()));
			}
			catch(NumberFormatException e){
				
			}
			
			recommendationLocationDtos.add(recommendationLocationDto);
		}
		
		return recommendationLocationDtos;
	}


/**
 * Added by Firdous on 29-03-2016
 * Method to get comments of a location by accepting location API Id
 * @throws Exception 
 */

public ArrayList<UserLocationRatingsDto> getCommentsOfLocationsByLocationAPIId(String locationAPIId) throws Exception{
	log.info("inside getLocationCommentsByLocationAPIId()");
	ArrayList<UserLocationRatings> usersLocationRatings=this.recommendationDao.getUserLocationRatingsByLocationAPIId(locationAPIId);
	ArrayList<UserLocationRatingsDto> usersLocationsRatingsDtos=new ArrayList<UserLocationRatingsDto>();
	for(UserLocationRatings userLocationRating:usersLocationRatings){
		UserLocationRatingsDto userLocationRatingsDto=UserLocationRatingsDto.populateUserLocationRatingsDto(userLocationRating);
		usersLocationsRatingsDtos.add(userLocationRatingsDto);
	}
	
	return usersLocationsRatingsDtos;
}
	/**
	 * Created By Bhagya On May 12th, 2016
	 * Method for to get the Apple Push Notification Servcie
	 * @throws IOException
	 */

	public void getApplePushNotificationService() throws IOException{
		log.info("INSIDE  getApplePushNotificationService()");
		if(null==service){
			Properties properties=new Properties();
			InputStream pushProperties=this.getClass().getClassLoader().getResourceAsStream("push.properties");
			properties.load(pushProperties);
			String passPhrase=properties.getProperty("passphrase");
			String enviroment=properties.getProperty("environment");
			InputStream certFile=this.getClass().getClassLoader().getResourceAsStream(properties.getProperty("certfile"));
			log.info("passPhrase"+passPhrase +" environment "+enviroment);
				ApnsServiceBuilder serviceBuilder = APNS.newService()
			    		.withCert(certFile, passPhrase);
				serviceBuilder.withDelegate(new ApnsDelegate() {
				      
					
					@Override
					public void connectionClosed(DeliveryError deliveryError, int messageIdentifier) {
						// TODO Auto-generated method stub
						System.out.println(" Notification Delivery Error Code For IOS Device: Error Code "+deliveryError.code() +" Error Message "+deliveryError.toString());
						log.info(" Notification Delivery Error Code For IOS Device: Error Code "+deliveryError.code() +" Error Message  "+deliveryError.toString());
					}
	
					
	
					@Override
					public void messageSendFailed(ApnsNotification notification, Throwable e) {
						// TODO Auto-generated method stub
						System.out.println("Daily Notification Message send failed.  Error Message: " + e.toString());
						log.info(" Notification Message send failed For IOS Device "+ e.toString());
					}

					@Override
					public void cacheLengthExceeded(int arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void messageSent(ApnsNotification notification, boolean arg1) {
						// TODO Auto-generated method stub
						System.out.println("Daily Notification Message sent.  Payload: " + notification.getPayload().toString() + " token " + notification.getDeviceToken().toString());
						log.info(" Notification Message sent For IOS Device");
						System.out.println("expiryy "+notification.getExpiry() +" identifer "+notification.getIdentifier());
						
					}

					@Override
					public void notificationsResent(int arg0) {
						// TODO Auto-generated method stub
						
					}
		        });
				log.info("BEFORE FOR LOOP passPhrase"+passPhrase +" environment "+enviroment );
			    	if(enviroment.equals("sandbox")){
			    		log.info("Sandbox host selected");
			    		System.out.println("Sandbox host selected");
			    	}
			    	else {
			    		log.info("Production host selected");
			    		System.out.println("Production host selected");
			    	}
			    	
			    	 service =enviroment.equals("sandbox") ?
			    			 	 serviceBuilder.withSandboxDestination().build() : 
			    				 serviceBuilder.withProductionDestination().build();
			    		log.info("After creating the service "+service.toString());
			    			Map<String, Date> devices=service.getInactiveDevices();
			    			System.out.println("SIZE OF INACTIVE DEVICES  "+devices.size());
			    			log.info("SIZE OF INACTIVE DEVICES  "+devices.size());
			    			for(Entry<String, Date> device: devices.entrySet()){
			    				
			    			}	
			    		
	
			}
		
	}
	/**
	 * Created By bhagya On May 12th, 2016
	 * @param deviceToken
	 * @param notificationMessage
	 * @param service
	 * @throws InterruptedException
	 * 
	 * Method for send The IOS User Rated Notifications
	 */

	public void sendIOSUserRatedNotifications(String deviceToken,String notificationMessage,ApnsService service) throws InterruptedException{
		log.info("INSIDE sendIOSUserRatedNotifications()");
		System.out.println(" inside the send IOS Notification");
		String payload = APNS.newPayload()
	            .alertBody(notificationMessage)
	            .sound("Glass.aiff")
	            .actionKey("Play").build();
		Thread.sleep(300);
		service.push(deviceToken.trim(), payload.trim());		
	
		System.out.println(" NOTIFICATION SENT TO IOS DEVICE : NOTIFICATION MESSAGE "+notificationMessage +" DEVICE TOKEN "+deviceToken);
		log.info("  NOTIFICATION SENT TO IOS DEVICE : NOTIFICATION MESSAGE "+notificationMessage +" DEVICE TOKEN "+deviceToken);
	}
	/**
	 * Created By Bhagya On June 30th , 2016
	 * 
	 * Steps:
	 * 1. Allow Location Rating From All Locations is ON means .. Without Present At Location User can able to rate the location
	 * 2. Allow Location Rating From All Locations is OFF means ... User Should Prsent at the location for rate the spot
	 * 
	 * These two conditions is handled at below service
	 */
	public Integer checkLocationTogglerConditionAndSaveRateALocation(UserLocationRatingsDto userLocationRatingsDto) throws Exception{
		System.out.println(" Inside Rating "+userLocationRatingsDto.getLocationAPIId() +" rating "+userLocationRatingsDto.getRating() +" comment "+ userLocationRatingsDto.getComment());
		Integer locationRatingSavedResult;
		LocationRatingToggler locationRatingToggler=this.recommendationDao.getLocationRatingToggler();
		if(locationRatingToggler.getAllowGlobalRating()){			
			 locationRatingSavedResult=this.saveUserLocationRatingtoDB(userLocationRatingsDto);
		}
		else{
			RecommendationLocationDto recommendationLocationDto=this.getLocationDetailsByLocationAPIId(userLocationRatingsDto.getLocationAPIId(),null,null,null);
			Double distance=this.distance(userLocationRatingsDto.getLatitude(), userLocationRatingsDto.getLongitude(), recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude());
			if(distance>0.05){
				throw new RecommendationGeoLocationNotMatchedException();
			}
			else{
				 locationRatingSavedResult=this.saveUserLocationRatingtoDB(userLocationRatingsDto);
			}
		}
		return locationRatingSavedResult;
	}
	
	
	/**
	 * Added By Bhagya on april 17th, 2018
	 * @param locationAPIId
	 * @param userId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the most recenet commented photos and ratings
	 */
	
	public ArrayList<UserLocationRatingsDto> getLocationCommentsAndLocationImagesByLocationAPIId(String locationAPIId){
		log.info("RecommendationServiceImpl -> getLocationCommentsAndLocationImagesByLocationAPIId()");
		ArrayList<UserLocationRatingsDto> usersLocationsRatingsDtos=new ArrayList<UserLocationRatingsDto>();
		try{
		ArrayList<UserLocationRatings> usersLocationRatings=this.recommendationDao.getUserLocationRatingsByLocationAPIId(locationAPIId);
		
		for(UserLocationRatings userLocationRating:usersLocationRatings){
			UserLocationRatingsDto userLocationRatingsDto=UserLocationRatingsDto.populateUserLocationRatingsDto(userLocationRating);
			try{
			UserRecommendationLocationImages userRecommendationLocationImage=this.recommendationDao.getMostRecentUserLocationImageByUserIdAndLocationId(userLocationRating.getLocationAPIId(), userLocationRating.getKikspotUser(),userLocationRating.getUserLocationRatingId());
			String imageURL=serverURL+"/recommendations/userImages/"+userLocationRatingsDto.getKikspotUserDto().getUserId()+"/"+userLocationRatingsDto.getLocationAPIId()+"/"+userRecommendationLocationImage.getImage();
			userLocationRatingsDto.setImage(imageURL);
						
			}
			catch(RecommendationLocationImagesNotFoundException e){
				
			}
			if((null!=userLocationRatingsDto.getComment()&& userLocationRatingsDto.getComment().length()>0) || (null!=userLocationRatingsDto.getImage()&& userLocationRatingsDto.getImage().length()>0)){
			
				usersLocationsRatingsDtos.add(userLocationRatingsDto);
			}
		}
		}
		catch(UserLocationRatingsNotFoundException e){
			
		}
		
		return usersLocationsRatingsDtos;
	}
	
	/**
	 * Created by bhagya on may 14th, 2018
	 * @param venueId
	 * @return
	 * @throws ParseException
	 * 
	 * Implemented the rating algorithm as per the algorithm which was mentioned from the client in trello
	 */
	
	public Double getAverageRatingofLocationBasedonRatingAlgorithm(String venueId, Double apiScore) throws ParseException{
		log.info("Inside RecommendationServiceImpl ->getAverageRatingofLocationBasedonRatingAlgorithm()");
		Double finalScore=0.0;
		try{
			/*Let n = user scores, i.e. rows in ScoreData
					Let nv = all the user scores for a given venue
					Let Today(nv) = all the user scores for a given venue on today’s date
					Let v = a counter in a for-loop representing a given venue for which a
					score is being calculated (outside of a for loop, it represents simply
					a given vendor)
					Let s = final score
					Let CurrentDow = the current day of the week for which the score is
					being calculated
					Let CurrentDow(nv) = user scores for a given day of the week, i.e. rows
					in ScoreData
					Let OtherDow(nv) = user scores for all the other days of the week,
					excluding the current day of the week; ~CurrentDow(nv)
					Let DayOfWeek = a number from 1 to 7 indicating which day of the week
					the DateStamp falls on (e.g. 1 = Sunday, 2 = Monday, etc.)
					Let ApiThreshold = the number of data points required for a venue before
					the algorithm will begin processing user scores rather than present API
					scores
					Let DowThreshold = the number of data points for a given day of the week
					required for a venue before the algorithm will begin weighting scores
					by days of the week
					Let DayThreshold = the number of data points for a given day required
					for a venue before the algorithm will begin weighting a specific day of
					the week’s scores by most recent data
					Let MemorySpan = the number of weeks that the algorithm considers “most
					recent” and gives day-of-week preference for; setting this variable
					higher than (Timeframe/7) will make it inapplicable
					Let Timeframe = the number of days within which user scores should be
					included in the processed score
					Let Timeframe(nv) = user scores falling within the timeframe
					Let DayWeight = the specific day weighting variable
					Let DowWeight = the day of the week weighting variable*/
			
			
			Calendar todayCalendar=Calendar.getInstance();
			todayCalendar.setTime(new Date());
			//ArrayList<ScoreData> usersScoreData=this.recommendationDao.getListofUsersScoreData();
			//Integer n=usersScoreData.size(); //users score i.e, rows in score data
			//ArrayList<ScoreData> usersVenueScoreData=this.recommendationDao.getListofUsersScoreDataByVenueId(venueId);
			ArrayList<UserLocationRatings> usersVenueScoreData =this.recommendationDao.getUserLocationRatingsByLocationAPIId(venueId);
			Integer nv=usersVenueScoreData.size(); // all the users score for a given Venue
			
					Integer apiThreshold = 10;  //number of data points
					Integer dowThreshold = 5;  //number of data points
					Integer dayThreshold = 3; // number of data points
					Integer memorySpan = 5; // number of weeks
					Integer timeframe = 90; // number of days
					Double dowWeight = .5 ;// weighting variable
					Double dayWeight = .34; // weighting variable
					Integer currentDow = todayCalendar.get(Calendar.DAY_OF_WEEK); // what day of week today is
					
					// Calculate Weighting Constants
					
						//weight for the OtherDow data points
						Double othWeight =1-dowWeight;
						
						//weight for the DoW data points when day-of data is available
						Double dowDayWeight = (1-dayWeight)*dowWeight;
						
						//weight for the OtherDow data points when day-of data is available
						Double othDayWeight = (1-dayWeight)*othWeight;
					
					//group data for vendor
						
						//determine cut-off date based on timeframe
						Date cutoffDate = subtractDays(new Date(), timeframe);
							
						//determine cut-off date for day of week based on memory span
						Date memoryCutoff =subtractDays(new Date(), memorySpan*7);
						Integer todaynv=0;
						Integer todaynvscore=0;
						Integer currentDownv=0;
						Integer currentDownvscore=0;
						Integer otherDownv=0;
						Integer otherDownvscore=0;
						Integer timeframenv=0;
						Integer timeframenvscore=0;
						SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
						String currentDate=dateFormat.format(new Date());
						String memoryCutoffFormat=dateFormat.format(memoryCutoff);
						String cutoffDateFormat=dateFormat.format(cutoffDate);
						//for each vendor
						for(UserLocationRatings venueData:usersVenueScoreData){
							Calendar venueCalendar=Calendar.getInstance();
							venueCalendar.setTime(venueData.getRatingDate());
							
							//find all data from today
							if(venueData.getRatingDate().compareTo(dateFormat.parse(currentDate))==0){
								todaynv=todaynv+1; //number of rows 
								todaynvscore=todaynvscore+venueData.getRating();
							}
							
							//find all data from the current day of the week within the memory cutoff
							if((venueData.getRatingDate().compareTo(dateFormat.parse(memoryCutoffFormat))>=0) && (venueCalendar.DAY_OF_WEEK==currentDow))  {
								currentDownv=currentDownv+1;
								currentDownvscore=currentDownvscore+venueData.getRating();
							}
							
							//find all data from all the other days of the week within timeframe
							if((venueData.getRatingDate().compareTo(dateFormat.parse(cutoffDateFormat))>=0)&& (venueCalendar.DAY_OF_WEEK!=currentDow))  {
								otherDownv=otherDownv+1;
								otherDownvscore=otherDownvscore+venueData.getRating();
							}
							
							//find all data within the timeframe
							if(venueData.getRatingDate().compareTo(dateFormat.parse(cutoffDateFormat))>=0){
								timeframenv=timeframenv+1;
								timeframenvscore=timeframenvscore+venueData.getRating();
							}
						}
						
						//for loop to calculate final scores
						
						Double tempDow=0.0;
						Double tempOth=0.0;
						Double tempDay=0.0;
						//for each vendor
						//for(ScoreData venueData:usersVenueScoreData){
							//CASE ONE: Use API data
							// if there is less data than the API threshold
						/*ApiData apiData=new ApiData();
						try{
						 apiData=this.recommendationDao.getApiScorebyVenueId(venueId);
						}
						catch(ApiScoreDataNotFoundException e){
							apiData.setApiScore(4.0); // If no score is available maintaing the score as 4 by default
						}*/
							if(nv>=apiThreshold){
								//use the API score
								finalScore=apiScore;
							}
							else{
								//CASE TWO: Use all data in the timeframe
								//if there is less data for that day of the week than the day of week threshold
								if(currentDow<dowThreshold){
									//use the unweighted mean of all of the data within the timeframe
									finalScore=mean(timeframenvscore,timeframenv);
								}
								else{
									//CASE THREE: Weight the average by the day of the week
									//if there is less data for today’s date than the daily threshold
									if(todaynv<dayThreshold){
										//calculate the weighted average of the data
										tempDow =  mean(currentDownvscore,currentDownv)*dowWeight;
										tempOth =  mean(otherDownvscore,otherDownv)*othWeight;
										finalScore=tempDow+tempOth;
									}
									else{
										//CASE FOUR: Weight the average by the data from
										// that day, then the data from that day of the
										// week, then the rest of the data in the
										// timeline
										//calculate the weighted average of the data
										
										tempDay =  mean(todaynvscore,todaynv)*dayWeight;
										tempDow =  mean(currentDownvscore,currentDownv)*dowDayWeight;
										tempOth =  mean(otherDownvscore,otherDownv)*othDayWeight;
										finalScore=tempDay+tempDow+tempOth;
									}
								}
							}
							//}
						
	
		}
		catch(UserLocationRatingsNotFoundException e){
			
		}
		
		// Saving the final Score to db .If already exists in db.. updating the score , otherwise creating the venue score.
		
		VenueScoreData venueScoreData=null;
		try{
			venueScoreData=this.recommendationDao.getVenueScoreByVenueId(venueId);
			venueScoreData.setScoreDate(new Date());
			venueScoreData.setVenueScore(finalScore);
		}
		catch(VenueScoreDataNotFoundException e){
			venueScoreData=new VenueScoreData();
			venueScoreData.setVenueId(venueId);
			venueScoreData.setScoreDate(new Date());
			venueScoreData.setVenueScore(finalScore);
		}
		
		// If the final score is zero, setting the google places rating or avg rating of the users as api score
		if(finalScore==0.0){
			finalScore=apiScore;
		}
		return finalScore;
	}
	
	
	/**
	 * subtract days to date in java
	 * @param date
	 * @param days
	 * @return
	 * 
	 * created by bhagya on may 14th, 2018
	 */
	public static Date subtractDays(Date date, int days) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, -days);
				
		return cal.getTime();
	}
	/**
	 * Calculate the mean Value
	 * @param sum
	 * @param length
	 * @return
	 * 
	 * Created by bhagya on may 14th, 2018
	 */
	public static Double mean(Integer sum, Integer length){
		Double average=(double) (sum/length);
		return average;
	}

	/**
	 * Created By Bhagya on April 02nd , 2019
	 * @param locationAPIId
	 * @return
	 * 
	 * Service for getting user rating count by user locationId
	 */
	
	public Integer getUserRatingByLocationId(String locationAPIId){
		log.info("Inside RecommendationServiceImpl ->getUserRatingByLocationId()");
		Integer userRatingsCount=0;
		try{
			userRatingsCount=this.recommendationDao.getUserRatingByLocationId(locationAPIId);
		}
		catch(UserLocationRatingsNotFoundException e){
			userRatingsCount=0;
		}
		return  userRatingsCount;
	}
	
	/**
	 * Created By Bhagya on May 21st, 2019
	 * @return
	 * @throws Exception
	 */
	public MasterGeoLocationConfigurationDto getMasterGeoLocationConfiguration() throws Exception{
		MasterGeoLocationConfiguration configuration=this.recommendationDao.getMasterGeoLocationConfiguration();
		MasterGeoLocationConfigurationDto configurationDto=MasterGeoLocationConfigurationDto.populateMasterGeoLocationConfigurationDto(configuration);
		return configurationDto;
	}
}
	
	
	
	
	
	
	
	
