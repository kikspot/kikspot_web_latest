package com.kikspot.frontend.admin.dto;

import com.kikspot.backend.admin.model.MasterGeoLocationConfiguration;



/**
 * Created By Bhagya On May 21st,2019
 *	Dto Class For Master Geo-Location Configuration
 */
public class MasterGeoLocationConfigurationDto{
	
	private Integer masterId;
	private Double latitude;
	private Double longitude;
	
	public Integer getMasterId() {
		return masterId;
	}
	public void setMasterId(Integer masterId) {
		this.masterId = masterId;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public static MasterGeoLocationConfigurationDto populateMasterGeoLocationConfigurationDto(MasterGeoLocationConfiguration configuration){
		MasterGeoLocationConfigurationDto configurationDto=new MasterGeoLocationConfigurationDto();
		configurationDto.setMasterId(configuration.getMasterId());
		configurationDto.setLatitude(configuration.getLatitude());
		configurationDto.setLongitude(configuration.getLongitude());
		
		return configurationDto;
	}
	
}