package com.kikspot.frontend.admin.service;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.admin.dao.AdminDao;
import com.kikspot.backend.admin.dao.AdminDaoImpl;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

/**
 * Created By Bhagya On December 03rd,2015
 *	Implementation class for Admin Service
 */
@Transactional
@Service("adminService")
public class AdminServiceImpl implements AdminService{
	
	private static Logger log=Logger.getLogger(AdminServiceImpl.class);
	
	
	
}