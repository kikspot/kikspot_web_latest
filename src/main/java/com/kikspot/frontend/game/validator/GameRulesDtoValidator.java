package com.kikspot.frontend.game.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.kikspot.frontend.game.dto.GameRulesDto;
/**
 * Created By Bhagya On December 15th,2015
 *	Validator For Game Rules
 */

@Component("gameRulesDtoValidator")
public class GameRulesDtoValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> paramClass) {
		// TODO Auto-generated method stub
		return GameRulesDto.class.equals(paramClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"rule","Rule Cannot be Empty","Rule Cannot be Empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"creds","Creds Cannot be Empty","Creds Cannot be Empty");
		
	}
}