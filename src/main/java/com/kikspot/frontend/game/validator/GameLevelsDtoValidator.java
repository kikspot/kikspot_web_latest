package com.kikspot.frontend.game.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.kikspot.frontend.game.dto.GameLevelsDto;
/**
 * Created By Bhagya On December 15th,2015
 *	Validator For Game Levels
 */
@Component("gameLevelsDtoValidator")
public class GameLevelsDtoValidator implements Validator{

	@Override
	public boolean supports(Class<?> paramClass) {
		// TODO Auto-generated method stub
		return GameLevelsDto.class.equals(paramClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// TODO Auto-generated method stub
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"level","Level Cannot be Empty","Level Cannot be Empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"minCreds","Minimum Creds Cannot be Empty","Minimum Creds Cannot be Empty");
	}
	
}