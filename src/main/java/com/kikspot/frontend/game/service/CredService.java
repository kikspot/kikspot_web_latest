package com.kikspot.frontend.game.service;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.game.dto.CredRulesDto;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;
import com.kikspot.frontend.game.dto.UserCredHistoryDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;

public interface CredService {

	public void addCredPostUserSignup(KikspotUser kikspotUser)throws Exception;
	public CredRulesDto getCredRuleBasedonTypeCredType (String credTypeValue)throws Exception;
	public UserGameCredsDto getUserGameCredsofUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException;
	public ArrayList<UserGameCredsDto> getTopFiveUserGameCredDtos()throws UserGameCredNotFoundException;
	public UserCredHistoryDto getMostRecentCredHistoryOfUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException;
	public Integer updateUserCreds(KikspotUser kikspotUser,Integer creds)throws Exception;
	public Integer saveOrUpdateKikspotRewards(KikspotRewardsDto kikspotRewardsDto) throws Exception;
	public ArrayList<KikspotRewardsDto> getAllKikspotRewards(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRewardNotFoundException;
	public KikspotRewardsDto getKikspotRewardByRewardId(Integer rewardId) throws KikspotRewardNotFoundException;
	public Integer updateKikspotRewardStatus(Integer rewardId,Boolean isActive) throws Exception;
	public Integer deleteKikspotRewardByRewardId(Integer rewardId) throws Exception;
	public ArrayList<KikspotRewardsDto> getKikspotRewardsOfUserByUserLevelAndCreds(Integer userId) throws UserNotFoundException, UserGameCredNotFoundException, KikspotRewardNotFoundException;
	public KikspotRewardsDto getKikspotRewardByRewardIdForUser(Integer userId,Integer rewardId) throws Exception;
	public Integer updateAllUsersCreds() throws Exception;
	public Integer getCredsOfUserInSixMonths(KikspotUserDto kikspotUserDto) throws Exception;
}
