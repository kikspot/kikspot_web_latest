package com.kikspot.frontend.game.service;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import twitter4j.org.json.JSONException;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleExceedsRuleLimitException;
import com.kikspot.backend.exceptions.GameRuleExpiredException;
import com.kikspot.backend.exceptions.GameRuleInActiveException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.GamesNotFoundException;
import com.kikspot.backend.exceptions.InvalidRequestException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.backend.exceptions.OverQueryLimitException;
import com.kikspot.backend.exceptions.PlaceNotFoundException;
import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.RequestDeniedException;
import com.kikspot.backend.exceptions.UnknownErrorException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.exceptions.UserLocationNotFoundException;
import com.kikspot.backend.game.dao.CredDao;
import com.kikspot.backend.game.dao.GameDao;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.Game;
import com.kikspot.backend.game.model.GameLevels;
import com.kikspot.backend.game.model.GameRuleTypes;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.NotificationTypes;
import com.kikspot.backend.recommendation.dao.RecommendationDao;
import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;
import com.kikspot.backend.recommendation.model.UserLocationRatings;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.user.model.UserLocations;
import com.kikspot.backend.user.model.UserType;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.game.dto.GameDto;
import com.kikspot.frontend.game.dto.GameLevelsDto;
import com.kikspot.frontend.game.dto.GameRuleTypeDto;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserCredHistoryDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.notification.service.NotificationService;
import com.kikspot.frontend.recommendation.dto.PlaceDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.helper.googlePlacesAPI.GooglePlacesAPIProvider;
import com.kikspot.frontend.recommendation.service.RecommendationService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.dto.UserLocationDto;
import com.kikspot.frontend.useraction.dto.FeedbackDto;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;




@Service("gameService")
public class GameServiceImpl implements GameService {

	private static Logger log=Logger.getLogger(GameServiceImpl.class);
	
	@Resource(name="gameDao")
	private GameDao gameDao;

	
	
	@Resource(name="credService")
	private CredService credService;
	
	
	@Resource(name="userDao")
	private UserDao userDao;
	
	/** Added Recommendation Dao Resource By Bhagya ON Jan 04th,2016*/
	@Resource(name="recommendationDao")
	private RecommendationDao recommendationDao;
	
	/** Added Cred Dao Resource By Bhagya ON Jan 07th,2016*/
	@Resource(name="credDao")
	private CredDao creadDao;
	
	@Resource(name="googlePlacesAPIProvider")
	private GooglePlacesAPIProvider googlePlacesAPIProvider;
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	@Resource(name="notificationService")
	private NotificationService notificationService;
	
	private static ApnsService service;
	
	
	
/* ** Push Notification Parameters */	
	private String enviroment;
	
	private String passphrase;
	
	private String certFile;

	

	

	public String getEnviroment() {
		return enviroment;
	}

	public void setEnviroment(String enviroment) {
		this.enviroment = enviroment;
	}

	public String getPassPhrase() {
		return passphrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passphrase = passPhrase;
	}

	public String getCertFile() {
		return certFile;
	}

	public void setCertFile(String certFile) {
		this.certFile = certFile;
	}
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 * Method to getPushNotificationService..
	 * 
	 * 
	 * @throws IOException
	 */
	public void getPushNotificationService()throws IOException{
		log.info("inside getPushNotificationService() ");
		System.out.println(certFile+" "+enviroment+" "+passphrase);
		InputStream certificate=this.getClass().getClassLoader().getResourceAsStream(certFile);
		if(enviroment.equals("sandbox")){
			service=APNS
					.newService()
					.withCert(certificate, passphrase)
					.withSandboxDestination()
					.build();
		}
		else if(enviroment.equals("production")){
			service=APNS
					.newService()
					.withCert(certificate, passphrase)
					.withProductionDestination()
					.build();
		}
		
		/*Map<String, Date> devices=service.getInactiveDevices();
		System.out.println("Inactive Devices "+devices.size());
		ArrayList<String> deviceTokens=new ArrayList<String>();
		for(Entry<String, Date> entry:devices.entrySet()){
			deviceTokens.add(entry.getKey());			
		}
		try{
			if(!deviceTokens.isEmpty()){
				ArrayList<UserDevice> userDevices=this.userDao.getUserDeviceByDeviceTokens(deviceTokens);
				for(UserDevice userDevice:userDevices){
					this.userDao.deleteUserDevice(userDevice);
				}
			}
		}
		catch(Exception e){			
		}*/
	}
	/**
	 *  Created by Jeevan on November 27, 2015
	 *  
	 *  Method to get UserDashboard Information;..
	 *  
	 * 
	 * @param userId
	 * @throws Exception
	 * 
	 * 
	 *  Steps:
	 *  
	 *  1. Get User by User Id.
	 *  2. Get Creds of User.
	 *  3. GEt PAst cred History of user and decide trend.
	 *  4. Get GameCreds of Top5 Users (Should be user Referrals).
	 *  5. GEt Game Rules
	 *  6. Populate DashboardDto,send..
	 * 
	 * 
	 * 
	 */
	public UserDashboardDto getUserDashboardInformation(Integer userId,double latitude,double longitude)throws Exception{
		log.info("inside getUserDashboardInformation() ");
		UserDashboardDto userDashboardDto=new UserDashboardDto();
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		if(!kikspotUser.getUserType().equals(UserType.BYPASS_USER)){
			UserGameCredsDto userGameCredsDto=this.credService.getUserGameCredsofUser(kikspotUser);		
			UserCredHistoryDto userCredHistoryDto=this.credService.getMostRecentCredHistoryOfUser(kikspotUser);
			userDashboardDto.setUserGameCredDto(userGameCredsDto);
			if(userCredHistoryDto.getCred()<=userGameCredsDto.getCreds()){
				userDashboardDto.setTrending("UP");
			}
			else{
				userDashboardDto.setTrending("DOWN");
			}	
			ArrayList<UserGameCredsDto> userGameCredDtos=this.credService.getTopFiveUserGameCredDtos();		
			userDashboardDto.setUserGameCredsDtos(userGameCredDtos);
		}
		else{
			UserGameCredsDto userGameCredsDto=new UserGameCredsDto();
			userGameCredsDto.setCreds(0);
			userDashboardDto.setTrending("UP");
			
			userDashboardDto.setUserGameCredDto(userGameCredsDto);
		}
		
		try{
			ArrayList<GameRules> gameRules=this.gameDao.getAllGameRules(true, true, null,null);
			ArrayList<GameRulesDto> allGameRules=new ArrayList<GameRulesDto>();
			ArrayList<GameRulesDto> gameRuleDtos=new ArrayList<GameRulesDto>();
			ArrayList<GameRulesDto> globalGameRules=new ArrayList<GameRulesDto>();
			ArrayList<GameRules> filteredGameRules=this.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(gameRules);
			for(GameRules gameRule:filteredGameRules){
				
				if(null==gameRule.getLatitude() && null==gameRule.getLongitude()){
					 GameRulesDto gameRuleDto=GameRulesDto.populateGameRulesDto(gameRule);
					  gameRuleDto.setLocationName("GLOBAL");
					  globalGameRules.add(gameRuleDto);
				}
				
				else if(this.recommendationService.distance(gameRule.getLatitude(),gameRule.getLongitude(),latitude,longitude)<0.7){
				  GameRulesDto gameRuleDto=GameRulesDto.populateGameRulesDto(gameRule);
				  gameRuleDto.setDistance(this.recommendationService.distance(latitude,longitude, gameRule.getLatitude(), gameRule.getLongitude()));
				  gameRuleDtos.add(gameRuleDto);
				 }
			}
			gameRuleDtos=this.sortGameRulesDtos(gameRuleDtos,"close");
			if(globalGameRules.size()>0){
				allGameRules.addAll(globalGameRules);
			}
			allGameRules.addAll(gameRuleDtos);
			userDashboardDto.setGameRulesDtos(allGameRules);
		}
		
		catch(GameRuleNotFoundException e){			
		}
		
	  
		userDashboardDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(kikspotUser));
		return userDashboardDto;
		
	}
	
	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on November 26, 2015
	 * 
	 * Method to get GameRules...
	 * 
	 * @return
	 * @throws Exception 
	 * 
	 * 
	 * 
	 */
	
	public ArrayList<GameRulesDto> getGameRules(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending,String locationFilter)throws Exception{
		log.info("inside getGameRules()");
		ArrayList<GameRules> gameRules=this.gameDao.getGameRulesofGame(1,pageNo,pageSize,sortBy,searchBy,ascending,locationFilter);
		ArrayList<GameRulesDto> gameRuleDtos=new ArrayList<GameRulesDto>();
		
		for(GameRules gameRule:gameRules){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				gameRule.setIsActive(false);
			}
			if(null!=gameRule.getRuleLimit() && gameRule.getRuleLimit()==0){
				gameRule.setIsActive(false);
			}
			this.gameDao.saveorUpdateGameRule(gameRule);
			GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
			gameRuleDtos.add(gameRulesDto);
		}
		return gameRuleDtos;
	}

	
	
	
	
   /**
    * 
    * added by Firdous on 02-12-2015
    * method to get the game rule by game rule id
    * 
    */

	@Override
	public GameRulesDto getGameRuleById(Integer id) throws Exception {
		log.info("inside getGameRuleById");
		GameRules gamerule=this.gameDao.getGameRuleByRuleId(id);
		GameRulesDto gameRuleDto=GameRulesDto.populateGameRulesDto(gamerule);
		return gameRuleDto;
		
	}


	/**
	 * Created By Bhagya on December 11th,2015
	 * @param gameRuleId
	 * @param isActive
	 * @return
	 * @throws Exception
	 * 
	 * Method For saving the updated status of game rule
	 */
	
	public Integer savingchangedGameRuleStatus(Integer gameRuleId, Boolean isActive) throws Exception{
		log.info("inside savingchangedGameRuleStatus()");
		GameRules gameRule=this.gameDao.getGameRuleByRuleId(gameRuleId);
		Boolean openDateRestriction=true;
		Boolean closeDateRestriction=true;
		if(isActive==true){
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				gameRule.setIsActive(false);
			}
			else{
				gameRule.setIsActive(true);
			}
		}
		else{
			gameRule.setIsActive(isActive);
		}
		Integer updatedResult=	this.gameDao.saveorUpdateGameRule(gameRule);
		return updatedResult;
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelDto
	 * @return
	 * @throws Exception
	 * 
	 * Method For Save Or Update the Game Level
	 */
	
	public Integer saveOrUpdateGameLevel(GameLevelsDto gameLevelDto) throws Exception{
		log.info("inside saveOrUpdateGameLevel()");
		GameLevels gameLevel=null;
		if(null!=gameLevelDto.getGameLevelId() && gameLevelDto.getGameLevelId()>0){
			 gameLevel=this.gameDao.getGameLevelByGameLevelId(gameLevelDto.getGameLevelId());
		}
		else{
			 gameLevel=new GameLevels();
		}
		Game game=this.gameDao.getGameById(1);
		gameLevel.setGame(game);
		gameLevel.setMinCreds(gameLevelDto.getMinCreds());
		gameLevel.setLevel(gameLevelDto.getLevel());
		Integer result=this.gameDao.saveorUpdateGameLevel(gameLevel);
		this.updateUsersLevelsBasedOnTheirCreds();//Updating the creds after levels edited
		return result;
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameRulesDto
	 * @return
	 * @throws Exception
	 * 
	 * Method For Save Or Update the Game Rule
	 * Steps: if the LocationId is numeric value means ,get the kikspot recommendation location and saving the locationName
	 * 		If the LocationId is String,setting the Location Name as Null.
	 * 		
	 * 	For Implementing CLOSE BY SORTING(As per discussion We find this as BEST SOLUTION)
	 * 		If the locationId is string means,Interact with the GooglePlacesApi and saving the latitude and longitude
	 */
	public Integer saveOrUpdateGameRule(GameRulesDto gameRulesDto) throws Exception{
		log.info("inside saveOrUpdateGameRule()");
		GameRules gameRule=null;
		if(null!=gameRulesDto.getGameRuleId() && gameRulesDto.getGameRuleId()>0){
			gameRule=this.gameDao.getGameRuleByRuleId(gameRulesDto.getGameRuleId());
		}
		else{
			gameRule=new GameRules();
			gameRule.setVisible(true);
		}
		Game game=this.gameDao.getGameById(1);
		gameRule.setGame(game);
		/*if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
			gameRule.setLocationId(gameRulesDto.getLocationId());
		}*/
		try{
			gameRule.setLocationId(gameRulesDto.getLocationId());
			Integer locationId=Integer.parseInt(gameRulesDto.getLocationId());
			
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
			//KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByRecommendationLocationId(gameRulesDto.getLocationId());
			gameRule.setLocationName(kikspotRecommendationLocation.getLocationName());
			gameRule.setLatitude(kikspotRecommendationLocation.getLatitude());
			gameRule.setLongitude(kikspotRecommendationLocation.getLongitude());
		}
		catch(NumberFormatException e){
			//gameRule.setLocationName(null);
			try{
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByKikspotLocationId(gameRulesDto.getLocationId());
			String locationId=String.valueOf(kikspotRecommendationLocation.getLocationId());
			gameRule.setLocationId(locationId);
			gameRule.setLocationName(kikspotRecommendationLocation.getLocationName());
			gameRule.setLatitude(kikspotRecommendationLocation.getLatitude());
			gameRule.setLongitude(kikspotRecommendationLocation.getLongitude());
			}
			catch(KikspotRecommendationLocationNotFoundException e1){
				
				//gameRule.setLocationName(null);
				if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
					try{
					PlaceDto placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(gameRulesDto.getLocationId());
					gameRule.setLocationName(placeDto.getPlaceName());
					gameRule.setLatitude(placeDto.getLatitude());
					gameRule.setLongitude(placeDto.getLongitude());
					}
					catch(InvalidRequestException e2){
						e.printStackTrace();
						System.out.println(" INVALID API REQUEST ,Requested Location Id not valid one  " +gameRulesDto.getLocationId() );
					}
				}
			}
		
			
		}
		
		if(null!=gameRulesDto.getUserId()){
			gameRule.setUserId(gameRulesDto.getUserId());
		}
		else{
			gameRule.setUserId(null);
		}
		gameRule.setGameRuleType(gameRulesDto.getGameRuleType());
		gameRule.setCreds(gameRulesDto.getCreds());
		gameRule.setRule(gameRulesDto.getRule());
		gameRule.setRuleLimit(gameRulesDto.getRuleLimit());
		gameRule.setExpiryDate(gameRulesDto.getExpiryDate());
		gameRule.setOpenDate(gameRulesDto.getOpenDate());
		gameRule.setStartTime(gameRulesDto.getStartTime());
		gameRule.setEndTime(gameRulesDto.getEndTime());
		Boolean openDateRestriction=true;
		Boolean closeDateRestriction=true;
		if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
			openDateRestriction=false;
		}
		if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
			closeDateRestriction=false;
		}
		if(openDateRestriction==false || closeDateRestriction==false){
			gameRule.setIsActive(false);
		}
		else{
			gameRule.setIsActive(true);
		}
	
	    Integer result=this.gameDao.saveorUpdateGameRule(gameRule);
		return result;
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @param ascending
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the Game Levels
	 */
	
	public ArrayList<GameLevelsDto> getGameLevels(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws Exception{
		log.info("inside getGameLevels()");
		ArrayList<GameLevels> gameLevels=this.gameDao.getGameLevelsofGame(1, pageNo, pageSize, sortBy, searchBy, ascending);
		ArrayList<GameLevels> sortedGmaeLevels=this.sortGameLevels(gameLevels);
		ArrayList<GameLevelsDto> gameLevelsDtos=new ArrayList<GameLevelsDto>();
		for(GameLevels gameLevel:sortedGmaeLevels){
			GameLevelsDto gameLevelsDto=GameLevelsDto.populateGameLevelDtos(gameLevel);
			gameLevelsDtos.add(gameLevelsDto);
		}
		return gameLevelsDtos;
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the GameLevel By GameLevelId
	 */
	
	public GameLevelsDto getGameLevelByGameLevelId(Integer gameLevelId) throws Exception{
		GameLevels gameLevel=this.gameDao.getGameLevelByGameLevelId(gameLevelId);
		GameLevelsDto gameLevelsDto=GameLevelsDto.populateGameLevelDtos(gameLevel);
		return gameLevelsDto;
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelId
	 * @return
	 * @throws Exception
	 * 
	 * Method For Deleting The Game Level
	 */
	public Integer deleteGameLevelByGameLevelId(Integer gameLevelId) throws Exception{
		GameLevels gameLevel=this.gameDao.getGameLevelByGameLevelId(gameLevelId);
		Integer deletedResult=this.gameDao.deleteGameLevel(gameLevel);
		this.updateUsersLevelsBasedOnTheirCreds();// If the level is deleted means,Based On the existing levels,updating the creds and levels to user
		return deletedResult;
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameRuleId
	 * @return
	 * @throws Exception
	 * 
	 * Method For deleting the Game Rule By GameRuleId
	 */
	public Integer deleteGameRuleByGameRuleId(Integer gameRuleId) throws UserGameCredNotFoundException,Exception{
		GameRules gameRule=this.gameDao.getGameRuleByRuleId(gameRuleId);
		Integer deletedResult=0;
		try{
		ArrayList<UserGameCreds> userGameCreds=this.gameDao.getUsersGameCredsByGameRule(gameRule);
		}
		catch(UserGameCredNotFoundException e){
			deletedResult=this.gameDao.deleteGameRule(gameRule);
		}
		
		return deletedResult;
	}
	/**
	 * Created By Bhagya On December 31st,2015
	 * @param locationId
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method for Getting the Game Rules By LocationId
	 */
	
	public ArrayList<GameRulesDto> getGameRulesByLocationId(String locationId) throws GameRuleNotFoundException{
		log.info("inside getGameRulesByLocationId()");
		ArrayList<GameRules> gameRules=this.gameDao.getGameRulesByLocationId(locationId);
		ArrayList<GameRulesDto> gameRulesDtos=new ArrayList<GameRulesDto>();
		for(GameRules gameRule:gameRules){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				gameRule.setIsActive(false);
			}
			GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
			gameRulesDtos.add(gameRulesDto);
		}
		return gameRulesDtos;
	}
	public ArrayList<GameRulesDto> getGameRulesByOnlyLocationId(String locationId) throws GameRuleNotFoundException{
		log.info("inside getGameRulesByLocationId()");
		ArrayList<GameRules> gameRules=this.gameDao.getGameRulesByOnlyLocationId(locationId);
		ArrayList<GameRulesDto> gameRulesDtos=new ArrayList<GameRulesDto>();
		for(GameRules gameRule:gameRules){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				gameRule.setIsActive(false);
			}
			GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
			gameRulesDtos.add(gameRulesDto);
		}
		return gameRulesDtos;
	}
	
	public ArrayList<GameRulesDto> getGameRulesByLocationIdAndUserId(String locationId,Integer userId) throws GameRuleNotFoundException{
		log.info("inside getGameRulesByLocationIdAndUserId()");
		System.out.println("getGameRulesByLocationIdAndUserId "+userId);
		ArrayList<GameRules> gameRules=new ArrayList<GameRules>();
		try{
		 gameRules=this.gameDao.getGameRulesByLocationIdAndUserId(locationId, userId);
		}
		catch(GameRuleNotFoundException e){
			
		}
		try{
		gameRules.addAll(this.gameDao.getGameRulesByOnlyLocationId(locationId));
		}
		
		catch(GameRuleNotFoundException e){
			
		}
		
		ArrayList<GameRulesDto> gameRulesDtos=new ArrayList<GameRulesDto>();
		
		for(GameRules gameRule:gameRules){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				gameRule.setIsActive(false);
			}
			GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
			
			gameRulesDtos.add(gameRulesDto);
			
		}
		return gameRulesDtos;
	}
	/**
	 * Created By Bhagya On Jan 04th,2016
	 * @param locationId
	 * @param rule
	 * @param creds
	 * @param ruleLimit
	 * @param expiryDate
	 * @param isActive
	 * @return
	 * @throws Exception
	 * 
	 * Method For Saving the Game Rule for recommendation Location
	 */
	
	public Integer addGameRuleForRecommendationLocation(Integer locationId,String rule,Integer creds,Integer userId,String gameRuleType,Integer ruleLimit,String startTime,String endTime,Date openDate,Date expiryDate,Boolean isActive) throws Exception{
		log.info("inside addGameRuleForRecommendationLocation()");
		GameRules gameRule=new GameRules();
		Game game=this.gameDao.getGameById(1);
		gameRule.setGame(game);
		gameRule.setLocationId(locationId.toString());
		//set the location Name
		KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
		gameRule.setLocationName(kikspotRecommendationLocation.getLocationName());
		gameRule.setRule(rule);
		gameRule.setCreds(creds);
		gameRule.setRuleLimit(ruleLimit);
		SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm a");
		
		if(null!=startTime && startTime.length()>0){
			gameRule.setStartTime(timeformat.parse(startTime));
		}
		if(null!=endTime && endTime.length()>0){
			gameRule.setEndTime(timeformat.parse(endTime));
		}
		if(null!=expiryDate){
			gameRule.setExpiryDate(expiryDate);
		}
		if(null!=openDate){
			gameRule.setOpenDate(openDate);
		}
		Boolean openDateRestriction=true;
		Boolean closeDateRestriction=true;
		if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
			openDateRestriction=false;
		}
		if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
			closeDateRestriction=false;
		}
		if(openDateRestriction==false || closeDateRestriction==false){
			gameRule.setIsActive(false);
		}
		else{
			gameRule.setIsActive(true);
		}
		gameRule.setVisible(true);
		gameRule.setGameRuleType(gameRuleType);
		if(null!=userId){
			gameRule.setUserId(userId);
		}
		gameRule.setLatitude(kikspotRecommendationLocation.getLatitude());
		gameRule.setLongitude(kikspotRecommendationLocation.getLongitude());
		Integer savedResult=this.gameDao.saveorUpdateGameRule(gameRule);
		return savedResult;
	}
	/**
	 * Created By Bhagya On Jan 05th,2015
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method for getting the Distinct locations Of a game Rules
	 */
	
	
	public ArrayList<String> getDistinctLocationsOfGameRules(){
		log.info("inside getDistinctLocationsOfGameRules()");
		ArrayList<String> locations=new ArrayList<String>();
		try{
			locations=this.gameDao.getDistinctLocationsOfGameRules();
			if(locations.contains("") && !locations.contains("GLOBAL")){
				locations.remove("");
				locations.add("GLOBAL");
				
			}	
		}
		catch(GameRuleNotFoundException e){
			
		}
		
		return locations;
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param locationId
	 * @param isActive
	 * @param checkExpiryDate
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method for getting the Game Rules Of a Location By Location Id For User
	 */
	
	public ArrayList<GameRulesDto> getGameRulesOfaLocationByLocationIdForUser(String locationId,Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException{
		log.info("inside getGameRulesOfaLocationByLocationIdForUser()");
		ArrayList<GameRules> locationGameRules=this.gameDao.getGameRulesOfaLocationByLocationIdForUser(locationId, isActive, checkExpiryDate,sortBy,userId);
		
		ArrayList<GameRules> filteredGameRules=this.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(locationGameRules);
		ArrayList<GameRulesDto> locationGameRulesDtos=new ArrayList<GameRulesDto>();
		/*UserLocations userLocation=null;
		if(null!=userId && userId>0){
			try{
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
			 userLocation=this.userDao.getUserLocationsOfUser(kikspotUser);
			}
			catch(UserNotFoundException e){
				
			}
			catch(UserLocationNotFoundException e){
				e.printStackTrace();
			}
			
		}*/
		for(GameRules locationGameRule:filteredGameRules){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			if(null!=locationGameRule.getOpenDate() && locationGameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=locationGameRule.getExpiryDate() && locationGameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				locationGameRule.setIsActive(false);
			}
			try {
				this.gameDao.saveorUpdateGameRule(locationGameRule);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(locationGameRule.getIsActive()==true){
				GameRulesDto locationGameRulesDto=GameRulesDto.populateGameRulesDto(locationGameRule);
				try {
					if(null!=locationGameRulesDto.getLocationId() && locationGameRulesDto.getLocationId().trim().length()>0){
						RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(locationGameRulesDto.getLocationId(),null,null,null);
						locationGameRulesDto.setLocationName(recommendationLocationDto.getLocationName());
						//locationGameRulesDto.setDistance(this.recommendationService.distance(userLocation.getLatitude(), userLocation.getLongitude(), recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude()));
						locationGameRulesDtos.add(locationGameRulesDto);
					}
					/*else{
						locationGameRulesDto.setLocationName("GLOBAL");
					}*/
				} 
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(null!=locationGameRule.getLatitude() && null!=locationGameRule.getLongitude()){
					//locationGameRulesDto.setDistance(this.recommendationService.distance(userLocation.getLatitude(), userLocation.getLongitude(), locationGameRule.getLatitude(), locationGameRule.getLongitude()));
				}
			}			
			//locationGameRulesDtos.add(locationGameRulesDto);
		}
		
		return locationGameRulesDtos;
		
		
	}
	/**
	 * Created By Bhagya Jan 06th,2016
	 * @param isActive
	 * @param checkExpiryDate
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method For Getting All GAme Rules
	 * Steps: 1. Getting the Game Rules by checking the isActive and expirydate and if sortBy is not null means ,we check the sortBy condition at  dao
	 * 
	 * 	2. Get the User By UserId
	 * 	3.Get the User Location By Kikspot User
	 * 	4. If SORY BY equal to equal to CLOSE or NULL means , we are sorting by distance
	 * 			if lat and long is not equal to null means add those to dto and sort those results
	 * 			else, lat and lng is null means add those global gamerulesdto
	 * 		Add those adding that globalGame rules to dtos
	 * 	else  SORT BY equal to time ,sorted at dao.. populate the dto and returning the result
	 * 
	 * Getting the locations with in 0.7 miles.. 
	 * Modified on feb 26th, 2019 as per the client requirement
	 * Modified the location distance -- fetching the game rules with in 5 miles
	 * 
	 */
	
	public ArrayList<GameRulesDto> getAllGameRules(Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId,double latitude,double longitude) throws GameRuleNotFoundException{
		log.info("inside getAllGameRules()");
		ArrayList<GameRulesDto> allGameRules=new ArrayList<GameRulesDto>();
		ArrayList<GameRules> gameRules=this.gameDao.getAllGameRules(isActive, checkExpiryDate,sortBy,userId);
		ArrayList<GameRules> filteredGameRules=this.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(gameRules);
		ArrayList<GameRulesDto> gameRulesDtos=new ArrayList<GameRulesDto>();
		UserLocations userLocation=null;
		if(null!=userId && userId>0){
			try{
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
			 userLocation=this.userDao.getUserLocationsOfUser(kikspotUser);
			}
			catch(UserNotFoundException e){
				
			}
			catch(UserLocationNotFoundException e){
				e.printStackTrace();
			}
			
		}
		ArrayList<GameRulesDto> globalGameRulesDtos=new ArrayList<GameRulesDto>();
		if(null==sortBy || sortBy.trim().length()<1 || sortBy.equalsIgnoreCase("time")){
			for(GameRules gameRule:filteredGameRules){
				GameRulesDto gameRulesDto=null;
				/*if(gameRule.getLatitude()==null){
					GameRulesDto globalGameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
					globalGameRulesDto.setLocationName("GLOBAL");
					globalGameRulesDtos.add(globalGameRulesDto);
				}*/
				
				
				/**
				 * Modified on may 08th, 2019 as per the client requirement 
				 * Commented the distance condition
				 * 
				 * The radius distance needs to be set to be 5 miles for the Close tab, meaning all active rewards within a 5 miles radius needs to be shown here.
                   All rewards should still be shown for the Time tab no matter the distance.
				 */
				/*if(null!=gameRule.getLatitude() && null!=gameRule.getLongitude() && this.recommendationService.distance(gameRule.getLatitude(),gameRule.getLongitude(),latitude,longitude)<5){*/
					Boolean openDateRestriction=true;
					Boolean closeDateRestriction=true;
					if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
						openDateRestriction=false;
					}
					if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
						closeDateRestriction=false;
					}
					if(openDateRestriction==false || closeDateRestriction==false){
						gameRule.setIsActive(false);
					}
					try {
						this.gameDao.saveorUpdateGameRule(gameRule);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(gameRule.getIsActive()==true){
						KikspotRecommendationLocation kikspotRecommendationLocation = new KikspotRecommendationLocation();

							gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
						
						try {
							if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
								RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRulesDto.getLocationId(),null,null,null);
								gameRulesDto.setLocationName(recommendationLocationDto.getLocationName());
								gameRulesDto.setDistance(this.recommendationService.distance(userLocation.getLatitude(), userLocation.getLongitude(), recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude()));
								try {
									
									kikspotRecommendationLocation = this.recommendationDao.getKikspotRecommendationLocationByLocationId(Integer.parseInt(gameRule.getLocationId()));
									if(null!=kikspotRecommendationLocation.getIsOpen() && kikspotRecommendationLocation.getIsOpen()==true){
										gameRulesDtos.add(gameRulesDto);
									}
									
									
								} 
								catch(NumberFormatException e){
									
									try{
										kikspotRecommendationLocation = this.recommendationDao.getKikspotRecommendationLocationByGoogleLocationId(gameRule.getLocationId());
										if(null!=kikspotRecommendationLocation.getIsOpen() && kikspotRecommendationLocation.getIsOpen()==true){
											gameRulesDtos.add(gameRulesDto);
										}
									}
									catch(KikspotRecommendationLocationNotFoundException e2){
										gameRulesDtos.add(gameRulesDto);
									}
								}
								
								catch (KikspotRecommendationLocationNotFoundException e1) {
									
								}
								
							}
							// Modified by bhagya on march 06th, 2019 --removed the global rules from rewards screen based on client requirement(SY)
							/*else{
								gameRulesDto.setLocationName("GLOBAL");
							}*/
						} 
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
							
						}
					
				/*}*/ // distance if condition
			}
			allGameRules.addAll(globalGameRulesDtos);
			allGameRules.addAll(gameRulesDtos);
			
			/** applying the sorting order 
			    1) closest expiry date to the current date/time
				2) by highest cred amount
				3) by closest proximity to the user location
				4) Expiry Date is NULL
			 * */
			 
				Collections.sort(allGameRules, new Comparator<GameRulesDto>() {
					@Override
					public int compare(GameRulesDto o1,
							GameRulesDto o2) {
						
						 	// adding multiple fields comparison based on client requirement (rating and distance comparison)
						// below if and else condition for sorting the null expairy date as Last
						if( o1.getExpiryDate() == null && o2.getExpiryDate() == null){
							return 0;
						}
						if (o1.getExpiryDate() == null) {
					        return 1;
					    }
					    if (o2.getExpiryDate() == null) {
					        return -1;
					    }
						    return new CompareToBuilder()
						  .append(o1.getExpiryDate(), o2.getExpiryDate())
						  .append(Math.round(o1.getCreds()), Math.round(o2.getCreds()))
						  .append(o1.getDistance(), o2.getDistance()).toComparison();
						  
			               
			    }	
				});
				
				
			
		}
		else{
			
			/**Modified on feb 26th, 2019 as per the client requirement
			 * Modified the location distance 5miles
			 * 
			 */
			for(GameRules gameRule:filteredGameRules){
				
				if(null!=gameRule.getLatitude() && null!=gameRule.getLongitude() && this.recommendationService.distance(gameRule.getLatitude(),gameRule.getLongitude(),latitude,longitude)<5){
					Boolean openDateRestriction=true;
					Boolean closeDateRestriction=true;
					if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
						openDateRestriction=false;
					}
					if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
						closeDateRestriction=false;
					}
					if(openDateRestriction==false || closeDateRestriction==false){
						gameRule.setIsActive(false);
					}
					try {
						this.gameDao.saveorUpdateGameRule(gameRule);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(gameRule.getIsActive()==true){
						KikspotRecommendationLocation kikspotRecommendationLocation = new KikspotRecommendationLocation();
						GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
						try{
							
							gameRulesDto.setDistance(this.recommendationService.distance(latitude,longitude, gameRule.getLatitude(), gameRule.getLongitude()));
						}
						catch(Exception e){}
						try {
							if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
								
								RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRulesDto.getLocationId(),null,null,null);
								gameRulesDto.setLocationName(recommendationLocationDto.getLocationName());
								try {
									
									kikspotRecommendationLocation = this.recommendationDao.getKikspotRecommendationLocationByLocationId(Integer.parseInt(gameRule.getLocationId()));
									if(null!=kikspotRecommendationLocation.getIsOpen() && kikspotRecommendationLocation.getIsOpen()==true){
										gameRulesDtos.add(gameRulesDto);
									}
									
									
								}
								catch(NumberFormatException e){
									
									try{
										kikspotRecommendationLocation = this.recommendationDao.getKikspotRecommendationLocationByGoogleLocationId(gameRule.getLocationId());
										if(null!=kikspotRecommendationLocation.getIsOpen() && kikspotRecommendationLocation.getIsOpen()==true){
											gameRulesDtos.add(gameRulesDto);
										}
									}
									catch(KikspotRecommendationLocationNotFoundException e2){
										gameRulesDtos.add(gameRulesDto);
									}
								}
								catch (KikspotRecommendationLocationNotFoundException e1) {
									
								}
								
							}
							// Modified by bhagya on march 06th, 2019 --removed the global rules from rewards screen based on client requirement(SY)
							
							/*else{
								gameRulesDto.setLocationName("GLOBAL");
							}*/
						} 
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					// Modified by bhagya on march 06th, 2019 --removed the global rules from rewards screen based on client requirement(SY)
					//gameRulesDtos.add(gameRulesDto);
					}
				}
				/*else if(gameRule.getLatitude()==null){
					GameRulesDto globalGameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
					globalGameRulesDto.setLocationName("GLOBAL");
					globalGameRulesDtos.add(globalGameRulesDto);
				}*/
				
			}
			//gameRulesDtos=this.sortGameRulesDtos(gameRulesDtos, sortBy);
			if(globalGameRulesDtos.size()>0){
				allGameRules.addAll(globalGameRulesDtos);
			}
			allGameRules.addAll(gameRulesDtos);
			/** applying the sorting order 
		    1) by closest proximity to the user location
			2) by highest cred amount
			3) closest expiry date to the current date/time
			4) Expiry Date is NULL
		 * */
			Collections.sort(allGameRules, new Comparator<GameRulesDto>() {
				@Override
				public int compare(GameRulesDto o1,
						GameRulesDto o2) {
					
					 	// adding multiple fields comparison based on client requirement (rating and distance comparison)
					// below if and else condition for sorting the null expairy date as Last
						/*if( o1.getExpiryDate() == null && o2.getExpiryDate() == null){
							return 0;
						}
						if (o1.getExpiryDate() == null) {
					        return 1;
					    }
					    if (o2.getExpiryDate() == null) {
					        return -1;
					    }*/
					    return new CompareToBuilder()
					      .append(o1.getDistance(), o2.getDistance())
						  .append(Math.round(o2.getCreds()), Math.round(o1.getCreds()))
						  .append(o1.getExpiryDate(), o2.getExpiryDate())  .toComparison();
					  
		               
		    }	
			});
			
		}
		return allGameRules;
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * @param locationAPIIds
	 * @param isActive
	 * @param checkExpiryDate
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Method for Getting The Game Rules For List Of Locations
	 * Steps: 1. Getting the Game Rules For List Of Locations by checking the isActive and expirydate and if sortBy is not null means ,we check the sortBy condition at  dao
	 * 
	 * 	2. Get the User By UserId
	 * 	3.Get the User Location By Kikspot User
	 *  4. If SORY BY equal to equal to CLOSE or NULL means , we are sorting by distance
	 * 			if lat and long is not equal to null means add athose to dto and sort those results
	 * 			else, lat and lng is null means add those globalgamerulesdto
	 * 		Add those adding that globalGame rules to dtos
	 * 	else  SORT BY equal to time ,sorted at dao.. populate the dto and returning the result
	 */
	
	public ArrayList<GameRulesDto> getGameRulesForListOfLocations(List<String> locationAPIIds,Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException{
		log.info("inside getGameRulesForListOfLocations()");
		ArrayList<GameRulesDto> allGameRules=new ArrayList<GameRulesDto>();
		ArrayList<GameRules> gameRulesLocations=this.gameDao.getGameRulesForListOfLocations(locationAPIIds, isActive, checkExpiryDate,sortBy,userId);
		ArrayList<GameRules> filteredGameRules=this.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(gameRulesLocations);
		ArrayList<GameRulesDto> gameRulesLocationsDtos=new ArrayList<GameRulesDto>();
		UserLocations userLocation=null;
		if(null!=userId && userId>0){
			try{
			KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
			 userLocation=this.userDao.getUserLocationsOfUser(kikspotUser);
			}
			catch(UserNotFoundException e){
				
			}
			catch(UserLocationNotFoundException e){
				//e.printStackTrace();
			}
			
		}
		ArrayList<GameRulesDto> globalGameRulesDtos=new ArrayList<GameRulesDto>();
		if(null==sortBy || sortBy.trim().length()<1 || sortBy.equalsIgnoreCase("time")){
			for(GameRules gameRule:filteredGameRules){
				
				if(gameRule.getLatitude()==null){
					GameRulesDto globalGameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
					globalGameRulesDto.setLocationName("GLOBAL");
					globalGameRulesDtos.add(globalGameRulesDto);
				}
				else{
					GameRulesDto gameRulesDto= GameRulesDto.populateGameRulesDto(gameRule);
					try {
						if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
							RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRulesDto.getLocationId(),null,null,null);
							gameRulesDto.setDistance(this.recommendationService.distance(userLocation.getLatitude(), userLocation.getLongitude(), recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude()));
							System.out.println(" locationId "+gameRulesDto.getLocationId() +" distance "+gameRulesDto.getDistance());
							gameRulesDto.setLocationName(recommendationLocationDto.getLocationName());
							
						}
						else{
							gameRulesDto.setLocationName("GLOBAL");
						}
					} 
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					gameRulesLocationsDtos.add(gameRulesDto);
				}
			}
			
			allGameRules.addAll(globalGameRulesDtos);
			allGameRules.addAll(gameRulesLocationsDtos);
			
		}
		else{
			for(GameRules gameRule:filteredGameRules){
				if(null!=gameRule.getLatitude() && null!=gameRule.getLongitude()){
					GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
					if(null!=userLocation){
						gameRulesDto.setDistance(this.recommendationService.distance(userLocation.getLatitude(), userLocation.getLongitude(), gameRule.getLatitude(), gameRule.getLongitude()));
					}
					try {
						if(null!=gameRulesDto.getLocationId() && gameRulesDto.getLocationId().trim().length()>0){
							RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(gameRulesDto.getLocationId(),null,null,null);
							gameRulesDto.setLocationName(recommendationLocationDto.getLocationName());
						}
						else{
							gameRulesDto.setLocationName("GLOBAL");
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					gameRulesLocationsDtos.add(gameRulesDto);
				}
				else if(gameRule.getLatitude()==null){
					GameRulesDto globalGameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
					globalGameRulesDto.setLocationName("GLOBAL");
					globalGameRulesDtos.add(globalGameRulesDto);
				}
				
				
			}
			if(null!=userLocation){
				gameRulesLocationsDtos=this.sortGameRulesDtos(gameRulesLocationsDtos, sortBy);
			}
			if(globalGameRulesDtos.size()>0){
				allGameRules.addAll(globalGameRulesDtos);
			}
			allGameRules.addAll(gameRulesLocationsDtos);
			
			
		}
		
		return allGameRules;
	}
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param gameRuleId
	 * @param userId
	 * @param locationId
	 * @return
	 * @throws Exception
	 * 
	 * Method for getting the UserGame Creds By Game Rule Id
	 * Steps : 1.Getting the kikspot user by userId
	 * 		   get the game rule by gameRuleId
	 * 			Adding the creds of a game rule to the user creds
	 * 			Based on user earned creds,we update the game level of a user
	 * 		  2. Update the cred history of a user
	 * 		  3. Decreasing the game Rule Limit by 1 and update the Game rules 
	 * 		  4. populate the usergame creds dto
	 * 		  5. return the userGameCredsDto
	 * 		
	 * 			
	 */
	
	public UserGameCredsDto updateUserGameCredsByGameRuleId(Integer gameRuleId,Integer userId,String locationId) throws Exception{
		log.info("inside updateUserGameCredsByGameRuleId()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		GameRules gameRule=this.gameDao.getGameRuleByGameRuleId(gameRuleId);
		// We are checking the below conditions because we have open date and close fields as optional
		Boolean openDateRestriction=true;
		Boolean closeDateRestriction=true;
		if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
			openDateRestriction=false;
		}
		if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
			closeDateRestriction=false;
		}
		
		
		
		if(gameRule.getIsActive()==false || openDateRestriction==false){
			throw new GameRuleInActiveException();
		}
		else if(closeDateRestriction==false){
			throw new GameRuleExpiredException();
		}
		else if(null!=gameRule.getRuleLimit() && gameRule.getRuleLimit()<1){
			throw new GameRuleExceedsRuleLimitException();
		}
		else{
						
			//UPDATING THE USER GAME CREDS BASED ON EARNED CREDS
			UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
			Integer oldCreds=userGameCreds.getCreds();
			Integer earnedCreds=gameRule.getCreds()+userGameCreds.getCreds();
			userGameCreds.setCreds(earnedCreds);
			userGameCreds.setCredType(CredType.GAME);
			userGameCreds.setGameRule(gameRule);
			Game game=this.gameDao.getGameById(1);
			userGameCreds.setGame(game);
			UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCreds);
			NotificationType notificationType=this.notificationService.getNotificationTypeByType("Level Attained");
			//As per client Modfication Commented the code because we modified the levels updattion of user based on mincreds - If that user reaches the atleast min creds means the level get changed 
			
			/*	UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
			Integer oldCreds=userGameCreds.getCreds();
			Integer earnedCreds=gameRule.getCreds()+userGameCreds.getCreds();
			userGameCreds.setCreds(earnedCreds);
			userGameCreds.setCredType(CredType.GAME);
			userGameCreds.setGameRule(gameRule);
			Game game=this.gameDao.getGameById(1);
			userGameCreds.setGame(game);
			
			ArrayList<KikspotUser> users=this.userDao.getAllUsersExceptByPass();
			for(KikspotUser user:users){
				try{
					
					ArrayList<GameLevels> gameLevels=this.gameDao.getGameLevelsByCreds(userGameCreds.getCreds());
					ArrayList<GameLevels> gameLevels1=this.sortGameLevels(gameLevels);
					userGameCreds.setLevel(gameLevels1.get(0).getLevel());
					userGameCreds.setLevel(gameLevels1.get(gameLevels1.size()-1).getLevel());
					this.creadDao.saveorUpdateUserGameCred(userGameCreds);
				}
				catch(UserGameCredNotFoundException e){
					
				}
				catch(GameLevelNotFoundException e){
					try{
					
						ArrayList<GameLevels> gameLevels=this.gameDao.getAllGameLevels();
						ArrayList<Integer> creds=new ArrayList<Integer>();
						for(GameLevels gameLevel1:gameLevels){
							creds.add(gameLevel1.getMinCreds());
						}
						Integer highestCreds=Collections.max(creds);
						GameLevels highestGameLevel=this.gameDao.getGameLevelByMinimumCreds(highestCreds);
						userGameCreds.setLevel(highestGameLevel.getLevel());
						this.creadDao.saveorUpdateUserGameCred(userGameCreds);
					}
					catch(UserGameCredNotFoundException e1){
						
					}
					catch(GameLevelNotFoundException e2){
						throw new GameLevelNotFoundException();
					}
				}
				}*/
			
			try{
				ArrayList<GameLevels> gameLevels=this.gameDao.getGameLevelsByCreds(userGameCreds.getCreds());
				ArrayList<GameLevels> gameLevels1=this.sortGameLevels(gameLevels);
				/*userGameCreds.setLevel(gameLevels1.get(0).getLevel());*/
				if(!userGameCreds.getLevel().equalsIgnoreCase((gameLevels1.get(gameLevels1.size()-1).getLevel()))){
					String message="You're socially mobile and on the move!  You just achieved the level of "+gameLevels1.get(gameLevels1.size()-1).getLevel();
					this.notificationService.sendNotificationToUser( userGameCredsDto.getKikspotUserDto().getUserId(), notificationType, message, locationId);
				}
				userGameCreds.setLevel(gameLevels1.get(gameLevels1.size()-1).getLevel());
			}
			
			catch(GameLevelNotFoundException e){
				try{
					
					ArrayList<GameLevels> gameLevels=this.gameDao.getAllGameLevels();
					ArrayList<Integer> creds=new ArrayList<Integer>();
					for(GameLevels gameLevel1:gameLevels){
						creds.add(gameLevel1.getMinCreds());
					}
					Integer highestCreds=Collections.max(creds);
					GameLevels highestGameLevel=this.gameDao.getGameLevelByMinimumCreds(highestCreds);
					if(!userGameCreds.getLevel().equalsIgnoreCase((highestGameLevel.getLevel()))){
						String message="You're socially mobile and on the move!  You just achieved the level of "+highestGameLevel.getLevel();
						this.notificationService.sendNotificationToUser( userGameCredsDto.getKikspotUserDto().getUserId(), notificationType, message, locationId);
					}
					userGameCreds.setLevel(highestGameLevel.getLevel());
				}
				
				catch(GameLevelNotFoundException e2){
					throw new GameLevelNotFoundException();
				}
			}
			
			this.creadDao.saveorUpdateUserGameCred(userGameCreds);
			
			// UPDATING THE USER CRED HISTORY
			UserCredHistory userCredHistory=this.creadDao.getMostRecentUserCredHistoryofUser(kikspotUser);
			userCredHistory.setCred(earnedCreds);
			userCredHistory.setDate(new Date());
			this.creadDao.saveorUpdateUserCredHistory(userCredHistory);
			if(null!=gameRule.getRuleLimit() && gameRule.getRuleLimit()>0){
				//Decreasing the game Rule Limit and Updating the game Rule
				Integer gameRuleLimit=gameRule.getRuleLimit();
				gameRule.setRuleLimit(gameRuleLimit-1);
				this.gameDao.saveorUpdateGameRule(gameRule);
			}
			
			//UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCreds);
			//String message="Congrats! U accomplish the game rule of "+gameRule.getRule()+", and you Redeemed "+gameRule.getCreds()+" Creds .Your Cred  is Succesfully updated ";

			
			try{
				//NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.RECOMMENDATION.toString());
				//Commented the notification as per client request,they wanted single notification for all rules redeemed creds of singlr rate in single notification
				//this.notificationService.sendNotificationToUser( userGameCredsDto.getKikspotUserDto().getUserId(), notificationType, message, locationId);
				//System.out.println("successfull");
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return userGameCredsDto;
		}
	}
	
	/**
	 * Method to send push notification
	 * @param notificationMessage
	 * @param deviceToken
	 * @param service
	 */
	private void sendNotification(String notificationMessage, String deviceToken, ApnsService service) {
    try{
	  System.out.println("NOTIFICATIONS LO");
		String payload = APNS.newPayload()
	            .alertBody(notificationMessage)
	            .sound("Glass.aiff")
	            .actionKey("Play").build();
	//	String token = "737906d52584a254cd16ea8534d06f790ec5c2afbe531f49dc822fad9c9de141"; 
	//	String token = "89f894579f584f618559f0b83df60e06b62777ddb3dd4fa3e4a8524a7ed99d94";	
		String token="f92b15136aa32137a3360172c10d767d4c03b4d80ce182de36061ec5ef6c792e";
		/** Modified code for getting inactive devices from APNSservice on 11th march 2016*/
		Map<String,Date> inactiveDevices=new HashMap<String, Date>();
		 inactiveDevices=service.getInactiveDevices();
		for(String inactiveDeviceToken:inactiveDevices.keySet()){
			log.info(" inactive device Token "+ inactiveDeviceToken);
			System.out.println(" inactive device Token"+inactiveDeviceToken);
		}
		/** End of the Modified code for getting inactive devices from APNSservice on 11th march 2016*/
		service.push(deviceToken, payload);		
		System.out.println(notificationMessage+" "+deviceToken);
		log.info(" NOtification Message Sent "+ notificationMessage+" "+deviceToken);
		
		
		//service.push(token, payload);
		//System.out.println(notificationMessage+" "+token);
		//log.info(" NOtification Message Sent "+ notificationMessage+" "+token);
		
	}
	catch(Exception e){
		e.printStackTrace();
	}
		
	}

	/**
	 * Created By bHagya On Jan 11th,2016
	 * @param level
	 * @return
	 * @throws UserGameCredNotFoundException
	 * 
	 * Method for getting the users of a USer Game Creds Table By Game Level
	 */
	
	
	public ArrayList<UserGameCredsDto> getUsersGameCredsByGameLevel(String level) throws UserGameCredNotFoundException{
		log.info("inside getUsersGameCredsByGameLevel()");
		ArrayList<UserGameCreds> users=this.gameDao.getUsersGameCredsByGameLevel(level);
		ArrayList<UserGameCredsDto> usersDto=new ArrayList<UserGameCredsDto>();
		for(UserGameCreds user:users){
			UserGameCredsDto userDto=UserGameCredsDto.populateUserGameCredsDto(user);
			usersDto.add(userDto);
		}
		return usersDto;
	}
	/**
	 * Created By Bhagya On Jan 12th,2016
	 * @param gameRulesDtos
	 * @param sortBy
	 * @return
	 * 
	 * Implementing sortby function for sortGameRules Dtos
	 */
	
	private ArrayList<GameRulesDto> sortGameRulesDtos(ArrayList<GameRulesDto> gameRulesDtos,String sortBy){
		log.info("inside sortGameRulesDtos");
		if(null==sortBy || sortBy.trim().length()<1 || sortBy.equalsIgnoreCase("close")){
			Collections.sort(gameRulesDtos, new Comparator<GameRulesDto>() {
				@Override
				public int compare(GameRulesDto o1,
						GameRulesDto o2) {
						return o1.getDistance().compareTo(o2.getDistance());
					
				}
				
			});
		}
		return gameRulesDtos;
	}

	/**
	 * created by firdous on 8th march
	 * 
	 * method to get the game details like creds ,position of all the users
	 * 
	 */
	public ArrayList<UserGameCredsDto> getAllUsersCredsBasedOnLocation(String time) throws Exception {
		log.info("inside  getAllUsersCreds()");
		ArrayList<UserGameCredsDto> userGameCredsDtos=new ArrayList<UserGameCredsDto>();
		ArrayList<UserGameCreds> userGameCreds=this.creadDao.getUserGameCredsOfAllUser();
		for(UserGameCreds userGameCred:userGameCreds){
			UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
			 userGameCredsDtos.add(userGameCredsDto);
	    }
	  return userGameCredsDtos;
		
	}

	/**
	 * created by firdous on 8th march
	 * method to get the game details like creds ,position of all the friends of user
	 * 
	 */
	public ArrayList<UserGameCredsDto> getUsersCredsOfUsersFriends(Integer userId) throws Exception {
		log.info("inside getUsersCredsOfUsersFriends()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		ArrayList<KikspotUser> referredUsers = this.userDao.getReferredUsersByKikspotUser(kikspotUser);
		ArrayList<UserGameCredsDto> userGameCredsDtos=new ArrayList<UserGameCredsDto>();
		ArrayList<UserGameCreds> userGameCreds=this.creadDao.getGameCredsofUsersInOrder(referredUsers);
		for(UserGameCreds userGameCred:userGameCreds){
			UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
			userGameCredsDtos.add(userGameCredsDto);
	    }
	  return userGameCredsDtos;
		
	}


  /**
   * Added by Firdous on 8th march 2016
   * Method to get the users game cred details by accepting user id
   * 
   */
	public UserGameCredsDto getUsersGameCredsByUserId(Integer userId) throws Exception {
		log.info("inside  getUsersGameCredsByUserId()");
		KikspotUser user=this.userDao.getKikspotUserById(userId);
		UserGameCreds userGameCred=this.creadDao.getUserGameCredsByUser(user);
		UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
		return userGameCredsDto;
	}

/**
 * Added by Firdous on 9th march 2016
 * Method to get the creds that are required for a user to enter in the next level
 * 
*/
@Override
public Integer getPointsNeededForNextLevel(UserGameCredsDto gameCredDto) throws Exception {
	log.info("inside getPointsNeededForNextLevel()");
	Integer value=0;
	String level=gameCredDto.getLevel();
	Integer value1=gameCredDto.getCreds();
	GameLevels gameLevel1=this.gameDao.getGameLevelByLevel(level);
	Integer value2=gameLevel1.getMinCreds();
    value=value2-value1;
	return value;
}

/**
 * 
 * Added by Firdous on 11th march 2016
 * Method to get all the users game creds  details in current week 
 * 
 */

@Override
public ArrayList<UserGameCredsDto> getAllUsersCredsInCurrentweek() throws UserNotFoundException,UserCredHistoryNotFoundException,UserGameCredNotFoundException {
	log.info("inside  getAllUsersCreds()");
	ArrayList<UserGameCredsDto> userGameCredsDtos=new ArrayList<UserGameCredsDto>();
	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.HOUR_OF_DAY, 0); 
	cal.clear(Calendar.MINUTE);
	cal.clear(Calendar.SECOND);
	cal.clear(Calendar.MILLISECOND);
	cal.set(Calendar.DAY_OF_WEEK,cal.getFirstDayOfWeek());
    ArrayList<UserCredHistory> usersCredHistory=this.creadDao.getCredsOfUsersInCurrentWeek(cal.getTime());
    ArrayList<KikspotUser> kikspotUsers=new ArrayList<KikspotUser>();
    for(UserCredHistory userCredHistory:usersCredHistory){
    	 kikspotUsers.add(userCredHistory.getKikspotUser());
    }
	ArrayList<UserGameCreds> userGameCreds=this.creadDao.getUserGameCredsOfAllUsers(kikspotUsers);
	for(UserGameCreds userGameCred:userGameCreds){
		UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
		 userGameCredsDtos.add(userGameCredsDto);
    }
    return userGameCredsDtos;
}

/**
 * 
 * Added By Firdous on 3rd march 2016
 * Method to get the all users game cred details
 * @throws Exception 
 * 
 */

@Override
public ArrayList<UserGameCredsDto> getGameCredDetailsOfAllUsers() throws UserGameCredNotFoundException{
	log.info("inside getGameCredDetailsOfAllUsers()");
	 ArrayList<UserGameCredsDto> userGameCredsDtos=new ArrayList<UserGameCredsDto>();
	 ArrayList<UserGameCreds> userGameCreds=this.creadDao.getUserGameCredsOfAllUser();
	 for(UserGameCreds userGameCred:userGameCreds){
		  UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
		  userGameCredsDtos.add(userGameCredsDto);
     }
	 
     return userGameCredsDtos;
   }


/**
 * 
 * Added by Firdous on 11th march 2016
 * Method to get the game cred details of users who are friend of a requested user in the time span of current week
 * 
 */
@Override
public ArrayList<UserGameCredsDto> getUserCredsOfFriendsInPresentWeek(Integer userId) throws  ReferredUsersNotFoundException,UserNotFoundException,UserCredHistoryNotFoundException,UserGameCredNotFoundException{
	log.info("inside  getUserCredsOfFriendsInPresentWeek()");
	KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
	ArrayList<UserGameCredsDto> userGameCredsDtos=new ArrayList<UserGameCredsDto>();
	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.HOUR_OF_DAY, 0); 
	cal.clear(Calendar.MINUTE);
	cal.clear(Calendar.SECOND);
	cal.clear(Calendar.MILLISECOND);
	cal.set(Calendar.DAY_OF_WEEK,cal.getFirstDayOfWeek());
	ArrayList<KikspotUser> referredUsers = this.userDao
			.getReferredUsersByKikspotUser(kikspotUser);
    ArrayList<UserCredHistory> usersCredHistory=this.creadDao.getCredsOfUsersInCurrentWeekOfFriends(cal.getTime(),referredUsers);
    ArrayList<KikspotUser> kikspotUsers=new ArrayList<KikspotUser>();
    for(UserCredHistory userCredHistory:usersCredHistory){
    	 kikspotUsers.add(userCredHistory.getKikspotUser());
    }
	ArrayList<UserGameCreds> userGameCreds=this.creadDao.getUserGameCredsOfAllUsers(kikspotUsers);
	for(UserGameCreds userGameCred:userGameCreds){
		UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
		 userGameCredsDtos.add(userGameCredsDto);
    }
  return userGameCredsDtos;
}



/**
 * ADDED BY Firdous on 17th march 2016
 * Method to get all the games
 */
public ArrayList<GameDto> getAllGames() throws GamesNotFoundException {
	log.info("inside getAllGames");
	ArrayList<Game> games=this.gameDao.getAllGames();
	ArrayList<GameDto> gameDtos=new ArrayList<GameDto>();
	for(Game game:games){
		GameDto gameDto=GameDto.populateGameDto(game);
		gameDtos.add(gameDto);
	}
	return gameDtos;
}

/**
 * Added by Firdous on 17th march 2016
 * Method to get game rules by game Id
 * 
 */

@Override
public ArrayList<GameRulesDto> getGameRulesByGameId(Integer gameId) throws GameRuleNotFoundException {
	log.info("inside getGameRulesByGameId()");
	ArrayList<GameRules> gamerules=this.gameDao.getGameRulesByGameId(gameId);
	ArrayList<GameRules> filteredRules=this.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(gamerules);
	ArrayList<GameRulesDto> gameRuleDtos=new ArrayList<GameRulesDto>();
	for(GameRules gamerule:filteredRules){
		GameRulesDto gameRuleDto=GameRulesDto.populateGameRulesDto(gamerule);
		gameRuleDtos.add(gameRuleDto);
	}
	return gameRuleDtos;
}


/**
 * Added by Firdous on 17th march 2016
 * Method to get creds required to complete the level
 * 
 */
public Integer getPointsNeededForNextLevelToReach(GameLevelsDto gameLevelDto,UserGameCredsDto userGameCredDto) throws Exception {
	log.info("inside getPointsNeededForNextLevelToReach()");
	Integer value=0;
    Integer value1=gameLevelDto.getMinCreds();//Creds needed to complete the level
	/*Integer value2=userGameCredDto.getCreds();//get current creds of user
	if(value1<value2){
		   return value;
	}
	else{
	   value=value1-value2;
	   return value;
	}*/
    Integer value2=userGameCredDto.getCreds();//get current creds of user
	if(value1<value2){
		   return value;
	}
	else{
	   value=value1-value2;
	   return value;
	}
	
}


/**
 * Added by Firdous
 * Method to get game level by game name
 * 
 */
@Override
public GameLevelsDto getGameLevelByLevelName(String level) throws Exception {
	log.info("inside getGameLevelByLevelName()");
	GameLevels gameLevel=this.gameDao.getGameLevelByLevel(level);
	GameLevelsDto gameLevelDto=GameLevelsDto.populateGameLevelDtos(gameLevel);
	return gameLevelDto;
}


/**
 * Added by Firdous
 * Method to get game rule types
 * 
 */
@Override
public ArrayList<GameRuleTypeDto> getGameRuleTypes() throws Exception {
	log.info("inside getGameRuleTypes()");
	ArrayList<GameRuleTypes> gameRuleTypes=this.userDao.getGameRuleTypes();
	ArrayList<GameRuleTypeDto> gameRuleTypeDtos=new ArrayList<GameRuleTypeDto>();
	for(GameRuleTypes gameRuleType:gameRuleTypes){
		GameRuleTypeDto gameRuleTypeDto=GameRuleTypeDto.populateGameRuleTypeDto(gameRuleType);
		gameRuleTypeDtos.add(gameRuleTypeDto);
	}
	return gameRuleTypeDtos;
 }


/**
 * Added by Firdous
 * Method to get game rule by game rule type
 * 
 */
@Override
public ArrayList<GameRules> getGameRuleByType(String ruleType) throws Exception {
	log.info("inside  getGameRuleByType()");
	ArrayList<GameRules> gameRules=this.gameDao.gameRuleByRuleType(ruleType);
	ArrayList<GameRules> filteredGameRules=this.getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(gameRules);
	return filteredGameRules;
}


/**
 * Method to get the game rule by activity type
 */
@Override
public GameRules getGameRuleByActivityType(String gameRuleType) throws Exception {
	log.info("inside  getGameRuleByActivityType()");
	GameRules gameRule=this.getFilteredRuleByCheckingOpenDateAndCloseDateAndRuleLimit(this.gameDao.getGameRuleByActivityType(gameRuleType));
	return gameRule;
}

/**
 * Added by Firdous
 * Method to change the game rule visiblity
 */
@Override
public Integer savingchangedGameRuleVisibility(Integer gameRuleId, Boolean visible) throws Exception {
	log.info("inside savingchangedGameRuleVisibility()");
	GameRules gameRule=this.gameDao.getGameRuleByRuleId(gameRuleId);
	gameRule.setVisible(visible);
	Integer updatedResult=	this.gameDao.saveorUpdateGameRule(gameRule);
	return updatedResult;
}


/**
 * Created By Bhagya On April 26th, 2016
 * @param userId
 * @param type
 * @return
 * @throws Exception
 * 
 * Method for to update the game creds for  Sharing Functionality rating
 */

	public Map<String,String> updateGameCredsForShareFunctions(Integer userId,String type) throws Exception{
		log.info("inside updateGameCredsForShareFunctions()");
		ArrayList<GameRules> gameRules=this.gameDao.gameRuleByRuleType(type);
		type=type.substring(6);
	
		if(gameRules.size()>0){
			
			Map<String,String> resultMap=new HashMap<String, String>();
			UserGameCredsDto userGameCredsDto=new UserGameCredsDto();
			Integer earnedCreds=0;
			for(GameRules gameRule:gameRules){
				if(null!=gameRule.getUserId()){
				
					if(gameRule.getUserId()==userId){
						 userGameCredsDto=this.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(), userId, null);
						 earnedCreds=earnedCreds+gameRule.getCreds();
						 try{
							 //NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.RATING.toString());
							// String message="Congrats! U accomplish the game rule of "+userGameCredsDto.getGameRule().getRule()+", and you Redeemed "+userGameCredsDto.getGameRule().getCreds()+" Creds .Your Cred  is Succesfully updated ";
							 NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.Shared.toString());
							 String message="Lets get the spotter community connected… You just shared "+type;
							 this.notificationService.sendNotificationToUser( userId, notificationType, message, null);
							 
							 NotificationType credEarnedNotificationType=this.notificationService.getNotificationTypeByType("Cred Earned");
							 String credEarnedMessage="Let it rain! You have just earned "+userGameCredsDto.getGameRule().getCreds()+" creds by redeeming the following rewards: "+userGameCredsDto.getGameRule().getRule();
							 this.notificationService.sendNotificationToUser( userId, credEarnedNotificationType, credEarnedMessage, null);
							 }
							 catch(Exception e){
								 
							 }
					}
				}
				else{
					
					 userGameCredsDto=this.updateUserGameCredsByGameRuleId(gameRule.getGameRuleId(), userId, null);
					earnedCreds=earnedCreds+gameRule.getCreds();
					try{
						 //NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.RATING.toString());
						 //String message="Congrats! U accomplish the game rule of "+userGameCredsDto.getGameRule().getRule()+", and you Redeemed "+userGameCredsDto.getGameRule().getCreds()+" Creds .Your Cred  is Succesfully updated ";
						
						
						NotificationType notificationType=this.notificationService.getNotificationTypeByType(NotificationTypes.Shared.toString());
						 String message="Lets get the spotter community connected… You just shared "+type;
						 this.notificationService.sendNotificationToUser( userId, notificationType, message, null);
						
						 NotificationType credEarnedNotificationType=this.notificationService.getNotificationTypeByType("Cred Earned");
						 String credEarnedMessage="Let it rain! You have just earned "+userGameCredsDto.getGameRule().getCreds()+" creds by redeeming the following rewards: "+userGameCredsDto.getGameRule().getRule();
						 this.notificationService.sendNotificationToUser( userId, credEarnedNotificationType, credEarnedMessage, null); 
						 
						 }
						 catch(Exception e){
							 e.printStackTrace();
						 }
				}
				
				
			}
			resultMap.put("earnedCreds", earnedCreds.toString());
			resultMap.put("gameLevel", userGameCredsDto.getLevel());
			return resultMap;
		}
		else{
			throw new GameRuleNotFoundException();
		}
		
	}
	
	/**
	 * Created By Bhagya On May 05th, 2016
	 * @param gameRules
	 * @return
	 * 
	 * Support Method for to filter the game rules by verifying the restrictions of open date ,expiry date and rule limit
	 */
	
	public ArrayList<GameRules> getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(ArrayList<GameRules> gameRules){
		ArrayList<GameRules> filteredGameRules=new ArrayList<GameRules>();
		for(GameRules gameRule:gameRules){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			Boolean ruleLimitRestriction=true;
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(null!=gameRule.getRuleLimit() && gameRule.getRuleLimit()<1){
				ruleLimitRestriction=false;
			}
			
			if(openDateRestriction==true && closeDateRestriction==true && ruleLimitRestriction==true){
				filteredGameRules.add(gameRule);
			}
		}
		
		return filteredGameRules;
	}
	
	/**
	 * Created By Bhagya On May 05th, 2016
	 * @param gameRule
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * Support method for filter the game rule  by verifying the conditions of opendate, close date and rule limit
	 */
	public GameRules getFilteredRuleByCheckingOpenDateAndCloseDateAndRuleLimit(GameRules gameRule) throws GameRuleNotFoundException{
	
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			Boolean ruleLimitRestriction=true;
			if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(null!=gameRule.getRuleLimit() && gameRule.getRuleLimit()<1){
				ruleLimitRestriction=false;
			}
			
			if(openDateRestriction==true && closeDateRestriction==true && ruleLimitRestriction==true){
				return gameRule;
			}
			else{
				throw new GameRuleNotFoundException();
			}
		
		
	}
	
	
	/**
	 * Created by Firdous on May 25th,2016
	 * @throws Exception
	 * Method to update the game level of users whenever an existing level's creds changes or when new level 
	 * is added and when an existing level is deleted
	 * 
	 */
	private void updateUsersLevelsBasedOnTheirCreds() throws Exception {
		log.info("inside updateUsersLevelsBasedOnTheirCreds()");
		ArrayList<KikspotUser> users=this.userDao.getAllUsersExceptByPass();
		for(KikspotUser user:users){
			try{
				UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(user);
				ArrayList<GameLevels> gameLevels=this.gameDao.getGameLevelsByCreds(userGameCreds.getCreds());
				ArrayList<GameLevels> gameLevels1=this.sortGameLevels(gameLevels);
				/*userGameCreds.setLevel(gameLevels1.get(0).getLevel());*/
				userGameCreds.setLevel(gameLevels1.get(gameLevels1.size()-1).getLevel());
				this.creadDao.saveorUpdateUserGameCred(userGameCreds);
			}
			catch(UserGameCredNotFoundException e){
				
			}
			catch(GameLevelNotFoundException e){
				try{
					UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(user);
					ArrayList<GameLevels> gameLevels=this.gameDao.getAllGameLevels();
					ArrayList<Integer> creds=new ArrayList<Integer>();
					for(GameLevels gameLevel1:gameLevels){
						creds.add(gameLevel1.getMinCreds());
					}
					Integer highestCreds=Collections.max(creds);
					GameLevels highestGameLevel=this.gameDao.getGameLevelByMinimumCreds(highestCreds);
					userGameCreds.setLevel(highestGameLevel.getLevel());
					this.creadDao.saveorUpdateUserGameCred(userGameCreds);
				}
				catch(UserGameCredNotFoundException e1){
					
				}
				catch(GameLevelNotFoundException e2){
					throw new GameLevelNotFoundException();
				}
			}
			}
		
	}
	
	
	/**
	 * Added by Firdous on 25th May 2016
	 * @param gameLevels
	 * @return
	 * Method to sort game levels based on minimum creds
	 * 
	 */
	private ArrayList<GameLevels> sortGameLevels(ArrayList<GameLevels> gameLevels){
		log.info("inside sortGameLevels");
		
			Collections.sort(gameLevels, new Comparator<GameLevels>() {
				@Override
				public int compare(GameLevels o1,
						GameLevels o2) {
						return o1.getMinCreds().compareTo(o2.getMinCreds());
				}
				
			});
		
		return gameLevels;
	}
	
	
	/**
	 * Added by Firdous 
	 * Method to get the game level by accepting level name
	 */
	@Override
	public GameLevelsDto getNextGameLevelByLevelName(String level) throws Exception {
		int x=0;
		GameLevels gameLevel=this.gameDao.getGameLevelByLevel(level);
		ArrayList<GameLevels> gameLevels=this.gameDao.getAllGameLevels();
		ArrayList<GameLevels> sortedGameLevels=this.sortGameLevels(gameLevels);
		for(int i=0;i<sortedGameLevels.size();i++){
			if(sortedGameLevels.get(i).getMinCreds()>gameLevel.getMinCreds()){
				return GameLevelsDto.populateGameLevelDtos(sortedGameLevels.get(i));
			}
			x=i;
		}
		return GameLevelsDto.populateGameLevelDtos(sortedGameLevels.get(x));
	}
    
	
	/**
	 * Added by Firdous
	 * Method to get all the active  global rules of a game
	 */
	@Override
	public ArrayList<GameRulesDto> getGlobalGameRulesByGameId(Integer gameId,Integer userId) throws Exception {
		ArrayList<GameRules> globalGameRules=this.gameDao.getGlobalGameRulesOfGame(gameId,userId);
		ArrayList<GameRulesDto> gameRuleDtos=new ArrayList<GameRulesDto>();
		for(GameRules gameRule:globalGameRules){
				Boolean openDateRestriction=true;
				Boolean closeDateRestriction=true;
				if(null!=gameRule.getOpenDate() && gameRule.getOpenDate().after(new Date())){
					openDateRestriction=false;
				}
				if(null!=gameRule.getExpiryDate() && gameRule.getExpiryDate().before(new Date())){
					closeDateRestriction=false;
				}
				
				if(openDateRestriction==false || closeDateRestriction==false){
					gameRule.setIsActive(false);
				}
				this.gameDao.saveorUpdateGameRule(gameRule);
				if(gameRule.getIsActive()==true){
					GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
					gameRuleDtos.add(gameRulesDto);
				}
				
		}
		
		/** applying the sorting order 
	    1) closest expiry date to the current date/time
		2) by highest cred amount
		3) by closest proximity to the user location
		4) Expiry Date is NULL
	 * */
	 
		Collections.sort(gameRuleDtos, new Comparator<GameRulesDto>() {
			@Override
			public int compare(GameRulesDto o1,
					GameRulesDto o2) {
				
				 	// adding multiple fields comparison based on client requirement (rating and distance comparison)
				// below if and else condition for sorting the null expairy date as Last
				if( o1.getExpiryDate() == null && o2.getExpiryDate() == null){
					return 0;
				}
				if (o1.getExpiryDate() == null) {
			        return 1;
			    }
			    if (o2.getExpiryDate() == null) {
			        return -1;
			    }
				    return new CompareToBuilder()
				  .append(o1.getExpiryDate(), o2.getExpiryDate())
				  .append(Math.round(o1.getCreds()), Math.round(o2.getCreds()))
				  .append(o1.getDistance(), o2.getDistance()).toComparison();
				  
	               
	    }	
		});
		
		
		return gameRuleDtos;
	}

	/**
	 * Gte the Location name based on Id
	 */
	
	public String getLocationName(String kikspotlocationId) throws Exception{
		//log.info("inside getLocationName");
		String locationName="";
		try{
			Integer locationId=Integer.parseInt(kikspotlocationId);
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
			//KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByRecommendationLocationId(gameRulesDto.getLocationId());
			locationName=kikspotRecommendationLocation.getLocationName();
			
		}
		catch(NumberFormatException e){
			//gameRule.setLocationName(null);
			try{
				
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByKikspotLocationId(kikspotlocationId);
			locationName=kikspotRecommendationLocation.getLocationName();
		
			}
			catch(KikspotRecommendationLocationNotFoundException e1){
				
				//gameRule.setLocationName(null);
				if(null!=kikspotlocationId && kikspotlocationId.trim().length()>0){
					try{
						
					PlaceDto placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(kikspotlocationId);
					locationName=placeDto.getPlaceName();
					}
					catch(InvalidRequestException e2){
						e.printStackTrace();
						System.out.println(" INVALID API REQUEST ,Requested Location Id not valid one  " +kikspotlocationId );
					}
				}
			}
		
			
		}
		
		return locationName;
	}
	
	/**
	 * Created By Bhagya on april 01st, 2019
	 * 
	 * Method for to get the distinct user game creds of all users
	 * 
	 */
	
	@Override
	public ArrayList<Integer> getDistinctGameCredDetailsOfAllUsers() throws UserGameCredNotFoundException{
		log.info("inside getDistinctGameCredDetailsOfAllUsers()");
		
		 ArrayList<Integer> userGameCreds=this.creadDao.getDistinctUserGameCredsOfAllUser();
		 
	     return userGameCreds;
	   }

}
