package com.kikspot.frontend.game.service;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.InvalidRequestException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.dao.CredDao;
import com.kikspot.backend.game.dao.GameDao;
import com.kikspot.backend.game.model.CredRules;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.GameLevels;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.game.model.UpdatedCredsInSixMonths;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.recommendation.dao.RecommendationDao;
import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.game.dto.CredRulesDto;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;
import com.kikspot.frontend.game.dto.UserCredHistoryDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.recommendation.dto.PlaceDto;
import com.kikspot.frontend.recommendation.helper.googlePlacesAPI.GooglePlacesAPIProvider;
import com.kikspot.frontend.user.dto.KikspotUserDto;
/**
 * 
 * Created by Jeevan on Novemeber 26, 2015
 * 
 * Service for CredService..
 * 
 * @author KNS-ACCONTS
 *
 */

@Service("credService")
public class CredServiceImpl implements CredService {
	
	@Resource(name="credDao")
	private CredDao creadDao;

	@Resource(name = "userDao")
	private UserDao userDao;
	
	@Resource(name="gameDao")
	private GameDao gameDao;
	
	/** Added Recommendation Dao Resource By Bhagya ON Jan 04th,2016*/
	@Resource(name="recommendationDao")
	private RecommendationDao recommendationDao;
	
	@Resource(name="googlePlacesAPIProvider")
	private GooglePlacesAPIProvider googlePlacesAPIProvider;
	
	private static Logger log=Logger.getLogger(CredServiceImpl.class);
	
	
	/**
	 * 
	 * Created by Jeevan on Novemver 26,2015
	 * 
	 * Method to addCredstooUsersAfter User Login...
	 * 
	 * 
	 * @param kikspotUser
	 * @throws Exception
	 * 
	 *  Steps :
	 *  
	 *  1. Saving Creds of Referral.
	 *             i. Get Cred Rule of Refferral
	 *             ii. Get USer Cred Currently
	 *                  User Cred+=Cred Rule Cred
	 *             iii. Update User Cred, User Cred History
	 *             
	 *  2.  For Sign up user
	 *                           i. Get Cred Rule of Sign up
	 *                           ii. Save New User Cred where cred=cred rule cred
	 *                           iii. Update Cred History..
	 * 
	 * 
	 * 
	 */
	
	public void addCredPostUserSignup(KikspotUser kikspotUser)throws Exception{
		log.info("inside addCredPostUserSignup");
		// These code was commented by bhagya ,because we have implemented the referral code option by game rule condition as "Sharee Account Creation"
		/*if(null!=kikspotUser.getReferredUser()){
			CredRulesDto credRulesDto=this.getCredRuleBasedonTypeCredType("REFERRAL");
			this.saveorUpdateCredsToUser(kikspotUser.getReferredUser(), true, credRulesDto.getCreds());
		}*/
		CredRulesDto credRulesDto=this.getCredRuleBasedonTypeCredType("SIGNUP");
		this.saveorUpdateCredsToUser(kikspotUser, false, credRulesDto.getCreds());
	}
	
	
	/**
	 * Created by Jeean on November 26, 2015 
	 * 
	 * 
	 * @param credRule
	 * @return
	 * @throws Exception
	 */
	public CredRulesDto getCredRuleBasedonTypeCredType (String credTypeValue)throws Exception{
		log.info("inside getCredRuleBasedonTypeCredType() ");
		CredType credType=CredType.valueOf(credTypeValue);
		CredRules credRules=this.creadDao.getCredRulesByCredType(credType);
		CredRulesDto credRulesDto=CredRulesDto.populateCredRuleDto(credRules);
		return credRulesDto;
	}
	
	
	
	/**
	 * 
	 * @param kikspotUser
	 * @param isReferral
	 * @param earnedCred
	 * @throws Exception
	 */
	public void saveorUpdateCredsToUser(KikspotUser kikspotUser,Boolean isReferral,Integer earnedCred)throws Exception{
		log.info("inside saveorUpdateCredsToUser()  ");		
		Integer userCred=0;
		UserGameCreds userGameCreds;
		if(isReferral){
			try{
				userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
				userCred=userGameCreds.getCreds();
			}
			catch(UserGameCredNotFoundException e){
				userGameCreds=new UserGameCreds();
			}
			userGameCreds.setCredType(CredType.REFERRAL);
			try{
				GameLevels gameLevel=this.gameDao.getGameLevelByMinimumCreds(userCred+earnedCred);
				userGameCreds.setLevel(gameLevel.getLevel());
			}
			catch(GameLevelNotFoundException e){
				
			}
		}
		else{
			userGameCreds=new UserGameCreds();
			GameLevels gameLevel=this.gameDao.getGameLevelByMinimumCreds(userCred+earnedCred);
			userGameCreds.setLevel(gameLevel.getLevel());
			userGameCreds.setCredType(CredType.SIGNUP);
			/*userGameCreds.setLevel("BASIC");*/
		}		
		userGameCreds.setKikspotUser(kikspotUser);
		
		userGameCreds.setCreds(userCred+earnedCred);
		this.creadDao.saveorUpdateUserGameCred(userGameCreds);
		UserCredHistory userCredHistory=new UserCredHistory();
		userCredHistory.setKikspotUser(kikspotUser);
		userCredHistory.setCred(userGameCreds.getCreds());
		userCredHistory.setDate(new Date());
		this.creadDao.saveorUpdateUserCredHistory(userCredHistory);
	}
	
	
	
	
	/**
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserGameCredNotFoundException
	 * 
	 *  Created by Jeevan on November 27, 2015
	 *  Get UserGameCredsForUser...
	 *  
	 * 
	 *  
	 */
	public UserGameCredsDto getUserGameCredsofUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException{
		log.info("inside getUserGameCredsofUser() ");
		UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
		UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCreds);
		return userGameCredsDto;
	}
	
	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on Nov 27, 2015
	 * Methiod to Get Tip Five User Game Creds...
	 * 
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	public ArrayList<UserGameCredsDto> getTopFiveUserGameCredDtos()throws UserGameCredNotFoundException{
		log.info("inside getTopFiveUserGameCredDtos() ");
		ArrayList<UserGameCreds> userGameCreds=this.creadDao.getTopFiveUserGameCreds();
		ArrayList<UserGameCredsDto> userGameCredDtos=new ArrayList<UserGameCredsDto>();
		for(UserGameCreds userGameCred : userGameCreds){
			UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
			userGameCredDtos.add(userGameCredsDto);
		}
		return userGameCredDtos;
	}
	
	
	
	
	/**
	 * 
	 *  
	 *  Created by Jeevan on November 27, 2015
	 *  
	 *  Method to getMostRecentCredHistoryOfUser..
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserCredHistoryNotFoundException
	 */
	public UserCredHistoryDto getMostRecentCredHistoryOfUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException{
		log.info("inside getMostRecentCredHistoryOfUser() ");
		UserCredHistory userCredHistory=this.creadDao.getMostRecentUserCredHistoryofUser(kikspotUser);
		UserCredHistoryDto userCredHistoryDto=UserCredHistoryDto.populateUserCredHistoryDto(userCredHistory);
		return userCredHistoryDto;
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 30, 2015
	 * 
	 * MEthod to updateUserCreds..
	 * 
	 * @param kikspotUser
	 * @param creds
	 * @return
	 * @throws Exception
	 */
	public Integer updateUserCreds(KikspotUser kikspotUser,Integer creds)throws Exception{
		log.info("inside updateUserCreds()");
		UserGameCreds userGameCred=this.creadDao.getUserGameCredsByUser(kikspotUser);
		userGameCred.setCreds(creds);
		GameLevels gameLevel=this.gameDao.getGameLevelByMinimumCreds(creds);
		userGameCred.setLevel(gameLevel.getLevel());
		
		Integer savedResult=this.creadDao.saveorUpdateUserGameCred(userGameCred);
		return savedResult;
	}
	/**
	 * Created By bhagya On January 20th,2016
	 * @param kikspotRewardsDto
	 * @return
	 * @throws Exception
	 * 
	 * Method for Save Or Update The Kikspot Rewards
	 */
	
	public Integer saveOrUpdateKikspotRewards(KikspotRewardsDto kikspotRewardsDto) throws Exception{
		log.info("inside saveOrUpdateKikspotRewards()");
		KikspotRewards kikspotReward;
		if(null!=kikspotRewardsDto.getRewardId() && kikspotRewardsDto.getRewardId()>0){
			 kikspotReward=this.creadDao.getKikspotRewardByRewardId(kikspotRewardsDto.getRewardId());
		}
		else{
			 kikspotReward=new KikspotRewards();
			 kikspotReward.setIsActive(true);
		}
		
		kikspotReward.setRewardType(kikspotRewardsDto.getRewardType());
		kikspotReward.setReward(kikspotRewardsDto.getReward());
		kikspotReward.setCreds(kikspotRewardsDto.getCreds());
		kikspotReward.setLevel(kikspotRewardsDto.getLevel());
		kikspotReward.setStartDate(kikspotRewardsDto.getStartDate());
		kikspotReward.setEndDate(kikspotRewardsDto.getEndDate());
		kikspotReward.setRewardValue(kikspotRewardsDto.getRewardValue());
		
		if(null!=kikspotRewardsDto.getLocationId() && kikspotRewardsDto.getLocationId().trim().length()>0){
			kikspotReward.setLocationId(kikspotRewardsDto.getLocationId());
		}
		try{
			Integer locationId=Integer.parseInt(kikspotRewardsDto.getLocationId());
			KikspotRecommendationLocation kikspotRecommendationLocation=this.recommendationDao.getKikspotRecommendationLocationByLocationId(locationId);
			kikspotReward.setLocationName(kikspotRecommendationLocation.getLocationName());
		
	}
	catch(NumberFormatException e){
		kikspotReward.setLocationName(null);
		if(null!=kikspotRewardsDto.getLocationId() && kikspotRewardsDto.getLocationId().trim().length()>0){
			try{
			PlaceDto placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(kikspotRewardsDto.getLocationId());
			kikspotReward.setLocationName(placeDto.getPlaceName());
			}
			catch(InvalidRequestException e1){
				e.printStackTrace();
				System.out.println(" INVALID API REQUEST ,Requested Location Id not valid one  " +kikspotRewardsDto.getLocationId() );
			}
		}
	}
		Integer result=this.creadDao.saveOrUpdateKikspotRewards(kikspotReward);
		return result;
	}
	
	/**
	 * Created By bhagya On January 20th,2016
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @param ascending
	 * @return
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Method For Getting the Kikspot Rewards
	 */
	public ArrayList<KikspotRewardsDto> getAllKikspotRewards(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRewardNotFoundException{
		log.info("inside getAllKikspotRewards()");
		ArrayList<KikspotRewardsDto> kikspotRewardsDtos=new ArrayList<KikspotRewardsDto>();
		ArrayList<KikspotRewards> kikspotRewards=this.creadDao.getAllKikspotRewards(pageNo, pageSize, sortBy, searchBy, ascending);
		for(KikspotRewards kikspotReward: kikspotRewards){
			Boolean openDateRestriction=true;
			Boolean closeDateRestriction=true;
			
			if(null!=kikspotReward.getStartDate() && kikspotReward.getStartDate().after(new Date())){
				openDateRestriction=false;
			}
			if(null!=kikspotReward.getEndDate() && kikspotReward.getEndDate().before(new Date())){
				closeDateRestriction=false;
			}
			if(openDateRestriction==false || closeDateRestriction==false){
				kikspotReward.setIsActive(false);
			}
			else{
				kikspotReward.setIsActive(true);
			}
			try {
				this.creadDao.saveOrUpdateKikspotRewards(kikspotReward);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
			kikspotRewardsDtos.add(kikspotRewardsDto);
		}
		
		return kikspotRewardsDtos;
	}
	/**
	 * Created By Bhagya On January 20th,2016
	 * @param rewardId
	 * @return
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Method For Getting the Kikspot Reward By RewardId
	 */
	public KikspotRewardsDto getKikspotRewardByRewardId(Integer rewardId) throws KikspotRewardNotFoundException{
		log.info("inside getKikspotRewardByRewardId()");
		KikspotRewards kikspotReward=this.creadDao.getKikspotRewardByRewardId(rewardId);
		KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
		return kikspotRewardsDto;
	}
	/**
	 * Created By Bhagya On January 20th,2016
	 * @param rewardId
	 * @param isActive
	 * @return
	 * @throws Exception
	 * 
	 * Method For Updating the Kikspot Reward Status
	 */
	
	public Integer updateKikspotRewardStatus(Integer rewardId,Boolean isActive) throws Exception{
		log.info("inside updateKikspotRewardStatus()");
		KikspotRewards kikspotReward=this.creadDao.getKikspotRewardByRewardId(rewardId);
		kikspotReward.setIsActive(isActive);
		Integer result=this.creadDao.saveOrUpdateKikspotRewards(kikspotReward);
		return result;
	}
	/**
	 * Created By bhagya On January 21st,2016
	 * @param rewardId
	 * @return
	 * @throws Exception
	 * 
	 * Method For Deleting Kikspot Reward By Reward Id
	 */
	
	public Integer deleteKikspotRewardByRewardId(Integer rewardId) throws Exception{
		KikspotRewards kikspotReward=this.creadDao.getKikspotRewardByRewardId(rewardId);
		Integer deletedResult=this.creadDao.deleteKikspotReward(kikspotReward);
		return deletedResult;
	}
	/**
	 * Created By Bhagya On January 22nd,2016
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserGameCredNotFoundException
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Method For Getting the Kikspot Rewrads Of a User By the User Level and Creds
	 * 
	 * 	Steps : 1. Get Kikspot User By UserId
	 * 			2. Get UserGameCreds By KikspotUser
	 * 			3. Get Kikspot Rewards By User Level
	 * 			4. For each reward we are checking the following conditions
	 * 				a. If the reward startdate is before the current date
	 * 						if the reward has endDate means, we check that enddate is after the current date
	 * 							if the reward has creds means ,we check that usercreds is greater than or equal to reward creds
	 * 								Add those reward to Dtos
	 * 							else ,we are not adding those reward to Dtos
	* 						else (Because if the reward has no end date means it will never end)	
	* 							if the reward has creds means ,we check that usercreds is greater than or equal to reward creds
	 * 								we are  adding those reward to Dtos	
	 * 							else ,we are not adding those reward to Dtos
	* 															
	 */						
	public ArrayList<KikspotRewardsDto> getKikspotRewardsOfUserByUserLevelAndCreds(Integer userId) throws UserNotFoundException, UserGameCredNotFoundException, KikspotRewardNotFoundException{
		log.info("inside getKikspotRewardsOfUserByUserLevelAndCreds()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
		ArrayList<KikspotRewards> kikspotRewards=this.creadDao.getKikspotRewardsByLevel(userGameCreds.getLevel());
		ArrayList<KikspotRewardsDto> kikspotRewardsDtos=new ArrayList<KikspotRewardsDto>();
		for(KikspotRewards kikspotReward:kikspotRewards){
			if(kikspotReward.getStartDate().before(new Date())){ // Checking if rewardstartDate is before the current date
				if(null!=kikspotReward.getEndDate()){
					if(kikspotReward.getEndDate().after(new Date())){ // Checking if rewardendDate is after the current date
						if(null!=kikspotReward.getCreds()){ // If that reward has creds means,we check that usercreds is greater than or equal to rewardcreds
							if(userGameCreds.getCreds()>=kikspotReward.getCreds()){
								KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
								kikspotRewardsDtos.add(kikspotRewardsDto);
							}
						}
						else{ // If that reward does not have creds 
							KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
							kikspotRewardsDtos.add(kikspotRewardsDto);
						}
					}
				}
				else{ // If end Date is null(it means that reward never end)
					if(null!=kikspotReward.getCreds()){	// If that reward has creds means,we check that usercreds is greater than or equal to rewardcreds
						if(userGameCreds.getCreds()>=kikspotReward.getCreds()){
							KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
							kikspotRewardsDtos.add(kikspotRewardsDto);
						}
					}
					else{ // If that reward does not have creds 
						KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
						kikspotRewardsDtos.add(kikspotRewardsDto);
					}
				}
			}
			
		}
		return kikspotRewardsDtos;
	}
	/**
	 * Created By bhagya On January 22nd,2016
	 * @param userId
	 * @param rewardId
	 * @return
	 * @throws Exception
	 * 
	 * Method FOR Getting the Kikspot Reward By rewardId for the user
	 * 	Steps:
	 * 		   1. get kikspotUser by userId
	 * 		   2. get kikspot reward by rewardId
	 * 		   3. get UserGameCreds By kikspotUser
	 * 		   4.  checking if the reward has creds
	 * 					Reduce the user creds and update the creds and level of user for remaining creds
	 * 			   else (if the reward has no creds)
	 * 					getting the creds by gamelevel
	 * 					reduce the user creds and then update the creds and level of user for remaining creds
	 * 		   5. Updating the cred history
	 * 					
	 * 		return the kikspotReward Dto
	 */
	
	public KikspotRewardsDto getKikspotRewardByRewardIdForUser(Integer userId,Integer rewardId) throws Exception{
		log.info("inside getKikspotRewardByRewardIdForUser()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		KikspotRewards kikspotReward=this.creadDao.getKikspotRewardByRewardId(rewardId);
		UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
		Integer reducedCreds=0;
		if(null!=kikspotReward.getCreds()){ //Checking If kikspot reward has creds
			 reducedCreds=userGameCreds.getCreds()-kikspotReward.getCreds();
			try{
				GameLevels gameLevel=this.gameDao.getGameLevelByMinimumCreds(reducedCreds);
				userGameCreds.setCreds(reducedCreds);
				userGameCreds.setLevel(gameLevel.getLevel());
				this.creadDao.saveorUpdateUserGameCred(userGameCreds);
			}
			catch(GameLevelNotFoundException e){
				
			}
		}
		else{ //If the kikspot reward has no creds
			try{
				GameLevels gameLevel=this.gameDao.getGameLevelByLevel(kikspotReward.getLevel());
				reducedCreds=userGameCreds.getCreds()-gameLevel.getMinCreds();
				GameLevels level=this.gameDao.getGameLevelByMinimumCreds(reducedCreds);
				userGameCreds.setCreds(reducedCreds);
				userGameCreds.setLevel(level.getLevel());
				this.creadDao.saveorUpdateUserGameCred(userGameCreds);
			}
			catch(GameLevelNotFoundException e){
				
			}
		}
		
		// UPDATING THE USER CRED HISTORY
		UserCredHistory userCredHistory=this.creadDao.getMostRecentUserCredHistoryofUser(kikspotUser);
			userCredHistory.setCred(reducedCreds);
			userCredHistory.setDate(new Date());
		this.creadDao.saveorUpdateUserCredHistory(userCredHistory);
		
		//Populate Kikspot Reward Dto
		KikspotRewardsDto kikspotRewardsDto=KikspotRewardsDto.populateKikspotRewards(kikspotReward);
		
		return kikspotRewardsDto;
	}

   /**
    * 
    * 
    * Added By Firdous 
    * Method to update User Creds in every Six months
    * 
    * 
    */
	@Override
	public Integer updateAllUsersCreds() throws Exception {
		ArrayList<UserGameCreds> userCredsDtos=this.creadDao.getGameCredsofAllUsers();
		for(UserGameCreds userCredsDto:userCredsDtos){
			try{
			UpdatedCredsInSixMonths updateCreds=this.creadDao.getCredsOfUserInSixMonthsPeriod(userCredsDto.getKikspotUser());
			updateCreds.setCreds(userCredsDto.getCreds());
			updateCreds.setKikspotUser(userCredsDto.getKikspotUser());
		    this.creadDao.saveOrUpdateCreds(updateCreds);
			}
			catch(UserGameCredNotFoundException e){
				UpdatedCredsInSixMonths updateCreds=new UpdatedCredsInSixMonths();
				updateCreds.setCreds(userCredsDto.getCreds());
				updateCreds.setKikspotUser(userCredsDto.getKikspotUser());
			    this.creadDao.saveOrUpdateCreds(updateCreds);
			}
		}
		return 1;
	}
    
	
	/**
	 * Added by Firdous
	 * Method to get the users creds in 6 months
	 */

	@Override
	public Integer getCredsOfUserInSixMonths(KikspotUserDto kikspotUserDto) throws Exception {
		KikspotUser user=this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
		UpdatedCredsInSixMonths updateCreds=this.creadDao.getCredsOfUserInSixMonthsPeriod(user);
		return updateCreds.getCreds();
	}
	
}
