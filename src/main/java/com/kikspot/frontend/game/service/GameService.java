package com.kikspot.frontend.game.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.GamesNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.game.dto.GameDto;
import com.kikspot.frontend.game.dto.GameLevelsDto;
import com.kikspot.frontend.game.dto.GameRuleTypeDto;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;

public interface GameService {

	public ArrayList<GameRulesDto> getGameRules(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending,String locationFilter)throws GameRuleNotFoundException, Exception;
	public UserDashboardDto getUserDashboardInformation(Integer userId, double latitude, double longitude)throws Exception;
	public GameRulesDto getGameRuleById(Integer id) throws Exception;
	public Integer savingchangedGameRuleStatus(Integer gameRuleId, Boolean isActive) throws Exception;
	public Integer saveOrUpdateGameLevel(GameLevelsDto gameLevelDto) throws Exception;
	public Integer saveOrUpdateGameRule(GameRulesDto gameRulesDto) throws Exception;
	public ArrayList<GameLevelsDto> getGameLevels(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws Exception;
	public GameLevelsDto getGameLevelByGameLevelId(Integer gameLevelId) throws Exception;
	public Integer deleteGameLevelByGameLevelId(Integer gameLevelId) throws Exception;
	public Integer deleteGameRuleByGameRuleId(Integer gameRuleId) throws Exception;
	public ArrayList<GameRulesDto> getGameRulesByLocationId(String locationId) throws GameRuleNotFoundException;
	public Integer addGameRuleForRecommendationLocation(Integer locationId,String rule,Integer creds,Integer userId, String gameRuleType,Integer ruleLimit,String startTime,String endTime,Date openDate,Date expiryDate,Boolean isActive) throws Exception;
	public ArrayList<String> getDistinctLocationsOfGameRules();
	public ArrayList<GameRulesDto> getGameRulesOfaLocationByLocationIdForUser(String locationId,Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException;
	public ArrayList<GameRulesDto> getAllGameRules(Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId, double latitude, double longitude) throws GameRuleNotFoundException;
	public ArrayList<GameRulesDto> getGameRulesForListOfLocations(List<String> locationAPIIds,Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException;
	public UserGameCredsDto updateUserGameCredsByGameRuleId(Integer gameRuleId,Integer userId,String locationId) throws Exception;
	public ArrayList<UserGameCredsDto> getUsersGameCredsByGameLevel(String level) throws UserGameCredNotFoundException;
	public ArrayList<UserGameCredsDto> getAllUsersCredsBasedOnLocation(String time) throws Exception;
	public ArrayList<UserGameCredsDto> getUsersCredsOfUsersFriends(Integer userId) throws Exception;
	public UserGameCredsDto getUsersGameCredsByUserId(Integer userId) throws Exception;
	public Integer getPointsNeededForNextLevel(UserGameCredsDto gameCredDto) throws Exception;
	public ArrayList<UserGameCredsDto> getAllUsersCredsInCurrentweek() throws Exception;
	public ArrayList<UserGameCredsDto> getGameCredDetailsOfAllUsers() throws UserCredHistoryNotFoundException,UserGameCredNotFoundException;
	public ArrayList<UserGameCredsDto> getUserCredsOfFriendsInPresentWeek(Integer userId) throws Exception;
	public ArrayList<GameDto> getAllGames() throws GamesNotFoundException;
	public ArrayList<GameRulesDto> getGameRulesByGameId(Integer gameId) throws GameRuleNotFoundException;
	public Integer getPointsNeededForNextLevelToReach(GameLevelsDto gameLevelDto, UserGameCredsDto gameCredDto) throws Exception;
	public GameLevelsDto getGameLevelByLevelName(String level) throws Exception;
	public void getPushNotificationService()throws IOException;
	public ArrayList<GameRuleTypeDto> getGameRuleTypes() throws Exception;
	public ArrayList<GameRules> getGameRuleByType(String ruleType) throws Exception;
	public GameRules getGameRuleByActivityType(String string) throws Exception;
	public Integer savingchangedGameRuleVisibility(Integer gameRuleId, Boolean visible) throws Exception;
	
	
	public Map<String, String> updateGameCredsForShareFunctions(Integer userId,String type) throws Exception;
	public ArrayList<GameRules> getFilteredRulesByCheckingOpenDateAndCloseDateAndRuleLimit(ArrayList<GameRules> gameRules);
	public GameRules getFilteredRuleByCheckingOpenDateAndCloseDateAndRuleLimit(GameRules gameRule) throws GameRuleNotFoundException;
	public GameLevelsDto getNextGameLevelByLevelName(String level) throws Exception;
	public ArrayList<GameRulesDto> getGlobalGameRulesByGameId(Integer gameId,Integer userId) throws Exception;
	public ArrayList<GameRulesDto> getGameRulesByLocationIdAndUserId(String locationId,Integer userId) throws GameRuleNotFoundException;
	public ArrayList<GameRulesDto> getGameRulesByOnlyLocationId(String locationId) throws GameRuleNotFoundException;
	public String getLocationName(String kikspotlocationId) throws Exception;
	
	public ArrayList<Integer> getDistinctGameCredDetailsOfAllUsers() throws UserGameCredNotFoundException;
}
