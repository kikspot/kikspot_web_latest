package com.kikspot.frontend.game.controller;
/**
 * 
 * created by Firdous on 02-12-2015
 * Handle Game Rules
 * 
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;


import java.util.TimeZone;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.game.dto.GameLevelsDto;
import com.kikspot.frontend.game.dto.GameRuleTypeDto;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.CredService;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.recommendation.dto.KikspotRecommendationLocationDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

import twitter4j.org.json.JSONObject;



@Controller("GameController")
@RequestMapping("/game")
public class GameController {
	
	
	private static Logger log=Logger.getLogger(GameController.class);
	
	@Resource(name="gameService")
	private GameService gameService;
	
	@Resource(name="credService")
	private CredService credService;

	
	@Resource(name="userService")
	private UserService userService;
	
    @InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy hh:mm");
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
		dateFormat.setLenient(true);
		timeFormat.setLenient(true);
		// true passed to CustomDateEditor constructor means convert empty String to null
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class,"startTime",new CustomDateEditor(timeFormat, true));
		binder.registerCustomEditor(Date.class,"endTime",new CustomDateEditor(timeFormat, true));
		
	}
     
    
  
	
    @Autowired
	@Qualifier("gameRulesDtoValidator")
	private Validator gameRulesDtoValidator;


	@InitBinder("gameRulesDto")
	protected void initGameRuleUserBinder(WebDataBinder binder) {
	    binder.setValidator(gameRulesDtoValidator);
	}
	
	@Autowired
	@Qualifier("gameLevelsDtoValidator")
	private Validator gameLevelsDtoValidator;


	@InitBinder("gameLevelsDto")
	protected void initGameLevelUserBinder(WebDataBinder binder) {
	    binder.setValidator(gameLevelsDtoValidator);
	}
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	/**
	 * 
	 * Added by Firdous on 03-12-2015
	 * method to initiate adding game rule
	 * 
	 * 
	 */
	@RequestMapping(value="/addgamerule.do",method=RequestMethod.GET)
	public String addGameRule(@ModelAttribute("gameRulesDto")GameRulesDto gameRulesDto,Map<String, Object> map) throws Exception{
		
		
		log.info("inside addGameRule()");
		try {
			ArrayList<GameRuleTypeDto> gameRuleTypes=this.gameService.getGameRuleTypes();
			/*ArrayList<KikspotUserDto> kikspotUserDtos=this.userService.getAllUsers(null,null,null,null,null);*/
			
			ArrayList<KikspotUserDto> kikspotUserDtos=this.userService.getAllUsersEcxeptByPassUsers();
			map.put("gameRuleTypes",gameRuleTypes);
			map.put("users", kikspotUserDtos);
			return "game/addGameRule";
		}
		catch(Exception e){
			return "error";
		}
	
	}
	
	/**
	 * 
	 * Added by Firdous on 03-12-2015
	 * method to add game rule
	 * 
	 * 
	 */
	
	@RequestMapping(value="/addgamerule.do",method=RequestMethod.POST)
    public String addGameRuleSuccess(@Valid @ModelAttribute ("gameRulesDto")GameRulesDto gameRulesDto,BindingResult validResult,Map<String, Object> map,RedirectAttributes redAttribs) throws Exception{
		
		log.info("inside addGameRuleSuccess()");
       	try {			
			if(validResult.hasErrors()) {
				
				return "game/addGameRule";
			}
			
		 	Integer savedResult=this.gameService.saveOrUpdateGameRule(gameRulesDto);
		    if(savedResult>0){
				redAttribs.addFlashAttribute("status","Game Rule Added Successfully");		
				return "redirect:/game/getgamerules.do";
		    }
		    else {
				throw new Exception();
		    }
			
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	
	}
	
    /**
    * Added by Firdous on 03-12-2015
    * method to get all the games rules
    * @param map
    * @return
    * @throws Exception
    * 
    * Modified By Bhagya On December 11th,2015
    * Implemented the pagination,searching and sorting
    */
	
	
	@RequestMapping(value="/getgamerules.do")
	public String getRules(Map<String, Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto,@RequestParam(value="locationFilter",required=false) String locationFilter) throws Exception{
		
		log.info("inside getRules()");
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("gameRuleId");
			}
			if(null!=locationFilter && locationFilter.contains("%26")){
				locationFilter=locationFilter.replace("%26", "&");
			}
		
			ArrayList<GameRulesDto> gameRuleDtos=this.gameService.getGameRules(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection(),locationFilter);
			/*for(GameRulesDto gameRuleDto:gameRuleDtos){
				SimpleDateFormat formatDate=new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
				formatDate.setTimeZone(TimeZone.getTimeZone("America/New_York"));
				SimpleDateFormat timeFormat= new SimpleDateFormat("hh:mm a");
				timeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
				if(null!=gameRuleDto.getExpiryDate()){
				Date expiryDate=gameRuleDto.getExpiryDate();
				String expiryDateString=formatDate.format(expiryDate);
				gameRuleDto.setExpiryDate(formatDate.parse(expiryDateString));
				System.out.println(" game rule "+gameRuleDto.getRule() +" expiry date "+formatDate.parse(expiryDateString));
				}
				if(null!=gameRuleDto.getOpenDate()){
				Date openDate=gameRuleDto.getOpenDate();
				formatDate.format(openDate);
				}
				if(null!=gameRuleDto.getStartTime()){
					Date startTime=gameRuleDto.getStartTime();
					timeFormat.format(startTime);
			    }
				if(null!=gameRuleDto.getEndTime()){
					Date endTime=gameRuleDto.getEndTime();
					timeFormat.format(endTime);
			    }
			}*/
			Integer totalResults=gameRuleDtos.get(0).getTotalRules();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			ArrayList<String> locations=this.gameService.getDistinctLocationsOfGameRules();
			map.put("locations", locations);
			map.put("locationFilter",locationFilter);
		    map.put("gameRuleDtos",gameRuleDtos);
		    return "game/gameRules";
		}
		
		catch(GameRuleNotFoundException e){
			String message=null;
			if(null!=listBeanDto.getSearchBy()&& listBeanDto.getSearchBy().trim().length()>0){
				message="Game Rule Not Found For Searched Criteria";
				}
			    message="Game Rules Not Found";
			ArrayList<String> locations=this.gameService.getDistinctLocationsOfGameRules();
			map.put("locations", locations);
			map.put("locationFilter",locationFilter);
			map.put("message", message);
			return "game/gameRules";
			
		}
		catch(Exception e) {
			e.printStackTrace();
			map.put("message","error");
			return "error";
		}
		
	}

    /**
     * 
     * Added by Firdous on 03-12-2015
     * method to get all the field of game rules for editing
     * 
    */
	@RequestMapping(method=RequestMethod.GET,value="/editgamerule.do")
	public String initEditGameRule(@RequestParam("gameRuleId")Integer id, Map<String, Object> map, RedirectAttributes redAttribs){
		
		log.info("inside initEditGameRule");
		
		try{
			GameRulesDto gameRuleDto=this.gameService.getGameRuleById(id);
			/*SimpleDateFormat formatDate=new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			formatDate.setTimeZone(TimeZone.getTimeZone("America/New_York"));
			SimpleDateFormat timeFormat= new SimpleDateFormat("hh:mm a");
			timeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
			if(null!=gameRuleDto.getExpiryDate()){
			Date expiryDate=gameRuleDto.getExpiryDate();
			formatDate.format(expiryDate);
			}
			if(null!=gameRuleDto.getOpenDate()){
			Date openDate=gameRuleDto.getOpenDate();
			formatDate.format(openDate);
			}
			if(null!=gameRuleDto.getStartTime()){
				Date startTime=gameRuleDto.getStartTime();
				timeFormat.format(startTime);
		    }
			if(null!=gameRuleDto.getEndTime()){
				Date endTime=gameRuleDto.getEndTime();
				timeFormat.format(endTime);
		    }*/
			ArrayList<GameRuleTypeDto> gameRuleTypes=this.gameService.getGameRuleTypes();
			/*ArrayList<KikspotUserDto> kikspotUserDtos=this.userService.getAllUsers(null,null,null,null,null);*/
			
			ArrayList<KikspotUserDto> kikspotUserDtos=this.userService.getAllUsersEcxeptByPassUsers();
			
			map.put("gameRuleTypes",gameRuleTypes);
			map.put("gameRulesDto", gameRuleDto);
			map.put("users",kikspotUserDtos);
			map.put("title","Edit rule");
			return "game/editGameRule";
		}
	
		catch(GameRuleNotFoundException e) {
			String message="Game Rule Not Found";
			redAttribs.addFlashAttribute("status", message);
			return "redirect:/game/getgamerules.do";
			
		}
		catch(Exception e) {
			e.printStackTrace();
			String message="Error While Editing ";
			map.put("message",message);
			map.put("title", message);
			return "error";
		}		
	}
	
	
	/**
	 * added by Firdous
	 * method to edit the game Rules
	 * @param gameRulesDto
	 * @param map
	 * @param redAttribs
	 * @return
	*/
	@RequestMapping(method=RequestMethod.POST,value="/editgamerule.do")
	public String editGameRules(@RequestParam("gameRuleId") Integer gameRuleId,@Valid @ModelAttribute GameRulesDto gameRulesDto,BindingResult validResult,Map<String, Object> map,RedirectAttributes redAttribs){
	
		log.info("inside editGameRules");
	 	try {
	 		if(validResult.hasErrors()){
	 			return "game/editGameRule";
	 		}
			Integer editedResult=this.gameService.saveOrUpdateGameRule(gameRulesDto);
			redAttribs.addFlashAttribute("status", " Game Rule Edited Successfully");
			if(editedResult>0){
				return "redirect:/game/getgamerules.do";
			}
			else{
				throw new Exception();
			}
		}
		
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Editing Game Rules ";
			map.put("message", message);
			map.put("title", message);
			return "error";
		}
	}
	
	/**
	 * Added by Firdous on 03-12-2015
	 * method to delete the game rule by accepting game rule id
	 * @param id
	 * @return
	*/
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="/deletegamerule.do")
	public String deleteGameRule(@RequestParam("gameRuleId")Integer id){
	 log.info("inside deleteGameRule()");
		try {
			Integer result=this.gameService.deleteGameRuleByGameRuleId(id);
			
			if(result>0){
				
					return "success";
			}
			else if(result==0){
				
				return "userCredsAvailable";
			}
			else{
					throw new Exception();
			}
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
			return "error";
		}
		
		
	}
	/**
	 * Created By Bhagya On december 11th,2015
	 * @param gameRuleId
	 * @param isActive
	 * @return
	 * 
	 * Method For Updating the Game Rule Status
	 */
	@ResponseBody
	@RequestMapping("/changegamerulestatus.do")
	public String changeGameRuleStatus(@RequestParam("gameRuleId")Integer gameRuleId,@RequestParam("isActive")Boolean isActive){
		log.info("inside changeGameRuleStatus()");
		try{
			Integer updatedResult=this.gameService.savingchangedGameRuleStatus(gameRuleId, isActive);
			GameRulesDto gameRulesDto=this.gameService.getGameRuleById(gameRuleId);
			
			if(updatedResult>0 && gameRulesDto.getIsActive()==isActive){
				return "success";
			}
			else if(updatedResult>0){
				return "missmatch";
			}
			else{
				return "error";
			}
		}
		catch(GameRuleNotFoundException e){
			System.out.println(" User Not Found For Changing the user Status");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelsDto
	 * @param map
	 * @return
	 * 
	 * Method For Adding the Game Level
	 */
	@RequestMapping(value="/addgamelevel.do",method=RequestMethod.GET)
	public String addGameLevel(@ModelAttribute("gameLevelsDto") GameLevelsDto gameLevelsDto, Map<String,Object> map,RedirectAttributes redAttribs){
		log.info("inside addGameLevel()");
		try{
			ArrayList<GameLevelsDto> gameLevelDtos=this.gameService.getGameLevels(null, null, null, null, null);
			if(gameLevelDtos.size()==5){
				redAttribs.addFlashAttribute("status", "U can not add game level.. 5 levels already exists");
				return"redirect:/game/viewgamelevels.do";
			}
			else{
				map.put("title", "Add Game Level");
				return "game/addGameLevel";
			}
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Adding Game Level");
			return "error";
		}
	
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelsDto
	 * @param map
	 * @return
	 * 
	 * Method For saving the Game Level Information
	 */
	@RequestMapping (value="/addgamelevel.do",method=RequestMethod.POST)
	public String saveGameLevel(@Valid @ModelAttribute("gameLevelsDto") GameLevelsDto gameLevelsDto,BindingResult validResult, Map<String,Object> map,RedirectAttributes redAttribs){
		log.info("inside saveGameLevel()");
		try{
			if(validResult.hasErrors()){
				return "game/addGameLevel";
			}
			Integer savedResult=this.gameService.saveOrUpdateGameLevel(gameLevelsDto);
			if(savedResult>0){
				redAttribs.addFlashAttribute("status", "Game Level Added Successfully");
				return"redirect:/game/viewgamelevels.do";
			}
			else{
				throw new Exception();
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Saving Game Level");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param map
	 * @param listBeanDto
	 * @return
	 * 
	 * Method for Viewing the game Levels 
	 */
	@RequestMapping(value="/viewgamelevels.do")
	public String viewGameLevels(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info("inside viewGameLevels()");
		
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("gameLevelId");
			}
			ArrayList<GameLevelsDto> gameLevelsDtos=this.gameService.getGameLevels(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection());
			Integer totalResults=gameLevelsDtos.get(0).getTotalLevels();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
		    map.put("gameLevels",gameLevelsDtos);
			return "game/viewGameLevels";
		}
		catch(GameLevelNotFoundException e){
			String message=null;
			if(null!=listBeanDto.getSearchBy()&& listBeanDto.getSearchBy().trim().length()>0){
			  message="Game Level Not Found For Searched Criteria";
			}
			else{
			 message="Game Levels Not Found";
			}
			map.put("message", message);
			return "game/viewGameLevels";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Viewing Game Levels");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelId
	 * @param map
	 * @param gameLevelsDto
	 * @param redAttribs
	 * @return
	 * 
	 * Method For Initiating The Edit Game level
	 */
	@RequestMapping(value="/editgamelevel.do",method=RequestMethod.GET)
	public String initEditGameLevel(@RequestParam("gameLevelId")Integer gameLevelId,Map<String,Object> map,@ModelAttribute("gameLevelsDto") GameLevelsDto gameLevelsDto,RedirectAttributes redAttribs){
		log.info("inside initEditGameLevel()");
		try{
			GameLevelsDto resultedGameLevelDto=this.gameService.getGameLevelByGameLevelId(gameLevelId);
			map.put("gameLevel", resultedGameLevelDto);
			return "game/editGameLevel";
		}
		catch(GameLevelNotFoundException e){
			String message="Game Level Not Found";
			redAttribs.addFlashAttribute("status", message);
			return "redirect:game/viewgamelevels.do";
			
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Initiating Edit Game Level");
			return "error";
		}
		
	}
	
	
	
	
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param map
	 * @param gameLevelsDto
	 * @return
	 * 
	 * Method for Updating The Game Level
	 */
	@RequestMapping(value="/editgamelevel.do",method=RequestMethod.POST)
	public String editGameLevel(Map<String,Object> map,@Valid @ModelAttribute("gameLevelsDto") GameLevelsDto gameLevelsDto,BindingResult validResult,RedirectAttributes redAttribs){
		log.info("inside editGameLevel()");
		try{
			if(validResult.hasErrors()){
				return "game/editGameLevel";
			}
			Integer updatedResult=this.gameService.saveOrUpdateGameLevel(gameLevelsDto);
			if(updatedResult>0){
				redAttribs.addFlashAttribute("status", "Game Level Edited Successfully");
				return"redirect:/game/viewgamelevels.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(GameLevelNotFoundException e){
			String message="Game Level Not Found";
			redAttribs.addFlashAttribute("status", message);
			return "redirect:game/viewgamelevels.do";
			
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Editing Game Levels");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On December 14th,2015
	 * @param gameLevelId
	 * @return
	 * 
	 * Method For Deleting the Game Level
	 * Steps: 1. check the If any Users Exists Under the Game level
	 * 				if users exists means ,return error
	 * 			otherwise
	 * 			delete	the get game Level By Id and return success
	 */
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="/deletegamelevel.do")
	public String deleteGameLevel(@RequestParam("gameLevelId")Integer gameLevelId,@RequestParam("gameLevel")String gameLevel){
		log.info("inside deleteGameLevel()");
		try {
			try{
				ArrayList<UserGameCredsDto> users=this.gameService.getUsersGameCredsByGameLevel(gameLevel);
				return "error";
			}
			catch(UserGameCredNotFoundException e){
				Integer result=this.gameService.deleteGameLevelByGameLevelId(gameLevelId);
				if(result>0){
						return "success";
				}
				else{
						throw new Exception();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "error";
		}
		
		
	}
	/***
	 * Created By Bhagya On December 31st,2015
	 * @param locationId
	 * @return
	 * 
	 * Method for getting the game rules By Recommendation Location Id
	 */
	@ResponseBody
	@RequestMapping(value="/gamerulesforlocation.do")
	public String getGameRulesByLocationId(@RequestParam("locationId") Integer locationId){
		log.info("inside getGameRulesByLocation()");
		
		try{
			ArrayList<GameRulesDto> gameRulesDtos=this.gameService.getGameRulesByLocationId(locationId.toString());
			JSONObject gameRuleJsons=new JSONObject();
			for(GameRulesDto gameRulesDto: gameRulesDtos){
				JSONObject gameRuleJson=new JSONObject();
				gameRuleJson.accumulate("gameRuleId", gameRulesDto.getGameRuleId());
				if(null!=gameRulesDto.getRule()){
					gameRuleJson.accumulate("rule", gameRulesDto.getRule());
				}
				else{
					gameRuleJson.accumulate("rule", "");
				}
				if(null!=gameRulesDto.getGameRuleType()){
					gameRuleJson.accumulate("gameRuleType", gameRulesDto.getGameRuleType());
				}
				else{
					gameRuleJson.accumulate("rule", "");
				}
				ArrayList<GameRuleTypeDto> gameRuleTypes=this.gameService.getGameRuleTypes();
				gameRuleJson.accumulate("gameRuleTypes",gameRuleTypes);
				gameRuleJson.accumulate("creds",gameRulesDto.getCreds());
				if(null!=gameRulesDto.getRuleLimit()){
					gameRuleJson.accumulate("ruleLimit", gameRulesDto.getRuleLimit());
				}
				else{
					gameRuleJson.accumulate("ruleLimit", "");
				}
				if(null!=gameRulesDto.getUserId()){
					gameRuleJson.accumulate("user",this.userService.getKikspotUserDetailsByUserId(gameRulesDto.getUserId()).getUsername());
				}
				else{
					gameRuleJson.accumulate("user","");
				}
				ArrayList<KikspotUserDto> users=this.userService.getAllUsersEcxeptByPassUsers();
				gameRuleJson.accumulate("users",users);
				if(null!=gameRulesDto.getExpiryDate()){
					Date expiryDate=gameRulesDto.getExpiryDate();
					SimpleDateFormat expiryDateFormat= new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
					//expiryDateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
					gameRuleJson.accumulate("expiryDate", expiryDateFormat.format(expiryDate));
				}
				else{
					gameRuleJson.accumulate("expiryDate","");
				}
				if(null!=gameRulesDto.getStartTime()){
					Date startTime=gameRulesDto.getStartTime();
					SimpleDateFormat startTimeFormat= new SimpleDateFormat("hh:mm a");
					//startTimeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
					gameRuleJson.accumulate("startTime", startTimeFormat.format(startTime));
				}
				else{
					gameRuleJson.accumulate("startTime","");
				}
				if(null!=gameRulesDto.getEndTime()){
					Date endTime=gameRulesDto.getEndTime();
					SimpleDateFormat endTimeFormat= new SimpleDateFormat("hh:mm a");
					//endTimeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
					gameRuleJson.accumulate("endTime",  endTimeFormat.format(endTime));
				}
				else{
					gameRuleJson.accumulate("endTime","");
				}
				if(null!=gameRulesDto.getOpenDate()){
					Date openDate=gameRulesDto.getOpenDate();
					SimpleDateFormat openDateFormat= new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
				//	openDateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
					gameRuleJson.accumulate("openDate", openDateFormat.format(openDate));
				}
				else{
					gameRuleJson.accumulate("openDate","");
				}
			   
				
				gameRuleJson.accumulate("isActive", gameRulesDto.getIsActive());
				
				gameRuleJsons.append("gameRule", gameRuleJson);
			
			}
		
			return gameRuleJsons.toString();
		}
		catch(GameRuleNotFoundException e){
			log.error("No Game Rules Found");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Viewing Game Rules For Location");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On Jan 04th,2016
	 * @param locationId
	 * @param rule
	 * @param creds
	 * @param ruleLimit
	 * @param expiryDate
	 * @param isActive
	 * @return
	 * 
	 * Method for Adding or saving the Game Rule Of a lOcation
	 */
	
	@ResponseBody
	@RequestMapping(value="/addgameruleforlocation.do")
	public String addGameRuleForRecommendationLocation(@RequestParam("locationId")Integer locationId,@RequestParam("rule") String rule,@RequestParam("creds") Integer creds,
			@RequestParam("ruleLimit") Integer ruleLimit,@RequestParam(value="userId",required=false) Integer userId,@RequestParam("gameRuleType") String gameRuleType,@RequestParam(value="startTime",required=false)String startTime,@RequestParam(value="endTime",required=false)String endTime,@RequestParam("openDate") Date openDate,@RequestParam("expiryDate") Date expiryDate,@RequestParam("isActive") Boolean isActive){
		log.info("inside addGameRulesForRecommendationLocation()");
		
		try{
			
			
		   Integer savedResult=this.gameService.addGameRuleForRecommendationLocation(locationId, rule, creds,userId,gameRuleType,ruleLimit,startTime,endTime, openDate, expiryDate, isActive);
			if(savedResult>0){
				return "success";
			}
			else{
				throw new Exception();
			}
		
			
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Adding Game Rule For Location");
			return "error";
		}
		
	}
	
	/**
	 * Added by Firdous on 14th march 2016
	 * method to update creds of users in time period of 6 months
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updatecredsinsixmonths.do")
	public String updateCredsInEverySixMoths()
	{
		log.info("inside updateCredsInEverySixMonths()");
		try{
			Integer savedResult=this.credService.updateAllUsersCreds();
			if(savedResult>0){
				 return "success";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	/**
	 * 
	 * @param userId
	 * @param map
	 * @return
	 * 
	 * Method to get the game details
	 */
	@RequestMapping(value="/user/getusergamedetails.do",method=RequestMethod.GET)
	public String getUserGameDetails(@RequestParam("userId") Integer userId,Map<String, Object> map){
		log.info("inside getUserGameDetails()");
		try{
		 UserGameCredsDto gameCredDto=this.gameService.getUsersGameCredsByUserId(userId);
		 ArrayList<UserGameCredsDto> userGameCredDtos=new ArrayList<UserGameCredsDto>();
		 userGameCredDtos=this.gameService.getGameCredDetailsOfAllUsers();
		 int rank=0;
		 int i=1;
		 for(UserGameCredsDto userGameCredDto:userGameCredDtos){
			if(userGameCredDto.getKikspotUserDto().getUserId().equals(userId)){
				rank=i;
			}
			i++;
	     }
		 map.put("gameCredDto",gameCredDto);
		 map.put("rank", rank);
		 return "game/gameDetails";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
		
	}
	
	
	/**
	 * Created by Firdous 
	 * Method to change the game rule visiblity
	 * @param gameRuleId
	 * @param visible
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value="/changegamerulevisiblity.do",method=RequestMethod.GET)
	public String changeGameRuleVisibility(@RequestParam("gameRuleId")Integer gameRuleId,@RequestParam("visible")Boolean visible){
		log.info("inside changeGameRuleVisibility()");
		System.out.println("change game rule status");
		try{
			Integer updatedResult=this.gameService.savingchangedGameRuleVisibility(gameRuleId,visible);
			if(updatedResult>0){
				System.out.println("success");
				return "success";
			}
			else{
				throw new Exception();
			}
		}
		catch(GameRuleNotFoundException e){
			System.out.println("Game Rule Not found for changing game rule visibility");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	
	}
	/**
	 * Check location existance
	 * @param kikspotLocationId
	 * @return
	 */
	@RequestMapping(value="/checkrecommendationlocation.do",method=RequestMethod.GET)
	@ResponseBody
	public String checkLocationExistenceByKikspotLocationId(@RequestParam("locationId")String kikspotLocationId){
		//log.info("inside checkLocationExistenceByKikspotLocationId()");
		
		try{
			
			String locationName=this.gameService.getLocationName(kikspotLocationId);
			if(null!=locationName&& locationName.length()>0){
				return locationName;
			}
			else{
				
				throw new KikspotRecommendationLocationNotFoundException();
			}
		}
		catch(KikspotRecommendationLocationNotFoundException e){
			log.info("No Location Exists,You Can Add Location");
			return "error";
		}
		catch(Exception e){
			
			log.error("Error While Validating Location Existence");
			return "error";
		}
	}
}

