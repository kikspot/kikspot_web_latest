package com.kikspot.frontend.game.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.game.model.GameLevels;
import com.kikspot.frontend.common.utility.dto.DisplayListBeanDto;
import com.kikspot.frontend.game.dto.GameLevelsDto;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;
import com.kikspot.frontend.game.service.CredService;
import com.kikspot.frontend.game.service.GameService;
/**
 * Created By Bhagya On January 20th,2016
 *	Controller class For Rewards
 */

@Controller("RewardsController")
@RequestMapping("/rewards")
public class RewardsController{
	
	private static Logger log=Logger.getLogger(RewardsController.class);
	
	@Resource(name="credService")
	private CredService credService;
	
	@Resource(name="gameService")
	private GameService gameService;
	
	 @Autowired
	 @Qualifier("kikspotRewardsDtoValidator")
	 private Validator kikspotRewardsDtoValidator;


	 @InitBinder("kikspotRewardsDto")
	 protected void initGameRuleUserBinder(WebDataBinder binder) {
		   binder.setValidator(kikspotRewardsDtoValidator);
	 }
	
	/*
	 * 
	 *  for Handling conditions when Date may be null, useful while Add/Edit Reward, where end date may be entered for certain rewards..
	 *  
	 *  */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    dateFormat.setLenient(false);
	    // true passed to CustomDateEditor constructor means convert empty String to null
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	
	/**
	 * Created By Bhagya On January 20th, 2016
	 * @param map
	 * @param kikspotRewardsDto
	 * @return
	 * 
	 * Method For Initiating The Add Kikspot Reward/Feature UnLocking 
	 */
	@RequestMapping(value="/addreward.do",method=RequestMethod.GET)
	public String initAddKikspotReward(Map<String,Object> map,@ModelAttribute("kikspotRewardsDto") KikspotRewardsDto kikspotRewardsDto){
		log.info("inside initAddKikspotReward()");
		try{
			map.put("title", "Add Rewrad");
			ArrayList<GameLevelsDto> gameLevels=this.gameService.getGameLevels(null, null, null, null, false);
			map.put("levels", gameLevels);
			return "rewards/addReward";
		}
		catch(GameLevelNotFoundException e){
			map.put("message", "Levels Not Found");
			return "error";
		}
		catch(Exception e){
			return "error";
		}
		
		
	}
	/**
	 * Created By Bhagya On January 20th, 2016
	 * @param kikspotRewardsDto
	 * @param map
	 * @return
	 * 
	 * Method to handle saving the Kikspot Reward
	 */
	
	@RequestMapping(value="/addreward.do", method=RequestMethod.POST)
	public String processAddKikspotReward(@Valid @ModelAttribute("kikspotRewardsDto") KikspotRewardsDto kikspotRewardsDto,BindingResult validResult,Map<String,Object> map,RedirectAttributes redAttribs){
		log.info("inside processAddKikspotReward()");
		try{
			if(validResult.hasErrors()) {	
				ArrayList<GameLevelsDto> gameLevels=this.gameService.getGameLevels(null, null, null, null, false);
				map.put("levels", gameLevels);
				return "rewards/addReward";
			}
			Integer result=this.credService.saveOrUpdateKikspotRewards(kikspotRewardsDto);
			if(result>0){
				redAttribs.addFlashAttribute("status","Reward/Feature Added Successfully");
				return "redirect:/rewards/viewrewards.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message","Error While Saving Kikspot Reward");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On January 20th,2016
	 * @param map
	 * @param listBeanDto
	 * @return
	 * 
	 * Method For View The Kikspot Rewards
	 */
	@RequestMapping(value="/viewrewards.do")
	public String getKikspotRewards(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info("inside getKikspotRewards()");
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("rewardId");
			}
			ArrayList<KikspotRewardsDto> kikspotRewardsDtos=this.credService.getAllKikspotRewards(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSortBy(),listBeanDto.getSearchBy(),listBeanDto.getSortDirection());
			Integer totalResults=kikspotRewardsDtos.get(0).getTotalRewards();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("rewardsDtos", kikspotRewardsDtos);
			return "rewards/viewRewards";
		}
		catch(KikspotRewardNotFoundException e){
			map.put("message","Kikspot Rewards Not Found,you can Add Reward");
			return "rewards/viewRewards";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message","Error While Viewing Kikspot Rewards");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On January 20th,2016
	 * @param rewardId
	 * @param map
	 * @return
	 * 
	 * Method for Initiating Edit Reward
	 */
	@RequestMapping(value="/editreward.do",method=RequestMethod.GET)
	public String initEditKikspotReward(@ModelAttribute("kikspotRewardsDto") KikspotRewardsDto kikspotRewardsDto,@RequestParam("rewardId") Integer rewardId, Map<String,Object> map){
		log.info("inside initEditKikspotReward()");
		try{
			KikspotRewardsDto kikspotRewardDto=this.credService.getKikspotRewardByRewardId(rewardId);
			ArrayList<GameLevelsDto> gameLevels=this.gameService.getGameLevels(null, null, null, null, false);
			map.put("levels", gameLevels);
			map.put("rewardDto", kikspotRewardDto);
			return "rewards/editReward";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message","Error While Editing Kikspot Rewards");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On January 20th, 2016
	 * @param kikspotRewardsDto
	 * @param map
	 * @param redAttribs
	 * @return
	 * Method For editing the Kikspot Reward
	 */
	@RequestMapping(value="/editreward.do",method=RequestMethod.POST)
	public String editKikspotReward(@Valid @ModelAttribute("kikspotRewardsDto") KikspotRewardsDto kikspotRewardsDto,BindingResult validResult,Map<String,Object> map,RedirectAttributes redAttribs){
		log.info("inside editKikspotReward()");
		try{
			if(validResult.hasErrors()) {	
				ArrayList<GameLevelsDto> gameLevels=this.gameService.getGameLevels(null, null, null, null, false);
				map.put("levels", gameLevels);
				return "rewards/editReward";
			}
			Integer result=this.credService.saveOrUpdateKikspotRewards(kikspotRewardsDto);
			if(result>0){
				redAttribs.addFlashAttribute("status","Reward/Feature Edited Successfully");
				return "redirect:/rewards/viewrewards.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message","Error While Editing Kikspot Rewards");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On January 20th,2016
	 * @param rewardId
	 * @param isActive
	 * @return
	 * 
	 * Method For Updating The Reward Status
	 */
	@ResponseBody
	@RequestMapping("/updaterewardstatus.do")
	public String updateKikspotRewardStatus(@RequestParam("rewardId") Integer rewardId, @RequestParam("isActive") Boolean isActive){
		log.info("inside updateKikspotRewardStatus()");
		try{
			Integer result=this.credService.updateKikspotRewardStatus(rewardId, isActive);
			if(result>0){
				return "success";
			}
			else{
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On January 21st,2016
	 * @param rewardId
	 * @return
	 * 
	 * Method For Deleting the kikspot Reward
	 */
	@ResponseBody
	@RequestMapping("/deletereward.do")
	public String deleteKikspotReward(@RequestParam("rewardId") Integer rewardId){
		log.info("inside deleteKikspotReward()");
		try{
			Integer deletedResult=this.credService.deleteKikspotRewardByRewardId(rewardId);
			if(deletedResult>0){
				return "success";
			}
			else{
				throw new Exception();
			}
		}
		
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
		
	}
}