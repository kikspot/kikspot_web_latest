package com.kikspot.frontend.game.dto;

import java.util.Date;

import com.kikspot.backend.game.model.KikspotRewards;
/**
 * Created By Bhagya On January 20th,2016
 *	Dto for Kikspot Rewards
 */
public class KikspotRewardsDto{
	
	private Integer rewardId;
	private String rewardType;
	private String reward;
	private Integer creds;
	private String level;
	private Date startDate;
	private Date endDate;
	private Boolean isActive;
	private Integer totalRewards;
	private String rewardValue;
	/**Added locationId and Location Name By Bhagya On Jan 04th,2015*/
	private String locationId;
	private String locationName;
	
	
	public Integer getRewardId() {
		return rewardId;
	}
	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public Integer getCreds() {
		return creds;
	}
	public void setCreds(Integer creds) {
		this.creds = creds;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public Integer getTotalRewards() {
		return totalRewards;
	}
	public void setTotalRewards(Integer totalRewards) {
		this.totalRewards = totalRewards;
	}
		
	public String getRewardValue() {
		return rewardValue;
	}
	public void setRewardValue(String rewardValue) {
		this.rewardValue = rewardValue;
	}
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public static KikspotRewardsDto populateKikspotRewards(KikspotRewards kikspotRewards){
		KikspotRewardsDto kikspotRewardsDto=new KikspotRewardsDto();
		kikspotRewardsDto.setRewardId(kikspotRewards.getRewardId());
		kikspotRewardsDto.setRewardType(kikspotRewards.getRewardType());
		kikspotRewardsDto.setReward(kikspotRewards.getReward());
		kikspotRewardsDto.setCreds(kikspotRewards.getCreds());
		kikspotRewardsDto.setLevel(kikspotRewards.getLevel());
		kikspotRewardsDto.setStartDate(kikspotRewards.getStartDate());
		kikspotRewardsDto.setEndDate(kikspotRewards.getEndDate());
		kikspotRewardsDto.setIsActive(kikspotRewards.getIsActive());
		if(null!=kikspotRewards.getTotalRewards())
		kikspotRewardsDto.setTotalRewards(kikspotRewards.getTotalRewards());
		kikspotRewardsDto.setRewardValue(kikspotRewards.getRewardValue());
		kikspotRewardsDto.setLocationId(kikspotRewards.getLocationId());
		kikspotRewardsDto.setLocationName(kikspotRewards.getLocationName());
		return kikspotRewardsDto;
	}
}