package com.kikspot.frontend.game.dto;

import com.kikspot.backend.game.model.UpdatedCredsInSixMonths;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.frontend.user.dto.KikspotUserDto;

public class UpdatedCredsDto {
	
	
	private Integer userCredId;
	private Integer creds;
	private KikspotUserDto kikspotUserDto;
	public Integer getUserCredId() {
		return userCredId;
	}
	public void setUserCredId(Integer userCredId) {
		this.userCredId = userCredId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public Integer getCreds() {
		return creds;
	}
	public void setCreds(Integer creds) {
		this.creds = creds;
	}
	/**
	 * 
	 * @param userCredHistory
	 * @return
	 */
	public static UpdatedCredsDto populateUpdatedCredsDto(UpdatedCredsInSixMonths updatedCredsInSixMonths){
		UpdatedCredsDto updatedCredsDto=new UpdatedCredsDto();
		updatedCredsDto.setUserCredId(updatedCredsInSixMonths.getUserCredId());
		updatedCredsDto.setCreds(updatedCredsInSixMonths.getCreds());
		updatedCredsDto.setKikspotUserDto(updatedCredsDto.getKikspotUserDto());
		return updatedCredsDto;
	}
	
}
