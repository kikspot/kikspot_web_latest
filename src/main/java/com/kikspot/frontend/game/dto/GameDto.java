package com.kikspot.frontend.game.dto;

import com.kikspot.backend.game.model.Game;

/**
 * Dto for Games
 * @author KNS-ACCONTS
 *
 */
public class GameDto {

	
	private Integer gameId;
	private String gameName;
	
	
	
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	
	public static GameDto populateGameDto(Game game){
		GameDto gameDto=new GameDto();
		gameDto.setGameId(game.getGameId());
		gameDto.setGameName(game.getGameName());
		return gameDto;
	}
	
	
	
}
