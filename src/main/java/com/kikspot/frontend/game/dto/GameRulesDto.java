package com.kikspot.frontend.game.dto;

import java.sql.Time;
import java.util.Date;

import com.kikspot.backend.game.model.GameRules;


/**
 * Created by Jeevan on November 26, 2015
 * 
 * Dto for Game Rules..
 * 
 * @author KNS-ACCONTS
 *
 */
public class GameRulesDto {
	
	private Integer gameRuleId;
	
	private GameDto gameDto;
	
	private String rule;
	
	private Integer ruleLimit;
	
	private Integer creds;
	
	
	private Integer userId;
	private Date openDate;
	private Date expiryDate;
	private String gameRuleType;
	private Date startTime;
	private Date endTime;
	

	
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	private Integer totalRules;
	/**
	 * Added isActive,by bhagya on December 11th,2015*/
	private Boolean isActive;
	
	
	private Boolean visible;
	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	/**Added locationId and Location Name By Bhagya On Jan 04th,2015*/
	private String locationId;
	private String locationName;
	
	/** Added,latitude ,longitude,distance by bhagya on jan 12th,2016
	 * FOr Implementing CLOSE BY SORTING*/
	private Double latitude;
	private Double longitude;
	private Double distance;
	

	public Integer getGameRuleId() {
		return gameRuleId;
	}

	public void setGameRuleId(Integer gameRuleId) {
		this.gameRuleId = gameRuleId;
	}

	public GameDto getGameDto() {
		return gameDto;
	}

	public void setGameDto(GameDto gameDto) {
		this.gameDto = gameDto;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public Integer getRuleLimit() {
		return ruleLimit;
	}

	public void setRuleLimit(Integer ruleLimit) {
		this.ruleLimit = ruleLimit;
	}

	public Integer getCreds() {
		return creds;
	}

	public void setCreds(Integer creds) {
		this.creds = creds;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getTotalRules() {
		return totalRules;
	}

	public void setTotalRules(Integer totalRules) {
		this.totalRules = totalRules;
	}
	
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	
	
	public String getGameRuleType() {
		return gameRuleType;
	}

	public void setGameRuleType(String gameRuleType) {
		this.gameRuleType = gameRuleType;
	}
	public static GameRulesDto populateGameRulesDto(GameRules gameRule){
		GameRulesDto gameRuleDto=new GameRulesDto();
		gameRuleDto.setGameRuleId(gameRule.getGameRuleId());
		gameRuleDto.setCreds(gameRule.getCreds());
		gameRuleDto.setGameRuleType(gameRule.getGameRuleType());
		
		if(null!=gameRule.getExpiryDate())
			gameRuleDto.setExpiryDate(gameRule.getExpiryDate());
		if(null!=gameRule.getGame())
			gameRuleDto.setGameDto(GameDto.populateGameDto(gameRule.getGame()));
		gameRuleDto.setRule(gameRule.getRule());
		if(null!=gameRule.getRuleLimit())
			gameRuleDto.setRuleLimit(gameRule.getRuleLimit());
		if(null!=gameRule.getOpenDate())
			gameRuleDto.setOpenDate(gameRule.getOpenDate());
		if(null!=gameRule.getTotalRules())
		gameRuleDto.setTotalRules(gameRule.getTotalRules());
		gameRuleDto.setIsActive(gameRule.getIsActive());
		gameRuleDto.setVisible(gameRule.getVisible());
		if(null!=gameRule.getLocationId()){
			gameRuleDto.setLocationId(gameRule.getLocationId());
		}
		if(null!=gameRule.getUserId()){
			gameRuleDto.setUserId(gameRule.getUserId());
		}
		if(null!=gameRule.getStartTime())
			gameRuleDto.setStartTime(gameRule.getStartTime());
		
		if(null!=gameRule.getEndTime())
			gameRuleDto.setEndTime(gameRule.getEndTime());
		
		if(null!=gameRule.getLocationName()){
			gameRuleDto.setLocationName(gameRule.getLocationName());
		}
		return gameRuleDto;
	}
}
