package com.kikspot.frontend.game.dto;

import java.util.Date;


import com.kikspot.backend.game.model.Game;
import com.kikspot.backend.game.model.UserAddedGameRules;

public class UserAddedGameRuleDto {

	private Integer userGameRuleId;
	
	private Game game;
	
	private String rule;
	
	private Integer creds;
	
	private String gameRuleType;
	
	private Integer totalRules;
	
	private String locationId;
	
	private String locationName;
	
	private Double latitude;
	
	private Double longitude;
	
	private Date openDate;

	public Integer getUserGameRuleId() {
		return userGameRuleId;
	}

	public void setUserGameRuleId(Integer userGameRuleId) {
		this.userGameRuleId = userGameRuleId;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public Integer getCreds() {
		return creds;
	}

	public void setCreds(Integer creds) {
		this.creds = creds;
	}

	public String getGameRuleType() {
		return gameRuleType;
	}

	public void setGameRuleType(String gameRuleType) {
		this.gameRuleType = gameRuleType;
	}

	public Integer getTotalRules() {
		return totalRules;
	}

	public void setTotalRules(Integer totalRules) {
		this.totalRules = totalRules;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	
	public static UserAddedGameRuleDto populateUserAddedGameRuleDto(UserAddedGameRules gameRule){
		
		
		return null;
		
	}
}
