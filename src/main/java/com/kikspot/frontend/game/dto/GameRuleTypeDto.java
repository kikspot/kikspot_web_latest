package com.kikspot.frontend.game.dto;

import com.kikspot.backend.game.model.GameRuleTypes;


public class GameRuleTypeDto {
	
	Integer gameRuleTypeId;	

	String gameRuleType;
	
	public Integer getGameRuleTypeId() {
		return gameRuleTypeId;
	}

	public void setGameRuleTypeId(Integer gameRuleTypeId) {
		this.gameRuleTypeId = gameRuleTypeId;
	}

	public String getGameRuleType() {
		return gameRuleType;
	}

	public void setGameRuleType(String gameRuleType) {
		this.gameRuleType = gameRuleType;
	}
    
	public static GameRuleTypeDto populateGameRuleTypeDto(GameRuleTypes gameRuleTypes){
		GameRuleTypeDto gameRuleTypeDto=new GameRuleTypeDto();
		gameRuleTypeDto.setGameRuleTypeId(gameRuleTypes.getGameRuleTypeId());
		gameRuleTypeDto.setGameRuleType(gameRuleTypes.getGameRuleType());
		return gameRuleTypeDto;
		
	}
}
