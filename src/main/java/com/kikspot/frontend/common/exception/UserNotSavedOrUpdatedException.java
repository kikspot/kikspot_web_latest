package com.kikspot.frontend.common.exception;
/**
 * Created By Bhagya On october 19th,2015
 * This UserDefined Exception handles the condition when user not saved or updated
 */
public class UserNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Not Saved Or Updated ";
	}
}