package com.kikspot.frontend.common.exception;
/**
 * Created By Bhagya On october 19th,2015
 * This Exception handles if the mail was not sent to user
 */
public class MailNotSentException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Mail Not Sent Exception";
	}
}