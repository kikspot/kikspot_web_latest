package com.kikspot.frontend.common.exception;
/***
 * Created By Bhagya On october 19th,2015
 *	This Exception Handles the condition when the user not found
 */
public class UpdatedAppVersionNotMatchException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "New Version Available! Please update the version from App Store";
	}
}