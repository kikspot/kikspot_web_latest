/*package com.kikspot.frontend.common.utility;

import java.io.File;
import java.io.IOException;
import java.util.List;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

*//**
 * This program demonstrates a usage of the MultipartUtility class.
 * 
 * @author www.codejava.net
 * 
 *//*
public class MultipartFileUploader {

	public static void main(String[] args) throws JSONException {
		
			
		File uploadFile3=new File("C:/Users/kns09055/Downloads/funnycat.mp4");
		String charset = "UTF-8";
		File uploadFile1 = new File("C:/Users/Public/Pictures/Sample Pictures/Chrysanthemum.jpg");
		File uploadFile2 = new File("C:/Users/Public/Pictures/Stage/011.jpg");
		File uploadFile3 = new File("C:/Users/Public/Pictures/Stage/012.jpg");
		
		String requestURL =  "http://localhost:8080/kikspot/mobile/adduserlocationimages.do";

		try {
			MultipartUtility multipart = new MultipartUtility(requestURL,
					charset);

			multipart.addHeaderField("User-Agent", "CodeJava");
			multipart.addHeaderField("Test-Header", "Header-Value");
			
			

			JSONObject json = new JSONObject();
			json.put("userId","514");
			json.put("locationId","ChIJN1t_tDeuEmsRUsoyG83frY4");
			json.put("locationId","15");
			multipart.addJSON(json);
			
			multipart.addFilePart("images", uploadFile1);
			multipart.addFilePart("images", uploadFile2);
			multipart.addFilePart("images", uploadFile3);
			
			System.out.println("inside multipart");
            List<String> response = multipart.finish();
			System.out.println("SERVER REPLIED:");

			for (String line : response) {
				System.out.println(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			System.err.println(ex);
		}
	
	}
}
*/
package com.kikspot.frontend.common.utility;

import java.io.File;
import java.io.IOException;
import java.util.List;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

/**
 * This program demonstrates a usage of the MultipartUtility class.
 * 
 * @author www.codejava.net
 * 
 */
public class MultipartFileUploader {

	public static void main(String[] args) throws JSONException {
		String charset = "UTF-8";

			
		/*File uploadFile3=new File("C:/Users/kns09055/Downloads/funnycat.mp4");*/
		String requestURL =  "http://localhost:8080/kikspot/mobile/feedback.do";

		File uploadFile1 = new File("C:/Users/Public/Pictures/Sample Pictures/Chrysanthemum.jpg");
		/*File uploadFile2 = new File("C:/Users/Public/Pictures/Sample Pictures/Tulips.jpg");
		File uploadFile3 = new File("C:/Users/Public/Pictures/Sample Pictures/Jellyfish.jpg");*/
		File uploadFile2 = new File("C:/Users/kns09055/Downloads/WAPWON.COM_Funny_cat.mp4");
		
		try {
			MultipartUtility multipart = new MultipartUtility(requestURL,
					charset);

			multipart.addHeaderField("User-Agent", "CodeJava");
			multipart.addHeaderField("Test-Header", "Header-Value");
			
			

			JSONObject json = new JSONObject();

			
			json.put("feedbackMessage","i like this application");

			json.put("userId","4");
			json.put("locationId","ChIJN1t_tDeuEmsRUsoyG83frY4");

			multipart.addJSON(json);
			

			
			multipart.addFilePart("files", uploadFile1);
			multipart.addFilePart("files", uploadFile2);
			/*multipart.addFilePart("images", uploadFile3);*/
			

			System.out.println("inside multipart");
            List<String> response = multipart.finish();
			System.out.println("SERVER REPLIED:");

			for (String line : response) {
				System.out.println(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			System.err.println(ex);
		}
	}
}
