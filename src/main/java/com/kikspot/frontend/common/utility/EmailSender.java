package com.kikspot.frontend.common.utility;

import java.io.File;
import java.util.List;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.user.dto.KikspotUserDto;

public interface EmailSender{
	public void sendForgotPasswordMail(final KikspotUserDto userDto) throws MailNotSentException;
	public void sendRegistrationMail(final KikspotUserDto kikspotUserDto)throws MailNotSentException;
	public void feedbackInfoToAdmin(final KikspotUserDto userDto, final String feedbackMessage,final List<File> files,final Integer result,final String adminMail) throws Exception;
	public void sendEmailToUser(final KikspotUserDto kikspotUserDto) throws Exception;
	public void sendForgotPasswordMailforMobileUser(final KikspotUser kikspotUser)throws MailNotSentException;
	
}