package com.kikspot.frontend.common.utility;

import java.io.File;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;


import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.user.dto.KikspotUserDto;
/**
 * 
 *Created By Bhagya On October 19th,2015
 *Implementing the services for sending email to the users
 */
@Service("emailSender")
public class EmailSenderImpl implements EmailSender{
	private static Logger log=Logger.getLogger(EmailSenderImpl.class);
	
	/**
	 * handles sending mail. Autowired using the bean defined in name mailSender
	 */
	@Autowired
	private JavaMailSender mailSender;

	/**
	 * 
	 * holds the server url (used to build the forgot password link in mail body)
	 * 
	 */
	private String serverHost;
	/**
	 * Template engine. Autowired using the bean defined in the name velocityEngine
	 */
	@Autowired
	private VelocityEngine velocityEngine;

	
	public String getServerHost() {
		return serverHost;
	}


	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	
	
	private String imagesPath;
	
		
	
	

	public String getImagesPath() {
		return imagesPath;
	}


	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}


	/**
	 * Created By bhagya on october 19th, 2015
	 * Method for send mail to users for forgot mail
	 * @param userDto
	 */
	public void sendForgotPasswordMail(final KikspotUserDto userDto) throws MailNotSentException{
		log.info("inside  sendForgotPasswordMail()");
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@kikspot.com");
				message.setTo(userDto.getEmailId());
				message.setSubject("Kikspot Password Reset Link");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", userDto.getEmailId());
				if(userDto.getFirstName()!=null && userDto.getFirstName().trim().length()>0 && userDto.getLastName()!=null && userDto.getLastName().trim().length()>0){
					map.put("name", userDto.getFirstName()+" "+userDto.getLastName());
				}
				else{
					map.put("name", userDto.getUsername());
				}
				map.put("passwordToken", userDto.getPasswordToken());
				map.put("user", userDto);
				map.put("serverURL", serverHost);
				//System.out.println(map.toString());
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/kikspot/templates/forgotPassword.vm","UTF-8", map);
				message.setText(content,true);
        		//System.out.println(content);
				FileSystemResource res = new FileSystemResource(new File(
	        		       imagesPath+ "/logo_new.jpg"));
	        		message.addInline("logo_new", res);
	        		
	        	FileSystemResource res2=new FileSystemResource(new File(imagesPath+"/fb.png"));
	        	message.addInline("fb", res2);
	        	
	        	FileSystemResource res3=new FileSystemResource(new File(imagesPath+"/twitter.png"));
	        	message.addInline("twitter", res3);
        		
			}
		};		
		this.mailSender.send(preparator);
	}
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan  on DECEMBER 15, 2015
	 *  
	 *  Method to send ForgotPassword Code for Mobile User..
	 *  
	 * 
	 * 
	 * @param kikspotUser
	 * @throws MailNotSentException
	 */
	public void sendForgotPasswordMailforMobileUser(final KikspotUser kikspotUser)throws MailNotSentException{
		log.info("inside sendForgotPasswordMailforMobileUser() ");
		MimeMessagePreparator preparator=new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@kikspot.com");
				message.setTo(kikspotUser.getEmailId());
				message.setSubject("Forgot Password: Kikspot");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", kikspotUser.getEmailId());
				if(kikspotUser.getFirstName()!=null && kikspotUser.getFirstName().trim().length()>0 && kikspotUser.getLastName()!=null && kikspotUser.getLastName().trim().length()>0){
					map.put("name", kikspotUser.getFirstName()+" "+kikspotUser.getLastName());
				}
				else{
					map.put("name", kikspotUser.getUsername());
				}
				map.put("passwordToken",kikspotUser.getPasswordToken());
				map.put("serverURL", serverHost);
				//System.out.println(map.toString());
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/kikspot/templates/forgotPasswordMobile.vm","UTF-8", map);
				message.setText(content,true);
				FileSystemResource res = new FileSystemResource(new File(
	        		       imagesPath+ "/logo_new.jpg"));
	        		message.addInline("logo_new", res);
	        		
	        	FileSystemResource res2=new FileSystemResource(new File(imagesPath+"/fb.png"));
	        	message.addInline("fb", res2);
	        	
	        	FileSystemResource res3=new FileSystemResource(new File(imagesPath+"/twitter.png"));
	        	message.addInline("twitter", res3);
			}
		};
		this.mailSender.send(preparator);
	}
	
	
	
	
	
	/**
	 * 
	 * @param userDto
	 * @throws MailNotSentException
	 * 
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  
	 *  Method to Send Registration Email..
	 *  
	 * 
	 */
	public void sendRegistrationMail(final KikspotUserDto kikspotUserDto)throws MailNotSentException{
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@kikspot.com");
				message.setTo(kikspotUserDto.getEmailId());
				message.setSubject("Kikspot Successfully Registered");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", kikspotUserDto.getEmailId());
				if(kikspotUserDto.getFirstName()!=null && kikspotUserDto.getFirstName().trim().length()>0 && kikspotUserDto.getLastName()!=null && kikspotUserDto.getLastName().trim().length()>0){
					map.put("name", kikspotUserDto.getFirstName()+" "+kikspotUserDto.getLastName());
				}
				else{
					map.put("name", kikspotUserDto.getUsername());
				}
				map.put("user", kikspotUserDto);
				map.put("serverURL", serverHost);
				//System.out.println(map.toString());
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/kikspot/templates/registration.vm","UTF-8", map);
				message.setText(content,true);
				FileSystemResource res = new FileSystemResource(new File(
	        		       imagesPath+ "/logo_new.jpg"));
	        		message.addInline("logo_new", res);
	        		
	        	FileSystemResource res2=new FileSystemResource(new File(imagesPath+"/fb.png"));
	        	message.addInline("fb", res2);
	        	
	        	FileSystemResource res3=new FileSystemResource(new File(imagesPath+"/twitter.png"));
	        	message.addInline("twitter", res3);
	        	
				
				
				
			}
		};
		this.mailSender.send(preparator);
	}

	
	
	/**
	 * 
	 * Created by Firdous on 26-11-2015
	 * Method to send the email notification to the admin after getting the feedback from user
	 * 
	 * Modification Done by Bhagya On Dec 03rd,2015
	 * Setting the map variable of user
	 */
	public void feedbackInfoToAdmin(final KikspotUserDto user, final String feedbackMessage,final List<File> files,final Integer result,final String adminMail) throws MailNotSentException{
	        log.info("inside send feedbackInfoToAdmin()");
	        System.out.println("Inside send feeback to admin");
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
					Boolean guestUser=false;
					message.setFrom("no-reply@kikspot.com");
					message.setTo(adminMail);
					message.setSubject("Feedback Notifcation");
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("email",user.getEmailId());
					
					if(user.getFirstName()!=null && user.getFirstName().trim().length()>0 && user.getLastName()!=null && user.getLastName().trim().length()>0){
						map.put("name", user.getFirstName()+" "+user.getLastName());
					}
					else{
						map.put("name", user.getUsername());
						if(user.getUsername().contains("GUEST")){
							guestUser=true;
						}
					}
					map.put("message",feedbackMessage);
					map.put("user", user);
					map.put("serverURL", serverHost);
					String content="";
					if(guestUser==false){		        	
						content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/kikspot/templates/feedbackInfoToAdmin.vm","UTF-8", map);
					}
					else{
						content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/kikspot/templates/guestUserFeedbackInfoToAdmin.vm","UTF-8", map);
					}
					for(File file:files){
					   FileSystemResource resource=new FileSystemResource(file);
					   message.addAttachment(file.getName(), resource);
					}
				
					message.setText(content,true);
					
					FileSystemResource res = new FileSystemResource(new File(
		        		       imagesPath+ "/logo_new.jpg"));
		        	message.addInline("logo_new", res);
		        	
		        	FileSystemResource res2=new FileSystemResource(new File(imagesPath+"/fb.png"));
			        message.addInline("fb", res2);
			        	
			        FileSystemResource res3=new FileSystemResource(new File(imagesPath+"/twitter.png"));
			        message.addInline("twitter", res3);
				    log.info(content);
				   
	        	}
			};
			this.mailSender.send(preparator);
		}


		
		/**
		 * 
		 * Created by Firdous on 25-11-2015
		 * Method to send the email notification to the user after getting the feed back 
		 * 
		 * Modification done by bhagya on dec 03rd,2015
		 * 	Getting the kikspotuser dto from feedback service
		 * 	And Setting the map variable user
		 */
		@Override
		public void sendEmailToUser(final KikspotUserDto kikspotUserDto)  throws MailNotSentException {
			
			System.out.println("EmailSenderImpl -> sendEmail() ");
			log.info("EmailSenderImpl -> sendEmail");
			
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
					message.setFrom("no-reply@kikspot.com");
					message.setTo(kikspotUserDto.getEmailId());
					message.setSubject("Feedback Notifcation");
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("email",kikspotUserDto.getEmailId());
					if(kikspotUserDto.getFirstName()!=null && kikspotUserDto.getFirstName().trim().length()>0 && kikspotUserDto.getLastName()!=null && kikspotUserDto.getLastName().trim().length()>0){
						map.put("name", kikspotUserDto.getFirstName()+" "+kikspotUserDto.getLastName());
					}
					else{
						map.put("name", kikspotUserDto.getUsername());
					}
					map.put("user", kikspotUserDto);
					map.put("serverURL", serverHost);
					
		        		
					String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/kikspot/templates/feedbackNotification.vm","UTF-8", map);
					message.setText(content,true);
					
					FileSystemResource res = new FileSystemResource(new File(
		        		       imagesPath+ "/logo_new.jpg"));
		        	message.addInline("logo_new", res);
		        	
		        	FileSystemResource res2=new FileSystemResource(new File(imagesPath+"/fb.png"));
			        message.addInline("fb", res2);
			        	
			        FileSystemResource res3=new FileSystemResource(new File(imagesPath+"/twitter.png"));
			        message.addInline("twitter", res3);
					//log.info(content);
				}
			};
			if(null!=kikspotUserDto.getEmailId())
				this.mailSender.send(preparator);
			
		}
}