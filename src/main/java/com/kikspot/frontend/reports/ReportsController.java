package com.kikspot.frontend.reports;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRXlsDataSource;

@Controller
@RequestMapping("/reports")
public class ReportsController{
	
	private static Logger log=Logger.getLogger(ReportsController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	/**
	 * Created By Bhagya On January 25th,2016
	 * @param map
	 * @return
	 * 
	 * Method For Initiating the Users Sample Report
	 */
	@RequestMapping(value="/userssamplereport.do",method=RequestMethod.GET)
	public String initUsersSampleReport(Map<String,Object> map){
		log.info("inside initUsersSampleReport()");
		try{
			return "/reports/usersSampleReport";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message","Error While initiating Sample Report");
			return "error";
		}
	}
	/**
	 * Created By Bhagya on January 25th,2016
	 * @param map
	 * @return
	 * Method For Getting the List of users for Reports
	 */
	@RequestMapping(value="/userssamplereport.do",method=RequestMethod.POST)
	public String usersSampleReport(Map<String,Object>map,@RequestParam(value="export",required=false) String export){
		log.info("inside usersSampleReport()");
		try{
			ArrayList<KikspotUserDto> kikspotUserDtos=this.userService.getAllUsers(null, null, null, null, false);
			JRDataSource jrDataSource=new JRBeanCollectionDataSource(kikspotUserDtos);				
			map.put("datasource", jrDataSource);
			if(null!=export && export.equalsIgnoreCase("excel")){
				/*Map<String, KikspotUserDto> usersData=new LinkedHashMap<String, KikspotUserDto>();
				int i=0;
				for(KikspotUserDto user:kikspotUserDtos){					
					usersData.put("user"+i, user);
					i++;
				}
				map.put("users", usersData);				
				return "sampleUsersExcel";*/
				
				return "xlsReport";
			}
			else{
				
				return "usersSampleReport";
			}
		}
		catch(UserNotFoundException e){
			map.put("message", "Users Not Found");
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("message", "Error While Getting Users");
			return "error";
		}
		
	}
}