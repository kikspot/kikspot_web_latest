package com.kikspot;

import java.io.File;
import java.io.IOException;
import java.util.List;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class MultipartFileUploader {
	
	public static void main(String[] args) throws JSONException {
		String charset = "UTF-8";
		File uploadFile1 = new File("C:/Users/Public/Pictures/Sample Pictures/Desert.jpg");
		/*String requestURL = "http://54.225.230.190:7090/kikspot" + "/mobile/saveprofileimage.do";*/
		String requestURL = " http://localhost:7070/kikspot/mobile/saveprofileimage.do";
		try {
			MultipartUtility multipart = new MultipartUtility(requestURL,
					charset);

			multipart.addHeaderField("User-Agent", "CodeJava");
			multipart.addHeaderField("Test-Header", "Header-Value");
			JSONObject json = new JSONObject();
			json.accumulate("userId", "1");
			json.accumulate("preferredVenueType", "club");
			
			multipart.addJSON(json);
			multipart.addFilePart("profileImage", uploadFile1);
			

			List<String> response = multipart.finish();

			System.out.println("SERVER REPLIED:");

			for (String line : response) {
				System.out.println(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			System.err.println(ex);
		}
	}

}
