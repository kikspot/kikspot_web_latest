package com.kikspot.backend.game.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.exceptions.GamesNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.Game;
import com.kikspot.backend.game.model.GameLevels;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.game.model.UserGameCreds;


public interface GameDao {
	
	public Integer saveorUpdateGameRule(GameRules gameRule)throws Exception;
	public ArrayList<GameRules> getGameRulesofGame(Integer gameId,Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending,String locationFilter)throws GameRuleNotFoundException;
	public GameRules getGameRuleByRuleId(Integer id) throws GameRuleNotFoundException;
	public Integer deleteGameRule(GameRules gamerule) throws Exception;
	public Game getGameById(int i) throws Exception;
	public Integer saveorUpdateGameLevel(GameLevels gameLevel)throws Exception;
	public ArrayList<GameLevels> getGameLevelsofGame(Integer gameId,Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending)throws GameLevelNotFoundException;
	public GameLevels getGameLevelByGameLevelId(Integer gameLevelId) throws GameLevelNotFoundException;
	public Integer deleteGameLevel(GameLevels gameLevel) throws Exception;
	public ArrayList<GameRules> getGameRulesByLocationId(String locationId) throws GameRuleNotFoundException;
	public ArrayList<String> getDistinctLocationsOfGameRules() throws GameRuleNotFoundException;
	public ArrayList<GameRules> getGameRulesOfaLocationByLocationIdForUser(String locationId,Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException;
	public ArrayList<GameRules> getAllGameRules(Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException;
	public ArrayList<GameRules> getGameRulesForListOfLocations(List<String> locationAPIIds,Boolean isActive,Boolean checkExpiryDate,String sortBy,Integer userId) throws GameRuleNotFoundException;
	public GameRules getGameRuleByGameRuleId(Integer gameRuleId) throws GameRuleNotFoundException;
	public Set<String> getActiveGameRulesOfGivenLocations(Set<String> locationIds,Integer userId)throws Exception;
	public GameLevels getGameLevelByMinimumCreds(Integer minCreds) throws GameLevelNotFoundException;
	public ArrayList<UserGameCreds> getUsersGameCredsByGameLevel(String gameLevel) throws UserGameCredNotFoundException;
	public GameLevels getGameLevelByLevel(String level) throws GameLevelNotFoundException;
	public ArrayList<Game> getAllGames() throws GamesNotFoundException;
	public ArrayList<GameRules> getGameRulesByGameId(Integer gameId) throws GameRuleNotFoundException;
	public ArrayList<GameRules> gameRuleByRuleType(String ruleType) throws Exception;
	public GameRules getGameRuleByActivityType(String gameRuleType) throws Exception;
	public ArrayList<GameRules> getGlobalGameRuleforRatingCondition(String gameRuleType,Integer userId) throws Exception ;
	public ArrayList<GameRules> getGameRuleByLocationId(String locationId) throws Exception;
	public ArrayList<GameLevels> getGameLevelsByCreds(Integer creds) throws GameLevelNotFoundException;
	public ArrayList<GameLevels> getAllGameLevels() throws GameLevelNotFoundException;
	public ArrayList<GameRules> getGlobalGameRulesOfGame(Integer gameId,Integer userId) throws Exception;
	public ArrayList<GameRules> getGameRulesByLocationIdAndUserId(String locationId,Integer userId) throws GameRuleNotFoundException;
	public ArrayList<GameRules> getGameRulesByOnlyLocationId(String locationId) throws GameRuleNotFoundException;
	public ArrayList<UserGameCreds> getUsersGameCredsByGameRule(GameRules gameRule) throws UserGameCredNotFoundException;
	
}
