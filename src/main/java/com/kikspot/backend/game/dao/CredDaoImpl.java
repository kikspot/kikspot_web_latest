package com.kikspot.backend.game.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.exceptions.CredRuleNotFoundException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.CredRules;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.game.model.UpdatedCredsInSixMonths;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserType;



@Transactional
@Repository("credDao")
public class CredDaoImpl implements CredDao{
	
	private static Logger log=Logger.getLogger(CredDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * 
	 *  Created by Jeevan on November 25, 2015.
	 *  Method to save or update Cred Rules..
	 * 
	 * 
	 * @param credRule
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateCredRules(CredRules credRule)throws Exception{
		log.info("inside saveOrUpdateCredRules() ");
		sessionFactory.getCurrentSession().saveOrUpdate(credRule);
		sessionFactory.getCurrentSession().flush();
		return credRule.getCredRuleId();
	}
	
	
	/**
	 * 
	 * @param credType
	 * @return
	 * @throws CredRuleNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public CredRules getCredRulesByCredType(CredType credType)throws CredRuleNotFoundException{
		log.info("inside getCredRulesByCredType() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(CredRules.class);
		criteria.add(Restrictions.eq("credType", credType));
		
		ArrayList<CredRules> credRules=(ArrayList<CredRules>) criteria.list();
		if(!credRules.isEmpty()){
			return credRules.get(0);
		}
		else{
			throw new CredRuleNotFoundException();
		}
	}
	
	
	/**
	 * 
	 * @param userCredHistoy
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserCredHistory(UserCredHistory userCredHistory)throws Exception{
		log.info("inside saveorUpdateUserCredHistory() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userCredHistory);
		sessionFactory.getCurrentSession().flush();
		return userCredHistory.getUserCredHistoryId();
	}
	
	
	/**
	 * Created by Jeevan on November 27, 2015
	 * Method to getUserGameCredsByUser..
	 * 
	 * Creat
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserGameCreds getUserGameCredsByUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException{
		log.info("inside getUserGameCredsByUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds.get(0);
		}
		else{
			throw new UserGameCredNotFoundException();
		}		
	}
	
	
	
	/**
	 * 
	 * @param userGameCreds
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserGameCred(UserGameCreds userGameCreds)throws Exception{
		log.info("inside saveorUpdateUserGameCred() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userGameCreds);
		sessionFactory.getCurrentSession().flush();
		return userGameCreds.getUserGameId();
	}
	


	
	/**
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserGameCreds getUserGameCredsOfUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException{
		log.info("inside getUserGameCredsOfUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class)
						.add(Restrictions.eq("kikspotUser", kikspotUser));
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds.get(0);
		}
		else{
			throw new UserGameCredNotFoundException();
		}		
	}
	
	
	
	/**
	 * 
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getTopFiveUserGameCreds()throws UserGameCredNotFoundException{
		log.info("inside getTopFiveUserGameCreds() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.createCriteria("kikspotUser").add(Restrictions.ne("userType",UserType.BYPASS_USER));
		criteria.addOrder(Order.desc("creds"));
		criteria.setMaxResults(5);
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds;
		}
		else{
			throw new UserGameCredNotFoundException();
		}		
	}
	
	
	
	/**
	 * 
	 * @param kikspotUsers
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getGameCredsofUsers(ArrayList<KikspotUser> kikspotUsers) throws Exception{
		log.info("inside getGameCredsofUsers() ");
		ArrayList<UserGameCreds> userGameCreds=new ArrayList<UserGameCreds>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.add(Restrictions.in("kikspotUser",kikspotUsers));
		userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds;
		}
		else{
			throw new UserGameCredNotFoundException();
		}
	}
	
	
	
	
	
	/**
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserCredHistoryNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserCredHistory getMostRecentUserCredHistoryofUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException{
		log.info("inside getMostRecentUserCredHistoryofUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserCredHistory.class)
				.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.addOrder(Order.desc("userCredHistoryId"));
		ArrayList<UserCredHistory> userCredHistories=(ArrayList<UserCredHistory>) criteria.list();
		if(!userCredHistories.isEmpty()){
			if(userCredHistories.size()>1){
				return userCredHistories.get(userCredHistories.size()-2);
			}
			return userCredHistories.get(0);
		}
		else{
			throw new UserCredHistoryNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On January  20th,2016
	 * @param kikspotRewards
	 * @return
	 * @throws Exception
	 * 
	 * Method For Save Or Update the Kikspot Rewards
	 */
	public Integer saveOrUpdateKikspotRewards(KikspotRewards kikspotRewards) throws Exception{
		log.info("inside saveOrUpdateKikspotRewards()");
		sessionFactory.getCurrentSession().saveOrUpdate(kikspotRewards);
		sessionFactory.getCurrentSession().flush();
		return kikspotRewards.getRewardId();
	}
	/**
	 * Created By Bhagya On january 20th,2016
	 * @param rewardId
	 * @return
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Method For Getting the kikspot Reward By RewardId
	 */
	@SuppressWarnings("unchecked")
	public KikspotRewards getKikspotRewardByRewardId(Integer rewardId) throws KikspotRewardNotFoundException{
		log.info("inside getKikspotRewardByRewardId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRewards.class);
		criteria.add(Restrictions.eq("rewardId", rewardId));
		ArrayList<KikspotRewards> kikspotRewards=(ArrayList<KikspotRewards>) criteria.list();
		if(!kikspotRewards.isEmpty()){
			return kikspotRewards.get(0);
		}
		else{
			throw new KikspotRewardNotFoundException();
		}
	}
	/**
	 * Created By bhagya On January 20th,2016
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @param ascending
	 * @return
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Method For getting the List Of all Kikspot Rewards
	 * And apply the restrictions For sorting, searching and Pagination
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotRewards> getAllKikspotRewards(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRewardNotFoundException{
		log.info("inside getAllKikspotRewards()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRewards.class);
		if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("rewardType", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("reward", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("level", searchBy,
					MatchMode.ANYWHERE));
			
			criteria.add(disjunction);
		}
		if(null!=sortBy){
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));
			}
			else{
				criteria.addOrder(Order.desc(sortBy));
			}				
		}
		Integer totalRewards=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<KikspotRewards> kikspotRewards=(ArrayList<KikspotRewards>) criteria.list();
		if(!kikspotRewards.isEmpty()){
			kikspotRewards.get(0).setTotalRewards(totalRewards);
			return kikspotRewards;
		}
		else{
			throw new KikspotRewardNotFoundException();
		}
		
		
	}
	/**
	 * Created By bhagya On January 21st,2016
	 * @param kikspotReward
	 * @return
	 * 
	 * Method For Deleting The kikspot reward
	 */
	
	public Integer deleteKikspotReward(KikspotRewards  kikspotReward) throws Exception{
		log.info("inside deleteKikspotReward()");
		sessionFactory.getCurrentSession().delete(kikspotReward);
		sessionFactory.getCurrentSession().flush();
		return kikspotReward.getRewardId();
	}
	/**
	 * Created By Bhagya On January 22nd,2016
	 * @param level
	 * @return
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Method For Getting the Kikspot Rewards By Level of the reward 
	 * 	Get the rewards which are active and satisfying the level criteria
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotRewards> getKikspotRewardsByLevel(String level) throws KikspotRewardNotFoundException{
		log.info("inside getKikspotRewardsByLevel()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotRewards.class);
		
		criteria.add(Restrictions.and(Restrictions.eq("level", level),Restrictions.eq("isActive", true)));
		ArrayList<KikspotRewards> kikspotRewards=(ArrayList<KikspotRewards>) criteria.list();
		if(!kikspotRewards.isEmpty()){
			return kikspotRewards;
		}
		else{
			throw new KikspotRewardNotFoundException();
		}
		
	}
    
	
	/**
	 * 
	 * 
	 * Created by Firdous on 8th march 2016
	 * method to get game details of all the users in descending order of their creds
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getUserGameCredsOfAllUser() throws UserGameCredNotFoundException {
		log.info("inside  getUserGameCredsOfAllUser()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.createCriteria("kikspotUser").add(Restrictions.eq("isActive",true));
		
		criteria.addOrder(Order.desc("creds"));
		ArrayList<UserGameCreds> userGameCreds = (ArrayList<UserGameCreds>) criteria.list();		
		if(!userGameCreds.isEmpty()){			
			return userGameCreds;
		}
		else{
			throw new UserGameCredNotFoundException();
		}
	}
    
	
	/**
	 * Created by Firdous on 8th march 2016
	 * method to get game details friends  of users in descending order of their creds
	 * 
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getGameCredsofUsersInOrder(ArrayList<KikspotUser> referredUsers) throws Exception {
		ArrayList<UserGameCreds> userGameCreds=new ArrayList<UserGameCreds>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.add(Restrictions.in("kikspotUser",referredUsers));
		criteria.addOrder(Order.desc("creds"));
		userGameCreds=(ArrayList<UserGameCreds>)criteria.list();
		return userGameCreds;
	}

    /**
     * Added by Firdous on 10th march 2015
     * Method to get the game creds of users
     * 
     */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getUserGameCredsOfAllUsers(ArrayList<KikspotUser> kispotUsers) throws UserGameCredNotFoundException {
		log.info("inside getUserGameCredsOfAllUsers( )");
		ArrayList<UserGameCreds> userGameCreds=new ArrayList<UserGameCreds>();
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.add(Restrictions.in("kikspotUser",kispotUsers));
		criteria.addOrder(Order.desc("creds"));
	    userGameCreds =(ArrayList<UserGameCreds>)criteria.list();
	    if(!userGameCreds.isEmpty()){
		    return userGameCreds;
	    }
	    else{
	    	throw new UserGameCredNotFoundException();
	    }
	} 

   /**
    * Added by Firdous on 11th march
    * Method to get the cred history of users whose creds updated in current week
    * 
    */
	@SuppressWarnings("unchecked")
	public ArrayList<UserCredHistory> getCredsOfUsersInCurrentWeek(Date time) throws UserCredHistoryNotFoundException {
		log.info("inside getCredsOfUsersInCurrentWeek( )");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserCredHistory.class);
		criteria.createCriteria("kikspotUser").add(Restrictions.eq("isActive",true));
		
		criteria.add(Restrictions.ge("date",time));
		System.out.println("success");
		ArrayList<UserCredHistory> usersCredHistory=(ArrayList<UserCredHistory>)criteria.list();
		if(!usersCredHistory.isEmpty()){
			return usersCredHistory;
		}
		else{
			throw new UserCredHistoryNotFoundException();
		}
	}


	@SuppressWarnings("unchecked")
	public ArrayList<UserCredHistory> getCredsOfUsersInCurrentWeekOfFriends(Date time,
		ArrayList<KikspotUser> referredUsers) throws UserCredHistoryNotFoundException {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserCredHistory.class);
		criteria.add(Restrictions.in("kikspotUser",referredUsers));
		criteria.add(Restrictions.ge("date",time));
		System.out.println("success");
		ArrayList<UserCredHistory> usersCredHistory=(ArrayList<UserCredHistory>)criteria.list();
		if(!usersCredHistory.isEmpty()){
		  return usersCredHistory;
		}
		else{
			throw new UserCredHistoryNotFoundException();
		}
	}


	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getGameCredsofAllUsers() throws Exception {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>)criteria.list();
		return userGameCreds;
	}



	public void saveOrUpdateCreds(UpdatedCredsInSixMonths updateCreds) throws Exception {
		log.info("inside saveOrUpdateCreds() ");
		sessionFactory.getCurrentSession().saveOrUpdate(updateCreds);
		sessionFactory.getCurrentSession().flush();
	}


	@SuppressWarnings("unchecked")
	public UpdatedCredsInSixMonths getCredsOfUserInSixMonthsPeriod(KikspotUser user) throws Exception {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UpdatedCredsInSixMonths.class);
		criteria.add(Restrictions.eq("kikspotUser",user));
		ArrayList<UpdatedCredsInSixMonths> updatedCredsInSixMonths=(ArrayList<UpdatedCredsInSixMonths>) criteria.list();
		if(!updatedCredsInSixMonths.isEmpty()){
			return updatedCredsInSixMonths.get(0);
		}
		else{
			throw new UserGameCredNotFoundException();
		}
			
		
	}
	/**
	 * Created By Bhagya on April 01st, 2019
	 * 
	 * Method for to get the distinct user game creds of all users
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<Integer> getDistinctUserGameCredsOfAllUser() {
		log.info("inside  getDistinctUserGameCredsOfAllUser()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.createCriteria("kikspotUser").add(Restrictions.eq("isActive",true));
		
		ProjectionList projectionList=Projections.projectionList();
		projectionList.add(Projections.property("creds"));
		criteria.setProjection(Projections.distinct(projectionList));
		criteria.addOrder(Order.desc("creds"));
		List<Integer> creds= criteria.list();
		ArrayList<Integer> credsList=new ArrayList<Integer>();
		for(Integer cred:creds){
			credsList.add(cred);
		}
		
		return credsList;
	}
}



