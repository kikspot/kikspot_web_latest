package com.kikspot.backend.game.dao;

import java.util.ArrayList;
import java.util.Date;

import com.kikspot.backend.exceptions.CredRuleNotFoundException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.CredRules;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.game.model.UpdatedCredsInSixMonths;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.user.model.KikspotUser;

public interface CredDao {
	
	public Integer saveorUpdateCredRules(CredRules credRule)throws Exception;
	public CredRules getCredRulesByCredType(CredType credType)throws CredRuleNotFoundException;
	public Integer saveorUpdateUserCredHistory(UserCredHistory userCredHistory)throws Exception;
	public UserGameCreds getUserGameCredsByUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException;
	public Integer saveorUpdateUserGameCred(UserGameCreds userGameCreds)throws Exception;
	public UserGameCreds getUserGameCredsOfUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException;
	public ArrayList<UserGameCreds> getTopFiveUserGameCreds()throws UserGameCredNotFoundException;
	public ArrayList<UserGameCreds> getGameCredsofUsers(ArrayList<KikspotUser> kikspotUsers) throws Exception;
	public UserCredHistory getMostRecentUserCredHistoryofUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException;
	public Integer saveOrUpdateKikspotRewards(KikspotRewards kikspotRewards) throws Exception;
	public KikspotRewards getKikspotRewardByRewardId(Integer rewardId) throws KikspotRewardNotFoundException;
	public ArrayList<KikspotRewards> getAllKikspotRewards(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRewardNotFoundException;
	public Integer deleteKikspotReward(KikspotRewards  kikspotReward) throws Exception;
	public ArrayList<KikspotRewards> getKikspotRewardsByLevel(String level) throws KikspotRewardNotFoundException;
	public ArrayList<UserGameCreds> getUserGameCredsOfAllUser() throws UserGameCredNotFoundException;
	public ArrayList<UserGameCreds> getGameCredsofUsersInOrder(ArrayList<KikspotUser> referredUsers) throws Exception;
	public ArrayList<UserGameCreds> getUserGameCredsOfAllUsers(ArrayList<KikspotUser> KispotUsers) throws UserGameCredNotFoundException;
	public ArrayList<UserCredHistory> getCredsOfUsersInCurrentWeek(Date time) throws UserCredHistoryNotFoundException;
	public ArrayList<UserCredHistory> getCredsOfUsersInCurrentWeekOfFriends(Date time,
			ArrayList<KikspotUser> referredUsers) throws UserCredHistoryNotFoundException;
	public ArrayList<UserGameCreds> getGameCredsofAllUsers() throws Exception;
	public void saveOrUpdateCreds(UpdatedCredsInSixMonths updateCreds) throws Exception;
	public UpdatedCredsInSixMonths getCredsOfUserInSixMonthsPeriod(KikspotUser user) throws Exception;
	public ArrayList<Integer> getDistinctUserGameCredsOfAllUser() throws UserGameCredNotFoundException;

}
