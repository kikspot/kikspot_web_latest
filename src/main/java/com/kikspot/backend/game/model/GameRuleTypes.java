package com.kikspot.backend.game.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * 
 * @author kns09055
 * Model class which has types of rules 
 * 
 *
 */
@Entity
@Table(name="game_rule_types")
public class GameRuleTypes implements Serializable{

	
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue
@Column(name="game_rule_type_id")
Integer gameRuleTypeId;	

@Column(name="game_rule_type")
String gameRuleType;

public String getGameRuleType() {
	return gameRuleType;
}

public void setGameRuleType(String gameRuleType) {
	this.gameRuleType = gameRuleType;
}

public Integer getGameRuleTypeId() {
	return gameRuleTypeId;
}

public void setGameRuleTypeId(Integer gameRuleTypeId) {
	this.gameRuleTypeId = gameRuleTypeId;
}



}
