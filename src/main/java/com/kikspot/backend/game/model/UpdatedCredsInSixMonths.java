package com.kikspot.backend.game.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kikspot.backend.user.model.KikspotUser;


/**
 * 
 * 
 * @author kns09055
 * Model class which has updated creds of users
 *
 */
@Entity
@Table(name="updated_creds_in_six_months")
public class UpdatedCredsInSixMonths {
	
	@Id
	@GeneratedValue
	@Column(name="user_credId")
	private int userCredId;
	
	@Column(name="creds")
	private int creds;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;




	public int getUserCredId() {
		return userCredId;
	}

	public void setUserCredId(int userCredId) {
		this.userCredId = userCredId;
	}

	public int getCreds() {
		return creds;
	}

	public void setCreds(int creds) {
		this.creds = creds;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}
	

}
