package com.kikspot.backend.game.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.exolab.castor.types.DateTime;

import com.kikspot.backend.user.model.KikspotUser;


/**
 * 
 * @author KNS-ACCONTS
 *
 * 
 *  Created by Jeevan on November 20, 2015
 *  
 *  Model for Game Rules...
 *  
 *
 */
@Entity
@Table(name="game_rules")
public class GameRules implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="game_rule_id")
	private Integer gameRuleId;
	
	
	@ManyToOne
	@JoinColumn(name="game_id")
	private Game game;
	
	
	@Column(name="rule")
	private String rule;
	
	
	@Column(name="rule_limit")
	private Integer ruleLimit;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="expiry_date")
	private Date expiryDate;
	/*
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="expiry_date")
	private DateTime expiryDate;*/
	
	@Column(name="creds")
	private Integer creds;
	
	@Column(name="gameRuleType")
	private String gameRuleType;
	
	@Transient
	private Integer totalRules;
	
	
	



	/**
	 * Added isActive, by bhagya On December 11th,2015
	 */
	@Column(name="is_active")
	private Boolean isActive;
	
	
	@Column(name="is_visible")
	private Boolean visible;
	

	/***
	 * Added locationId and location Name By bhagya On december 31st,2015
	*/
	@Column(name="location_id")
	private String locationId;
	
	@Column(name="location_name")
	private String locationName;
	
	/** Added latitide and longitude by bhagya on jan 12th,2016
	 * For Implementing CLOSE BY */
	
	@Column(name="latitude")
	private Double latitude;
	
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="longitude")
	private Double longitude;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="open_date")
	private Date openDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_time")
	private Date startTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_time")
	private Date endTime;
	
	

	



	public Date getStartTime() {
		return startTime;
	}


	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}


	public Date getEndTime() {
		return endTime;
	}


	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
	public Date getOpenDate() {
		return openDate;
	}


	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}


	public Integer getGameRuleId() {
		return gameRuleId;
	}


	public void setGameRuleId(Integer gameRuleId) {
		this.gameRuleId = gameRuleId;
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public String getRule() {
		return rule;
	}


	public void setRule(String rule) {
		this.rule = rule;
	}


	public Integer getRuleLimit() {
		return ruleLimit;
	}


	public void setRuleLimit(Integer ruleLimit) {
		this.ruleLimit = ruleLimit;
	}


	public Date getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Integer getCreds() {
		return creds;
	}


	/*public DateTime getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(DateTime expiryDate) {
		this.expiryDate = expiryDate;
	}
*/

	public void setCreds(Integer creds) {
		this.creds = creds;
	}


	public Integer getTotalRules() {
		return totalRules;
	}


	public void setTotalRules(Integer totalRules) {
		this.totalRules = totalRules;
	}
   
	
	public Boolean getVisible() {
		return visible;
	}


	public void setVisible(Boolean visible) {
		this.visible = visible;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public String getLocationId() {
		return locationId;
	}


	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public String getGameRuleType() {
		return gameRuleType;
	}


	public void setGameRuleType(String gameRuleType) {
		this.gameRuleType = gameRuleType;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	
	
	
	
}
