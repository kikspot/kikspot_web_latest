package com.kikspot.backend.game.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * Created by Jeevan on November 20, 2015
 * @author KNS-ACCONTS
 * 
 *  Model for Games..
 * 
 */

@Entity
@Table(name="game")
public class Game implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="game_id")
	private Integer gameId;
	
	
	@Column(name="game_name")
	private String gameName;


	public Integer getGameId() {
		return gameId;
	}


	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}


	public String getGameName() {
		return gameName;
	}


	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	
	
	

}
