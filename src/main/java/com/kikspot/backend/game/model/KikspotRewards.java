package com.kikspot.backend.game.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * Created By Bhagya on January 20th, 2016
 *	Model For Kikspot Rewards
 */
@Entity
@Table(name="kikspot_rewards")

public class KikspotRewards{
	
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="reward_id")
	private Integer rewardId;
	
	@Column(name="reward_type")
	private String rewardType;
	
	@Column(name="reward")
	private String reward;
	
	@Column(name="creds")
	private Integer creds;
	
	@Column(name="level")
	private String level;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="reward_value")
	private String rewardValue;
	
	@Transient
	private Integer totalRewards;
	/***
	 * Added locationId and location Name By bhagya On August14,2018
	*/
	@Column(name="location_id")
	private String locationId;
	
	@Column(name="location_name")
	private String locationName;
	
	public Integer getRewardId() {
		return rewardId;
	}

	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}

	public String getRewardType() {
		return rewardType;
	}

	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public String getReward() {
		return reward;
	}

	public void setReward(String reward) {
		this.reward = reward;
	}

	public Integer getCreds() {
		return creds;
	}

	public void setCreds(Integer creds) {
		this.creds = creds;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getTotalRewards() {
		return totalRewards;
	}

	public void setTotalRewards(Integer totalRewards) {
		this.totalRewards = totalRewards;
	}

	public String getRewardValue() {
		return rewardValue;
	}

	public void setRewardValue(String rewardValue) {
		this.rewardValue = rewardValue;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	
}