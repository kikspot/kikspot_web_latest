package com.kikspot.backend.game.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.kikspot.backend.user.model.KikspotUser;



/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on Novemeber 26, 2015
 *  
 *  Method to save UserCredHistory..,
 *  
 *  All User Creds needs to be saved,,,
 *
 */
@Entity
@Table(name="user_cred_history")
public class UserCredHistory implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue
	@Column(name="user_cred_history_id")
	private Integer userCredHistoryId;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;

	@Column(name="cred")
	private Integer cred;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date")
	private Date date;

	public Integer getUserCredHistoryId() {
		return userCredHistoryId;
	}

	public void setUserCredHistoryId(Integer userCredHistoryId) {
		this.userCredHistoryId = userCredHistoryId;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}

	public Integer getCred() {
		return cred;
	}

	public void setCred(Integer cred) {
		this.cred = cred;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
	
}
