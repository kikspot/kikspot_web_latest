package com.kikspot.backend.game.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.kikspot.backend.user.model.KikspotUser;

/**
 * 
 * 
 * @author kns09055
 * Model class which store user added game rules
 *
 */
@Entity
@Table(name="user_added_game_rules")
public class UserAddedGameRules {

	
	
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="user_game_rule_id")
	private Integer userGameRuleId;
	
	
	@ManyToOne
	@JoinColumn(name="game_id")
	private Game game;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;


	@Column(name="rule")
	private String rule;
	
	
	
	@Column(name="creds")
	private Integer creds;
	
	@Column(name="gameRuleType")
	private String gameRuleType;
	
	@Transient
	private Integer totalRules;
	
	
	/***
	 * Added locationId and location Name By bhagya On december 31st,2015
	*/
	@Column(name="location_id")
	private String locationId;
	
	@Column(name="location_name")
	private String locationName;
	
	/** Added latitide and longitude by bhagya on jan 12th,2016
	 * For Implementing CLOSE BY */
	
	@Column(name="latitude")
	private Double latitude;
	
	@Column(name="longitude")
	private Double longitude;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="open_date")
	private Date openDate;
	
	public Date getOpenDate() {
		return openDate;
	}


	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}


	

	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public String getRule() {
		return rule;
	}


	public void setRule(String rule) {
		this.rule = rule;
	}


	


	public Integer getCreds() {
		return creds;
	}


	/*public DateTime getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(DateTime expiryDate) {
		this.expiryDate = expiryDate;
	}
*/

	public void setCreds(Integer creds) {
		this.creds = creds;
	}


	public Integer getTotalRules() {
		return totalRules;
	}


	public void setTotalRules(Integer totalRules) {
		this.totalRules = totalRules;
	}



	public Integer getUserGameRuleId() {
		return userGameRuleId;
	}


	public void setUserGameRuleId(Integer userGameRuleId) {
		this.userGameRuleId = userGameRuleId;
	}


	public String getLocationId() {
		return locationId;
	}


	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public String getGameRuleType() {
		return gameRuleType;
	}


	public void setGameRuleType(String gameRuleType) {
		this.gameRuleType = gameRuleType;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}
}
