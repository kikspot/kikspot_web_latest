package com.kikspot.backend.game.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.kikspot.backend.user.model.KikspotUser;


/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on November 20, 2015
 *  
 *  Model for User Game Creds
 *
 */
@Entity
@Table(name="user_game_creds")
public class UserGameCreds implements Serializable{

	public CredType getCredType() {
		return credType;
	}


	public void setCredType(CredType credType) {
		this.credType = credType;
	}


	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="user_game_id")
	private Integer userGameId;
	
	
	@OneToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	@OneToOne
	@JoinColumn(name="game_id")
	private Game game;
	
	//may need to change to mapping..
	@Column(name="game_level")
	private String level;
	
	@Column(name="creds")
	private Integer creds;
	
	@Enumerated(EnumType.STRING)
	@Column(name="cred_type")
	private CredType credType;
	
	/** Added Game Rules By Bhagya On Jan 07th,2016*/
	@ManyToOne
	@JoinColumn(name="game_rule_id")
	private GameRules gameRule;
	
	
	@Transient
	private Integer totalUserGames;


	public Integer getUserGameId() {
		return userGameId;
	}


	public void setUserGameId(Integer userGameId) {
		this.userGameId = userGameId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public String getLevel() {
		return level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	public Integer getCreds() {
		return creds;
	}


	public void setCreds(Integer creds) {
		this.creds = creds;
	}


	public Integer getTotalUserGames() {
		return totalUserGames;
	}


	public void setTotalUserGames(Integer totalUserGames) {
		this.totalUserGames = totalUserGames;
	}


	public GameRules getGameRule() {
		return gameRule;
	}


	public void setGameRule(GameRules gameRule) {
		this.gameRule = gameRule;
	}
	
}
