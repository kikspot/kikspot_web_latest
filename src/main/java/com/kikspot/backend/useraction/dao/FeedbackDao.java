package com.kikspot.backend.useraction.dao;

import java.util.ArrayList;
import java.util.List;

import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.backend.useraction.model.FeedbackDefaultMail;

public interface FeedbackDao {
	
	 public Integer saveFeedBack(Feedback feedback) throws Exception;
     public Feedback getFeedbackById(Integer feedbackId) throws Exception;
     public Integer getTotalFeedbacks() throws Exception;
     public Integer deleteFeedback(Feedback feedback) throws Exception;
	 public ArrayList<Feedback> getAllFeedbacks(Integer pageNo, Integer pageSize, String sortBy, String searchBy, Boolean ascending, Integer id) throws Exception;
	 public ArrayList<KikspotUser> getFeedbackUsers() throws Exception;
	 public Feedback getFeedbackByUserId(Integer id) throws Exception;
	 public Integer saveorUpdateFeedbackDefaultMail(FeedbackDefaultMail feedbackDefaultMail)throws Exception;
	 
	 public FeedbackDefaultMail getFeedbackDefaultMail()throws Exception;
	public void deleteFeedback(Integer id) throws FeedBackNotFoundException;

}
