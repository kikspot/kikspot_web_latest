package com.kikspot.backend.useraction.dao;

/**
 * 
 * 
 * Created by Firdous on 23-11-2015
 * Dao to handle the  feedbacks of users
 * 
 * 
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.backend.useraction.model.FeedbackDefaultMail;
import com.kikspot.frontend.common.exception.UserNotFoundException;


@Repository("feedbackDao")
@Transactional
public class FeedbackDaoImpl implements FeedbackDao {


	private static Logger log=Logger.getLogger(FeedbackDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * 
	 * method to save the feedback  of a user inside the database
	 * 
	 */
	@Override
	public Integer saveFeedBack(Feedback feedback) throws Exception {		
		log.info("inside save feedback()");		
		sessionFactory.getCurrentSession().saveOrUpdate(feedback);			
		this.sessionFactory.getCurrentSession().flush();		
		return feedback.getFeedbackId();	
	}

	
	/**
	 * 
	 * 
	 * method to get the total number of feedbacks send by users
	 * 
	 * 
	 */
	
	public Integer getTotalFeedbacks() throws Exception{		
		log.info("inside getTotalFeedbacks()");		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Feedback.class);		
		criteria.setProjection(Projections.count("feedbackId")).list();		
		Integer totalFeedbacks=Integer.valueOf (criteria.list().get(0).toString());		
		return totalFeedbacks;
	}
	
	

    /**
     * 
     * created by Firdous on 26-11-2015
     * method to delete feedback of a user
     * 
     * 
     */

	@Override
	public Integer deleteFeedback(Feedback feedback) throws Exception {	
		log.info("inside delete feedback()");
		sessionFactory.getCurrentSession().delete(feedback);		
		sessionFactory.getCurrentSession().flush();	
		return 1;
	}


    /**
     * 
     * Added by firdous on 09-12-2015
     * method to get the feedback of a user by accepting feedback id
     * 
     */
	@Override
	@SuppressWarnings("unchecked")
	public Feedback getFeedbackById(Integer feedbackId) throws Exception {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Feedback.class);
		criteria.add(Restrictions.eq("feedbackId", feedbackId));
		ArrayList<Feedback> feedbacks = (ArrayList<Feedback>) criteria.list();
		if(!feedbacks.isEmpty()){
			return feedbacks.get(0);
		}
		else{
			throw new FeedBackNotFoundException();
		}
	}

   /**
    * 
    * added by Firdous on 11-12-2015
    * method to get all the feedbacks 
    * 
    */

	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<Feedback> getAllFeedbacks(Integer pageNo, Integer pageSize, String sortBy, String searchBy,
			Boolean ascending,Integer id) throws Exception {
		
		log.info("inside getAllFeedbacks()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Feedback.class).createAlias("kikspotUser", "kikspotUser");
		      
			if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("feedbackMessage", searchBy,
					MatchMode.ANYWHERE));
			
			
			criteria.add(disjunction);
			}
			if(null!=sortBy){
				if(ascending){
					criteria.addOrder(Order.asc(sortBy));
				}
				else{
				criteria.addOrder(Order.desc(sortBy));
				}	
				
				
			}
		
			if(null!=id){
				 criteria.add(Restrictions.eq("kikspotUser.userId", id));
			}
		Integer totalFeedbacks=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<Feedback> feedbacks = (ArrayList<Feedback>) criteria.list();
		
		if(!feedbacks.isEmpty()){
			feedbacks.get(0).setTotalFeedbacks(totalFeedbacks);
			return feedbacks;
		}
		else{
			throw new FeedBackNotFoundException();
		}
	}


    /**
     * 
     * Added by Firdous 
     * method to get all the users who has sent the feedbacks
     */
	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getFeedbackUsers() throws Exception {
		log.info("inside getFeedbackUsers()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Feedback.class);
		criteria.setProjection(Projections.distinct(Projections.property("kikspotUser")));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>)criteria.list() ;
		if(!users.isEmpty()){
		return users;
		}
		else {
			throw new UserNotFoundException();
		}	
	}
    /**
     * 
     * method to get the feedback by accepting userId
     * 
     */

	@Override
	@SuppressWarnings("unchecked")
	public Feedback getFeedbackByUserId(Integer id) throws Exception {
		log.info("inside getFeedbackByUserId()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Feedback.class);
		criteria.add(Restrictions.eq("kikspotUser.userId", id));
		ArrayList<Feedback> feedbacks = (ArrayList<Feedback>) criteria.list();
		if(!feedbacks.isEmpty()){
			return feedbacks.get(0);
		}
		else{
			throw new FeedBackNotFoundException();
		}
	}
	
	
	/**
	 * 
	 * @param feedbackDefaultMail
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateFeedbackDefaultMail(FeedbackDefaultMail feedbackDefaultMail)throws Exception{
		log.info("inside saveorUpdateFeedbackDefaultMail() ");
		sessionFactory.getCurrentSession().saveOrUpdate(feedbackDefaultMail);
		sessionFactory.getCurrentSession().flush();
		return feedbackDefaultMail.getFeedbackDefaultMailId();
		
	}
	
	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public FeedbackDefaultMail getFeedbackDefaultMail()throws Exception{
		ArrayList<FeedbackDefaultMail> feedbackDefaultMails=new ArrayList<FeedbackDefaultMail>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(FeedbackDefaultMail.class);
		feedbackDefaultMails=(ArrayList<FeedbackDefaultMail>) criteria.list();
		return feedbackDefaultMails.get(0);		
	}


	@SuppressWarnings("unchecked")
	public void deleteFeedback(Integer id) throws FeedBackNotFoundException{
		ArrayList<Feedback> feedbacks=(ArrayList<Feedback>)sessionFactory.getCurrentSession().createCriteria(Feedback.class)  
        .add(Restrictions.eq("kikspotUser.userId", id));	
		sessionFactory.getCurrentSession().delete(feedbacks);	
	}
	
	
}
