package com.kikspot.backend.useraction.model;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="feedback_default_mail")
public class FeedbackDefaultMail implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="feedback_default_mail_id")
	private Integer feedbackDefaultMailId;
	
	
	@Column(name="default_email")
	private String defaultEmail;


	public Integer getFeedbackDefaultMailId() {
		return feedbackDefaultMailId;
	}


	public void setFeedbackDefaultMailId(Integer feedbackDefaultMailId) {
		this.feedbackDefaultMailId = feedbackDefaultMailId;
	}


	public String getDefaultEmail() {
		return defaultEmail;
	}


	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}
	
	
	

}
