package com.kikspot.backend.exceptions;

/**
 * Created By Bhagya on may 28th, 2018
 *  User Defined Exception for when Venue Score Data not found
 */


public class VenueScoreDataNotFoundException extends Exception{
	

	private static final long serialVersionUID = 1L;
	private String message;
	
	public VenueScoreDataNotFoundException() {
			message="Venue Score Data Not Found";
	}
	
	
	public VenueScoreDataNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		
		return message;
	}

}
