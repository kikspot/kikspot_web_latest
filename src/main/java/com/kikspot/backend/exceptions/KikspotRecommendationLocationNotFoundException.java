package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On December 18th,2015
 *	User Defined Exception  for when the location was not found at KikspotRecommendation location 
 */
public class KikspotRecommendationLocationNotFoundException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Recommendation Location Not Found ";
	}
}