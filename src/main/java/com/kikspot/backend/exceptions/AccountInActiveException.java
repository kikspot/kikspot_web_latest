package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On December 10th,2015
 *	UserDefined Exception, When the User Account is InActive
 */
public class AccountInActiveException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	public String toString() {
		return "User Account is InActive";
	}
}