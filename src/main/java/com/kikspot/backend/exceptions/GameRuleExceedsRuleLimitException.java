package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On Januvary 07th,2016
 *	UserDefined Exception, When the Game Rule is exceeds the Rule Limit
 */
public class GameRuleExceedsRuleLimitException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	public String toString() {
		return "Game Rule Exceeds the Rule Limit";
	}
}