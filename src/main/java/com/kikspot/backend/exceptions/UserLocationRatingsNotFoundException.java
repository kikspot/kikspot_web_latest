package com.kikspot.backend.exceptions;

public class UserLocationRatingsNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	
	private String message;
	
	public UserLocationRatingsNotFoundException() {
		message="User Location Ratings Not Found";
	}
	
	
	public UserLocationRatingsNotFoundException(String message){
		this.message=message;
	}
	
	
	
	@Override
	public String toString() {
		
		return message;
	}
	
	
}
