package com.kikspot.backend.exceptions;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Bhagya On Feb 02nd, 2015
 *  
 *   User Prefernces Not Found Exception class.
 *
 */

public class UserPreferencesNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {	
		return "User Preferences Not Found";
	}

}
