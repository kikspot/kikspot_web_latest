package com.kikspot.backend.exceptions;

public class UsernameAlreadyExistsException extends Exception{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
	
		return "UserName Already Exists Exception";
	}

}
