package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On November 26th,2015
 *	User Defined Exception For handling ,when the request was denied may be API Key is not a valid one
 */
public class RequestDeniedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Request Denied While Requesting Google Places API, because of lack of an invalid key parameter.";
	}
}