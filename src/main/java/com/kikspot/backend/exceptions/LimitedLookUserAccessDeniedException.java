package com.kikspot.backend.exceptions;

public class LimitedLookUserAccessDeniedException extends Exception {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		
		return "Limited Look User Access Denied Exception ";
	}
	
	
	
}
