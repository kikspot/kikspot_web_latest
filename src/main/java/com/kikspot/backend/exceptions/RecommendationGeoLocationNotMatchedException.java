package com.kikspot.backend.exceptions;

public class RecommendationGeoLocationNotMatchedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		
		return "Your Current Location Does Not Matches with Recommendaiton Location";
	}
}
