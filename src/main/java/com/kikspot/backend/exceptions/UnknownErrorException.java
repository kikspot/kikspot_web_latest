package com.kikspot.backend.exceptions;
/**
 * 
 *Created By Bhagya On November 26th,2015
 * User defined Exception for Handling the Server side error while requesting Google Places API
 */
public class UnknownErrorException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Indicates a server-side error,trying again may be successful.";
	}
}