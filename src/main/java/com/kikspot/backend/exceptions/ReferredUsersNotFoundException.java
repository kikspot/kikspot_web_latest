package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On December 24th,2015
 *	User defined Exception for,when the Reffered Users not found for requested User
 */
public class ReferredUsersNotFoundException extends Exception{
	
	private static final long serialVersionUID = 1L;
		
		@Override
		public String toString() {	
			return "Reffered Users Not Found For Requested User";
		}
}