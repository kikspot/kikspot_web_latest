package com.kikspot.backend.exceptions;
/**
 *Created By Bhagya On November 26th, 2015 
 *User Defined exception for handling the ,when Google Places API returning the No results for the request 
 */
public class NoResultsFoundException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "No Results Found ";
	}
}