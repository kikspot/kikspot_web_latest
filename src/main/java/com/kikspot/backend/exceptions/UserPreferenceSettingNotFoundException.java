package com.kikspot.backend.exceptions;
/**
 * Created By bhagya On Feb 02nd, 2016
 *	User Defined Exception class for when User Preference Setting Not Found
 */
public class UserPreferenceSettingNotFoundException extends Exception{
private static final long serialVersionUID = 1L;
	
	public String toString() {
		return "User Preference Setting Not Found";
	}
}