package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On December 15th, 2015
 * User Defined Exception for handling, when the API Unavailable
 */
public class APIUnavailableException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "API is Unavailable,Can you try again..";
	}
}