package com.kikspot.backend.exceptions;

public class NotificationNotFoundException extends Exception{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String message;
	
	public NotificationNotFoundException() {
		message="Notification Type Not Found";
	}
	
	
	
	public NotificationNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {		
		return message;
	}
	
	
	
}
