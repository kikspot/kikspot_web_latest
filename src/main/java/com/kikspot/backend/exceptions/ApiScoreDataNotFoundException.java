package com.kikspot.backend.exceptions;



public class ApiScoreDataNotFoundException extends Exception{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public ApiScoreDataNotFoundException() {
			message="Users Score Data Not Found";
	}
	
	
	public ApiScoreDataNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		
		return message;
	}

}
