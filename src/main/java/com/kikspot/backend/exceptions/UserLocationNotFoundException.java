package com.kikspot.backend.exceptions;

public class UserLocationNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	
	public UserLocationNotFoundException() {
		message="User Location Not Found";
	}
	
	
	public UserLocationNotFoundException(String message){
		this.message=message;
	}
	
	
	
	@Override
	public String toString() {
		
		return message;
	}
}
