package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On Januvary 07th,2016
 *	UserDefined Exception, When the Game Rule is Expired
 */
public class GameRuleExpiredException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	public String toString() {
		return "Game Rule is Expired";
	}
}