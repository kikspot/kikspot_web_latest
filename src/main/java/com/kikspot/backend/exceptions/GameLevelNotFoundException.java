package com.kikspot.backend.exceptions;



public class GameLevelNotFoundException extends Exception{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public GameLevelNotFoundException() {
			message="Game Level Not Found";
	}
	
	
	public GameLevelNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		
		return message;
	}

}
