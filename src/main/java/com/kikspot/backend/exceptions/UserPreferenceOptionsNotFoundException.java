package com.kikspot.backend.exceptions;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Bhagya On Feb 02nd, 2015
 *  
 *   User Prefernce Options Not Found Exception class.
 *
 */

public class UserPreferenceOptionsNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {	
		return "User Preferences Not Found";
	}

}
