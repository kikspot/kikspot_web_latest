package com.kikspot.backend.exceptions;

/**
 * 
 * @author kns09055
 * created by Firdous on 26-11-2015
 *
 */

public class FeedBackNotFoundException extends Exception {
	
private static final long serialVersionUID = 1L;
	
	public String toString() {
		return "There is no feedback present for this user";
	}

}
