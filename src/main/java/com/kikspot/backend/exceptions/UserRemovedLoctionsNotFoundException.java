package com.kikspot.backend.exceptions;

public class UserRemovedLoctionsNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public UserRemovedLoctionsNotFoundException() {
		message="User Removed Location Not Found";
	}
	
	public UserRemovedLoctionsNotFoundException(String message){
		this.message=message;
	}
	
	
	
	@Override
	public String toString() {
		return message;
	}

}
