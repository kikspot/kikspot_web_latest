package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On January 20th,2016
 *	User Defined Exception  for when the kikspot reward was not found  
 */
public class KikspotRewardNotFoundException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Kikspot Reward Not Found ";
	}
}