package com.kikspot.backend.exceptions;

public class UserCredHistoryNotFoundException extends Exception{
	
	String message;
	
	public UserCredHistoryNotFoundException() {
		message="User Cred History Not Found";
	}
	
	public UserCredHistoryNotFoundException(String message){
		this.message=message;
	}
	
	
	
	@Override
	public String toString() {
		
		return message;
		
	}
	
	
	
	

}
