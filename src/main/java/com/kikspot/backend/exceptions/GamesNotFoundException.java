package com.kikspot.backend.exceptions;

public class GamesNotFoundException extends Exception{
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Games  Not Found ";
	}

}
