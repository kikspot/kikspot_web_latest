package com.kikspot.backend.exceptions;



public class UsersScoreDataNotFoundException extends Exception{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public UsersScoreDataNotFoundException() {
			message="Users Score Data Not Found";
	}
	
	
	public UsersScoreDataNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		
		return message;
	}

}
