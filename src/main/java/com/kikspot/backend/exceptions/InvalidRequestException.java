package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On November 26th,2015
 *	User Defined Exception For Handling the INVALID REQUEST RESPONSE from GOOGLE PLACES API
 */
public class InvalidRequestException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Invalid Request Exception While Requesting Google Places API,generally indicates that the query (reference) is missing";
	}
}