package com.kikspot.backend.exceptions;

public class UserDeviceNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String message;
	
	public UserDeviceNotFoundException() {
		message="User Device Not Found";
	}
	
	
	public UserDeviceNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		return message;
	}

}
