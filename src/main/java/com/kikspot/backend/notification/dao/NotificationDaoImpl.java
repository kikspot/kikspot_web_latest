package com.kikspot.backend.notification.dao;


import java.util.ArrayList;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.backend.exceptions.NotificationNotFoundException;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.UnsubscribedNotification;
import com.kikspot.backend.notification.model.UserNotification;
import com.kikspot.backend.useraction.model.Feedback;



/**
 * Created by Jeevan on DECEMBER 21, 2015
 * 
 *  Dao for Notifications..
 *  
 *  
 * @author KNS-ACCONTS
 *
 */
@Transactional
@Repository("notificationDao")
public class NotificationDaoImpl implements NotificationDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	private static Logger log=Logger.getLogger(NotificationDaoImpl.class);
	
	
	
	/**
	 * 
	 *  SHOULD INCLUDE METHODS 
	 *  
	 *   1. SAVE OR UPDATE NOTIFICATIONTYPE
	 *   2. GET NOTIFICATIONTYPE BY TYPE.
	 *   3. SAVE OR UPDATE UNSUBSCRIBED NOTIFICATION
	 *   4. DELETE UNSUBSCRIBED NOTIFICATION
	 *   5. GET ALL UNSUBSCRIBERD NOTIFICATIONS OF USER.
	 *   6. SAVE OR UPDATE USERNOTIFICATIONS
	 *   7. DELETE USER NOTIFICATION
	 *   8. GET USER NOTIFICATION OF USER with PAGINATION..
	 *   
	 *   9. GET UNREAD USER NOTIFICATION OF USER.
	 *  
	 */
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 21, 2015
	 * 
	 * Method to save or UpdatENotificationType..
	 * 
	 * @param notificationType
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateNotificationType(NotificationType notificationType)throws Exception{
		log.info("inside saveorUpdateNotificationType() ");
		sessionFactory.getCurrentSession().saveOrUpdate(notificationType);
		sessionFactory.getCurrentSession().flush();
		return notificationType.getNotificationTypeId();
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 21, 2015
	 * Method to get NotificationType by Type..
	 * 
	 * @param type
	 * @return
	 * @throws NotificationNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public NotificationType getNotificationTypeByType(String type)throws NotificationNotFoundException{
		log.info("inside getNotificationTypeByType() ");
	
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria( NotificationType.class)
				.add(Restrictions.eq("notificationType", type));
		ArrayList<NotificationType> notificationTypes=(ArrayList<NotificationType>) criteria.list();
		if(!notificationTypes.isEmpty()){
			return notificationTypes.get(0);
		}
		else{
			throw new NotificationNotFoundException();
		}
	}

	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on DECEMBER 23, 2015
	 * 
	 *  Method to getNotificationTypeById...
	 *  
	 * 
	 * @param notificationTypeId
	 * @return
	 * @throws NotificationNotFoundException
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public NotificationType getNotificationTypeById(Integer notificationTypeId)throws NotificationNotFoundException{
		log.info("inside getNotificationTypeById() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria( NotificationType.class)
				.add(Restrictions.eq("notificationTypeId",notificationTypeId));
		ArrayList<NotificationType> notificationTypes=(ArrayList<NotificationType>) criteria.list();
		if(!notificationTypes.isEmpty()){
			return notificationTypes.get(0);
		}
		else{
			throw new NotificationNotFoundException();
		}
	}
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 * 
	 * @return
	 * @throws NotificationNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<NotificationType> getAllNotificationTypes()throws NotificationNotFoundException{
		log.info("inside getAllNotificationTypes() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria( NotificationType.class);
				
		ArrayList<NotificationType> notificationTypes=(ArrayList<NotificationType>) criteria.list();
		if(!notificationTypes.isEmpty()){
			return notificationTypes;
		}
		else{
			throw new NotificationNotFoundException();
		}
	}

	
	
	
	
/************************ UNSUBSCRIBED NOTIFICATIONS ***************************************/
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * Method to saveorUpdateUnsubscribedNotifications..
	 * 
	 * @param unsubscribedNotification
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUnsubscribedNotifications(UnsubscribedNotification unsubscribedNotification)throws Exception{
		log.info("inside saveorUpdateUnsubscribedNotifications() ");
		sessionFactory.getCurrentSession().saveOrUpdate(unsubscribedNotification);
		sessionFactory.getCurrentSession().flush();
		return unsubscribedNotification.getUnsubscribedNotificationId();
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 * Method to deleteUnsubscribedNotification..
	 * 
	 * @param unsubscribedNotification
	 * @return
	 * @throws Exception
	 */
	public Integer deleteUnsubscribedNotification(UnsubscribedNotification unsubscribedNotification)throws Exception{
		log.info("inside deleteUnsubscribedNotification() ");
		sessionFactory.getCurrentSession().delete(unsubscribedNotification);
		sessionFactory.getCurrentSession().flush();
		return unsubscribedNotification.getUnsubscribedNotificationId();
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 * Method to getAllUnsubscribedNotificationsofUser.
	 * 
	 * 
	 * @param userId
	 * @return
	 * @throws NotificationNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UnsubscribedNotification> getAllUnsubscribedNotificationsofUser(Integer userId)throws NotificationNotFoundException{
		log.info("inside getAllUnsubscribedNotificationOfUser());");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UnsubscribedNotification.class)
								.add(Restrictions.eq("kikspotUser.userId", userId));
		ArrayList<UnsubscribedNotification> unsubscribedNotifications=(ArrayList<UnsubscribedNotification>) criteria.list();
		if(!unsubscribedNotifications.isEmpty()){
			return unsubscribedNotifications;
		}
		else{
			throw new NotificationNotFoundException("Unsubscribed Notifications Not Found for User "+userId);
		}
	}
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 23, 2015
	 * 
	 * Method to getUnsubscribedNotificationByUserandType..
	 * 
	 * 
	 * @param userId
	 * @param notificationTypeId
	 * @return
	 * @throws NotificationNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UnsubscribedNotification getUnsubscribedNotificationByUserandType(Integer userId,Integer notificationTypeId)throws NotificationNotFoundException{
		log.info("inside getUnsubscribedNotificationByUserandType()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UnsubscribedNotification.class)
				.add(Restrictions.and(Restrictions.eq("kikspotUser.userId", userId),Restrictions.eq("notificationType.notificationTypeId", notificationTypeId)));
		ArrayList<UnsubscribedNotification> unsubscribedNotifications=(ArrayList<UnsubscribedNotification>) criteria.list();
		if(!unsubscribedNotifications.isEmpty()){
			return unsubscribedNotifications.get(0);
		}
		else{
		throw new NotificationNotFoundException("Unsubscribed Notifications Not Found for User "+userId);
		}
	}
	
	
	
	
/************************ UNSUBSCRIBED NOTIFICATIONS ***************************************/	
	
	
	/**
	 * 	
	 * @param userNotification
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserNotification(UserNotification userNotification)throws Exception{
		log.info("inside saveorUpdateUserNotification()");
		sessionFactory.getCurrentSession().saveOrUpdate(userNotification);
		sessionFactory.getCurrentSession().flush();
		return userNotification.getUserNotificationId();
	}
	
	
	
	/**
	 * CREATED BY JEEVAN ON DECEMBER 22, 2015
	 * Method to delete UserNotification..
	 * @param userNotification
	 * @return
	 * @throws Exception
	 */
	//delete
	public Integer deleteUserNotification(UserNotification userNotification)throws Exception{
		log.info("inside deleteUserNotification() ");
		sessionFactory.getCurrentSession().delete(userNotification);
		sessionFactory.getCurrentSession().flush();
		return userNotification.getUserNotificationId();
	}
	
	
	
	/**
	 * 
	 *  Created by Jeevan on
	 * 
	 * @param userId
	 * @param pageNo
	 * @param pageSize
	 * @param isUnread
	 * @return
	 * @throws NotificationNotFoundException
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public ArrayList<UserNotification> getUserNotificationsOfUser(Integer userId,Integer pageNo,Integer pageSize,Boolean isUnread)throws NotificationNotFoundException{
		log.info("inside getUserNotificationsOfUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserNotification.class);
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		if(null!=isUnread){
			criteria.add(Restrictions.eq("isRead", false));
		}
		Integer totalNotifications=criteria.list().size();		
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}
		criteria.addOrder(Order.desc("notificationDate"));
		ArrayList<UserNotification> userNotifications=(ArrayList<UserNotification>) criteria.list();
		if(!userNotifications.isEmpty()){
			return userNotifications;
		}
		else{
			throw new NotificationNotFoundException("User Notification Not Found ");
		}
	}


   /**
    * Created by Firdous on 3rd Febraury 
    * Method to get all the notifications of a user
    * 
    */

	@SuppressWarnings("unchecked")
	public ArrayList<UserNotification> getNotifications(Integer userId) throws NotificationNotFoundException {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserNotification.class);
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		criteria.addOrder(Order.desc("notificationDate"));
		ArrayList<UserNotification> userNotifications = (ArrayList<UserNotification>) criteria.list();
		if(!userNotifications.isEmpty()){
			return userNotifications;
		}
		else{
			throw new NotificationNotFoundException();
		}		
	}
	
	
	
	
	/**
	 * Created by Jeevan on FEB 16, 2016
	 * 
	 * Method to getUserNotificationByNotificationId
	 * @param notificationId
	 * @return
	 * @throws NotificationNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserNotification getUserNotificationByNotificationId(Integer notificationId)throws NotificationNotFoundException{
		log.info("inside getUserNotificationByNotificationId() ");
		ArrayList<UserNotification> userNotifications=new ArrayList<UserNotification>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserNotification.class)
				.add(Restrictions.eq("userNotificationId", notificationId));
		userNotifications=(ArrayList<UserNotification>) criteria.list();
		if(!userNotifications.isEmpty()){
			return userNotifications.get(0);
		}
		else{
			throw new NotificationNotFoundException("No Notifications Found With Given Id");
		}		
	}




	@SuppressWarnings("unchecked")
	public void deleteUserNotifications(Integer id) throws Exception {
		ArrayList<UserNotification> userNotifications=(ArrayList<UserNotification>)sessionFactory.getCurrentSession().createCriteria(UserNotification.class)  
		        .add(Restrictions.eq("kikspotUser.userId", id));	
				sessionFactory.getCurrentSession().delete(userNotifications);	
		
	}
	
	 /**
     * Added by Firdous on 26th May 2016
     * Method to total number of unread notifications of a user
     * 
     */
	@SuppressWarnings("unchecked")
	public Integer getUnreadNotifications(Integer userId) throws Exception {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserNotification.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser.userId", userId),Restrictions.eq("isRead",false)));
		ArrayList<UserNotification> userNotifications = (ArrayList<UserNotification>) criteria.list();
		if(!userNotifications.isEmpty()){
			return userNotifications.size();
		}
		else{
			return 0;
		}			
	}
}
