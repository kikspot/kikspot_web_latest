package com.kikspot.backend.notification.dao;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.NotificationNotFoundException;
import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.notification.model.UnsubscribedNotification;
import com.kikspot.backend.notification.model.UserNotification;

public interface NotificationDao {
	public Integer saveorUpdateNotificationType(NotificationType notificationType)throws Exception;
	public NotificationType getNotificationTypeByType(String type)throws NotificationNotFoundException;
	public NotificationType getNotificationTypeById(Integer notificationTypeId)throws NotificationNotFoundException;
	public Integer saveorUpdateUnsubscribedNotifications(UnsubscribedNotification unsubscribedNotification)throws Exception;
	public Integer deleteUnsubscribedNotification(UnsubscribedNotification unsubscribedNotification)throws Exception;
	public ArrayList<UnsubscribedNotification> getAllUnsubscribedNotificationsofUser(Integer userId)throws NotificationNotFoundException;
	public UnsubscribedNotification getUnsubscribedNotificationByUserandType(Integer userId,Integer notificationTypeId)throws NotificationNotFoundException;
	public Integer saveorUpdateUserNotification(UserNotification userNotification)throws Exception;
	public Integer deleteUserNotification(UserNotification userNotification)throws Exception;
	public ArrayList<UserNotification> getUserNotificationsOfUser(Integer userId,Integer pageNo,Integer pageSize,Boolean isUnread)throws NotificationNotFoundException;
	public ArrayList<NotificationType> getAllNotificationTypes()throws NotificationNotFoundException;
	public ArrayList<UserNotification> getNotifications(Integer userId) throws NotificationNotFoundException;
	public UserNotification getUserNotificationByNotificationId(Integer notificationId)throws NotificationNotFoundException;
	public void deleteUserNotifications(Integer id) throws Exception;
	public Integer getUnreadNotifications(Integer userId) throws Exception;
}
