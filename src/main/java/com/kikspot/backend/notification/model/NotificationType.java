package com.kikspot.backend.notification.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * Created by Jeevan on DECEMBER 21, 2015
 * 
 *  class for NotificationTypes.
 *  
 * @author KNS-ACCONTS
 *
 */

@Entity
@Table(name="notification_type")
public class NotificationType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="notification_type_id")
	private Integer notificationTypeId;
		
	
	@Column(name="notification_type") 
	private String notificationType;
	
	


	public Integer getNotificationTypeId() {
		return notificationTypeId;
	}


	public void setNotificationTypeId(Integer notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}


	public String getNotificationType() {
		return notificationType;
	}


	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	
	
	
	
	
}
