package com.kikspot.backend.notification.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;





import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.kikspot.backend.notification.model.NotificationType;
import com.kikspot.backend.user.model.KikspotUser;


/**
 * Created by Jeevan on DECEMBER 21, 2015
 * 
 * Model for User Notifications..
 * 
 * 
 * @author KNS-ACCONTS
 *
 */
@Entity
@Table(name="user_notification")
public class UserNotification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7210781496381723444L;


	@Id
	@GeneratedValue
	@Column(name="user_notification_id")
	private Integer userNotificationId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	
	@ManyToOne
	@JoinColumn(name="notification_type_id")
	private NotificationType notificationType;
	
	
	@Column(name="notification_message")
	private String notificationMessage;
	
	@Column(name="notification_date")
	private Date notificationDate;
	
	
	//This column preserve Rating ID / REcommendation Id, User Id etc to go to a particular location..
	@Column(name="triggered_id")
	private String triggeredId;
	
	@Column(name="is_read")
	private Boolean isRead;
	
	
	
	@Transient
	private Integer totalNotifications;

	
	
	public Integer getUserNotificationId() {
		return userNotificationId;
	}

	public void setUserNotificationId(Integer userNotificationId) {
		this.userNotificationId = userNotificationId;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getTriggeredId() {
		return triggeredId;
	}

	public void setTriggeredId(String triggeredId) {
		this.triggeredId = triggeredId;
	}

	public Integer getTotalNotifications() {
		return totalNotifications;
	}

	public void setTotalNotifications(Integer totalNotifications) {
		this.totalNotifications = totalNotifications;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}
	
	
	
	
	
	
	
	
}
