package com.kikspot.backend.notification.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kikspot.backend.user.model.KikspotUser;


/**
 * 
 *  Created by Jeevan on DECEMBER 21, 2015
 *  
 *  Model for UserNotificationSettings
 * @author KNS-ACCONTS
 *
 */


@Entity
@Table(name="unsubscribed_notification")
public class UnsubscribedNotification implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	@Id
	@GeneratedValue
	@Column(name="unsubscribed_notication_id")
	private Integer unsubscribedNotificationId;
	
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	
	@ManyToOne
	@JoinColumn(name="notification_type_id")
	private NotificationType notificationType;
	
	
	
	

	public Integer getUnsubscribedNotificationId() {
		return unsubscribedNotificationId;
	}


	public void setUnsubscribedNotificationId(Integer unsubscribedNotificationId) {
		this.unsubscribedNotificationId = unsubscribedNotificationId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public NotificationType getNotificationType() {
		return notificationType;
	}


	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}
	
	
	
	
	
	
	
	
}
