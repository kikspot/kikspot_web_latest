package com.kikspot.backend.notification.model;

public enum NotificationTypes {
	//RECOMMENDATION,REFERRAL,NEWLOCATION,FEEDBACK,RATING,SIGNUP,ADMIN
	CredEarned,LevelAttained,RateSubmitted,NewLocation,LocationorPlacefollowed,Feedback,Shared
}
