package com.kikspot.backend.recommendation.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.kikspot.backend.admin.model.MasterGeoLocationConfiguration;
import com.kikspot.backend.exceptions.ApiScoreDataNotFoundException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.exceptions.UserRemovedLoctionsNotFoundException;
import com.kikspot.backend.exceptions.UsersScoreDataNotFoundException;
import com.kikspot.backend.exceptions.VenueScoreDataNotFoundException;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.recommendation.model.ApiData;
import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;
import com.kikspot.backend.recommendation.model.LocationRatingToggler;
import com.kikspot.backend.recommendation.model.RecommendationConfiguration;
import com.kikspot.backend.recommendation.model.RecommendationLocation;
import com.kikspot.backend.recommendation.model.RecommendationLocationImages;
import com.kikspot.backend.recommendation.model.ScoreData;
import com.kikspot.backend.recommendation.model.UserGeoLocationRatings;
import com.kikspot.backend.recommendation.model.UserLocationRatings;
import com.kikspot.backend.recommendation.model.UserRecommendationLocationImages;
import com.kikspot.backend.recommendation.model.UserRemovedLocation;
import com.kikspot.backend.recommendation.model.VenueScoreData;
import com.kikspot.backend.user.model.KikspotUser;



public interface RecommendationDao {

	public Integer saveOrUpdateUserRemovedLocation(UserRemovedLocation userRemovedLocation)throws Exception;
	public ArrayList<UserRemovedLocation> getUserRemovedLocations(Integer userId)throws UserRemovedLoctionsNotFoundException;
	public ArrayList<UserRemovedLocation> getUserRemovedLocationsOfUserandSort(Integer userId,String sortBy)throws UserRemovedLoctionsNotFoundException;
	public Integer deleteUserRemovedLocations(UserRemovedLocation userRemovedLocation)throws Exception;
	public Integer saveUserLocationRatings(UserLocationRatings userLocationRatings)throws Exception;
	public Map<String, Double> getAllAverageUserLocationRatings()throws UserLocationRatingsNotFoundException;
	public Integer getAverageUserLocationRating(String locationAPIId)throws UserLocationRatingsNotFoundException;
	public UserLocationRatings getLocationRatingByUserandLocation(Integer userId,String locationAPIId)throws UserLocationRatingsNotFoundException;
	
	public Integer saveOrUpdateKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation)throws Exception;
	public KikspotRecommendationLocation getKikspotRecommendationLocationByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException;
	public ArrayList<KikspotRecommendationLocation> getKikspotRecommendationLocations(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRecommendationLocationNotFoundException;
	public KikspotRecommendationLocation getKikspotRecommendationLocationByKikspotLocationId(String kikspotLocationId) throws KikspotRecommendationLocationNotFoundException;
	public Integer deleteKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation) throws Exception;
	public Integer saveOrUpdateKikspotRecommendationLocationImages(RecommendationLocationImages recommendationLocationImages)throws Exception;
	public ArrayList<RecommendationLocationImages> getRecommendationLocationImagesByKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation) throws RecommendationLocationImagesNotFoundException;
	public RecommendationLocationImages getRecommendationLocationImageById(Integer locationImageId) throws RecommendationLocationImagesNotFoundException;
	public Integer deleteRecommendationLocationImage(RecommendationLocationImages recommendationLocationImages) throws Exception;
	public ArrayList<KikspotRecommendationLocation> getAllKikspotRecommendationLocationsNearbyLocation(Double latitude,Double longitude,Integer pageNo,Integer pageSize,String sortBy)throws Exception;
	public ArrayList<UserLocationRatings> getUserLocationRatingsByLocationAPIId(String locationAPIId) throws UserLocationRatingsNotFoundException;
	public Integer saveOrUpdateUserRecommendationLocationImages(UserRecommendationLocationImages userRecommendationLocationImages)throws Exception;
	public UserLocationRatings getMostRecentLocationRatingOfUser(KikspotUser kikspotUser,String locationAPIId) throws UserLocationRatingsNotFoundException;
	public LocationRatingToggler getLocationRatingToggler()throws Exception;
	public KikspotRecommendationLocation getKikspotRecommendationLocationByGoogleLocationId(String googleLocationId) throws KikspotRecommendationLocationNotFoundException;
	public Integer saveorUpdateLocationRatingToggler(LocationRatingToggler locationRatingToggler)throws Exception;
	public Integer saveorUpdateRecommendationConfiguration(RecommendationConfiguration configuration)throws Exception;
	public RecommendationConfiguration getRecommendationConfiguration()throws Exception;
	
	
	
	// Added By Bhagya
	public ArrayList<UserLocationRatings> getUserLocationRatingsByUserId(Integer userId) throws UserLocationRatingsNotFoundException;
	public ArrayList<UserLocationRatings> getUserLocationCommentsByLocationId(String locationApIId) throws UserLocationRatingsNotFoundException;
	public ArrayList<UserGeoLocationRatings> getUserGeoLocationRatingsByRatedDate(KikspotUser kikspotUser) throws UserLocationRatingsNotFoundException;
	public Integer saveUserGeoLocationRatings(UserGeoLocationRatings userGeoLocationRatings) throws Exception;
	public ArrayList<UserLocationRatings> getUserLocationRatingsByLocationAPIIdAndDate(String locationAPIId) throws Exception;
	
	
	public ArrayList<UserLocationRatings> getLocationRatingsByLocationAPIIdAndStartTimeAndEndTime(String locationAPIId,Date startTime,Date endTime) throws Exception;
	public ArrayList<UserLocationRatings> getUserLocationRatingsByUserIdAndStartTimeAndEndTime(Integer userId,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException;
	public ArrayList<UserLocationRatings> getUserLocationCommentsByLocationIdAndUserIdAndStartTimeAndEndTime(String locationApIId,Integer userId,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException;
	public ArrayList<UserGeoLocationRatings> getUserGeoLocationRatingsByAndStartTimeAndEndTime(KikspotUser kikspotUser,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException;
	
	public ArrayList<UserLocationRatings> getUserRatingsPerDayByStartTimeAndEndTimeAndUser(KikspotUser kikspotUser,Date startTime,Date endTime,String locationAPIId) throws UserLocationRatingsNotFoundException;
	public ArrayList<UserLocationRatings> getUserCommentsPerDayByStartTimeAndEndTimeAndUser(KikspotUser kikspotUser,Date startTime,Date endTime,String locationAPIId) throws UserLocationRatingsNotFoundException;
	public ArrayList<UserLocationRatings> getUserNightLocationRatingsByUserIdAndLocationAPIIDStartTimeAndEndTime(String locationAPIId,Integer userId,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException;
	public RecommendationLocationImages getMostRecentRecommendationLocationImageByKikspotRecommendationLocation(
			KikspotRecommendationLocation recommendationLocation) throws RecommendationLocationImagesNotFoundException;
	
	public UserRecommendationLocationImages getMostRecentUserLocationImageByUserIdAndLocationId(String locationId,KikspotUser kikspotUser,Integer userLocationRatingId) throws RecommendationLocationImagesNotFoundException;
	public Integer saveorUpdateUserScore(ScoreData scoreData) throws Exception;
	public ArrayList<ScoreData> getListofUsersScoreData() throws UsersScoreDataNotFoundException;
	public ArrayList<ScoreData> getListofUsersScoreDataByVenueId(String venueId) throws UsersScoreDataNotFoundException;
	public String saveorUpdateAPIScoreForVenue(ApiData apiData) throws Exception;
	public ApiData getApiScorebyVenueId(String venueId) throws ApiScoreDataNotFoundException;
	
	public Integer saveOrUpdateVenueScoreData(VenueScoreData venueScoreData)throws Exception;
	public VenueScoreData getVenueScoreByVenueId(String venueId) throws VenueScoreDataNotFoundException;
	public VenueScoreData getVenueScoreOf30MinutesAgoByVenueId(String venueId) throws VenueScoreDataNotFoundException;
	
	public ArrayList<KikspotRewards> getTheActiveRewardsByLevelAndLocationId(String level,String locationId) throws KikspotRewardNotFoundException;
	public Integer getUserRatingByLocationId(String locationAPIId) throws UserLocationRatingsNotFoundException;
	//added by bhagya on may 21st, 2019
	public MasterGeoLocationConfiguration getMasterGeoLocationConfiguration()throws Exception;
	public Integer saveorUpdateMasterGeoLocationConfiguration(MasterGeoLocationConfiguration configuration)throws Exception;
}
