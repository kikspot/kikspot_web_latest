package com.kikspot.backend.recommendation.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.exolab.castor.types.DateTime;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.standard.DateTimeContext;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.admin.model.MasterGeoLocationConfiguration;
import com.kikspot.backend.exceptions.ApiScoreDataNotFoundException;
import com.kikspot.backend.exceptions.KikspotRecommendationLocationNotFoundException;
import com.kikspot.backend.exceptions.KikspotRewardNotFoundException;
import com.kikspot.backend.exceptions.RecommendationLocationImagesNotFoundException;
import com.kikspot.backend.exceptions.UserLocationRatingsNotFoundException;
import com.kikspot.backend.exceptions.UserRemovedLoctionsNotFoundException;
import com.kikspot.backend.exceptions.UsersScoreDataNotFoundException;
import com.kikspot.backend.exceptions.VenueScoreDataNotFoundException;
import com.kikspot.backend.game.model.KikspotRewards;
import com.kikspot.backend.recommendation.model.ApiData;
import com.kikspot.backend.recommendation.model.KikspotRecommendationLocation;
import com.kikspot.backend.recommendation.model.LocalAPIRecommendationLocations;
import com.kikspot.backend.recommendation.model.LocationRatingToggler;
import com.kikspot.backend.recommendation.model.RecommendationConfiguration;
import com.kikspot.backend.recommendation.model.RecommendationLocation;
import com.kikspot.backend.recommendation.model.RecommendationLocationImages;
import com.kikspot.backend.recommendation.model.ScoreData;
import com.kikspot.backend.recommendation.model.UserGeoLocationRatings;
import com.kikspot.backend.recommendation.model.UserLocationRatings;
import com.kikspot.backend.recommendation.model.UserRecommendationLocationImages;
import com.kikspot.backend.recommendation.model.UserRemovedLocation;
import com.kikspot.backend.recommendation.model.VenueScoreData;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.game.dto.KikspotRewardsDto;



/**
 * Created by Jeevan on December 07, 2015
 * 
 *  DAO for RecommendationDao..
 *  
 *  
 * @author KNS-ACCONTS
 *
 */
@Transactional
@Repository("recommendationDao")
public class RecommendationDaoImpl implements RecommendationDao {

	
	private static Logger log=Logger.getLogger(RecommendationDaoImpl.class);
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on December 07, 2015
	 * 
	 * Method to saveOrUpdate UserRemovedLocation..
	 * 
	 * 
	 * 
	 * @param userRemovedLocation
	 * @return
	 * @throws Exception
	 */
	public Integer saveOrUpdateUserRemovedLocation(UserRemovedLocation userRemovedLocation)throws Exception{
		log.info("inside saveorUpdateUserRemovedLocation() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userRemovedLocation);
		sessionFactory.getCurrentSession().flush();
		return userRemovedLocation.getRemovedLocationId();
	}
	
	
	
	
	/**
	 * Created by Jeevan on December 07, 2015
	 * Method to get USerRemovedLocations..
	 * 
	 * 
	 * @param userId
	 * @return
	 * @throws UserRemovedLoctionsNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserRemovedLocation> getUserRemovedLocations(Integer userId)throws UserRemovedLoctionsNotFoundException{
		log.info("inside getUserRemovedLocations");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserRemovedLocation.class);
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		ArrayList<UserRemovedLocation> userRemovedLocations=(ArrayList<UserRemovedLocation>) criteria.list();
		if(!userRemovedLocations.isEmpty()){
			return userRemovedLocations;
		}
		else{
			throw new UserRemovedLoctionsNotFoundException();
		}		
	}
	
	
	
	
	/**
	 * Created by Jeevan on FEBRUARY 22, 2016
	 * 
	 * Method to getUserRemovedLocationsOfUserandSort..
	 * 
	 * @param userId
	 * @param sortBy
	 * @return
	 * @throws UserRemovedLoctionsNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserRemovedLocation> getUserRemovedLocationsOfUserandSort(Integer userId,String sortBy)throws UserRemovedLoctionsNotFoundException{
		log.info("inside getUserRemovedLocations");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserRemovedLocation.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser.userId", userId),Restrictions.eq("sortBy",sortBy)));
		ArrayList<UserRemovedLocation> userRemovedLocations=(ArrayList<UserRemovedLocation>) criteria.list();
		if(!userRemovedLocations.isEmpty()){
			return userRemovedLocations;
		}
		else{
			throw new UserRemovedLoctionsNotFoundException();
		}		
	}
	
	
	
	
	/**
	 * 
	 */
	public Integer deleteUserRemovedLocations(UserRemovedLocation userRemovedLocation)throws Exception{
		log.info("inside deleteUserRemovedLocations() ");
		
		sessionFactory.getCurrentSession().delete(userRemovedLocation);
		sessionFactory.getCurrentSession().flush();
		return userRemovedLocation.getRemovedLocationId();
	}
	
	
	
	/**
	 * 
	 * Created by Jeevan on Decemeber 08, 2015
	 * 
	 * Method to saveUserLocationRatings..
	 * 
	 * 
	 * @param userLocationRatings
	 * @return
	 * @throws Exception
	 * 
	 * Modified By Bhagya On Jan 08th,2016
	 * 	Change the method to only save the rating(removed the update operation)
	 */
	public Integer saveUserLocationRatings(UserLocationRatings userLocationRatings)throws Exception{
		log.info("inside saveorUpdateUserLocationRatings() ");
		sessionFactory.getCurrentSession().save(userLocationRatings);
		sessionFactory.getCurrentSession().flush();
		return userLocationRatings.getUserLocationRatingId();
	}
	
	
	
	
	
	
	
	/**
	 * Created by Jeevan on December 08, 2015
	 * Method to getAverageUserLocationRatings..
	 * 
	 * 
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> getAllAverageUserLocationRatings()throws UserLocationRatingsNotFoundException{
		log.info("inside getAllAverateUserLocationRatings");
		Map<String, Double> ratingMap=new LinkedHashMap<String, Double>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		ProjectionList projectionList=Projections.projectionList();
		projectionList.add(Projections.property("locationAPIId"));
		projectionList.add(Projections.avg("rating"));
		projectionList.add(Projections.groupProperty("locationAPIId"));
		criteria.setProjection(projectionList);	
		if(criteria.list().size()>0){
			List<Object[]> rows=criteria.list();		
			if(rows.size()>0){
				for(Object[] row:rows){
					ratingMap.put((String)row[0], (Double)row[1]);
				}	
				return ratingMap;
			}
			else{
				return ratingMap;
			}
		}
		else{
			return ratingMap;
		}
	}
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on December 08, 2015
	 *  
	 *  Method to getAverageUserLocationRating..
	 *  
	 * 
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 */
	public Integer getAverageUserLocationRating(String locationAPIId)throws UserLocationRatingsNotFoundException{
		log.info("inside getAverageUserLocationRating() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class)
				.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.setProjection(Projections.avg("rating"));
		if(!criteria.list().isEmpty()){
			return (Integer)criteria.list().get(0);
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	
	
	
	
	/**
	 * 
	 *  Created by Jeevan on
	 *  December 09, 2015
	 *  
	 *  Method to getLocationRatingByUserandLocation..
	 *  
	 * 
	 * @param userId
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * 
	 */
	
	@SuppressWarnings("unchecked")
	public UserLocationRatings getLocationRatingByUserandLocation(Integer userId,String locationAPIId)throws UserLocationRatingsNotFoundException{
		log.info("inside getLocationRatingByUserandLocation() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser.userId", userId),Restrictions.eq("locationAPIId", locationAPIId)));
		ArrayList<UserLocationRatings> userLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!userLocationRatings.isEmpty()){
			return userLocationRatings.get(0);
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocation
	 * @return
	 * 
	 * Method For save Or Update the kikspot recommendation Locations Which are added By Admin
	 */
	
	public Integer saveOrUpdateKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation)throws Exception{
		log.info("inside saveOrUpdateKikspotRecommendationLocation()");
		sessionFactory.getCurrentSession().saveOrUpdate(kikspotRecommendationLocation);
		sessionFactory.getCurrentSession().flush();
		return kikspotRecommendationLocation.getLocationId();
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the KikspotRecommendation Location By location Id
	 */
	@SuppressWarnings("unchecked")	
	public KikspotRecommendationLocation getKikspotRecommendationLocationByLocationId(Integer locationId) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocationByLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		criteria.add(Restrictions.eq("locationId", locationId));
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			return kikspotRecommendationLocations.get(0);
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @param ascending
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the Recommendation Locations Which are added by Admin
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotRecommendationLocation> getKikspotRecommendationLocations(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocations()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		if(null!=searchBy){
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("googleLocationId", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("locationName", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("address", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("phoneNo", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("url", searchBy,
					MatchMode.ANYWHERE));
						
			criteria.add(disjunction);
		}
		if(null!=sortBy){
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));
			}
			else{
				criteria.addOrder(Order.desc(sortBy));
			}				
		}
		
		Integer totalLocations=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			kikspotRecommendationLocations.get(0).setTotalLocations(totalLocations);
			return kikspotRecommendationLocations;
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the KikspotRecommendation Location By Kikspot location Id
	 */
	@SuppressWarnings("unchecked")	
	public KikspotRecommendationLocation getKikspotRecommendationLocationByKikspotLocationId(String kikspotLocationId) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocationByKikspotLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		criteria.add(Restrictions.ne("googleLocationId",""));
		criteria.add(Restrictions.isNotNull("googleLocationId"));
		criteria.add(Restrictions.eq("googleLocationId", kikspotLocationId));
		
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			return kikspotRecommendationLocations.get(0);
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param locationId
	 * @return
	 * @throws KikspotRecommendationLocationNotFoundException
	 * 
	 * Method for Getting the KikspotRecommendation Location By Kikspot location Id
	 */
	@SuppressWarnings("unchecked")	
	public KikspotRecommendationLocation getKikspotRecommendationLocationByGoogleLocationId(String googleLocationId) throws KikspotRecommendationLocationNotFoundException{
		log.info("inside getKikspotRecommendationLocationByLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRecommendationLocation.class);
		criteria.add(Restrictions.eq("googleLocationId", googleLocationId));
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=(ArrayList<KikspotRecommendationLocation>) criteria.list();
		if(!kikspotRecommendationLocations.isEmpty()){
			return kikspotRecommendationLocations.get(0);
		}
		else{
			throw new KikspotRecommendationLocationNotFoundException();
		}
	}
	
	
	/**
	 * Created By Bhagya On December 18th,2015
	 * @param kikspotRecommendationLocation
	 * @return
	 * 
	 * Method for Deleting the kikspot Recommendation Location
	 */
	public Integer deleteKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation)throws Exception{
		log.info("inside deleteKikspotRecommendationLocation()");
		sessionFactory.getCurrentSession().delete(kikspotRecommendationLocation);
		sessionFactory.getCurrentSession().flush();
		return 1;
	}
	
	
	/**
	 * created By Bhagya On December 21st,2015
	 * @param recommendationLocationImages
	 * @return
	 * 
	 * Method for save or update the recommendation Location Images
	 */
	
	public Integer saveOrUpdateKikspotRecommendationLocationImages(RecommendationLocationImages recommendationLocationImages)throws Exception{
		log.info("inside saveOrUpdateKikspotRecommendationLocationImages()");
		sessionFactory.getCurrentSession().saveOrUpdate(recommendationLocationImages);
		sessionFactory.getCurrentSession().flush();
		return recommendationLocationImages.getLocationImageId();
	}
	/**
	 * Created By Bhagya On december 22nd,2015
	 * @param kikspotRecommendationLocation
	 * @return
	 * @throws RecommendationLocationImagesNotFoundException
	 * 
	 * Method for getting the list of recommendation location Images by Kikspot recommendation Location
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<RecommendationLocationImages> getRecommendationLocationImagesByKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation) throws RecommendationLocationImagesNotFoundException{
		log.info("inside getRecommendationLocationImagesByKikspotRecommendationLocation()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(RecommendationLocationImages.class);
		criteria.add(Restrictions.eq("kikspotRecommendationLocation", kikspotRecommendationLocation));
		ArrayList<RecommendationLocationImages> recommendationLocationImages=(ArrayList<RecommendationLocationImages>) criteria.list();
		if(!recommendationLocationImages.isEmpty()){
			return recommendationLocationImages;
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param locationImageId
	 * @return
	 * @throws RecommendationLocationImagesNotFoundException
	 * 
	 * Method For getting Recommendation Location Image By LocationImageId
	 */
	@SuppressWarnings("unchecked")
	public RecommendationLocationImages getRecommendationLocationImageById(Integer locationImageId) throws RecommendationLocationImagesNotFoundException{
		log.info("inside getRecommendationLocationImageById()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(RecommendationLocationImages.class);
		criteria.add(Restrictions.eq("locationImageId", locationImageId));
		ArrayList<RecommendationLocationImages> recommendationLocationImages=(ArrayList<RecommendationLocationImages>) criteria.list();
		if(!recommendationLocationImages.isEmpty()){
			return recommendationLocationImages.get(0);
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
	}
	
	
	/**
	 * Created By Bhagya On December 22nd,2015
	 * @param recommendationLocationImages
	 * @return
	 * @throws Exception
	 * 
	 * Method for delete the recommendation Location Image
	 */
	public Integer deleteRecommendationLocationImage(RecommendationLocationImages recommendationLocationImages) throws Exception{
		log.info("inside deleteRecommendationLocationImage()");
		sessionFactory.getCurrentSession().delete(recommendationLocationImages);
		sessionFactory.getCurrentSession().flush();
		return recommendationLocationImages.getLocationImageId();
	}
	
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 29, 2015
	 * 
	 * Method to getAllKikspotRecommendationLocationsNEarBylocation...
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws Exception
	 */
	public ArrayList<KikspotRecommendationLocation> getAllKikspotRecommendationLocationsNearbyLocation(Double latitude,Double longitude,Integer pageNo,Integer pageSize,String sortBy)throws Exception{
		log.info("inside getAllKikspotRecommendationLocationsNearbyLocation()");
		ArrayList<KikspotRecommendationLocation> kikspotRecommendationLocations=new ArrayList<KikspotRecommendationLocation>();
		StringBuilder query=new StringBuilder();
		Integer distanceCount=3791; /* 3791 refers to distance calculation in miles, 6371 refers to distance calculation in kilometers*/
		query.append("SELECT location_id,google_location_id,address,icon_url,location_name,phone_no,ratings,url,location_date, ( "+distanceCount+" * acos( cos( radians("+latitude+") ) * cos( radians( latitude ) ) * ");
		query.append(" cos( radians( longitude ) - radians("+longitude+") ) + sin( radians("+latitude+") )");
		query.append("* sin( radians( latitude ) ) ) ) AS distance,latitude,longitude,is_open,is_visible FROM kikspot_recommendation_location ");
		query.append("HAVING distance < 5 ORDER BY distance LIMIT 0 , 50 "); 
		SQLQuery sqlQuery=sessionFactory.getCurrentSession().createSQLQuery(query.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> rows=sqlQuery.list();
		if(rows.size()>0){
			for(Object[] row:rows){
				KikspotRecommendationLocation kikspotRecommendationLocation=new KikspotRecommendationLocation();
				kikspotRecommendationLocation.setLocationId((Integer)row[0]);
				kikspotRecommendationLocation.setGoogleLocationId((String)row[1]);
				kikspotRecommendationLocation.setAddress((String)row[2]);
				kikspotRecommendationLocation.setIconUrl((String)row[3]);
				kikspotRecommendationLocation.setLocationName(((String) row[4]));
				kikspotRecommendationLocation.setPhoneNo((String)row[5]);
				kikspotRecommendationLocation.setRatings((Double)row[6]);
				kikspotRecommendationLocation.setUrl((String)row[7]);
				kikspotRecommendationLocation.setLocationDate((Date)row[8]);
				kikspotRecommendationLocation.setDistance((Double)row[9]);
				kikspotRecommendationLocation.setLatitude((Double)row[10]);
				kikspotRecommendationLocation.setLongitude((Double)row[11]);
				kikspotRecommendationLocation.setIsOpen((Boolean)row[12]);
				if(null!=row[13] && (Boolean)row[13]==true ){
					kikspotRecommendationLocations.add(kikspotRecommendationLocation);
				}
			}	
		}
		return kikspotRecommendationLocations;
	}
	
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method For getting Location Ratings and sorted most recent order
	 * Steps: Getting the List of all User ratings by applying the criteria of locationAPIId
	 * 		  Sorting the results by userLocationRatingId for getting the most recent results at top
	 * 		  Return those sorted results (sequence of the most recent ratings)
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserLocationRatingsByLocationAPIId(String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Jan 08th,2016
	 * @param userRecommendationLocationImages
	 * @return
	 * 
	 * Method for save or update the User Recommendation Location Images
	 */
	
	public Integer saveOrUpdateUserRecommendationLocationImages(UserRecommendationLocationImages userRecommendationLocationImages)throws Exception{
		log.info("inside saveOrUpdateUserRecommendationLocationImages()");
		sessionFactory.getCurrentSession().saveOrUpdate(userRecommendationLocationImages);
		sessionFactory.getCurrentSession().flush();
		return userRecommendationLocationImages.getUserLocationImageId();
	}
	/**
	 * Created By BHagya On Jan 08th,2016
	 * @param kikspotUser
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method For getting the Most Recent rating of the user
	 * Steps: Getting the location rating which are satisfy the restricted criteria of kikspotuser and locatioAPIID
	 * 		  Sorting those results as descending order
	 * 		  And returning the first result(most recent)
	 */
	@SuppressWarnings("unchecked")
	public UserLocationRatings getMostRecentLocationRatingOfUser(KikspotUser kikspotUser,String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getMostRecentLocationRatingOfuser()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser", kikspotUser),Restrictions.eq("locationAPIId", locationAPIId)));
		criteria.addOrder(Order.desc("ratingDate"));
		ArrayList<UserLocationRatings> userLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!userLocationRatings.isEmpty()){
			return userLocationRatings.get(0);
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	
	
	/**
	 * 
	 *  Created by Jeevan on February 03, 2016
	 *  
	 *   MEthod to saveorUpdateLocalAPIRecommendationLocation..
	 * 
	 * @param localAPIRecommendation
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateLocalAPIRecommendationLocation(LocalAPIRecommendationLocations localAPIRecommendation)throws Exception{
		log.info("inside saveorUpdateLocalAPIRecommendationLocation() ");
		sessionFactory.getCurrentSession().saveOrUpdate(localAPIRecommendation);
		sessionFactory.getCurrentSession().flush();
		return localAPIRecommendation.getLocalAPIRecommendationLocationId();
	}
	
	
	/**
	 * 
	 * @param locationId
	 * @return
	 * @throws RecommendationLocationImagesNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public LocalAPIRecommendationLocations GetLocalAPIRecommendationLocationsByLocationId(String locationId)throws RecommendationLocationImagesNotFoundException{
		log.info("inside GetLocalAPIRecommendationLocationsByLocationId() ");
		ArrayList<LocalAPIRecommendationLocations> localAPIRecommendationLocations=new ArrayList<LocalAPIRecommendationLocations>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(LocalAPIRecommendationLocations.class)
				.add(Restrictions.eqOrIsNull("locationAPIId", locationId));
		localAPIRecommendationLocations=(ArrayList<LocalAPIRecommendationLocations>) criteria.list();
		if(!localAPIRecommendationLocations.isEmpty()){
			return localAPIRecommendationLocations.get(0);
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
	}
	
	
	
	/**
	 * Created by Jeevan on February 04, 2016
	 * Method to getLocationRatingToggler..
	 * 
	 * @return
	 * @throws Exception
	 */
	public LocationRatingToggler getLocationRatingToggler()throws Exception{
		log.info("inside getLocationRatingToggler()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(LocationRatingToggler.class);
		ArrayList<LocationRatingToggler> locationTogglers=(ArrayList<LocationRatingToggler>) criteria.list();
		return locationTogglers.get(0);
	}
	
	
	/**
	 * Created by Jeevan on February 04, 2016
	 * Method to saverUpdateLoca
	 * @param locationRatingToggler
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateLocationRatingToggler(LocationRatingToggler locationRatingToggler)throws Exception{
		log.info("inside saveorUpdateLocationRatingToggler() ");
		sessionFactory.getCurrentSession().saveOrUpdate(locationRatingToggler);
		sessionFactory.getCurrentSession().flush();
		return locationRatingToggler.getLocationRatingTogglerId();
	}
	
	
	/**
	 * 
	 * @param configuration
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateRecommendationConfiguration(RecommendationConfiguration configuration)throws Exception{
		log.info("inside saveorUpdateRecommendationConfiguration()");
		sessionFactory.getCurrentSession().saveOrUpdate(configuration);
		sessionFactory.getCurrentSession().flush();
		return configuration.getRecommendationConfigurationId();
	}
	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public RecommendationConfiguration getRecommendationConfiguration()throws Exception{
		log.info("inside getRecommendationConfiguration()");
		ArrayList<RecommendationConfiguration> configurations=new ArrayList<RecommendationConfiguration>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(RecommendationConfiguration.class);
		configurations=(ArrayList<RecommendationConfiguration>) criteria.list();
		return configurations.get(0);
	}
	
	
	/**
	 * Created By Bhagya On April 16th, 2016
	 * @param user
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the user location ratings of a user by user Id 
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserLocationRatingsByUserId(Integer userId) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	/**
	 *  Created By Bhagya On April 16th, 2016
	 *  
	 * @param locationApIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for get the location comments
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserLocationCommentsByLocationId(String locationApIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		criteria.add(Restrictions.eq("locationAPIId", locationApIId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.add(Restrictions.and(Restrictions.isNotNull("comment"),Restrictions.ne("comment", "")));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Aril 16th, 2016
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the user geo location ratings by latitude and longitude
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<UserGeoLocationRatings> getUserGeoLocationRatingsByRatedDate(KikspotUser kikspotUser) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserGeoLocationRatings()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGeoLocationRatings.class);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.add(Restrictions.between("geoRatingDate",fromDate,toDate));
		
		ArrayList<UserGeoLocationRatings> userGeoLocationRatings=(ArrayList<UserGeoLocationRatings>) criteria.list();
		if(!userGeoLocationRatings.isEmpty()){
			return userGeoLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On April 16th, 2016
	 * @param userGeoLocationRatings
	 * @return
	 * @throws Exception
	 * 
	 * Method for to save the User Geo Location Ratings
	 */
	public Integer saveUserGeoLocationRatings(UserGeoLocationRatings userGeoLocationRatings) throws Exception{
		log.info("inside saveUserGeoLocationRatings()");
		sessionFactory.getCurrentSession().saveOrUpdate(userGeoLocationRatings);
		sessionFactory.getCurrentSession().flush();
		return userGeoLocationRatings.getUserGeoLocationRatingId();
	}

	/**
	 * Created By Firdous
	 */

	@SuppressWarnings("unchecked")

	@Override
	public ArrayList<UserLocationRatings> getUserLocationRatingsByLocationAPIIdAndDate(String locationAPIId) throws Exception {
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On May 05th, 2016
	 * @param locationAPIId
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 * 
	 * Method for to get the location ratings by location APIId, starttime and end time for applying nightly rate conditions
	 */
	public ArrayList<UserLocationRatings> getLocationRatingsByLocationAPIIdAndStartTimeAndEndTime(String locationAPIId,Date startTime,Date endTime) throws Exception {
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		
	
		
		DateTime start=new DateTime(startTime);
		int startHours =start.getHour();
		int startMinutes=start.getMinute();
		int startSeconds=start.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, startHours);
		calendar.set(Calendar.MINUTE, startMinutes);
		calendar.set(Calendar.SECOND, startSeconds);
		Date startTimeFormat=calendar.getTime();
		
		
		DateTime end=new DateTime(endTime);
		int endHours =end.getHour();
		int endMinutes=end.getMinute();
		int endSeconds=end.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, endHours);
		calendar.set(Calendar.MINUTE, endMinutes);
		calendar.set(Calendar.SECOND, endSeconds);
		Date endTimeFormat=calendar.getTime();
		
	
		
		
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.add(Restrictions.between("ratingDate",startTimeFormat,endTimeFormat));
		
		criteria.addOrder(Order.desc("userLocationRatingId"));
		
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/***
	 * Created By Bhagya on may 05th, 2016
	 * @param userId
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the night spot user ratings
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserLocationRatingsByUserIdAndStartTimeAndEndTime(Integer userId,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
			
		DateTime start=new DateTime(startTime);
		int startHours =start.getHour();
		int startMinutes=start.getMinute();
		int startSeconds=start.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, startHours);
		calendar.set(Calendar.MINUTE, startMinutes);
		calendar.set(Calendar.SECOND, startSeconds);
		Date startTimeFormat=calendar.getTime();
		
		
		DateTime end=new DateTime(endTime);
		int endHours =end.getHour();
		int endMinutes=end.getMinute();
		int endSeconds=end.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, endHours);
		calendar.set(Calendar.MINUTE, endMinutes);
		calendar.set(Calendar.SECOND, endSeconds);
		Date endTimeFormat=calendar.getTime();
		
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.add(Restrictions.between("ratingDate",startTimeFormat,endTimeFormat));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On May 05th, 2016
	 * @param locationApIId
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Get the Location comments perday along with night time(starttime and endtime) for night rated conditions
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserLocationCommentsByLocationIdAndUserIdAndStartTimeAndEndTime(String locationApIId,Integer userId,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		
		DateTime start=new DateTime(startTime);
		int startHours =start.getHour();
		int startMinutes=start.getMinute();
		int startSeconds=start.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, startHours);
		calendar.set(Calendar.MINUTE, startMinutes);
		calendar.set(Calendar.SECOND, startSeconds);
		Date startTimeFormat=calendar.getTime();
		
		
		DateTime end=new DateTime(endTime);
		int endHours =end.getHour();
		int endMinutes=end.getMinute();
		int endSeconds=end.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, endHours);
		calendar.set(Calendar.MINUTE, endMinutes);
		calendar.set(Calendar.SECOND, endSeconds);
		Date endTimeFormat=calendar.getTime();
		
		
		criteria.add(Restrictions.eq("locationAPIId", locationApIId));
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.add(Restrictions.and(Restrictions.isNotNull("comment"),Restrictions.ne("comment", "")));
		criteria.add(Restrictions.between("ratingDate",startTimeFormat,endTimeFormat));
		
		criteria.addOrder(Order.desc("userLocationRatingId"));
		
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On may 06th, 2016
	 * @param kikspotUser
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the geo location ratings for ratings by startTime and end Time
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGeoLocationRatings> getUserGeoLocationRatingsByAndStartTimeAndEndTime(KikspotUser kikspotUser,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserGeoLocationRatings()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGeoLocationRatings.class);
		
		Calendar calendar = Calendar.getInstance();

		DateTime start=new DateTime(startTime);
		int startHours =start.getHour();
		int startMinutes=start.getMinute();
		int startSeconds=start.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, startHours);
		calendar.set(Calendar.MINUTE, startMinutes);
		calendar.set(Calendar.SECOND, startSeconds);
		Date startTimeFormat=calendar.getTime();
		
		
		DateTime end=new DateTime(endTime);
		int endHours =end.getHour();
		int endMinutes=end.getMinute();
		int endSeconds=end.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, endHours);
		calendar.set(Calendar.MINUTE, endMinutes);
		calendar.set(Calendar.SECOND, endSeconds);
		Date endTimeFormat=calendar.getTime();
		
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.add(Restrictions.between("geoRatingDate",startTimeFormat,endTimeFormat));
		
		ArrayList<UserGeoLocationRatings> userGeoLocationRatings=(ArrayList<UserGeoLocationRatings>) criteria.list();
		if(!userGeoLocationRatings.isEmpty()){
			return userGeoLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya On May 16th, 2016
	 * 
	 * @param kikspotUser
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the User Ratings per Day By Strt time And Time Based On user Id
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserRatingsPerDayByStartTimeAndEndTimeAndUser(KikspotUser kikspotUser,Date startTime,Date endTime,String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserRatingsPerDayByStartTimeAndEndTime");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		if(null!=startTime && null!=endTime){
			DateTime start=new DateTime(startTime);
			int startHours =start.getHour();
			int startMinutes=start.getMinute();
			int startSeconds=start.getSeconds();
			
			calendar.set(Calendar.HOUR_OF_DAY, startHours);
			calendar.set(Calendar.MINUTE, startMinutes);
			calendar.set(Calendar.SECOND, startSeconds);
			Date startTimeFormat=calendar.getTime();
			
			
			DateTime end=new DateTime(endTime);
			int endHours =end.getHour();
			int endMinutes=end.getMinute();
			int endSeconds=end.getSeconds();
			
			calendar.set(Calendar.HOUR_OF_DAY, endHours);
			calendar.set(Calendar.MINUTE, endMinutes);
			calendar.set(Calendar.SECOND, endSeconds);
			Date endTimeFormat=calendar.getTime();
			criteria.add(Restrictions.between("ratingDate",startTimeFormat,endTimeFormat));
		}
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On May 16th, 2016
	 * 
	 * @param kikspotUser
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * If the start time and end time are not null -->Method For get the User Comments By start Time and Time within a day
	 * otherwise get the user comments on the current day.
	 */
	@SuppressWarnings("unchecked")
	
	public ArrayList<UserLocationRatings> getUserCommentsPerDayByStartTimeAndEndTimeAndUser(KikspotUser kikspotUser,Date startTime,Date endTime,String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserRatingsPerDayByStartTimeAndEndTime");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.add(Restrictions.and(Restrictions.isNotNull("comment"),Restrictions.ne("comment", "")));
		if(null!=startTime && null!=endTime){
			DateTime start=new DateTime(startTime);
			int startHours =start.getHour();
			int startMinutes=start.getMinute();
			int startSeconds=start.getSeconds();
			
			calendar.set(Calendar.HOUR_OF_DAY, startHours);
			calendar.set(Calendar.MINUTE, startMinutes);
			calendar.set(Calendar.SECOND, startSeconds);
			Date startTimeFormat=calendar.getTime();
			
			
			DateTime end=new DateTime(endTime);
			int endHours =end.getHour();
			int endMinutes=end.getMinute();
			int endSeconds=end.getSeconds();
			
			calendar.set(Calendar.HOUR_OF_DAY, endHours);
			calendar.set(Calendar.MINUTE, endMinutes);
			calendar.set(Calendar.SECOND, endSeconds);
			Date endTimeFormat=calendar.getTime();
			criteria.add(Restrictions.between("ratingDate",startTimeFormat,endTimeFormat));
		}
		criteria.addOrder(Order.desc("userLocationRatingId"));
		
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
		
	}
	
	
	/***
	 * Created By Bhagya on may 05th, 2016
	 * @param userId
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the night spot user ratings
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLocationRatings> getUserNightLocationRatingsByUserIdAndLocationAPIIDStartTimeAndEndTime(String locationAPIId,Integer userId,Date startTime,Date endTime) throws UserLocationRatingsNotFoundException{
		log.info("inside getUserLocationRatingsByLocationAPIId()");
		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date fromDate = calendar.getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Date toDate = calendar.getTime();
			
		DateTime start=new DateTime(startTime);
		int startHours =start.getHour();
		int startMinutes=start.getMinute();
		int startSeconds=start.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, startHours);
		calendar.set(Calendar.MINUTE, startMinutes);
		calendar.set(Calendar.SECOND, startSeconds);
		Date startTimeFormat=calendar.getTime();
		
		
		DateTime end=new DateTime(endTime);
		int endHours =end.getHour();
		int endMinutes=end.getMinute();
		int endSeconds=end.getSeconds();
		
		calendar.set(Calendar.HOUR_OF_DAY, endHours);
		calendar.set(Calendar.MINUTE, endMinutes);
		calendar.set(Calendar.SECOND, endSeconds);
		Date endTimeFormat=calendar.getTime();
		
		criteria.add(Restrictions.eq("locationAPIId", locationAPIId));
		criteria.add(Restrictions.eq("kikspotUser.userId", userId));
		criteria.add(Restrictions.between("ratingDate",fromDate,toDate));
		criteria.add(Restrictions.between("ratingDate",startTimeFormat,endTimeFormat));
		criteria.addOrder(Order.desc("userLocationRatingId"));
		ArrayList<UserLocationRatings> usersLocationRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!usersLocationRatings.isEmpty()){
			return usersLocationRatings;
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
	}




	@Override
	public RecommendationLocationImages getMostRecentRecommendationLocationImageByKikspotRecommendationLocation(
			KikspotRecommendationLocation recommendationLocation) throws RecommendationLocationImagesNotFoundException {
		log.info("inside getMostRecentRecommendationLocationImageByKikspotRecommendationLocation()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(RecommendationLocationImages.class);
		criteria.add(Restrictions.eq("kikspotRecommendationLocation.locationId", recommendationLocation.getLocationId()));
		ArrayList<RecommendationLocationImages> recommendationLocationImages=(ArrayList<RecommendationLocationImages>) criteria.list();
		if(!recommendationLocationImages.isEmpty()){
			return recommendationLocationImages.get(recommendationLocationImages.size()-1);
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
		
	}
	
	/**
	 * Created By BHagya on april 17th, 2018R
	 * @param recommendationLocation
	 * @return
	 * @throws 
	 * 
	 * Method for to get the most recent recommendation location image
	 */
	@SuppressWarnings("unchecked")
	public UserRecommendationLocationImages getMostRecentUserLocationImageByUserIdAndLocationId(String locationId,KikspotUser kikspotUser,Integer userLocationRatingId) throws RecommendationLocationImagesNotFoundException {
		log.info("inside getMostRecentUserLocationImageByUserIdAndLocationId()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserRecommendationLocationImages.class);
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.add(Restrictions.eq("locationId", locationId));
		criteria.add(Restrictions.eq("userLocationRatingId",userLocationRatingId));
		ArrayList<UserRecommendationLocationImages> recommendationLocationImages=(ArrayList<UserRecommendationLocationImages>) criteria.list();
		if(!recommendationLocationImages.isEmpty()){
			return recommendationLocationImages.get(recommendationLocationImages.size()-1);
		}
		else{
			throw new RecommendationLocationImagesNotFoundException();
		}
		
	}
	
	/**
	 * Created By Bhagya on may 14th, 2018
	 * @param scoreData
	 * @return
	 * 
	 * Save Or Update User Score.
	 */

	public Integer saveorUpdateUserScore(ScoreData scoreData) throws Exception{
		log.info("inside saveorUpdateUserScore()");
		sessionFactory.getCurrentSession().saveOrUpdate(scoreData);
		sessionFactory.getCurrentSession().flush();
		return scoreData.getDataId();
	}
	/**
	 * Created By Bhagya on may 14th, 2018
	 * @return
	 * @throws Exception
	 * 
	 * Method for to get the list of users score data
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<ScoreData> getListofUsersScoreData() throws UsersScoreDataNotFoundException{
		log.info("inside getListofUsersScoreData()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(ScoreData.class);
		ArrayList<ScoreData> scoreData=(ArrayList<ScoreData>) criteria.list();
		if(!scoreData.isEmpty()){
			return scoreData;
		}
		else{
			throw new UsersScoreDataNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya on may 14th, 2018
	 * @param venueId
	 * @return
	 * @throws UsersScoreDataNotFoundException
	 * 
	 * Method for to get the list of users score by venueId( locationId)
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<ScoreData> getListofUsersScoreDataByVenueId(String venueId) throws UsersScoreDataNotFoundException{
		log.info("inside getListofUsersScoreData()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(ScoreData.class);
		criteria.add(Restrictions.eq("venueId", venueId));
		ArrayList<ScoreData> scoreData=(ArrayList<ScoreData>) criteria.list();
		if(!scoreData.isEmpty()){
			return scoreData;
		}
		else{
			throw new UsersScoreDataNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya on may 15th, 2018
	 * @param apiData
	 * @return
	 * @throws Exception
	 * 
	 * Method for to save or update the api score.
	 */
	public String saveorUpdateAPIScoreForVenue(ApiData apiData) throws Exception{
		log.info("inside saveorUpdateAPIScoreForVenue");
		sessionFactory.getCurrentSession().saveOrUpdate(apiData);
		sessionFactory.getCurrentSession().flush();
		return apiData.getVenueId();
	}
	/**
	 * Created By Bhagya on may 15th, 2018
	 * @param venueId
	 * @return
	 * @throws UsersScoreDataNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ApiData getApiScorebyVenueId(String venueId) throws ApiScoreDataNotFoundException{
		log.info("inside getApiScorebyVenueId");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(ApiData.class);
		criteria.add(Restrictions.eq("venueId", venueId));
		ArrayList<ApiData> apiData=(ArrayList<ApiData>) criteria.list();
		if(!apiData.isEmpty()){
			return apiData.get(0);
		}
		else{
			throw new ApiScoreDataNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya on May 28th, 2018
	 * @param venueScoreData
	 * @return
	 * @throws Exception
	 * 
	 * Method for to save or update venue score data
	 */
	
	
	public Integer saveOrUpdateVenueScoreData(VenueScoreData venueScoreData)throws Exception{
		log.info("inside saveOrUpdateVenueScoreData() ");
		sessionFactory.getCurrentSession().saveOrUpdate(venueScoreData);
		sessionFactory.getCurrentSession().flush();
		return venueScoreData.getDataId();
	}
	
	/**
	 * Created By bhagya on may 28th, 2018
	 * @param venueId
	 * @return
	 * @throws VenueScoreDataNotFoundException
	 * 
	 * Method for to get the VenueScore by venueId
	 */
	@SuppressWarnings("unchecked")
	public VenueScoreData getVenueScoreByVenueId(String venueId) throws VenueScoreDataNotFoundException{
		log.info("inside getVenueScoreByVenueId() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(VenueScoreData.class);
		criteria.add(Restrictions.eq("venueId", venueId));
		ArrayList<VenueScoreData> venueScoreData=(ArrayList<VenueScoreData>) criteria.list();
		if(!venueScoreData.isEmpty()){
			return venueScoreData.get(0);
		}
		else{
			throw new VenueScoreDataNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on may 28th, 2018
	 * @param venueId
	 * @return
	 * @throws VenueScoreDataNotFoundException
	 * 
	 * Method for to get the venue score of 30 mins ago data by venueid 
	 */
	@SuppressWarnings("unchecked")
	public VenueScoreData getVenueScoreOf30MinutesAgoByVenueId(String venueId) throws VenueScoreDataNotFoundException{
		log.info("inside getVenueScoreOf30MinutesAgoByVenueId() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(VenueScoreData.class);
		Calendar thirtyminutesBefore = Calendar.getInstance();
		thirtyminutesBefore.add(Calendar.MINUTE, -30);
		criteria.add(Restrictions.eq("venueId", venueId));
		criteria.add(Restrictions.between("scoreDate", thirtyminutesBefore.getTime(), new Date()));
		ArrayList<VenueScoreData> venueScoreData=(ArrayList<VenueScoreData>) criteria.list();
		if(!venueScoreData.isEmpty()){
	
			return venueScoreData.get(0);
		}
		else{
		
			throw new VenueScoreDataNotFoundException();
		}
	}
	/**
	 * 
	 * Created By Bhagya on august 13th , 2018
	 * @param level
	 * @return
	 * @throws KikspotRewardNotFoundException
	 * 
	 * Getting the active reward by game level
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotRewards> getTheActiveRewardsByLevelAndLocationId(String level,String locationId) throws KikspotRewardNotFoundException{
		log.info("inside RecommendationDaoImpl -> getTheActiveRewardsByLevel() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotRewards.class);
		Calendar today=Calendar.getInstance();
		criteria.add(Restrictions.eq("level",level));
		criteria.add(Restrictions.eq("locationId",locationId));
		criteria.add(Restrictions.eq("isActive",true));
		criteria.add(Restrictions.le("startDate", new Date()));
		criteria.add(Restrictions.ge("endDate", new Date()));
		ArrayList<KikspotRewards> kikspotRewards=(ArrayList<KikspotRewards>) criteria.list();
		if(!kikspotRewards.isEmpty()){
			return kikspotRewards;
		}
		else{
			throw new KikspotRewardNotFoundException();
		}
		
				
	}
	/**
	 * Created By Bhagya on April 02nd, 2019
	 * @param locationAPIId
	 * @return
	 * @throws UserLocationRatingsNotFoundException
	 * 
	 * Method for to get the user location ratings by locationId
	 */
	
	@SuppressWarnings("unchecked")
	public Integer getUserRatingByLocationId(String locationAPIId) throws UserLocationRatingsNotFoundException{
		log.info("inside RecommendationDaoImpl -> getUserRatingByLocationId() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocationRatings.class);
		criteria.add(Restrictions.eq("locationAPIId",locationAPIId));
		criteria.add(Restrictions.or(Restrictions.isNull("comment"),Restrictions.eq("comment","")));
		ArrayList<UserLocationRatings> userRatings=(ArrayList<UserLocationRatings>) criteria.list();
		if(!userRatings.isEmpty()){
			return userRatings.size();
		}
		else{
			throw new UserLocationRatingsNotFoundException();
		}
		
	}
	/**
	 * Created By bhagya On May 21st, 2019
	 * @return
	 * @throws Exception
	 * 
	 * Method for to get the master geo location coordinates
	 */
	@SuppressWarnings("unchecked")
	public MasterGeoLocationConfiguration getMasterGeoLocationConfiguration()throws Exception{
		log.info("inside getMasterGeoLocationConfiguration()");
		ArrayList<MasterGeoLocationConfiguration> configurations=new ArrayList<MasterGeoLocationConfiguration>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(MasterGeoLocationConfiguration.class);
		configurations=(ArrayList<MasterGeoLocationConfiguration>) criteria.list();
		return configurations.get(0);
	}
	
	/**
	 * Created By bhagya On may 21st, 2019
	 * 
	 * @param configuration
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateMasterGeoLocationConfiguration(MasterGeoLocationConfiguration configuration)throws Exception{
		log.info("inside saveorUpdateMasterGeoLocationConfiguration()");
		sessionFactory.getCurrentSession().saveOrUpdate(configuration);
		sessionFactory.getCurrentSession().flush();
		return configuration.getMasterId();
	}
}



