package com.kikspot.backend.recommendation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/***
 * Created by Jeevan on FEBRUARY 03, 2016
 * 
 * Model to hold all 
 * @author KNS-ACCONTS
 *
 */
@Entity
@Table(name="local_API_recommendation_locations")
public class LocalAPIRecommendationLocations implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue
	@Column(name="local_API_recommendation_location_id")
	private Integer localAPIRecommendationLocationId;
	
	

	@Column(name="location_API_id",unique=true)
	private String locationAPIId;
	
	
	@Column(name="location_name")
	private String locationName;
	
	@Column(name="latitude")
	private Double latitude;
	
	
	@Column(name="longitude")
	private Double longitude;
	
	
	
	@Column(name="ratings")
	private Double ratings;
	
	
	@Column(name="icon_url")
	private String iconUrl;
	
	
	@Column(name="address")
	private String address;
	
	
	@Column(name="phone_no")
	private String phoneNo;
	
	@Column(name="is_new")
	private Boolean isNew;
	
	@Column(name="distance")
	private Double distance;
	
	@Column(name="url")
	private String url;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="location_date")
	private Date locationDate;
	
	@Column(name="location_type")
	private String locationType;
	
	
	@Column(name="image")
	private String image;
	
	
	
	@Transient
	private Integer totalLocations;



	public Integer getLocalAPIRecommendationLocationId() {
		return localAPIRecommendationLocationId;
	}



	public void setLocalAPIRecommendationLocationId(
			Integer localAPIRecommendationLocationId) {
		this.localAPIRecommendationLocationId = localAPIRecommendationLocationId;
	}


	


	public String getLocationAPIId() {
		return locationAPIId;
	}



	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}



	public String getLocationName() {
		return locationName;
	}



	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}



	public Double getLatitude() {
		return latitude;
	}



	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}



	public Double getLongitude() {
		return longitude;
	}



	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}



	public Double getRatings() {
		return ratings;
	}



	public void setRatings(Double ratings) {
		this.ratings = ratings;
	}



	public String getIconUrl() {
		return iconUrl;
	}



	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPhoneNo() {
		return phoneNo;
	}



	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}



	public Boolean getIsNew() {
		return isNew;
	}



	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}



	public Double getDistance() {
		return distance;
	}



	public void setDistance(Double distance) {
		this.distance = distance;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	public Date getLocationDate() {
		return locationDate;
	}



	public void setLocationDate(Date locationDate) {
		this.locationDate = locationDate;
	}



	public String getLocationType() {
		return locationType;
	}



	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



	public Integer getTotalLocations() {
		return totalLocations;
	}



	public void setTotalLocations(Integer totalLocations) {
		this.totalLocations = totalLocations;
	}
	
}
