package com.kikspot.backend.recommendation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
/**
 * Created By Bhagya On december 18th,2015
 *	Model For Kikspot Recommendation Location
 *	
 *	Saving the locations which are added by admin
 */
@Entity
@Table(name="kikspot_recommendation_location")
public class KikspotRecommendationLocation implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="location_id")
	private Integer locationId;
	
	@Column(name="google_location_id",unique=true)
	private String googleLocationId;
	
	
	@Column(name="location_name")
	private String locationName;
	
	@Column(name="latitude")
	private Double latitude;
	
	
	@Column(name="longitude")
	private Double longitude;
	
	
	
	@Column(name="ratings")
	private Double ratings;
	
	
	@Column(name="icon_url")
	private String iconUrl;
	
	
	@Column(name="address")
	private String address;
	
	
	@Column(name="phone_no")
	private String phoneNo;
	
	@Column(name="is_new")
	private Boolean isNew;
	
	@Column(name="distance")
	private Double distance;
	
	@Column(name="url")
	private String url;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="location_date")
	private Date locationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="close_date")
	private Date closeDate;
	
	@Column(name="is_open")
	private Boolean isOpen;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="open_date")
	private Date openDate;
	
	@Column(name="is_visible")
	private Boolean visible;
	
	@Column(name="reason")
	private String reason;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_modified_date")
	private Date statusModifiedDate;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="modified_by")
	private String modifiedBy;
	
	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public Boolean getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}

	@Transient
	private Integer totalLocations;
	
	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

		

	public String getGoogleLocationId() {
		return googleLocationId;
	}

	public void setGoogleLocationId(String googleLocationId) {
		this.googleLocationId = googleLocationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getRatings() {
		return ratings;
	}

	public void setRatings(Double ratings) {
		this.ratings = ratings;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getTotalLocations() {
		return totalLocations;
	}

	public void setTotalLocations(Integer totalLocations) {
		this.totalLocations = totalLocations;
	}

	public Date getLocationDate() {
		return locationDate;
	}

	public void setLocationDate(Date locationDate) {
		this.locationDate = locationDate;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getStatusModifiedDate() {
		return statusModifiedDate;
	}

	public void setStatusModifiedDate(Date statusModifiedDate) {
		this.statusModifiedDate = statusModifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
}