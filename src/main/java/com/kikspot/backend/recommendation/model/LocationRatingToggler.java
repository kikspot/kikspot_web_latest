package com.kikspot.backend.recommendation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Created by Jeevan on Feb 04, 2016
 * 
 * Model for Toggling Location Ratings to Enable Rating Locatoin from All locations or permit to a single location..
 * @author KNS-ACCONTS
 *
 */

@Entity
@Table(name="location_rating_toggler")
public class LocationRatingToggler implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="location_rating_toggler_id")
	private Integer locationRatingTogglerId;
	
	
	@Column(name="allow_global_rating")
	private Boolean allowGlobalRating;


	public Integer getLocationRatingTogglerId() {
		return locationRatingTogglerId;
	}


	public void setLocationRatingTogglerId(Integer locationRatingTogglerId) {
		this.locationRatingTogglerId = locationRatingTogglerId;
	}


	public Boolean getAllowGlobalRating() {
		return allowGlobalRating;
	}


	public void setAllowGlobalRating(Boolean allowGlobalRating) {
		this.allowGlobalRating = allowGlobalRating;
	}

	
	

}

