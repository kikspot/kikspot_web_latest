package com.kikspot.backend.recommendation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.kikspot.backend.user.model.KikspotUser;


/***
 * 
 * Created by Jeevan on Novemeber 20, 2015
 * 
 * @author KNS-ACCONTS
 *
 *
 * Model for UserLocationRatings..
 * 
 *
 */
@Entity
@Table(name="user_location_ratings",uniqueConstraints=@UniqueConstraint(columnNames={"user_id","location_id"}))
public class UserLocationRatings implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="user_location_rating_id")
	private Integer userLocationRatingId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;

	
	@ManyToOne
	@JoinColumn(name="location_id")
	private RecommendationLocation recommendationLocation;
	
	@Column(name="rating")
	private Integer rating;
	
	
	@Column(name="comment")
	private String comment;
	
	
	@Column(name="rating_date")
	private Date ratingDate;
	
	
	@Column(name="location_API_ID")
	private String locationAPIId;
	
	

	@Transient
	private Integer totalRatings;
	
	

	public Integer getTotalRatings() {
		return totalRatings;
	}


	public void setTotalRatings(Integer totalRatings) {
		this.totalRatings = totalRatings;
	}


	public Integer getUserLocationRatingId() {
		return userLocationRatingId;
	}


	public void setUserLocationRatingId(Integer userLocationRatingId) {
		this.userLocationRatingId = userLocationRatingId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public RecommendationLocation getRecommendationLocation() {
		return recommendationLocation;
	}


	public void setRecommendationLocation(
			RecommendationLocation recommendationLocation) {
		this.recommendationLocation = recommendationLocation;
	}


	public Integer getRating() {
		return rating;
	}


	public void setRating(Integer rating) {
		this.rating = rating;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public Date getRatingDate() {
		return ratingDate;
	}


	public void setRatingDate(Date ratingDate) {
		this.ratingDate = ratingDate;
	}


	public String getLocationAPIId() {
		return locationAPIId;
	}


	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}
	
	
	
	
	

}
