package com.kikspot.backend.recommendation.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kikspot.backend.user.model.KikspotUser;
/**
 * Created By Bhagya on may 14th, 2018
 * @author KNS-ACCONTS
 * 
 */
@Entity
@Table(name="score_data")
public class ScoreData implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="data_id")
	private Integer dataId;
	
	@Column(name="venue_id")
	private String venueId;
	
	@Column(name="date_stamp")
	private Date dateStamp;
	
	@Column(name="time_stamp")
	private Date timeStamp;
	
	@Column(name="user_score")
	private Integer userScore;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public Date getDateStamp() {
		return dateStamp;
	}

	public void setDateStamp(Date dateStamp) {
		this.dateStamp = dateStamp;
	}

	
	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Integer getUserScore() {
		return userScore;
	}

	public void setUserScore(Integer userScore) {
		this.userScore = userScore;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}
	
	
	
}