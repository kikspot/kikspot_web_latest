package com.kikspot.backend.recommendation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Created by Jeevan on MARCH 03, 2016
 * Model to Configure Recommendations from Admin..
 * 
 * @author KNS-ACCONTS
 * 
 *  Wanted to go for Key - Value approach but doubted ability of Firdous to handle it, so going for more simpler approach
 *
 */
@Entity
@Table(name="recommendation_Configuration")
public class RecommendationConfiguration  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="recommendation_configuration_id")
	private Integer recommendationConfigurationId;
	
	@Column(name="is_bar")
	private Boolean isBar;
	
	
	@Column(name="is_pub")
	private Boolean isPub;
	
	@Column(name="is_night_club")
	private Boolean isNightClub;

	@Column(name="is_lounge")
	private Boolean isLounge;
	
	@Column(name="is_casino")
	private Boolean isCasino;
	
	@Column(name="is_bowling_alley")
	private Boolean isBowlingAlley;

	public Integer getRecommendationConfigurationId() {
		return recommendationConfigurationId;
	}

	public void setRecommendationConfigurationId(
			Integer recommendationConfigurationId) {
		this.recommendationConfigurationId = recommendationConfigurationId;
	}

	public Boolean getIsBar() {
		return isBar;
	}

	public void setIsBar(Boolean isBar) {
		this.isBar = isBar;
	}

	public Boolean getIsPub() {
		return isPub;
	}

	public void setIsPub(Boolean isPub) {
		this.isPub = isPub;
	}

	public Boolean getIsNightClub() {
		return isNightClub;
	}

	public void setIsNightClub(Boolean isNightClub) {
		this.isNightClub = isNightClub;
	}

	public Boolean getIsLounge() {
		return isLounge;
	}

	public void setIsLounge(Boolean isLounge) {
		this.isLounge = isLounge;
	}

	public Boolean getIsCasino() {
		return isCasino;
	}

	public void setIsCasino(Boolean isCasino) {
		this.isCasino = isCasino;
	}

	public Boolean getIsBowlingAlley() {
		return isBowlingAlley;
	}

	public void setIsBowlingAlley(Boolean isBowlingAlley) {
		this.isBowlingAlley = isBowlingAlley;
	}
	
	
}
