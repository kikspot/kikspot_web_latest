package com.kikspot.backend.recommendation.model;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;




/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on November 20, 2015
 *  Model for Recommendation Locations..
 *  
 *  All Data captured from Location related API's, i.e Yelp/Google Places would be persisted here.
 *  
 *
 */

@Entity
@Table(name="recommendation_location")
public class RecommendationLocation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="location_id")
	private Integer locationId;
	
	
	
	@Column(name="location_API_Id",unique=true)
	private String locationAPIId;
	
	
	@Column(name="location_name")
	private String locationName;
	
	@Column(name="latitude")
	private Double latitude;
	
	
	@Column(name="longitude")
	private Double longitude;
	
	
	
	@Column(name="ratings")
	private Double ratings;
	
	
	@Column(name="icon_url")
	private String iconUrl;
	
	
	@Column(name="image")
	private String image;
	
	
	@Column(name="address")
	private String address;
	
	
	@Column(name="phone_no")
	private String phoneNo;
	
	@Column(name="is_new")
	private Boolean isNew;

	@Column(name="page_token")
	private String pageToken;
	
	
	

	public Integer getLocationId() {
		return locationId;
	}


	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}


	public String getLocationAPIId() {
		return locationAPIId;
	}


	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}





	public Double getRatings() {
		return ratings;
	}


	public void setRatings(Double ratings) {
		this.ratings = ratings;
	}


	public String getIconUrl() {
		return iconUrl;
	}


	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhoneNo() {
		return phoneNo;
	}


	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	public Boolean getIsNew() {
		return isNew;
	}


	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}


	public String getPageToken() {
		return pageToken;
	}


	public void setPageToken(String pageToken) {
		this.pageToken = pageToken;
	}
	
	

}
