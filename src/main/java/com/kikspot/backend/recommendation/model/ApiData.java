package com.kikspot.backend.recommendation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created By Bhagya on may 14th, 2018
 * @author KNS-ACCONTS
 *	Model class for to store the venue scores
 */
@Entity
@Table(name="api_data")
public class ApiData implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="data_id")
	private Integer dataId;
	
	@Column(name="venue_id")
	private String venueId;
	
	@Column(name="api_score")
	private Double apiScore;

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public Double getApiScore() {
		return apiScore;
	}

	public void setApiScore(Double apiScore) {
		this.apiScore = apiScore;
	}

	
	
	
}