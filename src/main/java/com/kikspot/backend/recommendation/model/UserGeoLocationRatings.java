package com.kikspot.backend.recommendation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.kikspot.backend.user.model.KikspotUser;


/***
 * 
 * Created by Jeevan on Novemeber 20, 2015
 * 
 * @author KNS-ACCONTS
 *
 *
 * Model for UserLocationRatings..
 * 
 *
 */
@Entity
@Table(name="user_geolocation_ratings")
public class UserGeoLocationRatings implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="user_geolocation_rating_id")
	private Integer userGeoLocationRatingId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;

		
	@Column(name="latitude")
	private Double latitude;
	
	
	@Column(name="longitude")
	private Double longitude;
	
	
	@Column(name="geo_rating_date")
	private Date geoRatingDate;


	public Integer getUserGeoLocationRatingId() {
		return userGeoLocationRatingId;
	}


	public void setUserGeoLocationRatingId(Integer userGeoLocationRatingId) {
		this.userGeoLocationRatingId = userGeoLocationRatingId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}


	public Date getGeoRatingDate() {
		return geoRatingDate;
	}


	public void setGeoRatingDate(Date geoRatingDate) {
		this.geoRatingDate = geoRatingDate;
	}
	
	
	
	

}
