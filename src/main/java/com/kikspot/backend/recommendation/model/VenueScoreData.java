package com.kikspot.backend.recommendation.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created By Bhagya on may 14th, 2018
 * @author KNS-ACCONTS
 * 
 */
@Entity
@Table(name="venue_score_data")
public class VenueScoreData implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="data_id")
	private Integer dataId;
	
	@Column(name="venue_id")
	private String venueId;
	
	@Column(name="venue_score")
	private Double venueScore;
	
	@Column(name="score_date")
	private Date scoreDate;
	
	

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public Double getVenueScore() {
		return venueScore;
	}

	public void setVenueScore(Double venueScore) {
		this.venueScore = venueScore;
	}

	public Date getScoreDate() {
		return scoreDate;
	}

	public void setScoreDate(Date scoreDate) {
		this.scoreDate = scoreDate;
	}

	
	
	
}