package com.kikspot.backend.recommendation.model;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 
 * @author KNS-ACCONTS
 *
 *	Created by Jeevan on Novemeber 20, 2015
 *
 *   Model for Recommendation Location User snapped Images...
 *   
 *
 */
@Entity
@Table(name="recommendation_location_images")
public class RecommendationLocationImages implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue
	@Column(name="location_image_id")
	private Integer locationImageId;

	
	@ManyToOne
	@JoinColumn(name="location_id")
	private KikspotRecommendationLocation kikspotRecommendationLocation;
	
	
	@Column(name="image")
	private String image;


	
	
	public Integer getLocationImageId() {
		return locationImageId;
	}


	public void setLocationImageId(Integer locationImageId) {
		this.locationImageId = locationImageId;
	}

	
	
	

	public KikspotRecommendationLocation getKikspotRecommendationLocation() {
		return kikspotRecommendationLocation;
	}


	public void setKikspotRecommendationLocation(KikspotRecommendationLocation kikspotRecommendationLocation) {
		this.kikspotRecommendationLocation = kikspotRecommendationLocation;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}
	
	
	
	
	
	
}
