package com.kikspot.backend.admin.dao;

import java.util.ArrayList;



import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;

/**
 * Created By Bhagya On december 03rd,2015
 *	Dao Implementation class for Admin Dao
 */

@Transactional
@Repository("adminDao")
public class AdminDaoImpl implements AdminDao{
	
	private static Logger log=Logger.getLogger(AdminDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
}