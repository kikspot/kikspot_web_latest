package com.kikspot.backend.admin.model;

import java.io.Serializable;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;




/**
 * Created By Bhagya on May 21st,2019
 *  Entity class for Master Geo Location
 *  
 *  
 *   
 */

@Entity

@Table(name ="master_geolocation_configuration")
public class MasterGeoLocationConfiguration implements Serializable{

	private static final long serialVersionUID = 1L;

	
	
	@Id
	@GeneratedValue
	@Column(name="master_id")
	private Integer masterId;
	
	
	@Column(name="latitude")
	private Double latitude;
	
	
	@Column(name="longitude")
	private Double longitude;


	public Integer getMasterId() {
		return masterId;
	}


	public void setMasterId(Integer masterId) {
		this.masterId = masterId;
	}


	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	
	
	
	
	
	
}