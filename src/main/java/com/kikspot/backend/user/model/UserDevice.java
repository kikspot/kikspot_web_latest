package com.kikspot.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


/**
 * Created by Jeevan on DEcember 11, 2015
 * 
 *  Model to Preserver User Device n Device Token Details..
 *  
 *  
 * @author KNS-ACCONTS
 *
 */
@Entity
@Table(name="user_device")
public class UserDevice implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="user_device_id")
	private Integer userDeviceId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;

	
	@Column(name="device_token")
	private String deviceToken;

	@Column(name="device_type")
	private String deviceType;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;


	public Integer getUserDeviceId() {
		return userDeviceId;
	}


	public void setUserDeviceId(Integer userDeviceId) {
		this.userDeviceId = userDeviceId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public String getDeviceToken() {
		return deviceToken;
	}


	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}


	public String getDeviceType() {
		return deviceType;
	}


	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
	
	


}
