package com.kikspot.backend.user.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


/**
 * 
 *  Created by Jeevan on November 17, 2015
 *  
 *   Model for User Preference Setting...
 * 	@author KNS-ACCONTS
 *
 *	
 *	User Preferences have Preference Item
 *   
 *  User Preferences Options have Option details of User Preferences..
 *   
 *  User Preferences Setting have User Configured setting info.. 
 *
 *
 */


@Entity
@Table(name="user_preference_setting")
public class UserPreferenceSetting implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="user_preference_setting_id")
	private Integer userPreferenceSettingId;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	
	@ManyToOne
	@JoinColumn(name="user_preference_id")
	private UserPreferences userPreferences;
	
	
	@ManyToOne
	@JoinColumn(name="user_preference_options_id")
	private UserPreferenceOptions userPreferenceOptions;

	
	

	public Integer getUserPreferenceSettingId() {
		return userPreferenceSettingId;
	}


	public void setUserPreferenceSettingId(Integer userPreferenceSettingId) {
		this.userPreferenceSettingId = userPreferenceSettingId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public UserPreferences getUserPreferences() {
		return userPreferences;
	}


	public void setUserPreferences(UserPreferences userPreferences) {
		this.userPreferences = userPreferences;
	}


	public UserPreferenceOptions getUserPreferenceOptions() {
		return userPreferenceOptions;
	}


	public void setUserPreferenceOptions(UserPreferenceOptions userPreferenceOptions) {
		this.userPreferenceOptions = userPreferenceOptions;
	}
	
	
	

}
