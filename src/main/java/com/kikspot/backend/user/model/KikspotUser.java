package com.kikspot.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;



/**
 * Created By Bhagya on october 13th,2015
 *  Entity class for kikspot user
 *  
 *   Modified by Jeevan on Novemebr 17, 2015..
 *   
 */

@Entity
/*@Table(name ="kikspot_user", catalog = "kikspot_new")*/
@Table(name ="kikspot_user")
public class KikspotUser implements Serializable{

	private static final long serialVersionUID = 1L;

	
	
	@Id
	@GeneratedValue
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	private String password;
	
	@Column(name="email_id")
	private String emailId;
	
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth")
	private Date dob;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="account_creation_date")
	private Date accountCreationDate;
	
	@Column(name="address_line1")
	private String address1;
	
	@Column(name="address_line2")
	private String address2;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	


	@Column(name="zip_code")
	private String zip;
	
	@Column(name="password_token")
	private String passwordToken;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="password_token_expiry_date")
	private Date passwordTokenExpiryDate;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="user_role")
	private UserRole userRole;
	
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="user_type")
	private UserType userType;

	
	@ManyToOne()
	@JoinTable(name="kns_referred_user",joinColumns=@JoinColumn(name="user_id"),inverseJoinColumns=@JoinColumn(name="referral_id"))
	private KikspotUser referredUser;

	
	@Column(name="profile_picture")
	private String profilePicture;
	
	
	@Column(name="preferred_venue_type")
	private String preferredVenueType;
	
	@Column(name="referral_code")
	private String referralCode;
	
	@Transient
	private Integer totalUsers;
	
	/**
	 * Added isActive and Reactivation Date Columns By Bhagya
	 * 	on december 09th,2015
	*/
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="reactivation_date")
	private Date reactivationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_login_date")
	private Date lastLoginDate;
	
	@Column(name="is_master_geolocation")
	private Boolean isMasterGeoLocation;
	
	@Column(name="app_version")
	private String appVersion;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}


	public Date getAccountCreationDate() {
		return accountCreationDate;
	}


	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public String getPasswordToken() {
		return passwordToken;
	}


	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}


	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}


	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}


	public UserRole getUserRole() {
		return userRole;
	}


	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}


	public UserType getUserType() {
		return userType;
	}


	public void setUserType(UserType userType) {
		this.userType = userType;
	}


	public KikspotUser getReferredUser() {
		return referredUser;
	}


	public void setReferredUser(KikspotUser referredUser) {
		this.referredUser = referredUser;
	}


	public String getProfilePicture() {
		return profilePicture;
	}


	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}


	public Integer getTotalUsers() {
		return totalUsers;
	}


	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public Date getReactivationDate() {
		return reactivationDate;
	}


	public void setReactivationDate(Date reactivationDate) {
		this.reactivationDate = reactivationDate;
	}


	public String getPreferredVenueType() {
		return preferredVenueType;
	}


	public void setPreferredVenueType(String preferredVenueType) {
		this.preferredVenueType = preferredVenueType;
	}
	public String getReferralCode() {
		return referralCode;
	}


	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Boolean getIsMasterGeoLocation() {
		return isMasterGeoLocation;
	}

	public void setIsMasterGeoLocation(Boolean isMasterGeoLocation) {
		this.isMasterGeoLocation = isMasterGeoLocation;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	
	

	
	
	
	
	
	
	
}