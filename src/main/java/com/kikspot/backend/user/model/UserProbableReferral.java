package com.kikspot.backend.user.model;

import java.io.Serializable;




import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



/**
 * 
 * @author KNS-ACCONTS
 *
 *
 *	
 */
@Entity
@Table(name="user_probable_referral")
public class UserProbableReferral implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="user_probable_referral_id")
	private Integer userProbableReferralId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	
	@Column(name="referral_email",unique=true)
	private String email;


	public Integer getUserProbableReferralId() {
		return userProbableReferralId;
	}


	public void setUserProbableReferralId(Integer userProbableReferralId) {
		this.userProbableReferralId = userProbableReferralId;
	}


	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}


	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
		
	
	

}
