package com.kikspot.backend.user.dao;

import java.util.ArrayList;
import java.util.List;

import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserDeviceNotFoundException;
import com.kikspot.backend.exceptions.UserLocationNotFoundException;
import com.kikspot.backend.exceptions.UserPreferenceOptionsNotFoundException;
import com.kikspot.backend.exceptions.UserPreferenceSettingNotFoundException;
import com.kikspot.backend.exceptions.UserPreferencesNotFoundException;
import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;


import com.kikspot.backend.game.model.GameRuleTypes;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.user.model.UserLocations;
import com.kikspot.backend.user.model.UserPreferenceOptions;
import com.kikspot.backend.user.model.UserPreferenceSetting;
import com.kikspot.backend.user.model.UserPreferences;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;

/**
 * Created By Bhagya On October 13th,2015
 *	Interface for UserDao, consists of abstract methods
 */

public interface UserDao{
	
	public KikspotUser getKikspotUserByEmail(String email) throws UserNotFoundException;
	public Integer saveOrUpdateKikspotUser(KikspotUser kikspotUser) throws UserNotSavedOrUpdatedException;
	public KikspotUser getKikspotUserforAuthentication(String username)throws UserNotFoundException;
	public KikspotUser getKikspotUserforAuthenticationMobile(String username)throws UserNotFoundException;
	public KikspotUser getKikspotUserByPasswordResetToken(String token) throws UserNotFoundException;
	public KikspotUser getKikspotUserById(Integer id) throws UserNotFoundException;
	public KikspotUser getKikspotUserByUsername(String username)throws UserNotFoundException;
	public UserProbableReferral getUserProbableReferralByEmail(String email)throws UserProbableReferralNotFoundException;
	public Integer saveorUpdateUserProbableReferral(UserProbableReferral userProbableReferral)throws Exception;
	public ArrayList<KikspotUser> getAllUsers(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws UserNotFoundException;
	public ArrayList<KikspotUser> getKikspotUserByUserIds(ArrayList<Integer> userIds)throws UserNotFoundException;
	public Integer saveorUpdateUserDevice(UserDevice userDevice)throws Exception;
	public void deleteUserDevice(UserDevice userDevice)throws Exception;
	public UserDevice getUserDeviceByUserDeviceId(Integer userDeviceId)throws UserDeviceNotFoundException;
	public UserDevice getUserDeviceofUser(KikspotUser kikspotUser)throws UserDeviceNotFoundException;
	public ArrayList<UserDevice> getUserDevicesByUserIds(ArrayList<Integer> userIds)throws UserDeviceNotFoundException;
	public Integer saveorUpdateUserLocation(UserLocations userLocation)throws Exception;
	public UserLocations getUserLocationsOfUser(KikspotUser kikspotUser)throws UserLocationNotFoundException;
	public ArrayList<Integer> getAllUsersNearbytoGivenLocation(Double latitude,Double longitude,Integer radius,String unit)throws UserNotFoundException;
	public ArrayList<KikspotUser> getReferredUsersByKikspotUser(KikspotUser kikspotUser) throws ReferredUsersNotFoundException;
	public ArrayList<UserDevice> getUserDeviceByDeviceTokens(ArrayList<String> deviceTokens)throws UserDeviceNotFoundException;
	public ArrayList<UserDevice> getAllUserDevices()throws UserDeviceNotFoundException;
	public ArrayList<UserPreferences> getListOfAllUserPreferences() throws UserPreferencesNotFoundException;
	public ArrayList<UserPreferenceOptions> getUserPreferenceOptionsByUserPreference(UserPreferences userPreference) throws UserPreferenceOptionsNotFoundException;
	public ArrayList<UserPreferenceSetting> getUserPreferenceSettingsByUserAndUserPreference(KikspotUser kikspotUser,UserPreferences userPreference) throws UserPreferenceSettingNotFoundException;
	public ArrayList<UserPreferenceOptions> getUserPreferenceOptionsByUserAndUserPreference(KikspotUser kikspotUser,UserPreferences userPreference) throws UserPreferenceSettingNotFoundException;
	public ArrayList<UserPreferenceOptions> getUserPreferenceOptionsByOptionIds(ArrayList<Integer> optionIds) throws UserPreferenceOptionsNotFoundException;
	public ArrayList<UserPreferenceSetting> getUserPreferenceSettingsByUserIdAndUserPreference(KikspotUser kikspotUser,UserPreferences userPreference) throws UserPreferenceSettingNotFoundException;
	public Integer deleteUserPreferenceSetting(UserPreferenceSetting userPreferenceSetting) throws Exception;
	public Integer saveUserPreferenceSetting(UserPreferenceSetting userPreferenceSetting)throws Exception;
	public Integer deleteUser(KikspotUser user) throws Exception;
	public ArrayList<GameRuleTypes> getGameRuleTypes() throws Exception;
	public ArrayList<KikspotUser> getAllUsersExceptByPass() throws Exception;
	public KikspotUser getUserByReferralCode(String referralCode) throws ReferredUsersNotFoundException;
	public KikspotUser getKikspotUserByUsernameNotEqualToPassedUserId(String username, Integer userId)throws UserNotFoundException;
	public KikspotUser getKikspotUserByEmailNotEqualToPassedUserId(String username, Integer userId) throws Exception;
	
	public ArrayList<KikspotUser> getAllUsersOfMasterGeoLocated() throws Exception;
	
}