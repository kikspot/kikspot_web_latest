package com.kikspot.backend.user.dao;
/**
 * Created By Bhagya on october 13th,2015
 * Implementation class for UserDao interface
 */
import java.util.ArrayList;










import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import org.apache.log4j.Logger;

import com.kikspot.backend.exceptions.ReferredUsersNotFoundException;
import com.kikspot.backend.exceptions.UserDeviceNotFoundException;
import com.kikspot.backend.exceptions.UserLocationNotFoundException;
import com.kikspot.backend.exceptions.UserPreferenceOptionsNotFoundException;
import com.kikspot.backend.exceptions.UserPreferenceSettingNotFoundException;
import com.kikspot.backend.exceptions.UserPreferencesNotFoundException;
import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;
import com.kikspot.backend.game.model.GameRuleTypes;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserDevice;
import com.kikspot.backend.user.model.UserLocations;
import com.kikspot.backend.user.model.UserPreferenceOptions;
import com.kikspot.backend.user.model.UserPreferenceSetting;
import com.kikspot.backend.user.model.UserPreferences;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.backend.user.model.UserRole;
import com.kikspot.backend.user.model.UserType;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;




@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao{
	
	private static Logger log=Logger.getLogger(UserDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param email
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Method for getting the kikspotUser By Email
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByEmail(String email) throws UserNotFoundException{
		log.info("inside getKikspotUserByEmail()");		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("emailId", email));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	
	
	
	/**
	 * Created by Jeevan on November 23, 2015
	 * 
	 *  Method to get KikspotUserByUsername...
	 *  
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByUsername(String username)throws UserNotFoundException{
		log.info("inside getKikspotUserByUsername() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.ilike("username", username));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  Method to  get User by Username or emaill.
	 *  
	 */ 
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserforAuthentication(String username)throws UserNotFoundException{
		log.info("inside getKikspotUserByUsername() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("emailId", username),Restrictions.eq("username", username)));
		criteria.add(Restrictions.eq("userRole", UserRole.ROLE_ADMIN));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	public KikspotUser getKikspotUserforAuthenticationMobile(String username)throws UserNotFoundException{
		log.info("inside getKikspotUserByUsername() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("emailId", username),Restrictions.eq("username", username)));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param kikspotUser
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Method for save or Update the kikspot User
	 */
	public Integer saveOrUpdateKikspotUser(KikspotUser kikspotUser) throws UserNotSavedOrUpdatedException{
		log.info("inside saveOrUpdateKikspotUser()");
		sessionFactory.getCurrentSession().saveOrUpdate(kikspotUser);
		sessionFactory.getCurrentSession().flush();
		return kikspotUser.getUserId();
	}
	
	
	
	
	/**
	 * Returns the user's  one who has given Password reset token
	 * password reset token sent to the user
	 * 
	 * @param token
	 * @throws UserNotFoundException 
	 * @throws Exception
	 *             if there is no user with given token OR any kind of exception
	 *             while interacting with db
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByPasswordResetToken(String token) throws UserNotFoundException {
		log.info("inside getKikspotUserByPasswordResetToken()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("passwordToken", token));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>) criteria.list();
		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			throw new UserNotFoundException();
		}
	}
	
	
	
	
	/**
	 * Created By Bhagya On October 20th,2015
	 * @param id
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Method for getting kikspot user by Id
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserById(Integer id) throws UserNotFoundException{
		log.info("inside getKikspotUserById()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("userId", id));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	/**
	 * Created by Jeevan on  DECEMBER 29, 2015
	 * 
	 * MEthod to saveorUpdateUserProbableReferral.
	 * 
	 * 
	 * 
	 * @param userProbableReferral
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserProbableReferral(UserProbableReferral userProbableReferral)throws Exception{
		log.info("inside saveorUpdateUserProbableReferral() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userProbableReferral);
		sessionFactory.getCurrentSession().flush();
		return userProbableReferral.getUserProbableReferralId();
	}
	
	
	/**
	 * Created by Jeevan on Novemeber 23, 2015..
	 * 
	 * Method to getUserProbableReferralByEmail..
	 * 
	 * @param email
	 * @return
	 * @throws UserProbableReferralNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserProbableReferral getUserProbableReferralByEmail(String email)throws UserProbableReferralNotFoundException{
		log.info("inside getUserProbableReferralByEmail() ");
		ArrayList<UserProbableReferral> userProbableReferrals=new ArrayList<UserProbableReferral>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserProbableReferral.class);
		criteria.add(Restrictions.eq("email", email));
		userProbableReferrals=(ArrayList<UserProbableReferral>) criteria.list();
		if(userProbableReferrals.size()>0){
			return userProbableReferrals.get(0);
		}
		else{
			throw new UserProbableReferralNotFoundException();
		}
				
	}
	
	/**
	 * Created By Bhagya On december 03rd,2015
	 * @return
	 * @throws UserNotFoundException
	 * Method for getting the list of all users from DB
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getAllUsers(Integer pageNo,Integer pageSize,String sortBy,String searchBy,Boolean ascending) throws UserNotFoundException{
		log.info("inside AdminDaoImpl -> getAllUsers()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("firstName", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("lastName", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("emailId", searchBy,
					MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("username", searchBy,
					MatchMode.ANYWHERE));
						
			criteria.add(disjunction);
		}
		if(null!=sortBy && !sortBy.equalsIgnoreCase("creds")){
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));
			}
			else{
				criteria.addOrder(Order.desc(sortBy));
			}				
		}
		
		Integer totalUsers=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			users.get(0).setTotalUsers(totalUsers);
			return users;
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	/**
	 * Create by Jeevan on FEB 15, 2016
	 * 
	 * Method to getKikspotUserByUserIds..
	 * 
	 * 
	 * @param userIds
	 * @return
	 * @throws UserNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getKikspotUserByUserIds(ArrayList<Integer> userIds)throws UserNotFoundException{
		log.info("inside getKikspotUserByUserIds");
		ArrayList<KikspotUser> kikspotUsers=new ArrayList<KikspotUser>();
		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.in("userId", userIds));
		kikspotUsers=(ArrayList<KikspotUser>) criteria.list();
		if(!kikspotUsers.isEmpty()){
			return kikspotUsers;
		}
		else{
			throw new UserNotFoundException();
		}		
	}
	
	
	
	
/*************************** USER DEVICES  ***************************************/
	
	
	
	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 * 
	 *  Method to Save or Update USerDevice..
	 *  
	 * @param userDevice
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserDevice(UserDevice userDevice)throws Exception{
		log.info("inside saveorUpdateUserDevice() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userDevice);
		sessionFactory.getCurrentSession().flush();
		return userDevice.getUserDeviceId();		
	}
	
	
	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 * Method to deleteUserDevice..
	 * 
	 * @param userDevice
	 * @throws Exception
	 */
	public void deleteUserDevice(UserDevice userDevice)throws Exception{
		log.info("inside deleteUserDevice() ");
		sessionFactory.getCurrentSession().delete(userDevice);
		sessionFactory.getCurrentSession().flush();
	}
	
	
	
	/**
	 *  Created by Jeevan on DECEMBER 11, 2015
	 *  Method to getUserDeviceByUserDeviceId..
	 *  
	 *  
	 * @param userDeviceId
	 * @return
	 * @throws UserDeviceNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserDevice getUserDeviceByUserDeviceId(Integer userDeviceId)throws UserDeviceNotFoundException{
		log.info("inside getUserDeviceByUserDeviceId() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserDevice.class)
				.add(Restrictions.eq("userDeviceId", userDeviceId));
		ArrayList<UserDevice> userDevices=(ArrayList<UserDevice>) criteria.list();
		if(!userDevices.isEmpty()){
			return userDevices.get(0);
		}
		else{
			throw new UserDeviceNotFoundException();
		}
	}
	
	
	
	/**
	 * Created by Jeevan on December 11, 2015
	 * 
	 *  Method to getUserDevicesByUserIds
	 *  
	 *  
	 * @param userIds
	 * @return
	 * @throws UserDeviceNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserDevice> getUserDevicesByUserIds(ArrayList<Integer> userIds)throws UserDeviceNotFoundException{
		log.info("inside  getUserDevicesByUserIds() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserDevice.class);
		criteria.createCriteria("kikspotUser").add(Restrictions.in("userId", userIds));
		ArrayList<UserDevice> userDevices=(ArrayList<UserDevice>) criteria.list();
		if(!userDevices.isEmpty()){
			return userDevices;
		}
		else{
			throw new UserDeviceNotFoundException();
		}
	}

	
	
	
	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 * 
	 * Method to getUserDeviceofUser..
	 * 
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserDeviceNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserDevice getUserDeviceofUser(KikspotUser kikspotUser)throws UserDeviceNotFoundException{
		log.info("inside getUserDeviceofUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserDevice.class)
				.add(Restrictions.eq("kikspotUser", kikspotUser));
		ArrayList<UserDevice> userDevices=(ArrayList<UserDevice>) criteria.list();
		if(!userDevices.isEmpty()){
			return userDevices.get(0);
		}
		else{
			throw new UserDeviceNotFoundException();
		}
	}
	
	
	/**
	 * Created by Jeevan on DECEMBER 22, 2015
	 * 
	 *  Method to getUserDeviceByDeviceTokens().
	 *  
	 *  
	 * @param deviceTokens
	 * @return
	 * @throws UserDeviceNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserDevice> getUserDeviceByDeviceTokens(ArrayList<String> deviceTokens)throws UserDeviceNotFoundException{
		log.info("inside getUserDeviceByDeviceTokens() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserDevice.class)
				.add(Restrictions.in("deviceToken", deviceTokens));
		ArrayList<UserDevice> userDevices=(ArrayList<UserDevice>) criteria.list();
		if(!userDevices.isEmpty()){
			return userDevices;
		}
		else{
			throw new UserDeviceNotFoundException();
		}
	}
	
	
	/**
	 * Created by Jeevan on December 28, 2015
	 * Method to get all UserDevices,,
	 * 
	 * @return
	 * @throws UserDeviceNotFoundException
	 */
	public ArrayList<UserDevice> getAllUserDevices()throws UserDeviceNotFoundException{
		log.info("inside getAllUserDevices()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserDevice.class);
		ArrayList<UserDevice> userDevices=(ArrayList<UserDevice>) criteria.list();
		if(!userDevices.isEmpty()){
			return userDevices;
		}
		else{
			throw new UserDeviceNotFoundException();
		}
	}
	
	
	
/******* USER DEVICES **************************************/
	

	
 /******************** USER LOCATION **************************************************/
	
	
	/**
	 * Created by Jeevan on DECEMBER 11, 2015
	 * Method to saveorUpdateUserLocation..
	 * 
	 * @param userLocation
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserLocation(UserLocations userLocation)throws Exception{
		log.info("inside saveorUpdateUserLocation() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userLocation);
		sessionFactory.getCurrentSession().flush();
		return userLocation.getUserLocationId();
	}
	
	
	
	/**
	 * 
	 *  Created by Jeevan on DECEMBER 11, 2015
	 *  Method to getUserLocationsOfUser...
	 *  
	 *  
	 * @param kikspotUser
	 * @return
	 * @throws UserLocationNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserLocations getUserLocationsOfUser(KikspotUser kikspotUser)throws UserLocationNotFoundException{
		log.info("inside getUserLocationsofUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserLocations.class);
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		ArrayList<UserLocations> userLocations=(ArrayList<UserLocations>) criteria.list();
		if(!userLocations.isEmpty()){
			return userLocations.get(0);
		}
		else{
			throw new UserLocationNotFoundException();
		}
	}
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on DECEMBER 28, 2015
	 *  Method to get list of UserIds
	 *  which are near by to given location..
	 *  
	 * 
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @param unit
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * 
	 * 
	 */
	public ArrayList<Integer> getAllUsersNearbytoGivenLocation(Double latitude,Double longitude,Integer radius,String unit)throws UserNotFoundException{
		log.info("inside getAllUsersNearbytoGivenLocation");
		
		Integer distanceCount;
		if(unit.equals("miles")){
			distanceCount=3959;
		}
		else{
			distanceCount=6371;
		}		
		StringBuilder query=new StringBuilder();
		query.append("SELECT user_location_id, user_id, ( "+distanceCount+" * acos( cos( radians("+latitude+") ) * cos( radians( latitude ) ) * ");
		query.append(" cos( radians( longitude ) - radians("+longitude+") ) + sin( radians("+latitude+") )");
		query.append("* sin( radians( latitude ) ) ) ) AS distance FROM user_locations ");
		query.append("HAVING distance < "+radius+" ORDER BY distance LIMIT 0 , 50 ");		
		SQLQuery sqlQuery=sessionFactory.getCurrentSession().createSQLQuery(query.toString());		
		@SuppressWarnings("unchecked")
		List<Object[]> rows=sqlQuery.list();
		ArrayList<Integer> userIds=new ArrayList<Integer>();		
		for(Object[] row : rows){
			userIds.add((Integer)row[1]);
		}			
		return userIds;
	}
	
	
	
	
	
	
 /******************************** USER LOCATION ****************************************/	
 /******************************** USER LOCATION *****************************************/	
	
	/**
	 * Created By Bhagya On December 24th,2015
	 * @param kikspotUser
	 * @return
	 * @throws RefferdUsersNotFoundException
	 * 
	 * Method for getting the list of Referred Users Of a User
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getReferredUsersByKikspotUser(KikspotUser kikspotUser) throws ReferredUsersNotFoundException{
		log.info("inside getReferredUsersByKikspotUser()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("referredUser", kikspotUser));
		ArrayList<KikspotUser> referredUsers=(ArrayList<KikspotUser>) criteria.list();
		if(!referredUsers.isEmpty()){
			return referredUsers;
		}
		else{
			throw new ReferredUsersNotFoundException();
		}
	
	}

	
	/***************************************** USER PREFERENCES ****************************************************/
	/**
	 * Created By bhagya On feb 02nd, 2016
	 * 
	 * @return
	 * @throws UserPreferencesNotFoundException
	 * 
	 *  Method For Getting The List Of All User Preferences
	 *  
	 *  Modified by bhagya on feb 22nd , 2019
	 *  Based on client requirement suppress or remove the some user preference options
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserPreferences> getListOfAllUserPreferences() throws UserPreferencesNotFoundException{
		log.info("inside getListOfAllUserPreferences()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserPreferences.class);
		ArrayList<UserPreferences> userPreferences=(ArrayList<UserPreferences>) criteria.list();
		ArrayList<UserPreferences> filteredUserPreferences=new ArrayList<UserPreferences>();
		if(!userPreferences.isEmpty()){
			for(UserPreferences userPreference:userPreferences){
				if(userPreference.getPreferenceId()!=2 && userPreference.getPreferenceId()!=3 && userPreference.getPreferenceId()!=5 && userPreference.getPreferenceId()!=8){
					filteredUserPreferences.add(userPreference);
				}
			}
			return filteredUserPreferences;
		}
		else{
			throw new UserPreferencesNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On Feb 02nd, 2015
	 * @param userPreference
	 * @return
	 * @throws UserPreferenceOptionsNotFoundException 
	 * 
	 * Method For Getting The User Preference Option By User Prefernce
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserPreferenceOptions> getUserPreferenceOptionsByUserPreference(UserPreferences userPreference) throws UserPreferenceOptionsNotFoundException{
		log.info("inside getUserPreferenceOptionsByUserPreference()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserPreferenceOptions.class);
		criteria.add(Restrictions.eq("userPreferences", userPreference));
		ArrayList<UserPreferenceOptions> userPreferenceOptions=(ArrayList<UserPreferenceOptions>) criteria.list();
		if(!userPreferenceOptions.isEmpty()){
			return userPreferenceOptions;
		}
		else{
			throw new UserPreferenceOptionsNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On Feb 02nd, 2016
	 * @param kikspotUser
	 * @param userPreferenceOption
	 * @return
	 * @throws UserPreferenceSettingNotFoundException
	 * 
	 * Method For Getting the List of User Preference settings By User and User Preference 
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserPreferenceSetting> getUserPreferenceSettingsByUserAndUserPreference(KikspotUser kikspotUser,UserPreferences userPreference) throws UserPreferenceSettingNotFoundException{
		log.info("inside getUserPreferenceSettingsByUserAndUserPreference()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserPreferenceSetting.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser", kikspotUser),Restrictions.eq("userPreferences", userPreference)));
		ArrayList<UserPreferenceSetting> userPreferenceSettings=(ArrayList<UserPreferenceSetting>) criteria.list();
		if(!userPreferenceSettings.isEmpty()){
			return userPreferenceSettings;
		}
		else{
			throw new UserPreferenceSettingNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Feb 02nd, 2016
	 * @param kikspotUser
	 * @param userPreference
	 * @return
	 * @throws UserPreferenceSettingNotFoundException
	 * 
	 * Method for getting the List User Preference Option Ids.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserPreferenceOptions> getUserPreferenceOptionsByUserAndUserPreference(KikspotUser kikspotUser,UserPreferences userPreference) throws UserPreferenceSettingNotFoundException{
		log.info("inside getUserPreferenceOptionsByUserAndUserPreference()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserPreferenceSetting.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser", kikspotUser),Restrictions.eq("userPreferences", userPreference)));
		criteria.setProjection(Projections.property("userPreferenceOptions"));
		ArrayList<UserPreferenceOptions> userPreferenceOptions=(ArrayList<UserPreferenceOptions>) criteria.list();
		if(!userPreferenceOptions.isEmpty()){
			return userPreferenceOptions;
		}
		else{
			throw new UserPreferenceSettingNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On feb 03rd, 2016
	 * @param optionIds
	 * @return
	 * @throws UserPreferenceOptionsNotFoundException
	 * 
	 * Method for getting the User Preference Options By List of OptionIds
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserPreferenceOptions> getUserPreferenceOptionsByOptionIds(ArrayList<Integer> optionIds) throws UserPreferenceOptionsNotFoundException{
		log.info("inside getUserPreferenceOptionsByOptionIds()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserPreferenceOptions.class);
		criteria.add(Restrictions.in("userPreferenceOptionsId", optionIds));
		ArrayList<UserPreferenceOptions> userPreferenceOptions=(ArrayList<UserPreferenceOptions>) criteria.list();
		if(!userPreferenceOptions.isEmpty()){
			return userPreferenceOptions;
		}
		else{
			throw new UserPreferenceOptionsNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Feb 03rd, 2016
	 * @param kikspotUser
	 * @param userPreference
	 * @return
	 * @throws UserPreferenceSettingNotFoundException
	 * 
	 * Method For Getting the User Preference setting By UserId and User Preference
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserPreferenceSetting> getUserPreferenceSettingsByUserIdAndUserPreference(KikspotUser kikspotUser,UserPreferences userPreference) throws UserPreferenceSettingNotFoundException{
		log.info("inside getUserPreferenceSettingsByUserIdAndUserPreference()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserPreferenceSetting.class);
		criteria.add(Restrictions.and(Restrictions.eq("kikspotUser", kikspotUser),Restrictions.eq("userPreferences", userPreference)));
		ArrayList<UserPreferenceSetting> userPreferenceSettings=(ArrayList<UserPreferenceSetting>) criteria.list();
		if(!userPreferenceSettings.isEmpty()){
			return userPreferenceSettings;
		}
		else{
			throw new UserPreferenceSettingNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On Feb 03rd, 2016
	 * @param userPreferenceSetting
	 * @return
	 * @throws Exception
	 * 
	 * Method for deleting the User Preference Setting
	 */
	public Integer deleteUserPreferenceSetting(UserPreferenceSetting userPreferenceSetting) throws Exception {
		log.info("inside deleteUserPreferenceSetting()");
		 sessionFactory.getCurrentSession().delete(userPreferenceSetting);
		 sessionFactory.getCurrentSession().flush();
		 return userPreferenceSetting.getUserPreferenceSettingId();
	}
	/***
	 * Created By Bhagya On Feb 03rd, 2016
	 * @param userPreferenceSetting
	 * @return
	 * 
	 * Method for Saving the User Preference Setting
	 */
	public Integer saveUserPreferenceSetting(UserPreferenceSetting userPreferenceSetting)throws Exception{
		log.info("inside saveUserPreferenceSetting");
		sessionFactory.getCurrentSession().save(userPreferenceSetting);
		sessionFactory.getCurrentSession().flush();
		return userPreferenceSetting.getUserPreferenceSettingId();
	}





	@Override
	public Integer deleteUser(KikspotUser user) throws Exception {
		log.info("inside delete user()");
		sessionFactory.getCurrentSession().delete(user);		
		sessionFactory.getCurrentSession().flush();	
		return 1;
	}

	@SuppressWarnings("unchecked")
	
	public ArrayList<GameRuleTypes> getGameRuleTypes() throws Exception {
		// TODO Auto-generated method stub
		log.info("inside getGameRuleTypes()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(GameRuleTypes.class);
		
		ArrayList<GameRuleTypes> gameRuleTypes = (ArrayList<GameRuleTypes>) criteria.list();
		return gameRuleTypes;
	}


    /*
     *Method to get all users except by passs users
     *
     *Modified by bhagya on may 22nd, 2019-- added the isActive Restriction
     */


	@SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getAllUsersExceptByPass() throws Exception {
		log.info("inside getAllUsersExceptByPass()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.ne("userType",UserType.BYPASS_USER));
		criteria.add(Restrictions.eq("isActive", true));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>)criteria.list();
		return users;
	}


   /**
    * Method to get the referred user
    */

	@SuppressWarnings("unchecked")
	public KikspotUser getUserByReferralCode(String referralCode) throws ReferredUsersNotFoundException {
		log.info("inside getUserByReferralCode()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("referralCode",referralCode));
		ArrayList<KikspotUser> referredUsers=(ArrayList<KikspotUser>) criteria.list();
		if(!referredUsers.isEmpty()){
			return referredUsers.get(0);
		}
		else{
			throw new ReferredUsersNotFoundException();
		}
	}
	
	/** Created by Firdous on 31st may 2016, method to get the kikspotuser with username equals to passed username and with different user id**/
    @SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByUsernameNotEqualToPassedUserId(String username, Integer userId)throws UserNotFoundException {
		log.info("inside ggetKikspotUserByUsernameNotEqualToPassedUserId()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.and(Restrictions.eq("username", username),Restrictions.ne("userId",userId)));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new  UserNotFoundException();
		}
    }





    @SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByEmailNotEqualToPassedUserId(String username, Integer userId) throws Exception {
    	log.info("inside getKikspotUserByUsernameNotEqualToPassedUserId()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.and(Restrictions.eq("emailId", username),Restrictions.ne("userId",userId)));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new  UserNotFoundException();
		}
	}
    /**
     * Created By bhagya on may 21st, 2019
     * @return
     * @throws Exception
     * 
     * Method for to get the all users of master geo located
     */
    @SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getAllUsersOfMasterGeoLocated() throws Exception {
		log.info("inside getAllUsersOfMasterGeoLocated()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("isMasterGeoLocation",true));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>)criteria.list();
		return users;
	}
}