<!-- Change Password created by bhagya on december 17th,2015 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/user.js"></script>
<title>Change Password</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
<body class="l-dashboard  l-footer-sticky-1  ">
    <!--SECTION-->
    <section class="l-main-container">
    
      <!--Left Side Navigation Content-->
     <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
     
	     <!--Main Content-->
	      <section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	         <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom">
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Change Password</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
				<form:form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/changepassword.do" method="POST" commandName="changePasswordDto" onsubmit="return validateChangePassword();" >
				
	            <form:input type="hidden" path="userId" value="${userId}" />
	            	<c:if test="${not empty message}">
		      			<div class="status">${message}</div><br><br> 
		      		</c:if>
	        	<div class="form-group">
					<label  class="col-sm-3 control-label">Current Password :</label>
					<div class="col-sm-9">
						<form:input path="oldPassword"  id="oldPassword"  type="text" class="form-control" />
						<span id="oldPassword_error" class="validation_error">
						<form:errors path="oldPassword" class="validation_error"/>	
						</span> 
				    </div>
				</div>
				<br/>
			
				<div class="form-group">
					<label  class="col-sm-3 control-label">New Password :</label>
					<div class="col-sm-9">
						<form:input path="newPassword"  id="newPassword"  type="text"  class="form-control" />
						<span id="newPassword_error" class="validation_error">
						<form:errors path="newPassword" class="validation_error"/>	
						</span> 
				    </div>
				</div>
				<br/>
				
				<div class="form-group">
					<label  class="col-sm-3 control-label">Confirm Password :</label>
					<div class="col-sm-9">
						<form:input path="confirmPassword"  id="confirmPassword"  type="text"  class="form-control" />
						<span id="confirmPassword_error" class="validation_error">
						<form:errors path="confirmPassword" class="validation_error"/>	
						</span> 
				    </div>
				</div>
				<br/>
	
				
			    <div class="col-sm-offset-3 col-sm-9">
	                 <button type="submit" class="btn btn-dark">Submit</button>
				</div>
	
				</form:form>
			</div>
		</div><!-- l-spaced -->
	</div> <!-- l-box l-spaced-bottom -->
			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
	       </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>