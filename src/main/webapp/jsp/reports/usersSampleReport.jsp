<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Users Sample Reports</title>
	<%@ include file="/jsp/common/common.jsp"%>
	
	<script type="text/javascript">
	$(document).ready(function() {
		$('#excel').click(function(){
	      	$('<input type="hidden" name="export" value="excel" id="export" />').appendTo($('#usersForm'));    	
	    	$('#usersForm').submit();
	    	$('#export').remove();
	    });
	    
	});
	
	</script>
</head>
<body>
<body class="l-dashboard  l-footer-sticky-1  ">
    <!--SECTION-->
    <section class="l-main-container">
    
      <!--Left Side Navigation Content-->
     <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
     
	     <!--Main Content-->
	      <section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	        <!-- Middle Content Start -->
			<div>
				<h3>Users Sample Report </h3>
			</div>
			<form class="form-horizontal"  action="${pageContext.request.contextPath}/reports/userssamplereport.do" method="POST" id="usersForm">
			 <div style="text-align: center;height: 300px;font-size: 30px;margin-top: 100px;">
			 		
			 		<button type="Submit" class="btn btn-success">Download Pdf Report</button>
			 		<button type="button"  id="excel" class="btn btn-dark">Generate EXCEL Report</button>
			 
			 </div>
			</form>
			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
	       </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>