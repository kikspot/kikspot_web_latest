<!-- Created By bhagya On december 23rd,2015 
	Page For Logout-->
<!DOCTYPE html>

<html>
<head>

	<title>kikspot-Logout</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>
<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
		<section class="l-container">
	      
	      
	        <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Logout-kikspot</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form class="form-horizontal" autocomplete="off" action="" method="POST" >
          		
          		<div class="logopanel">
					<h4 class="logo_image_header"><a href="#"><img src="${pageContext.request.contextPath}/image/KiKSPOT_cmyk.jpg" width=450px;></a> </h4> 
				</div>
				
            	<div class="error_status">
			          ${message}
			          
			          <br />
			          <br />
			          <a href="${pageContext.request.contextPath}/login.do" style="text-decoration: underline;color:#1962CF;">	Click here to Login </a>
			    </div>
        		
			</form>
			</div>
			</div>
			</div>
				
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>