<!DOCTYPE html>
<html lang="en">
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
 
<head>
    <title>Kikspot - login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    
    <!-- ===== CSS =====-->
    <%@ include file="/jsp/common/common.jsp"%>
    
    <script type="text/javascript" >
    
    function validateEmail()
    {
   		var email=true;
    	var cmail=true;
    	var emailFilter=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	 	var emailId=$('#loginEmail').val();
		var cmailId=$('#confirmEmail').val();
	 
	
	
		if(emailId.trim().length<1 ){
			email=false;
			$('#email_error').text("Email cannot be empty");
		}
		else if(emailFilter.test(emailId)==false){
			email=false;
			$('#email_error').text("Enter valid email address");
		}
		else{
		   email=true;
		   $('#email_error').text("");
		}	
	
		if(cmailId!=emailId){
			cmail=false;	
			$('#cmail_error').text("Email's does not match");
		}
		else{
			cmail=true;
			$('#cmail_error').text("");
		}
	
		if(email==true && cmail==true){
			return true;
		}
		else{
			return false;
		}
		
 }
    
    
    </script>
  </head>
  <body class="login-bg">
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"> <img src="/kikspot/image/KiKSPOT_cmyk.jpg"></h1>
          
          
         
          <!--Login Form-->
          <form:form id="loginForm" class="login-form"  action="${pageContext.request.contextPath}/referuser.do" method='POST' onsubmit="return validateEmail();">
            <div class="form-group">
            	<input type="hidden" name="referredUser" value="${referredUser}" />
              <input id="loginEmail" type="text" name="email" placeholder="Email" class="form-control">
               <span id="email_error" class="error_class">
			 		<form:errors path="loginEmail" class="error_class"/>	
			    </span>
            </div>
            <div class="form-group">
              <input id="confirmEmail" type="text" name="confirmEmail"  placeholder="Confirm Email" class="form-control">
               <span id="cmail_error" class="error_class">
			 		<form:errors path="confirmEmail" class="error_class"/>	
			    </span>
            </div>
            <button type="submit" class="btn btn-dark btn-block btn-login">Submit</button>
            
           
          </form:form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    
 
  </body>

</html>