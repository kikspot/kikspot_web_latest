
<!DOCTYPE html>
<html lang="en">
  
<head>
    <title>Kikspot - login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    
    <!-- ===== CSS =====-->
    <%@ include file="/jsp/common/common.jsp"%>
    <style>
    	.privacyPolicy{
    	
    		text-decoration-line: underline;
    		color:#337ab7;
    	}
    </style>
  </head>
  <body class="login-bg">
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"> Kikspot</h1>
          <c:if test="${ not empty error}">
			<div id="login_head_text" class="status" style="color:#FF0000;">
				${error}
			</div>
		  </c:if>
		  <c:if test="${ not empty sucessMessage}">
		     <div id="login_head_text" class="status">
					${sucessMessage}
			 </div>
		  </c:if>
		  <c:if test="${ not empty errorMessage}">
			<div id="login_head_text" class="status" style="color: #FF0000;">
				${errorMessage}
			</div>
		  </c:if>
          <!--Login Form-->
          <form id="loginForm" class="login-form"  action="<c:url value= '/login' />" method='POST'>
            <div class="form-group">
              <input id="loginEmail" type="text" name="username" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input id="loginPassword" type="password" name="password" placeholder="Password" class="form-control">
            </div>
            	<p>By clicking "Sign In" i agree that</p>
            	<p>I have read and accepted the <a href=" https://www.kikspot.com/terms-of-use" target="_blank" style="text-decoration-line: underline;color:#33b7a5;">Terms of Use </a> and <a href="https://www.kikspot.com/privacy-policy/" target="_blank" style="text-decoration-line: underline;color:#33b7a5;"> Privacy Policy</a></p>
            
            <button type="submit" class="btn btn-dark btn-block btn-login">Sign In</button>
            <div class="login-social">
             
            </div>
            <div class="login-options"><a href="/kikspot/forgotpassword.do" class="fl">FORGOT PASSWORD ?</a></div>
            
           
          </form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    
 
  </body>

</html>