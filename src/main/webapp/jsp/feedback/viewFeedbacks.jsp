
<!DOCTYPE html >
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FeedBacks</title>
<%@ include file="/jsp/common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script  type="text/javascript">
	
	$(document).ready(function() {
			var sortBy=$('#sortBy').val();
			var sortDirection=$('#sortDirection').val();
			if(sortDirection=='true'){
				jQuery("#feedbackUser,#feedbackEmail,#feedbackMessage,#photos,#videos,#feedbackDate").removeClass('sorting').addClass('sorting_asc');
				
			}
			else{
				jQuery("#feedbackUser,#feedbackEmail,#feedbackMessage,#photos,#videos,#feedbackDate").removeClass('sorting').addClass('sorting_desc');
			}
				
			/**
			Function for serach by userId filter
			*/
			$('#dropDown').change(function (){
				var filter=$('#dropDown').val();
				window.location="/kikspot/feedback/getfeedbacks.do?feedbackUserId="+filter;
			});
	});
	
	function deleteFeedback(id){
		if(confirm("Are you Sure to Delete Feedback")==true){
			$.ajax({
			    url:  "/kikspot/feedback/deletefeedback.do",
			    type: 'GET',
			    data:
			    	{
			    		feedbackId:id
			       	},
			   	success: function(data) {
			   		window.location="/kikspot/feedback/getfeedbacks.do";
			   	    alert("Feedback Deleted Successfully ");
			       
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting the Feedback");
			    }
			});
		}
	}
	
	
	  function changeDefaultMail(){
		  var mail=$('#defaultMail').val();
		  $.ajax({
			 url: '${pageContext.request.contextPath}/feedback/changefeedbackmail.do',
			 method:'POST',
			 data:{
				 defaultMail:mail
			 },
			 success:function(data){
				if(data=='success'){
					location.href="${pageContext.request.contextPath}/feedback/getfeedbacks.do";
				}
			 }
		  });
	  }
	
	
	
	</script>


</head>

<body class="l-dashboard  l-footer-sticky-1  ">
	<section class="l-main-container">

		<%@ include file="/jsp/common/leftSideNavigation.jsp"%>


		<section class="l-container">

			<!--HEADER-->
			<%@ include file="/jsp/common/header.jsp"%>


			<div>
				<div class="l-spaced">

					<div class="resp-tab-content resp-tab-content-active"
						aria-labelledby="tab_item-0" style="display: block">
						<form:form commandName="displayListBean" method="POST"
							class="form-horizontal"
							action="${pageContext.request.contextPath}/feedback/getfeedbacks.do"
							role="form">
							<form:hidden path="sortBy" value="${displayListBean.sortBy}"
								id="sortBy" />
							<form:hidden path="searchBy" />
							<form:hidden path="sortDirection"
								value="${displayListBean.sortDirection }" id="sortDirection" />
							<form:hidden path="pagerDto.range" />
							<form:hidden path="pagerDto.pageNo" />

							<c:set var="range" value="${displayListBean.pagerDto.range}" />


							<div class="l-row l-spaced-bottom">
								<div class="l-box">
									<div class="l-box-header">
										<h2 class="l-box-title">
											<span>User Feedbacks</span>
										</h2>
										<ul class="l-box-options">
											<li><a href="#"><i class="fa fa-cogs"></i></a></li>
											<li><a href="#" data-ason-type="fullscreen"
												data-ason-target=".l-box" data-ason-content="true"
												class="ason-widget"><i class="fa fa-expand"></i></a></li>
											<li><a href="#" data-ason-type="refresh"
												data-ason-target=".l-box" data-ason-duration="1000"
												class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
											<li><a href="#" data-ason-type="toggle"
												data-ason-find=".l-box" data-ason-target=".l-box-body"
												data-ason-content="true" data-ason-duration="200"
												class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
											<li><a href="#" data-ason-type="delete"
												data-ason-target=".l-box" data-ason-content="true"
												data-ason-animation="fadeOut" class="ason-widget"><i
													class="fa fa-times"></i></a></li>
										</ul>
									</div>
									
									<div style="text-align:center;padding: 30px;">
										<form>
										<label>Email Id to Send Feedbacks :</label> &nbsp; &nbsp;
										<input type="email" required="required" name="defaultMail" value="${defaultMail}" style="width:200px;" id="defaultMail" />&nbsp; &nbsp;
										<input type="button" value="change" onclick="changeDefaultMail();"/>
										</form>
									</div>
									
									
									<div class="l-box-body">
										<div id="dataTableId_wrapper"
											class="dataTables_wrapper table-responsive">
											<div class="dataTables_length" id="dataTableId_length">
												<label>Show <select name="dataTableId_length"
													id="selectrange" aria-controls="dataTableId" class="">
														<option value="10" ${range=='10' ? 'selected' : ''  }>10</option>
														<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
														<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
														<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
												</select> entries
												</label>
											</div>
									

											<%--   <div id="dataTableId_filter" class="dataTables_filter">
                      	  	<label>Search:<input type="search" class="" placeholder="" aria-controls="dataTableId"></label>
                      	  </div>
                      	  <div class="form-group searchDiv" >
						  	<div class="col-sm-2" > </div>
						  	<div class="col-sm-2"> 
						  		<label class="control-label searchLbl">Search Feedback:</label>
						  	</div>
						  	<div class="col-sm-4 controls">
						  		<input id="searchText" name="searchText" type="text" value=""  class="searchTxt">
						  		<span class="help-inline">Search feedback </span>
						  	</div>
						  <div class="col-sm-3" > </div>
						  </div>
						  
						  <div class="form-group form-actions">							   
						  	<div class="col-sm-11" style="text-align: center;">
						  		<button  class="btn btn-danger" type="button" value="Search"  onclick="searchResults();">Search</button>
						  		<button class="btn btn-warning" type="button" onclick="window.location='${pageContext.request.contextPath}/feedback/getfeedbacks.do'">Reset</button>
						  	</div>
						  
						  
						  </div>
						  --%>

											<div class="form-group searchDiv">
												<div class="col-sm-2"></div>
												<div class="col-sm-2">
													<label class="control-label searchLbl">Search Feedbacks of user:</label>
												</div>
												
												<div class="col-sm-4">
													<select id="dropDown" class="form-control" name="feedbackUserId">
														<option value="" selected="selected">select user</option>
														<c:forEach var="user" items="${users}">
															<option value="${user.userId}"
																${feedbackUserId eq user.userId ? 'selected' : '' } >${user.username}</option>
														</c:forEach>
													</select>

												</div>
												
												<div class="col-sm-3"></div>
											</div>

											 <c:if test="${not empty message}">
		      										<div class="status">${message}</div><br><br> 
		      								 </c:if>

											<table id="dataTableId" cellspacing="0" width="100%"
												class="display dataTable table" role="grid"
												aria-describedby="dataTableId_info" style="width: 100%;">
												<thead>
													<tr role="row">
														<th class="sorting" id="feedbackUser" onclick="sortResults('kikspotUser.firstName');">Username</th>
														<th class="sorting" id="feedbackEmail" onclick="sortResults('kikspotUser.emailId');">Email</th>
														<th class="sorting" id="feedbackMessage"
															onclick="sortResults('feedbackMessage');">Feedback Message</th>
														<th class="sorting" id="photos"
															onclick="sortResults('attachments');">Attachments</th>

														<th class="sorting" id="feedbackDate"
															onclick="sortResults('feedbackDate');">Feedback date</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Username</th>
														<th>Email</th>
														<th>Feedback Message</th>
														<th>Attachments</th>
														<th>Feedback Date</th>
														<th>Delete</th>

													</tr>
												</tfoot>
												<tbody>
													<c:forEach var="feedback" items="${feedbacks}">
														<tr>
															<td>${feedback.kikspotUser.username}</td>
															<td>${feedback.kikspotUser.emailId}</td>
															<td>${feedback.feedbackMessage}</td>
															<td><c:forEach var="file" items="${feedback.feedbackAttachmentDto}">
															      <c:choose> 
															      	 <c:when test="${file.fileType eq 'image'}">
                                                                   		<img src="/feedbackimages/${feedback.userId}/${file.fileName}" onerror="this.src='${pageContext.request.contextPath}/image/default.jpg'" height="40" width="50"/>
                                                                     </c:when>
                                                                     <c:otherwise>
                                                                        <a href="/feedbackimages/${feedback.userId}/${file.fileName}">${file.fileName }</a>
                                           							</c:otherwise>
                                                                  </c:choose>
                                                                 </c:forEach></td>
															<td><fmt:formatDate pattern="MM/dd/yyyy" value="${feedback.feedbackDate}" /></td>
															
															<td><a href="#" onclick="deleteFeedback(${feedback.feedbackId})">
																    <i class="icon fa fa-remove"></i></a></td>

														</tr>
													</c:forEach>
												</tbody>
											</table>


											<c:if test="${empty message}">
												<c:set var="first" value="0" />
												<c:set var="end"
													value="${displayListBean.pagerDto.pagesNeeded }" />
												<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
												<c:set var="total"
													value="${displayListBean.pagerDto.totalItems}" />
												<c:set var="firstResult"
													value="${displayListBean.pagerDto.firstResult}" />
												<c:set var="lastResult"
													value="${displayListBean.pagerDto.lastResult}" />
												<div class="dataTables_info" id="dataTableId_info"
													role="status" aria-live="polite">Showing
													${firstResult} to ${lastResult} of ${total} entries</div>
												<div class="dataTables_paginate paging_simple_numbers"
													id="dataTableId_paginate">
													<%@ include file="/jsp/common/pager.jsp"%>
												</div>
											</c:if>
										</div>
									</div>


								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>

			<!--FOOTER-->
			<%@ include file="/jsp/common/footer.jsp"%>

		</section>
	</section>

</body>
</html>