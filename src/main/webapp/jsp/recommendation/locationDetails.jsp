<!-- Created By Bhagya On Feb 04th,2016
	Jsp Page For view the Recommendation Location Details-->
<!DOCTYPE html>

<html>
<head>

	<title>Recommendation Location Details</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/recommendationLocations.js"></script>
	<style>
		.transparent{
			background-color: #ffffff;
			opacity: 0.9;
		}
		.label_style{
			font-size:17px;
		}
		.location_span{
			font-size:16px;
			/* color:#f4a460; */ 
			color:#0000FF
		}
		.location_title{
			    font-size: 17px;
    			font-weight: bold;
		}
		.login-bg {
		    background: rgba(0, 0, 0, 0) url(../image/logo_new.png) repeat scroll center top;
		    background-attachment:fixed;
		    background-size:cover;
			background-repeat: no-repeat;
		}
		.l-box-title{
			float:none;
			text-align:center;
		}
		.mb_border_span{
			border:solid 1px #aaa;
			padding-top:5px;
			padding-bottom:5px;	
		}
		
	</style>
</head>
<body class="l-dashboard  l-footer-sticky-1 login-bg">

    <section class="l-main-container">
   
   	
		<section class="l-container">
	      
	        <!--HEADER-->
	       <%--  <%@ include file="/jsp/common/header.jsp"%> --%>
	      
	        <div class="l-spaced transparent" >
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title location_title">Recommendation Location Details</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form class="form-horizontal" autocomplete="off" action="" >
			
            	<div class="form-group">
					 <label  class="col-sm-3 control-label label_style">Location Icon :</label>
					<div class="col-sm-9">
						<img alt="" src="${location.iconUrl}" height="100px;" width="150px;">
					</div>
				</div>
				<br/>
				
				<div class="form-group">
					 <label  class="col-sm-3 control-label label_style">Location Image :</label>
					<div class="col-sm-9">
						<img alt="" src="${location.image}" height="100px;" width="150px;">
					</div>
				</div>
				<br/>
				
            	<div class="form-group">
					<label  class="col-sm-3 control-label label_style">Location Name :</label>
					<div class="col-sm-9">
						<span class="location_span">${location.locationName}</span>
					</div>
				</div>
			<br/>
        	
        	<%-- <div class="form-group">
				<label  class="col-sm-3 control-label label_style">Latitude :</label>
				<div class="col-sm-9">
					<span class="location_span">${location.latitude}</span>
				 </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label label_style">Longitude :</label>
				<div class="col-sm-9">
					<span class="location_span">${location.longitude}</span>
				</div>
			</div>
			<br/> --%>
			<div class="form-group">
				<label  class="col-sm-3 control-label label_style">Map Location:</label>
				<div class="col-sm-9">
					<%-- <span class="location_span">https://www.google.com/maps/@${location.latitude},${location.longitude}</span> --%>
					<span class="location_span">https://maps.google.com/?q=${location.latitude},${location.longitude}</span>
				</div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label label_style">Rating:</label>
				<div class="col-sm-9">
					<span class="location_span">${location.ratings}</span>
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label label_style">Address :</label>
				<div class="col-sm-9">
					<span class="location_span">${location.address}</span>
				</div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label label_style">Phone No :</label>
				<div class="col-sm-9">
					<span class="location_span">${location.phoneNo}</span>
				</div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label label_style">Website URL:</label>
				<div class="col-sm-9">
					<span class="location_span">${location.url}</span>
				</div>
			</div>
			<br/>
		
			<div class="form-group">
			<div class="col-sm-3">
				<label  class="col-md-12 control-label label_style">Rewards:</label>
			</div>
			<div class="col-sm-9">
			<c:if test="${not empty location.gameRulesDto  }">
				<c:forEach var="rule" items="${location.gameRulesDto}">
				<div class="col-sm-4 mb_border_span">
					<span class="location_span">${rule.rule} : </span>
					<span class="location_span">${rule.creds}</span>
				</div>					
				</c:forEach>
				</c:if>
			</div>
			</div>
			
			<br/>

			</form>
			</div>
			</div>
			</div>
				
	
   			
   			
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>