<!-- Created By Bhagya On December 18th,2015
	Jsp Page For Editing the Recommendation Location -->
<!DOCTYPE html>

<html>
<head>

	<title>Edit Recommendation Location</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/recommendationLocations.js"></script>
	<script>
		$(function() {
			$( "#closeDate" ).datepicker({  dateFormat:"mm/dd/yy"
				
				});
			$( "#openDate" ).datepicker({  dateFormat:"mm/dd/yy"
				
			});
			  
		}); 

    </script>
</head>
<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
    <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
	
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	      
	        <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Edit Recommendation Location</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form:form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/recommendations/editlocation.do" method="POST" commandName="kikspotRecommendationLocationDto" onsubmit="return validateEditRecommendationLocation();"
			enctype="multipart/form-data" >
			
            <form:input type="hidden" path="locationId" name="locationId" value="${location.locationId}" />
             <div class="col-sm-offset-3 col-sm-9">
            	<span class="error_class" style="color:#0033BA;"> * Note: LocationId is of GooglePlaceAPI</span>
            </div>
        	<div class="form-group">
				<label  class="col-sm-3 control-label">Location Id:</label>
				<div class="col-sm-9">
					<form:input path="googleLocationId"  id="googleLocationId"  type="text" class="form-control" value="${location.googleLocationId}" readonly="true"/>
					<span id="googleLocationId_error" class="validation_error">
					<form:errors path="googleLocationId" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
		
			<div class="form-group">
				<label  class="col-sm-3 control-label">Location Name :</label>
				<div class="col-sm-9">
					<form:input path="locationName"  id="locationName"  type="text"  class="form-control"  value="${location.locationName}"/>
					<span id="locationName_error" class="validation_error">
					<form:errors path="locationName" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Latitude :</label>
				<div class="col-sm-9">
					<form:input path="latitude"  id="latitude"  type="text"  class="form-control" value="${location.latitude}"/>
					<span id="latitude_error" class="validation_error">
					<form:errors path="latitude" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Longitude :</label>
				<div class="col-sm-9">
					<form:input path="longitude"  id="longitude"  type="text"  class="form-control" value="${location.longitude}"/>
					<span id="longitude_error" class="validation_error">
					<form:errors path="longitude" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Rating:</label>
				<div class="col-sm-9">
					<form:input path="ratings"  id="ratings"  type="text"  class="form-control" value="${location.ratings}"/>
					<%-- <span id="ratings_error" class="validation_error">
						<form:errors path="ratings" class="error_class"/>	
					</span>  --%>
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Address :</label>
				<div class="col-sm-9">
					<form:input path="address"  id="address"  type="text"  class="form-control" value="${location.address}"/>
					<span id="address_error" class="validation_error">
					<form:errors path="address" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Phone No :</label>
				<div class="col-sm-9">
					<form:input path="phoneNo"  id="phoneNo"  type="text"  class="form-control" value="${location.phoneNo}"/>
					<span id="phoneNo_error" class="validation_error">
					<form:errors path="phoneNo" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
		<%-- 	<div class="form-group">
				<label  class="col-sm-3 control-label">Distance :</label>
				<div class="col-sm-9">
					<form:input path="distance"  id="distance"  type="text"  class="form-control" value="${location.distance}"/>
					<span id="distance_error" class="validation_error">
					<form:errors path="distance" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/> --%>
			<div class="form-group">
				<label  class="col-sm-3 control-label">Open Date :</label>
				<div class="col-sm-9">
				    <fmt:formatDate value="${location.openDate}" var="openDateFormat" type="date" pattern="MM/dd/yyyy"/> 
					<form:input  path="openDate" id="openDate"  type="text" class="form-control"   value="${openDateFormat}"/>
					<span id="openDate_error" class="validation_error">
						<form:errors path="openDate" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Close Date :</label>
				<div class="col-sm-9">
				    <fmt:formatDate value="${location.closeDate}" var="startFormat" type="date" pattern="MM/dd/yyyy"/> 
					<form:input  path="closeDate" id="closeDate"  type="text" class="form-control"   value="${startFormat}"/>
					<span id="closeDate_error" class="validation_error">
						<form:errors path="closeDate" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Website URL:</label>
				<div class="col-sm-9">
					<form:input path="url"  id="url"  type="text"  class="form-control" value="${location.url}"/>
					<span id="url_error" class="validation_error">
					<form:errors path="url" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Icon:</label>
				<div class="col-sm-9">
				<img src="/recommendations/icons/${location.googleLocationId}/${location.locationId }/${location.iconUrl}" onerror="this.src='${pageContext.request.contextPath}/image/default.jpg'" height="65px;" width="66px;" /><br/>
					<form:input path="iconImage"  id="icon"  type="file"  class="form-control" />
					<span id="icon_error" class="validation_error"></span>
			    </div>
			</div>
			<div class="form-group">
				<label  class="col-sm-3 control-label">Images :</label>
				<div class="col-sm-9">
					<a href="${pageContext.request.contextPath}/recommendations/locationimages.do?locationId=${location.locationId}" target="_blank" style="text-decoration: underline;">Edit Images</a>					
			    </div>
			</div>
			<div class="form-group">
				<label  class="col-sm-3 control-label">Modified By:</label>
				<div class="col-sm-9">
					<form:input path="modifiedBy"  id="modifiedBy"  type="text"  class="form-control" />
					<span id="modifiedBy_error" class="validation_error">
					<form:errors path="modifiedBy" class="error_class"/>	
					</span> 
			    </div>
			</div>
		    <div class="col-sm-offset-3 col-sm-9">
                 <button type="submit" class="btn btn-dark">Submit</button>
			</div>

			</form:form>
			</div>
			</div>
			</div>
				
	
   			
   			
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>