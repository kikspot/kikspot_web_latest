<!-- Created By bhagya On december 22nd,2015 
	Page For edit the Recommendation Location Images-->
<!DOCTYPE html>

<html>
<head>

	<title>Edit Recommendation Location Images</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/recommendationLocations.js"></script>
	<style>
		.imagesMainDiv{
			display: inline-block;
		}
		 .imagesSubDiv{
			display: block;
		} 
		.remove-image{
			text-decoration: underline !important;
			margin-left:55px;
		}
		.location-image{
			margin-top:15px;
		}
	</style>
</head>
<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
   <%--  <%@ include file="/jsp/common/leftSideNavigation.jsp"%> --%>
	
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	      
	        <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Edit Recommendation Location Images</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/recommendations/addlocationimages.do" method="POST" onsubmit="return validateEditRecommendationLocationImages();"  
			enctype="multipart/form-data" >
          	
            	
        	<div class="form-group">
            <c:choose>
        	<c:when test="${not empty locationImages}">
        		<div class="col-md-12">
	  				<div class="col-md-9 imagesMainDiv">
						<c:forEach var="locationImage" items="${locationImages}">
								<div class="col-md-3  imagesSubDiv">
								<input type="hidden" name="locationId" value="${locationImage.kikspotRecommendationLocation.locationId}">
					    		<img src="/recommendations/images/${locationImage.kikspotRecommendationLocation.locationId}/${locationImage.image}" onerror="this.src='${pageContext.request.contextPath}/image/default.jpg'" height="100px;" width="150px;" class="location-image" /><br/> 
					    		<a href="${pageContext.request.contextPath}/recommendations/deletelocationimage.do?locationImageId=${locationImage.locationImageId}&locationId=${locationImage.kikspotRecommendationLocation.locationId}" class="remove-image"> Remove </a>
							</div>
						</c:forEach>
					</div>
				</div>
			</c:when>
			<c:otherwise>
					<input type="hidden" name="locationId" value="${locationId}">
					<div class="status">Recommendation Location images does not exist,you can add images</div>
			</c:otherwise>
			</c:choose>
			
			</div>
			
			<br/>
		
			
			
			<div class="form-group">
				<label  class="col-sm-3 control-label">Add Images:</label>
				<div class="col-sm-9">
					<input name="locationImages"  id="image"  type="file"  class="form-control" multiple="multiple"/>
					<span id="image_error" class="validation_error">
					</span> 
			    </div>
			</div>
			<br/> 
			
		    <div class="col-sm-offset-3 col-sm-9">
                 <button type="submit" class="btn btn-dark">Submit</button>
			</div>

			</form>
			</div>
			</div>
			</div>
				
	
   			
   			
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>