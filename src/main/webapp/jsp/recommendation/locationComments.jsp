<!DOCTYPE html>

<html>
<head>

	<title>Recommendation Location Details</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/recommendationLocations.js"></script>
	<style>
		.transparent{
			background-color: #ffffff;
			opacity: 0.6;
		}
		.label_style{
			font-size:17px;
		}
		.location_span{
			font-size:20px;
			/* color:#f4a460; */ 
			color:#0000FF
		}
		.location_title{
			    font-size: 17px;
    			font-weight: bold;
		}
		p{
		    font-size:25px;
		    font-weight:bold;
		    color:#00dd11;
		}
		.location{
			font-size:35px;
			font-weight:bold;
			color:#0000FF
		}
	</style>
</head>
<body class="l-dashboard  l-footer-sticky-1 login-bg">

    <section class="l-main-container">
   
   	
		<section class="l-container">
	      
	        <!--HEADER-->
	       <%--  <%@ include file="/jsp/common/header.jsp"%> --%>
	      
	        <div class="l-spaced transparent" >
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title location_title">Location Ratings and Comments</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			
			<form class="form-horizontal" autocomplete="off" action="" >
			       <div class="form-group">
				<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<span class="location">${location.locationName}</span>
					</div>
				    </div>	
				      <div class="form-group">
					<label  class="col-sm-3 control-label label_style">Location Address:</label>
					<div class="col-sm-9">
						<span class="location_span">${location.address}</span>
					</div>
				    </div>
				     <div class="form-group">
					 <label  class="col-sm-3 control-label label_style">Location Icon :</label>
					<div class="col-sm-9">
						<img alt="" src="${location.iconUrl}" height="100px;" width="150px;">
					</div>
				    </div>
				     <div class="form-group">
					 <label  class="col-sm-3 control-label label_style">Location Image :</label>
					<div class="col-sm-9">
						<img alt="" src="${location.image}" height="100px;" width="150px;">
					</div>
				    </div>
				    
				   
				   <!--  <div>
				    <h1 class="l-box-title">Comments</h1>
				    </div> -->
				    <c:choose>
				    <c:when test="${not empty userLocationRatings }">
				    <div>
				   <section class="comments" id="comments">
				     <c:forEach var="locationRating" items="${userLocationRatings}">
                     <section class="content">
                       
				        <p><i class="icon fa fa-user"></i>${locationRating.kikspotUserDto.username }</p>
				      
				       <br/>
						<span class="location_span">${locationRating.comment}</span>
					
						<br/>
						<span class="location_span">${locationRating.rating}</span> 
				     
				     </section>
				    
				     </c:forEach>  
	                 </section>
	                 </div>
	                 </c:when>
	                  <c:otherwise>
				      <div class="form-group">
				     
				       <label  class="col-sm-3 control-label label_style">Location Ratings :</label>
				      <span class="location_span">${rating}</span>
				     
				      </div>
				      </c:otherwise>
	                 </c:choose>
   			</form>
			</div>
			</div>
			</div>
			
   			
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</body>
</html>