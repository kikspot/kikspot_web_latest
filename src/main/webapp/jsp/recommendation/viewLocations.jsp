<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Recommendation Locations</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<link href="${pageContext.request.contextPath}/css/fonts/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/fonts/bootstrap/highlight.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/fonts/bootstrap/bootstrap-switch.css" rel="stylesheet">
    <link href="https://getbootstrap.com/assets/css/docs.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/fonts/bootstrap/main.css" rel="stylesheet">
   <script type="text/javascript" src="${pageContext.request.contextPath}/js/game.js"></script>
   <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap-multiselect.js"></script>
	
	
	  <script>
		$(function() {
			$("#openDate").datetimepicker({format:'m/d/Y H:i'});
			$("#expiryDate").datetimepicker({format:'m/d/Y H:i'});
			$('#startTime').datetimepicker(
					{
						 datepicker:false,
						    format:'g:i A',
						    formatTime: 'g:i A',
						   
						   step:30,
						    ampm: true
						});
		
		$('#endTime').datetimepicker(
				{
					 datepicker:false,
					    format:'g:i A',
					    formatTime: 'g:i A',
					    step:30,
					    ampm: true
					});
			   
		}); 

    </script>
	
    <script>

	
	$(document).ready(function() {
		var status;
		var locationId;
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true'){
			jQuery("#locationName,#latitude,#longitude,#ratings,#address,#phoneNo,#url").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#locationName,#latitude,#longitude,#ratings,#address,#phoneNo,#url").removeClass('sorting').addClass('sorting_desc');
		}
		
		 //maintain the popup at center of the page when browser resized
		$(window).bind('resize',positionPopup); 
		 
		 
		
			var toggleOption='${toggleRating}';
			if(toggleOption=='true'){
				$('#ratingToggle').attr('checked',true);				
				$("div").find("[data-toggle='toggle']").removeClass('btn-default');
				$("div").find("[data-toggle='toggle']").removeClass('off');
				$("div").find("[data-toggle='toggle']").addClass('btn-primary');
			}
			else{
				$('#ratingToggle').attr('checked',false);				
				$("div").find("[data-toggle='toggle']").addClass('btn-default');
				$("div").find("[data-toggle='toggle']").addClass('off');
				$("div").find("[data-toggle='toggle']").removeClass('btn-primary');
			}
			
			
	});
	
	 
	/**
	Created By Bhagya On december 14th,2015
	Function for deleting the recommendation Location
	*/
		function deleteRecommendationLocation(id){
			if(confirm("Are you Sure to Delete Recommendation Location")==true){
				$.ajax({
				    url: "/kikspot/recommendations/deletelocation.do",
				    type: 'GET',
				    data:
				    	{
				    		locationId:id
				       	},
				  /*  	success: function(data) {
				   		window.location="/kikspot/recommendations/viewlocations.do";
				        alert("Recommendation Location Deleted Successfully ");
				    }, */
				    success: function(data,status,er) {
					   	
				   		 if(data=="success"){
				   			alert("Recommendation Location Deleted Successfully");
				   		
				   		}
				   		 else if(data=="userCredsAvailable"){
				   			alert("Not able to delete the recommendation location because user earned the creds by using this location rule");
				   			
				   		}
				   		window.location="/kikspot/recommendations/viewlocations.do";
				    },
				    error:function(data,status,er) {
				        alert("Error While Deleting the Recommendation Location");
				    }
				});
			}
		}
		/************************** GAME RULES ***************************/
		/* $(function() {
			$( "#expiryDate" ).datepicker({ dateFormat:"mm/dd/yy"});   
			
			
		
			  
		});  */
		
		function gameRules(locationId){
			$('.results').remove();
			$('.add-rule').attr("id",locationId);
			var $body=$('body');
				$.ajax({
				    url: "/kikspot/game/gamerulesforlocation.do",
				    type: 'POST',
				   
				   	data:{
				   		locationId : locationId
				       	},
				   	success: function(data) {
				   			
				   			if(data=="error"){
				   				$('#game_rules_tbl').append('<tr class="results" id=results><td colspan="6" class="status">No Game Rules Found,you can Add Game Rules </td></tr>'); 
				   			}
				   			var obj = JSON.parse(data);
				   			var gameRule=obj.gameRule;										
							$.each(gameRule, function(i,val) {							
								//needs to create table row...
								var rule=val.rule;
								var creds=val.creds;
								var ruleLimit=val.ruleLimit;
								var userId=val.user;
								var startTime=val.startTime;
								var endTime=val.endTime;
								var expiryDate=val.expiryDate;
								var openDate=val.openDate;
								var isActive=val.isActive;
								var gameRuleType=val.gameRuleType;
								var gameRuleId=val.gameRuleId;
								if(isActive==true){
									$('#game_rules_tbl').append('<tr class="results" id=results'+i+'><input type=hidden name=gameRuleId value='+gameRuleId+' id=gameRuleId'+i+' /><td>'+rule+'</td><td>'+creds+'</td><td>'+gameRuleType+'</td><td>'+userId+'</td><td>'+ruleLimit+'</td><td>'+startTime+'</td><td>'+endTime+'</td><td>'+openDate+'</td><td>'+expiryDate+'</td><td><select id=status'+gameRuleId+' name=status onchange=changeStatus(this,'+gameRuleId+');><option value="active" selected>ACTIVE</option><option value="inactive">INACTIVE</option></select></td><td><a href="#" onclick=deleteGameRule('+gameRuleId+','+locationId+')><i class="icon fa fa-remove"></i></a></td></tr>');
								}
								else{
									$('#game_rules_tbl').append('<tr class="results" id=results'+i+'><input type=hidden name=gameRuleId value='+gameRuleId+' id=gameRuleId'+i+' /><td>'+rule+'</td><td>'+creds+'</td><td>'+gameRuleType+'</td><td>'+userId+'</td><td>'+ruleLimit+'</td><td>'+startTime+'</td><td>'+endTime+'</td><td>'+openDate+'</td><td>'+expiryDate+'</td><td><select id=status'+gameRuleId+' name=status onchange=changeStatus(this,'+gameRuleId+');><option value="active">ACTIVE</option><option value="inactive" selected>INACTIVE</option></select></td><td><a href="#" onclick=deleteGameRule('+gameRuleId+','+locationId+')><i class="icon fa fa-remove"></i></a></td></tr>');
								}
								
								
							}); 
				   		
				    },
				    error:function(data) {
				       	alert("Error While Getting Game Rules");
				    	$('#game_rules_tbl').append('<tr class="results" id=results><td colspan="6" class="status">No Game Rules Found,you can Add Game Rules </td></tr>'); 
				    	
				    } 
				    
				   
				});
				
				//open popup for Game Rules
				$body.addClass('loading');
				$("#game_rules_popup").fadeIn(1000);
				positionPopup();
				$("#focus_div").attr("tabindex",1).focus();
		}
		
	
	
		function closeGameRulesPopup(){
				var $body=$('body');
				$("#game_rules_popup").fadeOut(300);
				$body.removeClass('loading');
			
		}

		//position the popup at the center of the page
		function positionPopup(){
			
		  if(!$("#game_rules_popup").is(':visible')){
		    return;
		  } 
		  $("#game_rules_popup").css({
		      left: ($(window).width() - $('#game_rules_popup').width()) / 2,
		      top: ($(window).width() - $('#game_rules_popup').width()) / 21,
		      position:'absolute'
		  });
		
		}
		function saveReason(){
			var reason=$('#reason').val();
			var modifiedBy=$('#modifiedBy').val();
			 // close the  reason popup
			if(null!=reason && reason.length>0 && null!=modifiedBy && modifiedBy.length>0){
				$('#savereason').attr("data-dismiss","modal");
			if(status=="open"){
				 $.ajax({
				    url: "/kikspot/recommendations/changelocationstatus.do",
				    type: 'GET',
				    data:
				    	{
				    		locationId:locationId,
				    		isOpen:true,
				    		reason:reason,
				    		modifiedBy:modifiedBy
				    	},
				   	success: function(data) {
				   	
				   		if(data=="missmatch"){
				   			alert("Game Rule Status Not Updated to Active Based on Date Criteria");
				   			window.location.reload();
				   		}
				   		else{ 
				        alert("Location Status Successfully Updated to Open");
				        location.reload();
				   		}
				    },
				    error:function(data,status,er) {
				        alert("Error While Updating Location Status");
				    }
				}); 
			}
			else if(status=="close"){
				 $.ajax({
				    url: "/kikspot/recommendations/changelocationstatus.do",
				    type: 'GET',
				    data:
				    	{
				    	    locationId:locationId,
					    	isOpen:false,
					    	reason:reason,
					    	modifiedBy:modifiedBy
				    	},
				   	success: function(data) {
				   		/* if(data=="missmatch"){
				   			alert("Game Rule Status Not Updated to Active Based on Date Criteria");
				   			window.location.reload();
				   		} 
				   		*/
				   		alert("Location Status Successfully Updated to Close");
				   		location.reload();
				   		/*}*/
				    },
				    error:function(data,status,er) {
				    	alert("Error While Updating Location Status");
				    }
				});
				 
			}
			}
			else{
				alert(" Please enter Reason and ModifiedBy Values");
			}
		}
		function changeLocationStatus(select,id){
			$('#btnTrigger').click(); // calling the reason Popup
			 status=select.value;
			locationId=id;
			$('#reason').val('');
			$('#modifiedBy').val('');
		}
	
	/**
	Craeted By Bhagya On December 11th,2015
	Function for changing the Game rule Status
	*/
	function changeStatus(sel,id){
		var status=sel.value;
		if(status=="active"){
			$.ajax({
			    url: "/kikspot/game/changegamerulestatus.do",
			    type: 'GET',
			    data:
			    	{
			    		gameRuleId:id,
			    		isActive:true
			    	},
			   	success: function(data) {
			   	
			   		 if(data=="missmatch"){
			   			alert("Game Rule Status Not Updated to Active Based on Date Criteria");
			   			window.location.reload();
			   		}
			   		else{ 
			   			
			        	alert("Game Rule Status Successfully Updated to Active");
			        	window.location.reload();
			   		 } 
			    },
			    error:function(data,status,er) {
			        alert("Error While Updating Game Rule Status to Active");
			    }
			});
		}
		else if(status=="inactive"){
			$.ajax({
			    url: "/kikspot/game/changegamerulestatus.do",
			    type: 'GET',
			    data:
			    	{
			    		gameRuleId:id,
				    	isActive:false
			    	},
			   	success: function(data) {
			   		/* if(data=="missmatch"){
			   			alert("Game Rule Status Not Updated to InActive Based on Date Criteria");
			   			window.location.reload();
			   		}
			   		else{ */
			   			alert("Game Rule Status Successfully Updated to InActive");
			   		   window.location.reload();
			   		/* } */
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating Game Status to InActive");
			    }
			});
		}
		
	}
	
	/**
	Created By Bhagya On december 11th,2015
	Function for deleting the game rule
	*/
	function deleteGameRule(id,locationId){
	if(confirm("Are you Sure to Delete Game Rule")==true){
		$.ajax({
		    url: "/kikspot/game/deletegamerule.do",
		    type: 'GET',
		    data:
		    	{
		    		gameRuleId:id
		       	},
		   	success: function(data,status,er) {
		   	
		   		 if(data=="success"){
		   			alert("Game Rule Deleted Successfully ");
		   		
		   		}
		   		 else if(data=="userCredsAvailable"){
		   			alert("Not able to delete the game rule because user earned the creds by using this rule");
		   			
		   		}
		   		
		   		gameRules(locationId);
		    },
		    error:function(data,status,er) {
		        alert("Error While Deleting the Game Rule");
		    }
		});
	}
	}
	/**
		Created By BHagya On December 31st,2015
		Function For Adding The GAme Rule For Location
	*/
	function addGameRule(id){
		var validationResult=validateGameRules();
		
		if(validationResult==true){
			var rule=$('#rule').val();
			var creds=$('#creds').val();
			var ruleLimit=$('#ruleLimit').val();
			var startTime=$('#startTime').val();
			var endTime=$('#endTime').val();
			var expiryDate=$('#expiryDate').val();
			var openDate=$('#openDate').val();
			var gameRuleType=$('#gameRuleType').val();
		    var userId=$('#userId').val();
			var status=$('#status').val();
			var status_value=true;
			if(status=="active"){
				status_value=true;
			}
			else{
				status_value=false;
			}
			
			$.ajax({
			    url: "/kikspot/game/addgameruleforlocation.do",
			    type: 'POST',
			    
			    data:
			    	{
			    		locationId:id,
			    		rule:rule,
			    		creds:creds,
			    		ruleLimit:ruleLimit,
			    		startTime:startTime,
			    		userId:userId,
			    		endTime:endTime,
			    		expiryDate:expiryDate,
			    		openDate:openDate,
			    		gameRuleType:gameRuleType,
			    		isActive:status_value
			       	},
			   	success: function(data) {
			   		alert("Game Rule Added Successfully ");
			   		gameRules(id);
			   		$('#rule').val('');
			   		$('#creds').val('');
			   		$('#ruleLimit').val('');
			   		$('#startTime').val('');
			   		$('#endTime').val('');
			   		$('#expiryDate').val('');
			   		$('#openDate').val('');
			   		$('#userId').val('');
			   		$('#gameRuleType').val('');
			   		
			   		$('#status').val('active');
			    },
			    error:function(data,status,er) {
			        alert("Error While Adding Game Rule");
			    }
			});
		}
	}

	function toggleLocationRatingLocation(id){
		var allowGlobalRating=false;
		if($('#'+id).is(':checked')){
			allowGlobalRating=true;
		}
		$.ajax({
			url :'${pageContext.request.contextPath}/recommendations/togglelocationrating.do?allowGlobalRating='+allowGlobalRating,
			method:'GET',
			success:function(data){
				if(data=="success"){
					alert("Location Rating Restriction is Toggled Successfully");
					location.href="${pageContext.request.contextPath}/recommendations/viewlocations.do";
				}
				else{
					alert(data);
				}
			}
		});
	}
	
	/**
	 * Created By bhagya on May 22nd, 2019
	 * 
	 * Function for to validate master geo location.
	 */
	function validateMasterGeoLocation(){
		
		var filter=/[^0-9.-]+/g;
		var filter1=/[^0-9.-]+/g;
		var latitude=$('#latitude').val();
		var longitude=$('#longitude').val();
		var a,b=false;
		if(null==latitude || latitude.trim().length<1){
			a=false;
			$('#latitude_error').text(" *Latitude Cannot Be Empty");
		}
		else if(filter.test(latitude)){
			a=false;
			$('#latitude_error').text(" *Latitude Value is Not Valid,Please Enter Numeric Values");
		}
		else{
			a=true;
			$('#latitude_error').text("");
		}
		if(null==longitude || longitude.trim().length<1){
			b=false;
			$('#longitude_error').text(" *Longitude Cannot Be Empty");
		}
		else if(filter1.test(longitude)){
			b=false;
			$('#longitude_error').text(" *Longitude Value is Not Valid,Please Enter Numeric Values");
		}
		else{
			b=true;
			$('#longitude_error').text("");
		}
		
		if(a==true && b==true){
			return true;
		}
		else{
			return false;
		}
		
	}
</script>
<style>
		
		/* Loader  */	 

			#loader {
			        width: 100%;
			        height: 100%;
			        position: fixed;
			        top: 0;
			        left:0;
			        z-index: -1;
			        display: none;
			    }
			
			    #loader img {
			    	position: fixed;  
			    	top: 50%;
			    	 left: 50%;
			    }
			
			    .loading #loader {
			    	z-index: 1001;background:  rgba(54, 25, 25, .5); display
			    	: inline;
			    }	
	
	/* Stylings for game rules Popup **/
	
	.game_rules_popup{
				position: absolute;
				border: 2px solid rgb(99, 102, 103);
				background: white;
				width:70%;
				height: auto;
				z-index: 9999;
				
	}
	
	.game_rules_popup .fc-text {
			font-size: 18px;
			font-weight: bold;
			float: left;
			width:100%;
			background: #262626;
			color:#fff;
	 }
				
	#focus_div{
		margin-top: 10px;
    	margin-bottom: 10px;
    	margin-left: 20px;
	}
	.game_rules_table_body{
			color: #2e2e2e;
	}
	.add_rule_td{
		color: #262626;
	}
	.add_rule_title{
		color:#2e2e2e !important;font-size: 18px !important;font-weight: bold !important;
	}
	
	
	.subDiv{
		width: 40%;
    	display: block;
    	float: left;
	}
	
	.mainDiv{
		clear:both;
		margin-top: 10px;
		    display: inline-block;
    width: 100%;

	}
	.md-textarea{
		margin:0px 0px 10px 0px;
	}
	.l-box{
		margin:0 0 10px 
	}
	.l-box-body_mn{
		display: inline-block;
		padding: 10px 0 0
	}
	
 
/*Special part*/
span.multiselect-native-select{position:relative}
span.multiselect-native-select select{border:0!important;clip:rect(0 0 0 0)!important;height:1px!important;margin:-1px -1px -1px -3px!important;overflow:hidden!important;padding:0!important;position:absolute!important;width:1px!important;left:50%;top:30px}
.multiselect-container{position:absolute;list-style-type:none;margin:0;padding:0;width:100%; height: 184px;overflow: auto;}
.multiselect-container .input-group{margin:5px}
.multiselect-container .multiselect-reset .input-group{width:93%}
.multiselect-container>li{padding:0}
.multiselect-container>li>a.multiselect-all label{font-weight:700}
.multiselect-container>li.multiselect-group label{margin:0;padding:3px 20px;height:100%;font-weight:700}
.multiselect-container>li.multiselect-group-clickable label{cursor:pointer}
.multiselect-container>li>a{padding:0}
.multiselect-container>li>a>label{margin:0;height:100%;cursor:pointer;font-weight:400;padding:3px 20px 3px 40px}
.multiselect-container>li>a>label.checkbox,.multiselect-container>li>a>label.radio{margin:0}
.multiselect-container>li>a>label>input[type=checkbox]{margin-bottom:5px}
.btn-group>.btn-group:nth-child(2)>.multiselect.btn{border-top-left-radius:4px;border-bottom-left-radius:4px}
.form-inline .multiselect-container label.checkbox,.form-inline .multiselect-container label.radio{padding:3px 20px 3px 40px}
.form-inline .multiselect-container li a label.checkbox input[type=checkbox],.form-inline .multiselect-container li a label.radio input[type=radio]{margin-left:-20px;margin-right:0}
.multiselect-native-select .btn-group{
	width: 100%
}
.multiselect-native-select .btn-default{
	width: 100%
}
.multiselect-native-select .btn{
	border-radius: 0px;
	text-align: left;
}
.mn_multi, .multiselect, .btn-group{
	width: 100%;
	text-align:left
}
	.validation_message {
    
    color: #f01;
    width: 30%
	
</style>
	
</head>

<body  class="l-dashboard  l-footer-sticky-1  ">
	<section class="l-main-container">

 		<%@ include file="/jsp/common/leftSideNavigation.jsp"%>

        
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	       
	        <!-- Middle Content Start -->
	       <div>
	         <div class="l-spaced">
	         
	         	<div class="resp-tab-content resp-tab-content-active"  style="display:block">
	         		   <div class="l-row l-spaced-bottom">
	                  <div class="l-box">
	                    <div class="l-box-header">
	                      <h2 class="l-box-title"><span>Recommendation Configurations</span></h2>
	                      
	                      
	                    </div>
	                    <div class="l-box-body">
	                    		<form method="POST" action="${pageContext.request.contextPath}/recommendations/saverecommendationconfiguration.do"   style="padding: 30px;">
	                    			<input type="hidden" name="recommendationConfigurationId" value="${recommendationConfiguration.recommendationConfigurationId}" />
	                    		
	                    			<div class="mainDiv">
	                    				<div class="subDiv">
	                    					<input type="checkbox" name="isBar"  ${recommendationConfiguration.isBar eq true ? 'checked=checked' : '' }/> Bar
	                    				</div>
	                    				
	                    				<div class="subDiv">
	                    					<input type="checkbox" name="isBowlingAlley"  ${recommendationConfiguration.isBowlingAlley eq true ? 'checked=checked' : '' } /> Bowling Alley
	                    				</div>	                    			
	                    			</div>
	                    			
	                    			<div class="mainDiv">
	                    				<div class="subDiv">
	                    					<input type="checkbox" name="isCasino"  ${recommendationConfiguration.isCasino eq true ? 'checked=checked' : '' }/> Casino
	                    				</div>
	                    				
	                    				<div class="subDiv">
	                    					<input type="checkbox" name="isLounge"  ${recommendationConfiguration.isLounge eq true ? 'checked=checked' : '' } /> Lounge
	                    				</div>	                    			
	                    			</div>
	                    			
	                    			
	                    			<div class="mainDiv">
	                    				<div class="subDiv">
	                    					<input type="checkbox" name="isNightClub"  ${recommendationConfiguration.isNightClub eq true ? 'checked=checked' : '' }/>Night Club
	                    				</div>
	                    				
	                    				<div class="subDiv">
	                    					<input type="checkbox" name="isPub"  ${recommendationConfiguration.isPub eq true ? 'checked=checked' : '' } /> Pub
	                    				</div>	                    			
	                    			</div>
	                    		
	                    		
	                    		   <div class="mainDiv">                  		   
	                    		   	<input type="submit" value="SAVE"   style="background:#E1EBFF;" />
	                               </div>
	                    		
	                    		
	                    		</form>
	                    
	                    </div>
	                  </div>
	                 
	                  <div class="l-box">
	                    <div class="l-box-header">
	                      <h2 class="l-box-title"><span>Master Geo-Location Configuration </span></h2>
	                      </div>
	                    <div class="l-box-body_mn">
	                    		<form method="POST" autocomplete="off"action="${pageContext.request.contextPath}/recommendations/savemastergeolocationconfiguration.do"   style="padding: 30px;" onsubmit="return validateMasterGeoLocation();">
	                    			<input type="hidden" name="masterId" value="${masterGeoLocation.masterId}" />
	                    		
			                    <div class="form-group col-md-12">
									<label  class="col-sm-3 control-label">Latitude :</label>
									<div class="col-sm-5">
										<input type="text" id="latitude" name="latitude" class="form-control" value="${masterGeoLocation.latitude }"/>	
										<span id="latitude_error" class="validation_message"></span>				
								    </div>
								</div>
								<div class="form-group col-md-12">
									<label  class="col-sm-3 control-label">Longitude :</label>
									<div class="col-sm-5">
										<input type="text" id="longitude" name ="longitude" class="form-control" value="${masterGeoLocation.longitude }" />		
												
										<span id="longitude_error" class="validation_message"></span>		
								    </div>
								</div>
								<div class="form-group col-md-12">
									<label  class="col-sm-3 control-label">Users :</label>
									<div class="col-sm-5">
										<select id="users" name="users" multiple="multiple">
								        <c:forEach  var="user" items="${users}" >
									   		<option value="${user.userId}" ${fn:contains(masterGeoLocatedUsers, user.username) ? 'selected' : ''}>${user.username}</option>
								        </c:forEach>
								    </select>			
								    </div>
								</div>
								<div class="form-group col-md-12">
								<label  class="col-sm-3 control-label">&nbsp;</label>
								<div class="col-sm-5">
								<div class="mainDiv">	                    		   
		                    		  <input type="submit" value="SAVE"   style="background:#E1EBFF;" />
		                        </div>
		                        </div>
		                        </div>
		                        </form>
	                    </div>
	                  </div>
	                </div>
	         	  
	         	</div>
	         
        
	       		<div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
	       			<form:form commandName="displayListBean"
							method="POST"
							class="form-horizontal" 
							action="${pageContext.request.contextPath}/recommendations/viewlocations.do"
							role="form">
		            <form:hidden path="sortBy"  value="${displayListBean.sortBy}" id="sortBy"/>
				    <form:hidden path="searchBy"/>
					<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection"/>
					<form:hidden path="pagerDto.range" />
				    <form:hidden path="pagerDto.pageNo"/>
				   
				    <c:set var="range" value="${displayListBean.pagerDto.range}"/>
				    
	               <div class="l-row l-spaced-bottom">
	                  <div class="l-box">
	                    <div class="l-box-header">
	                      <h2 class="l-box-title"><span>Recommendation Locations</span></h2>
	                      <ul class="l-box-options">
	                        <li><a href="#"><i class="fa fa-cogs"></i></a></li>
	                        <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"><i class="fa fa-expand"></i></a></li>
	                        <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
	                        <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
	                        <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"><i class="fa fa-times"></i></a></li>
	                      </ul>
	                    </div>
	                    <div class="l-box-body">
	                      <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive">
		                      <div class="dataTables_length" id="dataTableId_length">
		                      <label>Show 
			                      <select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
			                      <option value="10" ${range=='10' ? 'selected' : '' }>10</option>
			                      <option value="25" ${range=='25' ? 'selected' : '' }>25</option>
			                      <option value="50" ${range=='50' ? 'selected' : '' }>50</option>
			                      <option value="100" ${range=='100' ? 'selected' : '' }>100</option>
			                      </select> entries
		                      </label>
		                      </div>
		                      <!-- Add Rule -->
		                       <div class="addButton">
		        				 <a href="${pageContext.request.contextPath}/recommendations/addlocation.do" ><button type="button" class="btn btn-dark">Add Recommendation Location</button></a><br/><br/>
		        				</div>
		                      
		                       <div style="text-align:center;">
		                       		<label>Allow Location Rating From All Locations:</label>
		                       		<input type="checkbox"  data-toggle="toggle" onchange="toggleLocationRatingLocation(this.id);" id="ratingToggle"  >
		                       </div>
		                      <!-- SEARCH DIVISION -->
	                      	 
	                      	  <div class="form-group searchDiv" >
							  	<div class="col-sm-2" > </div>
							  	<div class="col-sm-2"> 
							  		<label class="control-label searchLbl">Search Recommendation Location:</label>
							  	</div>
							  	<div class="col-sm-4 controls">
							  		<input id="searchText" name="searchText" type="text" value="${displayListBean.searchBy}"  class="searchTxt">
							  		<span class="help-inline">Search for location Name,address,phoneNo,websiteUrl</span>
							  	</div>
							  <div class="col-sm-3" > </div>
							  </div>
						       			
							  <div class="form-group form-actions">							   
							  	<div class="col-sm-11" style="text-align: center;">
							  		<button  class="btn btn-danger" type="button" value="Search"  onclick="searchResults();">Search</button>
							  		<button class="btn btn-warning" type="button" onclick="window.location='${pageContext.request.contextPath}/recommendations/viewlocations.do'">Reset</button>
							  	</div>
							  </div>
	                      	 <!-- END OF SEARCH DIVISION -->
	                      	  <c:if test="${not empty message}">
		      					<div class="status">${message}</div><br><br> 
		      				  </c:if>
			      			  <c:if test="${not empty status}">
		                    	<div class="status"> ${status }</div>
		                      </c:if>
	                       
						    <table id="dataTableId" cellspacing="0"  width="100%" class="display dataTable table" role="grid" aria-describedby="dataTableId_info" style="width: 100%;">
	                        <thead>
	                          <tr role="row">
		                      <!--     <th>Location Id</th> -->
		                          <th class="sorting" id="locationName" onclick="sortResults('locationName');">Location Name</th>
		                          <th class="sorting" id="latitude" onclick="sortResults('latitude');">Latitude</th>
		                          <th class="sorting" id="longitude" onclick="sortResults('longitude');">Longitude</th>
		                          <th class="sorting" id="ratings" onclick="sortResults('ratings');">Rating</th>
		                          <th class="sorting" id="address" onclick="sortResults('address');">Address</th>
		                          <th class="sorting" id="phoneNo" onclick="sortResults('phoneNo');">PhoneNo</th>
		                          <th>Open Date</th> 
		                          <th>Close Date </th>
		                         <!--  <th>Distance</th> -->
		                          <th class="sorting" id="url" onclick="sortResults('url');">Website URL</th>
		                          <th>Status</th>
		                          <th>Reason</th>
		                          <th>Created By</th>
		                          <th>Created Date</th>
		                          <th>Modified By</th>
		                          <th>Status Modified Date</th>
		                          <th>Game Rules</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
	                          </tr>
	                        </thead>
	                        <tbody>
	                            <c:forEach var="location" items="${locations}">
	                       		<tr role="row">
	                           		<%-- <td>${location.kikspotLocationId}</td> --%>
	                           		<td>${location.locationName}</td>
	                           		<td>${location.latitude}</td>
	                           		<td>${location.longitude}</td>
	                           		<td>${location.ratings}</td>
	                           		<td>${location.address}</td>
	                           		<td>${location.phoneNo}</td>
	                           		<td><fmt:formatDate value='${location.openDate}'  type='date' pattern='MM/dd/yyyy'/></td>
	                           		<td><fmt:formatDate value='${location.closeDate}'  type='date' pattern='MM/dd/yyyy'/></td>
	                           	<%-- 	<td>${location.distance}</td> --%>
	                           		<td>${location.url}</td>
	                           		<td>
	                         		<select id="status${location.locationId}" name="status" onchange="changeLocationStatus(this,${location.locationId});">
					                   		<option value="open" ${location.isOpen==true ? 'selected' : '' }>OPEN</option>
				                      		<option value="close" ${location.isOpen==false ? 'selected' : '' }>CLOSE</option>
				                        </select>
	                            	</td>
	                            	<td>${location.reason}</td>
	                            	<td>${location.createdBy}</td>
	                            	<td><fmt:formatDate value='${location.locationDate}'  type='date' pattern='MM/dd/yyyy'/></td>
	                            	<td>${location.modifiedBy}</td>
	               		        	<td><fmt:formatDate value='${location.statusModifiedDate}'  type='date' pattern='MM/dd/yyyy'/></td>
	                           		<td id="game_rules${location.locationId}" onclick="gameRules(${location.locationId});" class="td_link">Game Rules</td>
	                           		<td><a href="${pageContext.request.contextPath}/recommendations/editlocation.do?locationId=${location.locationId}" title="Edit Recommendation Location"><i class="icon fa fa-edit"></i></a></td>
	                           		<td><a href="#" onclick="deleteRecommendationLocation(${location.locationId})" title="Delete Recommendation Location"><i class="icon fa fa-remove"></i></a></td>
					      			
	                          	</tr>
							    </c:forEach>
							</tbody>
							<tfoot>
			   	          	<tr>
			                	 <!--  <th>Location Id</th> -->
		                          <th>Location Name</th>
		                          <th>Latitude</th>
		                          <th>Longitude</th>
		                          <th>Rating</th>
		                          <th>Address</th>
		                          <th>PhoneNo</th>
		                          <th>Open Date</th>
		                          <th>close Date</th>
		     	            <!-- <th>Distance</th> -->
		                          <th>Website URL</th>
		                          <th>Status</th>
		                          <th>Reason</th>
		                          <th>Created By</th>
		                          <th>Created Date</th>
		                          <th>Modified By</th>
		                          <th>Status Modified Date</th>
		                          <th>Game Rules</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
			                	</tr>
			               </tfoot> 
	                      </table>
                      
	                      	<c:if test="${empty message}">
						 		<c:set var="first" value="0"/>
					     		<c:set var="end" value="${displayListBean.pagerDto.pagesNeeded }"/>
						 		<c:set var="page" value="${displayListBean.pagerDto.pageNo}"/>
						 		<c:set var="total" value="${displayListBean.pagerDto.totalItems}"/>
						 		<c:set var="firstResult" value="${displayListBean.pagerDto.firstResult}"/>
						 		<c:set var="lastResult" value="${displayListBean.pagerDto.lastResult}"/>
						 		<div class="dataTables_info" id="dataTableId_info" role="status" aria-live="polite">Showing ${firstResult} to ${lastResult} of ${total} entries</div>
							  	<div class="dataTables_paginate paging_simple_numbers" id="dataTableId_paginate">
							  		     	<%@ include file="/jsp/common/pager.jsp" %>
							  	</div>
					 		  </c:if>  
                      </div> <!-- data tables Wrapper -->
                     
                   </div><!-- l-box-body -->
                   
                  </div> <!-- l-box -->
                  
                </div> <!-- l-row-spaced -->
                
              </form:form> 
              
           </div><!-- resp-tab-content -->
            
        </div><!-- l-spaced -->
        
	</div><!-- Middle Content -->
	
		<div id="loader" ></div>
	
		<!-----------------------------------------------  POPUPUI FOR GAME RULES  ------------------------------------------------>
			  <div  class="game_rules_popup" id="game_rules_popup" style="display: none;" >
			  	<div class="title_div fc-text">
	          		<a href="#" id="close_popup" onclick="closeGameRulesPopup();"><img src="${pageContext.request.contextPath}/image/close_icon1.png"  height="28px;" width="28px;" style="float:right;"/></a> 
	            	<div class="" id="focus_div" >Game Rules</div>
	            </div>
	            <div id="focus_div" class="add_rule_title" style="margin-top: 55px;">Existing Game Rules</div>
	                <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive">
	                	<table id="game_rules_tbl"  class="display dataTable table" >
		                	<thead>
		                    	<tr>
		                        	<th>Game Rule</th>
		                         	<th>Creds</th>
		                         	<th>Condition</th>
		                         	<th>User</th>
		                         	<th>Rule Limit</th>
		                         	<th>Start Time</th>
		                         	<th>End Time</th>
		                         	<th>Open Date</th>
		                         	<th>Expiry Date</th>
		                         	<th>Status</th>
		                         	<th>Delete</th>
		                         </tr>
		                     </thead>
		                     <tbody class="game_rules_table_body">
		                         		
		                     </tbody>	
	                     </table>
	                  </div>
	                  	<!------------------ Add Game Rule ----------->
	                  <form:form>
	                  	<div class="title_div">
	          				<div id="focus_div" class="add_rule_title">Add Game Rule</div>
	            		</div>
	            		 <div class="title_div">
            				<span class="error_class" style="color:#0033BA;"> * Note: If you are not entering Start Time and End Time for Night conditions means, we will consider timings as 5PM to 9PM</span>
           				 </div>
            			<br/>
	                        <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive">
	                  		<table id="game_rules_tbl"  class="display dataTable table" >
		                	<thead>
		                    	<tr>
		                        	<th>Game Rule</th>
		                         	<th>Creds</th>
		                         	<th>Condition</th>
		                         	<th>User</th> 
		                         	<th>Rule Limit</th>
		                         	<th>Open Date</th>
		                         	<th>Expiry Date</th>
		                         	<th>Start Time</th>
		                         	<th>End Time</th>
		                         	<th>Status</th>
		                         	
		                         
		                         </tr>
		                     </thead>
		                     <tbody >
		                         	<tr>
		                         		<td class="add_rule_td"><input type="text" name="rule" id="rule"><span id="rule_error" class="error_class"></span></td>
		                         		<td class="add_rule_td"><input type="text" name="creds" id="creds"><span id="cred_error" class="error_class"></span></td>
		                         		<td class="add_rule_td">  
		                         			 <select  id="gameRuleType"  >
		                         			 	 	<option value="" selected="selected">Select Game Rule Condition<option>
						          				<c:forEach var="gameRuleType" items="${gameRuleTypes}">
								  					<option value="${gameRuleType.gameRuleType}">${gameRuleType.gameRuleType}</option>
												</c:forEach>
								  			 </select>
								 			 <span id="gameruleType_error" class="error_class"></span> 
					          			</td>
					          			<td class="add_rule_td">  
		                         			 <select  id="userId"  >
		                         			 	 	<option value="" selected="selected">Select User<option>
						          				<c:forEach var="user" items="${users}">
								  					<option value="${user.userId}">${user.username}</option>
												</c:forEach>
								  			 </select>
								 			
					          			</td>		                         		
					          			<td class="add_rule_td"><input type="text" name="ruleLimit" id="ruleLimit">
					          			<span id="ruleLimit_error" class="error_class"></span></td>
					          			<td class="add_rule_td"><input type="text" name="openDate" id="openDate"></td>
		                         		<td class="add_rule_td"><input type="text" name="expiryDate" id="expiryDate"></td>
		                         		<td class="add_rule_td"><input type="text" name="startTime" id="startTime"></td>
		                         		<td class="add_rule_td"><input type="text" name="endTime" id="endTime"></td>
		                         		<td class="add_rule_td">
		                            		<select id="status" name="status">
						                   		<option value="active">ACTIVE</option>
					                      		<option value="inactive">INACTIVE</option>
					                        </select>
		                            	</td>
	                            	   
	                            	
	                 			    <td class="add_rule_td"><button type="button"  class="btn btn-danger add-rule" onclick="addGameRule(this.id);">Add Rule</button></td>
							
		                         	</tr>	
		                     </tbody>	
	                     </table>
					     </div>
	                  </form:form>
	           </div>
	     
			<!------------------------------------------End Of Popup UI For Game Rules ----------------------------->
			
			<!------------------------------------------ Popup UI For Reason ----------------------------->
			<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" id="btnTrigger">
             </button>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reason</h4>
                  </div>
                  <div class="modal-body">
  				   <label  class="control-label">Reason</label>
                   <textarea type="text" id="reason" class="md-textarea form-control" rows="3"></textarea>
  				   <label  class="control-label">Modified By:</label>
  				   <input   id="modifiedBy"  type="text"  class="form-control" />
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="saveReason();" id="savereason">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
			<!------------------------------------------ End Of Popup UI For Reason ----------------------------->
	<!--FOOTER-->
	
	  <%@ include file="/jsp/common/footer.jsp"%>
	        
	   </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->
<script>
function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}

$('.modal').on('show.bs.modal', centerModal);
$(window).on("resize", function () {
    $('.modal:visible').each(centerModal);
});
</script>
<script type="text/javascript">
        $(function () {
            $('#users').multiselect({
                includeSelectAllOption: true
            });
            $('#btnSelected').click(function () {
                var selected = $("#users option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
            });
        });
 </script>
</body>
</html>