
<!DOCTYPE html>

<html>
<head>

	<title>List of Shared Recommendation Locations Details</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<style>
		.transparent{
			background-color: #ffffff;
			opacity: 0.6;
		}
		.label_style{
			font-size:17px;
		}
		.location_span{
			font-size:16px;
			/* color:#f4a460; */ 
			color:#0000FF
		}
		.location_title{
			    font-size: 17px;
    			font-weight: bold;
		}
	</style>
</head>
<body class="l-dashboard  l-footer-sticky-1 login-bg">

    <section class="l-main-container">
   
   	
		<section class="l-container">
	      
	        <!--HEADER-->
	       <%--  <%@ include file="/jsp/common/header.jsp"%> --%>
	      
	        <div class="l-spaced transparent" >
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title location_title">Recommendation Locations</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
	      <table id="dataTableId" cellspacing="0"  width="100%" class="display dataTable table" role="grid" aria-describedby="dataTableId_info" style="width: 100%;">
	       <thead>
	             <tr role="row">
		                      <!--     <th>Location Id</th> -->
		                          <th>Icon</th> 
		                          <th>Image</th>
		                          <th>Location Name</th>
		                          <th>Latitude</th>
		                          <th>Longitude</th>
		                          <th>Rating</th>
		                          <th>Address</th>
		                          <th>Phone</th>
		                          <th>Website URL</th>
		                          
	                          </tr>
	                        </thead>
	                          <tbody>
	                            <c:forEach var="location" items="${locations}">
	                       		<tr role="row">
	                           		<td><img alt="" src="${location.iconUrl}" height="100px;" width="150px;"></td>
	                           		<td><img alt="" src="${location.image}" height="100px;" width="150px;"></td>
	                           		<td>${location.locationName}</td>
	                           		<td>${location.latitude}</td>
	                           		<td>${location.longitude}</td>
	                           		<td>${location.ratings}</td>
	                           		<td>${location.address}</td>
	                           		<td>${location.phoneNo}</td>
	                           		<td>${location.url}</td>
	                           	</tr>
	                           	</c:forEach>
	                           	</tbody>
	 </table>
	 </div>
	 </div>
	 </div>
	 </section>
	</section>
</body>
</html>