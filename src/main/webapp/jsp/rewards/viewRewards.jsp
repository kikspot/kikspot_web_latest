
<!DOCTYPE html >
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Reward/Feature Unlocking</title>
	<%@ include file="/jsp/common/common.jsp"%>
	
    <script type="text/javascript">
    
    $(document).ready(function() {
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true'){
			jQuery("#rewardType,#reward,#rewardValue,#creds,#level,#startDate,#endDate").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#rewardType,#reward,#rewardValue,#creds,#level,#startDate,#endDate").removeClass('sorting').addClass('sorting_desc');
		}
		
	});
    
    
	/**
	Created By Bhagya On January 20th,2016
	Function for deleting the Reward/Feature
	*/
		function deleteReward(id){
			if(confirm("Are you Sure to Delete Reward/Feature")==true){
				$.ajax({
				    url: "/kikspot/rewards/deletereward.do",
				    type: 'GET',
				    data:
				    	{
				    		rewardId:id
				       	},
				   	success: function(data) {
				   		window.location="/kikspot/rewards/viewrewards.do";
				        alert("Reward/Feature Deleted Successfully ");
				   		
				    },
				    error:function(data,status,er) {
				        alert("Error While Deleting the Reward/Feature");
				    }
				});
			}
		}
	/**
		Created By Bhagya On January 20th,2016
		Function for changing the Reward Status
	*/
	function changeStatus(sel,id){
		var status=sel.value
		if(status=="active"){
			$.ajax({
			    url: "/kikspot/rewards/updaterewardstatus.do",
			    type: 'GET',
			    data:
			    	{
			    		rewardId:id,
			    		isActive:true
			    	},
			   	success: function(data) {
			        alert("Reward/Feature Status Successfully Updated to Active");
			    },
			    error:function(data,status,er) {
			        alert("Error While Updating Reward/Feature Status to Active");
			    }
			});
		}
		else if(status=="inactive"){
			$.ajax({
			    url: "/kikspot/rewards/updaterewardstatus.do",
			    type: 'GET',
			    data:
			    	{
			    		rewardId:id,
				    	isActive:false
			    	},
			   	success: function(data) {
			   		alert("Reward/Feature Status Successfully Updated to InActive");
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating Reward/Feature Status to InActive");
			    }
			});
		}
		
		
	}
	
	</script>
</head>

<body  class="l-dashboard  l-footer-sticky-1  ">
	<section class="l-main-container">

 		<%@ include file="/jsp/common/leftSideNavigation.jsp"%>

        
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	       
	        <!-- Middle Content Start -->
	       <div>
	         <div class="l-spaced">
        
	       		<div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
	       			<form:form commandName="displayListBean"
							method="POST"
							class="form-horizontal" 
							action="${pageContext.request.contextPath}/rewards/viewrewards.do"
							role="form">
		            <form:hidden path="sortBy"  value="${displayListBean.sortBy}" id="sortBy"/>
				    <form:hidden path="searchBy"/>
					<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection"/>
					<form:hidden path="pagerDto.range" />
				    <form:hidden path="pagerDto.pageNo"/>
				   
				    <c:set var="range" value="${displayListBean.pagerDto.range}"/>
				    
	               <div class="l-row l-spaced-bottom">
	                  <div class="l-box">
	                    <div class="l-box-header">
	                      <h2 class="l-box-title"><span>Reward/Feature Unlocking </span></h2>
	                      <ul class="l-box-options">
	                        <li><a href="#"><i class="fa fa-cogs"></i></a></li>
	                        <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"><i class="fa fa-expand"></i></a></li>
	                        <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
	                        <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
	                        <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"><i class="fa fa-times"></i></a></li>
	                      </ul>
	                    </div>
	                    <div class="l-box-body">
	                      <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive">
		                      <div class="dataTables_length" id="dataTableId_length">
		                      <label>Show 
			                      <select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
			                      <option value="10" ${range=='10' ? 'selected' : '' }>10</option>
			                      <option value="25" ${range=='25' ? 'selected' : '' }>25</option>
			                      <option value="50" ${range=='50' ? 'selected' : '' }>50</option>
			                      <option value="100" ${range=='100' ? 'selected' : '' }>100</option>
			                      </select> entries
		                      </label>
		                      </div>
		                      <!-- Add Reward/Feature -->
		                       <div class="addButton">
		        				 <a href="${pageContext.request.contextPath}/rewards/addreward.do" ><button type="button" class="btn btn-dark">Add Reward/Feature</button></a>
		        		       </div>
		                      
		                      <!-- SEARCH DIVISION -->
	                      	 
	                      	  <div class="form-group searchDiv" >
							  	<div class="col-sm-2" > </div>
							  	<div class="col-sm-2"> 
							  		<label class="control-label searchLbl">Search Reward/Feature:</label>
							  	</div>
							  	<div class="col-sm-4 controls">
							  		<input id="searchText" name="searchText" type="text" value="${displayListBean.searchBy}"  class="searchTxt">
							  		<span class="help-inline">Search for reward type, reward, level</span>
							  	</div>
							  <div class="col-sm-3" > </div>
							  </div>
						       			
							  <div class="form-group form-actions">							   
							  	<div class="col-sm-11" style="text-align: center;">
							  		<button  class="btn btn-danger" type="button" value="Search"  onclick="searchResults();">Search</button>
							  		<button class="btn btn-warning" type="button" onclick="window.location='${pageContext.request.contextPath}/rewards/viewrewards.do'">Reset</button>
							  	</div>
							  </div>
	                      	 <!-- END OF SEARCH DIVISION -->
	                      	  <c:if test="${not empty message}">
		      					<div class="status">${message}</div><br><br> 
		      				  </c:if>
			      			  <c:if test="${not empty status}">
		                    	<div class="status"> ${status }</div>
		                      </c:if>
	                       
						    <table id="dataTableId" cellspacing="0"  width="100%" class="display dataTable table" role="grid" aria-describedby="dataTableId_info" style="width: 100%;">
	                        <thead>
	                          <tr role="row">
		                          <th class="sorting" id="rewardType" onclick="sortResults('rewardType');">Reward Type</th>
		                          <th class="sorting" id="rewardType" onclick="sortResults('reward');">Reward</th>
		                          <th class="sorting" id="rewardValue" onclick="sortResults('rewardValue');">Reward Value</th>
		                          <th class="sorting" id="rewardType" onclick="sortResults('creds');">Creds</th>
		                          <th class="sorting" id="rewardType" onclick="sortResults('level');">Level</th>
		                          <th class="sorting" id="rewardType" onclick="sortResults('startDate');">Start Date</th>
		                          <th class="sorting" id="rewardType" onclick="sortResults('endDate');">End Date</th>
		                          <th class="sorting" id="rewardType" onclick="sortResults('locationName');">Location Name</th>
		                          <th>Status</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
	                          </tr>
	                        </thead>
	                        <tbody>
	                            <c:forEach var="rewardDto" items="${rewardsDtos}">
	                       		<tr role="row">
	                           		<td>${rewardDto.rewardType}</td>
	                           		<td>${rewardDto.reward}</td>
	                           		<td>${rewardDto.rewardValue}</td>
	                           		<td>${rewardDto.creds}</td>
	                           		<td>${rewardDto.level}</td>
	                           		<td><fmt:formatDate pattern="MM/dd/yyyy" value="${rewardDto.startDate}" /></td>
	                           		<td><fmt:formatDate pattern="MM/dd/yyyy" value="${rewardDto.endDate}" /></td>
	                           		<td>${rewardDto.locationName}</td>
	                           		<td>
	                            		<select id="status${rewardDto.rewardId}" name="status" onchange="changeStatus(this,${rewardDto.rewardId});">
					                   		<option value="active" ${rewardDto.isActive==true ? 'selected' : '' }>ACTIVE</option>
				                      		<option value="inactive" ${rewardDto.isActive==false ? 'selected' : '' }>INACTIVE</option>
				                        </select>
	                            	</td>
	                            	
	                           		<td><a href="${pageContext.request.contextPath}/rewards/editreward.do?rewardId=${rewardDto.rewardId}" title="Edit Reward"><i class="icon fa fa-edit"></i></a></td>
	                           		<td><a href="#" onclick="deleteReward(${rewardDto.rewardId})" title="Delete Reward"><i class="icon fa fa-remove"></i></a></td>
					      			
	                          	</tr>
							    </c:forEach>
							</tbody>
							<tfoot>
			                	<tr>
			                	  <th>Reward Type</th>
		                          <th>Reward</th>
		                          <th>Reward Value</th>
		                          <th>Creds</th>
		                          <th>Level</th>
		                          <th>Start Date</th>
		                          <th>End Date</th>
		                          <th>Status</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
			                	</tr>
			               </tfoot> 
	                      </table>
                      
	                      	<c:if test="${empty message}">
						 		<c:set var="first" value="0"/>
					     		<c:set var="end" value="${displayListBean.pagerDto.pagesNeeded }"/>
						 		<c:set var="page" value="${displayListBean.pagerDto.pageNo}"/>
						 		<c:set var="total" value="${displayListBean.pagerDto.totalItems}"/>
						 		<c:set var="firstResult" value="${displayListBean.pagerDto.firstResult}"/>
						 		<c:set var="lastResult" value="${displayListBean.pagerDto.lastResult}"/>
						 		<div class="dataTables_info" id="dataTableId_info" role="status" aria-live="polite">Showing ${firstResult} to ${lastResult} of ${total} entries</div>
							  	<div class="dataTables_paginate paging_simple_numbers" id="dataTableId_paginate">
							  		     	<%@ include file="/jsp/common/pager.jsp" %>
							  	</div>
					 		  </c:if>  
                      </div> <!-- data tables Wrapper -->
                     
                   </div><!-- l-box-body -->
                   
                  </div> <!-- l-box -->
                  
                </div> <!-- l-row-spaced -->
                
              </form:form> 
              
           </div><!-- resp-tab-content -->
            
        </div><!-- l-spaced -->
        
	</div><!-- Middle Content -->
			
	<!--FOOTER-->
	  <%@ include file="/jsp/common/footer.jsp"%>
	        
	   </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>