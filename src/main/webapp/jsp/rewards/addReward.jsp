<!DOCTYPE html>

<html>
<head>

	<title>Add Reward/Feature</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/reward.js"></script>
	 <script>
		$(function() {
			$( "#startDate" ).datepicker({ dateFormat:"mm/dd/yy"}); 
		}); 

		$(function() {
			$( "#endDate" ).datepicker({ dateFormat:"mm/dd/yy"});   
			  
		}); 
    </script>
	
</head>


<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
    <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
	
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	      
	       	 	<div class="l-spaced">
	        
	       		 	<div class="l-box l-spaced-bottom"> 
	       			<!-- Middle Content Start -->
					<div class="l-box-header">
						<h2 class="l-box-title">Add Reward/Feature</h2>
					</div>
					
					<div class="l-box-body l-spaced">
						<form:form class="form-horizontal" autocomplete="off"  action="${pageContext.request.contextPath}/rewards/addreward.do"  method="POST" commandName="kikspotRewardsDto"  onsubmit="return validateRewards();">
							<!-- Added Note and Location Id by BHagya On Aug 14th,2018 -->
				             <div class="col-sm-offset-3 col-sm-9">
				            	<span class="error_class" style="color:#0033BA;"> * Note: LocationId is of GooglePlaceAPI</span>
				            </div>
				            <div class="form-group">
				            	<label  class="col-sm-3 control-label">LocationId :</label>
								<div class="col-sm-9">
									<form:input path="locationId"  id="locationId"  type="text" class="form-control" />
								 </div>
							</div>
							<br/>
						<div class="form-group">
				          <label  class="col-sm-3 control-label">Reward Type :</label>
				             <div class="col-sm-9">
					         <form:select path="rewardType" id="rewardType" class="form-control">
					         	<form:option value="" selected="selected">Select Reward Type</form:option>
								<form:option value="APPFEATURE">APPFEATURE</form:option>
								<form:option value="GIVEAWAY">GIVEAWAY</form:option>
							</form:select>
							   <span id="rewardType_error" class="validation_error">
					          <form:errors path="rewardType" class="error_class"/>	
					          </span> 
			                 </div>
			            </div>
			            
		
						<div class="form-group">
				          <label  class="col-sm-3 control-label">Reward :</label>
				             <div class="col-sm-9">
					          <form:select path="reward" id="reward" class="form-control">
					          	<form:option value="" selected="selected">Select Reward</form:option>
					          	<form:option value="Coupons">Coupons</form:option>
								<form:option value="Gift Voucher">Gift Voucher</form:option>
							 </form:select>
							  <span id="reward_error" class="validation_error">
					          <form:errors path="reward" class="error_class"/>	
					          </span>
			                 </div>
			            </div>
			            
			            <div class="form-group">
				          <label  class="col-sm-3 control-label">Reward Value :</label>
				             <div class="col-sm-9">
					          <form:input path="rewardValue"  id="rewardValue"  type="text" class="form-control" />
					           <span id="rewardValue_error" class="validation_error"> 
					            <form:errors path="rewardValue" class="error_class"/>
					            </span>
			                 </div>
			             </div>
			             
			            <div class="form-group">
				          <label  class="col-sm-3 control-label">Cred Amount :</label>
				             <div class="col-sm-9">
					          <form:input path="creds"  id="creds"  type="text" class="form-control" />
					           <span id="creds_error" class="validation_error"></span> 
			                 </div>
			             </div>
			             
			             <div class="form-group">
				          <label  class="col-sm-3 control-label">Level:</label>
				             <div class="col-sm-9">
						          <form:select path="level" id="level" class="form-control">
						          	<form:option value="" selected="selected">Select Level</form:option>
								  		<c:forEach var="level" items="${levels}">
								  			<form:option value="${level.level}">${level.level }</form:option>
										</c:forEach>
								  </form:select>
								  <span id="level_error" class="validation_error">
					              <form:errors path="level" class="error_class"/>	
					            </span> 
			                 </div>
			            </div>		
						
			              <div class="form-group">
				            <label  class="col-sm-3 control-label">Start Date :</label>
				             <div class="col-sm-9">
					            <form:input  path="startDate" id="startDate"  type="text" class="form-control"  />
					               <span id="startDate_error" class="validation_error">
			       	               <form:errors path="startDate" class="error_class"/>	
			   		              </span> 
			                   </div>
			              </div>
			             
			             
			             <div class="form-group">
				            <label  class="col-sm-3 control-label">End Date :</label>
				             <div class="col-sm-9">
					            <form:input  path="endDate" id="endDate"  type="text" class="form-control"  />
					            <span id="endDate_error" class="validation_error">
			       	               <form:errors path="endDate" class="error_class"/>
			       	             </span>	
					         </div>
			              </div>
			             
			             
			              <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-dark">Submit</button>
			              </div>
						</form:form>
			
                   </div>
			</div>
		</div>
	</section>
</section>


</body>