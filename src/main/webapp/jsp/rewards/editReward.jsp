<!DOCTYPE html>

<html>
<head>

	<title>Edit Reward/Feature</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/reward.js"></script>
	 <script>
		$(function() {
			$( "#startDate" ).datepicker({ dateFormat:"mm/dd/yy"}); 
		}); 

		$(function() {
			$( "#endDate" ).datepicker({ dateFormat:"mm/dd/yy"});   
			  
		}); 
    </script>
	
</head>


<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
    <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
	
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	      
	       	 	<div class="l-spaced">
	        
	       		 	<div class="l-box l-spaced-bottom"> 
	       			<!-- Middle Content Start -->
					<div class="l-box-header">
						<h2 class="l-box-title">Edit Reward/Feature</h2>
					</div>
					
					<div class="l-box-body l-spaced">
						<form:form class="form-horizontal" autocomplete="off"  action="${pageContext.request.contextPath}/rewards/editreward.do"  method="POST" commandName="kikspotRewardsDto"  onsubmit="return validateRewards();">
							
							<form:input type="hidden" path="rewardId" name="rewardId" value="${rewardDto.rewardId}"/>
							
							 <!-- Added Note and Location Id by BHagya On Aug 14th,2018 -->
				             <div class="col-sm-offset-3 col-sm-9">
				            	<span class="error_class" style="color:#0033BA;"> * Note: LocationId is of GooglePlaceAPI</span>
				            </div>
				            <div class="form-group">
				            	<label  class="col-sm-3 control-label">LocationId :</label>
								<div class="col-sm-9">
									<form:input path="locationId"  id="locationId"  type="text" class="form-control" value="${rewardDto.locationId}"/>
								 </div>
							</div>
							<br/>
						<div class="form-group">
				          <label  class="col-sm-3 control-label">Reward Type :</label>
				             <div class="col-sm-9">
					         <form:select path="rewardType" id="rewardType" class="form-control">
					         	<option value="" selected="selected">Select Reward Type</option>
					         	<option value="APPFEATURE" ${rewardDto.rewardType eq 'APPFEATURE'? 'selected="true"' : '' }>APPFEATURE</option>
					         	<option value="GIVEAWAY" ${rewardDto.rewardType eq 'GIVEAWAY' ? 'selected="true"' : '' } >GIVEAWAY</option>
							</form:select>
							   <span id="rewardType_error" class="validation_error">
					          <form:errors path="rewardType" class="error_class"/>	
					          </span> 
			                 </div>
			            </div>
			            
		
						<div class="form-group">
				          <label  class="col-sm-3 control-label">Reward :</label>
				             <div class="col-sm-9">
					          <form:select path="reward" id="reward" class="form-control">
					          	<option value="" selected="selected">Select Reward</option>
					          	<option value="Coupons" ${rewardDto.reward eq 'Coupons' ? 'selected' : '' }>Coupons</option>
					          	<option value="Gift Voucher" ${rewardDto.reward eq 'Gift Voucher' ? 'selected' : '' }>Gift Voucher</option>
							 </form:select>
							  <span id="reward_error" class="validation_error">
					          <form:errors path="reward" class="error_class"/>	
					          </span>
			                 </div>
			            </div>
			            
			             <div class="form-group">
				          <label  class="col-sm-3 control-label">Reward Value :</label>
				             <div class="col-sm-9">
					          <form:input path="rewardValue"  id="rewardValue"  type="text" class="form-control" value="${rewardDto.rewardValue}"/>
					           <span id="rewardValue_error" class="validation_error"> 
					            <form:errors path="rewardValue" class="error_class"/>
					            </span>
			                 </div>
			             </div>
			            
			            <div class="form-group">
				          <label  class="col-sm-3 control-label">Cred Amount :</label>
				             <div class="col-sm-9">
					          <form:input path="creds"  id="creds"  type="text" class="form-control"  value="${rewardDto.creds}"/>
					           <span id="creds_error" class="validation_error"></span> 
			                 </div>
			             </div>
			             
			             <div class="form-group">
				          <label  class="col-sm-3 control-label">Level:</label>
				             <div class="col-sm-9">
						          <form:select path="level" id="level" class="form-control">
						          	<option value="" selected="selected">Select Level</option>
								  		<c:forEach var="level" items="${levels}">
								  			<option value="${level.level}" ${rewardDto.level eq level.level ? 'selected' : '' }>${level.level }</option>
										</c:forEach>
								  </form:select>
								  <span id="level_error" class="validation_error">
					              <form:errors path="level" class="error_class"/>	
					            </span> 
			                 </div>
			            </div>		
						
			              <div class="form-group">
				            <label  class="col-sm-3 control-label">Start Date :</label>
				             <div class="col-sm-9">
				             	<fmt:formatDate value="${rewardDto.startDate}" var="startDateFormat" type="date" pattern="MM/dd/yyyy"/>
					            <form:input  path="startDate" id="startDate"  type="text" class="form-control"  value="${startDateFormat}" />
					               <span id="startDate_error" class="validation_error">
			       	               <form:errors path="startDate" class="error_class"/>	
			   		              </span> 
			                   </div>
			              </div>
			             
			             
			             <div class="form-group">
				            <label  class="col-sm-3 control-label">End Date :</label>
				             <div class="col-sm-9">
				             	<fmt:formatDate value="${rewardDto.endDate}" var="endDateFormat" type="date" pattern="MM/dd/yyyy"/>
					            <form:input  path="endDate" id="endDate"  type="text" class="form-control"  value="${endDateFormat}"/>
					            <span id="endDate_error" class="validation_error">
			       	               <form:errors path="endDate" class="error_class"/>	
			   		              </span> 
					         </div>
			              </div>
			             
			              <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-dark">Submit</button>
			              </div>
						</form:form>
			
                   </div>
			</div>
		</div>
	</section>
</section>

</body>