<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/user.js"></script>
<%@ include file="/jsp/common/common.jsp"%>
<title>Forgot Password</title>
</head>
<body class="login-bg">
	<form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/forgotpassword.do"  method='POST' onsubmit="return validateForgetPasswordForm();">  
    	<div class="col-md-4"></div>
		<div class="col-md-5 content mb_forgot">
	    	<div class="logopanel mb_forgot_1">
				<h1 style="text-align:center;"><img src="image/kikspot_logo.png"></h1>
			</div>
			<div class="content-title">Forgot Password</div>	
			<div class="form-group">
				<label  class="col-sm-3 control-label" style="font-size:18px;">Email :</label>
				<div class="col-sm-9">
					<input name="emailId"  id="email" placeholder="Please Enter Your Registered Email" type="text" class="form-control" />
					<span id="email_error" class="error_class"></span> 
				</div>
			</div>
			<br/> 
			<div class="col-sm-offset-3 col-sm-9" style="text-align:right">
            	<button type="submit" class="btn btn-dark">Retreive Password</button>
			</div>      
			    
		 </div>
		<div class="col-md-3"></div>
					
 	</form>  
 </body>
</html>