<!DOCTYPE html>

<html>
<head>

	<title>Add Game rule</title>
	<%@ include file="/jsp/common/common.jsp"%>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/game.js"></script>
	
     <script>
		$(function() {
			$("#openDate").datetimepicker({format:'m/d/Y H:i'});
			$("#expiryDate").datetimepicker({format:'m/d/Y H:i'});     
				
			
			$('#startTime').datetimepicker(
					{
						 datepicker:false,
						    format:'g:i A',
						    formatTime: 'g:i A',
						   
						   step:30,
						    ampm: true
						});
		
		$('#endTime').datetimepicker(
				{
					 datepicker:false,
					    format:'g:i A',
					    formatTime: 'g:i A',
					    step:30,
					    ampm: true
					});
			   
		}); 

    </script>
</head>
<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
    <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
	
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	      
	        <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Add Game Rule</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form:form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/game/addgamerule.do" method="POST" commandName="gameRulesDto" onsubmit="return validateGameRules();" >
			<!-- Added Note and Location Id by BHagya On Jan 04th,2016 -->
             <div class="col-sm-offset-3 col-sm-9">
            	<span class="error_class" style="color:#0033BA;"> * Note: LocationId is of GooglePlaceAPI</span>
            </div>
            <div class="form-group">
            	<label  class="col-sm-3 control-label">LocationId :</label>
				<div class="col-sm-9">
					<form:input path="locationId"  id="locationId"  type="text" class="form-control" oninput="LocationIdInput();" />
					 <span id="locationId_error" class="validation_error">
			       <form:errors path="locationId" class="error_class"/>	
			     </span>
				 </div>
			</div>
			<br/>
			 <div class="form-group">
            	<label  class="col-sm-3 control-label">Location Name :</label>
				<div class="col-sm-9">
					<form:input path="locationName"  id="locationName"  type="text" class="form-control" value="" readonly="true"/>
				 </div>
			</div>
			<br/>
			
        	<div class="form-group">
				<label  class="col-sm-3 control-label">Creds :</label>
				<div class="col-sm-9">
					<form:input path="creds"  id="creds"  type="text" class="form-control" />
					<span id="cred_error" class="validation_error">
					<form:errors path="creds" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
		
		 
		
			<div class="form-group">
				<label  class="col-sm-3 control-label">Rule :</label>				
				<div class="col-md-9">							
					    <form:textarea path="rule" class="form-control valid" cols="50" id="rule" maxlength="2500" name="Resume" rows="5"></form:textarea>
						<span id="rule_error" class="validation_error">
							<form:errors path="rule" class="error_class"/>	
						</span> 
					<div id="text-counter-parent"><span id="text-counter">2500</span> character(s) left.</div>
				</div>
			</div>
			<br/>
            
            
             <div class="form-group">
				          <label  class="col-sm-3 control-label">Condition:</label>
				             <div class="col-sm-9">
						          <form:select path="gameRuleType" id="gameRuleType" class="form-control">
						          	<form:option value="" selected="selected">Select Game Rule Condition</form:option>
								  		<c:forEach var="gameRuleType" items="${gameRuleTypes}">
								  			<form:option value="${gameRuleType.gameRuleType}">${gameRuleType.gameRuleType}</form:option>
										</c:forEach>
								  </form:select>
								  <span id="gameruleType_error" class="validation_error">
					              <form:errors path="gameRuleType" class="error_class"/>	
					            </span> 
			                 </div>
			 </div>	
			<br/>
			
          <div class="form-group">
				          <label  class="col-sm-3 control-label">User Id:</label>
				             <div class="col-sm-9">
						          <form:select path="userId" id="userId" class="form-control">
						          	<form:option value="" selected="selected">Select User</form:option>
								  		<c:forEach var="user" items="${users}">
								  			<form:option value="${user.userId}">${user.username}</form:option>
										</c:forEach>
								  </form:select>
			                 </div>
			 </div>	
			 <br/> 
			 
			<div class="form-group">
				<label  class="col-sm-3 control-label">Rule Limit :</label>
				<div class="col-sm-9">
					<form:input path="ruleLimit"  id="ruleLimit"  type="text" class="form-control"  />
					<span id="ruleLimit_error" class="validation_error">
					<form:errors path="ruleLimit" class="error_class"/>	
					</span>
			    </div> 
			</div>
			<br/>
			
			 <div class="form-group">
				<label  class="col-sm-3 control-label">Open Date :</label>
				<div class="col-sm-9">
					<form:input  path="openDate" id="openDate"  type="text" class="form-control"  /> 
			    </div>
			</div>
			<br/>
		    <div class="form-group">
				<label  class="col-sm-3 control-label">Expiry Date :</label>
				<div class="col-sm-9">
					<form:input  path="expiryDate" id="expiryDate"  type="text" class="form-control"  />
			    </div>
			</div>
			<br/>
			  <div class="form-group">
				<label  class="col-sm-3 control-label">Start Time:</label>
				<div class="col-sm-9">
					<form:input  path="startTime" id="startTime"  type="text" class="form-control"  /> 
			    </div>
			</div>
			<br/>
			<div class="form-group">
				<label  class="col-sm-3 control-label">End Time:</label>
				<div class="col-sm-9">
					<form:input  path="endTime" id="endTime"  type="text" class="form-control"  /> 
			    </div>
			</div>
			 <div class="col-sm-offset-3 col-sm-9">
            	<span class="error_class" style="color:#0033BA;"> * Note: If you are not entering Start Time and End Time for Night conditions means, we will consider timings as 5PM to 9PM</span>
            </div>
            <br/>
            <br/>
		    <div class="col-sm-offset-3 col-sm-9">
                 <button type="submit" class="btn btn-dark">Submit</button>
			</div>

			</form:form>
			</div>
			</div>
			</div>
				
	
   			
   			
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>