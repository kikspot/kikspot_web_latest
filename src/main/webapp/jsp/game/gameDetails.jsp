<!DOCTYPE html>

<html>
<head>

	<title>Game Score  Details</title>
	<%@ include file="/jsp/common/common.jsp"%>
	
	<style>
		.transparent{
			background-color: #ffffff;
			opacity: 0.6;
		}
		.label_style{
			font-size:17px;
		}
		.location_span{
			font-size:20px;
			/* color:#f4a460; */ 
			color:#0000FF
		}
		.location_title{
			    font-size: 17px;
    			font-weight: bold;
		}
		p{
		    font-size:25px;
		    font-weight:bold;
		    color:#00dd11;
		}
		.location{
			font-size:35px;
			font-weight:bold;
			color:#0000FF
		}
		a{
		    font-size:30px;
			font-weight:bold;
			color:#0000FF
		}
	</style>
</head>
<body class="l-dashboard  l-footer-sticky-1 login-bg">

    <section class="l-main-container">
   
   	
		<section class="l-container">
	      
	        <!--HEADER-->
	       <%--  <%@ include file="/jsp/common/header.jsp"%> --%>
	      
	        <div class="l-spaced transparent" >
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title location_title">Game Score</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			
			<form class="form-horizontal" autocomplete="off" action="" >
			<div class="form-group">
					 <label  class="col-sm-3 control-label label_style">User Name :</label>
					<div class="col-sm-9">
						<span class="location_span">${gameCredDto.kikspotUserDto.username}</span>
					</div>
				</div>
				<br/>
				
				<div class="form-group">
					 <label  class="col-sm-3 control-label label_style">Game Name :</label>
					<div class="col-sm-9">
						<span class="location_span">${gameCredDto.gameDto.gameName}</span>
					</div>
				</div>
				<br/>
				
            	<div class="form-group">
					<label  class="col-sm-3 control-label label_style">Score :</label>
					<div class="col-sm-9">
						<span class="location_span">${gameCredDto.creds}</span>
					</div>
				</div>
			<br/>
        	
        	<div class="form-group">
					<label  class="col-sm-3 control-label label_style"> Game Level:</label>
					<div class="col-sm-9">
						<span class="location_span">${gameCredDto.level}</span>
					</div>
				</div>
			<br/>
			<div class="form-group">
					<label  class="col-sm-3 control-label label_style">Rank:</label>
					<div class="col-sm-9">
						<span class="location_span">${rank}</span>
					</div>
				</div>
			<br/>
			</form>
			
			<div class="col-sm-3"></div> <a href="https://itunes.apple.com/us/app/kikspot/id1118403068?ls=1&mt=8">"Hey! Checkout this new app, Kikspot! "</a>
			
	
			</div>
			</div>
			</div>
			</section>
			</section>
			

</body>
</html>