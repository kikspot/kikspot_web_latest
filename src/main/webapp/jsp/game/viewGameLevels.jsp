
<!DOCTYPE html >
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Game Levels</title>
	<%@ include file="/jsp/common/common.jsp"%>
	
    <script type="text/javascript">
	/**
	Created By Bhagya On december 14th,2015
	Function for deleting the game Level
	*/
		function deleteGameLevel(id,level){
			if(confirm("Are you Sure to Delete Game Level")==true){
				$.ajax({
				    url: "/kikspot/game/deletegamelevel.do",
				    type: 'GET',
				    data:
				    	{
				    		gameLevelId:id,
				    		gameLevel:level
				       	},
				   	success: function(data) {
				   		if(data=="error"){
				   			alert("Users Exist Under Game Level,it Cannot be Deleted");
				   		}
				   		else{
				   		window.location="/kikspot/game/viewgamelevels.do";
				        alert("Game Level Deleted Successfully ");
				   		}
				    },
				    error:function(data,status,er) {
				        alert("Error While Deleting the Game Level");
				    }
				});
			}
		}
	</script>
</head>

<body  class="l-dashboard  l-footer-sticky-1  ">
	<section class="l-main-container">

 		<%@ include file="/jsp/common/leftSideNavigation.jsp"%>

        
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	       
	        <!-- Middle Content Start -->
	       <div>
	         <div class="l-spaced">
        
	       		<div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
	       			<form:form commandName="displayListBean"
							method="POST"
							class="form-horizontal" 
							action="${pageContext.request.contextPath}/game/viewgamelevels.do"
							role="form">
		            <form:hidden path="sortBy"  value="${displayListBean.sortBy}" id="sortBy"/>
				    <form:hidden path="searchBy"/>
					<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection"/>
					<form:hidden path="pagerDto.range" />
				    <form:hidden path="pagerDto.pageNo"/>
				   
				    <c:set var="range" value="${displayListBean.pagerDto.range}"/>
				    
	               <div class="l-row l-spaced-bottom">
	                  <div class="l-box">
	                    <div class="l-box-header">
	                      <h2 class="l-box-title"><span>Game Levels</span></h2>
	                      <ul class="l-box-options">
	                        <li><a href="#"><i class="fa fa-cogs"></i></a></li>
	                        <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"><i class="fa fa-expand"></i></a></li>
	                        <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
	                        <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
	                        <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"><i class="fa fa-times"></i></a></li>
	                      </ul>
	                    </div>
	                    <div class="l-box-body">
	                      <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive">
		                      <div class="dataTables_length" id="dataTableId_length">
		                      <label>Show 
			                      <select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
			                      <option value="10" ${range=='10' ? 'selected' : '' }>10</option>
			                      <option value="25" ${range=='25' ? 'selected' : '' }>25</option>
			                      <option value="50" ${range=='50' ? 'selected' : '' }>50</option>
			                      <option value="100" ${range=='100' ? 'selected' : '' }>100</option>
			                      </select> entries
		                      </label>
		                      </div>
		                      <!-- Add Rule -->
		                       <div class="addButton">
		        				 <a href="${pageContext.request.contextPath}/game/addgamelevel.do" ><button type="button" class="btn btn-dark">Add Level</button></a>
		        		       </div>
		                      
		                      <!-- SEARCH DIVISION -->
	                      	 
	                      	  <div class="form-group searchDiv" >
							  	<div class="col-sm-2" > </div>
							  	<div class="col-sm-2"> 
							  		<label class="control-label searchLbl">Search Game Level:</label>
							  	</div>
							  	<div class="col-sm-4 controls">
							  		<input id="searchText" name="searchText" type="text" value="${displayListBean.searchBy}"  class="searchTxt">
							  		<span class="help-inline">Search for game level</span>
							  	</div>
							  <div class="col-sm-3" > </div>
							  </div>
						       			
							  <div class="form-group form-actions">							   
							  	<div class="col-sm-11" style="text-align: center;">
							  		<button  class="btn btn-danger" type="button" value="Search"  onclick="searchResults();">Search</button>
							  		<button class="btn btn-warning" type="button" onclick="window.location='${pageContext.request.contextPath}/game/viewgamelevels.do'">Reset</button>
							  	</div>
							  </div>
	                      	 <!-- END OF SEARCH DIVISION -->
	                      	  <c:if test="${not empty message}">
		      					<div class="status">${message}</div><br><br> 
		      				  </c:if>
			      			  <c:if test="${not empty status}">
		                    	<div class="status"> ${status }</div>
		                      </c:if>
	                       
						    <table id="dataTableId" cellspacing="0"  width="100%" class="display dataTable table" role="grid" aria-describedby="dataTableId_info" style="width: 100%;">
	                        <thead>
	                          <tr role="row">
		                          <th>Game Level</th>
		                          <th>Minimum Creds</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
	                          </tr>
	                        </thead>
	                        <tbody>
	                            <c:forEach var="gameLevel" items="${gameLevels}">
	                       		<tr role="row">
	                           		<td>${gameLevel.level }</td>
	                           		<td>${gameLevel.minCreds }</td>
	                           		<td><a href="${pageContext.request.contextPath}/game/editgamelevel.do?gameLevelId=${gameLevel.gameLevelId}"  title="Edit Game Level"><i class="icon fa fa-edit"></i></a></td>
	                           		<td><a href="#" onclick="deleteGameLevel(${gameLevel.gameLevelId}, '${gameLevel.level}')" title="Delete Game Level"><i class="icon fa fa-remove"></i></a></td>
					      			
	                          	</tr>
							    </c:forEach>
							</tbody>
							<tfoot>
			                	<tr>
			                	  <th>Game Level</th>
		                          <th>Minimum Creds</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
			                	</tr>
			               </tfoot> 
	                      </table>
                      
	                      	<c:if test="${empty message}">
						 		<c:set var="first" value="0"/>
					     		<c:set var="end" value="${displayListBean.pagerDto.pagesNeeded }"/>
						 		<c:set var="page" value="${displayListBean.pagerDto.pageNo}"/>
						 		<c:set var="total" value="${displayListBean.pagerDto.totalItems}"/>
						 		<c:set var="firstResult" value="${displayListBean.pagerDto.firstResult}"/>
						 		<c:set var="lastResult" value="${displayListBean.pagerDto.lastResult}"/>
						 		<div class="dataTables_info" id="dataTableId_info" role="status" aria-live="polite">Showing ${firstResult} to ${lastResult} of ${total} entries</div>
							  	<div class="dataTables_paginate paging_simple_numbers" id="dataTableId_paginate">
							  		     	<%@ include file="/jsp/common/pager.jsp" %>
							  	</div>
					 		  </c:if>  
                      </div> <!-- data tables Wrapper -->
                     
                   </div><!-- l-box-body -->
                   
                  </div> <!-- l-box -->
                  
                </div> <!-- l-row-spaced -->
                
              </form:form> 
              
           </div><!-- resp-tab-content -->
            
        </div><!-- l-spaced -->
        
	</div><!-- Middle Content -->
			
	<!--FOOTER-->
	  <%@ include file="/jsp/common/footer.jsp"%>
	        
	   </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>