<!DOCTYPE html>

<html>
<head>

	<title>Edit Game Level</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/game.js"></script>
	
</head>
<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
    <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
	
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	      
	        <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Edit Game Level</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form:form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/game/editgamelevel.do" method="POST" commandName="gameLevelsDto" onsubmit="return validateGameLevel();" >
              <form:input type="hidden" path="gameLevelId" name="gameLevelId" value="${gameLevel.gameLevelId }" />
        	<div class="form-group">
				<label  class="col-sm-3 control-label">Minimum Creds  :</label>
				<div class="col-sm-9">
					<form:input path="minCreds"  id="minCreds"  type="text" class="form-control" value="${gameLevel.minCreds }"/>
					<span id="minCreds_error" class="validation_error">
					<form:errors path="minCreds" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>
		
			<div class="form-group">
				<label  class="col-sm-3 control-label">Level :</label>
				<div class="col-sm-9">
					<form:input path="level"  id="level"  type="text"  class="form-control" value="${gameLevel.level}" />
					<span id="level_error" class="validation_error">
					<form:errors path="level" class="error_class"/>	
					</span> 
			    </div>
			</div>
			<br/>

			
		    <div class="col-sm-offset-3 col-sm-9">
                 <button type="submit" class="btn btn-dark">Submit</button>
			</div>

			</form:form>
			</div>
			</div>
			</div>
				
	
   			
   			
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>