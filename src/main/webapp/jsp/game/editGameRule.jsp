
<!DOCTYPE html ">

<html>

<head>
	
	<title>Edit Game Rules</title>
	<%@ include file="/jsp/common/common.jsp"%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/game.js"></script>
	  <script>
	  $(function() {
		  $("#openDate").datetimepicker({format:'m/d/Y H:i'});
			$("#expiryDate").datetimepicker({format:'m/d/Y H:i'});    
				
				$('#startTime').datetimepicker(
						{
							 datepicker:false,
							    format:'g:i A',
							    formatTime: 'g:i A',
							   
							   step:30,
							    ampm: true
							});
			
			$('#endTime').datetimepicker(
					{
						 datepicker:false,
						    format:'g:i A',
						    formatTime: 'g:i A',
						    step:30,
						    ampm: true
						});
			   
		}); 

    </script>
</head>


<body class="l-dashboard  l-footer-sticky-1  ">

	<section class="l-main-container">
     <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
   		<section class="l-container">
    			<!--HEADER-->
	    <%@ include file="/jsp/common/header.jsp"%>
	        
	       <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
	        
			<div class="l-box-header">
				<h2 class="l-box-title">Edit Game Rule</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form:form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/game/editgamerule.do" method="POST" commandName="gameRulesDto" onsubmit="return validateGameRules();" >
            
            <!-- Added Note and Location Id by BHagya On Jan 04th,2016 -->
             <div class="col-sm-offset-3 col-sm-9">
            	<span class="error_class" style="color:#0033BA;"> * Note: LocationId is of GooglePlaceAPI</span>
            </div>
            <div class="form-group">
            	<label  class="col-sm-3 control-label">LocationId :</label>
				<div class="col-sm-9">
					<form:input path="locationId"  id="locationId"  type="text" class="form-control" value="${gameRulesDto.locationId}" oninput="LocationIdInput();"/>
					 <span id="locationId_error" class="validation_error">
			       <form:errors path="locationId" class="error_class"/>	
			     </span>
				 </div>
			</div>
			<br/>
			 <div class="form-group">
            	<label  class="col-sm-3 control-label">Location Name :</label>
				<div class="col-sm-9">
					<form:input path="locationName"  id="locationName"  type="text" class="form-control" value="" readonly="true"/>
				 </div>
			</div>
			<br/>
        	<div class="form-group">
				<label  class="col-sm-3 control-label">Creds  :</label>
				<div class="col-sm-9">
				<form:input path="creds"  id="creds"  type="text" class="form-control" value="${gameRulesDto.creds }"/>
				 <span id="cred_error" class="validation_error">
			       <form:errors path="creds" class="error_class"/>	
			     </span> 
			     </div>
			</div>
			<br/>
		
		 	<div class="form-group">
				<label  class="col-sm-3 control-label">Rule :</label>				
				<div class="col-md-9">							
					    <form:textarea path="rule" class="form-control valid" cols="50" id="rule" maxlength="2500" name="Resume" rows="5" value="${gameRulesDto.rule}"></form:textarea>
						<span id="rule_error" class="validation_error">
							<form:errors path="rule" class="error_class"/>	
						</span> 
					<div id="text-counter-parent"><span id="text-counter">2500</span> character(s) left.</div>
				</div>
			</div>
		
			<%-- <div class="form-group">
				<label  class="col-sm-3 control-label">Rule :</label>
				<div class="col-sm-9">
				<form:input path="rule"  id="rule"  type="text"  class="form-control" value="${gameRulesDto.rule }" />
				<span id="rule_error" class="validation_error">
			       <form:errors path="rule" class="error_class"/>	
			    </span> 
			    </div>
			</div>
			<br/> --%>

			  <div class="form-group">
				          <label  class="col-sm-3 control-label">Condition:</label>
				             <div class="col-sm-9">
						          <form:select path="gameRuleType" id="gameRuleType" class="form-control">
						          	<option value="" selected="selected">Select Game Rule Condition<option>
								  		<c:forEach var="gameRuleType" items="${gameRuleTypes}">
								  			<option value="${gameRuleType.gameRuleType}" ${gameRulesDto.gameRuleType eq gameRuleType.gameRuleType ? 'selected' : ''} >${gameRuleType.gameRuleType}</option>
										</c:forEach>
								  </form:select>
								  <span id="gameruleType_error" class="validation_error">
					              <form:errors path="gameRuleType" class="error_class"/>	
					            </span> 
			                 </div>
			 </div>	

			<br/>
               <div class="form-group">
				          <label  class="col-sm-3 control-label">User Id:</label>
				             <div class="col-sm-9">
						          <form:select path="userId" id="userId" class="form-control">
						          	<option value="" selected="selected">Select User<option>
								  		<c:forEach var="user" items="${users}">
								  			<option value="${user.userId}" ${gameRulesDto.userId eq user.userId ? 'selected' : ''} >${user.username}</option>
										</c:forEach>
								  </form:select>
			                 </div>
			 </div>	
			 <br/> 
			<div class="form-group">
				<label  class="col-sm-3 control-label">Rule Limit :</label>
				<div class="col-sm-9">
				<form:input path="ruleLimit"  id="ruleLimit"  type="text" class="form-control"  value="${gameRulesDto.ruleLimit }" />
				<span id="ruleLimit_error" class="validation_error">
			       <form:errors path="ruleLimit" class="error_class"/>	
			    </span>
			    </div> 
			</div>
			<br/>
		    
		    
		   <div class="form-group">
				<label  class="col-sm-3 control-label">Open Date:</label>
				<div class="col-sm-9">
				
				<fmt:formatDate value="${gameRulesDto.openDate}" var="startFormat" type="date" pattern="MM/dd/yyyy hh:mm "/>
				<form:input  path="openDate" id="openDate"  type="text" class="form-control"   value="${startFormat}"/>
				
				<%-- <span id="expiryDate_error" class="validation_error">
			       <form:errors path="expiryDate" class="error_class"/>	
			    </span>  --%>
			    </div>
			</div>
			<br/>
		    <div class="form-group">
				<label  class="col-sm-3 control-label">Expiry Date:</label>
				<div class="col-sm-9">
				
				<fmt:formatDate value="${gameRulesDto.expiryDate}" var="startFormat" type="date" pattern="MM/dd/yyyy hh:mm "/>
				<form:input  path="expiryDate" id="expiryDate"  type="text" class="form-control"   value="${startFormat}"/>
				
			    </div>
			</div> 
			<br/>
		  <div class="form-group">
				<label  class="col-sm-3 control-label">Start time:</label>
				<div class="col-sm-9">
			
				<fmt:formatDate value="${gameRulesDto.startTime}" var="startFormat" type="date" pattern="hh:mm a"/>
				<form:input  path="startTime" id="startTime"  type="text" class="form-control"   value="${startFormat}"/>
				
			    </div>
			</div> 
			<br/>
			  <div class="form-group">
				<label  class="col-sm-3 control-label">End time:</label>
				<div class="col-sm-9">
			
				<fmt:formatDate value="${gameRulesDto.endTime}" var="startFormat" type="date" pattern="hh:mm a"/>
				<form:input  path="endTime" id="endTime"  type="text" class="form-control"   value="${startFormat}"/>
				</div>
			</div> 
			 <div class="col-sm-offset-3 col-sm-9">
            	<span class="error_class" style="color:#0033BA;"> * Note: If you are not entering Start Time and End Time for Night conditions means, we will consider timings as 5PM to 9PM</span>
            </div>
            <br/>
            <br/>
			    <form:input type="hidden" path="gameRuleId" name="gameruleId" value="${gameRulesDto.gameRuleId }" />
			    <div class="col-sm-offset-3 col-sm-9">
			    <button type="submit" class="btn btn-dark" >Submit</button>
			   </div>
			</form:form>
			</div>
			</div>
			</div>
		
		
		<!--FOOTER-->
	    <%@ include file="/jsp/common/footer.jsp"%>
		</section>
	</section>
 

</body>
</html>