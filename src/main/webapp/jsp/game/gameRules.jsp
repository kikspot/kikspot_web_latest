
<!DOCTYPE html >
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Game rules</title>
	<%@ include file="/jsp/common/common.jsp"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<script type="text/javascript">
		$(document).ready(function() {
			
			var sortBy=$('#sortBy').val();
			var sortDirection=$('#sortDirection').val();
			if(sortDirection=='true'){
				jQuery("#status").removeClass('sorting').addClass('sorting_asc');
				
			}
			else{
				jQuery("#status").removeClass('sorting').addClass('sorting_desc');
			}
			/**
			Function for serach the Game Rules Of Location
			*/
			$('#location_filter').change(function (){
				var filter=$('#location_filter').val();
				if (filter.indexOf("&") > 0)
			       {
					filter = filter.replace("&", "%26");
			
			       }
				window.location="/kikspot/game/getgamerules.do?locationFilter="+filter;
			});
		});
		/**
			Created By Bhagya On december 11th,2015
			Function for deleting the game rule
		*/
		function deleteGameRule(id){
			if(confirm("Are you Sure to Delete Game Rule")==true){
				$.ajax({
				    url: "/kikspot/game/deletegamerule.do",
				    type: 'GET',
				    data:
				    	{
				    		gameRuleId:id
				       	},
				   	success: function(data) {
				   	 	if(data=="success"){
				   			alert("Game Rule Deleted Successfully ");
				   		
				   		}
				   		 else if(data=="userCredsAvailable"){
				   			alert("Not able to delete the game rule because user earned the creds by using this rule");
				   			
				   		}
				   		window.location="/kikspot/game/getgamerules.do";
				       
				    },
				    error:function(data,status,er) {
				        alert("Error While Deleting the Game Rule");
				    }
				});
			}
		}
		/**
			Craeted By Bhagya On December 11th,2015
			Function for changing the Game rule Status
		*/
		function changeStatus(sel,id){
			var status=sel.value;
			if(status=="active"){
				$.ajax({
				    url: "/kikspot/game/changegamerulestatus.do",
				    type: 'GET',
				    data:
				    	{
				    		gameRuleId:id,
				    		isActive:true
				    	},
				   	success: function(data) {
				   	
				    	if(data=="missmatch"){
				   			alert("Game Rule Status Not Updated to Active Based on Date Criteria");
				   			window.location.reload();
				   		}
				   		else{ 
				        	alert("Game Rule Status Successfully Updated to Active");
				        	window.location.reload();
				   		 } 
				    },
				    error:function(data,status,er) {
				        alert("Error While Updating Game Rule Status to Active");
				    }
				});
			}
			else if(status=="inactive"){
				$.ajax({
				    url: "/kikspot/game/changegamerulestatus.do",
				    type: 'GET',
				    data:
				    	{
				    		gameRuleId:id,
					    	isActive:false
				    	},
				   	success: function(data) {
				   		/* if(data=="missmatch"){
				   			alert("Game Rule Status Not Updated to InActive Based on Date Criteria");
				   			window.location.reload();
				   		}
				   		else{ */
				   		alert("Game Rule Status Successfully Updated to InActive");
				   		window.location.reload();
				   		/* } */
				    },
				    error:function(data,status,er) {
				    	alert("Error While Updating Game Rule Status to InActive");
				    }
				});
			}
			
			
		}
		function changeVisible(sel,id){
			var visible=sel.value
			if(visible=="yes"){
				
				$.ajax({
				    url: "/kikspot/game/changegamerulevisiblity.do",
				    type: 'GET',
				    data:
				    	{
				    		gameRuleId:id,
				    		visible:true
				    	},
				   	success: function(data) {
				        alert("Game Rule Successfully Updated to visible");
				    },
				    error:function(data,status,er) {
				        alert("Error While Updating Game Rule Status to visible");
				    }
				});
			}
			else if(visible=="no"){
				$.ajax({
				    url: "/kikspot/game/changegamerulevisiblity.do",
				    type: 'GET',
				    data:
				    	{
				    		gameRuleId:id,
					    	visible:false
				    	},
				   	success: function(data) {
				   		alert("Game Rule Successfully Updated to invisible");
				    },
				    error:function(data,status,er) {
				    	alert("Error While Updating Game Rule Status to Invisible");
				    }
				});
			}
			
			
		}
		
	</script>
 <style>
 	.filter{
 		float:right;
 		margin-top:2%;
 	}
 </style>
</head>

<body  class="l-dashboard  l-footer-sticky-1  ">
	<section class="l-main-container">

 		<%@ include file="/jsp/common/leftSideNavigation.jsp"%>

        
		<section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	       
	        <!-- Middle Content Start -->
	       <div>
	         <div class="l-spaced">
        
	       		<div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
	       			<form:form commandName="displayListBean"
							method="POST"
							class="form-horizontal" 
							action="${pageContext.request.contextPath}/game/getgamerules.do"
							role="form">
		            <form:hidden path="sortBy"  value="${displayListBean.sortBy}" id="sortBy"/>
				    <form:hidden path="searchBy"/>
					<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection"/>
					<form:hidden path="pagerDto.range" />
				    <form:hidden path="pagerDto.pageNo"/>
				   
				    <c:set var="range" value="${displayListBean.pagerDto.range}"/>
				    
	               <div class="l-row l-spaced-bottom">
	                  <div class="l-box">
	                    <div class="l-box-header">
	                      <h2 class="l-box-title"><span>Game Rules</span></h2>
	                      <ul class="l-box-options">
	                        <li><a href="#"><i class="fa fa-cogs"></i></a></li>
	                        <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"><i class="fa fa-expand"></i></a></li>
	                        <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
	                        <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
	                        <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"><i class="fa fa-times"></i></a></li>
	                      </ul>
	                    </div>
	                    <div class="l-box-body">
	                      <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive">
		                      <div class="dataTables_length" id="dataTableId_length">
		                      <label>Show 
			                      <select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
			                      <option value="10" ${range=='10' ? 'selected' : '' }>10</option>
			                      <option value="25" ${range=='25' ? 'selected' : '' }>25</option>
			                      <option value="50" ${range=='50' ? 'selected' : '' }>50</option>
			                      <option value="100" ${range=='100' ? 'selected' : '' }>100</option>
			                      </select> entries
		                      </label>
		                      </div>
		                      
		                      <!-- Search Filter -->
		                      <div class="filter">
		                      <div class="col-md-12 " >
		                       			<div class="col-md-2"></div>
								   		<div class="col-md-4"> 
								  			<label class="control-label searchLbl">Search Game Rules of Location :</label>
								  		</div>
								  		<div class="col-md-6">
											<select id="location_filter" class="form-control" name="locationFilter">
												<option value="" selected="selected">--Select Location--</option>
													<c:forEach var="location" items="${locations}">
														<option value="${location}" ${locationFilter eq location ? 'selected' : '' } >${location}</option>
													</c:forEach>
											</select>
										</div>
								  </div>
								   <div class="addButton">                     		
		        				  		<a href="${pageContext.request.contextPath}/game/addgamerule.do" ><button type="button" class="btn btn-dark">Add Rule</button></a>
		        		       	   </div>
								</div>
		                      <!-- End Of Search Filter -->
		                        
		                      <!-- SEARCH DIVISION -->
	                      	 
	                         <div class="form-group searchDiv" >
							  	<div class="col-sm-2" > </div>
							  	<div class="col-sm-2"> 
							  		<label class="control-label searchLbl">Search Game Rule:</label>
							  	</div>
							  	<div class="col-sm-4 controls">
							  		<input id="searchText" name="searchText" type="text" value="${displayListBean.searchBy}" class="searchTxt">
							  		<span class="help-inline">Search for game rule</span>
							  	</div>
							  <div class="col-sm-3" > </div>
							  </div>
						       			
							  <div class="form-group form-actions">							   
							  	<div class="col-sm-11" style="text-align: center;">
							  		<button  class="btn btn-danger" type="button" value="Search"  onclick="searchResults();">Search</button>
							  		<button class="btn btn-warning" type="button" onclick="window.location='${pageContext.request.contextPath}/game/getgamerules.do'">Reset</button>
							  	</div>
							  </div> 
							  
							
	                      	 <!-- END OF SEARCH DIVISION -->
	                      	  <c:if test="${not empty message}">
		      					<div class="status">${message}</div><br><br> 
		      				  </c:if>
			      			  <c:if test="${not empty status}">
		                    	<div class="status"> ${status }</div>
		                      </c:if>
	                       
						    <table id="dataTableId" cellspacing="0"  width="100%" class="display dataTable table" role="grid" aria-describedby="dataTableId_info" style="width: 100%;">
	                        <thead>
	                          <tr role="row">
	                              <th>Visible</th>
		                          <th>Game Rule</th>
		                          <th>Creds</th>
		                          <th>Rule Limit</th>
		                          <th>Start Time</th>
		                          <th>End Time</th>
		                          <th>Open Date</th>
		                          <th>Expiry Date</th>
		                          <th>Location</th>
		                          <th>Condition</th>
		                          <th class="sorting" id="status" onclick="sortResults('isActive');">Status</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
	                          </tr>
	                        </thead>
	                        <tbody>
	                            <c:forEach var="gameRuleDto" items="${gameRuleDtos }">
	                       		<tr role="row" >
	                       			<td>
	                       		    <select id="visible${gameRuleDto.gameRuleId}" name="visible" onchange="changeVisible(this,${gameRuleDto.gameRuleId});">
					                   		<option value="yes" ${gameRuleDto.visible==true ? 'selected' : '' }>YES</option>
				                      		<option value="no" ${gameRuleDto.visible==false ? 'selected' : '' }>NO</option>
				                        </select>
				                    </td>
	                           		<td>${gameRuleDto.rule }</td>
	                           		<td>${gameRuleDto.creds }</td>
	                           		<td>${gameRuleDto.ruleLimit }</td>
	                           		
	                           		 <td><fmt:formatDate value='${gameRuleDto.startTime}'  type='date' pattern='hh:mm a'/></td>
	                           		  <td><fmt:formatDate value='${gameRuleDto.endTime}'  type='date' pattern='hh:mm a'/></td>
	                           		 <td><fmt:formatDate value='${gameRuleDto.openDate}'  type='date' pattern='MM/dd/yyyy hh:mm aa'/></td>
	                            	<td><fmt:formatDate value='${gameRuleDto.expiryDate}'  type='date' pattern='MM/dd/yyyy hh:mm aa'/></td>
	                            	<%-- <td>${gameRuleDto.openDate}</td>
	                            	<td>${gameRuleDto.expiryDate}</td> --%>
	                            	<td>
	                            		<c:choose>
	                            			<c:when test="${not empty gameRuleDto.locationName}">
	                            				${gameRuleDto.locationName}
	                            			</c:when>
	                            			<c:otherwise>
	                            				<c:choose>
	                            					<c:when test="${not empty gameRuleDto.locationId}">
	                            						${gameRuleDto.locationId}
	                            					</c:when>
	                            					<c:otherwise>
	                            						GLOBAL
	                            					</c:otherwise>
	                            				</c:choose>
	                            				
	                            			</c:otherwise>
	                            		</c:choose>
	                            	</td>
	                            	<td>${gameRuleDto.gameRuleType}</td>
	                            	<td>
	                            		<select id="status${gameRuleDto.gameRuleId}" name="status" onchange="changeStatus(this,${gameRuleDto.gameRuleId});">
					                   		<option value="active" ${gameRuleDto.isActive==true ? 'selected' : '' }>ACTIVE</option>
				                      		<option value="inactive" ${gameRuleDto.isActive==false ? 'selected' : '' }>INACTIVE</option>
				                        </select>
	                            	</td>
	                           		<td><a href="${pageContext.request.contextPath}/game/editgamerule.do?gameRuleId=${gameRuleDto.gameRuleId}" title="Edit Game Rule"><i class="icon fa fa-edit"></i></a></td>
	                           		<td><a href="#" onclick="deleteGameRule(${gameRuleDto.gameRuleId})" title="Delete Game Rule"><i class="icon fa fa-remove"></i></a></td>
					      			
	                          	</tr>
							    </c:forEach>
							</tbody>
							<tfoot>
			                	<tr>
			                	 <th>Visible</th>
			                	  <th>Game Rule</th>
		                          <th>Creds</th>
		                          <th>Rule Limit</th>
		                           <th>Start Time</th>
		                          <th>End Time</th>
		                          <th>Open Date</th>
		                          <th>Expiry Date</th>
		                          <th>Location</th>
		                          <th>Condition</th>
		                          <th>Status</th>
		                          <th>Edit</th>
		                          <th>Delete</th>
			                	</tr>
			               </tfoot> 
	                      </table>
                      
	                      	<c:if test="${empty message}">
						 		<c:set var="first" value="0"/>
					     		<c:set var="end" value="${displayListBean.pagerDto.pagesNeeded }"/>
						 		<c:set var="page" value="${displayListBean.pagerDto.pageNo}"/>
						 		<c:set var="total" value="${displayListBean.pagerDto.totalItems}"/>
						 		<c:set var="firstResult" value="${displayListBean.pagerDto.firstResult}"/>
						 		<c:set var="lastResult" value="${displayListBean.pagerDto.lastResult}"/>
						 		<div class="dataTables_info" id="dataTableId_info" role="status" aria-live="polite">Showing ${firstResult} to ${lastResult} of ${total} entries</div>
							  	<div class="dataTables_paginate paging_simple_numbers" id="dataTableId_paginate">
							  		     	<%@ include file="/jsp/common/pager.jsp" %>
							  	</div>
					 		  </c:if>  
                      </div> <!-- data tables Wrapper -->
                     
                   </div><!-- l-box-body -->
                   
                  </div> <!-- l-box -->
                  
                </div> <!-- l-row-spaced -->
                
              </form:form> 
              
           </div><!-- resp-tab-content -->
            
        </div><!-- l-spaced -->
        
	</div><!-- Middle Content -->
			
	<!--FOOTER-->
	  <%@ include file="/jsp/common/footer.jsp"%>
	        
	   </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>