  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  

<html>  
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/user.js"></script>
<%@ include file="/jsp/common/common.jsp"%>
<title>ResetPassword</title>
</head>
<body class="login-bg">
	<form class="form-horizontal" autocomplete="off" action="${pageContext.request.contextPath}/resetpassword.do"  method='POST' onsubmit="return validateResetPasswordForm();">  
    	<div class="col-md-4"></div>
		<div class="col-md-5 content">
	    	<div class="logopanel">
				<h1 style="text-align:center;"><img src="image/logo.png"></h1>
			</div>
			<div class="content-title">Reset Password</div>	
			
			<input type="hidden" name="userId" value="${userId}">
			
			<div class="form-group">
					<label  class="col-sm-3 control-label">Password :</label>
					<div class="col-sm-9">
						<input name="password"  id="password"  type="text"  class="form-control" />
						<span id="password_error" class="error_class"></span> 
				    </div>
				</div>
				<br/>
				
				<div class="form-group">
					<label  class="col-sm-3 control-label">Confirm Password :</label>
					<div class="col-sm-9">
						<input name="confirmPassword"  id="confirmPassword"  type="text"  class="form-control" />
						<span id="confirmPassword_error" class="error_class"></span> 
				    </div>
				</div>
				<br/>
			<div class="col-sm-offset-3 col-sm-9" style="text-align:right">
            	<button type="submit" class="btn btn-dark">Reset Password</button>
			</div>      
			    
		 </div>
		<div class="col-md-3"></div>
					
 	</form>  
</body>
</html>