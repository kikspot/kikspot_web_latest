
<!DOCTYPE html >
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>

 	<%@ include file="/jsp/common/common.jsp" %>
	<script src="${pageContext.request.contextPath}/js/register.js" type="text/javascript"></script>
 	<title>Edit Profile</title>
	<style>
	.register_error {
			display: block;
			float: right;
			color:#f01;
		}
	</style>	
	<script src="js/jquery-ui-1.8.15.custom.min.js"></script>
	<link rel="stylesheet" href="css/jqueryCalendar.css">
	<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script>
		$(function() {
			$( "#dob" ).datepicker({ dateFormat:"mm/dd/yy"});   
			
		}); 

    </script>	
</head>

<body class="l-dashboard  l-footer-sticky-1  ">

	<section class="l-main-container">
     <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
   		<section class="l-container">
    			<!--HEADER-->
	    <%@ include file="/jsp/common/header.jsp"%>
	        
	       <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
	        
			<div class="l-box-header">
				<h2 class="l-box-title">Edit Profile</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			 <form:form   autocomplete="off" action="${pageContext.request.contextPath}/register/editprofile.do" method="POST" commandName="kikspotUserDto"  class="form-horizontal" onsubmit="return validateform();">

      			 
	 
	 
	    	
	    	     <div class="form-group"> 
	         	   <label  class="col-sm-3 control-label">First name :</label>
				   <div class="col-sm-9">
	        	   <form:input type="text" class="form-control"  path="firstName" id="firstName" value="${kikspotUserDto.firstName}"/>
	            
	               <span id="fname_error" class="register_error">
			       <form:errors path="firstName" class="register_error"/>	
			      </span> 
			      </div>
	   	         </div>
	   	     
	   	        
	            <div  class="form-group"> 
	            
	            <label  class="col-sm-3 control-label">Last name :</label>
				   <div class="col-sm-9">
	            
	               <form:input type="text" class="form-control" path="lastName" id="lastName" value="${kikspotUserDto.lastName}"/> 
	            
	               <span id="lname_error" class="register_error">
			       <form:errors path="lastName" class="register_error"/>	
			       </span>
			       </div>
	            </div> 
	        
	           
		      
	     	 <div class="form-group"> 
	         	<label  class="col-sm-3 control-label">Address1 :</label>
				   <div class="col-sm-9">
	        	  <form:input type="text" class="form-control" path="address1" id="address1"  value="${kikspotUserDto.address1}" /> 
	            
	            <span id="address1_error" class="register_error">
			      <form:errors path="address1" class="register_error"/>	
			    </span>
			    </div>
	         </div> 
	         
	        
			     
	         <div  class="form-group"> 
	            <label  class="col-sm-3 control-label">Address2 :</label>
				   <div class="col-sm-9">
	              <form:input type="text" class="form-control" path="address2" id="address2"  value="${kikspotUserDto.address2}"  /> 
	           
	            <span id="address2_error" class="register_error">
			      <form:errors path="address2" class="register_error"/>	
			    </span>
			    </div>
			  </div> 
			  
			   <div class="form-group"> 
	         	   <label  class="col-sm-3 control-label">Date Of birth :</label>
				   <div class="col-sm-9">
	        	   <form:input type="text" class="form-control"  path="dob" id="dob" value="${startFormat}"/>
	        	   <fmt:formatDate value="${kikspotUserDto.dob}" var="startFormat" type="date" pattern="MM/dd/yyyy"/>
	            
	               <span id="date_error" class="register_error">
			       <form:errors path="dob" class="register_error"/>	
			      </span> 
			      </div>
			  </div>
			  
			  <div class="form-group">
			        <label  class="col-sm-3 control-label">City :</label>
				   <div class="col-sm-9">
					<form:input class="form-control" type="text" path="city" id="city" value="${kikspotUserDto.city}" />
					
					<span id="city_error" class="register_error">
					 <form:errors path="city" class="register_error"/>	
				    </span>
				    </div>
					
			</div>
			
			<div class="form-group">
			        <label  class="col-sm-3 control-label">State :</label>
				   <div class="col-sm-9">
					<form:input class="form-control"  type="text" path="state" id="state" value="${kikspotUserDto.state}"/>
					
					<span id="state_error" class="register_error">
					 <form:errors path="state" class="register_error"/>	
				    </span>
				    </div>
					
			</div>
			
	          <div  class="form-group"> 
	              <label  class="col-sm-3 control-label">Zip Code :</label>
				   	<div class="col-sm-9">
	             	 	<form:input type="text" class="form-control" path="zip" id="zip" value="${kikspotUserDto.zip}"/> 
	            
	           			<span id="zip_error" class="register_error">
			      			<form:errors path="zip" class="register_error"/>	
			   			</span> 
			    	</div>
		      </div> 
	    
	    	 <form:input type="hidden" path="userId" name="userId" value="${ kikspotUserDto.userId}" />
	     
	      	 <div class="col-sm-offset-3 col-sm-9">
			      <button type="submit" class="register-btn btn btn-dark">Update Profile</button>
			 </div>
		 
		
		
			 </form:form>
         </div>
      </div>
     </div>
    </section>
   </section>
 
    
</body>
</html>