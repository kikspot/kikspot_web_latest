<!-- Created By bhagya On december 22nd,2015 
	Page For Error-->
<!DOCTYPE html>

<html>
<head>

	<title>kikspot-Error</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>
<body class="l-dashboard  l-footer-sticky-1">

    <section class="l-main-container">
   
	
		<section class="l-container">
	      
	        <div class="l-spaced">
	        
	        <div class="l-box l-spaced-bottom"> 
	        <!-- Middle Content Start -->
			<div class="l-box-header">
				<h2 class="l-box-title">Error</h2>
			</div>
			
			
			<div class="l-box-body l-spaced">
			<form class="form-horizontal" autocomplete="off" action="" method="POST" >
          		
          		<div class="logopanel">
					<h4 class="logo_image_header"><a href="#"><img src="${pageContext.request.contextPath}/image/KiKSPOT_cmyk.png" width=450px;></a> </h4> 
				</div>
				
            	<div class="error_status">
			          ${message}
			    </div>
        		
			</form>
			</div>
			</div>
			</div>
				
   			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
        </section>
    </section>

   </body>
</html>