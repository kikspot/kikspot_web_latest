/**
 * Created By Bhagya On December 07th,2015
 *  Common Java Script Functions
 */

/**
 * this function for ,based on selected range changing the data
 */
$(document).ready(function() {
	$('#selectrange')
	.change(
			function() {
				var selected = $(this).find('option:selected');
				var range = selected.val();
				jQuery("input[name='pagerDto.pageNo']").val(0);
				jQuery("input[name='pagerDto.range']").val(range).closest('form').submit();
				
	});
 });


function sortResults(element){
	if(jQuery("input[name='sortBy']").val()==element){
	if((jQuery("input[name='sortDirection']").val())=="true"){
	jQuery("input[name='sortDirection']").val(false);
	}
	else{
	jQuery("input[name='sortDirection']").val(true);
	}
	}
	else{
	jQuery("input[name='sortDirection']").val(true);
	}
	jQuery("input[name='pagerDto.pageNo']").val(0);
	jQuery("input[name='sortBy']").val(element).closest('form').submit();
	}


function searchResults(){
	var searchText=jQuery('#searchText').val();
	jQuery("input[name='pagerDto.pageNo']").val(0);
	jQuery("input[name='searchBy']").val(searchText).closest('form').submit();
}



