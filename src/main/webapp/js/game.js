/**
 *  game.js file
 *  
 *  consists of Validations of Game Rules And Game Levels
 */
	
	
	
	
	function validateGameRules(){
		
	var numberFilter=/[^0-9]+/g;
	var numberFilter1=/[^0-9.]+/g;
	
	var c=true;
	var rl=true;
	var r=true;
	/*var edate=true;*/
	var ruleType=true;
	var creds=$('#creds').val();
	var ruleLimit=$('#ruleLimit').val();
	var rule=$('#rule').val();
	/*var expiryDate=$('#expiryDate').val();*/
	var gameRuleType=$('#gameRuleType').val();
	
	 if(numberFilter.test(creds)) {
		 $('#cred_error').text("*Creds Should Contain Only Digits");
		 c=false;
	 }
	 else if(null== creds|| creds.trim().length<1) {
		 c=false;
		 $('#cred_error').text('*Creds Cannot be Empty');
	 }
	 else {
		c=true;
		$('#cred_error').text('');
	 }
	 
	 if(null!=ruleLimit && ruleLimit.length>1){
		 if(numberFilter1.test(ruleLimit)) {
			$('#ruleLimit_error').text("*Rule Limit Should Contain Only Digits");
			rl=false;
		 }
		 else {
			rl=true;
			$('#ruleLimit_error').text('');
		 }
	 }
	 
	if(null== rule|| rule.trim().length<1) {
		 r=false;
		 $('#rule_error').text('*Rule Cannot be Empty');
		 
	}
	else if(rule.trim().length>2500) {
		 r=false;
		 $('#rule_error').text('*Rule Should not exceed 2500 characters');
	}
	else {
		r=true;
		$('#rule_error').text('');
	 }
	 
	if(null==gameRuleType|| gameRuleType.trim().length<1) {
		ruleType=false;
		 $('#gameruleType_error').text('*Condition Cannot be Empty');
	}
	else {
		ruleType=true;
		$('#gameruleType_error').text('');
	 }
	/* if(null==expiryDate || expiryDate.trim().length<1) {
		edate=false;
		$('#expiryDate_error').text('*Expiry Date Cannot be Empty');
	}
	 else {
		 edate=true;
		 $('#expiryDate_error').text('');
	 }
		 */
	if(c==true && r==true && rl==true  && ruleType==true)  {
		  return true;
	}
	 
	 else{
		 return false;
	 }
	
}
	/**
	 * Added By Bhagya On December 14th,2015
	 * Function for Validationg the Game Level
	 */
	function validateGameLevel(){
		var minCreds=$('#minCreds').val();
		var level=$('#level').val();
		var a,b=false;
		var numberFilter=/[^0-9.]+/g;
	
		if(null==minCreds || minCreds.trim().length<1){
			a=false;
			$('#minCreds_error').text('*Minimum Creds Cannot be Empty');
		}
		else if(numberFilter.test(minCreds)){
			a=false;
			$('#minCreds_error').text('*Should Enter Only Digits')
		}
		else{
			a=true;
			$('#minCreds_error').text('');
		}
		
		if(null==level ||level.trim().length<1){
			b=false;
			$('#level_error').text('*Level Cannot be Empty');
		}
		else{
			b=true;
			$('#level_error').text('');
		}
		
		if(a==true && b==true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function LocationIdInput(){
		var locationId=$('#locationId').val();
		if(locationId.length>0){
		$.ajax({
			url : '/kikspot/game/checkrecommendationlocation.do',
			type: 'GET',
			data:{locationId:locationId},
			async: true,
			success:function(data){
				
				if(data==="error"){
					
					$('#locationId_error').text('*No Location exists with this LocationId');
					$('#locationName').val('');
				}
				else if(data!="success"){
				
					$('#locationId_error').text('');
					$('#locationName').val(data);
				}
			}			
		});
		}
		else{
			$('#locationName').val('');
		}
	}
	
	
	
	
	
	$(function () {
	    $('#rule').keyup(function (e) {
	        var max = 2500;
	        var len = $(this).val().length;
	        var char = max - len;
	        $('#text-counter').html(char);
	    });
	});