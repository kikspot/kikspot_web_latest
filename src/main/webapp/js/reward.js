/**
 * added by Firdous on 20-01-2016
 * function to validate rewards
 * 
 */

function validateRewards(){
	
	var numberFilter=/[^0-9.]+/g;
	
	var r1=true;
	var r2=true;
	var c=true;
	var l=true;
	var d1=true;
	var d2=true;
	
	var rewardType=$('#rewardType').val();
	var reward=$('#reward').val();
	var creds=$('#creds').val();
	var level=$('#level').val();
	var startDate=$('#startDate').val();
	var endDate=$('#endDate').val();
	
	
	 if(null== rewardType || rewardType.trim().length<1) {
		 r1=false;
		 $('#rewardType_error').text('*Select a reward Type');
	 }
	 else {
		 r1=true;
		 $('#rewardType_error').text('');
	 }
	 
	 if(null== reward || reward.trim().length<1) {
		 r2=false;
		 $('#reward_error').text('*Select a reward');
	 }
	 else {
		 r2=true;
		 $('#reward_error').text('');
	 }
	 
	 if(null== creds || creds.trim().length<1) {
		 c=false;
		 $('#creds_error').text('*Add cred points');
	 }
	 else if(numberFilter.test(creds)){
		 c=false;
		 $('#creds_error').text('*Cred should always be numbers');
	 }
	 else {
		 c=true;
		 $('#cred_error').text('');
	 }
	 
	 if(null==level || level.trim().length<1) {
		 l=false;
		 $('#level_error').text('*Select the level');
	 }
	 else {
		 l=true;
		 $('#level_error').text('');
	 }
	 
	 if(null==startDate||startDate.trim().length<1) {
		 d1=false;
		 $('#startDate_error').text('*Start date cannot be empty');
	 }
	 else {
		 d1=true;
		 $('#startDate_error').text('');
	 }
	 
	 if(null==endDate||endDate.trim().length<1) {
		 d2=false;
		 $('#endDate_error').text('*End date cannot be empty');
	 }
	 else {
		 d2=true;
		 $('#endDate_error').text('');
	 }
	 
	 
	 if(r1==true && r2==true && c==true && l==true && d1==true && d2==true){
		 return true;
	 }
	 else {
		 return false;
	 }
	 
	 
}