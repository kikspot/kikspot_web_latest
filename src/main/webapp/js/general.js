
function runBtnBlur(morrisChart){

    $(".btn").mouseup(function(e){
        e.preventDefault();
        $(this).blur();
    });

}



function setScrollTop(){
    $.scrollUp({
        animation: 'fade',
        activeOverlay: '#00FFFF',
        scrollText: '<i class="fa fa-chevron-up"></i>'
    });
}




$(function(){

	  
        runBtnBlur();
        setScrollTop();
});

