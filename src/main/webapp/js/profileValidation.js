/**
 * added by Firdous on 5th feb 2016
 */


function  profileValidation()
{
	
	var nameFilter2=/[^a-zA-Z]+$/;
	var nameFilter3=/[^a-zA-Z]+$/;
	var numberFilter=/[^0-9]+$/;
	var emailFilter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	var uname=true;
	var fName=true;
    var lName=true;
	var email=true;
	var s=true;
	
	
	var userName=$('#form_userName').val();
	var firstName=$('#form_firstName').val();
	var emailId=$('#form_email').val();
	var lastName=$('#form_lastName').val();
	var zipCode=$('#form_zip').val();
	
	
	if(null==userName || userName.trim().length<1) {
		uname=false;
		$('#uname_error').text("*Username Cannot be Empty");
	}
	
	else {
		uname=true;
		$('#uname_error').text('');
	}
	
	
  
    if(nameFilter2.test(firstName))
	{   
		fName=false;
		$('#fname_error').text("*First name Should conatin only alphabets");
	}
    else {
		fName=true;
		$('#fname_error').text('');
	}
    
   
    if(nameFilter3.test(lastName)){
		lName=false;
		$('#lname_error').text("*Last name Should conatin only alphabets");
	}
	else {
		lName=true;
		$('#lname_error').text('');
	}
	
    if(null== emailId || emailId.trim().length<1) {
		$('#email_error').text("* Email cannot be empty");
	    email=false;
	}
    else if(!emailFilter.test(emailId)){
		email=false;
		$('#email_error').text("*Email pattern does not match");
	}
	else {
		email=true;
		$('#email_error').text('');
	}
	
  
	 if(numberFilter.test(zipCode)) {
		$('#zip_error').text("* Zip code should  have  digits");
		zip=false;
	 }
	 else {
		zip=true;
		$('#zip_error').text('');
	 }
	 
	
	
	 if(uname==true && fName==true &&  lName==true && zip==true && email==true) {
		return true;
	 }
	 
	 else {
		return false;
	 }	
	
}

function passwordValidation(){
	
	var password=$('#newPassword').val();
	var cpassword=$('#confirmNewPassword').val();
	
	var pwd=true; 
	var cpwd=true;
	
	if(password.trim().length<1){
		
		$('#password_error').html('* Please Enter Password');
		pwd=false;
	}
	else if(password.trim().length<8 || password.trim().lenght>20){
		$('#password_error').html('* Password Should have atleast 8 characters and maximum 20 characters');
		pwd=false;
	}
	else{
		$('#password_error').html('');
		pwd=true;
	}
	if(cpassword.trim().length<1){
		
		$('#confirmPassword_error').html('* Please Enter Confirm Password');
		pwd=false;
	}
	else if(password!=cpassword){
		$('#confirmPassword_error').html('* Password and Confirm Password Should Match');
		cpwd=false;
	}
	else{			
		$('#confirmPassword_error').html('');			
		cpwd=true;
	}		
	if(pwd && cpwd){
		return true;
	}
	else{
		return false;
	}		
	
}
