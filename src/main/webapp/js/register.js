var nameFilter=/[^a-zA-Z]+$/;
var filter= /[^a-zA-Z_0-9]+/g;
var numberFilter=/[^0-9]+$/;

function validateform()
{
	
	
	var fName=true;
    var lName=true;
	
	var addrs1=true;
	var addrs2=true;
	var date=true;
	var zip=true;
	var c=true;
	var s=true;
	
	
	
	var firstName=$('#firstName').val();

	var lastName=$('#lastName').val();
	
	var addressLine1=$('#address1').val();
	var addressLine2=$('#address2').val();
	var dob=$('#dob').val();
	var zipCode=$('#zip').val();
	var city=$('#city').val();
	var state=$('#state').val();
	
	
	
	
	if(nameFilter.test(firstName)) {
		$('#fname_error').text("* First Name should have only alphabets");
	    fName=false;
	}
	else if(null== firstName || firstName.trim().length<1) {
		fName=false;
		$('#fname_error').text('* first name Cannot be Empty');
	}
	
	else {
		fName=true;
		$('#fname_error').text('');
	}
	
	
	if(null== lastName || lastName.trim().length<1) {
		lName=false;
		$('#lname_error').text('* last name Cannot be Empty');
	}
	else if(nameFilter.test(lastName)) {
		$('#lname_error').text("* last Name should  have only alphabets");
	    fName=false;
	}
	else {
		lName=true;
		$('#lname_error').text('');
	}
	
	
	
	if(null== addressLine1 || addressLine1.trim().length<1) {
		addrs1=false;
		$('#address1_error').text('* address Cannot be Empty');
	}	
	else {
		addrs1=true;
		$('#address1_error').text('');
	}
	
	if(null== addressLine2 || addressLine2.trim().length<1) {
		addrs2=false;
		$('#address2_error').text('* address Cannot be Empty');
	}	
	else {
		addrs2=true;
		$('#address2_error').text('');
	}
	
	if(null== dob || dob.trim().length<1) {
		date=false;
		$('#dob_error').text('* date of birth Cannot be Empty');
	}	
	else {
		date=true;
		$('#dob_error').text('');
	}
	 if(nameFilter.test(city)) {
			$('#city_error').text("city name should  have only alphabets");
			c=false;
	 }
	 else if(null== city || city.trim().length<1) {
			c=false;
			$('#city_error').text('* city name Cannot be Empty');
	 }	
	 else {
			c=true;
			$('#city_error').text('');
	 }
	 
	 if(nameFilter.test(state)) {
			$('#state_error').text("state name should  have only alphabets");
			s=false;
	 }
	 else if(null== state || state.trim().length<1) {
			s=false;
			$('#state_error').text('*state name Cannot be Empty');
	 }	
	 else {
			s=true;
			$('#state_error').text('');
	 }
	if(null== zipCode || zipCode.trim().length<1)  {
		zip=false;
		$('#zip_error').text('* zip cannot be Empty');
	}
	else if( numberFilter.test(zipCode)) {
		$('#zip_error').text("*Zip code should  have  digits");
		zip=false;
	}
	else {
		zip=true;
		$('#zip_error').text('');
		
	}
	
	
	
	if(fName==true &&  lName==true &&  addrs1==true && addrs2==true && c==true && s==true && zip==true) {
		return true;
	}
	
	else {
		return false;
	}	
	
}
