/*
 * Created By Bhagya On October 26th, 2015
 * Javascript functions for checking sample input and output for mobile services
 */


/***
 * Function For verify the facebook login user
 */
function socialLogin() {
	 $.ajax({
	    url: "/kikspot/mobile/sociallogin.do",
	    type: 'POST',
	    data: JSON.stringify({ emailId: "ftfirdous@gmail.com", firstName:"firru",lastName:"tabbu"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}



function signup() {
	 $.ajax({
	    url: "/kikspot/mobile/signup.do",
	    type: 'POST',
	    data: JSON.stringify({ emailId: "bhagya565.r@gmail.com", username:"bhagya_lakshmi" ,password:"123",referralCode:"",deviceToken:"8659280b95d456ca176570695b0a440e17d71f8f7065b05d28b2d504af49df60" }),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}


function completeSignup() {
	 $.ajax({
	    url: "/kikspot/mobile/completesignup.do",
	    type: 'POST',
	    data: JSON.stringify({
	    		userId:81,
	    		
	    		firstName:"Firdous",
	    		lastName:"Raavi",
	    		dateofBirth:"12/11/1992",
	    		address1:"19/25",
	    		address2: "Bannerghatta Road",
	    		city: "Bangalore",
	    		state: "Karnataka",
	    		zip : "560029",
	    		
	    	}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

function bypassUser(){
	 $.ajax({
		    url: "/kikspot/mobile/bypassuser.do",
		    type: 'GET',
		    
		    contentType: 'application/json',
		 	success: function(data) {
		        alert(data);
		    },
		    error:function(data,status,er) {
		        alert("error: "+data+" status: "+status+" er:"+er);
		    }
		});
}


function loginUser() {
	 $.ajax({
	    url: "/kikspot/mobile/login.do",
	    type: 'POST',
	    data: JSON.stringify(
	    		{ 
	    			username:"knsdeveloper",
	    			password:"welcome123",
	    			appVersion:"4.0"
	    		}
	    	),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

/**
 * Created By Bhagya On November 23rd,2015
 * Function for getting Basic details of near by locations based on lat and lng
 */
function getLocations() {
	 $.ajax({
	    url: "/kikspot/mobile/getrecommendationlocations.do",
	    type: 'POST',
	    data: JSON.stringify({"userId":79,"latitude":12.9164361241245,"longitude":77.619547754714,"pageToken":"",
	    	"sortBy":"trending","requestFilter":1}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}
/**
 * Created By Bhagya On November 25th, 2015
 * Function For get the location Dtails By Location APIID
 */
function getLocationDetailsByLocationAPIId() {
	 $.ajax({
	    url: "/kikspot//mobile/getrecommendationlocationdetails.do",
	    type: 'POST',
	    data: JSON.stringify({ locationAPIId:"ChIJK8BZqFsUrjsRKAzJRLhde5E","latitude":12.9164355645629,"longitude":77.6195991971077,"userId":3}),
	    contentType: 'application/json',
	 	success: function(data) {
	 		
	 		
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}



function userDashboard(){
	 $.ajax({
		    url: "/kikspot/mobile/dashboard.do",
		    type: 'POST',
		    data: JSON.stringify({
		    	userId:583,
		    	latitude:12.9164757845265,
		    	longitude:77.6195628970071
		    		}),
		    contentType: 'application/json',
		 	success: function(data) {
		        alert(data);
		    },
		    error:function(data,status,er) {
		        alert("error: "+data+" status: "+status+" er:"+er);
		    }
		});
}


/**
 * Created By Bhagya On November 26th,2015
 * Function For edit Profile
 */
function editProfile() {
	 $.ajax({
	    url: "/kikspot/mobile/editprofile.do",
	    type: 'POST',
	    data: JSON.stringify({
	    		userId:1,
	    		firstName:"jeevan",
	    		lastName:"mysore",
	    		dateofBirth:"12/12/1988",
	    		address1:"48/1",
	    		address2: "tavarekere",
	    		city: "Bangalore",
	    		state: "Karnataka",
	    		zip : "560029"
	    	}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

/**
 * 
 *  Created by Firdous on 26th November 2015
 *  Functions to handle feedback
 *  
 */

function getfeedback() {
	$.ajax({
		type : 'POST',
		url : "/kikspot/mobile/feedback.do",
		data : JSON.stringify({
		
			feedbackMessage:"I like this applcation ~@!#$%^&*()_+:<>??/=-][\|*.,",
			userId:1
		}),
		contentType : 'application/json',
		success : function(data) {
			alert(data);
		}
		
	});
}


function showfeedback() {
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/showfeedback.do",
		data : JSON.stringify({
	    feedbackId:10
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}

function showfTotalfeedback(){

	$.ajax({
		type : "POST",
		url :  "/kikspot/mobile/totalfeedbacks.do",
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}

	   
	});
}
		
function deleteFeedback() {
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/deletefeedback.do",
		data : JSON.stringify({
			feedbackId:161
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}	


function removeLocation(){
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/removerecommendation.do",
		data : JSON.stringify({
			userId:1,
			locationAPIId:"ChIJIfBAsjeuEmsRdgu9Pl1Ps48"
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}




function rateLocation(){
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/ratelocation.do",
		data : JSON.stringify({
			userId:21,
			locationAPIId:"ChIJd2p1hBwUrjsRXgtLTeAKfJc",
			comment: '',
			rating : '8',
			latitude:'12.9164361241245',
			longitude:'77.619547754714'
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}

/**
 * Created By Bhagya On Jan 05th,2016
 * Function for Get location RAting
 */

function getLocationRating(){
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/getuserlocationrating.do",
		data : JSON.stringify({
			userId:1,
			locationAPIId:"ChIJIfBAsjeuEmsRdgu9Pl1Ps48"
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}

/**
 * Created By Bhagya On Jan 07th,2016
 * Function for Getting the Location Comments
 */

function getLocationComments(){
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/getlocationcomments.do",
		data : JSON.stringify({
				userId:1,
				locationAPIId:"ChIJIfBAsjeuEmsRdgu9Pl1Ps48"
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}

function saveUserLocation(){
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/saveuserlocation.do",
		data : JSON.stringify({
			userId:48,
			latitude:14.32,
			longitude:19.32,
			city:"Bengaluru South",
			country:"india"
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
	
}
	

	function forgotPassword(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/forgotpassword.do",
			data : JSON.stringify({
				email:"jeevan@knstek.com"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}

	
	

	function resetPassword(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/resetpassword.do",
			data : JSON.stringify({
				email:"jeevan@knstek.com",
				token:"8547",
				password:"123"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	/**
	 * Created By Bhagya On December 17th,2015
	 * Function for change Password
	 */
	
	function changePassword(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/changepassword.do",
			data : JSON.stringify({
				userId:35,
				oldPassword:"123",
				newPassword:"welcome123"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	
	
	
	function getNotificationConfigurations(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/usernotifications.do",
			data : JSON.stringify({
				userId:24
			
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	
	
	
	function changeNotificationConfiguration(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/notificationconfig.do",
			data : JSON.stringify({
				userId:14,
				notificationId:3,
				enable:"false"
			
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	/**
	 * Created By Bhagya On December 28th,2015
	 * Method for getting the profile details
	 */
	function profileDetails(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/profiledetails.do",
			data : JSON.stringify({
				userId:13
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	
	
	
	function shareApp(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/shareapp.do",
			data : JSON.stringify({
				userId:14
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * Function For Getting The Game Rules Of A Location
	 */
	
	function getGameRulesOfaLocation(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgamerulesoflocation.do",
			data : JSON.stringify({
					locationAPIId:"6",
					isActive:true,
					checkExpiryDate:true,
					sortBy:"",
					userId:1	
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * Function For Getting All Game Rules
	 */
	function getAllGameRules(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgamerules.do",
			data : JSON.stringify({
					isActive:true,
					checkExpiryDate:true,
					sortBy:"time",
					userId:21,
					latitude:12.9164757845265,
					longitude:77.6195628970071
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	/**
	 * Created By Bhagya On Jan 06th,2016
	 * Function For Getting The GAme Rules For List Of Locations
	 */
	
	function getGameRulesOfLocationsList(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgamerulesoflocationslist.do",
			data : JSON.stringify({
					locationAPIId:["19"],
					isActive:true,
					checkExpiryDate:true,
					sortBy:"time",
					userId:5
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	/**
	 * Created By Bhagya On Jan 07th,2016
	 * Function for updating the User Game Creds
	 */
	
	function updateUserGameCredsByGameRule(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/updateusergamecreds.do",
			data : JSON.stringify({
					gameRuleId:39,
					userId:14,
					locationAPIId:"18"
					
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	/**
	 * 
	 * Created By bhagya On January 22nd, 2016
	 * 	Function For Getting the Kikspot Rewards Of a User
	 * 
	 */
	
	function getUserRewards(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getuserrewards.do",
			data : JSON.stringify({
					userId:60,
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
	   });
	}
	/**
	 * 
	 * Created By bhagya On January 22nd, 2016
	 * Function For Getting the Reward Details By KikspotReward For User
	 * 
	 */
	function getUserRewardDetails(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getrewarddetails.do",
			data : JSON.stringify({
					userId:60,
					rewardId:14
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
	   });
	}
	/**
	 * Created By bhagya On Feb 02nd, 2016
	 * Getting the User Preferences Of User
	 */
	

	function getUserPreferencesOfUser(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getuserpreferences.do",
			data : JSON.stringify({
					userId:60
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
	   });
	}
	/**
	 * Created By bhagya On feb 02nd, 2016
	function showNotificationOfUser() {
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/notifications.do",
			data : JSON.stringify({
		    userId:1
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}
	
	
	
	
	
	 * Function for saving the user preferences
	 */
	function saveUserPreferences(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/saveuserpreferences.do",
			data : JSON.stringify({
					userId:1,
					options:[1,5,18,25,36]
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
	   });
	}
		
	/**
		 * created by Firdous on feb 03,2016
		 * 
	 */
	function showNotificationOfUser() {
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/notifications.do",
			data : JSON.stringify({
					userId:21
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
		    }
				
		});
	}
	 
    /**
     * created by Firdous on 8th march
     * Get user game details like rank, creds, game level
     * 
     */
		
	function getGameDetails(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgamedetailsofuser.do",
			data : JSON.stringify({
					userId:5
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		 });
	}
	
	
	/**
	  * 
    * created by Firdous on 8th march
    * Get game details of all the users
    * 
    */	
		
	function getGameDetailsOfAllUsers(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgamedetailsofallusers.do",
			data : JSON.stringify({
					userId:21,
					time:"alltime",
					location:"country"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		});
	}
		
	 /**
	  * 
     * created by Firdous on 8th march
     * Get game details of friends of a user
     * 
     */	
		
	
	function getGameDetailsOfUsersFriends(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getfriendsgamedetails.do",
			data : JSON.stringify({
					userId:21,
					time:"alltime",
					location:"country"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		});
	}
	
	/**
	 * Added by Firdous on 17 march 2016
	 * Get game details
	 */
	function getGameDetailsWithGameRules(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgamedetails.do",
			data : JSON.stringify({
					userId:14
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		});
	}
	
	/**
	 * Added by Firdous on 17 march 2016
	 * Get game level details
	 */
	function getGameLevelDetails(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getgameleveldetails.do",
			data : JSON.stringify({
					userId:583
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		});
	}
	

	/**
	 * Added by Firdous on 17 march 2016
	 * Get game level details
	 */
	function addGameRule(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/addgamerule.do",
			data : JSON.stringify({
					userId:14,
					locationAPIId:"18",
					rule:"reach this location",
					condition:"geolocated"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		});
	}
	
	/*function gameCredsForSharingLocations(){
		$.ajax({type : "POST",
		url : "/kikspot/mobile/updatecredsforsharingrecommendationlocation.do",
		data : JSON.stringify({
				userId:14,
				
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
	});
	}*/
	function gameCredsForSharing(){
		$.ajax({type : "POST",
		url : "/kikspot/mobile/updatecredsforsharing.do",
		data : JSON.stringify({
				userId:21,
				type:"Share App"
				
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
	});
	}
	
	
	function gameCredsForSharingComments(){
		$.ajax({type : "POST",
		url : "/kikspot/mobile/updatecredsforsharingcomments.do",
		data : JSON.stringify({
				userId:14,
				
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
	});
	}
	
	
	
	function getUnreadNotificationsCount(){
		$.ajax({type : "POST",
			url : "/kikspot/mobile/getnumberofunreadnotifications.do",
			data : JSON.stringify({
					userId:567
					
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
		});
	}	
	
	
	function getLocationCommentsAndImages(){
		$.ajax({
			type : "POST",
			url : "/kikspot/mobile/getlocationcommentsandimages.do",
			data : JSON.stringify({
					userId:1,
					locationAPIId:"ChIJIfBAsjeuEmsRdgu9Pl1Ps48"
			}),
			contentType : 'application/json',
			success : function(data) {
			alert(data);
			}
			
		});
	}