/**
 * Created By Bhagya On December 18th,2015
 * Validtions for recommendation Locations
 */



/**
 * Created By Bhagya On December 18th,2015
 * @returns {Boolean}
 * 
 * Function For checking the Location Existence
 */

function validateLocationExistence(){
			var kikspotLocationId=$('#googleLocationId').val();
			var validationResult=false;
			$.ajax({
				url : '/kikspot/recommendations/checklocation.do',
				type: 'GET',
				data:{kikspotLocationId:kikspotLocationId},
				async: false,
				success:function(data){
					if(data==="success"){
						validationResult=true;
						$('#googleLocationId_error').text('');
					}
					else{
						validationResult=false;
						$('#googleLocationId_error').text('*Location Id Already Exists');
					}
				}			
			});
			
			return validationResult;
		}


/**
 * Created By Bhagya On december 18th,2015
 * Validations For Recommendation Locations
 */


function validateRecommendationLocation(){
	var locationId=$('#googleLocationId').val();
	var locationName=$('#locationName').val();
	var latitude=$('#latitude').val();
	var longitude=$('#longitude').val();
	var ratings=$('#ratings').val();
	var address=$('#address').val();
	var phoneNo=$('#phoneNo').val();
	var distance=$('#distance').val();
	var icon=$('#icon').val();
	var image=$('#image').val();
	var closeDate=$('#closeDate').val();
	var url=$('#url').val();
	var iconExtension = icon.split('.').pop().toUpperCase();
	var imageExtension='';
	if(image.trim().length>0){
		imageExtension=image.split('.').pop().toUpperCase();
	}
	
	var filter=/[^0-9.-]+/g;
	var filter1=/[^0-9.-]+/g;
	var filter2=/[^0-9.]+/g;
	var distanceFilter=/[^0-9.]+/g;
	
	var a,b,c,d,e,f,g,h,i,j,k,l,m=false;
	var validationResult=false;
	a=true;
	var createdBy=$('#createdBy').val();
	
	
	/*if(null==locationId || locationId.trim().length<1){
		a=false;
		$('#kikspotLocationId_error').text(" *Location Id Cannot Be Empty");
	}
	else{
		a=true;
		$('#kikspotLocationId_error').text("");
	}*/
	if(null==locationName || locationName.trim().length<1){
		b=false;
		$('#locationName_error').text(" *Location Name Cannot Be Empty");
	}
	else{
		b=true;
		$('#locationName_error').text("");
	}
	if(null==latitude || latitude.trim().length<1){
		c=false;
		$('#latitude_error').text(" *Latitude Cannot Be Empty");
	}
	else if(filter.test(latitude)){
		c=false;
		$('#latitude_error').text(" *Latitude Entered is Not Valid,Please Enter Numeric Values");
	}
	else{
		c=true;
		$('#latitude_error').text("");
	}
	if(null==longitude || longitude.trim().length<1){
		d=false;
		$('#longitude_error').text(" *Longitude Cannot Be Empty");
	}
	else if(filter1.test(longitude)){
		d=false;
		$('#longitude_error').text(" *Longitude Entered is Not Valid,Please Numeric Values");
	}
	else{
		d=true;
		$('#longitude_error').text("");
	}
	
	 if(null!=ratings && filter2.test(ratings)){
		e=false;
		$('#ratings_error').text(" *Rating Entered is Not Valid,Please Enter Numeric Values");
	}
	 else if(ratings>5 ){
		 e=false;
		 $('#ratings_error').text(" *Rating Should Be Between 1 to 5");
	 }
	else{
		e=true;
		$('#ratings_error').text("");
	}
	 
	 
	
		f=true;
		
	
		g=true;
	
	/*if(null==distance || distance.trim().length<1){
		h=false;
		$('#distance_error').text(" *Distance Cannot Be Empty");
	}
	else if(filter1.test(longitude)){
		h=false;
		$('#distance_error').text(" *Distance Entered is Not Valid,Please Enter Numeric Values ");
	}
	else{
		h=true;
		$('#distance_error').text("");
	}*/
	h=true;
	i=true;
	 if (null!=icon && icon.trim().length>0 && iconExtension!="PNG" && iconExtension!="JPG" && iconExtension!="GIF" && iconExtension!="JPEG"){
		$('#icon_error').html('*Only .png, .jpg, .gif, .jpeg are allowed');
		i=false;
    }

	else{
		i=true;
		$('#icon_error').text("");
	}
	/*if(null==image || image.trim().length<1){
		j=false;
		$('#image_error').text(" *Image Should Be Upload");
	}
	else*/
	if(null!=image && image.trim().length>0){
		if ( imageExtension!="PNG" && imageExtension!="JPG" && imageExtension!="GIF" && imageExtension!="JPEG"){
			$('#image_error').html('*Only .png, .jpg, .gif, .jpeg are allowed');
			j=false;
	    }
		else{
			j=true;
			$('#image_error').text("");
		}
	}
	else{
		j=true;
	}
	
	l=true;
	
	k=true;
	if(null==createdBy || createdBy.trim().length<1){
		m=false;
		$('#createdBy_error').text(" *Created By Cannot Be Empty");
	}
	else{
		m=true;
		$('#createdBy_error').text("");
	}
	if(a==true && b==true && c==true && d==true && f==true && g==true && h==true && i==true && j==true && k==true && l==true && e==true&& m==true){
		validationResult=validateLocationExistence();
		if(validationResult==true){
			  return true;
		}
		else{
			return false;
		}
		return true;
	}
	else{
		return false;
	}
	
}
/**
 * Created By Bhagya On december 18th,2015
 * @returns {Boolean}
 * 
 * Function for validating the recommendation Locations
 */
function validateEditRecommendationLocation(){

	var locationId=$('#googleLocationId').val();
	var locationName=$('#locationName').val();
	var latitude=$('#latitude').val();
	var longitude=$('#longitude').val();
	
	/*var ratings=$('#ratings').val();*/
	var address=$('#address').val();
	var phoneNo=$('#phoneNo').val();
	
	/*var closeDate=$('#closeDate').val();*/
//	var distance=$('#distance').val();
	var icon=$('#icon').val();
	
	var url=$('#url').val();
	var iconExtension="";
	if(null!=icon && icon.trim().length>0){
	 iconExtension = icon.split('.').pop().toUpperCase();
	}
	
	var filter=/[^0-9.-]+/g;
	var filter1=/[^0-9.-]+/g;
	var filter2=/[^0-9.]+/g;
	var distanceFilter=/[^0-9.]+/g;
	
	var a,b,c,d,e,f,g,h,i,j,k,l,m=false;
	
	var validationResult=false;
	var modifiedBy=$('#modifiedBy').val();

	/*if(null==locationId || locationId.trim().length<1){
		a=false;
		$('#kikspotLocationId_error').text(" *Location Id Cannot Be Empty");
	}
	else{
		a=true;
		$('#kikspotLocationId_error').text("");
	}*/
	a=true;
	if(null==locationName || locationName.trim().length<1){
		b=false;
		$('#locationName_error').text(" *Location Name Cannot Be Empty");
	}
	else{
		b=true;
		$('#locationName_error').text("");
	}
	if(null==latitude || latitude.trim().length<1){
		c=false;
		$('#latitude_error').text(" *Latitude Cannot Be Empty");
	}
	else if(filter.test(latitude)){
		c=false;
		$('#latitude_error').text(" *Latitude Entered is Not Valid,Please Enter Numeric Values");
	}
	else{
		c=true;
		$('#latitude_error').text("");
	}
	if(null==longitude || longitude.trim().length<1){
		d=false;
		$('#longitude_error').text(" *Longitude Cannot Be Empty");
	}
	else if(filter1.test(longitude)){
		d=false;
		$('#longitude_error').text(" *Longitude Entered is Not Valid,Please Numeric Values");
	}
	else{
		d=true;
		$('#longitude_error').text("");
	}
	
	/* if(ratings.trim().length<1|| filter2.test(ratings)){
			e=false;
			$('#ratings_error').text(" *Rating Entered is Not Valid,Please Enter Numeric Values");
		}
		else{
			e=true;
			$('#ratings_error').text("");
	}*/
	
	
		f=true;
	
	
		g=true;
	
	/*if(null==closeDate || closeDate.trim().length<1){
		l=false;
		$('#closeDate_error').text(" *Please enter the close date");
	}
	else{
		l=true;
		$('#closeDate_error').text("");
	}*/
	/*if(null==distance || distance.trim().length<1){
		h=false;
		$('#distance_error').text(" *Distance Cannot Be Empty");
	}
	else if(filter1.test(longitude)){
		h=false;
		$('#distance_error').text(" *Distance Entered is Not Valid,Please Enter Numeric Values ");
	}
	else{
		h=true;
		$('#distance_error').text("");
	}
*/
	h=true;
	if(null!=icon && icon.trim().length>0){
		 if (iconExtension!="PNG" && iconExtension!="JPG" && iconExtension!="GIF" && iconExtension!="JPEG"){
			$('#icon_error').html('*Only .png, .jpg, .gif, .jpeg are allowed');
			i=false;
	    }
		else{
			i=true;
			$('#icon_error').text("");
		}
	}
	else{
		i=true;
		$('#icon_error').text("");
	}
	
	/*if(null==url || url.trim().length<1){
		k=false;
		$('#url_error').text(" *Website URL Cannot Be Empty");
	}
	else{
		k=true;
		$('#url_error').text("");
	}*/
	k=true;
	if(null==modifiedBy || modifiedBy.trim().length<1){
		
		m=false;
		$('#modifiedBy_error').text(" *Modified By Cannot Be Empty");
	}
	else{
		
		m=true;
		$('#modifiedBy_error').text("");
	}
	
	if(a==true && b==true && c==true && d==true && f==true && g==true && h==true && i==true && k==true && m==true){
		
		return true;
	}
	else{
		
		return false;
	}
	
}


	/**
	 * Created By Bhagya On December 23rd,2015
	 * 
	 * Validation function for edit recommendation location Images
	 */


	function validateEditRecommendationLocationImages(){
		var image=$('#image').val();
		var imageExtension = image.split('.').pop().toUpperCase();
		var a=false;
		
		if(null!=image && image.trim().length>0){
			 if (imageExtension!="PNG" && imageExtension!="JPG" && imageExtension!="GIF" && imageExtension!="JPEG"){
				$('#image_error').html('*Only .png, .jpg, .gif, .jpeg are allowed');
				a=false;
		    }
			else{
				a=true;
				$('#image_error').text("");
			}
		}
		else{
			a=true;
			$('#image_error').text("");
		}
		if(a==true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function LocationIdInput(){
		var locationId=$('#googleLocationId').val();
		if(locationId.length>0){
		$.ajax({
			url : '/kikspot/game/checkrecommendationlocation.do',
			type: 'GET',
			data:{locationId:locationId},
			async: true,
			success:function(data){
				
				if(data==="error"){
					
					$('#googleLocationId_error').text('*No Location exists with this LocationId');
					$('#locationName').val('');
				}
				else if(data!="success"){
				
					$('#googleLocationId_error').text('');
					$('#locationName').val(data);
				}
			}			
		});
		}
		else{
			$('#locationName').val('');
		}
		
		
	}